var _ = require('lodash');
var fs = require('fs');
var mkdirp = require('mkdirp');

var common = require('./common');

fs.readdir(common.languagesFromGoogleDir, function (err, files) {
  var languages = [];

  _.forEach(files, function (file) {
    var langName = file.substr(0, file.length - 4);
    var fileExtension = file.substr(file.length - 4, file.length - 1);

    if (fileExtension !== '.tsv') {
      console.log('Language file "' + file + '" should have [.tsv] extension');
      return;
    }

    var path = common.languagesFromGoogleDir + file;
    languages.push({
      lang: langName,
      path: path
    });
  });

  console.log('Languages found', _.map(languages, 'lang'));
  constructJsonFromTsv(languages);
});

function constructJsonFromTsv(languages) {
  mkdirp(common.languagesFromGoogleAsJsonDir, function (err) {
    if (err) {
      console.log(err, 'dir not created: ', common.languagesFromGoogleAsJsonDir);
      return;
    }

    var languageToLocale = {
      'Czech'     : 'cs',
      'Dutch'     : 'nl',
      'English'   : 'en',
      'French'    : 'fr',
      'German'    : 'de',
      'Hungarian' : 'hu',
      'Italian'   : 'it',
      'Polish'    : 'pl',
      'Romanian'  : 'ro',
      'Russian'   : 'ru',
      'Slovak'    : 'sk',
      'Spanish'   : 'es'
    };

    _.forEach(languages, function (language) {
      var name = languageToLocale[language.lang] == undefined ? language.lang : languageToLocale[language.lang];
      var jsonFileName = common.languagesFromGoogleAsJsonDir + name + '.json';
      fs.access(jsonFileName, fs.F_OK, function (err) {
        // if (!err) {  // no file
        //   console.log('file exists', jsonFileName);
        //   return;
        // }

        fs.readFile(language.path, 'utf8', function (err, tsvData) {
          if (err) {
            console.log('Error while reading "' + language.lang + '": ' + err);
            return;
          }

          var tsvLines = tsvData.split(/\r?\n/);

          var tsvRegexp = /(\S+)\t(.*)/i;
          for (var i = 0, delimiters = 0; i < tsvLines.length; i++) {
            delimiters = (tsvLines[i].split(/\t/) || []).length;
            if (delimiters != 2) {
              tsvRegexp = /(\S+)\t(.*)(\t(.+))/i;
              break;
            }
          }

          var jsonLines = _.map(tsvLines, function (tsvLine) {
            var parts =  tsvLine.split(/\t/);

            var translation = parts[1].replaceAll(/"/, '\\"');
            return '  "' + parts[0] + '": "' + translation + '",';
          });

          var lastLine = jsonLines[jsonLines.length - 1];
          jsonLines[jsonLines.length - 1] = lastLine.substring(0, lastLine.length - 1);

          var jsonKeysValues = _.reduce(jsonLines, function (memo, jsonLine) {
            return memo + jsonLine + '\n';
          }, '');
          var jsonData = '{\n' + jsonKeysValues + '}';

          fs.writeFile(jsonFileName, jsonData, function (err) {
            if (err) {
              console.log(err, 'file not created: ', jsonFileName);
              return;
            }
            console.log('file created: ', jsonFileName);
          });
        });
      });
    });
  });

}

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.split(search).join(replacement);
};