var fs = require('fs');
var _ = require('lodash');

var common = require('./common');

fs.readdir(common.languagesDir, function (err, files) {
  var languages = [];

  _.forEach(files, function (file) {
    var langName = file.substr(0, file.length - 5);
    var path = common.languagesDir + file;
    languages.push({
      lang: langName,
      data: require(path)
    });
  });

  console.log('Languages found', _.map(languages, 'lang'));
  detectEqualities(languages);
});

function detectEqualities(languages) {
  _.forEach(languages, function (language) {
    var otherLanguages = _.filter(languages, function (o) {
      return o.lang !== language.lang;
    });
    _.forEach(otherLanguages, function (otherLanguage) {
      var sameValues = [];
      _.forEach(Object.keys(language.data), function (key) {
        if (otherLanguage.data[key] !== undefined) {
          var value = language.data[key];
          if (value === otherLanguage.data[key]) {
            sameValues.push({
              key: key,
              value: value
            });
          }
        }
      });

      console.log('Equality between "' + language.lang + '" and "' + otherLanguage.lang + '" is: \n' + JSON.stringify(sameValues, null, 2));
    })
  });
}