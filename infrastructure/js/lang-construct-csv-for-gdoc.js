var _ = require('lodash');
var fs = require('fs');
var mkdirp = require('mkdirp');

var common = require('./common');

fs.readdir(common.languagesDir, function (err, files) {
  var languages = [];

  _.forEach(files, function (file) {
    var langName = file.substr(0, file.length - 5);
    var path = common.languagesDir + file;
    languages.push({
      lang: langName,
      data: require(path)
    });
  });

  console.log('Languages found', _.map(languages, 'lang'));
  prepareFiles(languages);
});


function prepareFiles(languages) {
  mkdirp(common.languagesToGoogleDir, function (err) {
    if (err) {
      console.log(err, 'dir not created: ', common.languagesToGoogleDir);
      return;
    }

    _.forEach(languages, function (language) {
      var filename = common.languagesToGoogleDir + language.lang + '.tsv';
      fs.access(filename, fs.F_OK, function (err) {
        if (err) {  // no file
          var data = '';
          _.forEach(Object.keys(language.data), function (key) {
            var value = language.data[key];
            if (value.indexOf(';') !== -1) {
              throw new Error('language ' + language.lang + '. value of key ' + key + ' has ; symbol: ' + value);
            }
            data = data + key + '\t' + value + '\r\n';
          });
          fs.writeFile(filename, data, function (err) {
            if (err) {
              console.log(err, 'file not created: ', filename);
              return;
            }
            console.log('file created: ', filename);
          });
          return;
        }

        console.log('file exists', filename);
      });

    });
  });
}