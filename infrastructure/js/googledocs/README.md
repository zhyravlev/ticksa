The steps below show how to build and run a simple standalone script that export data in all sheets in the current spreadsheet as individual TSV files.

##Set it up

Visit [script.google.com](https://script.google.com/) to open the script editor. (You'll need to be signed in to your Google account.) If this is the first time you've been to script.google.com, you'll be redirected to a page that introduces Apps Script. Click **Start Scripting** to proceed to the script editor.
A welcome screen will ask what kind of script you want to create. Click **Blank Project** or **Close**.
Delete any code in the script editor and paste in the code from the *export-all-sheets-as-tsv.js*.
Select the menu item **File > Save**. Name your new script and click **OK**.

##Try it out

1. To execute the script, either click ▶ or select the menu item **Run > onOpen**.
2. A dialog box will appear and tell you that the script requires authorization. Click **Continue**. A second dialog box will then request authorization for specific Google services. Read the notice carefully, then click **Allow**.
3. A yellow bar at the top of the script editor will indicate when the script is running. When the yellow bar disappears, the script has finished.
4. Open **Ticksa. Translations** in your Google Drive. You'll see that new menu item **Ticksa** is appeared.
5. Select **Ticksa > export all sheets as tsv files** to execute the script.
6. Go to **My drive** and download all files in the script folder that have been created during the script execution.
