var fs = require('fs');
var _ = require('lodash');

var common = require('./common');

fs.readdir(common.languagesDir, function (err, files) {
  var languages = [];

  _.forEach(files, function (file) {
    var langName = file.substr(0, file.length - 5);
    var path = common.languagesDir + file;
    languages.push({
      lang: langName,
      data: require(path)
    });
  });

  console.log('Languages found', _.map(languages, 'lang'));
  detectDifferences(languages);
});

function detectDifferences(languages) {
  _.forEach(languages, function (language) {
    var otherLanguages = _.filter(languages, function (o) {
      return o.lang !== language.lang;
    });
    _.forEach(otherLanguages, function (otherLanguage) {
      var thisKeys = Object.keys(language.data);
      var otherKeys = Object.keys(otherLanguage.data);

      console.log('Difference between "' + language.lang + '" and "' + otherLanguage.lang + '" is: ' + _.difference(thisKeys, otherKeys));
    })
  });
}