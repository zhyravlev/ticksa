var fs = require('fs');
var _ = require('lodash');

var mkdirp = require('mkdirp');

var common = require('./common');

const lang_file = common.languagesDir + 'en.json';

const lang_data = require(lang_file);

const duplicates = {};

_.forEach(Object.keys(lang_data), function (key_src) {
  var value_src = lang_data[key_src];

  _.forEach(Object.keys(lang_data), function (key_dst) {
    if (key_src !== key_dst) {
      var value_dst = lang_data[key_dst];
      if (value_dst == value_src) {
        if (typeof duplicates[value_src] === 'undefined') {
          duplicates[value_src] = [];
        }

        if (_.indexOf(duplicates[value_src], key_dst) == -1) {
          duplicates[value_src].push(key_dst);
        }

      }
    }
  });
});

console.info(duplicates);

var languages = [];

fs.readdir(common.languagesDir, function (err, files) {
  _.forEach(files, function (file) {
    var langName = file.substr(0, file.length - 5);

    // if (langName !== 'en') {
      var path = common.languagesDir + file;
      languages.push({
        lang: langName,
        data: require(path)
      });
    // }
  });

  console.log('Languages found', _.map(languages, 'lang'));

  // console.log('duplicates: ' + Object.keys(duplicates));

  const languages_duplicates = {};

  _.forEach(Object.keys(duplicates), function (original_key) {
    _.forEach(duplicates[original_key], function (translation_key) {
      _.forEach(languages, function (language) {
        // console.log(language.lang + ' : ' + translation_key + ' = ' + language.data[translation_key]);
        if (typeof languages_duplicates[language.lang] === 'undefined') {
          languages_duplicates[language.lang] = {};
        }

        languages_duplicates[language.lang][translation_key] = language.data[translation_key];
      });
    });
  });

  mkdirp(common.languagesDuplicatesDir, function (err) {
    if (err) {
      console.warn(err, 'dir not created: ', common.languagesDuplicatesDir);
      return;
    }

    _.forEach(languages_duplicates, function (value, lang_key) {
      // console.log(lang_key, value);
      var jsonFileName = common.languagesDuplicatesDir + lang_key + '.json';

      fs.access(jsonFileName, fs.F_OK, function (err) {
        if (!err) {  // no file
          console.log('file exists', jsonFileName);
          return;
        }

        var jsonData = value;

        fs.writeFile(jsonFileName, JSON.stringify(jsonData), function (err) {
          if (err) {
            console.log(err, 'file not created: ', jsonFileName);
            return;
          }
          console.log('file created: ', jsonFileName);
        });
      });
    });
  });
});


