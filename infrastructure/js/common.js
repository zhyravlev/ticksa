module.exports = {
  languagesDir: './../../webapp/src/assets/languages/',
  tmpDir: './tmp/',
  languagesToGoogleDir: './tmp/languages-to-google/',
  languagesFromGoogleDir: './tmp/languages-from-google/',
  languagesFromGoogleAsJsonDir: './tmp/languages-from-google-json/',
  languagesDuplicatesDir: './tmp/languages-duplicates/'
};