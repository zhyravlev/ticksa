#!/usr/bin/env bash
cd webapp
rm -rf bower_components
bower install
gulp build && gulp deploy
cd ../server
gulp zip && gulp deploy