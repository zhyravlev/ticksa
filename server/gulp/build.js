'use strict';

var babel = require("gulp-babel");
var cache = require('gulp-cached');
var del = require('del');
var fs = require('fs');
var gulp = require('gulp');
var merge2 = require('merge2');
var runSequence = require('run-sequence');
var sourcemaps = require('gulp-sourcemaps');
var ts = require('gulp-typescript');
var tslint = require('gulp-tslint');

var conf = require('./conf');
var logger = conf.logger('build');
var ticksaDuration = require('./utils/ticksa-duration');

var tsconfig = require('../tsconfig.json');
var tsPaths = {
  app: conf.paths.src.app + '/**/*.ts',
  test: conf.paths.src.test + '/**/*.ts',
  cli: conf.paths.src.cli + '/**/*.ts'
};

gulp.task('build:tslint', function () {
  return gulp.src([tsPaths.app, tsPaths.test, tsPaths.cli])
    .pipe(tslint({configuration: '../tslint.json'}))
    .pipe(tslint.report())
});

gulp.task('build:validate', function (done) {
  try {
    logger.info('Check that webapp files folder exists: ' + conf.paths.src.webapp);
    var stats = fs.lstatSync(conf.paths.src.webapp);

    if (stats.isDirectory()) {
      logger.info('Success! Folder exists');
      done();
    } else {
      logger.error('Folder [' + cof.paths.src.webapp + '] missed');
      done('Error');
    }
  } catch (err) {
    logger.error(err);
    done(err);
  }
});

gulp.task('build:clean', function (done) {
  return del([
    conf.paths.src.app + '/**/*.js',
    conf.paths.src.app + '/**/*.js.map',
    conf.paths.src.test + '/**/*.js',
    conf.paths.src.test + '/**/*.js.map',
    conf.paths.src.cli + '/**/*.js',
    conf.paths.src.cli + '/**/*.js.map',
    '!' + conf.paths.src.webapp + '/**/*',
  ], {force: true}, done);
});

gulp.task('build:compile', function () {
  var app = tsCompile(tsPaths.app);
  var test = tsCompile(tsPaths.test);
  var cli = tsCompile(tsPaths.cli);

  return merge2([
    app.pipe(gulp.dest(conf.paths.src.app)),
    test.pipe(gulp.dest(conf.paths.src.test)),
    cli.pipe(gulp.dest(conf.paths.src.cli))
  ]);
});

gulp.task('build:clean-and-compile', function (done) {
  return runSequence('build:clean', 'build:compile', done);
});

gulp.task('build', function (done) {
  return runSequence('build:validate', 'build:clean', 'build:compile', done);
});

function tsCompile(path) {
  var durationLogger = ticksaDuration.logger(logger);
  var duration = ticksaDuration.start(durationLogger);

  return gulp.src(path)
    .pipe(cache('tsCompile'))
    .pipe(duration.step('Cache generation for ' + path))
    .pipe(sourcemaps.init())
    .pipe(duration.step('Sourcemaps init for ' + path))
    .pipe(ts(tsconfig.compilerOptions))
    .pipe(duration.step('Compiling ' + path))
    .pipe(babel({
      // .babelrc file is in the same directory
      babelrc: true,
      presets: ['es2015', 'stage-0']
    }))
    .pipe(duration.step('Babelify ' + path))
    .pipe(sourcemaps.write('.', {sourceRoot: ''}))
    .pipe(duration.step('Sourcemaps write for ' + path));
}
