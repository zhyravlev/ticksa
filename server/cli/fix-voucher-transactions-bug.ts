/// <reference path="../app/typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Promise = require('bluebird');

import env = require('../app/env/env');
import program = require('commander');
import context = require('../app/context');
import connection = require('../app/connection');

const logger = require('../app/logger/logger')('fix-voucher-migration.ts');

env.throwExceptionIfInvalidEnv();

program
  .version('0.0.1');
program.parse(process.argv);


async function start() {
  await context.init();

  let query = 'WITH transactions AS (SELECT ta.ticks_transaction_id ' +
    'FROM "TicksAmount" AS ta, ' +
    '(SELECT round(extract(\'epoch\' FROM amount.created_at) / 10) AS round, ' +
    'MAX(amount.ticks_transaction_id)                    AS ticks_transaction_id, ' +
    'COUNT(*)                                            AS count ' +
    'FROM "TicksAmount" AS amount ' +
    'WHERE amount.amount = 50 AND amount.type = 0 ' +
    'AND amount.created_at > \'2016-04-18 23:59\' AND amount.created_at < \'2016-06-30 23:59\' ' +
    'GROUP BY round) AS result ' +
    'WHERE result.count > 1 AND ta.ticks_transaction_id = result.ticks_transaction_id) ' +
    'DELETE FROM "TicksAmount" ' +
    'WHERE ticks_transaction_id IN (SELECT * FROM transactions);';

  let any = await connection.query(query, {type: Sequelize.QueryTypes.DELETE});
  // console.log('any', any);
  logger.log('any', any);

  let updateQuery = 'UPDATE "Account" SET current_balance = current_balance + (SELECT ' +
  'SUM(ta.amount) - ' +
  '(a.current_balance + ' +
  'a.current_balance_free_ticks + ' +
  'a.current_balance_earned_ticks) AS result ' +
  'FROM "TicksAmount" AS ta, "Account" AS a ' +
  'WHERE ta.account_id = -4 AND ta.account_id = a.id GROUP BY a.id) WHERE id = -4 ';

  await connection.query(updateQuery, {type: Sequelize.QueryTypes.UPDATE});
}

start().then(function() {
  logger.info('Voucher transactions fixed');
  process.exit(0);
}).catch(function (err) {
  logger.err(err);
  process.exit(0);
});
