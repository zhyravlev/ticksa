/// <reference path="../app/typings/tsd.d.ts"/>
const logger = require('../app/logger/logger')('thumbnails.ts');

/*
 * This module was created during issue APP-807
 * It is a command line tool that creates thumbnails.
 * Flow:
 *  1) it queries all Video instances from DB (or only one instance by its ID)
 *  2) with help of ffmpeg it makes thumbnail in size of videofile
 *  3) it uploads new file to AWS S3
 *  4) it changes thumbnail image File instance - associate it with new created file
 *  5) old thumbnail file is still on S3 - to prevent legacy link issues
 */

import env = require('../app/env/env');
env.throwExceptionIfInvalidEnv();

import program = require('commander');
import context = require('../app/context');
import ThumbnailService = require('../app/services/thumbnail-service');

program
  .version('0.0.1')
  .option('--videoId <n>', 'Process thumbnail by content ID')
  .option('--all', 'Process thumbnail by all contents');
program.parse(process.argv);

const videoId: number = program['videoId'];
const all: boolean = program['all'];

logger.info("Video ID:", videoId);
logger.info("Flag all:", all);

context.init()
  .then(function () {
    if (all) {
      logger.info('Process thumbnails by all videos started');
      return ThumbnailService.generateAll()
        .catch((err) => logger.error('Thumbnail generation error!', err));
    }

    if (videoId != null) {
      logger.info('Process thumbnail by video ID ' + videoId + ' started');
      return ThumbnailService.generateById(videoId)
        .catch((err) => logger.error('Thumbnail generation error!', err));
    }

    logger.error('Cant execute thumbnails process, invalid parameters');
  })
  .finally(() => process.exit(0));
