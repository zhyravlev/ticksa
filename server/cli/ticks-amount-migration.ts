/// <reference path="../app/typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Promise = require('bluebird');

import env = require('../app/env/env');
import program = require('commander');
import context = require('../app/context');
import connection = require('../app/connection');

const logger = require('../app/logger/logger')('ticks-amount-migration.ts');

env.throwExceptionIfInvalidEnv();

program
  .version('0.0.1');
program.parse(process.argv);

interface QueryResult {
  ticks_amount_id: number,
  ticks_transaction_id: number,
}

async function start() {
  await context.init();

  let query = 'UPDATE "TicksAmount" AS ticksAmount SET type = 1 FROM ' +
    '(SELECT * FROM "TicksAmount" WHERE ticks_transaction_id IN ' +
    '(SELECT ticks_transaction_id FROM (SELECT ta.ticks_transaction_id, count(*) AS count FROM "TicksAmount" AS ta ' +
    'WHERE ta.ticks_transaction_id IN (SELECT t.ticks_transaction_id FROM "TicksAmount" AS t WHERE t.account_id = -1 AND t.created_at < \'2016-03-01\') ' +
    'GROUP BY ta.ticks_transaction_id) AS res WHERE res.count <= 4)) AS result ' +
    'WHERE ticksAmount.ticks_transaction_id = result.ticks_transaction_id ' +
    'RETURNING ticksAmount.id AS ticks_amount_id, ticksAmount.ticks_transaction_id;';

  let result: QueryResult[] = await connection.query(query, {type: Sequelize.QueryTypes.UPDATE});

  let queryForMultiplyTypeAmounts = 'UPDATE "TicksAmount" AS ta SET type = 1 WHERE ta.id ' +
  'IN (215,216,217, 1123, 1124, 1125, 5403, 5404, 5405, 5474, 5475, 5476, ' +
  '  8685, 8686, 8687, 8890, 8891, 8892, 8962, 8963, 8964, 9407, 9408, 9409, ' +
  '  10326, 10327, 10328, 10451, 10452, 10453, 10805, 10806, 10807, 11366, 11367, 11368, 11369, ' +
  '  11746, 11747, 11748, 12363, 12364, 12365, 12377, 12378, 12379, 1275, 12755, 12756, ' +
  '  12830, 12831, 12832, 13079, 13080, 13081) ' +
  'RETURNING id AS ticks_amount_id, ticks_transaction_id;';

  let resultForMultiplyTypeAmounts: QueryResult[] = await connection.query(queryForMultiplyTypeAmounts, {type: Sequelize.QueryTypes.UPDATE});

  result = result.concat(resultForMultiplyTypeAmounts);

  logger.info('Migration finished. Updated ' + result.length + ' rows');
  logger.info('result:', JSON.stringify(result));
}

start().then(function() {
  process.exit(0);
}).catch(function () {
  process.exit(0);
});
