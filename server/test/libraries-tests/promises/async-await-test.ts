/// <reference path="../../../app/typings/tsd.d.ts"/>

import * as chai from 'chai';
import * as Promise from 'bluebird';

import * as Fixtures from '../../utils-for-tests/fixtures';
import {Tickser} from '../../../app/models/models';
import {NotFound} from "../../../app/errors";

const assert = chai.assert;

describe('Async/Await test', function () {

  describe('Basics', function () {

    it('Any promise can be awaited. Success', async function () {
      const promise = new Promise<number>(function (resolve) {
        resolve(1);
      }).then(number => number + 2);
      const result = await promise;
      assert.equal(result, 3);
    });

    it('Any promise can be awaited. Failure', async function () {
      const promise = new Promise(function (resolve) {
        resolve(1);
      }).then(number => Promise.reject('Some error happened'));

      try {
        await promise;
        assert.fail('No exception', 'But we expect exception');
      } catch (err) {
        assert.equal(err, 'Some error happened');
      }
    });

    it('Async function can be used in non-async code in promise-like way', function (done: MochaDone) {
      asyncFun()
        .then(x => {
          assert.equal(x, 11);
          done();
        })
        .catch(done);
    });

  });

  describe('Check DB querying function', function () {

    let sideEffect = 0;

    before('init fixtures', function (done: MochaDone) {
      Fixtures.Builder.defaultFixtures().build()
        .then(() => done())
        .catch(done);
    });

    beforeEach('clear side effect', function() {
      sideEffect = 0;
    });

    it('Tickser querying as simple promise. Promise way', function (done: MochaDone) {
      asSimplePromise()
        .then((tickser: Tickser.Instance) => {
          assert.equal(tickser.name, 'tickser1');
          done();
        })
        .catch(done);
    });

    it('Tickser querying with side effect. Promise way', function (done: MochaDone) {
      asPromiseWithSideEffect()
        .then((tickser: Tickser.Instance) => {
          assert.equal(tickser.name, 'tickser1');
          assert.equal(sideEffect, 1);
          done();
        })
        .catch(done);
    });

    it('Tickser querying as simple promise. Aync/await way', async function () {
      const tickser: Tickser.Instance = await asSimplePromise();
      assert.equal(tickser.name, 'tickser1');
    });

    it('Tickser querying with side effect. Aync/await way', async function () {
      const tickser: Tickser.Instance = await asPromiseWithSideEffect();
      assert.equal(tickser.name, 'tickser1');
      assert.equal(sideEffect, 1);
    });

    async function asSimplePromise(): Promise<Tickser.Instance> {
      return Tickser.Model.findById(1);
    }

    async function asPromiseWithSideEffect(): Promise<Tickser.Instance> {
      const tickser = await Tickser.Model.findById(1);
      sideEffect = 1;
      return tickser;
    }
  });

  describe('Handle async-functions in non-async code', function () {

    it('Without return keyword', function (done: MochaDone) {
      const res = asEmptyPromise();
      assert.isDefined(res.then, 'Then not defined');
      assert.isDefined(res.catch, 'Catch not defined');
      res
        .then(result => {
          assert.isUndefined(result, 'Result is not an undefined');
          done()
        })
        .catch(done);

      async function asEmptyPromise(): Promise<void> {
      }
    });

    it('Returning undefined', function (done: MochaDone) {
      const res = asUndefinedPromise();
      assert.isDefined(res.then, 'Then not defined');
      assert.isDefined(res.catch, 'Catch not defined');
      res
        .then(result => {
          assert.isUndefined(result, 'Result is not an undefined');
          done()
        })
        .catch(done);

      async function asUndefinedPromise(): Promise<void> {
        return;
      }
    });

    it('Returning string', function (done: MochaDone) {
      const res = asStringPromise();
      assert.isDefined(res.then, 'Then not defined');
      assert.isDefined(res.catch, 'Catch not defined');
      res
        .then(result => {
          assert.isString(result);
          assert.equal(result, 'foo');
          done()
        })
        .catch(done);

      async function asStringPromise(): Promise<string> {
        return 'foo';
      }
    });

    it('Returning number', function (done: MochaDone) {
      const res = asNumberPromise();
      assert.isDefined(res.then, 'Then not defined');
      assert.isDefined(res.catch, 'Catch not defined');
      res
        .then(result => {
          assert.isNumber(result);
          assert.equal(result, 42);
          done()
        })
        .catch(done);

      async function asNumberPromise(): Promise<number> {
        return 42;
      }
    });

    it('No return and contains promise', function (done: MochaDone) {
      const res = asEmptyPromiseThatContainsAnotherPromise();
      assert.isDefined(res.then, 'Then not defined');
      assert.isDefined(res.catch, 'Catch not defined');
      res
        .then(result => {
          assert.isUndefined(result, 'Result is not an undefined');
          done()
        })
        .catch(done);

      async function asEmptyPromiseThatContainsAnotherPromise(): Promise<void> {
        await asyncFun();
      }
    });

    it('Returning empty array', function (done: MochaDone) {
      const res = asPromise();
      assert.isDefined(res.then, 'Then not defined');
      assert.isDefined(res.catch, 'Catch not defined');
      res
        .then(result => {
          assert.deepEqual(result, [], 'Result is not an empty array');
          done()
        })
        .catch(done);

      async function asPromise(): Promise<number[]> {
        return [];
      }
    });
  });

  describe('Handle async-functions in async code', function () {

    it('Without return keyword', async function () {
      const res = await asEmptyPromise();
      assert.isUndefined(res, 'Result is not an undefined');

      async function asEmptyPromise(): Promise<void> {
      }
    });

    it('Returning undefined', async function () {
      const res = await asUndefinedPromise();
      assert.isUndefined(res, 'Result is not an undefined');

      async function asUndefinedPromise(): Promise<any> {
        return;
      }
    });

    it('Returning string', async function () {
      const res = await asStringPromise();
      assert.isString(res);
      assert.equal(res, 'foo');

      async function asStringPromise(): Promise<string> {
        return 'foo';
      }
    });

    it('Returning number', async function () {
      const res = await asNumberPromise();
      assert.isNumber(res);
      assert.equal(res, 42);

      async function asNumberPromise(): Promise<number> {
        return 42;
      }
    });

    it('No return and contains promise', async function () {
      const res = await asEmptyPromiseThatContainsAnotherPromise();
      assert.isUndefined(res, 'Result is not an undefined');

      async function asEmptyPromiseThatContainsAnotherPromise(): Promise<void> {
        await asyncFun();
      }
    });

    it('Returning empty array', async function () {
      const res = await asPromise();
      assert.deepEqual(res, [], 'Result is not an empty array');

      async function asPromise(): Promise<number[]> {
        return [];
      }
    });
  });

  describe('Returning from async that waits for another async result', function () {
    let sideEffect: number = 0;

    beforeEach('clean side effect', function () {
      sideEffect = 0;
    });

    it('Promise style', function (done: MochaDone) {
      assert.equal(sideEffect, 0, 'Wrong initial side effect');

      const res = asAsyncThatWaitsForAnotherAsync();
      assert.isDefined(res.then, 'Then not defined');
      assert.isDefined(res.catch, 'Catch not defined');
      res
        .then(result => {
          assert.equal(sideEffect, 1, 'Side effect missed');
          assert.isNumber(result);
          assert.equal(result, 42, 'Incorrect result');
          done();
        })
        .catch(done);
    });

    it('Async/await style', async function () {
      assert.equal(sideEffect, 0, 'Wrong initial side effect');

      const res = await asAsyncThatWaitsForAnotherAsync();
      assert.equal(sideEffect, 1, 'Side effect missed');
      assert.isNumber(res);
      assert.equal(res, 42, 'Incorrect result');
    });

    async function asAsyncThatWaitsForAnotherAsync(): Promise<number> {
      await someSideEffect();
      return 42;
    }

    function someSideEffect(): Promise<void> {
      return Promise.resolve()
        .then(() => {
          sideEffect = 1;
        });
    }

  });

  describe('Exceptions inside async functions', function () {

    it('Handle in promise-like way', function (done: MochaDone) {
      withException()
        .then(_ => done(new Error('We should not be here because exception was thrown')))
        .catch(err => {
          assert.instanceOf(err, Error);
          assert.instanceOf(err, NotFound);
          assert.equal(err.message, 'foo');
          assert.equal(err.status, 404);
          assert.equal(err.name, 'NotFound');
          done();
        });

    });

    it('Handle in async-like way', async function (done: MochaDone) {
      try {
        await withException();
        done(new Error('We should not be here because exception was thrown'));
      } catch (err) {
        assert.instanceOf(err, Error);
        assert.instanceOf(err, NotFound);
        assert.equal(err.message, 'foo');
        assert.equal(err.status, 404);
        assert.equal(err.name, 'NotFound');
        done();
      }
    });

    async function withException() {
      throw new NotFound('foo');
    }
  });

  describe('Nested exception', function () {

    it('If exception in async inside non-async Promise', async function (done: MochaDone) {
      try {
        await withRejectedPromise();
        done(new Error('We should not be here because Promise rejected'));
      } catch (err) {
        assert.isString(err);
        assert.equal(err, 'foo');
        done();
      }

      async function withRejectedPromise() {
        await rejectedPromise();
        done(new Error('[inside] We should not be here because Promise rejected'));
      }

      function rejectedPromise(): Promise<string> {
        return Promise.reject('foo');
      }
    });
  });

  describe('Logic branching', function () {

    it('Condition that affects async result', async function (done: MochaDone) {
      try {
        const trueCondition = await conditionalAsyncResult(true);
        assert.deepEqual(trueCondition, [7, 42, 11]);

        const falseCondition = await conditionalAsyncResult(false);
        assert.deepEqual(falseCondition, [7]);

        done();
      } catch (err) {
        done(err);
      }

      async function conditionalAsyncResult(condition: boolean): Promise<number[]> {
        const result: number[] = [7];
        if (condition) {
          const someNumber = await asyncFun();
          result.push(42);
          result.push(someNumber);
        }
        return result;
      }
    });

    describe('Conditional executions', function() {

      const SIDE_EFFECT: string = 'SIDE_EFFECT';
      const NOT_EXECUTED: string = 'NOT_EXECUTED';
      let sideEffect = {};
      let shouldExecute = {};  // used to prevent possible compiler optimizations

      beforeEach('clear side effect', function () {
        sideEffect = {};
        shouldExecute = {};
      });

      it('Was executed', async function() {
        shouldExecute['execute'] = true;
        await conditionalExecution('execute');

        assert.equal(sideEffect['execute'], SIDE_EFFECT);
      });

      it('Was not executed', async function() {
        shouldExecute['do-not-execute'] = false;
        await conditionalExecution('do-not-execute');

        assert.equal(sideEffect['do-not-execute'], NOT_EXECUTED);
      });

      async function conditionalExecution(conditionName: string): Promise<void> {
        const condition = shouldExecute[conditionName];

        if (condition) {
          await asyncWithSideEffect(conditionName);
        }

        if (!condition) {
          sideEffect[conditionName] = NOT_EXECUTED;
        }
      }

      async function asyncWithSideEffect(sideEffectName: string): Promise<void> {
        return new Promise<void>(function (resolve) {
          sideEffect[sideEffectName] = SIDE_EFFECT;
          resolve();
        });
      }
    });
  });

  describe('Multiple flat sequential promises', function () {
    const ON_SUCCESS: string = 'ON_SUCCESS';
    const ON_FAILURE: string = 'ON_FAILURE';
    let sideEffects = {
      // it is a container to collect side effects
    };

    beforeEach('clear side effects', function () {
      sideEffects = {};
    });

    it('Two promises. All successful', async function () {
      await (async function () {
        await successSideEffect('first');
        await successSideEffect('second');
      })();

      assert.equal(sideEffects['first'], ON_SUCCESS);
      assert.equal(sideEffects['second'], ON_SUCCESS);
    });

    it('Two promises. The second is rejected', async function () {
      try {
        await (async function () {
          await successSideEffect('first');
          await failureSideEffect('second');
        })();
        assert.fail('No exception', 'We want exception');
      } catch (err) {
        assert.equal(sideEffects['first'], ON_SUCCESS);
        assert.equal(err, 'Failure for: second');
        assert.equal(sideEffects['second'], ON_FAILURE);
      }
    });

    it('Two promises. The first is rejected', async function () {
      try {
        await (async function () {
          await failureSideEffect('first');
          await failureSideEffect('second');  // here we try create failure, but it will not be called
        })();
        assert.fail('No exception', 'We want exception');
      } catch (err) {
        assert.equal(err, 'Failure for: first');
        assert.equal(sideEffects['first'], ON_FAILURE);
        assert.isUndefined(sideEffects['second']);
      }

      try {
        await (async function () {
          await failureSideEffect('first');
          await successSideEffect('second');  // here we try create success, but it will not be called
        })();
        assert.fail('No exception', 'We want exception');
      } catch (err) {
        assert.equal(err, 'Failure for: first');
        assert.equal(sideEffects['first'], ON_FAILURE);
        assert.isUndefined(sideEffects['second']);
      }
    });

    it('Two promises. All successful. Promise-like way', function (done: MochaDone) {
      const res: Promise<void> = (async function () {
        await successSideEffect('first');
        await successSideEffect('second');
      })();

      res
        .then(() => {
          assert.equal(sideEffects['first'], ON_SUCCESS);
          assert.equal(sideEffects['second'], ON_SUCCESS);
          done();
        })
        .catch(done)
    });

    it('Two promises. The second is rejected. Promise-like way', function (done: MochaDone) {
      const res: Promise<void> = (async function () {
        await successSideEffect('first');
        await failureSideEffect('second');
      })();

      res
        .then(() => done(new Error('We should not be here, because we expect exception')))
        .catch((err) => {
          assert.equal(sideEffects['first'], ON_SUCCESS);
          assert.equal(err, 'Failure for: second');
          assert.equal(sideEffects['second'], ON_FAILURE);
          done();
        })
        .catch(done);
    });

    it('Two promises. The first is rejected. Promise-like way', function (done: MochaDone) {
      const res: Promise<void> = (async function () {
        await failureSideEffect('first');
        await failureSideEffect('second');   // here we try create failure, but it will not be called
      })();

      res
        .then(() => done(new Error('We should not be here, because we expect exception')))
        .catch((err) => {
          assert.equal(err, 'Failure for: first');
          assert.equal(sideEffects['first'], ON_FAILURE);
          assert.isUndefined(sideEffects['second']);
          done();
        })
        .catch(done);
    });

    it('Three promises. All successful', async function () {
      await (async function () {
        await successSideEffect('first');
        await successSideEffect('second');
        await successSideEffect('third');
      })();

      assert.equal(sideEffects['first'], ON_SUCCESS);
      assert.equal(sideEffects['second'], ON_SUCCESS);
      assert.equal(sideEffects['third'], ON_SUCCESS);
    });

    it('Three promises. The third is rejected', async function () {
      try {
        await (async function () {
          await successSideEffect('first');
          await successSideEffect('second');
          await failureSideEffect('third');
        })();
        assert.fail('No exception', 'We want exception');
      } catch (err) {
        assert.equal(sideEffects['first'], ON_SUCCESS);
        assert.equal(sideEffects['second'], ON_SUCCESS);
        assert.equal(err, 'Failure for: third');
        assert.equal(sideEffects['third'], ON_FAILURE);
      }
    });

    it('Three promises. The second is rejected', async function () {
      try {
        await (async function () {
          await successSideEffect('first');
          await failureSideEffect('second');
          await failureSideEffect('third');   // here we try create failure, but it will not be called
        })();
        assert.fail('No exception', 'We want exception');
      } catch (err) {
        assert.equal(sideEffects['first'], ON_SUCCESS);
        assert.equal(err, 'Failure for: second');
        assert.equal(sideEffects['second'], ON_FAILURE);
        assert.isUndefined(sideEffects['third']);
      }

      try {
        await (async function () {
          await successSideEffect('first');
          await failureSideEffect('second');
          await successSideEffect('third');   // here we try create success, but it will not be called
        })();
        assert.fail('No exception', 'We want exception');
      } catch (err) {
        assert.equal(sideEffects['first'], ON_SUCCESS);
        assert.equal(err, 'Failure for: second');
        assert.equal(sideEffects['second'], ON_FAILURE);
        assert.isUndefined(sideEffects['third']);
      }
    });

    it('Three promises. The first is rejected', async function () {
      try {
        await (async function () {
          await failureSideEffect('first');
          await failureSideEffect('second');  // here we try create failure, but it will not be called
          await failureSideEffect('third');   // here we try create failure, but it will not be called
        })();
        assert.fail('No exception', 'We want exception');
      } catch (err) {
        assert.equal(err, 'Failure for: first');
        assert.equal(sideEffects['first'], ON_FAILURE);
        assert.isUndefined(sideEffects['second']);
        assert.isUndefined(sideEffects['third']);
      }

      try {
        await (async function () {
          await failureSideEffect('first');
          await successSideEffect('second');  // here we try create success, but it will not be called
          await successSideEffect('third');   // here we try create success, but it will not be called
        })();
        assert.fail('No exception', 'We want exception');
      } catch (err) {
        assert.equal(err, 'Failure for: first');
        assert.equal(sideEffects['first'], ON_FAILURE);
        assert.isUndefined(sideEffects['second']);
        assert.isUndefined(sideEffects['third']);
      }
    });

    it('Four promises. All successful', async function () {
      await (async function () {
        await successSideEffect('first');
        await successSideEffect('second');
        await successSideEffect('third');
        await successSideEffect('fourth');
      })();
      assert.equal(sideEffects['first'], ON_SUCCESS);
      assert.equal(sideEffects['second'], ON_SUCCESS);
      assert.equal(sideEffects['third'], ON_SUCCESS);
      assert.equal(sideEffects['fourth'], ON_SUCCESS);
    });

    async function successSideEffect(sideEffectName: string): Promise<void> {
      sideEffects[sideEffectName] = ON_SUCCESS;
    }

    async function failureSideEffect(sideEffectName: string): Promise<void> {
      sideEffects[sideEffectName] = ON_FAILURE;
      return Promise.reject('Failure for: ' + sideEffectName);
    }
  });

  async function asyncFun(): Promise<number> {
    let value = await Promise
      .resolve(1)
      .then(x => x * 3)
      .then(x => x + 5)
      .then(x => x / 2);
    value += 7;

    return value;
  }
});
