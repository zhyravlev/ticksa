/// <reference path="../../../app/typings/tsd.d.ts"/>
import chai = require('chai');
import Promise = require('bluebird');

const assert = chai.assert;

// Test for idea how to loop around promises
// Case is:
//  * iterate few times
//  * should check that correct count of iterations passed
//  * result should depend on async function call
describe('WhileLoopTest', function () {

  // http://stackoverflow.com/questions/24660096/correct-way-to-write-loops-for-promise
  it('Promise way', function (done: MochaDone) {
    let counter: number = 5;
    let sumOfRounds: number = 0;

    function promise(count: number): Promise<number> {
      sumOfRounds += count;
      return Promise.resolve(--count);
    }

    var promiseFor = Promise.method(
      function (condition: (count: number) => boolean,
                action: (count: number) => Promise<number>,
                counter: number): Promise<any> {
        if (!condition(counter)) {
          return Promise.resolve(42);
        }
        return action(counter)
          .then(promiseFor.bind(null, condition, action));
      });

    promiseFor(count => count > 0, count => promise(count), counter)
      .then((res: number) => {
        assert.equal(res, 42);
        assert.equal(sumOfRounds, 15);
        done()
      })
      .catch(done);
  });

  // It looks as usual code! Amazing!
  it('Async/Await way', async function() {
    let counter: number = 5;
    let sumOfRounds: number = 0;
    let result: number = 0;

    while (counter > 0) {
      sumOfRounds += counter;
      counter--;
      result = await asyncFunc(counter);
    }

    assert.equal(result, 42);
    assert.equal(sumOfRounds, 15);

    function asyncFunc(delta: number): Promise<number> {
      return Promise.resolve()
        .then(_ => 42 - delta);
    }
  });

});
