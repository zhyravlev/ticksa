/// <reference path="../../../app/typings/tsd.d.ts"/>
import chai = require('chai');
var assert = chai.assert;

import Promise = require('bluebird');

describe('PromisesTest', function () {

  it('Should go to "then" if rejection is caught', function (done: MochaDone) {
    Promise.reject('rejection reason')
      .catch(function (err: string) {
        assert.equal(err, 'rejection reason');
        return 'rejection caught';
      })
      .then(function (data: string) {
        assert.equal(data, 'rejection caught');
        done();
      })
      .catch(function (err) {
        done(new Error('should not be here: ' + err));
      })
  });

  it('Should go to "then" if Error is caught', function (done: MochaDone) {
    Promise.resolve('some data')
      .then(function (err: string) {
        throw new Error('some error');
      })
      .catch(function (err: Error) {
        assert.equal(err.message, 'some error');
        return 'error caught'
      })
      .then(function (data: string) {
        assert.equal(data, 'error caught');
        done();
      })
      .catch(function (err) {
        done(new Error('should not be here: ' + err));
      })
  });

  it('Using .spread is helpful', function (done: MochaDone) {
    Promise.all([1, 2, 3, 4]).spread(function (result1, result2) {
      assert.equal(result1, 1);
      assert.equal(result2, 2);
      done();
    })
  });

  it('If no data returned then Promise is resolved too. Promise.try()', function (done: MochaDone) {
    Promise.try(function () {
      // no data returned
    })
      .then(function (data: any) {
        assert.isUndefined(data);
        done();
      })
      .catch(function (err: any) {
        done(new Error('should not be here: ' + err));
      })
  });

  it('If no data returned then Promise is resolved too. Promise.resolve()', function (done: MochaDone) {
    Promise.resolve(1)
      .then(function () {
        // no data returned
      })
      .then(function (data: any) {
        assert.isUndefined(data);
        done();
      })
      .catch(function (err: any) {
        done(new Error('should not be here: ' + err));
      })
  });

  it('Catch section can be skipped - flow can be on the next then section', function (done: MochaDone) {
    Promise.resolve(1)
      .then(function (data: number) {
        return data;
      })
      .catch(function (err: any) {
        done(new Error('should not be here: ' + err));
      })
      .then(function (data: number) {
        assert.equal(data, 1, 'Catch section skipped, you are here');
        done();
      })
  });

  it('Test Promise.try() API on resolving', function (done: MochaDone) {
    Promise.try(() => {
      return 'ok';
    })
      .then((data: string) => {
        assert.equal(data, 'ok');
        done();
      });
  });

  it('Test Promise.try() API on rejecting', function (done: MochaDone) {
    Promise.try(() => {
      return Promise.reject('rejected');
    })
      .then((data: string) => {
        done(new Error('should not be here: ' + data));
      })
      .catch((err) => {
        assert.equal(err, 'rejected', '"Then" section skipped, you are here');
        done();
      });
  });

});