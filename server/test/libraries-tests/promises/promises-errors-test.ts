///<reference path="../../../app/typings/tsd.d.ts"/>

import chai = require('chai');
import chaiAsPromised = require('chai-as-promised');

// http://chaijs.com/plugins/chai-as-promised/
chai.use(chaiAsPromised);

import Promise = require('bluebird');
import E = require('../../../app/errors');

const assert = chai.assert;

describe('PromisesErrorsTest', function () {

  it('check that bluebird promises can handle custom errors', function (done: MochaDone) {
    Promise.resolve(0).then(function() {
      throw new E.ServerError('This is errror');
    }).catch(E.ServerError, function (e: E.ServerError) {
      assert.equal('This is errror', e.message);
      assert.equal(500, e.httpCode);

      // we should be here
      done();
    }).catch(function () {

      // we should not be here
      done(new Error('we should not be here, because error should be handled in another "catch" section'));
    });
  });

  it('same checks but with chai-as-promised', function() {
    const promiseWithInternalError = Promise.resolve(0).then(function() {
      throw new E.ServerError('This is error');
    });
    return assert.isRejected(promiseWithInternalError);
  });
});
