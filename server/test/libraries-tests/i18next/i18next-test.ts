/// <reference path="../../../app/typings/tsd.d.ts"/>

import mocha = require('mocha');
import chai = require('chai');
import _ = require('lodash');
import i18next = require('i18next');
let assert = chai.assert;

describe('i18next test', function () {

  it('no languages', function () {
    const noLanguages: I18next.ResourceStore = {};
    const i18n: I18next.I18n = i18next.init({
      saveMissing: false,
      debug: false,
      resources: noLanguages,
      fallbackLng: null
    });
    assert.equal(i18n.t(undefined), '');
    assert.equal(i18n.t(null), '');
    assert.equal(i18n.t(''), '');
    assert.equal(i18n.t('foo'), 'foo');
    assert.equal(i18n.t('foo', {lng: null}), 'foo');
    assert.equal(i18n.t('foo', {lng: ''}), 'foo');
    assert.equal(i18n.t('foo', {lng: 'any'}), 'foo');
  });

  it('language exists', function () {
    const languageName = 'lng';
    const oneLanguage: I18next.ResourceStore = {};
    oneLanguage[languageName] = {
      translation: {
        fooKey: 'fooValue'
      }
    };
    const i18n: I18next.I18n = i18next.init({
      saveMissing: false,
      debug: false,
      resources: oneLanguage,
      fallbackLng: languageName
    });
    assert.equal(i18n.t(undefined), '');
    assert.equal(i18n.t(null), '');
    assert.equal(i18n.t(''), '');
    assert.equal(i18n.t('fooKey'), 'fooValue');
    assert.equal(i18n.t('fooKey', {lng: null}), 'fooValue');
    assert.equal(i18n.t('fooKey', {lng: ''}), 'fooValue');
    assert.equal(i18n.t('fooKey', {lng: 'any'}), 'fooValue');
    assert.equal(i18n.t('fooKey', {lng: languageName}), 'fooValue');
    assert.equal(i18n.t('bar'), 'bar');
    assert.equal(i18n.t('bar', {lng: null}), 'bar');
    assert.equal(i18n.t('bar', {lng: ''}), 'bar');
    assert.equal(i18n.t('bar', {lng: 'any'}), 'bar');
    assert.equal(i18n.t('bar', {lng: languageName}), 'bar');
  });

});