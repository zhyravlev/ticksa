/// <reference path="../../../app/typings/tsd.d.ts"/>

import events = require('events');
import stream = require('stream');
import mocha = require('mocha');
import chai = require('chai');
import _ = require('lodash');

import EventUtils = require('../../../app/utils/event-utils');
import CsvUtils = require('../../../app/utils/csv-utils');

const csv = require('csv');
const assert = chai.assert;

describe('csv test', function () {

  const HEADERS: any = {
    numberField: 'numberValue',
    stringField: 'stringValue',
  };

  interface Data {
    numberField: number,
    stringField: string
  }

  interface ExtendedData extends Data {
    additionalData: string
  }

  it('no headers', function (done: MochaDone) {
    const stringifier = csv.stringify({columns: HEADERS});

    const castedToParentClass: stream.Transform = CsvUtils.castToParentClass(stringifier);
    castedToParentClass.end();

    streamToString(castedToParentClass)
      .then(str => {
        assert.equal(str, '');
        done();
      })
      .catch(done);
  });

  it('only headers', function (done: MochaDone) {
    const stringifier = csv.stringify({header: true, columns: HEADERS});

    const castedToParentClass: stream.Transform = CsvUtils.castToParentClass(stringifier);
    castedToParentClass.end();

    streamToString(castedToParentClass)
      .then(str => {
        assert.equal(str, 'numberValue,stringValue\n');
        done();
      })
      .catch(done);
  });

  it('headers and single data', function (done: MochaDone) {
    const stringifier = csv.stringify({header: true, columns: HEADERS});

    const data: Data = {numberField: 42, stringField: 'foo'};
    writeToStringifier(stringifier, data);

    const castedToParentClass: stream.Transform = CsvUtils.castToParentClass(stringifier);
    castedToParentClass.end();

    streamToString(castedToParentClass)
      .then(str => {
        assert.equal(str, 'numberValue,stringValue\n42,foo\n');
        done();
      })
      .catch(done);
  });

  it('no headers but single data', function (done: MochaDone) {
    const stringifier = csv.stringify({header: false, columns: HEADERS});

    const data: Data = {numberField: 42, stringField: 'foo'};
    writeToStringifier(stringifier, data);

    const castedToParentClass: stream.Transform = CsvUtils.castToParentClass(stringifier);
    castedToParentClass.end();

    streamToString(castedToParentClass)
      .then(str => {
        assert.equal(str, '42,foo\n');
        done();
      })
      .catch(done);
  });

  it('headers and multiple data', function (done: MochaDone) {
    const stringifier = csv.stringify({header: true, columns: HEADERS});

    const data1: Data = {numberField: 41, stringField: 'foo1'};
    const data2: Data = {numberField: 42, stringField: 'foo2'};
    const data3: Data = {numberField: 43, stringField: 'foo3'};
    writeToStringifier(stringifier, data1);
    writeToStringifier(stringifier, data2);
    writeToStringifier(stringifier, data3);

    const castedToParentClass: stream.Transform = CsvUtils.castToParentClass(stringifier);
    castedToParentClass.end();

    streamToString(castedToParentClass)
      .then(str => {
        assert.equal(str, 'numberValue,stringValue\n41,foo1\n42,foo2\n43,foo3\n');
        done();
      })
      .catch(done);
  });

  it('no headers and extended data', function (done: MochaDone) {
    const stringifier = csv.stringify({header: false, columns: HEADERS});

    const data: ExtendedData = {numberField: 42, stringField: 'foo', additionalData: 'bar'};
    writeToStringifier(stringifier, data);

    const castedToParentClass: stream.Transform = CsvUtils.castToParentClass(stringifier);
    castedToParentClass.end();

    streamToString(castedToParentClass)
      .then(str => {
        assert.equal(str, '42,foo\n');
        done();
      })
      .catch(done);
  });

  it('no headers and data contains comma', function (done: MochaDone) {
    const stringifier = csv.stringify({header: false, columns: HEADERS});

    const data: Data = {numberField: 0, stringField: 'foo,bar'};
    writeToStringifier(stringifier, data);

    const castedToParentClass: stream.Transform = CsvUtils.castToParentClass(stringifier);
    castedToParentClass.end();

    streamToString(castedToParentClass)
      .then(str => {
        assert.equal(str, '0,\"foo,bar\"\n');
        done();
      })
      .catch(done);
  });

  function streamToString(stream: events.EventEmitter): Promise<string> {
    const chunks: string[] = [];

    stream.on('data', function (chunk) {
      chunks.push(chunk.toString());
    });

    return EventUtils.waitForOnce(stream, 'end', 5000,
      function () {
      }
    )
      .then(() => chunks.join(''));
  }

  function writeToStringifier(stringifier: any, data: Data): void {
    stringifier.write(data);
  }
});