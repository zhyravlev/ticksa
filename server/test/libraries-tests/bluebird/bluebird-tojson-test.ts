/// <reference path="../../../app/typings/tsd.d.ts"/>
const logger = require('../../../app/logger/logger')(__filename);

import * as Bluebird from 'bluebird';
import {assert} from 'chai';

describe('bluebird toJson test', function () {

  it('simple hanging', () => {
    const bluebird: Bluebird<number> = hangingBluebirdPromise<number>();
    assert.equal(JSON.stringify(bluebird), '{"isFulfilled":false,"isRejected":false}');
  });

  it('simple resolved', () => {
    const bluebird: Bluebird<number> = resolvedBluebirdPromise(42);
    assert.equal(JSON.stringify(bluebird), '{"isFulfilled":true,"isRejected":false,"fulfillmentValue":42}');
  });

  it('simple rejected', () => {
    const bluebird: Bluebird<string> = rejectedBluebirdPromise('some');
    assert.equal(JSON.stringify(bluebird), '{"isFulfilled":false,"isRejected":true,"rejectionReason":"some"}');
  });

  it('customized hanging', () => {
    const bluebird: Bluebird<number> = hangingBluebirdPromise<number>();
    bluebird.toJSON = overriddenToJson;
    assert.equal(JSON.stringify(bluebird), '{"hanging":true}');
  });

  it('customized resolved', () => {
    const bluebird: Bluebird<number> = resolvedBluebirdPromise(42);
    bluebird.toJSON = overriddenToJson;
    assert.equal(JSON.stringify(bluebird), '{"resolvedTo":42}');
  });

  it('customized rejected', () => {
    const bluebird: Bluebird<string> = rejectedBluebirdPromise('some');
    bluebird.toJSON = overriddenToJson;
    assert.equal(JSON.stringify(bluebird), '{"rejectedBy":"some"}');
  });

  function hangingBluebirdPromise<T>(): Bluebird<T> {
    return new Bluebird<T>(function() {
    });
  }

  function resolvedBluebirdPromise<T>(value: T): Bluebird<T> {
    return Bluebird.resolve(value);
  }

  function rejectedBluebirdPromise<T>(value: T): Bluebird<T> {
    return Bluebird.reject(value);
  }

  function overriddenToJson(): any {
    const that: Bluebird<any> = this;
    if (that.isFulfilled()) {
      return {
        resolvedTo: that.value()
      }
    }

    if (that.isRejected()) {
      return {
        rejectedBy: that.reason()
      }
    }

    return {
      hanging: true
    };
  }

});