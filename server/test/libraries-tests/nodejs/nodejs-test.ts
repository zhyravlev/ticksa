/// <reference path="../../../app/typings/tsd.d.ts"/>

import {assert} from 'chai';

describe('nodejs test', function () {

  it('array sort test', function () {
    assertSorted([], []);
    assertSorted([1], [1]);
    assertSorted([1, 2], [1, 2]);
    assertSorted([2, 1], [1, 2]);
    assertSorted([1, 2, 3], [1, 2, 3]);
    assertSorted([2, 3, 1], [1, 2, 3]);
    assertSorted([3, 1, 2], [1, 2, 3]);
    assertSorted([1, 3, 2], [1, 2, 3]);

    function assertSorted(initialArray: number[], resultArray: number[]): void {
      assert.deepEqual(initialArray.sort(), resultArray);
    }
  });

  it('array concat test', function () {
    const arr = [];
    const added = arr.concat(1);
    assert.deepEqual(arr, []);
    assert.deepEqual(added, [1]);

    const oneMoreAdded = added.concat(2);
    assert.deepEqual(arr, []);
    assert.deepEqual(added, [1]);
    assert.deepEqual(oneMoreAdded, [1, 2]);

    const arrayToAdd = [3, 4];
    const addedAsArray = oneMoreAdded.concat(arrayToAdd);
    assert.deepEqual(arr, []);
    assert.deepEqual(added, [1]);
    assert.deepEqual(oneMoreAdded, [1, 2]);
    assert.deepEqual(arrayToAdd, [3, 4]);
    assert.deepEqual(addedAsArray, [1, 2, 3, 4]);

    const addedToInitial = arr.concat(arrayToAdd);
    assert.deepEqual(arr, []);
    assert.deepEqual(added, [1]);
    assert.deepEqual(oneMoreAdded, [1, 2]);
    assert.deepEqual(arrayToAdd, [3, 4]);
    assert.deepEqual(addedAsArray, [1, 2, 3, 4]);
    assert.deepEqual(addedToInitial, [3, 4]);
  });

  it('array push test', function () {
    const arr = [];
    assert.deepEqual(arr, []);

    arr.push(1);
    assert.deepEqual(arr, [1]);

    arr.push(2, 3);
    assert.deepEqual(arr, [1, 2, 3]);

    arr.push([4, 5]);
    assert.deepEqual(arr, [1, 2, 3, [4, 5]]);
  });

  it('getting access by index', function () {
    const indexAsProperty = {};
    const indexAsArrayIndex = [];

    indexAsProperty[0] = 'value';
    indexAsArrayIndex.push('value');

    const index = 1;
    assert.isUndefined(indexAsProperty[index]);
    assert.isUndefined(indexAsArrayIndex[index]);

    assert.isTrue(indexAsProperty.hasOwnProperty(0));
    assert.isFalse(indexAsProperty.hasOwnProperty(index));
  });

  it('property with custom name and null value', function () {
    const container = {};

    const customName = 0;
    container[customName] = null;

    assert.isTrue(container.hasOwnProperty(customName))
  });

});