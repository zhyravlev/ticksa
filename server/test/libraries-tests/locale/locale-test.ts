///<reference path="../../../app/typings/tsd.d.ts"/>

var locale = require('locale');
import chai = require('chai');
var assert = chai.assert;

describe('locale test', function () {

  it('check en-US, en, de, hu, sk', function () {
    var acceptLanguage = 'en-US,en;q=0.8,de;q=0.6,hu;q=0.4,sk;q=0.2';

    var lang = new locale.Locales(acceptLanguage).best();
    assert.isObject(lang);
    assert.equal(lang.code, 'en-US');
    assert.equal(lang.country, 'US');
    assert.equal(lang.defaulted, true);
    assert.equal(lang.language, 'en');
    assert.equal(lang.normalized, 'en_US');
  });

  it('check empty', function () {
    var acceptLanguage = '';

    var lang = new locale.Locales(acceptLanguage).best();
    assert.isObject(lang);
    assert.equal(lang.code, 'en_US.UTF-8');
    assert.equal(lang.country, 'US');
    assert.equal(lang.defaulted, true);
    assert.equal(lang.language, 'en');
    assert.equal(lang.normalized, 'en_US');
  });

  it('check undefined', function () {
    var acceptLanguage = undefined;

    var lang = new locale.Locales(acceptLanguage).best();
    assert.isObject(lang);
    assert.equal(lang.code, 'en_US.UTF-8');
    assert.equal(lang.country, 'US');
    assert.equal(lang.defaulted, true);
    assert.equal(lang.language, 'en');
    assert.equal(lang.normalized, 'en_US');
  });

  it('check null', function () {
    var acceptLanguage = null;

    var lang = new locale.Locales(acceptLanguage).best();
    assert.isObject(lang);
    assert.equal(lang.code, 'en_US.UTF-8');
    assert.equal(lang.country, 'US');
    assert.equal(lang.defaulted, true);
    assert.equal(lang.language, 'en');
    assert.equal(lang.normalized, 'en_US');
  });

  it('check bad string', function () {
    var acceptLanguage = 'bad string';

    var lang = new locale.Locales(acceptLanguage).best();
    assert.isObject(lang);
    assert.equal(lang.code, 'bad string');
    assert.equal(lang.country, 'STRING');
    assert.equal(lang.defaulted, true);
    assert.equal(lang.language, 'bad');
    assert.equal(lang.normalized, 'bad_STRING');
  });

  it('check en', function () {
    var acceptLanguage = 'en';

    var lang = new locale.Locales(acceptLanguage).best();
    assert.isObject(lang);
    assert.equal(lang.code, 'en');
    assert.isUndefined(lang.country);
    assert.equal(lang.defaulted, true);
    assert.equal(lang.language, 'en');
    assert.equal(lang.normalized, 'en');
  });

  it('check en-us, en', function () {
    var acceptLanguage = 'en-US,en;q=0.5';

    var lang = new locale.Locales(acceptLanguage).best();
    assert.isObject(lang);
    assert.equal(lang.code, 'en-US');
    assert.equal(lang.country, 'US');
    assert.equal(lang.defaulted, true);
    assert.equal(lang.language, 'en');
    assert.equal(lang.normalized, 'en_US');
  });

  it('check en-gb, en-us, en', function () {
    var acceptLanguage = 'en-GB,en-US;q=0.7,en;q=0.3';

    var lang = new locale.Locales(acceptLanguage).best();
    assert.isObject(lang);
    assert.equal(lang.code, 'en-GB');
    assert.equal(lang.country, 'GB');
    assert.equal(lang.defaulted, true);
    assert.equal(lang.language, 'en');
    assert.equal(lang.normalized, 'en_GB');
  });

  it('check de, en', function () {
    var acceptLanguage = 'de,en;q=0.5';

    var lang = new locale.Locales(acceptLanguage).best();
    assert.isObject(lang);
    assert.equal(lang.code, 'de');
    assert.isUndefined(lang.country);
    assert.equal(lang.defaulted, true);
    assert.equal(lang.language, 'de');
    assert.equal(lang.normalized, 'de');
  });

});