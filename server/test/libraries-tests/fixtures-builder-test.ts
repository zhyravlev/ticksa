/// <reference path="../../app/typings/tsd.d.ts"/>

import * as Fixtures from '../utils-for-tests/fixtures';

describe('FixturesBuilderTest', function () {

  it('Building with DB synchronisation is ok', function (done: MochaDone) {
    const builder = new Fixtures.Builder();
    builder.withSyncDatabase().build().then(function () {
      done();
    }).catch(function (err) {
      done(err);
    });
  });

});