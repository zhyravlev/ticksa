/// <reference path="../../../app/typings/tsd.d.ts"/>

import * as chai from 'chai';

import * as Fixtures from '../../utils-for-tests/fixtures';
import {
  LemonwayCashCallback,
  LemonwayCashIn,
  CashIO
} from '../../../app/models/models';

describe('SequelizeMixinTest', function () {

  before('init fixtures', function (done) {
    Fixtures.Builder.defaultFixtures().build().then(function () {
      done();
    }).catch(function (err) {
      done(err);
    });
  });

  it('HasManyCreateAssociationMixin and HasManyGetAssociationsMixin', function (done: MochaDone) {

    var cashIO: CashIO.Attributes = {
      accountId: 1,  // account with id was created during fixtures
      tickserId: 1,  // tickser with id was created during fixtures
      userCurrencyAmount: 100,
      tickserEmail: 'tickser1@test.com',
      phoneCountryCode: 'de',
      status: CashIO.Status.PENDING,
      type: CashIO.Type.CASH_IN
    };

    CashIO.Model.create(cashIO).then(function (cashIO: CashIO.Instance) {

      var cashIn: LemonwayCashIn.Attributes = {
        initRequest: 'placeholder',
        initResponse: 'placeholder',
        type: LemonwayCashIn.Type.WITHOUT_CARD,
        status: LemonwayCashIn.Status.CREATED,
        cashIoId: cashIO.id,
        wkToken: 'placeholder'
      };

      return LemonwayCashIn.Model.create(cashIn);
    }).then(function (cashIn: LemonwayCashIn.Instance) {
      var cashCallback: LemonwayCashCallback.Attributes = {
        type: LemonwayCashCallback.Type.SUCCESS,
        params: 'params',
        cashInId: cashIn.id,
      };
      return cashIn.createCallback(cashCallback, {save: true}).then(function () {
        return cashIn.getCallbacks();
      });
    }).then(function (callbacks: LemonwayCashCallback.Instance[]) {
      chai.assert.equal(callbacks.length, 1);
      done();
    }).catch(function (err) {
      done(err);
    });
  });
});

