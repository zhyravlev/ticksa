/// <reference path="../../../app/typings/tsd.d.ts"/>

import * as chai from 'chai';
import * as moment from 'moment';

import * as Fixtures from '../../utils-for-tests/fixtures';

import {
  Tickser,
  Voucher,
  Country
} from '../../../app/models/models';

const assert = chai.assert;

// when we used Sequelize 3.23.2 we faced with problem that number field value was returned from DB query as string
// so we should be sure that after querying type is correct
describe('SequelizeTypeofTest', function () {

  before('init fixtures', function (done) {
    const builder = new Fixtures.Builder();
    builder.withSyncDatabase().build()
      .then(function () {
        done();
      })
      .catch(done);
  });

  it('Tickser number fields should have type number', async function () {
    const tickser: Tickser.Attributes = {
      id: 1,
      countryId: 59,
    };
    const created: Tickser.Instance = await Tickser.Model.create(tickser);
    assert.isNumber(created.id);
    assert.isNumber(created.countryId);

    const queried: Tickser.Instance = await Tickser.Model.findById(1);
    assert.isNumber(queried.id);
    assert.isNumber(queried.countryId);
  });

  // Voucher table is chosen as random table to execute tests
  it('Voucher fields should have correct types and values', async function () {
    const now = moment("2016-08-15", "YYYY-MM-DD");
    const yesterday = now.subtract(1, 'days');
    const tomorrow = now.add(1, 'days');

    const voucher: Voucher.Attributes = {
      id: 1,
      code: 'ASD123',
      ticksAmount: 17.07,
      startDate: yesterday.toDate(),
      stopDate: tomorrow.toDate(),
    };

    const created: Voucher.Instance = await Voucher.Model.create(voucher);
    assert.isNumber(created.id);
    assert.isString(created.code);
    assert.isNumber(created.ticksAmount);
    assert.equal(created.id, 1);
    assert.equal(created.code, 'ASD123');
    assert.equal(created.ticksAmount, 17.07);
    assert.equal(created.startDate.getFullYear(), yesterday.toDate().getFullYear());
    assert.equal(created.startDate.getMonth(), yesterday.toDate().getMonth());
    assert.equal(created.startDate.getDay(), yesterday.toDate().getDay());
    assert.equal(created.startDate.getHours(), yesterday.toDate().getHours());
    assert.equal(created.startDate.getMinutes(), yesterday.toDate().getMinutes());
    assert.equal(created.startDate.getSeconds(), yesterday.toDate().getSeconds());
    assert.equal(created.stopDate.getFullYear(), tomorrow.toDate().getFullYear());
    assert.equal(created.stopDate.getMonth(), tomorrow.toDate().getMonth());
    assert.equal(created.stopDate.getDay(), tomorrow.toDate().getDay());
    assert.equal(created.stopDate.getHours(), tomorrow.toDate().getHours());
    assert.equal(created.stopDate.getMinutes(), tomorrow.toDate().getMinutes());
    assert.equal(created.stopDate.getSeconds(), tomorrow.toDate().getSeconds());

    const queried: Voucher.Instance = await Voucher.Model.findById(1);
    assert.isNumber(queried.id);
    assert.isString(queried.code);
    assert.isNumber(queried.ticksAmount);
    assert.equal(queried.id, 1);
    assert.equal(queried.code, 'ASD123');
    assert.equal(queried.ticksAmount, 17.07);
    assert.equal(queried.startDate.getFullYear(), yesterday.toDate().getFullYear());
    assert.equal(queried.startDate.getMonth(), yesterday.toDate().getMonth());
    assert.equal(queried.startDate.getDay(), yesterday.toDate().getDay());
    assert.equal(queried.startDate.getHours(), yesterday.toDate().getHours());
    assert.equal(queried.startDate.getMinutes(), yesterday.toDate().getMinutes());
    assert.equal(queried.startDate.getSeconds(), yesterday.toDate().getSeconds());
    assert.equal(queried.stopDate.getFullYear(), tomorrow.toDate().getFullYear());
    assert.equal(queried.stopDate.getMonth(), tomorrow.toDate().getMonth());
    assert.equal(queried.stopDate.getDay(), tomorrow.toDate().getDay());
    assert.equal(queried.stopDate.getHours(), tomorrow.toDate().getHours());
    assert.equal(queried.stopDate.getMinutes(), tomorrow.toDate().getMinutes());
    assert.equal(queried.stopDate.getSeconds(), tomorrow.toDate().getSeconds());
  });

  // Country table is chosen as random table to execute tests that contains NUMERIC FIELD
  it('Country numeric field should have correct type and value', async function () {
    const NEW_ID = 666; // some new Country

    const country: Country.Attributes = {
      id: NEW_ID,
      VAT: 3.42
    };

    const created: Country.Instance = await Country.Model.create(country);
    assert.isNumber(created.id);
    assert.isNumber(created.VAT);
    assert.equal(created.id, NEW_ID);
    assert.equal(created.VAT, 3.42);

    const queried: Country.Instance = await Country.Model.findById(NEW_ID);
    assert.isNumber(queried.id);
    assert.isNumber(queried.VAT);
    assert.equal(queried.id, NEW_ID);
    assert.equal(queried.VAT, 3.42);
  });
});

