/// <reference path="../../../app/typings/tsd.d.ts"/>

import ES6Error = require('es6-error');

export class SomeError extends ES6Error {

  private _someField: string;

  constructor(msg: string, someField: string) {
    super(msg);
    this._someField = someField;
  }

  get someField(): string {
    return this._someField;
  }
}

export class AnotherError extends SomeError {
  private _anotherField: string;

  constructor(message: string, someField: string, anotherField: string) {
    super(message, someField);
    this._anotherField = anotherField;
  }

  get anotherField(): string {
    return this._anotherField;
  }
}
