/// <reference path="../../../app/typings/tsd.d.ts"/>

import chai = require('chai');

import CustomClasses = require('./custom-classes');
import CustomErrors = require('./custom-errors');
import CustomES6Errors = require('./custom-es6-errors');
import CustomErrorsReusedError = require('./custom-errors-reused-error');

import ES6Error = require('es6-error');
const assert = chai.assert;

// please uncomment if you want to see few stack traces from different Errors implementations
// const logger = require('../../../app/logger/logger')('oop-test.ts');

describe('babel OOP test', function () {

  describe('custom classes inheritance', () => {
    it('custom class instanceOf', () => {
      const someObject = new CustomClasses.SomeClass('foo');
      assert.instanceOf(someObject, CustomClasses.SomeClass);
      assert.equal('foo', someObject.someField);
    });

    it('custom extended class instanceOf', () => {
      const anotherObject = new CustomClasses.AnotherClass('bar', 'baz');
      assert.instanceOf(anotherObject, CustomClasses.SomeClass);
      assert.instanceOf(anotherObject, CustomClasses.AnotherClass);
      assert.equal('bar', anotherObject.someField);
      assert.equal('baz', anotherObject.anotherField);
    });
  });

  describe('custom errors inheritance', () => {
    it('custom error instanceOf', () => {
      const someError = new CustomErrors.SomeError('msg1', 'foo');
      assert.instanceOf(someError, Error);
      assert.notInstanceOf(someError, CustomErrors.SomeError);    // warning! THIS IS VERY BAD!
      assert.equal('msg1', someError.message);
      assert.notEqual('foo', someError.someField);                // warning! THIS IS VERY BAD!
    });

    it('custom extended error instanceOf', () => {
      const anotherError = new CustomErrors.AnotherError('msg2', 'bar', 'baz');
      assert.instanceOf(anotherError, Error);
      assert.notInstanceOf(anotherError, CustomErrors.SomeError);    // warning! THIS IS VERY BAD!
      assert.notInstanceOf(anotherError, CustomErrors.AnotherError); // warning! THIS IS VERY BAD!
      assert.equal('msg2', anotherError.message);
      assert.notEqual('bar', anotherError.someField);                // warning! THIS IS VERY BAD!
      assert.notEqual('baz', anotherError.anotherField);             // warning! THIS IS VERY BAD!
    });
  });

  describe('custom ES6-error inheritance', () => {
    it('ES6-error instanceOf', () => {
      const someError: ES6Error.ES6Error = new ES6Error('msg');
      assert.instanceOf(someError, Error);
      assert.instanceOf(someError, ES6Error);
      assert.equal('msg', someError.message);
      assert.isDefined(someError.stack);
    });

    it('custom ES6-error instanceOf', () => {
      const someError: CustomES6Errors.SomeError = new CustomES6Errors.SomeError('msg', 'foo');
      assert.instanceOf(someError, Error);
      assert.instanceOf(someError, ES6Error);
      assert.instanceOf(someError, CustomES6Errors.SomeError);
      assert.equal('msg', someError.message);
      assert.equal('foo', someError.someField);
    });

    it('it is possible to throw custom ES6-error', () => {
      try {
        throw new CustomES6Errors.SomeError('msg', 'foo');
      } catch (e) {
        assert.instanceOf(e, Error);
        assert.instanceOf(e, ES6Error);
        assert.instanceOf(e, CustomES6Errors.SomeError);
        assert.equal('msg', e.message);
        assert.equal('foo', e.someField);
        assert.isDefined(e.stack);
      }
    });

    it('custom extended ES6-error instanceOf', () => {
      const anotherError: CustomES6Errors.AnotherError = new CustomES6Errors.AnotherError('msg', 'bar', 'baz');
      assert.instanceOf(anotherError, Error);
      assert.instanceOf(anotherError, ES6Error);
      assert.instanceOf(anotherError, CustomES6Errors.SomeError);
      assert.instanceOf(anotherError, CustomES6Errors.AnotherError);
      assert.equal('msg', anotherError.message);
      assert.equal('bar', anotherError.someField);
      assert.equal('baz', anotherError.anotherField);
    });

    it('it is possible to throw custom extended ES6-error', () => {
      try {
        throw new CustomES6Errors.AnotherError('msg', 'bar', 'baz');
      } catch (e) {
        assert.instanceOf(e, Error);
        assert.instanceOf(e, ES6Error);
        assert.instanceOf(e, CustomES6Errors.SomeError);
        assert.instanceOf(e, CustomES6Errors.AnotherError);
        assert.equal('msg', e.message);
        assert.equal('bar', e.someField);
        assert.equal('baz', e.anotherField);
      }
    });

    it('ES6-error contains stack', () => {
      try {
        someFunctionToGetStackFromES6Error();
      } catch (e) {
        assert.instanceOf(e, Error);
        assert.instanceOf(e, CustomES6Errors.AnotherError);
        assert.isTrue(e.toString().indexOf('AnotherError') != -1);
        assert.isDefined(e.stack);
        assert.isTrue(e.stack.indexOf('someFunctionToGetStackFromES6Error') != -1);
        assert.isTrue(e.stack.indexOf('someAnotherFunctionToGetStackFromES6Error') != -1);

        // please uncomment if you want to see that this implementation has pretty stack
        // logger.info('--------------ES6 error-');
        // logger.info(e);
        // logger.info(e.stack);
      }
    });

    function someFunctionToGetStackFromES6Error() {
      someAnotherFunctionToGetStackFromES6Error();
    }

    function someAnotherFunctionToGetStackFromES6Error() {
      throw new CustomES6Errors.AnotherError('msg', 'bar', 'baz');
    }
  });

  describe('custom-error-that-reuses-js-error inheritance', () => {
    it('custom-error-that-reuses-js-error instanceOf', () => {
      const someError: CustomErrorsReusedError.SomeError = new CustomErrorsReusedError.SomeError('msg1', 'foo');
      assert.instanceOf(someError, Error);
      assert.instanceOf(someError, CustomErrorsReusedError.SomeError);
      assert.equal('msg1', someError.message);
      assert.equal('foo', someError.someField);
    });

    it('it is possible to throw custom-error-that-reuses-js-error', () => {
      try {
        throw new CustomErrorsReusedError.SomeError('msg1', 'foo');
      } catch (e) {
        assert.instanceOf(e, Error);
        assert.instanceOf(e, CustomErrorsReusedError.SomeError);
        assert.equal('msg1', e.message);
        assert.equal('foo', e.someField);
      }
    });

    it('custom-error-that-reuses-js-error contains stack', () => {
      try {
        someFunctionToGetStackFromErrorThatReusesError();
      } catch (e) {
        assert.instanceOf(e, Error);
        assert.instanceOf(e, CustomErrorsReusedError.AnotherError);
        assert.isTrue(e.toString().indexOf('AnotherError') != -1);
        assert.isDefined(e.stack);
        assert.isTrue(e.stack.indexOf('someFunctionToGetStackFromErrorThatReusesError') != -1);
        assert.isTrue(e.stack.indexOf('someAnotherFunctionToGetStackFromErrorThatReusesError') != -1);

        // please uncomment if you want to see that this implementation has noise information in stack
        // logger.info('--------------error that reuses error-');
        // logger.info(e);
        // logger.info(e.stack);
      }
    });

    function someFunctionToGetStackFromErrorThatReusesError() {
      someAnotherFunctionToGetStackFromErrorThatReusesError();
    }

    function someAnotherFunctionToGetStackFromErrorThatReusesError() {
      throw new CustomErrorsReusedError.AnotherError('msg', 'foo', 'bar');
    }
  });

  describe('throwings', () => {
    it('JS Error contains stack', () => {
      try {
        someFunctionToGetStackFromJSError();
      } catch (e) {
        assert.instanceOf(e, Error);
        assert.isTrue(e.toString().indexOf('Error') != -1);
        assert.isDefined(e.stack);
        assert.isTrue(e.stack.indexOf('someFunctionToGetStackFromJSError') != -1);
        assert.isTrue(e.stack.indexOf('someAnotherFunctionToGetStackFromJSError') != -1);

        // please uncomment if you want to see initial stack from js Error
        // logger.info('--------------js error-');
        // logger.info(e);
        // logger.info(e.stack);
      }
    });

    function someFunctionToGetStackFromJSError() {
      someAnotherFunctionToGetStackFromJSError();
    }

    function someAnotherFunctionToGetStackFromJSError() {
      throw new Error();
    }

    it('no error stack if you try to throw string', () => {
      try {
        throw 'some string';
      } catch (e) {
        assert.typeOf(e, 'String');
        // assert.instanceOf(e, String); // WARNING! Somehow it doesnt work!!!
        assert.isUndefined(e.message);
        assert.isUndefined(e.stack);
      }
    });

    it('no error stack if you try to throw number', () => {
      try {
        throw 42;
      } catch (e) {
        assert.typeOf(e, 'Number');
        // assert.instanceOf(e, Number); // WARNING! Somehow it doesnt work!!!
        assert.isUndefined(e.message);
        assert.isUndefined(e.stack);
      }
    });

    it('no error stack if you try to throw object', () => {
      try {
        throw {};
      } catch (e) {
        assert.instanceOf(e, Object);
        assert.isUndefined(e.message);
        assert.isUndefined(e.stack);
      }
    });
  });

});
