/// <reference path="../../../app/typings/tsd.d.ts"/>

// such implementation that is described in current module is incorrect for babel (you can not extend Error):
// http://stackoverflow.com/questions/33870684

export class SomeError extends Error {

  private _someField: string;

  constructor(message: string, someField: string) {
    super(message);
    this._someField = someField;
  }

  get someField(): string {
    return this._someField;
  }
}

export class AnotherError extends SomeError {

  private _anotherField: string;

  constructor(message: string, someField: string, anotherField: string) {
    super(message, someField);
    this._anotherField = anotherField;
  }

  get anotherField(): string {
    return this._anotherField;
  }
}
