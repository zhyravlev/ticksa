/// <reference path="../../../app/typings/tsd.d.ts"/>

export class SomeClass {

  private _someField: string;

  constructor(someField: string) {
    this._someField = someField;
  }

  get someField(): string {
    return this._someField;
  }
}

export class AnotherClass extends SomeClass {

  private _anotherField: string;

  constructor(someField: string, anotherField: string) {
    super(someField);
    this._anotherField = anotherField;
  }

  get anotherField(): string {
    return this._anotherField;
  }
}
