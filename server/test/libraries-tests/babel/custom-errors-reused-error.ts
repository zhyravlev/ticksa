/// <reference path="../../../app/typings/tsd.d.ts"/>

import util = require('util');

/*
 *  Description.
 *  This module is just an attempt to understand how to create custom Error hierarchy.
 *  In truth 'es6-errors' is implemented according to the same ideas. But 'es6-errors' is better,
 *   - it is tested by large community
 *   - its errors being printed give stack trace with less noise information, it prints only what you need. (please
 *     check oop-test.ts)
 */

export class ErrorThatReuseError {

  public stack: string;
  public name: string;
  public message: string;

  constructor(message?: string) {
    this.stack = new Error(message || 'Error').stack;
    this.name = this.constructor.name;
    this.message = message || 'Error';
  }
}

util.inherits(ErrorThatReuseError, Error);

export class BaseError extends ErrorThatReuseError {
}

export class SomeError extends BaseError {

  private _someField: string;

  constructor(message: string, someField: string) {
    super(message);
    this._someField = someField;
  }

  get someField(): string {
    return this._someField;
  }
}

export class AnotherError extends SomeError {

  private _anotherField: string;

  constructor(_message: string, someField: string, anotherField: string) {
    super(_message, someField);
    this._anotherField = anotherField;
  }

  get anotherField(): string {
    return this._anotherField;
  }
}
