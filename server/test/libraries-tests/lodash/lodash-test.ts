/// <reference path="../../../app/typings/tsd.d.ts"/>

import mocha = require('mocha');
import chai = require('chai');
import _ = require('lodash');
const assert = chai.assert;

describe('lodash test', function () {

  it('reduce test', function () {
    const array = [
      {count: 5},
      {count: 3},
      {count: 1},
      {count: 9},
      {count: 4},
    ];

    const total = _.reduce(array, function (total: number, item) {
      return total + item.count;
    }, 0);
    assert.equal(22, total);
  });

  it('merge test', function () {

    const query = {
      where: {
        tickserId: 10
      }
    };

    const options = {
      transaction: 't'
    };

    const actual = {
      where: {
        tickserId: 10
      },
      transaction: 't'
    };

    const result = _.merge(query, options);
    assert.deepEqual(actual, result);

    const any = {
      some: 'text'
    };
    assert.deepEqual(any, _.merge(any, null));
  });

  it('keys.first test', function () {
    const nullObject = null;
    assert.equal(_(nullObject).keys().first(), null);

    const objectWithoutProperties = {};
    assert.equal(_(objectWithoutProperties).keys().first(), null);

    const objectWithOneParam = {some: 'value'};
    assert.equal(_(objectWithOneParam).keys().first(), 'some');

    const objectWithOneAddedParam = {};
    objectWithOneAddedParam['someAdded'] = 'value';
    assert.equal(_(objectWithOneAddedParam).keys().first(), 'someAdded');
  });

  it('values test', function () {
    const sortedFields: any = {
      field1: 'value1',
      field2: 'value2',
      field3: 42,
      field4: 15
    };
    assert.deepEqual(_.values(sortedFields), ['value1', 'value2', 42, 15]);

    const unsortedFields: any = {
      field3: 42,
      field2: 'value2',
      field1: 'value1',
      field4: 15
    };
    assert.deepEqual(_.values(unsortedFields), [42, 'value2', 'value1', 15]);
  });

  it('list of filtered keys values', function () {
    const sortedFields: any = {
      field1: 'value1',
      field2: 'value2',
      field3: 42,
      field4: 15
    };
    assert.deepEqual(_(sortedFields).keys().reject(key => key == 'field1').map(key => sortedFields[key]).value(), ['value2', 42, 15]);
    assert.deepEqual(_(sortedFields).keys().reject(key => key == 'field2').map(key => sortedFields[key]).value(), ['value1', 42, 15]);
    assert.deepEqual(_(sortedFields).keys().reject(key => key == 'field3').map(key => sortedFields[key]).value(), ['value1', 'value2', 15]);
    assert.deepEqual(_(sortedFields).keys().reject(key => key == 'field4').map(key => sortedFields[key]).value(), ['value1', 'value2', 42]);

    const unsortedFields: any = {
      field3: 42,
      field2: 'value2',
      field1: 'value1',
      field4: 15
    };
    assert.deepEqual(_(unsortedFields).keys().reject(key => key == 'field1').map(key => sortedFields[key]).value(), [42, 'value2', 15]);
    assert.deepEqual(_(unsortedFields).keys().reject(key => key == 'field2').map(key => sortedFields[key]).value(), [42, 'value1', 15]);
    assert.deepEqual(_(unsortedFields).keys().reject(key => key == 'field3').map(key => sortedFields[key]).value(), ['value2', 'value1', 15]);
    assert.deepEqual(_(unsortedFields).keys().reject(key => key == 'field4').map(key => sortedFields[key]).value(), [42, 'value2', 'value1']);
  });

  it('isEmpty test', function () {
    assert.isTrue(_.isEmpty(undefined), 'undefined');
    assert.isTrue(_.isEmpty(null), 'null');
    assert.isTrue(_.isEmpty([]), 'empty array');
    assert.isTrue(_.isEmpty({}), 'empty object');
    assert.isFalse(_.isEmpty([42]), 'not empty array');
    assert.isFalse(_.isEmpty({some: 'foo'}), 'not emnpty object');
    assert.isTrue(_.isEmpty(''), 'string: ""');
    assert.isFalse(_.isEmpty('foo'), 'string: "foo"');

    // special cases
    assert.isTrue(_.isEmpty(0), 'number: 0');
    assert.isTrue(_.isEmpty(1), 'number: 1');
    assert.isTrue(_.isEmpty(-1), 'number: -1');
    assert.isTrue(_.isEmpty(100), 'number: 100');
    assert.isTrue(_.isEmpty(-100), 'number: -100');
    assert.isTrue(_.isEmpty(true), 'boolean: true');
    assert.isTrue(_.isEmpty(false), 'boolean: false');
  });

  it('includes test', function () {
    assert.isFalse(_.includes(null, null));
    assert.isFalse(_.includes(null, {}));
    assert.isFalse(_.includes([], {}));
    assert.isFalse(_.includes([], null));
    assert.isFalse(_.includes(null, 'hi'));
    assert.isTrue(_.includes([1], 1));
    assert.isTrue(_.includes([1, 2], 1));
  });

  it('pluck test', function () {
    assert.deepEqual(_.pluck(null, null), []);
    assert.deepEqual(_.pluck(null, 'some'), []);
    assert.deepEqual(_.pluck([], null), []);
    assert.deepEqual(_.pluck([], 'some'), []);
  });

  it('forEach test. empty collection - no loop', function (done: MochaDone) {
    loops(undefined);
    loops(null);
    loops([]);
    done();

    function loops(collection: any): void {
      const sideEffect = [];
      _.forEach(collection, () => sideEffect.push(Math.random()));
      assert.equal(sideEffect.length, 0);

      _.forEach(collection, () => {
        throw new Error('Oops!')
      });
    }
  });

  it('findWhere test. not flat object', function () {
    const element1 = {
      some: {
        foo: 'foo1',
        bar: 'bar1'
      },
      baz: 'baz1'
    };
    const element2 = {
      some: {
        foo: 'foo2',
        bar: 'bar2'
      },
      baz: 'baz2'
    };
    const collection = [element1, element2];

    assert.deepEqual(_.findWhere(collection, {some: {foo: 'foo1'}}), element1);
    assert.deepEqual(_.findWhere(collection, {some: {foo: 'foo1', bar: 'bar1'}}), element1);
    assert.deepEqual(_.findWhere(collection, {some: {bar: 'bar2'}}), element2);
    assert.deepEqual(_.findWhere(collection, {some: {bar: 'bar2'}, baz: 'baz2'}), element2);
    assert.isUndefined(_.findWhere(collection, {some: {foo: ''}}));
    assert.isUndefined(_.findWhere(collection, {some: {foo: 'foo1', bar: ''}}));
    assert.isUndefined(_.findWhere(collection, {some: {foo: 'foo1', bar: 'bar1'}, baz: ''}));
  });
});