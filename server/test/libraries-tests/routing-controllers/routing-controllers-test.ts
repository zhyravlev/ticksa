/// <reference path="../../../app/typings/tsd.d.ts"/>
const logger = require('../../../app/logger/logger')(__filename);

import {assert} from 'chai';
import * as supertest from 'supertest-as-promised';
import * as TestApp from './test-app';

// can be switched on after https://ticksa.atlassian.net/browse/APP-923
describe.skip('routing controllers test', () => {

  let testApplication;
  let stopServer;

  before('start test application', () => {
    const start = TestApp.startApplication();
    testApplication = start.app;
    stopServer = start.stopServer;
  });

  after('stop test application', () => {
    stopServer();
  });

  describe(TestApp.CONTROLLERS.controller, () => {

    function url(location): string {
      return TestApp.CONTROLLERS.controller + location;
    }

    it(TestApp.TEST_CONTROLLER_ROUTES.string, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.string))
        .expect(200, 'This action returns string');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.stringEmpty, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.stringEmpty))
        .expect(200, '');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.null, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.null))
        .expect(204, '');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.undefined, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.undefined))
        .expect(404, 'Cannot GET /controller/undefined\n');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.number0, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.number0))
        .expect(200, '0');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.number42, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.number42))
        .expect(200, '42');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.booleanTrue, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.booleanTrue))
        .expect(200, 'true');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.booleanFalse, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.booleanFalse))
        .expect(200, 'false');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.object, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.object))
        .expect(200, '[object Object]');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.bluebird, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.bluebird))
        .expect(200, '[object Object]');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.promise, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.promise))
        .expect(200, 'some');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.array, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.array))
        .expect(200, '1,2,3');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.exception, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.exception))
        .expect(function(res) {
          assert.equal(res.status, 500);
          assert.isTrue(res.serverError);
          assert.isTrue(res.text.startsWith('Error: some exception\n'));
        });
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.asyncException, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.asyncException))
        .expect(function(res) {
          assert.equal(res.status, 500);
          assert.isTrue(res.serverError);
          assert.isTrue(res.text.startsWith('Error: some exception\n'));
        });
    });

  });

  describe(TestApp.CONTROLLERS.jsonController, () => {

    function url(location): string {
      return TestApp.CONTROLLERS.jsonController + location;
    }

    it(TestApp.TEST_CONTROLLER_ROUTES.string, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.string))
        .expect(200, '"This action returns string"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.stringEmpty, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.stringEmpty))
        .expect(200, '""');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.null, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.null))
        .expect(204, '');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.undefined, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.undefined))
        .expect(404, 'Cannot GET /json-controller/undefined\n');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.number0, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.number0))
        .expect(200, '0');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.number42, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.number42))
        .expect(200, '42');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.booleanTrue, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.booleanTrue))
        .expect(200, 'true');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.booleanFalse, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.booleanFalse))
        .expect(200, 'false');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.object, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.object))
        .expect(200, '' +
          '{' +
          '"dataString":"str",' +
          '"dataNumber0":0,' +
          '"dataNumber42":42,' +
          '"dataBooleanTrue":true,' +
          '"dataBooleanFalse":false,' +
          '"dataNull":null,' +
          '"dataObject":{"someString":"str","someNumber":42}' +
          '}'
        );
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.bluebird, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.bluebird))
        .expect(200, '{"_bitField":33554432,"_rejectionHandler0":"some"}');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.promise, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.promise))
        .expect(200, '"some"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.array, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.array))
        .expect(200, '[1,2,3]');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.exception, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.exception))
        .expect(function(res) {
          assert.equal(res.status, 500);
          assert.isTrue(res.serverError);
          assert.isTrue(res.text.startsWith('{"name":"Error","message":"some exception","stack":"Error: some exception'));
          assert.equal(res.body.name, 'Error');
          assert.equal(res.body.message, 'some exception');
        });
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.asyncException, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.asyncException))
        .expect(function(res) {
          assert.equal(res.status, 500);
          assert.isTrue(res.serverError);
          assert.isTrue(res.text.startsWith('{"name":"Error","message":"some exception","stack":"Error: some exception'));
          assert.equal(res.body.name, 'Error');
          assert.equal(res.body.message, 'some exception');
        });
    });

  });

  describe(TestApp.CONTROLLERS.specialsController, () => {

    function url(location): string {
      return TestApp.CONTROLLERS.specialsController + location;
    }

    it('string with custom code', async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_SPECIALS_CONTROLLER_ROUTES.stringWithCustomCode))
        .expect(222, 'This action returns string but with custom CODE: 222');
    });

    it('without return', async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_SPECIALS_CONTROLLER_ROUTES.withoutReturn))
        .expect(404, 'Cannot GET /specials-controller/without-return\n');
    });

    it('without return but with decorator', async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_SPECIALS_CONTROLLER_ROUTES.withoutReturnButWitDecorator))
        .expect(200, '');
    });

  });

  describe(TestApp.CONTROLLERS.jsonControllerWithCustomResponse, () => {

    function url(location): string {
      return TestApp.CONTROLLERS.jsonControllerWithCustomResponse + location;
    }

    it('custom status and string', async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_CONTROLLER_WITH_CUSTOM_RESPONSE_ROUTES.getCustomStatusAndString))
        .expect(222, 'Some returned string');
    });

    it('redirect', async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_CONTROLLER_WITH_CUSTOM_RESPONSE_ROUTES.redirect))
        .expect(302)
        .expect('Location', '/redirect');
    });

  });

  describe(TestApp.CONTROLLERS.interceptedController, () => {

    function url(location): string {
      return TestApp.CONTROLLERS.interceptedController + location;
    }

    it(TestApp.TEST_CONTROLLER_ROUTES.string, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.string))
        .expect(200, 'string');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.stringEmpty, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.stringEmpty))
        .expect(200, 'string');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.null, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.null))
        .expect(200, 'object');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.undefined, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.undefined))
        .expect(200, 'undefined');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.number0, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.number0))
        .expect(200, 'number');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.number42, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.number42))
        .expect(200, 'number');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.booleanTrue, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.booleanTrue))
        .expect(200, 'boolean');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.booleanFalse, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.booleanFalse))
        .expect(200, 'boolean');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.object, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.object))
        .expect(200, 'object');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.bluebird, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.bluebird))
        .expect(200, 'object');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.promise, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.promise))
        .expect(200, 'string');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.array, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.array))
        .expect(200, 'object');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.exception, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.exception))
        .expect(function(res) {
          assert.equal(res.status, 500);
          assert.isTrue(res.serverError);
          assert.isTrue(res.text.startsWith('Error: some exception\n'));
        });
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.asyncException, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.asyncException))
        .expect(function(res) {
          assert.equal(res.status, 500);
          assert.isTrue(res.serverError);
          assert.isTrue(res.text.startsWith('Error: some exception\n'));
        });
    });

  });

  describe(TestApp.CONTROLLERS.jsonInterceptedController, () => {

    function url(location): string {
      return TestApp.CONTROLLERS.jsonInterceptedController + location;
    }

    it(TestApp.TEST_CONTROLLER_ROUTES.string, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.string))
        .expect(200, '"string"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.stringEmpty, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.stringEmpty))
        .expect(200, '"string"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.null, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.null))
        .expect(200, '"object"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.undefined, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.undefined))
        .expect(200, '"undefined"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.number0, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.number0))
        .expect(200, '"number"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.number42, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.number42))
        .expect(200, '"number"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.booleanTrue, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.booleanTrue))
        .expect(200, '"boolean"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.booleanFalse, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.booleanFalse))
        .expect(200, '"boolean"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.object, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.object))
        .expect(200, '"object"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.bluebird, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.bluebird))
        .expect(200, '"object"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.promise, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.promise))
        .expect(200, '"string"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.array, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.array))
        .expect(200, '"object"');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.exception, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.exception))
        .expect(function(res) {
          assert.equal(res.status, 500);
          assert.isTrue(res.serverError);
          assert.isTrue(res.text.startsWith('{"name":"Error","message":"some exception","stack":"Error: some exception'));
          assert.equal(res.body.name, 'Error');
          assert.equal(res.body.message, 'some exception');
        });
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.asyncException, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.asyncException))
        .expect(function(res) {
          assert.equal(res.status, 500);
          assert.isTrue(res.serverError);
          assert.isTrue(res.text.startsWith('{"name":"Error","message":"some exception","stack":"Error: some exception'));
          assert.equal(res.body.name, 'Error');
          assert.equal(res.body.message, 'some exception');
        });
    });

  });

  describe(TestApp.CONTROLLERS.bluebirdInterceptedController, () => {

    function url(location): string {
      return TestApp.CONTROLLERS.bluebirdInterceptedController + location;
    }

    it(TestApp.TEST_CONTROLLER_ROUTES.string, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.string))
        .expect(200, 'not Bluebird');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.bluebird, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.bluebird))
        .expect(200, 'content instanceof Bluebird');
    });

    it(TestApp.TEST_CONTROLLER_ROUTES.promise, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_CONTROLLER_ROUTES.promise))
        .expect(200, 'not Bluebird');
    });

  });

  describe(TestApp.CONTROLLERS.jsonOverriddenBluebirdController, () => {

    function url(location): string {
      return TestApp.CONTROLLERS.jsonOverriddenBluebirdController + location;
    }

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.hanging, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.hanging))
        .expect(200, '{"hanging":true}');
    });

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedNumber, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedNumber))
        .expect(200, '{"resolvedTo":42}');
    });

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedObject, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedObject))
        .expect(200, '{"resolvedTo":{"foo":"bar"}}');
    });

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedString, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedString))
        .expect(200, '{"rejectedBy":"some"}');
    });

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedObject, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedObject))
        .expect(200, '{"rejectedBy":{"bar":"baz"}}');
    });

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.notBluebird, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.notBluebird))
        .expect(200, '"not bluebird"');
    });

  });

  describe(TestApp.CONTROLLERS.jsonBluebirdPackedToPromiseController, () => {

    function url(location): string {
      return TestApp.CONTROLLERS.jsonBluebirdPackedToPromiseController + location;
    }

    it.skip(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.hanging, async function () {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.hanging))
        .expect(200, '{"hanging":true}');
    });

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedNumber, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedNumber))
        .expect(200, '42');
    });

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedObject, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedObject))
        .expect(200, '{"foo":"bar"}');
    });

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedString, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedString))
        .expect(500, '"some"');
    });

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedObject, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedObject))
        .expect(500, '{"bar":"baz"}');
    });

    it(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.notBluebird, async() => {
      await supertest(testApplication)
        .get(url(TestApp.TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.notBluebird))
        .expect(200, '"not bluebird"');
    });

  });
});
