/// <reference path='../../../app/typings/tsd.d.ts'/>

const logger = require('../../../app/logger/logger')(__filename);

const ExpressRemoveRoute = require('express-remove-route');

import 'reflect-metadata';
import * as _ from 'lodash';
import * as Ticksa from 'ticksa';
import * as http from 'http';

import {
  createExpressServer,
  defaultMetadataArgsStorage,
  Controller,
  JsonController,
  Get,
  Res,
  HttpCode,
  UndefinedResultCode,
  Interceptor,
  InterceptorInterface,
  UseInterceptor
} from 'routing-controllers';

import {ControllerMetadataArgs} from 'routing-controllers/metadata/args/ControllerMetadataArgs';
import {InterceptorMetadataArgs} from 'routing-controllers/metadata/args/InterceptorMetadataArgs';

export const CONTROLLERS = {
  controller: '/controller',
  jsonController: '/json-controller',
  specialsController: '/specials-controller',
  jsonControllerWithCustomResponse: '/json-controller-with-custom-response',
  interceptedController: '/intercepted-controller',
  jsonInterceptedController: '/json-intercepted-controller',
  bluebirdInterceptedController: '/bluebird-intercepted-controller',
  jsonOverriddenBluebirdController: '/json-overridden-bluebird-controller',
  jsonBluebirdPackedToPromiseController: '/json-bluebird-packed-to-promise-controller'
};

export const TEST_CONTROLLER_ROUTES = {
  string: '/string',
  stringEmpty: '/string-empty',
  null: '/null',
  undefined: '/undefined',
  number0: '/number-0',
  number42: '/number-42',
  booleanTrue: '/boolean-true',
  booleanFalse: '/boolean-false',
  object: '/object',
  bluebird: '/bluebird',
  promise: '/promise',
  array: '/array',
  exception: '/exception',
  asyncException: '/async-exception'
};

export const TEST_SPECIALS_CONTROLLER_ROUTES = {
  withoutReturn: '/without-return',
  withoutReturnButWitDecorator: '/without-return-but-with-decorator',
  stringWithCustomCode: '/string-with-custom-code'
};

export const TEST_JSON_CONTROLLER_WITH_CUSTOM_RESPONSE_ROUTES = {
  redirect: '/redirect',
  getCustomStatusAndString: '/custom-status-and-string'
};

export const TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES = {
  resolvedNumber: '/resolved-number',
  resolvedObject: '/resolved-object',
  rejectedString: '/rejected-string',
  rejectedObject: '/rejected-object',
  hanging: '/hanging',
  notBluebird: '/not-bluebird',
};

const TEST_CONTROLLER_RESULTS = {
  string: function () {
    return 'This action returns string';
  },
  stringEmpty: function () {
    return '';
  },
  null: function () {
    return null;
  },
  undefined: function () {
    return undefined;
  },
  number0: function () {
    return 0;
  },
  number42: function () {
    return 42;
  },
  booleanTrue: function () {
    return true;
  },
  booleanFalse: function () {
    return false;
  },
  object: function () {
    return {
      dataString: 'str',
      dataNumber0: 0,
      dataNumber42: 42,
      dataBooleanTrue: true,
      dataBooleanFalse: false,
      dataNull: null,
      dataUndefined: undefined,
      dataObject: {
        someString: 'str',
        someNumber: 42
      }
    };
  },
  bluebird: function () {
    const Bluebird = require('bluebird');
    return new Bluebird((resolve) => {
      resolve('some');
    });
  },
  promise: function () {
    return new Promise((resolve) => {
      resolve('some');
    });
  },
  array: function () {
    return [1, 2, 3];
  },
  exception: function () {
    throw new Error('some exception');
  }
};

export function startApplication() {

  @Controller(CONTROLLERS.controller)
  class TestController {

    @Get(TEST_CONTROLLER_ROUTES.string)
    getString() {
      return TEST_CONTROLLER_RESULTS.string();
    }

    @Get(TEST_CONTROLLER_ROUTES.stringEmpty)
    getStringEmpty() {
      return TEST_CONTROLLER_RESULTS.stringEmpty();
    }

    @Get(TEST_CONTROLLER_ROUTES.null)
    getNull() {
      return TEST_CONTROLLER_RESULTS.null();
    }

    @Get(TEST_CONTROLLER_ROUTES.undefined)
    getUndefined() {
      return TEST_CONTROLLER_RESULTS.undefined();
    }

    @Get(TEST_CONTROLLER_ROUTES.number0)
    getNumber0() {
      return TEST_CONTROLLER_RESULTS.number0();
    }

    @Get(TEST_CONTROLLER_ROUTES.number42)
    getNumber42() {
      return TEST_CONTROLLER_RESULTS.number42();
    }

    @Get(TEST_CONTROLLER_ROUTES.booleanTrue)
    getBooleanTrue() {
      return TEST_CONTROLLER_RESULTS.booleanTrue();
    }

    @Get(TEST_CONTROLLER_ROUTES.booleanFalse)
    getBooleanFalse() {
      return TEST_CONTROLLER_RESULTS.booleanFalse();
    }

    @Get(TEST_CONTROLLER_ROUTES.object)
    getObject() {
      return TEST_CONTROLLER_RESULTS.object();
    }

    @Get(TEST_CONTROLLER_ROUTES.bluebird)
    getBluebird() {
      return TEST_CONTROLLER_RESULTS.bluebird();
    }

    @Get(TEST_CONTROLLER_ROUTES.promise)
    getPromise() {
      return TEST_CONTROLLER_RESULTS.promise();
    }

    @Get(TEST_CONTROLLER_ROUTES.array)
    getArray() {
      return TEST_CONTROLLER_RESULTS.array();
    }

    @Get(TEST_CONTROLLER_ROUTES.exception)
    getException() {
      return TEST_CONTROLLER_RESULTS.exception();
    }

    @Get(TEST_CONTROLLER_ROUTES.asyncException)
    async getAsyncException() {
      return TEST_CONTROLLER_RESULTS.exception();
    }

  }

  @JsonController(CONTROLLERS.jsonController)
  class TestJsonController {

    @Get(TEST_CONTROLLER_ROUTES.string)
    getString() {
      return TEST_CONTROLLER_RESULTS.string();
    }

    @Get(TEST_CONTROLLER_ROUTES.stringEmpty)
    getStringEmpty() {
      return TEST_CONTROLLER_RESULTS.stringEmpty();
    }

    @Get(TEST_CONTROLLER_ROUTES.null)
    getNull() {
      return TEST_CONTROLLER_RESULTS.null();
    }

    @Get(TEST_CONTROLLER_ROUTES.undefined)
    getUndefined() {
      return TEST_CONTROLLER_RESULTS.undefined();
    }

    @Get(TEST_CONTROLLER_ROUTES.number0)
    getNumber0() {
      return TEST_CONTROLLER_RESULTS.number0();
    }

    @Get(TEST_CONTROLLER_ROUTES.number42)
    getNumber42() {
      return TEST_CONTROLLER_RESULTS.number42();
    }

    @Get(TEST_CONTROLLER_ROUTES.booleanTrue)
    getBooleanTrue() {
      return TEST_CONTROLLER_RESULTS.booleanTrue();
    }

    @Get(TEST_CONTROLLER_ROUTES.booleanFalse)
    getBooleanFalse() {
      return TEST_CONTROLLER_RESULTS.booleanFalse();
    }

    @Get(TEST_CONTROLLER_ROUTES.object)
    getObject() {
      return TEST_CONTROLLER_RESULTS.object();
    }

    @Get(TEST_CONTROLLER_ROUTES.bluebird)
    getBluebird() {
      return TEST_CONTROLLER_RESULTS.bluebird();
    }

    @Get(TEST_CONTROLLER_ROUTES.promise)
    getPromise() {
      return TEST_CONTROLLER_RESULTS.promise();
    }

    @Get(TEST_CONTROLLER_ROUTES.array)
    getArray() {
      return TEST_CONTROLLER_RESULTS.array();
    }

    @Get(TEST_CONTROLLER_ROUTES.exception)
    getException() {
      return TEST_CONTROLLER_RESULTS.exception();
    }

    @Get(TEST_CONTROLLER_ROUTES.asyncException)
    async getAsyncException() {
      return TEST_CONTROLLER_RESULTS.exception();
    }
  }

  @Controller(CONTROLLERS.specialsController)
  class TestSpecialsController {

    @Get(TEST_SPECIALS_CONTROLLER_ROUTES.withoutReturn)
    getWithoutReturn() {
    }

    @Get(TEST_SPECIALS_CONTROLLER_ROUTES.withoutReturnButWitDecorator)
    @UndefinedResultCode(200)
    getWithoutReturnButWitDecorator() {
    }

    @Get(TEST_SPECIALS_CONTROLLER_ROUTES.stringWithCustomCode)
    @HttpCode(222)
    getStringWithCustomCode() {
      return 'This action returns string but with custom CODE: 222';
    }

  }

  @JsonController(CONTROLLERS.jsonControllerWithCustomResponse)
  class TestJsonControllerWithCustomResponse {

    @Get(TEST_JSON_CONTROLLER_WITH_CUSTOM_RESPONSE_ROUTES.redirect)
    getRedirect(@Res() res: Ticksa.Response) {
      res.redirect(302, '/redirect');
    }

    @Get(TEST_JSON_CONTROLLER_WITH_CUSTOM_RESPONSE_ROUTES.getCustomStatusAndString)
    getCustomStatusAndString(@Res() res: Ticksa.Response) {
      res.status(222).send('Some returned string');
    }

  }

  @Interceptor()
  class TestResultTypeInterceptor implements InterceptorInterface {

    intercept(request: any, response: any, content: any) {
      return typeof content;
    }

  }

  @UseInterceptor(TestResultTypeInterceptor)
  @Controller(CONTROLLERS.interceptedController)
  class TestInterceptedController {

    @Get(TEST_CONTROLLER_ROUTES.string)
    getString() {
      return TEST_CONTROLLER_RESULTS.string();
    }

    @Get(TEST_CONTROLLER_ROUTES.stringEmpty)
    getStringEmpty() {
      return TEST_CONTROLLER_RESULTS.stringEmpty();
    }

    @Get(TEST_CONTROLLER_ROUTES.null)
    getNull() {
      return TEST_CONTROLLER_RESULTS.null();
    }

    @Get(TEST_CONTROLLER_ROUTES.undefined)
    getUndefined() {
      return TEST_CONTROLLER_RESULTS.undefined();
    }

    @Get(TEST_CONTROLLER_ROUTES.number0)
    getNumber0() {
      return TEST_CONTROLLER_RESULTS.number0();
    }

    @Get(TEST_CONTROLLER_ROUTES.number42)
    getNumber42() {
      return TEST_CONTROLLER_RESULTS.number42();
    }

    @Get(TEST_CONTROLLER_ROUTES.booleanTrue)
    getBooleanTrue() {
      return TEST_CONTROLLER_RESULTS.booleanTrue();
    }

    @Get(TEST_CONTROLLER_ROUTES.booleanFalse)
    getBooleanFalse() {
      return TEST_CONTROLLER_RESULTS.booleanFalse();
    }

    @Get(TEST_CONTROLLER_ROUTES.object)
    getObject() {
      return TEST_CONTROLLER_RESULTS.object();
    }

    @Get(TEST_CONTROLLER_ROUTES.bluebird)
    getBluebird() {
      return TEST_CONTROLLER_RESULTS.bluebird();
    }

    @Get(TEST_CONTROLLER_ROUTES.promise)
    getPromise() {
      return TEST_CONTROLLER_RESULTS.promise();
    }

    @Get(TEST_CONTROLLER_ROUTES.array)
    getArray() {
      return TEST_CONTROLLER_RESULTS.array();
    }

    @Get(TEST_CONTROLLER_ROUTES.exception)
    getException() {
      return TEST_CONTROLLER_RESULTS.exception();
    }

    @Get(TEST_CONTROLLER_ROUTES.asyncException)
    async getAsyncException() {
      return TEST_CONTROLLER_RESULTS.exception();
    }
  }

  @UseInterceptor(TestResultTypeInterceptor)
  @JsonController(CONTROLLERS.jsonInterceptedController)
  class TestJsonInterceptedController {

    @Get(TEST_CONTROLLER_ROUTES.string)
    getString() {
      return TEST_CONTROLLER_RESULTS.string();
    }

    @Get(TEST_CONTROLLER_ROUTES.stringEmpty)
    getStringEmpty() {
      return TEST_CONTROLLER_RESULTS.stringEmpty();
    }

    @Get(TEST_CONTROLLER_ROUTES.null)
    getNull() {
      return TEST_CONTROLLER_RESULTS.null();
    }

    @Get(TEST_CONTROLLER_ROUTES.undefined)
    getUndefined() {
      return TEST_CONTROLLER_RESULTS.undefined();
    }

    @Get(TEST_CONTROLLER_ROUTES.number0)
    getNumber0() {
      return TEST_CONTROLLER_RESULTS.number0();
    }

    @Get(TEST_CONTROLLER_ROUTES.number42)
    getNumber42() {
      return TEST_CONTROLLER_RESULTS.number42();
    }

    @Get(TEST_CONTROLLER_ROUTES.booleanTrue)
    getBooleanTrue() {
      return TEST_CONTROLLER_RESULTS.booleanTrue();
    }

    @Get(TEST_CONTROLLER_ROUTES.booleanFalse)
    getBooleanFalse() {
      return TEST_CONTROLLER_RESULTS.booleanFalse();
    }

    @Get(TEST_CONTROLLER_ROUTES.object)
    getObject() {
      return TEST_CONTROLLER_RESULTS.object();
    }

    @Get(TEST_CONTROLLER_ROUTES.bluebird)
    getBluebird() {
      return TEST_CONTROLLER_RESULTS.bluebird();
    }

    @Get(TEST_CONTROLLER_ROUTES.promise)
    getPromise() {
      return TEST_CONTROLLER_RESULTS.promise();
    }

    @Get(TEST_CONTROLLER_ROUTES.array)
    getArray() {
      return TEST_CONTROLLER_RESULTS.array();
    }

    @Get(TEST_CONTROLLER_ROUTES.exception)
    getException() {
      return TEST_CONTROLLER_RESULTS.exception();
    }

    @Get(TEST_CONTROLLER_ROUTES.asyncException)
    async getAsyncException() {
      return TEST_CONTROLLER_RESULTS.exception();
    }
  }

  @Interceptor()
  class TestBluebirdCheckerInterceptor implements InterceptorInterface {

    intercept(request: any, response: any, content: any) {
      const Bluebird = require('bluebird');

      if (content instanceof Bluebird) {
        return 'content instanceof Bluebird';
      }

      return 'not Bluebird';
    }

  }

  @UseInterceptor(TestBluebirdCheckerInterceptor)
  @Controller(CONTROLLERS.bluebirdInterceptedController)
  class TestBluebirdInterceptedController {

    @Get(TEST_CONTROLLER_ROUTES.string)
    getString() {
      return TEST_CONTROLLER_RESULTS.string();
    }

    @Get(TEST_CONTROLLER_ROUTES.stringEmpty)
    getStringEmpty() {
      return TEST_CONTROLLER_RESULTS.stringEmpty();
    }

    @Get(TEST_CONTROLLER_ROUTES.null)
    getNull() {
      return TEST_CONTROLLER_RESULTS.null();
    }

    @Get(TEST_CONTROLLER_ROUTES.undefined)
    getUndefined() {
      return TEST_CONTROLLER_RESULTS.undefined();
    }

    @Get(TEST_CONTROLLER_ROUTES.number0)
    getNumber0() {
      return TEST_CONTROLLER_RESULTS.number0();
    }

    @Get(TEST_CONTROLLER_ROUTES.number42)
    getNumber42() {
      return TEST_CONTROLLER_RESULTS.number42();
    }

    @Get(TEST_CONTROLLER_ROUTES.booleanTrue)
    getBooleanTrue() {
      return TEST_CONTROLLER_RESULTS.booleanTrue();
    }

    @Get(TEST_CONTROLLER_ROUTES.booleanFalse)
    getBooleanFalse() {
      return TEST_CONTROLLER_RESULTS.booleanFalse();
    }

    @Get(TEST_CONTROLLER_ROUTES.object)
    getObject() {
      return TEST_CONTROLLER_RESULTS.object();
    }

    @Get(TEST_CONTROLLER_ROUTES.bluebird)
    getBluebird() {
      return TEST_CONTROLLER_RESULTS.bluebird();
    }

    @Get(TEST_CONTROLLER_ROUTES.promise)
    getPromise() {
      return TEST_CONTROLLER_RESULTS.promise();
    }

    @Get(TEST_CONTROLLER_ROUTES.array)
    getArray() {
      return TEST_CONTROLLER_RESULTS.array();
    }

    @Get(TEST_CONTROLLER_ROUTES.exception)
    getException() {
      return TEST_CONTROLLER_RESULTS.exception();
    }

    @Get(TEST_CONTROLLER_ROUTES.asyncException)
    async getAsyncException() {
      return TEST_CONTROLLER_RESULTS.exception();
    }
  }

  @Interceptor()
  class TestOverrideBluebirdResultInterceptor implements InterceptorInterface {

    intercept(request: any, response: any, content: any) {
      const Bluebird = require('bluebird');

      if (content instanceof Bluebird) {
        return TestOverrideBluebirdResultInterceptor.getResultOfBluebirdPromise(content);
      }

      return content;
    }

    private static getResultOfBluebirdPromise(bluebird): any {
      if (bluebird.isFulfilled()) {
        return {
          resolvedTo: bluebird.value()
        }
      }

      if (bluebird.isRejected()) {
        return {
          rejectedBy: bluebird.reason()
        }
      }

      return {
        hanging: true
      };
    }

  }

  @UseInterceptor(TestOverrideBluebirdResultInterceptor)
  @JsonController(CONTROLLERS.jsonOverriddenBluebirdController)
  class TestJsonOverriddenBluebirdController {

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.hanging)
    getHanging() {
      const Bluebird = require('bluebird');
      return new Bluebird(function () {
      });
    }

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedNumber)
    getResolvedNumber() {
      const Bluebird = require('bluebird');
      return Bluebird.resolve(42);
    }

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedObject)
    getResolvedObject() {
      const Bluebird = require('bluebird');
      return Bluebird.resolve({foo: 'bar'});
    }

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedString)
    getRejectedString() {
      const Bluebird = require('bluebird');
      return Bluebird.reject('some');
    }

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedObject)
    getRejectedObject() {
      const Bluebird = require('bluebird');
      return Bluebird.reject({bar: 'baz'});
    }

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.notBluebird)
    getNotBluebird() {
      return 'not bluebird';
    }

  }

  @Interceptor()
  class TestBluebirdPackedToPromiseInterceptor implements InterceptorInterface {

    intercept(request: any, response: any, content: any) {
      const Bluebird = require('bluebird');

      if (content instanceof Bluebird) {
        return new Promise((resolve, reject) => {
          content
            .then(data => resolve(data))
            .catch(err => reject(err));
        });
      }

      return content;
    }

  }

  @UseInterceptor(TestBluebirdPackedToPromiseInterceptor)
  @JsonController(CONTROLLERS.jsonBluebirdPackedToPromiseController)
  class TestJsonBluebirdPackedToPromiseController {

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.hanging)
    getHanging() {
      const Bluebird = require('bluebird');
      return new Bluebird(function () {
      });
    }

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedNumber)
    getResolvedNumber() {
      const Bluebird = require('bluebird');
      return Bluebird.resolve(42);
    }

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.resolvedObject)
    getResolvedObject() {
      const Bluebird = require('bluebird');
      return Bluebird.resolve({foo: 'bar'});
    }

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedString)
    getRejectedString() {
      const Bluebird = require('bluebird');
      return Bluebird.reject('some');
    }

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.rejectedObject)
    getRejectedObject() {
      const Bluebird = require('bluebird');
      return Bluebird.reject({bar: 'baz'});
    }

    @Get(TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES.notBluebird)
    getNotBluebird() {
      return 'not bluebird';
    }

  }

  // server can be created after all controllers defined
  const app = createExpressServer();
  const server = (<any> http.createServer(app)).listen(); // hack for IDEA to prevent RED color for 'listen()'
  const port = server.address().port;
  app.set('port', port);

  Utils.log(app);
  return {
    app: app,
    stopServer: stopServer
  };

  // hack to remove element from the array while iterating
  // http://stackoverflow.com/questions/9882284
  function stopServer(): void {
    server.close();

    const routesNames: string[] = [];
    Utils.collectPropertiesNames(routesNames, CONTROLLERS.controller, TEST_CONTROLLER_ROUTES);
    Utils.collectPropertiesNames(routesNames, CONTROLLERS.jsonController, TEST_CONTROLLER_ROUTES);
    Utils.collectPropertiesNames(routesNames, CONTROLLERS.specialsController, TEST_SPECIALS_CONTROLLER_ROUTES);
    Utils.collectPropertiesNames(routesNames, CONTROLLERS.jsonControllerWithCustomResponse, TEST_JSON_CONTROLLER_WITH_CUSTOM_RESPONSE_ROUTES);
    Utils.collectPropertiesNames(routesNames, CONTROLLERS.interceptedController, TEST_CONTROLLER_ROUTES);
    Utils.collectPropertiesNames(routesNames, CONTROLLERS.jsonInterceptedController, TEST_CONTROLLER_ROUTES);
    Utils.collectPropertiesNames(routesNames, CONTROLLERS.bluebirdInterceptedController, TEST_CONTROLLER_ROUTES);
    Utils.collectPropertiesNames(routesNames, CONTROLLERS.jsonOverriddenBluebirdController, TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES);
    Utils.collectPropertiesNames(routesNames, CONTROLLERS.jsonBluebirdPackedToPromiseController, TEST_JSON_OVERRIDDEN_BLUEBIRD_CONTROLLER_ROUTES);

    _.forEach(routesNames, route => ExpressRemoveRoute(app, route));

    const metadataArgsStorage = defaultMetadataArgsStorage();

    const controllers = metadataArgsStorage.controllers;
    let controllerIndex = controllers.length;
    while (controllerIndex--) {
      const controller: ControllerMetadataArgs = controllers[controllerIndex];
      const isTestController: boolean = [
          CONTROLLERS.controller,
          CONTROLLERS.jsonController,
          CONTROLLERS.specialsController,
          CONTROLLERS.jsonControllerWithCustomResponse,
          CONTROLLERS.interceptedController,
          CONTROLLERS.jsonInterceptedController,
          CONTROLLERS.bluebirdInterceptedController,
          CONTROLLERS.jsonOverriddenBluebirdController,
          CONTROLLERS.jsonBluebirdPackedToPromiseController
        ].indexOf(controller.route) > -1;
      if (!isTestController) {
        continue;
      }
      const index: number = controllers.indexOf(controller);
      controllers.splice(index, 1);
    }

    const interceptors = metadataArgsStorage.interceptors;
    let interceptorIndex = interceptors.length;
    while (interceptorIndex--) {
      const interceptor: InterceptorMetadataArgs = interceptors[interceptorIndex];
      const interceptorClassConstructor = interceptor.target;
      const isTestInterceptor: boolean = Utils.isKnownTestInterceptor(
        interceptorClassConstructor,
        [
          TestResultTypeInterceptor,
          TestBluebirdCheckerInterceptor,
          TestOverrideBluebirdResultInterceptor,
          TestBluebirdPackedToPromiseInterceptor
        ]
      );
      if (!isTestInterceptor) {
        continue;
      }
      const index: number = interceptors.indexOf(interceptor);
      interceptors.splice(index, 1);
    }

    Utils.log(app);
  }
}

class Utils {

  static collectPropertiesNames(container, prefix, routesListObject) {
    for (const prop in routesListObject) {
      if (routesListObject.hasOwnProperty(prop)) {
        container.push(prefix + routesListObject[prop]);
      }
    }
  }

  static isKnownTestInterceptor(interceptorClassConstructor: any, interceptorClasses: any[]) {
    let isOneOfInterceptors: boolean = false;

    _.forEach(interceptorClasses, cls => {
      if (isOneOfInterceptors) {
        return;
      }
      isOneOfInterceptors = cls === interceptorClassConstructor;
    });

    return isOneOfInterceptors;
  }

  static log(app) {
    const routesOfExpressApplication: string[] = [];
    app._router.stack.forEach(function (middleware: any) {
      if (middleware.route) { // routes registered directly on the app
        const path = middleware.route.path;
        routesOfExpressApplication.push(path);
      } else if (middleware.name === 'router') { // router middleware
        const prefix = middleware.regexp
          ? middleware.regexp
          : '';
        middleware.handle.stack.forEach(function (handler) {
          const route = handler.route;
          if (!route) {
            return;
          }

          const path = route.path;
          routesOfExpressApplication.push(prefix + path);
        });
      }
    });
    logger.debug('----------------------------test-app--');
    logger.debug(routesOfExpressApplication);
    logger.debug(defaultMetadataArgsStorage().controllers);
    logger.debug(defaultMetadataArgsStorage().interceptors);
  }
}