/// <reference path="../../../app/typings/tsd.d.ts"/>

import * as chai from 'chai';
import * as _ from 'lodash';
import * as Promise from 'bluebird';

import * as Fixtures from '../../utils-for-tests/fixtures';

import {
  getConnection, Condition, Content, ContentCondition
} from '../../../app/models/models';
import * as sequelize from 'sequelize';

const assert = chai.assert;

describe('ConditionTest', function () {

  const CONTENT_ID_1: number = 1;
  const CONTENT_ID_2: number = 2;
  const CONTENT_ID_3: number = 3;

  before('init fixtures', async function () {
    await Fixtures.Builder.defaultFixtures().build();
  });

  beforeEach('clean previously created', async function () {
    await getConnection().transaction(async function(t: sequelize.Transaction): Promise<void> {
      const options: sequelize.DestroyOptions = {transaction: t, truncate: true, cascade: true};
      await ContentCondition.Model.destroy(options);
      await Condition.Model.destroy(options);
    })
  });

  it('Condition is unique at DB level', async function () {
    const attributes: Condition.Attributes = {
      value: {
        type: Condition.Type.PURCHASE,
        contentId: CONTENT_ID_1
      }
    };

    const created = await Condition.Model.create(attributes);
    assert.equal(created.value.type, Condition.Type.PURCHASE);
    assert.equal(created.value.contentId, 1);

    try {
      await Condition.Model.create(attributes);
      assert.fail('We should not be here. DB exception should be thrown');
    } catch (err) {
      assert.equal(err.message, 'Validation error');
      assert.equal(err.name, 'SequelizeUniqueConstraintError');
      assert.isArray(err.errors);
      assert.equal(err.errors[0].message, 'value must be unique');
      assert.equal(err.errors[0].path, 'value');
    }
  });

  it('should be possible to get all contents linked with current condition. one content', async function () {
    const condition1 = await createPurchasedCondition(CONTENT_ID_1);
    await linkConditionsToContent(CONTENT_ID_2, [condition1]);

    const queried = await Condition.Model.scope(Condition.Scopes.INCLUDE_CONTENTS).findById(condition1.id);
    assert.equal(queried.contents.length, 1);
    assert.equal(queried.contents[0].id, CONTENT_ID_2);
  });

  it('should be possible to get all contents linked with current condition. two contents', async function () {
    const condition1 = await createPurchasedCondition(CONTENT_ID_1);
    await linkConditionsToContent(CONTENT_ID_2, [condition1]);
    await linkConditionsToContent(CONTENT_ID_3, [condition1]);

    const queried = await Condition.Model.scope(Condition.Scopes.INCLUDE_CONTENTS).findById(condition1.id);
    assert.equal(queried.contents.length, 2);
    assert.isDefined(_.findWhere(queried.contents, {id: CONTENT_ID_2}));
    assert.isDefined(_.findWhere(queried.contents, {id: CONTENT_ID_3}));
  });

  async function createPurchasedCondition(contentId: number): Promise<Condition.Instance> {
    return Condition.Model.create({
      value: {
        type: Condition.Type.PURCHASE,
        contentId: contentId
      }
    });
  }

  async function linkConditionsToContent(contentId: number, conditions: Condition.Instance[]): Promise<void> {
    const condition = await Content.Model.scope(Content.Scopes.INCLUDE_CONDITIONS).findById(contentId);
    return condition.addConditions(conditions);
  }
});

