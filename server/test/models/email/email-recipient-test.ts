/// <reference path="../../../app/typings/tsd.d.ts"/>
import mocha = require('mocha');
import chai = require('chai');
import _ = require('lodash');

import Models = require('../../../app/models/models');
import EmailRecipient = Models.EmailRecipient;

let assert = chai.assert;

describe('EmailRecipientTest', function () {

  const DEFAULT_LANGUAGE: EmailRecipient.PossibleLanguage = 'English';

  it('available language', function () {
    assert.equal(EmailRecipient.LANGUAGE_SELECTOR.select(undefined), DEFAULT_LANGUAGE);
    assert.equal(EmailRecipient.LANGUAGE_SELECTOR.select(null), DEFAULT_LANGUAGE);
    assert.equal(EmailRecipient.LANGUAGE_SELECTOR.select(''), DEFAULT_LANGUAGE);
    assert.equal(EmailRecipient.LANGUAGE_SELECTOR.select('any'), DEFAULT_LANGUAGE);
    assert.equal(EmailRecipient.LANGUAGE_SELECTOR.select('de'), 'German');
    assert.equal(EmailRecipient.LANGUAGE_SELECTOR.select('sk'), 'Slovak');
    assert.equal(EmailRecipient.LANGUAGE_SELECTOR.select('en'), DEFAULT_LANGUAGE);
  })
});

