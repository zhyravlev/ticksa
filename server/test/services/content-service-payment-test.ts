/// <reference path="../../app/typings/tsd.d.ts"/>

const logger = require('../../app/logger/logger')(__filename);

import * as _ from 'lodash';

import * as Fixtures from '../utils-for-tests/fixtures';

import * as ContentService from '../../app/services/content-service';
import {TicksTransaction} from '../../app/models/models';

describe('ContentServiceTest', function () {

  before('init fixtures', function (done: MochaDone) {

    var builder = Fixtures.Builder.defaultFixtures();

    var tickserBuilder = builder.getTickserBuilder();
    var accountBuilder = tickserBuilder.getAccountBuilder();
    var contentBuilder = tickserBuilder.getContentBuilder();

    //account1  with 10 payed tics
    accountBuilder.withPayedTicks(10, [1]);
    //account2  with 10 free tics
    accountBuilder.withFreeTicks(10, [2]);
    //account3 with 1 payed and 1 free tics + content4 with tickPrice = 2
    accountBuilder.withPayedTicks(1, [3]).withFreeTicks(1, [3]);
    contentBuilder.withTickPrice(2, [4]);

    builder.build().then(function () {
      done();
    }).catch(function (err) {
      done(err);
    });
  });

  it('pay + refund for payed ticks> check transactions', function (done: MochaDone) {
    var userId = 1;
    var accountId = 1;
    var contentId = 2;

    payRefundWithCheckCorrectTransactions(userId, accountId, contentId).then(function () {
      done();
    }).catch(function (err) {
      done(err);
    });
  });

  it('pay + refund for payed + free ticks > check transactions', function (done: MochaDone) {
    var userId = 2;
    var accountId = 2;
    var contentId = 3;

    payRefundWithCheckCorrectTransactions(userId, accountId, contentId).then(function () {
      done();
    }).catch(function (err) {
      done(err);
    });
  });

  it('pay + refund for payed + free ticks > check transactions', function (done: MochaDone) {
    var userId = 3;
    var accountId = 3;
    var contentId = 4;

    payRefundWithCheckCorrectTransactions(userId, accountId, contentId).then(function () {
      done();
    }).catch(function (err) {
      done(err);
    });
  });

  it('pay + refund should not be done because not an existing user', function (done: MochaDone) {
    var userId = 99;
    var accountId = 5;
    var contentId = 6;

    payRefundWithCheckCorrectTransactions(userId, accountId, contentId).then(function () {
      done();
    }).catch(function (err) {
      logger.info(err);
      done();
    });
  });

  it('pay + refund should not be done because not enough money to pay', function (done: MochaDone) {
    var userId = 5;
    var accountId = 5;
    var contentId = 6;

    payRefundWithCheckCorrectTransactions(userId, accountId, contentId).then(function () {
      done();
    }).catch(function (err) {
      logger.info(err);
      done();
    });
  });
});

const geoip = {ip: '', countryA2Code: ''}; // it is enough because contents has no geographic fencing enabled

function payRefundWithCheckCorrectTransactions(userId, accountId, contentId) {
  return ContentService.payContent(userId, accountId, contentId, geoip).then(function () {
    return ContentService.refundContent(userId, accountId, contentId);
  }).then(function () {
    return TicksTransaction.Model.scope(['ticksAmounts']).findAll({
      where: {
        contentId: contentId,
      },
    });
  }).then(function (transactions: Array<any>) {
    if (transactions.length != 2) {
      return Promise.reject('Transactions count: ' + transactions.length);
    }

    if (!isCorrectReversedTransactions(transactions[0], transactions[1])) {
      return Promise.reject('Incorrect transactions after refund');
    }
  });
}

function isCorrectReversedTransactions(t1, t2) {
  if (t1.ticksAmounts.length != t2.ticksAmounts.length) {
    return false;
  }

  var firstTicksAmounts: any = _.indexBy(t1.ticksAmounts, 'accountId');
  var secondTicksAmounts: any = _.indexBy(t2.ticksAmounts, 'accountId');

  var isSumOfAllReverseAmountEqualZero = true;
  _.forIn<any>(firstTicksAmounts, function (value, id) {
    if (firstTicksAmounts[id].amount + secondTicksAmounts[id].amount != 0) {
      isSumOfAllReverseAmountEqualZero = false;
    }
  });
  return isSumOfAllReverseAmountEqualZero;
}