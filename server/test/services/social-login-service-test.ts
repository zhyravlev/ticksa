/// <reference path="../../app/typings/tsd.d.ts"/>
import mocha = require('mocha');
import chai = require('chai');
import _ = require('lodash');

import SocialLoginService = require('../../app/services/social-login-service');

let assert = chai.assert;

describe('SocialLoginServiceTest', function () {

  it('test', function () {
    check('', '', '/social/login/facebook/ref--content-');
    check('1', '2', '/social/login/facebook/ref-1-content-2');
    check('12345', '67890', '/social/login/facebook/ref-12345-content-67890');
    check(12345, 67890, '/social/login/facebook/ref-12345-content-67890');
    check(undefined, undefined, '/social/login/facebook/ref-undefined-content-undefined');
    check(null, null, '/social/login/facebook/ref-null-content-null');
  });

  function check(referrerId: any, contentId: any, result: string) {
    assert.equal(SocialLoginService.facebookLoginRedirectUriForContentTeaser(referrerId, contentId), result);
  }

});