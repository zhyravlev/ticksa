/// <reference path="../../app/typings/tsd.d.ts"/>

import * as util from 'util';
import * as _ from 'lodash';

import * as config from '../../app/config';
import * as Fixtures from '../utils-for-tests/fixtures';
import * as TicksTransactionService from '../../app/services/ticks-transaction-service';
import * as ContentService from '../../app/services/content-service';

import * as Models from '../../app/models/models';
import {
  TicksTransaction,
  TicksAmount,
  Account,
  Country
} from '../../app/models/models';
import {QueryTypes} from 'sequelize';

import {assert} from 'chai';

// case1: process two way transaction (payed ticks) (content price > 0)
const CONTENT_ID_CASE1 = 1; // content id == channel id
const CREDIT_ACCOUNT_CASE1 = 1;
const SHARED_ACCOUNT1_CASE1 = 2;
const SHARED_ACCOUNT2_CASE1 = 3;
const DEBIT_ACCOUNT_CASE1 = 4;
const SHARED_PERCENTS_CASE1 = 50;

// case2: process two way transaction (payed + free ticks) (content price > 0)
const CONTENT_ID_CASE2 = 5; // content id == channel id
const CREDIT_ACCOUNT_CASE2 = 6;
const SHARED_ACCOUNT1_CASE2 = 7;
const SHARED_ACCOUNT2_CASE2 = 8;
const DEBIT_ACCOUNT_CASE2 = 9;
const SHARED_PERCENTS_CASE2 = 25;

// case3: process two way transaction with campaign account (payed + free ticks) (content price > 0)
const CONTENT_ID_CASE3 = 10; // content id == channel id
const CREDIT_ACCOUNT_CASE3 = 11;
const DEBIT_ACCOUNT_CASE3 = 12;
const CAMPAIGN_ACCOUNT_CASE3 = 13;

// case4: process two way transaction with credited account as shared (content price > 0)
const CONTENT_ID_CASE4 = 14; // content id == channel id
const CREDIT_ACCOUNT_CASE4 = 14;
const DEBIT_ACCOUNT_CASE4 = 16;
const SHARED_ACCOUNT_CASE4 = DEBIT_ACCOUNT_CASE4;
const SHARED_PERCENTS_CASE4 = 50;

// case5: process two way transaction with donate channel behalf other tickser (content price > 0)
const CHANNEL_ID_CASE5 = 18; // content id == channel id
const CREDIT_ACCOUNT_CASE5 = CHANNEL_ID_CASE5;
const DEBIT_ACCOUNT_CASE5 = 19;
const BEHALF_ACCOUNT_CASE5 = 20;
const DONATE_AMOUNT_CASE5 = 10;

// case9: process two way transaction (payed ticks) (content price < 0)
const CONTENT_ID_CASE9 = 19; // content id == channel id
const SELLER_ACCOUNT_CASE9 = 21;
const BUYER_ACCOUNT_CASE9 = 22;

// case10: process two way transaction (payed + free ticks) (content price < 0)
const CONTENT_ID_CASE10 = 20; // content id == channel id
const SELLER_ACCOUNT_CASE10 = 23;
const BUYER_ACCOUNT_CASE10 = 24;

// case11: process two way transaction with campaign account (payed + free ticks)  (content price < 0)
const CONTENT_ID_CASE11 = 21; // content id == channel id
const SELLER_ACCOUNT_CASE11 = 25;
const BUYER_ACCOUNT_CASE11 = 26;
const CAMPAIGN_ACCOUNT_CASE11 = 27;

// case12: process two way transaction with credited account as shared (content price < 0)
const CONTENT_ID_CASE12 = 22; // content id == channel id
const SELLER_ACCOUNT_CASE12 = 28;
const BUYER_ACCOUNT_CASE12 = 29;

// case16: process two way transaction (payed ticks) (content price = 0)
const CONTENT_ID_CASE16 = 23; // content id == channel id
const SELLER_ACCOUNT_CASE16 = 30;
const SHARED_ACCOUNT1_CASE16 = 31;
const SHARED_ACCOUNT2_CASE16 = 32;
const BUYER_ACCOUNT_CASE16 = 33;
const SHARED_PERCENTS_CASE16 = 25;

// case17: process two way transaction (payed + free ticks) (content price = 0)
const CONTENT_ID_CASE17 = 24; // content id == channel id
const SELLER_ACCOUNT_CASE17 = 34;
const SHARED_ACCOUNT1_CASE17 = 35;
const SHARED_ACCOUNT2_CASE17 = 36;
const BUYER_ACCOUNT_CASE17 = 37;
const SHARED_PERCENTS_CASE17 = 25;

// case18: process two way transaction with campaign account (payed + free ticks) (content price = 0)
const CONTENT_ID_CASE18 = 25; // content id == channel id
const SELLER_ACCOUNT_CASE18 = 38;
const BUYER_ACCOUNT_CASE18 = 39;
const CAMPAIGN_ACCOUNT_CASE18 = 40;

// case19: process two way transaction with credited account as shared (content price = 0)
const CONTENT_ID_CASE19 = 26; // content id == channel id
const SELLER_ACCOUNT_CASE19 = 41;
const BUYER_ACCOUNT_CASE19 = 42;
const SHARED_ACCOUNT_CASE19 = BUYER_ACCOUNT_CASE19;
const SHARED_PERCENTS_CASE19 = 25;

// case20: process two way transaction with country vat
const CONTENT_ID_CASE20 = 27; // content id == channel id
const SELLER_ACCOUNT_CASE20 = 43;
const BUYER_ACCOUNT_CASE20 = 44;
const SELLER_COUNTRY_CASE20 = 1;

// case21: process two way transaction with country. but seller has no country
const CONTENT_ID_CASE21 = 28;
const SELLER_ACCOUNT_CASE21 = 45;
const BUYER_ACCOUNT_CASE21 = 46;

interface AccountDiffMap {
  beforeBuy?: Account.Instance[];
  afterBuy?: Account.Instance[];
  afterRefund?: Account.Instance[];
}

describe('TicksTransactionServiceTest', function () {

  const accountsInitialState: Account.Attributes[] = [];

  before('init fixtures', async() => {
    let builder = Fixtures.Builder.defaultFixtures();

    let tickserBuilder = builder.getTickserBuilder().withCount(46);
    let accountBuilder = tickserBuilder.getAccountBuilder();
    let contentBuilder = tickserBuilder.getContentBuilder();
    let channelBuilder = tickserBuilder.getChannelBuilder();
    let revenueBuilder = new Fixtures.ChannelRevenueBuilder();

    // case1: process two way transaction (payed ticks)  (content price > 0)
    accountBuilder
      .withPayedTicks(10, [DEBIT_ACCOUNT_CASE1]);
    contentBuilder
      .withTickPrice(10, [CONTENT_ID_CASE1]);
    revenueBuilder
      .addRevenue({
        channelId: CONTENT_ID_CASE1,
        tickserId: SHARED_ACCOUNT1_CASE1,
        accountId: SHARED_ACCOUNT1_CASE1,
        percent: SHARED_PERCENTS_CASE1
      })
      .addRevenue({
        channelId: CONTENT_ID_CASE1,
        tickserId: SHARED_ACCOUNT2_CASE1,
        accountId: SHARED_ACCOUNT2_CASE1,
        percent: SHARED_PERCENTS_CASE1
      });
    channelBuilder
      .withShareIncome(true, [CONTENT_ID_CASE1])
      .withRevenueBuilder(revenueBuilder);

    // case2: process two way transaction (payed + free ticks) (content price > 0)
    accountBuilder
      .withPayedTicks(5, [DEBIT_ACCOUNT_CASE2])
      .withFreeTicks(5, [DEBIT_ACCOUNT_CASE2]);
    contentBuilder
      .withTickPrice(10, [CONTENT_ID_CASE2]);
    revenueBuilder
      .addRevenue({
        channelId: CONTENT_ID_CASE2,
        tickserId: SHARED_ACCOUNT1_CASE2,
        accountId: SHARED_ACCOUNT1_CASE2,
        percent: SHARED_PERCENTS_CASE2
      })
      .addRevenue({
        channelId: CONTENT_ID_CASE2,
        tickserId: SHARED_ACCOUNT2_CASE2,
        accountId: SHARED_ACCOUNT2_CASE2,
        percent: SHARED_PERCENTS_CASE2
      });
    channelBuilder
      .withShareIncome(true, [CONTENT_ID_CASE2])
      .withRevenueBuilder(revenueBuilder);

    // case3: process two way transaction with campaign account (payed + free ticks) (content price > 0)
    accountBuilder
      .withPayedTicks(5, [DEBIT_ACCOUNT_CASE3])
      .withFreeTicks(5, [DEBIT_ACCOUNT_CASE3]);
    contentBuilder
      .withTickPrice(10, [CONTENT_ID_CASE3]);

    // case4: process two way transaction with credited account as shared (content price > 0)
    revenueBuilder
      .addRevenue({
        channelId: CONTENT_ID_CASE4,
        tickserId: SHARED_ACCOUNT_CASE4,
        accountId: SHARED_ACCOUNT_CASE4,
        percent: SHARED_PERCENTS_CASE4
      });
    accountBuilder
      .withPayedTicks(10, [DEBIT_ACCOUNT_CASE4]);
    contentBuilder
      .withTickPrice(10, [CONTENT_ID_CASE4]);
    channelBuilder
      .withShareIncome(true, [CONTENT_ID_CASE4])
      .withRevenueBuilder(revenueBuilder);

    // case5: process two way transaction with donate channel behalf other tickser (content price > 0)
    accountBuilder
      .withPayedTicks(10, [DEBIT_ACCOUNT_CASE5]);
    channelBuilder
      .withShowRankedList(true, [CHANNEL_ID_CASE5]);

    // case9: process two way transaction (payed ticks) (content price < 0)
    accountBuilder
      .withPayedTicks(10, [SELLER_ACCOUNT_CASE9]);
    contentBuilder
      .withTickPrice(10, [CONTENT_ID_CASE9]);

    // case10: process two way transaction (payed + free ticks) (content price < 0)
    accountBuilder
      .withPayedTicks(5, [SELLER_ACCOUNT_CASE10])
      .withFreeTicks(5, [SELLER_ACCOUNT_CASE10]);
    contentBuilder
      .withTickPrice(10, [CONTENT_ID_CASE10]);

    // case11: process two way transaction with campaign account (payed + free ticks)  (content price < 0)
    accountBuilder
      .withPayedTicks(5, [SELLER_ACCOUNT_CASE11])
      .withFreeTicks(5, [SELLER_ACCOUNT_CASE11]);
    contentBuilder
      .withTickPrice(10, [CONTENT_ID_CASE11]);

    // case12: process two way transaction with credited account as shared (content price < 0)
    accountBuilder
      .withPayedTicks(10, [SELLER_ACCOUNT_CASE12]);
    contentBuilder
      .withTickPrice(10, [CONTENT_ID_CASE12]);

    // case16: process two way transaction (payed ticks) (content price = 0)
    accountBuilder
      .withPayedTicks(10, [SELLER_ACCOUNT_CASE16]);
    contentBuilder
      .withTickPrice(0, [CONTENT_ID_CASE16]);
    revenueBuilder
      .addRevenue({
        channelId: CONTENT_ID_CASE16,
        tickserId: SHARED_ACCOUNT1_CASE16,
        accountId: SHARED_ACCOUNT1_CASE16,
        percent: SHARED_PERCENTS_CASE16
      })
      .addRevenue({
        channelId: CONTENT_ID_CASE16,
        tickserId: SHARED_ACCOUNT2_CASE16,
        accountId: SHARED_ACCOUNT2_CASE16,
        percent: SHARED_PERCENTS_CASE16
      });
    channelBuilder
      .withShareIncome(true, [CONTENT_ID_CASE16])
      .withRevenueBuilder(revenueBuilder);

    // case17: process two way transaction (payed + free ticks) (content price = 0)
    accountBuilder
      .withPayedTicks(5, [SELLER_ACCOUNT_CASE17])
      .withFreeTicks(5, [SELLER_ACCOUNT_CASE17]);
    contentBuilder
      .withTickPrice(0, [CONTENT_ID_CASE17]);
    revenueBuilder
      .addRevenue({
        channelId: CONTENT_ID_CASE17,
        tickserId: SHARED_ACCOUNT1_CASE17,
        accountId: SHARED_ACCOUNT1_CASE17,
        percent: SHARED_PERCENTS_CASE17
      })
      .addRevenue({
        channelId: CONTENT_ID_CASE17,
        tickserId: SHARED_ACCOUNT2_CASE17,
        accountId: SHARED_ACCOUNT2_CASE17,
        percent: SHARED_PERCENTS_CASE17
      });
    channelBuilder
      .withShareIncome(true, [CONTENT_ID_CASE17])
      .withRevenueBuilder(revenueBuilder);

    // case18: process two way transaction with campaign account (payed + free ticks) (content price = 0)
    accountBuilder
      .withPayedTicks(5, [SELLER_ACCOUNT_CASE18])
      .withFreeTicks(5, [SELLER_ACCOUNT_CASE18]);
    contentBuilder
      .withTickPrice(0, [CONTENT_ID_CASE11]);

    // case19: process two way transaction with credited account as shared (content price = 0)
    revenueBuilder
      .addRevenue({
        channelId: CONTENT_ID_CASE19,
        tickserId: SHARED_ACCOUNT_CASE19,
        accountId: SHARED_ACCOUNT_CASE19,
        percent: SHARED_PERCENTS_CASE19
      });
    accountBuilder
      .withPayedTicks(10, [SELLER_ACCOUNT_CASE19]);
    contentBuilder
      .withTickPrice(0, [CONTENT_ID_CASE19]);
    channelBuilder
      .withShareIncome(true, [CONTENT_ID_CASE19])
      .withRevenueBuilder(revenueBuilder);

    // case20: process two way transaction with country vat
    accountBuilder
      .withPayedTicks(5, [BUYER_ACCOUNT_CASE20])
      .withFreeTicks(5, [BUYER_ACCOUNT_CASE20]);
    tickserBuilder
      .withCountryId(SELLER_COUNTRY_CASE20, [SELLER_ACCOUNT_CASE20]);

    // case21: process two way transaction with country. but seller has no country
    accountBuilder
      .withPayedTicks(10, [BUYER_ACCOUNT_CASE21]);
    contentBuilder
      .withTickPrice(10, [CONTENT_ID_CASE21]);
    tickserBuilder.withCountryId(null, [SELLER_ACCOUNT_CASE21]);

    await builder.build();

    // for case20
    await Country.Model.update(
      {
        VAT: 0.25
      }, {
        where: {
          id: SELLER_COUNTRY_CASE20
        }
      });

    // init postconditions state
    const accounts: Account.Instance[] = await Account.Model.findAll({
      where: {
        id: {
          $notIn: [CREDIT_ACCOUNT_CASE5, DEBIT_ACCOUNT_CASE5] // These accounts no need to compare because they are haven't refunded
        }
      }
    });

    _.forEach(accounts, a => {
      accountsInitialState.push(a.get({plain: true, clone: true}))
    });
  });

  function assertTicksAmount(ticksAmount: TicksAmount.Instance, amount: number, reason?: TicksAmount.Reason) {
    reason = (reason != null) ? reason : TicksAmount.Reason.DEFAULT;
    assert.isNotNull(ticksAmount, 'TicksAmount is null');
    assert.isDefined(ticksAmount, 'TicksAmount is undefined');
    assert.equal(ticksAmount.amount, amount, 'TicksAmount amount not equal');
    assert.equal(ticksAmount.reason, reason, 'TicksAmount reason not equal');
  }

  it('case1: process two way transaction (payed ticks) (content price > 0)', async() => {

    let usedAccounts = [CREDIT_ACCOUNT_CASE1, SHARED_ACCOUNT1_CASE1, SHARED_ACCOUNT2_CASE1, DEBIT_ACCOUNT_CASE1,
      Account.SystemAccount.GENERAL_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: DEBIT_ACCOUNT_CASE1,
      sellerTickserId: CREDIT_ACCOUNT_CASE1,
      contentId: CONTENT_ID_CASE1,
      amount: 10,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 8);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForDebit = _.findWhere(ticksAmounts, {
      accountId: DEBIT_ACCOUNT_CASE1,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForGeneral = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.GENERAL_ACCOUNT,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForCredit = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE1,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForSharing = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE1,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });
    let ticksAmountForShared1 = _.findWhere(ticksAmounts, {
      accountId: SHARED_ACCOUNT1_CASE1,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });
    let ticksAmountForShared2 = _.findWhere(ticksAmounts, {
      accountId: SHARED_ACCOUNT2_CASE1,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });

    let ticksDebited = 10;
    let adminCost = ticksDebited * config.global.adminCostPercent;
    let ticksCredited = ticksDebited - adminCost;
    let sharedTicks1 = (ticksDebited - adminCost) / 100 * SHARED_PERCENTS_CASE1;
    let sharedTicks2 = (ticksDebited - adminCost) / 100 * SHARED_PERCENTS_CASE1;
    let ticksForSharing = sharedTicks1 + sharedTicks2;

    assertTicksAmount(ticksAmountForDebit, -ticksDebited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForGeneral, adminCost, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCredit, ticksCredited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSharing, -ticksForSharing, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared1, sharedTicks1, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared2, sharedTicks2, TicksAmount.Reason.REVENUE_SHARE);

    await refundAndDiffAccounts(usedAccounts, accountsDiffMap, DEBIT_ACCOUNT_CASE1, CONTENT_ID_CASE1);
  });

  it('case2: process two way transaction (payed + free ticks) (content price > 0)', async() => {
    let usedAccounts = [CREDIT_ACCOUNT_CASE2, SHARED_ACCOUNT1_CASE2, SHARED_ACCOUNT2_CASE2, DEBIT_ACCOUNT_CASE2,
      Account.SystemAccount.GENERAL_ACCOUNT, Account.SystemAccount.FREE_TICKS_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: DEBIT_ACCOUNT_CASE2,
      sellerTickserId: CREDIT_ACCOUNT_CASE2,
      contentId: CONTENT_ID_CASE2,
      amount: 10,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 14);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForDebitFree = _.findWhere(ticksAmounts, {
      accountId: DEBIT_ACCOUNT_CASE2,
      type: TicksAmount.Type.FREE
    });
    let ticksAmountForSystemFree = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.FREE_TICKS_ACCOUNT,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForCreditFree = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE2,
      type: TicksAmount.Type.FREE,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForSharingFree = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE2,
      type: TicksAmount.Type.FREE,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });
    let ticksAmountForShared1Free = _.findWhere(ticksAmounts, {
      accountId: SHARED_ACCOUNT1_CASE2,
      type: TicksAmount.Type.FREE
    });
    let ticksAmountForShared2Free = _.findWhere(ticksAmounts, {
      accountId: SHARED_ACCOUNT2_CASE2,
      type: TicksAmount.Type.FREE
    });

    let ticksAmountForDebitPaid = _.findWhere(ticksAmounts, {
      accountId: DEBIT_ACCOUNT_CASE2,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForSystemPaid = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.GENERAL_ACCOUNT,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForCreditPaid = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE2,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForSharingPaid = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE2,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });
    let ticksAmountForShared1Paid = _.findWhere(ticksAmounts, {
      accountId: SHARED_ACCOUNT1_CASE2,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForShared2Paid = _.findWhere(ticksAmounts, {
      accountId: SHARED_ACCOUNT2_CASE2,
      type: TicksAmount.Type.PAID
    });

    let ticksDebited = 5;

    let adminCostFreeTicks = ticksDebited * config.global.adminCostFreeTicksPercent;
    let freeTicksCredited = ticksDebited - adminCostFreeTicks;
    let sharedFreeTicks1 = freeTicksCredited / 100 * SHARED_PERCENTS_CASE2;
    let sharedFreeTicks2 = freeTicksCredited / 100 * SHARED_PERCENTS_CASE2;
    let freeTicksForSharing = sharedFreeTicks1 + sharedFreeTicks2;

    let adminCostPaidTicks = ticksDebited * config.global.adminCostPercent;
    let paidTicksCredited = ticksDebited - adminCostPaidTicks;
    let sharedPaidTicks1 = paidTicksCredited / 100 * SHARED_PERCENTS_CASE2;
    let sharedPaidTicks2 = paidTicksCredited / 100 * SHARED_PERCENTS_CASE2;
    let paidTicksForSharing = sharedPaidTicks1 + sharedPaidTicks2;

    assertTicksAmount(ticksAmountForDebitFree, -ticksDebited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSystemFree, adminCostFreeTicks, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCreditFree, freeTicksCredited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSharingFree, -freeTicksForSharing, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared1Free, sharedFreeTicks1, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared2Free, sharedFreeTicks2, TicksAmount.Reason.REVENUE_SHARE);

    assertTicksAmount(ticksAmountForDebitPaid, -ticksDebited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSystemPaid, adminCostPaidTicks, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCreditPaid, paidTicksCredited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSharingPaid, -paidTicksForSharing, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared1Paid, sharedPaidTicks1, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared2Paid, sharedPaidTicks2, TicksAmount.Reason.REVENUE_SHARE);

    await refundAndDiffAccounts(usedAccounts, accountsDiffMap, DEBIT_ACCOUNT_CASE2, CONTENT_ID_CASE2);
  });

  it('case3: process two way transaction with campaign account (payed + free ticks) (content price > 0)', async() => {
    let usedAccounts = [CREDIT_ACCOUNT_CASE3, CAMPAIGN_ACCOUNT_CASE3, DEBIT_ACCOUNT_CASE3,
      Account.SystemAccount.GENERAL_ACCOUNT, Account.SystemAccount.FREE_TICKS_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: DEBIT_ACCOUNT_CASE3,
      sellerTickserId: CREDIT_ACCOUNT_CASE3,
      campaignTickserId: CAMPAIGN_ACCOUNT_CASE3,
      contentId: CONTENT_ID_CASE3,
      amount: 10,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 10);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForCreditFree = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE3,
      type: TicksAmount.Type.FREE
    });
    let ticksAmountForCreditPaid = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE3,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForCampaignFree = _.findWhere(ticksAmounts, {
      accountId: CAMPAIGN_ACCOUNT_CASE3,
      type: TicksAmount.Type.FREE
    });
    let ticksAmountForCampaignPaid = _.findWhere(ticksAmounts, {
      accountId: CAMPAIGN_ACCOUNT_CASE3,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForDebitFree = _.findWhere(ticksAmounts, {
      accountId: DEBIT_ACCOUNT_CASE3,
      type: TicksAmount.Type.FREE
    });
    let ticksAmountForDebitPaid = _.findWhere(ticksAmounts, {
      accountId: DEBIT_ACCOUNT_CASE3,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForSystemGeneral = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.GENERAL_ACCOUNT,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForSystemFree = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.FREE_TICKS_ACCOUNT,
      reason: TicksAmount.Reason.DEFAULT
    });

    let ticksDebited = 5;
    let campaignFreeTicksPercent = ticksDebited * config.global.campaignFreeTicksPercent;
    let campaignPaidTicksPercent = ticksDebited * config.global.campaignTicksPercent;

    let adminCostFreeTicks = ticksDebited * config.global.adminCostFreeTicksPercent;
    let adminCostPaidTicks = ticksDebited * config.global.adminCostPercent;

    let creditFreeTicks = ticksDebited - adminCostFreeTicks - campaignFreeTicksPercent;
    let creditPaidTicks = ticksDebited - adminCostPaidTicks - campaignPaidTicksPercent;

    assert.equal(ticksAmountForDebitFree.amount, -ticksDebited);
    assert.equal(ticksAmountForDebitPaid.amount, -ticksDebited);

    assert.equal(ticksAmountForCreditFree.amount, creditFreeTicks);
    assert.equal(ticksAmountForCreditPaid.amount, creditPaidTicks);

    assert.equal(ticksAmountForCampaignFree.amount, campaignFreeTicksPercent);
    assert.equal(ticksAmountForCampaignPaid.amount, campaignPaidTicksPercent);

    assert.equal(ticksAmountForSystemFree.amount, adminCostFreeTicks);
    assert.equal(ticksAmountForSystemGeneral.amount, adminCostPaidTicks);

    return refundAndDiffAccounts(usedAccounts, accountsDiffMap, DEBIT_ACCOUNT_CASE3, CONTENT_ID_CASE3);
  });

  it('case4: process two way transaction with debited account as shared (content price > 0)', async() => {

    let usedAccounts = [CREDIT_ACCOUNT_CASE4, SHARED_ACCOUNT_CASE4, DEBIT_ACCOUNT_CASE4, Account.SystemAccount.GENERAL_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: DEBIT_ACCOUNT_CASE4,
      sellerTickserId: CREDIT_ACCOUNT_CASE4,
      contentId: CONTENT_ID_CASE4,
      amount: 10,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 7);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForDebit = _.findWhere(ticksAmounts, {
      accountId: DEBIT_ACCOUNT_CASE4,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForGeneral = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.GENERAL_ACCOUNT,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForCredit = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE4,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForSharing = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE4,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });
    let ticksAmountForShared = _.findWhere(ticksAmounts, {
      accountId: SHARED_ACCOUNT_CASE4,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });

    let ticksDebited = 10;
    let adminCost = ticksDebited * config.global.adminCostPercent;
    let ticksCredited = ticksDebited - adminCost;
    let sharedTicks = (ticksDebited - adminCost) / 100 * SHARED_PERCENTS_CASE4;
    let ticksForSharing = sharedTicks;

    assertTicksAmount(ticksAmountForDebit, -ticksDebited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForGeneral, adminCost, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCredit, ticksCredited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSharing, -ticksForSharing, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared, sharedTicks, TicksAmount.Reason.REVENUE_SHARE);

    return refundAndDiffAccounts(usedAccounts, accountsDiffMap, DEBIT_ACCOUNT_CASE4, CONTENT_ID_CASE4);
  });

  it('case5: process two way transaction with donate channel behalf other tickser (content price > 0)', async() => {
    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: DEBIT_ACCOUNT_CASE5,
      sellerTickserId: CREDIT_ACCOUNT_CASE5,
      donateChannelId: CHANNEL_ID_CASE5,
      behalfPayingTickserId: BEHALF_ACCOUNT_CASE5,
      amount: DONATE_AMOUNT_CASE5,
      type: TicksTransaction.Type.CHANNEL_DONATE,
      t: null
    };

    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 5);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForDebit = _.findWhere(ticksAmounts, {
      accountId: DEBIT_ACCOUNT_CASE5,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForGeneral = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.GENERAL_ACCOUNT,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForCredit = _.findWhere(ticksAmounts, {
      accountId: CREDIT_ACCOUNT_CASE5,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForBehalfIn = _.findWhere(ticksAmounts, {
      accountId: BEHALF_ACCOUNT_CASE5,
      reason: TicksAmount.Reason.BEHALF_PAYING,
      amount: DONATE_AMOUNT_CASE5
    });
    let ticksAmountForBehalfOut = _.findWhere(ticksAmounts, {
      accountId: BEHALF_ACCOUNT_CASE5,
      reason: TicksAmount.Reason.BEHALF_PAYING,
      amount: -DONATE_AMOUNT_CASE5
    });

    let ticksDebited = DONATE_AMOUNT_CASE5;
    let adminCost = ticksDebited * config.global.adminCostPercent;
    let ticksCredited = ticksDebited - adminCost;

    assertTicksAmount(ticksAmountForDebit, -ticksDebited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForGeneral, adminCost, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCredit, ticksCredited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForBehalfIn, ticksDebited, TicksAmount.Reason.BEHALF_PAYING);
    assertTicksAmount(ticksAmountForBehalfOut, -ticksDebited, TicksAmount.Reason.BEHALF_PAYING);

  });

  it('case6: not enough money (content price > 0)', function (done: MochaDone) {
    Models.getConnection().transaction(function (t) {
      let params: TicksTransactionService.TwoWayTransactionParams = {
        buyerTickserId: 1,
        sellerTickserId: 2,
        contentId: 3,
        amount: 999999,
        type: TicksTransaction.Type.ACCESS,
        t: t
      };

      return TicksTransactionService.processTwoWayTransaction(params);
    }).then(function () {
      done(new Error('Not enough money'));
    }).catch(function () {
      done();
    });
  });

  it('case7: should not buy (content price > 0)', function (done: MochaDone) {
    Models.getConnection().transaction(function (t) {
      let params: TicksTransactionService.TwoWayTransactionParams = {
        buyerTickserId: 1,
        sellerTickserId: 1,
        contentId: 1,
        amount: 10,
        type: TicksTransaction.Type.ACCESS,
        t: t
      };

      return TicksTransactionService.processTwoWayTransaction(params);
    }).then(function () {
      done(new Error('should not buy'));
    }).catch(function () {
      done();
    });
  });

  it('case8: should not buy (content price > 0)', function (done: MochaDone) {
    Models.getConnection().transaction(function (t) {
      let params: TicksTransactionService.TwoWayTransactionParams = {
        buyerTickserId: 1,
        sellerTickserId: 2,
        campaignTickserId: 1,
        contentId: 1,
        amount: 10,
        type: TicksTransaction.Type.ACCESS,
        t: t
      };

      return TicksTransactionService.processTwoWayTransaction(params);
    }).then(function () {
      done(new Error('should not buy'));
    }).catch(function () {
      done();
    });
  });

  it('case9: process two way transaction (payed ticks) (content price < 0)', async() => {

    let usedAccounts = [SELLER_ACCOUNT_CASE9, BUYER_ACCOUNT_CASE9, Account.SystemAccount.GENERAL_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: BUYER_ACCOUNT_CASE9,
      sellerTickserId: SELLER_ACCOUNT_CASE9,
      contentId: CONTENT_ID_CASE9,
      amount: -10,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 3);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForDebit = _.findWhere(ticksAmounts, {accountId: SELLER_ACCOUNT_CASE9});
    let ticksAmountForGeneral = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.GENERAL_ACCOUNT,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForCredit = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE9,
      reason: TicksAmount.Reason.DEFAULT
    });

    let ticksDebited = -10;

    let absTicksDebited = Math.abs(ticksDebited);

    let adminCost = absTicksDebited * config.global.adminCostPercent;
    let ticksCredited = absTicksDebited - adminCost;

    assertTicksAmount(ticksAmountForDebit, ticksDebited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForGeneral, adminCost, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCredit, ticksCredited, TicksAmount.Reason.DEFAULT);

    await refundAndDiffAccounts(usedAccounts, accountsDiffMap, BUYER_ACCOUNT_CASE9, CONTENT_ID_CASE9);
  });

  it('case10: process two way transaction (payed ticks + free ticks) (content price < 0)', async() => {
    let usedAccounts = [SELLER_ACCOUNT_CASE10, BUYER_ACCOUNT_CASE10,
      Account.SystemAccount.GENERAL_ACCOUNT, Account.SystemAccount.FREE_TICKS_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: BUYER_ACCOUNT_CASE10,
      sellerTickserId: SELLER_ACCOUNT_CASE10,
      contentId: CONTENT_ID_CASE10,
      amount: -10,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 6);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForDebitFree = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE10,
      type: TicksAmount.Type.FREE
    });
    let ticksAmountForSystemFree = _.findWhere(ticksAmounts, {accountId: Account.SystemAccount.FREE_TICKS_ACCOUNT});
    let ticksAmountForCreditFree = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE10,
      type: TicksAmount.Type.FREE,
      reason: TicksAmount.Reason.DEFAULT
    });

    let ticksAmountForDebitPaid = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE10,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForSystemPaid = _.findWhere(ticksAmounts, {accountId: Account.SystemAccount.GENERAL_ACCOUNT});
    let ticksAmountForCreditPaid = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE10,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.DEFAULT
    });

    let ticksDebited = -5;

    let absTicksDebited = Math.abs(ticksDebited);

    let adminCostFreeTicks = absTicksDebited * config.global.adminCostFreeTicksPercent;
    let freeTicksCredited = absTicksDebited - adminCostFreeTicks;

    let adminCostPaidTicks = absTicksDebited * config.global.adminCostPercent;
    let paidTicksCredited = absTicksDebited - adminCostPaidTicks;

    assertTicksAmount(ticksAmountForDebitFree, ticksDebited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSystemFree, adminCostFreeTicks, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCreditFree, freeTicksCredited, TicksAmount.Reason.DEFAULT);

    assertTicksAmount(ticksAmountForDebitPaid, ticksDebited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSystemPaid, adminCostPaidTicks, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCreditPaid, paidTicksCredited, TicksAmount.Reason.DEFAULT);

    await refundAndDiffAccounts(usedAccounts, accountsDiffMap, BUYER_ACCOUNT_CASE10, CONTENT_ID_CASE10);
  });

  it('case11: process two way transaction with campaign account (payed + free ticks) (content price < 0)', async() => {
    let usedAccounts = [SELLER_ACCOUNT_CASE11, CAMPAIGN_ACCOUNT_CASE11, BUYER_ACCOUNT_CASE11,
      Account.SystemAccount.GENERAL_ACCOUNT, Account.SystemAccount.FREE_TICKS_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: BUYER_ACCOUNT_CASE11,
      sellerTickserId: SELLER_ACCOUNT_CASE11,
      campaignTickserId: CAMPAIGN_ACCOUNT_CASE11,
      contentId: CONTENT_ID_CASE11,
      amount: -10,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 8);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForCreditFree = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE11,
      type: TicksAmount.Type.FREE
    });
    let ticksAmountForCreditPaid = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE11,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForCampaignFree = _.findWhere(ticksAmounts, {
      accountId: CAMPAIGN_ACCOUNT_CASE11,
      type: TicksAmount.Type.FREE
    });
    let ticksAmountForCampaignPaid = _.findWhere(ticksAmounts, {
      accountId: CAMPAIGN_ACCOUNT_CASE11,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForDebitFree = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE11,
      type: TicksAmount.Type.FREE
    });
    let ticksAmountForDebitPaid = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE11,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForSystemGeneral = _.findWhere(ticksAmounts, {accountId: Account.SystemAccount.GENERAL_ACCOUNT});
    let ticksAmountForSystemFree = _.findWhere(ticksAmounts, {accountId: Account.SystemAccount.FREE_TICKS_ACCOUNT});

    let ticksDebited = -5;
    let absTicksDebited = Math.abs(ticksDebited);

    let campaignFreeTicksPercent = absTicksDebited * config.global.campaignFreeTicksPercent;
    let campaignPaidTicksPercent = absTicksDebited * config.global.campaignTicksPercent;

    let adminCostFreeTicks = absTicksDebited * config.global.adminCostFreeTicksPercent;
    let adminCostPaidTicks = absTicksDebited * config.global.adminCostPercent;

    let creditFreeTicks = absTicksDebited - adminCostFreeTicks - campaignFreeTicksPercent;
    let creditPaidTicks = absTicksDebited - adminCostPaidTicks - campaignPaidTicksPercent;

    assert.equal(ticksAmountForCreditFree.amount, ticksDebited);
    assert.equal(ticksAmountForCreditPaid.amount, ticksDebited);

    assert.equal(ticksAmountForDebitFree.amount, creditFreeTicks);
    assert.equal(ticksAmountForDebitPaid.amount, creditPaidTicks);

    assert.equal(ticksAmountForCampaignFree.amount, campaignFreeTicksPercent);
    assert.equal(ticksAmountForCampaignPaid.amount, campaignPaidTicksPercent);

    assert.equal(ticksAmountForSystemFree.amount, adminCostFreeTicks);
    assert.equal(ticksAmountForSystemGeneral.amount, adminCostPaidTicks);

    return refundAndDiffAccounts(usedAccounts, accountsDiffMap, BUYER_ACCOUNT_CASE11, CONTENT_ID_CASE11);
  });

  it('case12: process two way transaction with debited account as shared (content price < 0)', async() => {

    let usedAccounts = [SELLER_ACCOUNT_CASE12, BUYER_ACCOUNT_CASE12, Account.SystemAccount.GENERAL_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: BUYER_ACCOUNT_CASE12,
      sellerTickserId: SELLER_ACCOUNT_CASE12,
      contentId: CONTENT_ID_CASE12,
      amount: -10,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 3);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksDebited = -10;
    let absTicksDebited = Math.abs(ticksDebited);

    let adminCost = absTicksDebited * config.global.adminCostPercent;
    let ticksCredited = absTicksDebited - adminCost;

    let ticksAmountForDebit = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE12,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForGeneral = _.findWhere(ticksAmounts, {accountId: Account.SystemAccount.GENERAL_ACCOUNT});
    let ticksAmountForCredit = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE12,
      reason: TicksAmount.Reason.DEFAULT
    });

    assertTicksAmount(ticksAmountForDebit, ticksDebited, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForGeneral, adminCost, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCredit, ticksCredited, TicksAmount.Reason.DEFAULT);

    return refundAndDiffAccounts(usedAccounts, accountsDiffMap, BUYER_ACCOUNT_CASE12, CONTENT_ID_CASE12);
  });

  it('case13: not enough money (content price < 0)', function (done: MochaDone) {
    Models.getConnection().transaction(function (t) {
      let params: TicksTransactionService.TwoWayTransactionParams = {
        buyerTickserId: 1,
        sellerTickserId: 2,
        contentId: 3,
        amount: -999999,
        type: TicksTransaction.Type.ACCESS,
        t: t
      };

      return TicksTransactionService.processTwoWayTransaction(params);
    }).then(function () {
      done(new Error('Not enough money'));
    }).catch(function () {
      done();
    });
  });

  it('case14: should not buy (content price < 0)', function (done: MochaDone) {
    Models.getConnection().transaction(function (t) {
      let params: TicksTransactionService.TwoWayTransactionParams = {
        buyerTickserId: 1,
        sellerTickserId: 1,
        contentId: 1,
        amount: -10,
        type: TicksTransaction.Type.ACCESS,
        t: t
      };

      return TicksTransactionService.processTwoWayTransaction(params);
    }).then(function () {
      done(new Error('should not buy'));
    }).catch(function () {
      done();
    });
  });

  it('case15: should not buy (content price < 0)', function (done: MochaDone) {
    Models.getConnection().transaction(function (t) {
      let params: TicksTransactionService.TwoWayTransactionParams = {
        buyerTickserId: 1,
        sellerTickserId: 2,
        campaignTickserId: 1,
        contentId: 1,
        amount: -10,
        type: TicksTransaction.Type.ACCESS,
        t: t
      };

      return TicksTransactionService.processTwoWayTransaction(params);
    }).then(function () {
      done(new Error('should not buy'));
    }).catch(function () {
      done();
    });
  });

  it('case16: process two way transaction (payed ticks) (content price = 0)', async() => {

    let usedAccounts = [SELLER_ACCOUNT_CASE16, SHARED_ACCOUNT1_CASE16, SHARED_ACCOUNT2_CASE16, BUYER_ACCOUNT_CASE16,
      Account.SystemAccount.GENERAL_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: BUYER_ACCOUNT_CASE16,
      sellerTickserId: SELLER_ACCOUNT_CASE16,
      contentId: CONTENT_ID_CASE16,
      amount: 0,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 6);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForDebit = _.findWhere(ticksAmounts, {accountId: BUYER_ACCOUNT_CASE16});
    let ticksAmountForGeneral = _.findWhere(ticksAmounts, {accountId: Account.SystemAccount.GENERAL_ACCOUNT});
    let ticksAmountForCredit = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE16,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForSharing = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE16,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });
    let ticksAmountForShared1 = _.findWhere(ticksAmounts, {accountId: SHARED_ACCOUNT1_CASE16});
    let ticksAmountForShared2 = _.findWhere(ticksAmounts, {accountId: SHARED_ACCOUNT2_CASE16});

    assertTicksAmount(ticksAmountForDebit, 0, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForGeneral, 0, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCredit, 0, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSharing, 0, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared1, 0, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared2, 0, TicksAmount.Reason.REVENUE_SHARE);

    return refundAndDiffAccounts(usedAccounts, accountsDiffMap, BUYER_ACCOUNT_CASE16, CONTENT_ID_CASE16);
  });

  it('case17: process two way transaction (payed ticks + free ticks) (content price = 0)', async() => {
    let usedAccounts = [SELLER_ACCOUNT_CASE17, SHARED_ACCOUNT1_CASE17, SHARED_ACCOUNT2_CASE17, BUYER_ACCOUNT_CASE17,
      Account.SystemAccount.GENERAL_ACCOUNT, Account.SystemAccount.FREE_TICKS_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: BUYER_ACCOUNT_CASE17,
      sellerTickserId: SELLER_ACCOUNT_CASE17,
      contentId: CONTENT_ID_CASE17,
      amount: 0,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };
    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 6);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForDebitPaid = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE17,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForSystemPaid = _.findWhere(ticksAmounts, {accountId: Account.SystemAccount.GENERAL_ACCOUNT});
    let ticksAmountForCreditPaid = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE17,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForSharingPaid = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE17,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });
    let ticksAmountForShared1Paid = _.findWhere(ticksAmounts, {
      accountId: SHARED_ACCOUNT1_CASE17,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForShared2Paid = _.findWhere(ticksAmounts, {
      accountId: SHARED_ACCOUNT2_CASE17,
      type: TicksAmount.Type.PAID
    });

    assertTicksAmount(ticksAmountForDebitPaid, 0, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSystemPaid, 0, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCreditPaid, 0, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSharingPaid, 0, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared1Paid, 0, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared2Paid, 0, TicksAmount.Reason.REVENUE_SHARE);

    return refundAndDiffAccounts(usedAccounts, accountsDiffMap, BUYER_ACCOUNT_CASE17, CONTENT_ID_CASE17);
  });

  it('case18: process two way transaction with campaign account (payed + free ticks) (content price = 0)', async() => {
    let usedAccounts = [SELLER_ACCOUNT_CASE18, CAMPAIGN_ACCOUNT_CASE18, BUYER_ACCOUNT_CASE18,
      Account.SystemAccount.GENERAL_ACCOUNT, Account.SystemAccount.FREE_TICKS_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: BUYER_ACCOUNT_CASE18,
      sellerTickserId: SELLER_ACCOUNT_CASE18,
      campaignTickserId: CAMPAIGN_ACCOUNT_CASE18,
      contentId: CONTENT_ID_CASE18,
      amount: 0,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 4);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForCreditPaid = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE18,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForCampaignPaid = _.findWhere(ticksAmounts, {
      accountId: CAMPAIGN_ACCOUNT_CASE18,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForDebitPaid = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE18,
      type: TicksAmount.Type.PAID
    });
    let ticksAmountForSystemGeneral = _.findWhere(ticksAmounts, {accountId: Account.SystemAccount.GENERAL_ACCOUNT});

    assert.equal(ticksAmountForCreditPaid.amount, 0);
    assert.equal(ticksAmountForDebitPaid.amount, 0);
    assert.equal(ticksAmountForCampaignPaid.amount, 0);
    assert.equal(ticksAmountForSystemGeneral.amount, 0);

    return refundAndDiffAccounts(usedAccounts, accountsDiffMap, BUYER_ACCOUNT_CASE18, CONTENT_ID_CASE18);
  });

  it('case19: process two way transaction with debited account as shared (content price = 0)', async() => {

    let usedAccounts = [SELLER_ACCOUNT_CASE19, SHARED_ACCOUNT_CASE19, BUYER_ACCOUNT_CASE19, Account.SystemAccount.GENERAL_ACCOUNT];

    let params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: BUYER_ACCOUNT_CASE19,
      sellerTickserId: SELLER_ACCOUNT_CASE19,
      contentId: CONTENT_ID_CASE19,
      amount: 0,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 5);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    let ticksAmountForDebit = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE19,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForGeneral = _.findWhere(ticksAmounts, {accountId: Account.SystemAccount.GENERAL_ACCOUNT});
    let ticksAmountForCredit = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE19,
      reason: TicksAmount.Reason.DEFAULT
    });
    let ticksAmountForSharing = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE19,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });
    let ticksAmountForShared = _.findWhere(ticksAmounts, {
      accountId: SHARED_ACCOUNT_CASE19,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });

    assertTicksAmount(ticksAmountForDebit, 0, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForGeneral, 0, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCredit, 0, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForSharing, 0, TicksAmount.Reason.REVENUE_SHARE);
    assertTicksAmount(ticksAmountForShared, 0, TicksAmount.Reason.REVENUE_SHARE);

    return refundAndDiffAccounts(usedAccounts, accountsDiffMap, BUYER_ACCOUNT_CASE19, CONTENT_ID_CASE19);
  });

  it('case20: process two way transaction with country vat', async() => {

    const usedAccounts = [
      BUYER_ACCOUNT_CASE20,
      SELLER_ACCOUNT_CASE20,
      Account.SystemAccount.GENERAL_ACCOUNT,
      Account.SystemAccount.VAT_GLOBAL,
      Account.SystemAccount.FREE_TICKS_ACCOUNT
    ];

    const params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: BUYER_ACCOUNT_CASE20,
      sellerTickserId: SELLER_ACCOUNT_CASE20,
      contentId: CONTENT_ID_CASE20,
      amount: 10,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 8);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    const ticksAmountForDebitFree = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE20,
      reason: TicksAmount.Reason.DEFAULT,
      type: TicksAmount.Type.FREE
    });
    const ticksAmountForGeneralFree = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.FREE_TICKS_ACCOUNT,
      reason: TicksAmount.Reason.DEFAULT,
      type: TicksAmount.Type.FREE
    });
    const ticksAmountForCreditFree = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE20,
      reason: TicksAmount.Reason.DEFAULT,
      type: TicksAmount.Type.FREE
    });
    const ticksAmountForDebitVat = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE20,
      reason: TicksAmount.Reason.VAT,
      type: TicksAmount.Type.PAID
    });
    const ticksAmountForGeneralVATGlobal = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.VAT_GLOBAL,
      reason: TicksAmount.Reason.VAT,
      type: TicksAmount.Type.PAID
    });
    const ticksAmountForDebit = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE20,
      reason: TicksAmount.Reason.DEFAULT,
      type: TicksAmount.Type.PAID
    });
    const ticksAmountForGeneral = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.GENERAL_ACCOUNT,
      reason: TicksAmount.Reason.DEFAULT,
      type: TicksAmount.Type.PAID
    });
    const ticksAmountForCreditPaid = _.findWhere(ticksAmounts, {
      accountId: SELLER_ACCOUNT_CASE20,
      reason: TicksAmount.Reason.DEFAULT,
      type: TicksAmount.Type.PAID
    });

    assertTicksAmount(ticksAmountForDebitFree, -5, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForGeneralFree, 4, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCreditFree, 1, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForDebitVat, -1, TicksAmount.Reason.VAT);
    assertTicksAmount(ticksAmountForGeneralVATGlobal, 1, TicksAmount.Reason.VAT);
    assertTicksAmount(ticksAmountForDebit, -4, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForGeneral, 0.8, TicksAmount.Reason.DEFAULT);
    assertTicksAmount(ticksAmountForCreditPaid, 3.2, TicksAmount.Reason.DEFAULT);

    await refundAndDiffAccounts(usedAccounts, accountsDiffMap, BUYER_ACCOUNT_CASE20, CONTENT_ID_CASE20);
  });

  it('case21: process two way transaction with country. but seller has no country. check that TicksAmount has null in VAT fields', async() => {

    const usedAccounts = [
      BUYER_ACCOUNT_CASE21,
      SELLER_ACCOUNT_CASE21,
      Account.SystemAccount.GENERAL_ACCOUNT,
      Account.SystemAccount.VAT_GLOBAL
    ];

    const params: TicksTransactionService.TwoWayTransactionParams = {
      buyerTickserId: BUYER_ACCOUNT_CASE21,
      sellerTickserId: SELLER_ACCOUNT_CASE21,
      contentId: CONTENT_ID_CASE21,
      amount: 10,
      type: TicksTransaction.Type.ACCESS,
      t: null
    };

    const accountsDiffMap: AccountDiffMap = {
      beforeBuy: await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'})
    };
    const result: TicksTransactionService.TwoWayTransactionResult = await TicksTransactionService.processTwoWayTransaction(params);
    await assertAmountsLengthAndTotalSum(result.ticksTransaction, 5);

    const ticksAmounts: TicksAmount.Instance[] = result.ticksTransaction.ticksAmounts;
    const ticksAmountForDebitVat = _.findWhere(ticksAmounts, {
      accountId: BUYER_ACCOUNT_CASE21,
      reason: TicksAmount.Reason.VAT,
      type: TicksAmount.Type.PAID
    });
    const ticksAmountForGeneralVATGlobal = _.findWhere(ticksAmounts, {
      accountId: Account.SystemAccount.VAT_GLOBAL,
      reason: TicksAmount.Reason.VAT,
      type: TicksAmount.Type.PAID
    });
    assert.equal(ticksAmountForDebitVat.amount, 0);
    assert.isNull(ticksAmountForDebitVat.vat);
    assert.isNull(ticksAmountForDebitVat.vatCountryId);
    assert.equal(ticksAmountForGeneralVATGlobal.amount, 0);
    assert.isNull(ticksAmountForGeneralVATGlobal.vat);
    assert.isNull(ticksAmountForGeneralVATGlobal.vatCountryId);

    await refundAndDiffAccounts(usedAccounts, accountsDiffMap, BUYER_ACCOUNT_CASE21, CONTENT_ID_CASE21);
  });

  it('calculate vat test', async() => {
    assert.equal(2, TicksTransactionService.calculateVAT(10, 0.25));
    assert.equal(1, TicksTransactionService.calculateVAT(5, 0.25));
    assert.equal(1.67, TicksTransactionService.calculateVAT(5, 0.5));
  });

  after('check accounts', function (done: MochaDone) {
    const notEqual = [];

    Account.Model.findAll({
      where: {
        id: {
          $notIn: [CREDIT_ACCOUNT_CASE5, DEBIT_ACCOUNT_CASE5] // These accounts no need to compare because they are haven't refunded
        }
      }
    })
      .then(accounts => _.forEach(accounts, a => assertOneAccount(a)))
      .then(() => assert.deepEqual(notEqual, []))
      .then(() => done())
      .catch(done);

    function assertOneAccount(account: Account.Instance): void {
      const initial: Account.Attributes = _.findWhere(accountsInitialState, {id: account.id});
      assert.isDefined(initial, 'Initial not found for: ' + account.id);
      const fieldsToCompare = [
        'tickserId',
        'totalTicksDebited',
        'totalTicksCredited',
        'totalFreeTicks',
        'currentBalance',
        'currentBalanceFreeTicks',
        'currentBalanceEarnedTicks',
        'status',
        'type'];

      _.forEach(fieldsToCompare, field => {
        if (initial[field] != account[field]) {
          notEqual.push(util.format(
            'account id [%d] field [%s] initial [%s] after [%s]',
            account.id, field, initial[field], account[field])
          );
        }
      });
    }
  });
});

async function refundAndDiffAccounts(usedAccounts: number[], accountsDiffMap: AccountDiffMap, accountId: number, contentId: number): Promise<void> {
  // const accountsAfterBuy: Account.Instance[] = await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'});

  await ContentService.refundContent(accountId, accountId, contentId);
  const accountsAfterRefund: Account.Instance[] = await Account.Model.findAllByIds(usedAccounts, {order: 'id DESC'});

  _.forEach(accountsDiffMap.beforeBuy, function (accountBeforeBuy: Account.Instance) {
    const accountAfterRefund = _.findWhere<any, Account.Instance>(accountsAfterRefund, {id: accountBeforeBuy.id});
    diffAccountsBalance(accountBeforeBuy, accountAfterRefund);
  });
}

function diffAccountsBalance(accountBeforeBuy: Account.Instance, accountAfterRefund: Account.Instance) {
  assert.equal(accountBeforeBuy.currentBalance, accountAfterRefund.currentBalance, 'currentBalance not equals Account ID: ' + accountBeforeBuy.id);
  assert.equal(accountBeforeBuy.currentBalanceEarnedTicks, accountAfterRefund.currentBalanceEarnedTicks, 'currentBalanceEarnedTicks not equals Account ID: ' + accountBeforeBuy.id);
  assert.equal(accountBeforeBuy.currentBalanceFreeTicks, accountAfterRefund.currentBalanceFreeTicks, 'currentBalanceFreeTicks not equals Account ID: ' + accountBeforeBuy.id);
  assert.equal(accountBeforeBuy.totalTicksCredited, accountAfterRefund.totalTicksCredited, 'totalTicksCredited not equals Account ID: ' + accountBeforeBuy.id);
  assert.equal(accountBeforeBuy.totalTicksDebited, accountAfterRefund.totalTicksDebited, 'totalTicksDebited not equals Account ID: ' + accountBeforeBuy.id);
  assert.equal(accountBeforeBuy.totalFreeTicks, accountAfterRefund.totalFreeTicks, 'totalFreeTicks not equals Account ID: ' + accountBeforeBuy.id);
}

async function assertAmountsLengthAndTotalSum(transaction: TicksTransaction.Instance, length: number): Promise<void> {
  assert.equal(transaction.ticksAmounts.length, length);

  const totalAmount: number = _.reduce(transaction.ticksAmounts, function (total: number, ticksAmount: TicksAmount.Instance) {
    return total + ticksAmount.amount;
  }, 0);
  assert.equal(totalAmount, 0);

  const sql = `SELECT SUM(ta.amount) as amount FROM "TicksAmount" as ta WHERE ta.ticks_transaction_id=${transaction.id} LIMIT 1;`;
  const result = await Models.getConnection().query(sql, {type: QueryTypes.SELECT});

  assert.equal(result[0].amount, 0);
}