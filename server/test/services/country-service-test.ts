/// <reference path="../../app/typings/tsd.d.ts"/>

import * as chai from 'chai';
import * as _ from 'lodash';

import * as Fixtures from '../utils-for-tests/fixtures';

import * as CountryService from '../../app/services/country-service';
import {Country} from '../../app/models/models';

const InitCountries = require('../../app/init/country');
const assert = chai.assert;

describe('CountryServiceTest', function () {

  before('init fixtures', function (done: MochaDone) {
    new Fixtures.Builder().withSyncDatabase().build()
      .then(() => {
        done();
      })
      .catch(done);
  });

  it('Service read correct data from DB', function (done) {
    CountryService.getAllCountries()
      .then(function (countries: Country.Attributes[]) {
        assert.isTrue(countries.length > 0, 'Invalid countries list length');
        assert.equal(InitCountries.length, countries.length, 'Unequal countries count');
        _.forEach(countries, c => assertCountry(c));
        done();
      })
      .catch(done);

    function assertCountry(fromDb: Country.Attributes) {
      const name: string = fromDb.name;
      const a2: string = fromDb.a2;
      const fromInit: any = _.findWhere(InitCountries, {a2: a2});

      if (fromInit == null) {
        done(new Error('Can\'t find country from init for: ' + name));
      }

      assert.equal(fromDb.name, fromInit.name, 'name for ' + name);
      assert.equal(fromDb.a2, fromInit.a2, 'a2 for ' + name);
      assert.equal(fromDb.a3, fromInit.a3, 'a3 for ' + name);
      assert.equal(fromDb.number, fromInit.number, 'number for ' + name);
      assert.equal(fromDb.dialingCode, fromInit.dialingCode, 'dialingCode for ' + name);
      // assert.equal(fromDb.currencyId, fromInit.currencyId, 'currencyId for ' + name);  -- depends on initialization order
      assert.equal(fromDb.VAT, fromInit.VAT, 'VAT for ' + name);
      assert.equal(fromDb.bookingCode, fromInit.bookingCode, 'bookingCode for ' + name);
    }
  });

  it('Assert random country. AT', async function () {
    const country: Country.Attributes = await CountryService.getValidCountry('AT');

    assert.equal(country.name, 'Austria');
    assert.equal(country.a2, 'AT');
    assert.equal(country.a3, 'AUT');
    assert.equal(country.number, 40);
    assert.equal(country.dialingCode, 43);
    // assert.equal(country.currencyId, 2);  -- depends on initialization order
    assert.equal(country.VAT, 0.20);
    assert.equal(country.bookingCode, '4286000');
  });

  it('Assert random country. US', async function () {
    const country: Country.Attributes = await CountryService.getValidCountry('US');

    assert.equal(country.name, 'United States');
    assert.equal(country.a2, 'US');
    assert.equal(country.a3, 'USA');
    assert.equal(country.number, 840);
    assert.equal(country.dialingCode, 1);
    // assert.equal(country.currencyId, 2);  -- depends on initialization order
    assert.equal(country.VAT, 0.0);
    assert.equal(country.bookingCode, '');
  });

  it('Default country should be null-filled', async function () {
    await assertThatCountryIsNull(undefined);
    await assertThatCountryIsNull(null);
    await assertThatCountryIsNull(123);
    await assertThatCountryIsNull({});
    await assertThatCountryIsNull([]);
    await assertThatCountryIsNull('EN');
    await assertThatCountryIsNull('Russia');

    async function assertThatCountryIsNull(countryCode: any): Promise<void> {
      const country: Country.Attributes = await CountryService.getValidCountry(countryCode);

      assert.isNull(country.name, 'name for ' + countryCode);
      assert.isNull(country.a2, 'a2 for ' + countryCode);
      assert.isNull(country.a3, 'a3 for ' + countryCode);
      assert.isNull(country.number, 'number for ' + countryCode);
      assert.isNull(country.dialingCode, 'dialingCode for ' + countryCode);
      assert.isNull(country.currencyId, 'currencyId for ' + countryCode);
      assert.isNull(country.VAT, 'VAT for ' + countryCode);
      assert.isNull(country.bookingCode, 'bookingCode for ' + countryCode);
    }
  });
});
