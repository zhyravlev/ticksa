/// <reference path="../../app/typings/tsd.d.ts"/>

import * as chai from 'chai';
import * as Ticksa from 'ticksa';
import * as supertest from 'supertest';

import * as InfinService from '../../app/services/infin-service';
import {CashIO, InfinCashIn} from '../../app/models/models';

import * as Fixtures from '../utils-for-tests/fixtures';

const assert = chai.assert;

describe('InfinServiceTest', function () {

  describe('UnitTest', function () {

    const unused: string = 'unused';
    const financeConfig = <Ticksa.Configuration.FinanceConfig>{
      infin: {
        mode: 'test',
        serviceName: unused,
        apiKey: unused,
        secretKey: 'test',
        url: unused
      },
      payPal: {
        paymentServerUrl: unused,
        api: {
          mode: unused,
          host: unused,
          client_id: unused,
          client_secret: unused
        }
      },
      lemonway: {
        wlLogin: unused,
        wlPass: unused,
        directKitWSDLFile: unused,
        webkitUrl: unused
      }
    };

    it('test p4 verifying', function () {
      const query: any = {
        api_key: "inf762bde77189ee858898444262efa8ed7",
        session_id: "testphone2",
        amount: "0.5",
        country_code: "de",
        infin_sn: "my game name",
        infin_pn: "100 gold",
        verb: "request_all",
        transaction_id: "6e839a79fe3755014321ffa88d0db2b5",
        infinpy_country_code: "de",
        infinpy_amount: "0.50",
        infinpy_currency: "EUR",
        infinpy_payment_type: "tan_standard_fm2",
        p3: "875c63944edcf6c900f89d3e6d34276d",
        infinpy_date_used: "20140318T083838Z",
        p4: "d686d77489989b521d9ff1ac6e119ce7"
      };

      const verificationResult = InfinService.verifyCashInCallbackP4(query, financeConfig);
      assert.equal(verificationResult.res, 'ok', 'error: ' + JSON.stringify(verificationResult));
    })
  });

  describe('IntegrationTest', function () {

    describe('Invalid callback', function () {

      before('init fixtures', function (done: MochaDone) {
        initFixtures(done);
      });

      it('Even if amount is wrong HTTP code should be 200 and status of CashIO should be PENDING', function (done: MochaDone) {
        let app = require('../../app/app');
        supertest(app)
          .get('/api/infin/callback'
          + '?amount=1.50'    // in fixtures amount is 0.50
          + '&session_id=1'
          + '&country_code=DE'
          + '&infinpy_currency=EUR'
          + '&p4=baa85696204dbbd12d49e9965472555b')
          .expect(200)
          .end(function () {
            CashIO.Model.findById(1).then(function (cashIo: CashIO.Instance) {
              assert.equal(CashIO.Status.PENDING, cashIo.status, 'CashIO Status Not Equal');
              done();
            }).catch(done)
          })
      });
    });

    describe('Valid callback', function () {

      before('init fixtures', function (done: MochaDone) {
        initFixtures(done);
      });

      it('If callback is valid HTTP code should be 200 and CashIO should have status FINAL', function (done: MochaDone) {
        let app = require('../../app/app');
        supertest(app)
          .get('/api/infin/callback'
          + '?amount=0.50'
          + '&session_id=1'
          + '&country_code=DE'
          + '&infinpy_currency=EUR'
          + '&p4=baa85696204dbbd12d49e9965472555b')
          .expect(200)
          .end(function () {
            CashIO.Model.findById(1).then(function (cashIo: CashIO.Instance) {
              assert.equal(CashIO.Status.FINAL, cashIo.status, 'CashIO Status Not Equal');
              done();
            }).catch(done)
          })
      });
    });

    function initFixtures(done: MochaDone) {
      var builder = Fixtures.Builder.defaultFixtures();

      builder.build().then(function () {
        let createCashIo = CashIO.Model.create({
          id: 1,
          accountId: 1,
          tickserId: 1,
          userCurrencyId: 2,
          userCurrencyAmount: 0.50,
          tickserPhoneNumber: '7575757575',
          phoneCountryCode: 'at',
          tickserEmail: 'test@test.com',
          countryId: 10,
          status: CashIO.Status.PENDING,
          type: CashIO.Type.CASH_IN,
          receivedAmount: null,
          receivedTicks: null,
          feesCurrencyId: null,
          feesAmount: null,
          VATAmount: null,
          NETAmount: null,
          error: null,
          exchangeRateToEuro: null,
        });

        return createCashIo.then(function (cashIO: CashIO.Instance) {
          return InfinCashIn.Model.create({
            id: 1,
            url: 'https://py-gui0903.infin-connect.de/extern/gui_ci/main.php?api_key=tic65b5df932d536cfeefda6924b3c7d880&session_id=9&amount=3&country_code=at&infin_sn=Ticksa.com&infin_an=004367693130011',
            status: InfinCashIn.Status.CREATED,
            countryCode: 'DE',
            amount: 0.50,
            cashIoId: cashIO.id,
          })
        })
      }).then(function () {
        done();
      }).catch(function (err) {
        done(err);
      });
    }
  });
});