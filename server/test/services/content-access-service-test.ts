/// <reference path="../../app/typings/tsd.d.ts"/>

const logger = require('../../app/logger/logger')(__filename);

import RecursiveIterator from '../utils-for-tests/recursive-iterator';
import * as Fixtures from '../utils-for-tests/fixtures';
import * as ContentService from '../../app/services/content-service';

describe('ContentAccessServiceTest', function () {

  before('init fixtures', function (done: MochaDone) {
    const builder = Fixtures.Builder.defaultFixtures();

    const tickserBuilder = builder.getTickserBuilder();
    tickserBuilder.getAccountBuilder().withPayedTicks(99999);

    builder.build().then(function () {
      done();
    }).catch(function (err) {
      done(err);
    });

  });

  describe('Pay and refund content', function () {
    it('pay content multi transactions', function (done: MochaDone) {
      payContentPromise().then(function() {
        done();
      }).catch(function (err) {
        done(err)
      })
    });

    it('refund content multi transactions', function (done: MochaDone) {
      refundContentPromise().then(function() {
        done();
      }).catch(function (err) {
        done(err)
      })
    })
  });
});

const geoip = {ip: '', countryA2Code: ''}; // it is enough because contents has no geographic fencing enabled

var payContentPromise = function () : Promise<any> {
  var recursiveIterator = new RecursiveIterator(5);
  recursiveIterator.setExecuteIterationsCount(10);

  recursiveIterator.on(RecursiveIterator.CREATE_ITERATION_EVENT, function (callback) {
    callback(ContentService.payContent(1, 1, 2, geoip));
  });
  recursiveIterator.on(RecursiveIterator.AFTER_EXECUTE_ITERATION_EVENT, function (callback) {
    callback(ContentService.refundContent(1, 1, 2).then(function () {
      logger.info('AFTER REFUND CONTENT');
    }));
  });

  return recursiveIterator.start();
};

var refundContentPromise = function () : Promise<any> {
  var recursiveIterator = new RecursiveIterator(5);
  recursiveIterator.setExecuteIterationsCount(10);
  recursiveIterator.on(RecursiveIterator.BEFORE_EXECUTE_ITERATION_EVENT, function (callback) {
    callback(ContentService.payContent(1, 1, 2, geoip));
  });
  recursiveIterator.on(RecursiveIterator.CREATE_ITERATION_EVENT, function (callback) {
    callback(ContentService.refundContent(1, 1, 2));
  });
  return recursiveIterator.start();
};