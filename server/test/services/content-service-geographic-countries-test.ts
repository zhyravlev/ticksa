/// <reference path="../../app/typings/tsd.d.ts"/>

import * as chai from 'chai';
import * as _ from 'lodash';

import * as Fixtures from '../utils-for-tests/fixtures';
import * as ContentService from '../../app/services/content-service';
import * as CountryService from '../../app/services/country-service';
import {Country} from '../../app/models/models';

const assert = chai.assert;

describe('ContentServiceGeographicCountriesTest', function () {

  before('init fixtures', async function () {
    await new Fixtures.Builder().withSyncDatabase().build()
  });

  it('Valid countries', async function () {
    await validCountries(undefined);
    await validCountries(null);
    await validCountries(['US']);
    await validCountries(['US', 'AT']);

    const allCountries: Country.Attributes[] = await CountryService.getAllCountries();
    const allCoutriesCodes = _.map(allCountries, (c: Country.Attributes) => c.a2);
    await validCountries(allCoutriesCodes);
    await validCountries(allCoutriesCodes.concat('US'));
    await validCountries(allCoutriesCodes.concat(allCoutriesCodes));
  });

  it('Invalid countries', async function () {
    await invalidCountries({});
    await invalidCountries(1);
    await invalidCountries([]);
    await invalidCountries('us');
    await invalidCountries(['us']);
    await invalidCountries(['US', 'at']);
    await invalidCountries(['EN']);
    await invalidCountries(['Russia']);
    await invalidCountries(['United States']);
    await invalidCountries(['US', 'SOME INVALID COUNTRY CODE']);
    await invalidCountries(['US', 'EN']);
    await invalidCountries(['US', 'RU']);  // for now there is no Russian Federation in the list of countries

    const allCountries: Country.Attributes[] = await CountryService.getAllCountries();
    const allCoutriesCodes = _.map(allCountries, (c: Country.Attributes) => c.a2);
    await invalidCountries(allCoutriesCodes.concat('RU'));   // for now there is no Russian Federation in the list of countries
    await invalidCountries(allCoutriesCodes.concat('EN'));
    await invalidCountries(allCoutriesCodes.concat(allCoutriesCodes).concat('EN'));
  });

  async function validCountries(countries): Promise<void> {
    assert.isTrue(await ContentService.areValidGeographicCountries(countries), 'For: ' + JSON.stringify(countries));
  }

  async function invalidCountries(countries): Promise<void> {
    assert.isFalse(await ContentService.areValidGeographicCountries(countries), 'For: ' + JSON.stringify(countries));
  }
});
