/// <reference path="../../app/typings/tsd.d.ts"/>

import * as util from 'util';
import * as chai from 'chai';
import * as Promise from 'bluebird';
import * as _ from 'lodash';
import * as sequelize from "sequelize";

import * as Fixtures from '../utils-for-tests/fixtures';

import * as ConditionMapper from '../../app/mappers/condition-mapper';
import * as ConditionService from "../../app/services/condition-service";
import {
  Content,
  Condition,
  ContentCondition,
  ContentAccess,
  TickserCondition
} from "../../app/models/models";
import {getConnection} from "../../app/models/models";

const assert = chai.assert;

describe('ConditionServiceTest', function () {

  const CONTENT_ID_1: number = 1;
  const CONTENT_ID_2: number = 2;
  const CONTENT_ID_3: number = 3;
  const CONTENT_ID_4: number = 4;
  const CONTENT_ID_5: number = 5;
  const CONTENT_ID_6: number = 6;
  const CONTENT_ID_7: number = 7;
  const TICKSER_ID_1: number = 1;
  const TICKSER_ID_2: number = 2;

  before('init fixtures', async function () {
    await Fixtures.Builder.defaultFixtures().build()
  });

  beforeEach('clean previously created', async function () {
    await getConnection().transaction(async function(t: sequelize.Transaction): Promise<void> {
      const options: sequelize.DestroyOptions = {transaction: t, truncate: true, cascade: true};
      await ContentAccess.Model.destroy(options);
      await ContentCondition.Model.destroy(options);
      await TickserCondition.Model.destroy(options);
      await Condition.Model.destroy(options);
    });
  });

  it('Condition linked to content', async function () {
    const condition: Condition.Instance = await createPurchasedCondition(CONTENT_ID_2);
    await linkConditionsToContent(CONTENT_ID_1, [condition]);

    await assertContentConditions(CONTENT_ID_1, [CONTENT_ID_2]);
  });

  it('Condition cant be linked to content more than once. DB constraint', async function () {
    const condition: Condition.Instance = await createPurchasedCondition(CONTENT_ID_2);
    await linkConditionsToContent(CONTENT_ID_1, [condition]);
    try {
      await ContentCondition.Model.create({
        contentId: CONTENT_ID_1,
        conditionId: condition.id
      });
      assert.fail('We should not be here. DB exception should be thrown');
    } catch (err) {
      assert.equal(err.message, 'Validation error');
      assert.equal(err.name, 'SequelizeUniqueConstraintError');
    }
  });

  it('Condition cant be linked to content more than once. Sequelize mixin is safe-to-use', async function () {
    // before
    const condition: Condition.Instance = await createPurchasedCondition(CONTENT_ID_2);
    await linkConditionsToContent(CONTENT_ID_1, [condition]);

    // when
    await linkConditionsToContent(CONTENT_ID_1, [condition]);

    // then
    assertContentConditions(CONTENT_ID_1, [CONTENT_ID_2]);
  });

  it('Conditions linked to content', async function () {
    const condition1: Condition.Instance = await createPurchasedCondition(CONTENT_ID_2);
    const condition2: Condition.Instance = await createPurchasedCondition(CONTENT_ID_3);
    await linkConditionsToContent(CONTENT_ID_1, [condition1, condition2]);

    await assertContentConditions(CONTENT_ID_1, [CONTENT_ID_2, CONTENT_ID_3]);
  });

  it('Content conditions should be queried before using ConditionService.updateContentConditions', async function () {
    // before
    const contentWithoutConditionsQueriedFromDB = await Content.Model.scope().findById(CONTENT_ID_1);

    try {

      // when
      await getConnection().transaction(async function (t) {
        return ConditionService.updateContentConditions(contentWithoutConditionsQueriedFromDB, [], t);
      });
      assert.fail('We should not be here because exception should occur')
    } catch (err) {

      // then
      assert.instanceOf(err, Error);
      assert.equal(err.message, 'Invalid server logic of querying content [' + CONTENT_ID_1 + ']');
    }
  });

  it('If same conditions added then everithing still be the same', async function () {
    // before
    const condition1: Condition.Instance = await createPurchasedCondition(CONTENT_ID_1);
    const condition2: Condition.Instance = await createPurchasedCondition(CONTENT_ID_2);
    await linkConditionsToContent(CONTENT_ID_3, [condition1, condition2]);

    // when
    const newCondition1: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_1);
    const newCondition2: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_2);
    await updateConditions(CONTENT_ID_1, [newCondition2, newCondition1, newCondition2]);

    // then
    await assertContentConditions(CONTENT_ID_1, [CONTENT_ID_1, CONTENT_ID_2]);
  });

  it('Conditions updated to content', async function () {
    // before
    const condition1: Condition.Instance = await createPurchasedCondition(CONTENT_ID_1);
    const condition2: Condition.Instance = await createPurchasedCondition(CONTENT_ID_2);
    await linkConditionsToContent(CONTENT_ID_3, [condition1, condition2]);

    // when
    const newCondition2: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_2);
    const newCondition5: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_5);
    const newCondition6: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_6);
    await updateConditions(CONTENT_ID_1, [newCondition2, newCondition5, newCondition6]);

    // then
    await assertContentConditions(CONTENT_ID_1, [CONTENT_ID_2, CONTENT_ID_5, CONTENT_ID_6]);
  });

  it('Only unique condition will be added', async function () {
    // before
    const newCondition1: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_1);
    const newCondition1_duplicate: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_1);

    // when
    await updateConditions(CONTENT_ID_2, [newCondition1, newCondition1_duplicate]);

    // then
    await assertContentConditions(CONTENT_ID_2, [CONTENT_ID_1]);
  });

  it('Content conditions can be cleared', async function () {
    // before
    const condition1: Condition.Instance = await createPurchasedCondition(CONTENT_ID_1);
    await linkConditionsToContent(CONTENT_ID_2, [condition1]);

    // when
    await updateConditions(CONTENT_ID_2, []);

    // then
    await assertContentConditions(CONTENT_ID_2, []);
  });

  it('Content conditions can be reduced', async function () {
    // before
    const condition1: Condition.Instance = await createPurchasedCondition(CONTENT_ID_1);
    const condition2: Condition.Instance = await createPurchasedCondition(CONTENT_ID_2);
    await linkConditionsToContent(CONTENT_ID_3, [condition1, condition2]);

    // when
    const newCondition1: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_1);
    await updateConditions(CONTENT_ID_3, [newCondition1]);

    // then
    await assertContentConditions(CONTENT_ID_3, [CONTENT_ID_1]);
  });

  it('When condition created tickser will be linked with such condition if he meets it', async function () {
    // before
    await tickserHasAccessTo(TICKSER_ID_1, CONTENT_ID_1);

    // when
    const newCondition1: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_1);
    await updateConditions(CONTENT_ID_2, [newCondition1]);

    // then
    await assertContentConditions(CONTENT_ID_2, [CONTENT_ID_1]);
    await assertTickserConditions(TICKSER_ID_1, [CONTENT_ID_1]);
  });

  it('When condition created tickser will not be linked with such conditions if doesnt meet', async function () {
    // before
    await tickserHasAccessTo(TICKSER_ID_1, CONTENT_ID_3);

    // when
    const newCondition1: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_1);
    await updateConditions(CONTENT_ID_3, [newCondition1]);

    // then
    await assertContentConditions(CONTENT_ID_3, [CONTENT_ID_1]);
    await assertTickserConditions(TICKSER_ID_1, []);
  });

  it('If condition already exist it will not be created. Case 1', async function () {
    // before
    await createPurchasedCondition(CONTENT_ID_1);

    // when
    const newCondition1: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_1);
    await updateConditions(CONTENT_ID_2, [newCondition1]);

    // then
    await assertConditions([CONTENT_ID_1]);
  });

  it('If condition already exist it will not be created. Case 2', async function () {
    // before
    await createPurchasedCondition(CONTENT_ID_1);

    // when
    const newCondition1: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_1);
    const newCondition2: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_2);
    await updateConditions(CONTENT_ID_3, [newCondition1, newCondition2]);

    // then
    await assertConditions([CONTENT_ID_1, CONTENT_ID_2]);
  });

  it('Complicated use-case 2', async function () {
    // before

    /* condition that was created but now it is not connected to any content */
    await createPurchasedCondition(CONTENT_ID_1);

    /* content with one condition */
    const condition2: Condition.Instance = await createPurchasedCondition(CONTENT_ID_2);
    await linkConditionsToContent(CONTENT_ID_3, [condition2]);

    /* ticksers have access to few contents*/
    await tickserHasAccessTo(TICKSER_ID_1, CONTENT_ID_4);
    await tickserHasAccessTo(TICKSER_ID_1, CONTENT_ID_5);
    await tickserHasAccessTo(TICKSER_ID_2, CONTENT_ID_4);
    await tickserHasAccessTo(TICKSER_ID_2, CONTENT_ID_6);

    // when

    const newCondition1: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_1);
    const newCondition4: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_4);
    const newCondition5: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_5);
    await updateConditions(CONTENT_ID_3, [newCondition1, newCondition4, newCondition5]);

    // then

    await assertConditions([CONTENT_ID_1, CONTENT_ID_2, CONTENT_ID_4, CONTENT_ID_5]);
    await assertContentConditions(CONTENT_ID_3, [CONTENT_ID_1, CONTENT_ID_4, CONTENT_ID_5]);
    await assertTickserConditions(TICKSER_ID_1, [CONTENT_ID_4, CONTENT_ID_5]);
    await assertTickserConditions(TICKSER_ID_2, [CONTENT_ID_4]);
  });

  it('Complicated use-case 1', async function () {

    // before

    /* condition that was created but now it is not connected to any content */
    await createPurchasedCondition(CONTENT_ID_1);

    /* content with one condition */
    const condition2: Condition.Instance = await createPurchasedCondition(CONTENT_ID_2);
    const condition3: Condition.Instance = await createPurchasedCondition(CONTENT_ID_3);
    await linkConditionsToContent(CONTENT_ID_4, [condition2, condition3]);

    /* ticksers has access to few contents*/
    await tickserHasAccessTo(TICKSER_ID_1, CONTENT_ID_5);
    await tickserHasAccessTo(TICKSER_ID_1, CONTENT_ID_6);
    await tickserHasAccessTo(TICKSER_ID_2, CONTENT_ID_5);
    await tickserHasAccessTo(TICKSER_ID_2, CONTENT_ID_6);
    await tickserHasAccessTo(TICKSER_ID_2, CONTENT_ID_7);

    // when

    const newCondition1: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_1);
    const newCondition2: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_2);
    const newCondition5: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_5);
    const newCondition7: ConditionMapper.IConditionRequest = purchaseConditionRequest(CONTENT_ID_7);
    await updateConditions(CONTENT_ID_4, [newCondition1, newCondition2, newCondition5, newCondition7]);

    // then

    await assertConditions([CONTENT_ID_1, CONTENT_ID_2, CONTENT_ID_3, CONTENT_ID_5, CONTENT_ID_7]);
    await assertContentConditions(CONTENT_ID_4, [CONTENT_ID_1, CONTENT_ID_2, CONTENT_ID_5, CONTENT_ID_7]);
    await assertTickserConditions(TICKSER_ID_1, [CONTENT_ID_5]);
    await assertTickserConditions(TICKSER_ID_2, [CONTENT_ID_5, CONTENT_ID_7]);
  });

  function purchaseConditionRequest(contentId: number): ConditionMapper.IConditionRequest {
    return {
      type: Condition.Type.PURCHASE,
      content: {
        id: contentId
      }
    }
  }

  async function updateConditions(contentId: number, conditions: ConditionMapper.IConditionRequest[]): Promise<void> {
    return getConnection().transaction(async function (t) {
      const content = await Content.Model.scope(Content.Scopes.INCLUDE_CONDITIONS).findById(contentId, {transaction: t});
      return ConditionService.updateContentConditions(content, conditions, t);
    });
  }

  async function assertContentConditions(contentId: number, contentInsideConditions: number[]): Promise<void> {
    contentInsideConditions = contentInsideConditions || [];

    const content = await Content.Model.scope(Content.Scopes.INCLUDE_CONDITIONS).findById(contentId);
    assert.equal(
      content.conditions.length,
      contentInsideConditions.length,
      util.format('Content conditions length - contentId [%d], got: %s', contentId,
        util.inspect(_.map(content.conditions, c => {
          return {type: c.value.type, contentId: c.value.contentId}
        }))
      )
    );

    const errors = [];
    _.forEach(contentInsideConditions, cId => {
      const found: Condition.Instance = _.chain(content.conditions)
        .filter((c: Condition.Instance) => c.value.contentId == cId)
        .first();
      if (found == null) {
        errors.push(util.format('Condition {type: %d, contentId: %d} not found for content [%d]',
          Condition.Type.PURCHASE, cId, contentId
        ));
      }
    });
    assert.deepEqual(errors, []);
  }

  async function assertTickserConditions(tickserId: number, contentInsideConditions: number[]): Promise<void> {
    contentInsideConditions = contentInsideConditions || [];

    const tickserConditions = await TickserCondition.Model.scope(TickserCondition.Scopes.INCLUDE_CONDITION).findAll({
      where: {
        tickserId: tickserId
      }
    });
    assert.equal(
      tickserConditions.length,
      contentInsideConditions.length,
      util.format('Tickser conditions length - tickserId [%d], got: %s', tickserId,
        util.inspect(_.map(tickserConditions, tc => {
          return {type: tc.condition.value.type, contentId: tc.condition.value.contentId};
        }))
      )
    );

    const errors = [];
    _.forEach(contentInsideConditions, cId => {
      const found: TickserCondition.Instance = _.chain(tickserConditions)
        .filter((tc: TickserCondition.Instance) => tc.condition.value.contentId == cId)
        .first();
      if (found == null) {
        errors.push(util.format('Condition {type: %d, contentId: %d} not found for tickser [%d]',
          Condition.Type.PURCHASE, cId, tickserId
        ));
      }
    });
    assert.deepEqual(errors, []);
  }

  async function assertConditions(contentInsideConditions: number[]): Promise<void> {
    contentInsideConditions = contentInsideConditions || [];

    const conditions = await Condition.Model.findAll();
    assert.equal(
      conditions.length,
      contentInsideConditions.length,
      util.format('Conditions length - got: %s',
        util.inspect(_.map(conditions, c => {
          return {type: c.value.type, contentId: c.value.contentId};
        }))
      )
    );

    const errors = [];
    _.forEach(contentInsideConditions, cId => {
      const found: TickserCondition.Instance = _.chain(conditions)
        .filter((c: Condition.Instance) => c.value.contentId == cId)
        .first();
      if (found == null) {
        errors.push(util.format('Condition {type: %d, contentId: %d} not found',
          Condition.Type.PURCHASE, cId
        ));
      }
    });
    assert.deepEqual(errors, []);
  }

  async function createPurchasedCondition(contentId: number): Promise<Condition.Instance> {
    return Condition.Model.create({
      value: {
        type: Condition.Type.PURCHASE,
        contentId: contentId
      }
    });
  }

  async function linkConditionsToContent(contentId: number, conditions: Condition.Instance[]): Promise<void> {
    const condition = await Content.Model.scope(Content.Scopes.INCLUDE_CONDITIONS).findById(contentId);
    return condition.addConditions(conditions);
  }

  async function tickserHasAccessTo(tickserAccountId: number, contentId: number): Promise<ContentAccess.Instance> {
    return ContentAccess.Model.create({
      contentId: contentId,
      accountId: tickserAccountId,
      hasAccess: true
    });
  }

});
