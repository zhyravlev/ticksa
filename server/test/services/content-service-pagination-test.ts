/// <reference path="../../app/typings/tsd.d.ts"/>

import * as chai from 'chai';
import * as Promise from 'bluebird';
import * as _ from 'lodash';
import * as Ticksa from 'ticksa';

import * as Fixtures from '../utils-for-tests/fixtures';

import * as ContentService from '../../app/services/content-service';
import {ContentFilter} from "../../app/utils/request-utils";

import {Content}  from '../../app/models/models';

const assert = chai.assert;

function buildFixtures(withPositionInDiscovery: boolean, done: MochaDone) {
  var builder = Fixtures.Builder.defaultFixtures();
  var tickserBuilder = builder.getTickserBuilder().withCount(20);

  var contentBuilder = tickserBuilder.getContentBuilder();
  var channelBuilder = tickserBuilder.getChannelBuilder();

  if (withPositionInDiscovery) {
    contentBuilder.withPositionInDiscovery(1, [4]);
  }

  var randomPosition = 100;
  let identicalPosition = 200;

  contentBuilder.withRandomPosition(randomPosition++, [1]);
  contentBuilder.withRandomPosition(identicalPosition, [2]);
  contentBuilder.withRandomPosition(randomPosition++, [3]);
  contentBuilder.withRandomPosition(randomPosition++, [4]);
  contentBuilder.withRandomPosition(randomPosition++, [5]);
  contentBuilder.withRandomPosition(randomPosition++, [6]);
  contentBuilder.withRandomPosition(randomPosition++, [10]);
  contentBuilder.withRandomPosition(randomPosition++, [11]);
  contentBuilder.withRandomPosition(randomPosition++, [12]);
  contentBuilder.withRandomPosition(randomPosition++, [7]);
  contentBuilder.withRandomPosition(randomPosition++, [8]);
  contentBuilder.withRandomPosition(identicalPosition, [9]);
  contentBuilder.withRandomPosition(randomPosition++, [13]);
  contentBuilder.withRandomPosition(randomPosition++, [14]);
  contentBuilder.withRandomPosition(identicalPosition, [15]);
  contentBuilder.withRandomPosition(randomPosition++, [16]);
  contentBuilder.withRandomPosition(randomPosition++, [17]);
  contentBuilder.withRandomPosition(randomPosition++, [18]);
  contentBuilder.withRandomPosition(randomPosition++, [19]);
  contentBuilder.withRandomPosition(randomPosition++, [20]);

  contentBuilder.withPublished(false, [5]);
  contentBuilder.withStatus(Content.Status.DEACTIVATED, [10]);
  contentBuilder.withTitle('TITLE', [10, 12]);

  channelBuilder.withName('TITLE', [20]);

  builder.build().then(function () {
    done();
  }).catch(done);
}

describe('ContentPaginationTest', function () {

  describe('ContentPaginationTest', function () {
    before('init fixtures', function (done: MochaDone) {
      buildFixtures(true, done);
    });

    it('search content with PositionInDiscovery', function (done: MochaDone) {
      var findAllContents = findIds(20, 0, null);
      var findFirstPageContents = findIds(5, 0, null);
      var findOffset1Contents = findIds(5, 5, null);
      var findOffset2Contents = findIds(5, 10, null);
      var findOffset3Contents = findIds(5, 15, null);
      var findOffset4Contents = findIds(5, 20, null);

      var findAllWithSearchContents = findIds(20, 0, 'TITLE');

      Promise.all([findAllContents, findFirstPageContents, findOffset1Contents,
        findOffset2Contents, findOffset3Contents, findOffset4Contents, findAllWithSearchContents])
        .spread(function (allContents: number[], firstPageContents: number[], offset1Contents: number[],
                          offset2Contents: number[], offset3Contents: number[], offset4Contents: number[],
                          witchSearch: number[]) {

          assert.deepEqual([4, 20, 19, 18, 17, 16, 1, 3, 6, 11, 12, 7, 8, 13, 14, 2, 9, 15], allContents);
          assert.deepEqual([4, 20, 19, 18, 17], firstPageContents);
          assert.deepEqual([16, 1, 3, 6, 11], offset1Contents);
          assert.deepEqual([12, 7, 8, 13, 14], offset2Contents);
          assert.deepEqual([2, 9, 15], offset3Contents);
          assert.deepEqual([20, 12], witchSearch);
          assert.deepEqual(_.intersection(firstPageContents, offset1Contents), []);
          assert.deepEqual(_.intersection(firstPageContents, offset2Contents), []);
          assert.deepEqual(_.intersection(firstPageContents, offset3Contents), []);
          assert.deepEqual(_.intersection(offset1Contents, offset2Contents), []);
          assert.deepEqual(_.intersection(offset1Contents, offset3Contents), []);
          assert.deepEqual(_.intersection(offset2Contents, offset3Contents), []);

          done();
        }).catch(function (err) {
          done(err);
        });
    });
  });

  describe('ContentPaginationTest 2', function () {
    before('init fixtures', function (done: MochaDone) {
      buildFixtures(false, done);
    });

    it('search content without PositionInDiscovery', function (done: MochaDone) {
      var findAllContents = findIds(20, 0, null);
      var findFirstPageContents = findIds(10, 0, null);
      var findOffsetContents = findIds(10, 10, null);

      Promise.all([findAllContents, findFirstPageContents, findOffsetContents])
        .spread(function (allContents: number[], firstPageContents: number[], offsetContents: number[]) {
          assert.deepEqual([20, 19, 18, 17, 16, 1, 3, 4, 6, 11, 12, 7, 8, 13, 14, 2, 9, 15], allContents);
          assert.deepEqual([20, 19, 18, 17, 16, 1, 3, 4, 6, 11], firstPageContents);
          assert.deepEqual([12, 7, 8, 13, 14, 2, 9, 15], offsetContents);
          done();
        }).catch(function (err) {
          done(err);
        });
    });
  });

  function findIds(limit: number, offset: number, searchString: any) {
    const userMeta: ContentService.UserMeta = {
      tickserId: null,
      countryA2Code: null
    };
    return ContentService.findContentIds(pageable(limit, offset, searchString), userMeta);
  }

  function pageable(limit: number, offset: number, searchString: string): Ticksa.IPageable<ContentFilter> {
    return {
      limit: limit,
      offset: offset,
      searchString: searchString,
      filter: {
        categories: [],
        languages: []
      }
    };
  }
});
