/// <reference path="../../app/typings/tsd.d.ts"/>

import {Parser} from '../../app/services/sharing-service';
import {assert} from 'chai';

describe('SocialSharingServiceTest', function () {

  it('test positive', function () {

    checkTrue('/teaser/1/3?referrerId=7', 3);
    checkTrue('%2Fteaser%2F1%2F3%3FreferrerId%3D7', 3);

    checkTrue('/teaser/1/4', 4);
    checkTrue('%2Fteaser%2F1%2F4', 4);

    checkTrue('/channels/1/contents/2', 2);
    checkTrue('%2Fchannels%2F1%2Fcontents%2F2', 2);
  });

  it('test nevative', function () {
    checkFalse(null);
    checkFalse(undefined);
    checkFalse('');
    checkFalse('sgasf');
    checkFalse('%2Fchannels%2F1%2Fcontents%2F');
    checkFalse('%2Fteaser%2F1%2F33FreferrerId%3D7');
    checkFalse('%2Fteaser%2F%2F33FreferrerId%3D7');
    checkFalse('%2Fteaser%2F1%2F');
    checkFalse('%2Fteaser%2F12F4');
    checkFalse('%2teaser%2F1%2F4');
  });

  function checkTrue(escapedFragment: string, contentId: number) {
    assert.deepEqual(
      Parser.isQueryForContent(escapedFragment),
      {isSuitable: true, data: {contentId: contentId}},
      'Fragment: ' + escapedFragment
    );
  }

  function checkFalse(escapedFragment: string) {
    assert.deepEqual(
      Parser.isQueryForContent(escapedFragment),
      {isSuitable: false},
      'Fragment: ' + escapedFragment
    );
  }

});