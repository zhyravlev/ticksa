/// <reference path="../../app/typings/tsd.d.ts"/>

import * as chai from 'chai';
import * as _ from 'lodash';

import * as Fixtures from '../utils-for-tests/fixtures';

import * as ContentService from '../../app/services/content-service';
import {Content} from '../../app/models/models';

const assert = chai.assert;

describe('ChannelServiceTest', function () {

  before('init fixtures', function (done: MochaDone) {
    new Fixtures.Builder().withSyncDatabase().build()
      .then(() => done())
      .catch(done);
  });

  it('sort by content ids', function (done) {

    var contents: Content.Instance[] = [
      Content.Model.build({id: 1, title: 'h'}),
      Content.Model.build({id: 2, title: 'g'}),
      Content.Model.build({id: 3, title: 'f'}),
      Content.Model.build({id: 4, title: 'e'}),
      Content.Model.build({id: 5, title: 'd'}),
      Content.Model.build({id: 6, title: 'c'}),
      Content.Model.build({id: 7, title: 'b'}),
      Content.Model.build({id: 8, title: 'a'}),
    ];

    var sortedByTitle = _.sortBy(contents, 'title');
    var sorted = ContentService.sortContentBySortedIds(sortedByTitle, [4, 3, 2, 1]);
    assert.deepEqual([4, 3, 2, 1, 8, 7, 6, 5], _.pluck(sorted, 'id'));

    sorted = ContentService.sortContentBySortedIds(sortedByTitle, [66, 42, 8, 6, 4, 2, 0, 8684, 157]);
    assert.deepEqual([8, 6, 4, 2, 7, 5, 3, 1], _.pluck(sorted, 'id'));

    sorted = ContentService.sortContentBySortedIds(sortedByTitle, null);
    assert.deepEqual([8, 7, 6, 5, 4, 3, 2, 1], _.pluck(sorted, 'id'));

    sorted = ContentService.sortContentBySortedIds(sortedByTitle, []);
    assert.deepEqual([8, 7, 6, 5, 4, 3, 2, 1], _.pluck(sorted, 'id'));

    done();
  })
});
