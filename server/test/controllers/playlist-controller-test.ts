/// <reference path="../../app/typings/tsd.d.ts"/>

import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';
import * as SecurityService from '../../app/services/security-service';
import * as PlaylistMessages from '../../app/dto/playlist/playlist-messages';
import {assert} from 'chai';
import {Playlist, Content} from '../../app/models/models';

describe('PlaylistControllerTest', function () {
  const DEACTIVATED_PLAYLIST_ID = 3;

  let app;

  before('init fixtures', async function () {
    const builder = Fixtures.Builder.defaultFixtures();
    const playlistBuilder = builder.getTickserBuilder().getPlaylistBuilder();
    playlistBuilder.withPublished(false, [DEACTIVATED_PLAYLIST_ID]);
    await builder.build();
    app = builder.getApp();
  });

  it('GET /user/playlists', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/user/playlists')
      .set('token', token)
      .send()
      .expect(res => {
        assert.equal(res.status, 200);
        assert.equal(res.text, '[{"id":1,"name":"tickser1`s playlist","coverImage":null}]');
      })
    ;
  });

  it('POST /user/playlists', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');
    const request: PlaylistMessages.IPlaylistUserPostRequest = {
      name: 'name',
      backgroundImageId: null,
      coverImageId: null,
      published: true,
      contentsIds: [1]
    };

    await supertest(app)
      .post('/api/user/playlists')
      .set('token', token)
      .send(request)
      .expect(res => {
        assert.equal(res.status, 200);
        assert.isNotNull(res.body.id);
        assert.isNumber(res.body.id);
      })
    ;
  });

  it('POST /user/playlists  Error because of incorrect contentId', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');
    const request: PlaylistMessages.IPlaylistUserPostRequest = {
      name: 'name',
      backgroundImageId: null,
      coverImageId: null,
      published: true,
      contentsIds: [-1]
    };

    await supertest(app)
      .post('/api/user/playlists')
      .set('token', token)
      .send(request)
      .expect(400)
    ;
  });

  it('GET /user/playlists/1', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');
    await supertest(app)
      .get('/api/user/playlists/1')
      .set('token', token)
      .send()
      .expect(res => {
        assert.equal(res.status, 200);
        assert.equal(res.text, '{"id":1,"name":"tickser1`s playlist","coverImage":null,"backgroundImage":null,"contents":[],"published":true}');
      })
    ;
  });

  it('GET /user/playlists/1 Error because not an owner', async() => {
    const token = SecurityService.generateToken(2, 'tickser2');
    await supertest(app)
      .get('/api/user/playlists/1')
      .set('token', token)
      .send()
      .expect(403)
    ;
  });

  it('POST then GET', async() => {
    const token = SecurityService.generateToken(2, 'tickser2');
    const request: PlaylistMessages.IPlaylistUserPostRequest = {
      name: 'name',
      backgroundImageId: null,
      coverImageId: null,
      published: true,
      contentsIds: [1]
    };

    const response: any = await supertest(app)
        .post('/api/user/playlists')
        .set('token', token)
        .send(request)
        .expect(200)
      ;
    const newPlaylistId = response.body.id;

    const playlist: Playlist.Instance = await Playlist.Model.findOne({
      order: [['id', 'DESC']]
    });

    await supertest(app)
      .get('/api/user/playlists')
      .set('token', token)
      .send()
      .expect(res => {
        assert.equal(res.status, 200);
        assert.equal(
          res.text,
          '[' +
          '{"id":2,"name":"tickser2`s playlist","coverImage":null},' +
          '{"id":' + newPlaylistId + ',"name":"name","coverImage":null}' +
          ']'
        );
      })
    ;

    await supertest(app)
      .get('/api/user/playlists/' + playlist.id)
      .set('token', token)
      .send()
      .expect(res => {
        assert.equal(res.status, 200);
        assertThatContainsExpectedData(res.body, false);
      })
    ;
  });

  it('PUT /user/playlists/1', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');
    const request: PlaylistMessages.IPlaylistUserPostRequest = {
      name: 'name',
      backgroundImageId: null,
      coverImageId: null,
      published: true,
      contentsIds: [1]
    };

    await supertest(app)
      .put('/api/user/playlists/1')
      .set('token', token)
      .send(request)
      .expect(200);

    await supertest(app)
      .get('/api/user/playlists/1')
      .set('token', token)
      .send()
      .expect(res => {
        assert.equal(res.status, 200);
        assertThatContainsExpectedData(res.body, false);
      })
    ;
  });

  it('PUT /user/playlists/1 Error because not an owner', async() => {
    const token = SecurityService.generateToken(2, 'tickser2');
    const request: PlaylistMessages.IPlaylistUserPostRequest = {
      name: 'name',
      backgroundImageId: null,
      coverImageId: null,
      published: true,
      contentsIds: [1]
    };

    await supertest(app)
      .put('/api/user/playlists/1')
      .set('token', token)
      .send(request)
      .expect(403);
  });

  it('GET /playlists/3 For yours but deactivated playlist', async() => {
    const token = SecurityService.generateToken(3, 'tickser3');
    await supertest(app)
      .get('/api/playlists/3')
      .set('token', token)
      .send()
      .expect(res => {
        assert.equal(res.status, 404);
        assert.equal(res.body.message, 'Playlist not found');
        assert.equal(res.body.name, 'NotFound');
      });
  });

  it('GET /playlists/4 For yours active playlist', async() => {
    const token = SecurityService.generateToken(4, 'tickser4');
    await supertest(app)
      .get('/api/playlists/4')
      .set('token', token)
      .send()
      .expect(res => {
        assert.equal(res.status, 200);
        assert.equal(res.body.name, 'tickser4`s playlist');
        assert.isNull(res.body.backgroundImage);
        assert.deepEqual(res.body.contents, []);
        assert.isNull(res.body.coverImage);
      });
  });

  it('GET /playlists/3 For not yours deactivated playlist', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');
    await supertest(app)
      .get('/api/playlists/3')
      .set('token', token)
      .send()
      .expect(res => {
        assert.equal(res.status, 404);
        assert.equal(res.body.message, 'Playlist not found');
        assert.equal(res.body.name, 'NotFound');
      });
  });

  it('GET /playlists/4 For not yours active playlist', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');
    await supertest(app)
      .get('/api/playlists/4')
      .set('token', token)
      .send()
      .expect(res => {
        assert.equal(res.status, 200);
        assert.equal(res.body.name, 'tickser4`s playlist');
        assert.isNull(res.body.backgroundImage);
        assert.deepEqual(res.body.contents, []);
        assert.isNull(res.body.coverImage);
      });
  });

  it('GET /playlists/4 For not yours active playlist where you own content', async() => {
    const request: PlaylistMessages.IPlaylistUserPostRequest = {
      name: 'name',
      backgroundImageId: null,
      coverImageId: null,
      published: true,
      contentsIds: [1]
    };
    await supertest(app)
      .put('/api/user/playlists/4')
      .set('token', SecurityService.generateToken(4, 'tickser4'))
      .send(request);

    await supertest(app)
      .get('/api/playlists/4')
      .set('token', SecurityService.generateToken(1, 'tickser1'))
      .send()
      .expect(res => {
        assert.equal(res.status, 200);
        assertThatContainsExpectedData(res.body, true);
      });
  });

  it('DELETE /user/playlists/5', async() => {
    const token = SecurityService.generateToken(5, 'tickser5');
    await supertest(app)
      .delete('/api/user/playlists/5')
      .set('token', token)
      .send()
      .expect(200);
  });

  it('DELETE /user/playlists/1 Error because not an owner', async() => {
    const token = SecurityService.generateToken(5, 'tickser5');
    await supertest(app)
      .delete('/api/user/playlists/1')
      .set('token', token)
      .send()
      .expect(403);
  });

  function assertThatContainsExpectedData(body: any, isManagerOfContent: boolean) {
    assert.equal(body.name, 'name');
    assert.isNull(body.coverImage);
    assert.isNull(body.backgroundImage);
    assert.isArray(body.contents);
    assert.equal(body.contents.length, 1);

    const content: any = body.contents[0];
    assert.equal(content.id, 1);
    assert.equal(content.title, 'tickser1`s content');
    assert.equal(content.type, Content.Type.TEXT);
    assert.equal(content.likes, 0);
    assert.equal(content.dislikes, 0);
    assert.equal(content.tickPrice, 1);
    assert.isNull(content.textLength);
    assert.isNull(content.imageCount);
    assert.isNull(content.videoLength);
    assert.equal(content.bought, false);
    assert.equal(content.manager, isManagerOfContent);
    assert.equal(content.published, true);
    assert.equal(content.haveCondition, false);
    assert.equal(content.haveGeographicLimited, false);
    assert.equal(content.channel.id, 1);
    assert.equal(content.channel.name, 'tickser1`s channel');
    assert.equal(content.channel.verified, false);
    assert.isNull(content.channel.coverImage);
    assert.isNull(content.channel.background);
    assert.deepEqual(content.tags, []);
    assert.isNull(content.teaserText);
    assert.isNull(content.teaserImage);
    assert.isNull(content.teaserVideo);
  }

});