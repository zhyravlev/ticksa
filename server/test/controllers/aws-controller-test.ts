/// <reference path="../../app/typings/tsd.d.ts"/>

import * as supertest from 'supertest-as-promised';
import * as SecurityService from '../../app/services/security-service';
import * as AwsService from '../../app/services/aws';
import * as Fixtures from '../utils-for-tests/fixtures';

describe('AwsControllerTest', function () {

  let app;

  before('init', async() => {
    const builder = Fixtures.Builder.defaultFixtures();

    await builder.build();
    app = builder.getApp();
  });

  it('GET /aws/generate-signature?to_sign', async() => {
    const toSign = 'params';
    const expectedSignature = AwsService.generateSignature(toSign);

    const response = await supertest(app)
      .get('/api/aws/generate-signature?to_sign=' + toSign)
      .set('token', SecurityService.generateToken(1, 'tickser1'))
      .expect(200, expectedSignature);
  });

});
