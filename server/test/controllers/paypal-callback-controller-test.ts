/// <reference path="../../app/typings/tsd.d.ts"/>

import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';

describe('PaypalControllerTest', function () {

  let app;
  before('init fixtures', async function () {
    const builder = Fixtures.Builder.defaultFixtures();
    await builder.build();
    app = builder.getApp();
  });

  it('GET /paypal/complete/:paypalCashInId', async() => {
    await supertest(app)
      .get('/api/paypal/complete/1?paymentId=1&token=token&PayerID=1')
      .expect(302)
  });

  it('GET /paypal/cancel/:paypalCashInId', async() => {
    await supertest(app)
      .get('/api/paypal/cancel/1?token=token')
      .expect(302)
  });

});