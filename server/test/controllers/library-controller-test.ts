/// <reference path="../../app/typings/tsd.d.ts"/>
import * as chai from 'chai';
import * as supertest from 'supertest-as-promised';
import * as SecurityService from '../../app/services/security-service';
import Fixtures = require('../utils-for-tests/fixtures');
import {Image} from '../../app/models/models';

const assert = chai.assert;

describe('LibraryControllerTest', function () {

  let app;

  before('init fixtures', async () => {
    const builder = Fixtures.Builder.defaultFixtures();
    const accountBuilder = builder.getTickserBuilder().getAccountBuilder();
    accountBuilder.withPayedTicks(100, [1]);

    await builder.build();
    app = builder.getApp();


  });

  it('should be got library', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/library/media')
      .set('token', token)
      .expect(200);
  });

  it('should be upload multiple', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    const images = [{
      name: 'testImage1',
      file: {
        name: 'testImageFile1',
        key: 'key1',
        size: 1345357,
        isUploaded: true
      }
    }, {
      name: 'testImage2',
      file: {
        name: 'testImageFile2',
        key: 'key2',
        size: 1345356,
        isUploaded: true
      }
    }];

    await supertest(app)
      .post('/api/library/images-multiple')
      .set('token', token)
      .send(images)
      .expect(200);
  });

  it('should be got images', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/library/images')
      .set('token', token)
      .expect(200);
  });

  it('should be delete image', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');
    await createImage(token);
    await supertest(app)
      .delete('/api/library/images/1')
      .set('token', token)
      .expect(200);
  });

  //TODO: Need AWS service mock
  //it('should be upload and got video', async () => {
  //  const token = SecurityService.generateToken(1, 'tickser1');
  //
  //  const video: Image.Attributes = {
  //    name: 'testVideo',
  //    file: {
  //      name: 'testVideoFile',
  //      key: 'testVideoKey',
  //      size: 135354535,
  //      isUploaded: true
  //    }
  //  };
  //
  //  await supertest(app)
  //    .post('/api/library/videos')
  //    .set('token', token)
  //    .send(video)
  //    .expect(200);
  //
  //  await supertest(app)
  //    .get('/api/library/videos/1')
  //    .set('token', token)
  //    .expect(200);
  //});

  it('should be got videos', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/library/videos')
      .set('token', token)
      .expect(200);
  });

  async function createImage (token) {
    const image = {
      name: 'testImage',
      file: {
        name: 'testImageFile',
        key: 'key',
        size: 134535,
        isUploaded: true
      }
    };

    await
      supertest(app)
        .post('/api/library/images')
        .set('token', token)
        .send(image)
        .expect(200);
  };

});


