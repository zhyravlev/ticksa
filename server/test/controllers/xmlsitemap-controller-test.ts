/// <reference path="../../app/typings/tsd.d.ts"/>

import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';
import * as UtilService from '../../app/services/utilities';
import SharingTestUtils from '../utils-for-tests/sharing-test-utils';
import {Response} from 'supertest-as-promised';
import {assert} from 'chai';

describe('XmlSitemapControllerTest', function () {

  let app;
  before('init fixtures', async() => {
    const builder = Fixtures.Builder.defaultFixtures();
    await builder.build();
    app = builder.getApp();
  });

  describe('as browser', () => {

    it('GET /q/:encodedData', async() => {
      const contentId = UtilService.encodeNumberToRadix64(1);

      await supertest(app)
        .get('/q/' + contentId)
        .expect(302)
        .expect('Location', '/#!/teaser/1/1')
      ;
    });
  });

  describe('as social network crawler', () => {

    it('GET /q/:encodedData', async() => {
      const contentId = UtilService.encodeNumberToRadix64(1);

      await SharingTestUtils.checkThatResponseIsMarkupForSocialNetworkCrawler(
        supertest(app)
          .get('/q/' + contentId)
          .set('user-agent', SharingTestUtils.SOCIAL_NETWORK_CRAWLER_USER_AGENT),
        'tickser1`s content'
      );
    });

  });

  describe('as search engine crawler', () => {

    it('GET /q/:encodedData', async() => {
      const contentId = UtilService.encodeNumberToRadix64(1);

      await SharingTestUtils.checkThatResponseIsMarkupForSearchEngineCrawler(
        supertest(app)
          .get('/q/' + contentId)
          .set('user-agent', SharingTestUtils.SEARCH_ENGINE_CRAWLER_USER_AGENT),
        'tickser1`s content',
        'tickser1`s channel'
      );
    });

  });

  describe('/sitemap.xml', () => {

    it('/sitemap.xml', async() => {
      await supertest(app)
        .get('/sitemap.xml')
        .set('user-agent', SharingTestUtils.SEARCH_ENGINE_CRAWLER_USER_AGENT)
        .expect(200)
        .expect('Content-Type', /application\/xml/)
        .expect(function (res: Response) {
          assert.isTrue(res.text.indexOf('<?xml version="1.0" encoding="UTF-8"?>') > -1, 'Check xml start');
          assert.isTrue(res.text.indexOf('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">') > -1, 'Check sitemap tag');
          assert.isTrue(res.text.indexOf('<url><loc>http://localhost:3000/q/A</loc>') > -1, 'Check 10th content');
          assert.isTrue(res.text.indexOf('<url><loc>http://localhost:3000/q/9</loc>') > -1, 'Check 9th content');
          assert.isTrue(res.text.indexOf('<url><loc>http://localhost:3000/q/8</loc>') > -1, 'Check 8th content');
          assert.isTrue(res.text.indexOf('<url><loc>http://localhost:3000/q/7</loc>') > -1, 'Check 7th content');
          assert.isTrue(res.text.indexOf('<url><loc>http://localhost:3000/q/6</loc>') > -1, 'Check 6th content');
          assert.isTrue(res.text.indexOf('<url><loc>http://localhost:3000/q/5</loc>') > -1, 'Check 5th content');
          assert.isTrue(res.text.indexOf('<url><loc>http://localhost:3000/q/4</loc>') > -1, 'Check 4th content');
          assert.isTrue(res.text.indexOf('<url><loc>http://localhost:3000/q/3</loc>') > -1, 'Check 3th content');
          assert.isTrue(res.text.indexOf('<url><loc>http://localhost:3000/q/2</loc>') > -1, 'Check 2th content');
          assert.isTrue(res.text.indexOf('<url><loc>http://localhost:3000/q/1</loc>') > -1, 'Check 1th content');
        });
    });

  });

});
