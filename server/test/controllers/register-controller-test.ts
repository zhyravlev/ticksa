/// <reference path="../../app/typings/tsd.d.ts"/>

import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';
import {Tickser} from '../../app/models/models';

describe('RegisterControllerTest', function () {

  let app;
  let builder;
  let tickserBuilder;

  before('init fixtures', async () => {
    builder = Fixtures.Builder.defaultFixtures();
    tickserBuilder = builder.getTickserBuilder();

    tickserBuilder.withConfirmationUrl('confirmation-url', [1]);
    tickserBuilder.withStatus(Tickser.Status.NEW, [1]);
    tickserBuilder.withoutChannelTicksers([1]);
    tickserBuilder.withoutAccountTicksers([1]);

    tickserBuilder.withStatus(Tickser.Status.NEW, [2]);

    tickserBuilder.withChangePasswordUrl('change-password-url', [4]);

    await builder.build();
    app = builder.getApp();
  });

  it('GET /registrations/check-email', async () => {
    await supertest(app)
      .get('/api/registrations/check-email?email=' + tickserBuilder.getUsernameByTickser(1))
      .expect(200);
  });

  it('GET /registrations/confirm', async () => {
    await supertest(app)
      .get('/api/registrations/confirm?url=confirmation-url')
      .expect(302);
  });

  it('GET /registrations/resend', async () => {
    await supertest(app)
      .get('/api/registrations/resend?username=tickser2')
      .expect(200);
  });

  it('POST /registrations/forgot-password', async () => {
    await supertest(app)
      .post('/api/registrations/forgot-password')
      .send({email: tickserBuilder.getEmailByTickser(3)})
      .expect(200);
  });

  it('GET /registrations/new-password', async () => {
    await supertest(app)
      .post('/api/registrations/new-password')
      .send({url: 'change-password-url', password: 'password'})
      .expect(200);
  });

});