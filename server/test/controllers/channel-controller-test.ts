/// <reference path="../../app/typings/tsd.d.ts"/>
import * as chai from 'chai';
import * as moment from 'moment';
import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';
import * as SecurityService from '../../app/services/security-service';
import * as ChannelMessages from '../../app/dto/channel/channel-messages';
import {Channel} from '../../app/models/models';
import {BadRequest} from '../../app/errors';
import {Forbidden} from '../../app/errors';
import {NotFound} from '../../app/errors';
const assert = chai.assert;
const DEACTIVATED_CHANNEL_ID = 3;

describe('ChannelControllerTest', function () {
  let app;
  before('init fixtures', async function () {
    const builder = Fixtures.Builder.defaultFixtures();
    const channelBuilder = builder.getTickserBuilder().getChannelBuilder();
    channelBuilder.withStatus(Channel.Status.DEACTIVATED, [DEACTIVATED_CHANNEL_ID]);

    await builder.build();
    app = builder.getApp();
  });

  it('should be create', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');
    const request: ChannelMessages.IChannelPostRequest = {
      name: 'name',
      description: 'description',
      type: Channel.Type.PUBLIC,
      backgroundImageId: null,
      coverImageId: null,
      shareIncome: true,
      showRankedList: true,
      allowVoting: true,
      revenues: []
    };

    const goodRevenue: ChannelMessages.IChannelRevenue = {
      percent: 30,
      expiryDate: moment().add(1, 'days').toDate(),
      isExpired: false,
      tickser: {
        id: 2,
        email: 'tickser2@test.com',
      }
    };
    request.revenues = [];
    request.revenues.push(goodRevenue);

    const res1 = await supertest(app)
      .post('/api/user/channels')
      .set('token', token)
      .expect(200)
      .send(request);
  });

  it('should not be create', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    const request: ChannelMessages.IChannelPostRequest = {
      name: 'name',
      description: 'description',
      type: Channel.Type.PUBLIC,
      backgroundImageId: null,
      coverImageId: null,
      shareIncome: true,
      showRankedList: true,
      allowVoting: true,
      revenues: []
    };

    const badRevenue: ChannelMessages.IChannelRevenue = {
      percent: 30,
      expiryDate: moment().add(1, 'days').toDate(),
      isExpired: false,
      tickser: {
        id: 1,
        email: 'tickser1@test.com',
      }
    };
    request.revenues = [];
    request.revenues.push(badRevenue);

    const res2 = await supertest(app)
      .post('/api/user/channels')
      .set('token', token)
      .send(request).expect(400);
  });

  it('should be update', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    const request: ChannelMessages.IChannelPostRequest = {
      name: 'new name',
      description: 'description',
      type: Channel.Type.PUBLIC,
      backgroundImageId: null,
      coverImageId: null,
      shareIncome: true,
      showRankedList: true,
      allowVoting: true,
      revenues: []
    };

    await supertest(app)
      .put('/api/user/channels/1')
      .set('token', token)
      .send(request)
      .expect(200);
  });

  it('should not be update', async() => {
    const token = SecurityService.generateToken(2, 'tickser2');

    const request: ChannelMessages.IChannelPostRequest = {
      name: 'new name',
      description: 'description',
      type: Channel.Type.PUBLIC,
      backgroundImageId: null,
      coverImageId: null,
      shareIncome: true,
      showRankedList: true,
      allowVoting: true,
      revenues: []
    };

    const res = await supertest(app)
      .put('/api/user/channels/1')
      .set('token', token)
      .send(request)
      .expect(403);

  });

  it('get channel by id', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    const res = await supertest(app)
      .get('/api/user/channels/1')
      .set('token', token)
      .expect(200);

    const channel: ChannelMessages.IChannelGetResponse = res.body;

    assert.isExtensible(channel);
  });

  it('get deactivated channel by id', async() => {
    const token = SecurityService.generateToken(3, 'tickser3');
    const res = await supertest(app)
      .get('/api/user/channels/' + DEACTIVATED_CHANNEL_ID)
      .set('token', token).expect(404);
  });

});