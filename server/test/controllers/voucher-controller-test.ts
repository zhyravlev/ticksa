/// <reference path="../../app/typings/tsd.d.ts"/>
import * as chai from 'chai';
import * as supertest from 'supertest-as-promised';
import * as SecurityService from '../../app/services/security-service';
import Fixtures = require('../utils-for-tests/fixtures');
import {Voucher} from '../../app/models/models';
import * as VoucherMessages from '../../app/dto/voucher/voucher-messages';

const assert = chai.assert;
const VOUCHER_CODE_12345 = '12345';

describe('VoucherControllerTest', function () {
  let app;
  before('init fixtures', async function () {
    const builder = Fixtures.Builder.defaultFixtures();

    await builder.build();
    app = builder.getApp();

    const voucher: Voucher.Attributes = {
      code: VOUCHER_CODE_12345,
      ticksAmount: 50,
    };

    await Voucher.Model.bulkCreate([voucher]);
  });

  it('should be checked and applied', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    const checkRes = await supertest(app)
      .post('/api/voucher/check')
      .set('token', token)
      .send({code: VOUCHER_CODE_12345})
      .expect(200);

    let checkResBody = <VoucherMessages.VoucherPostResponse> checkRes.body;
    assert.equal(checkResBody.rights.canCheck, true);
    assert.equal(checkResBody.status.validCode, true);

    const applyRes = await supertest(app)
      .post('/api/voucher/apply')
      .set('token', token)
      .send({code: VOUCHER_CODE_12345})
      .expect(200);

    let applyResBody = <VoucherMessages.VoucherPostResponse> applyRes.body;
    assert.equal(applyResBody.status.successfullyApplied, true);
  });

  it('should not be checked and applied', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    const checkRes = await supertest(app)
      .post('/api/voucher/check')
      .set('token', token)
      .send({code: '100500'})
      .expect(200);

    const checkResBody = <VoucherMessages.VoucherPostResponse> checkRes.body;
    assert.equal(checkResBody.status.invalidCode, true);

    const applyRes = await supertest(app)
      .post('/api/voucher/apply')
      .set('token', token)
      .send({code: VOUCHER_CODE_12345})
      .expect(200);

    let applyResBody = <VoucherMessages.VoucherPostResponse> applyRes.body;
    assert.equal(applyResBody.status.alreadyApplied, true);
  });
});