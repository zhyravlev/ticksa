/// <reference path="../../app/typings/tsd.d.ts"/>

import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';

describe('LemonwayControllerTest', function () {

  let app;
  before('init fixtures', async function () {
    const builder = Fixtures.Builder.defaultFixtures();
    await builder.build();
    app = builder.getApp();
  });

  it('GET /lemonway/callback/error', async() => {
    await supertest(app)
      .get('/api/lemonway/callback/error')
      .expect(302)
  });

  it('GET /lemonway/callback/success', async() => {
    await supertest(app)
      .get('/api/lemonway/callback/error')
      .expect(302)
  });

  it('GET /lemonway/callback/cancel', async() => {
    await supertest(app)
      .get('/api/lemonway/callback/error')
      .expect(302)
  });

  it('POST /lemonway/callback/error', async() => {
    const data = {
      "response_code": 'code',
      "response_wkToken": 'token',
      "response_transactionId": 'transactionId',
      "response_transactionAmount": 'amount',
    };

    await supertest(app)
      .post('/api/lemonway/callback/error')
      .send(data)
      .expect(400)
  });

  it('POST /lemonway/callback/cancel', async() => {
    const data = {
      "response_code": 'code',
      "response_wkToken": 'token',
      "response_transactionId": 'transactionId',
      "response_transactionAmount": 'amount',
    };

    await supertest(app)
      .post('/api/lemonway/callback/cancel')
      .send(data)
      .expect(400)
  });

  it('POST /lemonway/callback/success', async() => {
    const data = {
      "response_code": 'code',
      "response_wkToken": 'token',
      "response_transactionId": 'transactionId',
      "response_transactionAmount": 'amount',
    };

    await supertest(app)
      .post('/api/lemonway/callback/success')
      .send(data)
      .expect(400)
  });


});