/// <reference path="../../app/typings/tsd.d.ts"/>
import * as chai from 'chai';
import * as supertest from 'supertest-as-promised';
import * as SecurityService from '../../app/services/security-service';
import Fixtures = require('../utils-for-tests/fixtures');
import {Tickser} from '../../app/models/models';
import {InvalidCredentials} from '../../app/errors';
import {Content} from '../../app/models/models';
import * as ContentMessages from '../../app/dto/content/content-messages';
import {Category} from '../../app/models/models';
import {Language} from '../../app/models/models';

const assert = chai.assert;

describe('ContentControllerTest', function () {

  let app;
  let category: Category.Instance;
  let language: Language.Instance;

  before('init fixtures', async () => {
    const builder = Fixtures.Builder.defaultFixtures();
    const accountBuilder = builder.getTickserBuilder().getAccountBuilder();
    accountBuilder.withPayedTicks(100, [3]);
    accountBuilder.withPayedTicks(100, [4]);

    await builder.build();
    app = builder.getApp();
    category = await Category.Model.findOne();
    language = await Language.Model.findOne();
  });

  it('POST /user/channels/:channelId/contents should be create', async () => {
    const token = SecurityService.generateToken(4, 'tickser4');

    const content  = {
      title: 'title',
      type: Content.Type.TEXT,
      tickPrice: 10,
      languageId: language.id,
      categoryId: category.id,
      contentText: {
        text: 'text'
      }
    };

    await supertest(app)
      .post('/api/user/channels/4/contents')
      .set('token', token)
      .send(content)
      .expect(200);
  });

  it('should be update', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    const content  = {
      title: 'new title',
      type: Content.Type.TEXT,
      tickPrice: 10,
      languageId: language.id,
      categoryId: category.id,
      contentText: {
        text: 'text'
      }
    };

    await supertest(app)
      .put('/api/user/channels/1/contents/1')
      .set('token', token)
      .send(content)
      .expect(200);
  });

  it('should be publish', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .post('/api/user/channels/1/contents/1/publish')
      .set('token', token)
      .expect(200);
  });

  it('should be unpublish', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .post('/api/user/channels/1/contents/1/unpublish')
      .set('token', token)
      .expect(200);
  });

  it('should be got contents by channel', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/user/channels/1/contents')
      .set('token', token)
      .expect(200);
  });

  it('should be got all contents', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/contents')
      .set('token', token)
      .expect(200);
  });

  it('should be found contents', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/contents/search?q=t')
      .set('token', token)
      .expect(200);
  });

  it('should be got contents by channel', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/channels/1/contents')
      .set('token', token)
      .expect(200);
  });

  it('should be got content teaser', async () => {
    const token = SecurityService.generateToken(2, 'tickser2');

    await supertest(app)
      .get('/api/channels/2/contents/2/teaser')
      .set('token', token)
      .expect(200);
  });

  it('should be extend content', async () => {
    const token = SecurityService.generateToken(3, 'tickser3');

    await supertest(app)
      .post('/api/contents/1/extend')
      .set('token', token)
      .expect(200);
  });

  it('should be like content', async () => {
    const token = SecurityService.generateToken(3, 'tickser3');

    await supertest(app)
      .post('/api/contents/1/like')
      .set('token', token)
      .expect(200);
  });

  it('should be dislike content', async () => {
    const token = SecurityService.generateToken(3, 'tickser3');

    await supertest(app)
      .post('/api/contents/1/dislike')
      .set('token', token)
      .expect(200);
  });

  it('should be pay and refund content', async () => {
    const token = SecurityService.generateToken(3, 'tickser3');

    await supertest(app)
      .post('/api/contents/4/pay')
      .set('token', token)
      .expect(200);

    await supertest(app)
      .post('/api/contents/4/refund')
      .set('token', token)
      .expect(200);
  });

  it('should be send content card', async () => {
    const token = SecurityService.generateToken(3, 'tickser3');

    await supertest(app)
      .post('/api/contents/1/send-content-card-to-me')
      .set('token', token)
      .expect(200);
  });

  it('should be got html card', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/contents/1/html-card')
      .set('token', token)
      .expect(200);
  });

  it('should be got full content', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/channels/1/contents/1')
      .set('token', token)
      .expect(200);
  });

  it('should be deactivate content', async () => {
    const token = SecurityService.generateToken(5, 'tickser5');

    await supertest(app)
      .delete('/api/channels/5/contents/5')
      .set('token', token)
      .expect(200);
  });

  it('should be got history', async () => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/history')
      .set('token', token)
      .expect(200);
  });



});


