/// <reference path="../../app/typings/tsd.d.ts"/>

import * as supertest from 'supertest-as-promised';
import * as SecurityService from '../../app/services/security-service';
import * as Fixtures from '../utils-for-tests/fixtures';

describe('AccountControllerTest', function () {

  describe('/account/change-password', function () {
    let app;
    before('init fixtures', async () => {
      const builder = Fixtures.Builder.defaultFixtures();
      const tickserBuilder = builder.getTickserBuilder();
      tickserBuilder.withPassword('', [2]);

      await builder.build();
      app = builder.getApp();
    });

    it('should be changed', async () => {
      const token = SecurityService.generateToken(1, 'tickser1');

      await supertest(app)
        .post('/api/account/change-password')
        .set('token', token)
        .send({newPassword: 'newpassword', oldPassword: 'test'})
        .expect(200);
    });

    it('should be changed empty password', async () => {
      const token = SecurityService.generateToken(2, 'tickser2');

      await supertest(app)
        .post('/api/account/change-password')
        .set('token', token)
        .send({newPassword: 'newpassword', oldPassword: ''})
        .expect(200)
    });

    it('should be changed empty password', async () => {
      const token = SecurityService.generateToken(1, 'tickser1');

      const res = await supertest(app)
        .post('/api/account/change-password')
        .set('token', token)
        .send({newPassword: 'newpassword', oldPassword: 'invalidpassword'})
        .expect(453);
    });

  });
});
