/// <reference path="../../app/typings/tsd.d.ts"/>
import * as _ from 'lodash';
import * as chai from 'chai';
import * as sinon from 'sinon';
import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';
import * as AuthService from '../../app/services/authentication-service';
import {TickserSocial} from '../../app/models/models';

const assert = chai.assert;

describe('SocialLoginControllerTest', function () {
  const FACEBOOK_SOCIAL_ID = 'facebook_social_id';
  const FACEBOOK_RESPONSE = {
    id: FACEBOOK_SOCIAL_ID,
    name: 'name',
    email: 'email',
    picture: {
      data: {
        is_silhouette: true,
        url: 'url',
      }
    },
    error: null
  };

  let app;
  let tickserBuilder;
  before('init fixtures', async function () {
    sinon.stub(AuthService, 'facebookOAuth').returns(Promise.resolve(FACEBOOK_RESPONSE));

    const builder = Fixtures.Builder.defaultFixtures();
    tickserBuilder = builder.getTickserBuilder();
    await builder.build();

    await TickserSocial.Model.create({
      type: TickserSocial.Type.FACEBOOK,
      socialId: FACEBOOK_SOCIAL_ID,
      tickserId: 1,
    });


    app = builder.getApp();
  });

  it('GET /social/login/facebook/content-list', async() => {
    const response: any = await supertest(app)
      .get('/social/login/facebook/content-list?code=code&granted_scopes=email,public_profile')
      .expect(302);

    const location = response.headers['location'];
    assert.isTrue(_.include(location, '/contents'), 'location "' + location + '" not contains')
  });

  it('GET /social/login/facebook/content-add-type-1', async() => {
    const response: any = await supertest(app)
      .get('/social/login/facebook/content-add-type-1?code=code&granted_scopes=email,public_profile')
      .expect(302);

    const location = response.headers['location'];
    assert.isTrue(_.include(location, '/add-content/'), 'location "' + location + '" not contains')
  });

  it('GET /social/login/facebook/ref-:referrerId-content-:contentId', async() => {
    const response: any = await supertest(app)
      .get('/social/login/facebook/ref-1-content-2?code=code&granted_scopes=email,public_profile')
      .expect(302);

    const location = response.headers['location'];
    assert.isTrue(_.include(location, '/teaser/'), 'location "' + location + '" not contains')
  });
});