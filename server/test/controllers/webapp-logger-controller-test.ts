/// <reference path="../../app/typings/tsd.d.ts"/>
import * as chai from 'chai';
import * as supertest from 'supertest-as-promised';
import * as SecurityService from '../../app/services/security-service';
import Fixtures = require('../utils-for-tests/fixtures');
import {Voucher} from '../../app/models/models';
import UtilService = require('../../app/services/utilities');

const assert = chai.assert;

describe('WebappLoggerControllerTest', function () {
  let app;
  const logs = {
    logs: [{
      type: 'error',
      message: 'message',
      encId: UtilService.encodeNumberToRadix64(1),
    },
      {
        type: 'warning',
        message: 'message',
        encId: UtilService.encodeNumberToRadix64(1),
      }
    ]
};

  before('init fixtures', async function () {
    const builder = Fixtures.Builder.defaultFixtures();

    await builder.build();
    app = builder.getApp();
  });

  it('POST /logger/log auth user', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    const checkRes = await supertest(app)
      .post('/api/logger/log')
      .set('token', token)
      .send(logs)
      .expect(200);
  });

  it('POST /logger/log guest user', async() => {
    const checkRes = await supertest(app)
      .post('/api/logger/log')
      .send(logs)
      .expect(200);
  });

});