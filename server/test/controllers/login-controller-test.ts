/// <reference path="../../app/typings/tsd.d.ts"/>
import * as chai from 'chai';
import * as moment from 'moment';
import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';
import * as SecurityService from '../../app/services/security-service';
const assert = chai.assert;

describe('LoginControllerTest', function () {
  let app;
  let tickserBuilder;
  before('init fixtures', async function () {
    const builder = Fixtures.Builder.defaultFixtures();
    tickserBuilder = builder.getTickserBuilder();

    await builder.build();
    app = builder.getApp();
  });

  it('POST /login', async() => {
    const data = {username: tickserBuilder.getUsernameByTickser(1), password: tickserBuilder.DEFAULT_PASSWORD};

    await supertest(app)
      .post('/api/login')
      .send(data)
      .expect(200)
  });

  it('POST /login/refresh-token', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .post('/api/login/refresh-token')
      .set('token', token)
      .expect(200)
  });


});