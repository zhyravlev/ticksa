/// <reference path="../../app/typings/tsd.d.ts"/>
import * as chai from 'chai';
import * as moment from 'moment';
import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';
import * as SecurityService from '../../app/services/security-service';
const assert = chai.assert;

describe('EmailControllerTest', function () {
  let app;
  before('init fixtures', async function () {
    const builder = Fixtures.Builder.defaultFixtures();

    await builder.build();
    app = builder.getApp();
  });

  it('GET /emails/verify-token', async() => {
    const token = SecurityService.generateEmailToken({email: null, tickserId: null, recipientId: null});

    await supertest(app)
      .get('/api/emails/verify-token?token=' + token)
      .expect(200)
  });

  it('GET /emails/check-subscription', async() => {
    const token = SecurityService.generateEmailToken({email: 'test@test.com', tickserId: null, recipientId: null});

    await supertest(app)
      .get('/api/emails/check-subscription?token=' + token)
      .expect(200)
  });

  it('POST /emails/reply', async() => {
    const token = SecurityService.generateEmailToken({email: 'test@test.com', tickserId: 1, recipientId: 1});

    await supertest(app)
      .post('/api/emails/reply')
      .send({token: token})
      .expect(200)
  });

  it('POST /emails/subscribe', async() => {
    const token = SecurityService.generateEmailToken({email: 'test@test.com', tickserId: null, recipientId: null});

    await supertest(app)
      .post('/api/emails/subscribe')
      .send({token: token})
      .expect(200)
  });

  it('POST /emails/unsubscribe', async() => {
    const token = SecurityService.generateEmailToken({email: 'test@test.com', tickserId: 1, recipientId: null});

    await supertest(app)
      .post('/api/emails/unsubscribe')
      .send({token: token})
      .expect(200)
  });
});