/// <reference path="../../app/typings/tsd.d.ts"/>
import * as chai from 'chai';
import * as moment from 'moment';
import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';
import * as SecurityService from '../../app/services/security-service';
import * as ChannelMessages from '../../app/dto/channel/channel-messages';
import {Channel} from '../../app/models/models';
import {BadRequest} from '../../app/errors';
import {Forbidden} from '../../app/errors';
import {NotFound} from '../../app/errors';
import {Feed} from '../../app/models/models';
const assert = chai.assert;

describe('FeedControllerTest', function () {
  let app;
  let FEED_ID;
  before('init fixtures', async function () {
    const builder = Fixtures.Builder.defaultFixtures();
    const accountBuilder = builder.getTickserBuilder().getAccountBuilder();
    accountBuilder.withPayedTicks(100, [1]);

    await builder.build();
    app = builder.getApp();

    const feed = await Feed.Model.create({
      text: 'text',
      contentId: 1,
      tickserId: 1,
      status: Feed.Status.ACTIVE
    });
    FEED_ID = feed.id;

  });

  it('POST /contents/:contentId/feeds should be create', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    const feed = {
      text: 'text'
    };

    await supertest(app)
      .post('/api/contents/2/feeds')
      .set('token', token)
      .send(feed)
      .expect(200);
  });

  it('GET /contents/:contentId/feeds should be get', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .post('/api/contents/2/feeds')
      .set('token', token)
      .expect(200);
  });

  it('POST /feeds/:feedId/like should be like', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .post('/api/feeds/' + FEED_ID +'/like')
      .set('token', token)
      .expect(200);
  });

  it('POST /feeds/:feedId/dislike should be dislike', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .post('/api/feeds/' + FEED_ID +'/dislike')
      .set('token', token)
      .expect(200);
  });

  it('GET /user/feeds should be get', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/user/feeds')
      .set('token', token)
      .expect(200);
  });

  it('GET /user/feeds/:feedId should be get by id', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    await supertest(app)
      .get('/api/user/feeds/' + FEED_ID)
      .set('token', token)
      .expect(200);
  });

  it('PUT /user/feeds/:feedId should be edit', async() => {
    const token = SecurityService.generateToken(1, 'tickser1');

    const feed = {
      text: 'new text'
    };

    await supertest(app)
      .put('/api/user/feeds/' + FEED_ID)
      .set('token', token)
      .send(feed)
      .expect(200);
  });
});