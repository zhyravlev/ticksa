/// <reference path="../../app/typings/tsd.d.ts"/>

import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';
import * as UtilService from '../../app/services/utilities';
import SharingTestUtils from '../utils-for-tests/sharing-test-utils';

describe('SocialSharingControllerTest', function () {

  let app;
  before('init fixtures', async() => {
    const builder = Fixtures.Builder.defaultFixtures();
    await builder.build();
    app = builder.getApp();
  });

  describe('as browser', () => {

    it('GET /s/:encodedData', async() => {
      const contentId = UtilService.encodeNumberToRadix64(1);
      const referrerId = UtilService.encodeNumberToRadix64(2);

      await supertest(app)
        .get('/s/' + contentId + '-' + referrerId)
        .expect(302)
        .expect('Location', '/#!/teaser/1/1?referrerId=2')
      ;
    });

    it('GET /channels/:channelId/contents/:contentId', async() => {
      await SharingTestUtils.checkThatResponseIsIndexPage(
        supertest(app)
          .get('/channels/1/contents/1')
      );
    });

    it('GET /teaser/:channelId/:contentId', async() => {
      await SharingTestUtils.checkThatResponseIsIndexPage(
        supertest(app)
          .get('/teaser/1/1')
      );
    });

    it('GET /', async() => {
      await SharingTestUtils.checkThatResponseIsIndexPage(
        supertest(app)
          .get('/')
      );
    });

    it('GET /#!/teaser/1/1?referrerId=2', async() => {
      await SharingTestUtils.checkThatResponseIsIndexPage(
        supertest(app)
          .get('/#!/teaser/1/1?referrerId=2')
      );
    });

    it('GET /?_escaped_fragment_=%2Fchannels%2F1%2Fcontents%2F2', async() => {
      await SharingTestUtils.checkThatResponseIsIndexPage(
        supertest(app)
          .get('/?_escaped_fragment_=%2Fchannels%2F1%2Fcontents%2F2')
      );
    });

  });

  describe('as social network crawler', () => {

    it('GET /s/:encodedData', async() => {
      const contentId = UtilService.encodeNumberToRadix64(1);
      const referrerId = UtilService.encodeNumberToRadix64(2);

      await SharingTestUtils.checkThatResponseIsMarkupForSocialNetworkCrawler(
        supertest(app)
          .get('/s/' + contentId + '-' + referrerId)
          .set('user-agent', SharingTestUtils.SOCIAL_NETWORK_CRAWLER_USER_AGENT),
        'tickser1`s content'
      );
    });

    it('GET /channels/:channelId/contents/:contentId', async() => {
      await SharingTestUtils.checkThatResponseIsMarkupForSocialNetworkCrawler(
        supertest(app)
          .get('/channels/1/contents/1')
          .set('user-agent', SharingTestUtils.SOCIAL_NETWORK_CRAWLER_USER_AGENT),
        'tickser1`s content'
      );
    });

    it('GET /teaser/:channelId/:contentId', async() => {
      await SharingTestUtils.checkThatResponseIsMarkupForSocialNetworkCrawler(
        supertest(app)
          .get('/teaser/1/1')
          .set('user-agent', SharingTestUtils.SOCIAL_NETWORK_CRAWLER_USER_AGENT),
        'tickser1`s content'
      );
    });

    it('GET /', async() => {
      await SharingTestUtils.checkThatResponseIsIndexPage(
        supertest(app)
          .get('/')
          .set('user-agent', SharingTestUtils.SOCIAL_NETWORK_CRAWLER_USER_AGENT)
      );
    });

    it('GET /?_escaped_fragment_=%2Fchannels%2F2%2Fcontents%2F2', async() => {
      await SharingTestUtils.checkThatResponseIsMarkupForSocialNetworkCrawler(
        supertest(app)
          .get('/?_escaped_fragment_=%2Fchannels%2F2%2Fcontents%2F2')
          .set('user-agent', SharingTestUtils.SOCIAL_NETWORK_CRAWLER_USER_AGENT),
        'tickser2`s content'
      );
    });
  });

  describe('as search engine crawler', () => {

    it('GET /s/:encodedData', async() => {
      const contentId = UtilService.encodeNumberToRadix64(1);
      const referrerId = UtilService.encodeNumberToRadix64(2);

      await SharingTestUtils.checkThatResponseIsMarkupForSearchEngineCrawler(
        supertest(app)
          .get('/s/' + contentId + '-' + referrerId)
          .set('user-agent', SharingTestUtils.SEARCH_ENGINE_CRAWLER_USER_AGENT),
        'tickser1`s content',
        'tickser1`s channel'
      );
    });

    it('GET /channels/:channelId/contents/:contentId', async() => {
      await SharingTestUtils.checkThatResponseIsMarkupForSearchEngineCrawler(
        supertest(app)
          .get('/channels/1/contents/1')
          .set('user-agent', SharingTestUtils.SEARCH_ENGINE_CRAWLER_USER_AGENT),
        'tickser1`s content',
        'tickser1`s channel'
      );
    });

    it('GET /teaser/:channelId/:contentId', async() => {
      await SharingTestUtils.checkThatResponseIsMarkupForSearchEngineCrawler(
        supertest(app)
          .get('/teaser/1/1')
          .set('user-agent', SharingTestUtils.SEARCH_ENGINE_CRAWLER_USER_AGENT),
        'tickser1`s content',
        'tickser1`s channel'
      );
    });

    it('GET /', async() => {
      await SharingTestUtils.checkThatResponseIsIndexPage(
        supertest(app)
          .get('/')
          .set('user-agent', SharingTestUtils.SEARCH_ENGINE_CRAWLER_USER_AGENT)
      );
    });

    it('GET /?_escaped_fragment_=%2Fchannels%2F2%2Fcontents%2F2', async() => {
      await SharingTestUtils.checkThatResponseIsMarkupForSearchEngineCrawler(
        supertest(app)
          .get('/?_escaped_fragment_=%2Fchannels%2F2%2Fcontents%2F2')
          .set('user-agent', SharingTestUtils.SEARCH_ENGINE_CRAWLER_USER_AGENT),
        'tickser2`s content',
        'tickser2`s channel'
      );
    });
  });

});
