/// <reference path="../../../app/typings/tsd.d.ts"/>
import chai = require('chai');

import lemonway = require('../../../app/api/lemonway/lemonway');
import LemonwayCashInService = require('../../../app/services/lemonway-cashin-service');

describe('LemonwayTest', function () {

  it('css url must be URI encoded', function () {
    var webkitUrl = 'http://localhost:3000';
    var moneyInToken = 'some-token';
    var cssUrl = 'http://localhost:3001/1412512769/assets/lemonway/style.css';
    var lang = 'en';

    var result = lemonway.LemonwayApiUtils.constructWebkitUrl(webkitUrl, moneyInToken, cssUrl, lang);
    chai.expect(result).to.equal(webkitUrl
      + '?moneyintoken=' + moneyInToken
      + '&p=' + 'http%3A%2F%2Flocalhost%3A3001%2F1412512769%2Fassets%2Flemonway%2Fstyle.css'
      + '&lang=' + lang);
  });

  it('calculate fees for LW', function (done) {
    chai.assert.equal(LemonwayCashInService.calculateFees(100), 1.95);
    chai.assert.equal(LemonwayCashInService.calculateFees(65), 1.32);
    chai.assert.equal(LemonwayCashInService.calculateFees(135), 2.58);

    done();
  });
});