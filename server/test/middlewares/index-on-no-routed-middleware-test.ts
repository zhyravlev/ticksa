/// <reference path="../../app/typings/tsd.d.ts"/>

import {assert} from 'chai';
import {Response} from 'supertest-as-promised';
import * as supertest from 'supertest-as-promised';
import * as Fixtures from '../utils-for-tests/fixtures';
import IndexOnNoRoutedMiddleware from '../../app/middlewares/index-on-no-routed-middleware';

describe('IndexOnNoRoutedMiddlewareTest', function () {

  let app;
  before('init fixtures', async() => {
    const builder = Fixtures.Builder.defaultFixtures();
    await builder.build();
    app = builder.getApp();
  });

  describe('check routes', () => {

    it('check routes', async() => {
      const unregisteredUrls = [];

      app._router.stack.forEach(function (middleware: any) {
        if (middleware.route) { // routes registered directly on the app
          const path = middleware.route.path;
          if (IndexOnNoRoutedMiddleware.isRegisterRouteUrl(path)) {
            return;
          }
          unregisteredUrls.push(path);

        } else if (middleware.name === 'router') { // router middleware
          middleware.handle.stack.forEach(function (handler) {
            const route = handler.route;
            if (route == null) {
              return;
            }

            const path = route.path;
            if (IndexOnNoRoutedMiddleware.isRegisterRouteUrl(path)) {
              return;
            }
            unregisteredUrls.push(path);
          });
        }
      });

      assert.deepEqual(unregisteredUrls, ['/'], 'There are unregistered urls!')
    });

  });

  describe('requests', () => {

    it('GET /not-found/route', async() => {
      await supertest(app)
        .get('/not-found/route')
        .expect(200)
        .expect('Content-Type', /text\/html/)
        .expect(function (res: Response) {
          assert.isTrue(res.text.indexOf('<!DOCTYPE html>') > -1, 'Check DOCTYPE');
          assert.isTrue(res.text.indexOf('<html ng-app="ticksaWebapp"') > -1, 'Check the beginning of html');
          assert.isTrue(res.text.indexOf('Ticksa - My Premium Channel') > -1, 'Check "Ticksa" title');
        })
      ;
    });

    it('GET /another-not-found', async() => {
      await supertest(app)
        .get('/another-not-found')
        .expect(200)
        .expect('Content-Type', /text\/html/)
        .expect(function (res: Response) {
          assert.isTrue(res.text.indexOf('<!DOCTYPE html>') > -1, 'Check DOCTYPE');
          assert.isTrue(res.text.indexOf('<html ng-app="ticksaWebapp"') > -1, 'Check the beginning of html');
          assert.isTrue(res.text.indexOf('Ticksa - My Premium Channel') > -1, 'Check "Ticksa" title');
        })
      ;
    });

    it('GET /?some-unknown-parameter=2', async() => {
      await supertest(app)
        .get('/?some-unknown-parameter=2')
        .expect(200)
        .expect('Content-Type', /text\/html/)
        .expect(function (res: Response) {
          assert.isTrue(res.text.indexOf('<!DOCTYPE html>') > -1, 'Check DOCTYPE');
          assert.isTrue(res.text.indexOf('<html ng-app="ticksaWebapp"') > -1, 'Check the beginning of html');
          assert.isTrue(res.text.indexOf('Ticksa - My Premium Channel') > -1, 'Check "Ticksa" title');
        })
      ;
    });
  });

});