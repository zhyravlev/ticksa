/// <reference path="../../app/typings/tsd.d.ts"/>

const logger = require('../../app/logger/logger')(__filename);

import Promise = require('bluebird');
import events = require('events');
import moment = require('moment');

export default class RecursiveIterator extends events.EventEmitter {
  public static AFTER_EXECUTE_ITERATION_EVENT: string = 'after-execute-iteration';
  public static BEFORE_EXECUTE_ITERATION_EVENT: string = 'before-execute-iteration';
  public static CREATE_ITERATION_EVENT: string = 'create-iteration';
  public static FINISH_EVENT: string = 'finish';

  private iterationCounter: number;
  private statistics: {};
  private dateOfCompletion: any;
  private executeIterationsCount: number;

  constructor(private numIterationsPerExecution: number) {
    super();
    this.iterationCounter = 0;
    this.statistics = {};
  }

  public start() {
    logger.info('START');
    var iterator = this;

    return this.startRecursive(iterator);
  }

  private startRecursive(iterator: RecursiveIterator): Promise<any> {
    return iterator.emitAsync(RecursiveIterator.BEFORE_EXECUTE_ITERATION_EVENT).then(function () {
      return Promise.delay(100);
    }).then(function () {
      iterator.iterationCounter += 1;
      return iterator.createIteration(iterator);
    }).then(function () {
      var statistic : Statistic = iterator.statistics[iterator.iterationCounter];
      if(statistic.success > 1) {
        return Promise.reject({message:'Error iteration', iteration: statistic})
      }
      return iterator.emitAsync(RecursiveIterator.AFTER_EXECUTE_ITERATION_EVENT);
    }).then(function () {
      return Promise.delay(100);
    }).then(function () {
      logger.info(iterator.iterationCounter.toString(), JSON.stringify(iterator.statistics[iterator.iterationCounter]));

      if (!iterator.isFinished()) {
        return iterator.startRecursive(iterator);
      } else {
        logger.info('FINISH!');
        return Promise.delay(100);
      }
    });
  }

  private isFinished(): boolean {
    if (this.dateOfCompletion) {
      return moment().isBefore(this.dateOfCompletion);
    } else if (this.executeIterationsCount) {
      return this.iterationCounter >= this.executeIterationsCount;
    } else {
      throw new Error('setExecutionTimeInMinutes() or setExecuteIterationsCount() must be set');
    }
  }

  private createIteration(iterator: RecursiveIterator): Promise<any> {
    var promises: Array<Promise<any>> = [];
    iterator.statistics[iterator.iterationCounter] = new Statistic();
    for (var i = 0; i < iterator.numIterationsPerExecution; i++) {
      var promise = iterator.emitAsync(RecursiveIterator.CREATE_ITERATION_EVENT);

      promise.then(function (result) {
        iterator.statistics[iterator.iterationCounter].success += 1;
        return result;
      }).catch(function (err) {
        iterator.statistics[iterator.iterationCounter].failed += 1;
        iterator.statistics[iterator.iterationCounter].causes.push(err.message);
        return Promise.resolve();
      });

      promises.push(promise);
    }
    return Promise.all(promises).catch(function (err) {
      return Promise.resolve();
    });
  }

  private emitAsync(event): Promise<any> {
    var iterator = this;

    if (iterator.listeners(event).length == 0) {
      return Promise.resolve();
    }

    return new Promise(function (resolve, reject) {
      iterator.emit(event, resolve);
    });
  }

  public setExecutionTimeInMinutes(minutes: number) {
    this.dateOfCompletion = moment().add(minutes.toString(), 'minutes');
  }

  public setExecuteIterationsCount(count: number) {
    this.executeIterationsCount = count;
  }

}

class Statistic {
  success: number = 0;
  failed: number = 0;
  causes: Array<string> = [];

  constructor() {
  }
}
