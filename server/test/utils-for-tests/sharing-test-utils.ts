/// <reference path="../../app/typings/tsd.d.ts"/>

import * as supertest from 'supertest-as-promised';
import {Response} from 'supertest-as-promised';
import {assert} from 'chai';

export default class SharingTestUtils {

  public static SOCIAL_NETWORK_CRAWLER_USER_AGENT: string = 'facebookexternalhit';
  public static SEARCH_ENGINE_CRAWLER_USER_AGENT: string = 'googlebot';

  public static async checkThatResponseIsIndexPage(test: supertest.Test) {
    await test
      .expect(200)
      .expect('Content-Type', /text\/html/)
      .expect(function (res: Response) {
        assert.isTrue(res.text.indexOf('<!DOCTYPE html>') > -1, 'Check DOCTYPE');
        assert.isTrue(res.text.indexOf('<html ng-app="ticksaWebapp"') > -1, 'Check the beginning of html');
        assert.isTrue(res.text.indexOf('Ticksa - My Premium Channel') > -1, 'Check "Ticksa" title');
        assert.isTrue(res.text.indexOf('app.css') > -1, 'Check "Ticksa" css');
        assert.isTrue(res.text.indexOf('vendor.css') > -1, 'Check "Ticksa" vendors css');
        assert.isTrue(res.text.indexOf('app.js') > -1, 'Check "Ticksa" js');
        assert.isTrue(res.text.indexOf('vendor.js') > -1, 'Check "Ticksa" vendors js');
      });
  }

  public static async checkThatResponseIsMarkupForSocialNetworkCrawler(test: supertest.Test, titleAndDescription: string) {
    await test
      .expect(200)
      .expect('Content-Type', /text\/html/)
      .expect(function (res: Response) {
        assert.isTrue(res.text.indexOf('<html><head>') > -1, 'Check the beginning of html');
        assert.isTrue(res.text.indexOf('<meta property="og:url" content=') > -1, 'Check url meta tag');
        assert.isTrue(res.text.indexOf('<meta property="og:type" content="website"/>') > -1, 'Check type meta tag');
        assert.isTrue(res.text.indexOf('<meta property="og:title" content="' + titleAndDescription + '"/>') > -1, 'Check title meta tag');
        assert.isTrue(res.text.indexOf('<meta property="og:description" content="' + titleAndDescription + '"/>') > -1, 'Check description meta tag');
      });
  }

  public static async checkThatResponseIsMarkupForSearchEngineCrawler(test: supertest.Test, content: string, channel: string) {
    await test
      .expect(200)

      .expect(function (res: Response) {
        assert.isTrue(res.text.indexOf('<html><head>') > -1, 'Check the beginning of html');
        assert.isTrue(res.text.indexOf('<meta name="description" content="' + content + '"/>') > -1, 'Check description meta');
        assert.isTrue(res.text.indexOf('<title>' + content + ' | Ticksa - My Premium Channel</title>') > -1, 'Check title');
        assert.isTrue(res.text.indexOf('<h1>' + content + '</h1>') > -1, 'Check h1');
        assert.isTrue(res.text.indexOf('<p>' + content + '</p>') > -1, 'Check p');
        assert.isTrue(res.text.indexOf('<p><span>' + channel + '</span></p>') > -1, 'Check span');
      });
  }

}