/// <reference path="../../app/typings/tsd.d.ts"/>

import * as _ from 'lodash';
import * as moment from 'moment';
import * as express from 'express';
import {
  Account,
  Tickser,
  Channel,
  ChannelRevenue,
  TickserChannel,
  Content,
  ContentText,
  Playlist,
  initialize
} from '../../app/models/models';
import * as SecurityService from '../../app/services/security-service';
import * as connection from '../../app/connection';

// https://simonmcmanus.wordpress.com/2013/01/03/forcing-garbage-collection-with-node-js-and-v8/
function forceGC(): void {
  if (global.gc) {
    global.gc();
  }
}

export class Builder {
  private _tickserBuilder: TickserBuilder;
  private _systemAccountsBuilder: AccountBuilder;
  private isSyncDatabase: boolean;
  private _app: express.Express;

  constructor() {
    this._tickserBuilder = new TickserBuilder();
    this._systemAccountsBuilder = new AccountBuilder(this._tickserBuilder);
  }

  public static defaultFixtures(): Builder {
    forceGC();

    const tickserBuilder = new TickserBuilder();
    return new Builder()
      .withSyncDatabase()
      .withTicksers(tickserBuilder.withCount(10)
        .withAccounts(new AccountBuilder(tickserBuilder))
        .withChannels(new ChannelBuilder(tickserBuilder))
        .withContent(new ContentBuilder(tickserBuilder))
        .withPlaylists(new PlaylistBuilder(tickserBuilder)))
  }

  public withSyncDatabase() {
    this.isSyncDatabase = true;
    return this;
  }

  public withSystemAccounts(tickserBuilder: TickserBuilder, sysAccountsBuilder?: AccountBuilder): Builder {
    this._systemAccountsBuilder = sysAccountsBuilder
      ? sysAccountsBuilder
      : new AccountBuilder(tickserBuilder).withOnlySystem();
    return this;
  }

  public withTicksers(tickserBuilder: TickserBuilder): Builder {
    this._tickserBuilder = tickserBuilder;
    return this;
  }

  public syncDatabase(): Promise<any> {
    if (this.isSyncDatabase) {
      return initialize();
    }
    return Promise.resolve();
  }

  public async build(): Promise<any> {
    const builder = this;
    await this.syncDatabase();
    await Promise.all([
      builder._systemAccountsBuilder.build(),
      builder._tickserBuilder.build()
    ]);
    builder._app = require('../../app/app');
  }

  public getTickserBuilder(): TickserBuilder {
    return this._tickserBuilder;
  }

  getApp(): express.Express {
    return this._app;
  }
}

abstract class AbstractBuilder {

  public async updateSequence(tableName: any, length: number) {
    length++;
    const sequenceName = tableName + '_id_seq';
    return connection.query("SELECT setval('\"" + sequenceName + "\"', " + length + ")");
  }

  abstract build(): Promise<any>
}


export class TickserBuilder {
  public DEFAULT_PASSWORD = 'test';

  private status: ValueBuilder = new ValueBuilder(Tickser.Status.ACTIVE);
  private password: ValueBuilder = new ValueBuilder(this.DEFAULT_PASSWORD);
  private confirmationUrl: ValueBuilder = new ValueBuilder();
  private changePasswordUrl: ValueBuilder = new ValueBuilder();
  private countryId: ValueBuilder = new ValueBuilder(null);
  public tickserIdsWithoutChannel: ValueBuilder = new ValueBuilder();
  public tickserIdsWithoutAccount: ValueBuilder = new ValueBuilder();
  public tickserIdsWithoutPlaylist: ValueBuilder = new ValueBuilder();

  private _ticksersCount: number = 0;
  private _accountBuilder: AccountBuilder;
  private _channelBuilder: ChannelBuilder;
  private _contentBuilder: ContentBuilder;
  private _playlistBuilder: PlaylistBuilder;
  public ticksers: Tickser.Instance[] = [];

  constructor() {
    this._accountBuilder = new AccountBuilder(this);
    this._channelBuilder = new ChannelBuilder(this);
    this._contentBuilder = new ContentBuilder(this);
    this._playlistBuilder = new PlaylistBuilder(this);
  }

  public withoutChannelTicksers(tickserIds: number[]): TickserBuilder {
    this.tickserIdsWithoutChannel.addFilterableValue(true, tickserIds);
    return this;
  }

  public withoutAccountTicksers(tickserIds: number[]): TickserBuilder {
    this.tickserIdsWithoutAccount.addFilterableValue(true, tickserIds);
    return this;
  }

  public withoutPlaylistTicksers(tickserIds: number[]): TickserBuilder {
    this.tickserIdsWithoutPlaylist.addFilterableValue(true, tickserIds);
    return this;
  }

  public withPassword(password: string, ids?: Array<number>): TickserBuilder {
    this.password.addFilterableValue(password, ids);
    return this;
  }

  public withConfirmationUrl(confirmationUrl: string, ids?: Array<number>): TickserBuilder {
    this.confirmationUrl.addFilterableValue(confirmationUrl, ids);
    return this;
  }

  public withChangePasswordUrl(changePasswordUrl: string, ids?: Array<number>): TickserBuilder {
    this.changePasswordUrl.addFilterableValue(changePasswordUrl, ids);
    return this;
  }

  public withStatus(status: Tickser.Status, ids?: Array<number>): TickserBuilder {
    this.status.addFilterableValue(status, ids);
    return this;
  }

  public withCountryId(countryId: number, ids?: Array<number>): TickserBuilder {
    this.countryId.addFilterableValue(countryId, ids);
    return this;
  }

  public withCount(count: number): TickserBuilder {
    this._ticksersCount = count;
    return this;
  }

  public withAccounts(accountBuilder: AccountBuilder): TickserBuilder {
    this._accountBuilder = accountBuilder;
    return this;
  }

  public withChannels(channelBuilder: ChannelBuilder): TickserBuilder {
    this._channelBuilder = channelBuilder;
    return this;
  }

  public withContent(contentBuilder: ContentBuilder): TickserBuilder {
    this._contentBuilder = contentBuilder;
    return this;
  }

  public withPlaylists(playlistBuilder: PlaylistBuilder): TickserBuilder {
    this._playlistBuilder = playlistBuilder;
    return this;
  }

  public build(): Promise<any> {
    var builder = this;
    return builder.createTicksers().then(function (ticksers: Tickser.Instance[]) {
      builder.ticksers = ticksers;
      builder._channelBuilder.byTicksers(ticksers);
      builder._accountBuilder.byTicksers(ticksers);
      builder._contentBuilder.byTicksers(ticksers);
      builder._playlistBuilder.byTicksers(ticksers);
      return Promise.all([
        builder._accountBuilder.build(),
        builder._channelBuilder.build(),
        builder._playlistBuilder.build()
      ]).then(function () {
        return builder._contentBuilder.build();
      });
    });
  }

  private async createTicksers(): Promise<any> {
    var ticksers = [];
    var builder = this;
    for (var id = 1; id <= this._ticksersCount; id++) {
      const passwordValue = builder.password.getValue(id);
      let password;
      if (passwordValue) {
        password = await SecurityService.createPassword(passwordValue);
      }

      var tickser: Tickser.Attributes = {
        id: id,
        username: builder.getUsernameByTickser(id),
        email: builder.getEmailByTickser(id),
        name: 'tickser' + id,
        countryId: builder.countryId.getValue(id),
        defaultChannelId: id,
        password: password,
        registrationDate: new Date(),
        status: builder.status.getValue(id),
        confirmationUrl: builder.confirmationUrl.getValue(id),
        changePasswordUrl: builder.changePasswordUrl.getValue(id)
      };
      ticksers.push(tickser);
    }
    return Tickser.Model.bulkCreate(ticksers);
  }

  public getEmailByTickser(id: number): string {
    return 'test.tickser' + id + '@test.com';
  }

  public getUsernameByTickser(id: number): string {
    return 'tickser' + id;
  }

  public getAccountBuilder(): AccountBuilder {
    return this._accountBuilder;
  }

  public getChannelBuilder(): ChannelBuilder {
    return this._channelBuilder;
  }

  public getContentBuilder(): ContentBuilder {
    return this._contentBuilder;
  }

  public getPlaylistBuilder(): PlaylistBuilder {
    return this._playlistBuilder;
  }
}

export class AccountBuilder {
  private freeTicks: ValueBuilder = new ValueBuilder(0);
  private payedTicks: ValueBuilder = new ValueBuilder(0);
  private onlySystem: boolean = false;
  private _ticksers: Array<any> = [];
  private _tickserBuilder: TickserBuilder;

  constructor(tickserBuilder: TickserBuilder) {
    this._tickserBuilder = tickserBuilder;
  }

  public withFreeTicks(freeTicks: number, ids?: Array<number>): AccountBuilder {
    this.freeTicks.addFilterableValue(freeTicks, ids);
    return this;
  }

  public withPayedTicks(payedTicks: number, ids?: Array<number>): AccountBuilder {
    this.payedTicks.addFilterableValue(payedTicks, ids);
    return this;
  }

  public withOnlySystem(): AccountBuilder {
    this.onlySystem = true;
    return this;
  }

  public byTicksers(ticksers: Tickser.Instance[]): AccountBuilder {
    this._ticksers = ticksers;
    return this;
  }

  public build(): Promise<any> {
    var builder = this;
    var accounts = [];

    if (this.onlySystem) {
      _.forIn(Account.SystemAccount, function (id, key) {
        accounts.push(builder.createAccount(id));
      });
    } else {
      for (var id = 1; id <= builder._ticksers.length; id++) {
        const withoutAccount = builder._tickserBuilder.tickserIdsWithoutAccount.getValue(id);
        if (withoutAccount) {
          continue;
        }
        accounts.push(builder.createAccount(id));
      }

    }

    return Account.Model.bulkCreate(accounts);
  }

  private createAccount(id): Account.Attributes {
    return {
      id: id,
      tickserId: this.onlySystem ? null : id,
      currentBalance: this.payedTicks.getNumValue(id),
      totalFreeTicks: 0,
      totalTicksDebited: 0,
      totalTicksCredited: 0,
      currentBalanceFreeTicks: this.onlySystem ? 0 : this.freeTicks.getValue(id),
      currentBalanceEarnedTicks: 0,
      balanceUpdated: new Date(),
      status: Account.Status.NORMAL,
      type: this.onlySystem ? Account.AccountType.SYSTEM : Account.AccountType.PRIVATE,
    }
  }
}

export class ChannelBuilder extends AbstractBuilder {
  private ticksers: Tickser.Instance[];
  private nameValue: ValueBuilder = new ValueBuilder();
  private revenueBuilder: AbstractBuilder = new FakeBuilder();
  private shareIncome: ValueBuilder = new ValueBuilder(false);
  private showRankedList: ValueBuilder = new ValueBuilder(false);
  private status: ValueBuilder = new ValueBuilder(Channel.Status.ACTIVE);

  private _tickserBuilder: TickserBuilder;


  constructor(tickserBuilder: TickserBuilder) {
    super();
    this._tickserBuilder = tickserBuilder;
  }

  public byTicksers(ticksers: Tickser.Instance[]): ChannelBuilder {
    this.ticksers = ticksers;
    return this;
  }

  public withName(name: string, ids?: Array<number>): ChannelBuilder {
    this.nameValue.addFilterableValue(name, ids);
    return this;
  }

  public withRevenueBuilder(revenueBuilder: ChannelRevenueBuilder) {
    this.revenueBuilder = revenueBuilder;
    return this;
  }

  public withShowRankedList(showRankedList: boolean, ids?: Array<number>) {
    this.showRankedList.addFilterableValue(showRankedList, ids);
    return this;
  }

  public withShareIncome(shareIncome: boolean, ids?: Array<number>) {
    this.shareIncome.addFilterableValue(shareIncome, ids);
    return this;
  }

  public withStatus(status: Channel.Status, ids?: Array<number>) {
    this.status.addFilterableValue(status, ids);
    return this;
  }

  public async build(): Promise<any> {
    var builder = this;
    const channels: Channel.Instance[] = await builder.createChannels();
    await builder.updateSequence(Channel.Model.getTableName(), channels.length);
    await builder.createTickserChannels();
    await builder.revenueBuilder.build();
  }

  private createChannels(): Promise<Channel.Instance[]> {
    var channels: Channel.Attributes[] = [];
    var builder = this;

    _.forEach(this.ticksers, function (tickser: Tickser.Instance, index: number) {
      const withoutChannel = builder._tickserBuilder.tickserIdsWithoutChannel.getValue(tickser.id);
      if (withoutChannel) {
        return;
      }

      let value = builder.nameValue.getValue(tickser.id);
      let name = value ? value : tickser.name + '`s channel';

      channels.push({
        id: tickser.id,
        name: name,
        status: builder.status.getValue(tickser.id),
        type: Channel.Type.PUBLIC,
        shareIncome: builder.shareIncome.getValue(tickser.id),
        showRankedList: builder.showRankedList.getValue(tickser.id),
      })
    });

    return Channel.Model.bulkCreate(channels);
  }

  private createTickserChannels() {
    var builder = this;
    var tickserChannels = [];

    _.forEach(this.ticksers, function (tickser: Tickser.Instance) {
      const withoutChannel = builder._tickserBuilder.tickserIdsWithoutChannel.getValue(tickser.id);
      if (withoutChannel) {
        return;
      }

      tickserChannels.push({
        ownerType: TickserChannel.OwnerType.CREATOR,
        tickserId: tickser.id,
        channelId: tickser.id
      });
    });
    return TickserChannel.Model.bulkCreate(tickserChannels, {returning: true});
  }
}

export class ContentBuilder extends AbstractBuilder {
  private ticksers: Array<any> = [];
  private tickPriceValue: ValueBuilder = new ValueBuilder(1.0);
  private positionInDiscoveryValue: ValueBuilder = new ValueBuilder();
  private randomPositionValue: ValueBuilder = new ValueBuilder();
  private publishedValue: ValueBuilder = new ValueBuilder(true);
  private statusValue: ValueBuilder = new ValueBuilder(Content.Status.ACTIVE);
  private titleValue: ValueBuilder = new ValueBuilder();

  private count: number;
  private _tickserBuilder: TickserBuilder;

  constructor(tickserBuilder: TickserBuilder) {
    super();
    this._tickserBuilder = tickserBuilder;
  }

  public withPositionInDiscovery(randomPosition: number, ids: Array<number>): ContentBuilder {
    this.positionInDiscoveryValue.addFilterableValue(randomPosition, ids);
    return this;
  }

  public withRandomPosition(position: number, ids: Array<number>): ContentBuilder {
    this.randomPositionValue.addFilterableValue(position, ids);
    return this;
  }

  public withPublished(published: boolean, ids: Array<number>): ContentBuilder {
    this.publishedValue.addFilterableValue(published, ids);
    return this;
  }

  public withStatus(status: Content.Status, ids: Array<number>): ContentBuilder {
    this.statusValue.addFilterableValue(status, ids);
    return this;
  }

  public withTickPrice(price: number, ids: Array<number>): ContentBuilder {
    this.tickPriceValue.addFilterableValue(price, ids);
    return this;
  }

  public withTitle(title: string, ids: Array<number>): ContentBuilder {
    this.titleValue.addFilterableValue(title, ids);
    return this;
  }

  public byTicksers(ticksers: Tickser.Instance[]): ContentBuilder {
    this.ticksers = ticksers;
    return this;
  }

  public setCount(count: number): ContentBuilder {
    this.count = count;
    return this;
  }

  public build(): Promise<any> {
    return this.createContent();
  }

  private async createContent() {
    var contentTexts = [];
    var contents: Content.Attributes[] = [];

    for (var id = 1; id <= this.ticksers.length; id++) {
      var index = (id - 1);

      const withoutChannel = this._tickserBuilder.tickserIdsWithoutChannel.getValue(id);
      if (withoutChannel) {
        continue;
      }

      var value = this.titleValue.getValue(id);
      var title = value ? value : this.ticksers[index].name + '`s content';
      contents.push({
        id: id,
        title: title,
        likes: 0,
        dislikes: 0,
        status: this.statusValue.getValue(id),
        type: Content.Type.TEXT,
        tickPrice: this.tickPriceValue.getValue(id),
        published: this.publishedValue.getValue(id),
        tickserId: id,
        channelId: id,
        position_in_discover: this.positionInDiscoveryValue.getValue(id),
        randomPosition: this.randomPositionValue.getValue(id),
        expiryDate: moment().add(7, 'd').toDate(),
        createdAt: moment().add(index, 'm').toDate()
      });

      contentTexts.push({
        text: '<p>' + this.ticksers[index].name + '`s content</p>',
        contentId: id,
      });
    }

    const instances: Content.Instance[] = await Content.Model.bulkCreate(contents);
    await ContentText.Model.bulkCreate(contentTexts, {returning: true});
    await this.updateSequence(Content.Model.getTableName(), instances.length);
  }
}

export class ChannelRevenueBuilder extends AbstractBuilder {
  private revenues: ChannelRevenue.Attributes[] = [];

  constructor() {
    super();
  }

  public addRevenue(revenue: ChannelRevenue.Attributes) {
    this.revenues.push(revenue);
    return this;
  }

  public build(): Promise<ChannelRevenue.Instance[]> {
    if (this.revenues.length == 0) {
      return Promise.resolve([]);
    }
    return ChannelRevenue.Model.bulkCreate(this.revenues);
  }
}

class FakeBuilder extends AbstractBuilder {
  public build(): Promise<any> {
    return Promise.resolve();
  }
}

class ValueBuilder {
  private _defaultValue: any;
  private _filterableValues: { [id: number]: any; };

  constructor(defaultValue?: any) {
    this._defaultValue = defaultValue;
    this._filterableValues = {};
  }

  public setDefaultValue(defaultValue: any) {
    this._defaultValue = defaultValue;
  }

  public addFilterableValue(value: any, ids: Array<number>) {
    const that = this;

    if (!ids || ids.length == 0) {
      that.setDefaultValue(value);
      return;
    }

    ids.forEach(function (id) {
      that._filterableValues[id] = value;
    });
  }

  public getValue(id: number): any {
    if (this._filterableValues.hasOwnProperty(id)) {
      return this._filterableValues[id];
    }
    return this._defaultValue;
  }

  public getNumValue(id: number): number {
    return this.getValue(id);
  }
}

export class PlaylistBuilder extends AbstractBuilder {
  private ticksers: Tickser.Instance[];
  private nameValue: ValueBuilder = new ValueBuilder();
  private published: ValueBuilder = new ValueBuilder(true);
  private _tickserBuilder: TickserBuilder;

  constructor(tickserBuilder: TickserBuilder) {
    super();
    this._tickserBuilder = tickserBuilder;
  }

  public byTicksers(ticksers: Tickser.Instance[]): PlaylistBuilder {
    this.ticksers = ticksers;
    return this;
  }

  public withName(name: string, ids?: Array<number>): PlaylistBuilder {
    this.nameValue.addFilterableValue(name, ids);
    return this;
  }

  public withPublished(published: boolean, ids?: Array<number>) {
    this.published.addFilterableValue(published, ids);
    return this;
  }

  public async build(): Promise<any> {
    const builder = this;
    const playlists: Playlist.Instance[] = await builder.createPlaylists();
    await builder.updateSequence(Playlist.Model.getTableName(), playlists.length);
  }

  private createPlaylists(): Promise<Playlist.Instance[]> {
    const playlists: Playlist.Attributes[] = [];
    const builder = this;

    _.forEach(this.ticksers, function (tickser: Tickser.Instance, index: number) {
      const withoutPlaylist = builder._tickserBuilder.tickserIdsWithoutPlaylist.getValue(tickser.id);
      if (withoutPlaylist) {
        return;
      }

      const value = builder.nameValue.getValue(tickser.id);
      const name = value ? value : tickser.name + '`s playlist';

      playlists.push({
        id: tickser.id,
        name: name,
        tickserId: tickser.id,
        published: builder.published.getValue(tickser.id),
        deleted: false,
        coverImageId: null,
        backgroundImageId: null,
        orderedContents: []
      })
    });

    return Playlist.Model.bulkCreate(playlists);
  }
}