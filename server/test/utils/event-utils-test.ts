/// <reference path="../../app/typings/tsd.d.ts"/>
import events = require('events');
import mocha = require('mocha');
import chai = require('chai');
import _ = require('lodash');

import EventUtils = require('../../app/utils/event-utils');

let assert = chai.assert;

describe('EventUtilsTest', function () {

  const EVENT_NAME: string = 'custom-event';

  it('test negative', function (done: MochaDone) {
    const emitter: events.EventEmitter = new events.EventEmitter();

    EventUtils.waitForOnce(emitter, EVENT_NAME, 1000,
      function () {
      }
    )
      .then(() => done(new Error('Should not be here')))
      .catch((err) => done());
  });

  it('test positive', function (done: MochaDone) {
    const emitter: events.EventEmitter = new events.EventEmitter();

    EventUtils.waitForOnce(emitter, EVENT_NAME, 1000,
      function () {
      }
    )
      .then(() => done())
      .catch((err) => done(new Error('Should not be here')));

    emitter.emit(EVENT_NAME);
  });

  it('test positive with handler', function (done: MochaDone) {
    const emitter: events.EventEmitter = new events.EventEmitter();

    let sideEffect: string = 'no';
    const eventHandler: Function = function (val: string) {
      sideEffect = val;
    };

    EventUtils.waitForOnce(emitter, EVENT_NAME, 1000, eventHandler)
      .then(() => {
        assert.equal(sideEffect, 'yes');
        done();
      })
      .catch((err) => done(new Error('Should not be here')));

    emitter.emit(EVENT_NAME, 'yes');
  });

});

