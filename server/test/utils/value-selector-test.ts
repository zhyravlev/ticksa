/// <reference path="../../app/typings/tsd.d.ts"/>

import util = require('util');
import mocha = require('mocha');
import chai = require('chai');
import _ = require('lodash');

import ValueSelector = require('../../app/utils/value-selector');

const assert = chai.assert;

describe('ValueSelectorTest', function () {

  it('check for string', function () {
    const dic: ValueSelector.KeyValueDictionary<string> = ValueSelector.KeyValueDictionary.forDictionary({
      foo: 'value'
    });
    const defaultValue: string = 'default';
    const selector: ValueSelector.ValueSelector<string> = new ValueSelector.ValueSelector<string>(dic, defaultValue);
    assert.equal(selector.select(undefined), defaultValue);
    assert.equal(selector.select(null), defaultValue);
    assert.equal(selector.select('bar'), defaultValue);
    assert.equal(selector.select('foo'), 'value');
  });

  it('check for number', function () {
    const dic: ValueSelector.KeyValueDictionary<number> = ValueSelector.KeyValueDictionary.forDictionary({
      foo: 1
    });
    const defaultValue: number = 0;
    const selector: ValueSelector.ValueSelector<number> = new ValueSelector.ValueSelector<number>(dic, defaultValue);
    assert.equal(selector.select(undefined), defaultValue);
    assert.equal(selector.select(null), defaultValue);
    assert.equal(selector.select('bar'), defaultValue);
    assert.equal(selector.select('foo'), 1);
  });

  describe('check for custom class', function () {

    interface SomeCustomClass {
      fieldString: string;
      fieldNumber: number;
    }

    it('check', function () {
      const dic: ValueSelector.KeyValueDictionary<SomeCustomClass> = ValueSelector.KeyValueDictionary.forDictionary({
        foo: {
          fieldString: 'value',
          fieldNumber: 1
        }
      });
      const defaultValue: SomeCustomClass = {
        fieldString: 'default',
        fieldNumber: 0
      };
      const selector: ValueSelector.ValueSelector<SomeCustomClass> = new ValueSelector.ValueSelector<SomeCustomClass>(dic, defaultValue);
      assert.deepEqual(selector.select(undefined), defaultValue);
      assert.deepEqual(selector.select(null), defaultValue);
      assert.deepEqual(selector.select('bar'), defaultValue);
      assert.deepEqual(selector.select('foo'),
        {
          fieldString: 'value',
          fieldNumber: 1
        });
    });
  });

  describe('check for enum', function () {

    enum SomeEnum {
      VAL = 1,
      DEFAULT = 0
    }

    it('check', function () {
      const dic: ValueSelector.KeyValueDictionary<SomeEnum> = ValueSelector.KeyValueDictionary.forDictionary({
        foo: SomeEnum.VAL
      });
      const defaultValue: SomeEnum = SomeEnum.DEFAULT;
      const selector: ValueSelector.ValueSelector<SomeEnum> = new ValueSelector.ValueSelector<SomeEnum>(dic, defaultValue);
      assert.deepEqual(selector.select(undefined), defaultValue);
      assert.deepEqual(selector.select(null), defaultValue);
      assert.deepEqual(selector.select('bar'), defaultValue);
      assert.deepEqual(selector.select('foo'), SomeEnum.VAL);
    });
  });

  describe('check for string based enum', function () {

    type SomeStringEnum = 'Value' | 'Default';

    it('check', function () {
      const dic: ValueSelector.KeyValueDictionary<SomeStringEnum> = ValueSelector.KeyValueDictionary.forDictionary<SomeStringEnum>({
        foo: 'Value'
      });
      const defaultValue: SomeStringEnum = 'Default';
      const selector: ValueSelector.ValueSelector<SomeStringEnum> = new ValueSelector.ValueSelector<SomeStringEnum>(dic, defaultValue);
      assert.deepEqual(selector.select(undefined), defaultValue);
      assert.deepEqual(selector.select(null), defaultValue);
      assert.deepEqual(selector.select('bar'), defaultValue);
      assert.deepEqual(selector.select('foo'), 'Value');
    });
  });

  describe('available keys', function () {
    const dic: ValueSelector.KeyValueDictionary<string> = ValueSelector.KeyValueDictionary.forDictionary({
      foo: 'fooValue',
      bar: 'barValue',
      baz: 'bazValue',
    });
    const defaultValue: string = 'defaultValue';

    it('list of available keys', function () {
      const selector: ValueSelector.ValueSelector<string> = new ValueSelector.ValueSelector<string>(dic, defaultValue);
      const availableKeys = selector.getKeys();
      assert.deepEqual(availableKeys, ['foo', 'bar', 'baz']);
    });

    it('list of available keys is not a hack to internal state', function () {
      const selector: ValueSelector.ValueSelector<string> = new ValueSelector.ValueSelector<string>(dic, defaultValue);
      const keysToChange = selector.getKeys();
      keysToChange.push('newKey');
      assert.deepEqual(keysToChange, ['foo', 'bar', 'baz', 'newKey']);

      const keysNotChanged = selector.getKeys();
      assert.deepEqual(keysNotChanged, ['foo', 'bar', 'baz']);
    });
  });

  describe('available values', function () {
    interface IVal {
      val: string
    }

    const dic: ValueSelector.KeyValueDictionary<IVal> = ValueSelector.KeyValueDictionary.forDictionary({
      foo: {val: 'fooValue'},
      bar: {val: 'barValue'},
      baz: {val: 'bazValue'},
    });
    const defaultValue: IVal = {val: 'defaultValue'};

    it('list of available values', function () {
      const selector: ValueSelector.ValueSelector<IVal> = new ValueSelector.ValueSelector<IVal>(dic, defaultValue);
      const availableValues = selector.getValues();
      assert.deepEqual(availableValues, [{val: 'fooValue'}, {val: 'barValue'}, {val: 'bazValue'}]);
    });

    it('list of available values IS A HACK to internal state of values', function () {
      const selector: ValueSelector.ValueSelector<IVal> = new ValueSelector.ValueSelector<IVal>(dic, defaultValue);
      const valuesToChange = selector.getValues();
      _.forEach(valuesToChange, v => {
        if (v.val == 'fooValue') {
          v.val = 'newValue';
        }
      });
      assert.deepEqual(valuesToChange, [{val: 'newValue'}, {val: 'barValue'}, {val: 'bazValue'}]);

      const valuesWereChanged = selector.getValues();
      assert.deepEqual(valuesWereChanged, [{val: 'newValue'}, {val: 'barValue'}, {val: 'bazValue'}]);
    });
  });

  describe('Dictionary stringifying', function () {
    interface IVal {
      val: string
    }

    const dicContainer: ValueSelector.KeyValueDictionaryContainer<IVal> = {
      foo: {val: 'fooValue'},
      bar: {val: 'barValue'},
      baz: {val: 'bazValue'},
    };
    const defaultValue: IVal = {val: 'defaultValue'};

    it('Only keys in toString', function() {
      const stringifier: ValueSelector.KeyValueDictionaryStringifier<IVal> = function(): ValueSelector.KeyValueDictionaryStringifierResult {
        return {
          needString: false
        }
      };

      const dictionary = ValueSelector.KeyValueDictionary.forStringifierAndDictionary(stringifier, dicContainer);
      assert.equal(dictionary.toString(), '{foo, bar, baz}');
    });

    it('Full info in toString', function() {
      const stringifier: ValueSelector.KeyValueDictionaryStringifier<IVal> = function(val: IVal): ValueSelector.KeyValueDictionaryStringifierResult {
        return {
          needString: true,
          stringValue: util.inspect(val)
        }
      };

      const dictionary = ValueSelector.KeyValueDictionary.forStringifierAndDictionary(stringifier, dicContainer);
      assert.equal(dictionary.toString(), "{foo: { val: 'fooValue' }, bar: { val: 'barValue' }, baz: { val: 'bazValue' }}");
    });

    it('Any custom data', function() {
      const stringifier: ValueSelector.KeyValueDictionaryStringifier<IVal> = function(val: IVal): ValueSelector.KeyValueDictionaryStringifierResult {
        return {
          needString: true,
          stringValue: 'SOME_ANY_DATA'
        }
      };

      const dictionary = ValueSelector.KeyValueDictionary.forStringifierAndDictionary(stringifier, dicContainer);
      assert.equal(dictionary.toString(), "{foo: SOME_ANY_DATA, bar: SOME_ANY_DATA, baz: SOME_ANY_DATA}");
    });
  });
});

