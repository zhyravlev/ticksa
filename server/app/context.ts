/// <reference path="typings/tsd.d.ts"/>
const logger = require('./logger/logger')(__filename);

import _ = require('lodash');
import fs = require('fs');
import Promise = require("bluebird");

import config = require('./config');
import Models = require('./models/models');

import JobService = require('./jobs/job-service');
import I18nService = require('./services/i18n-service');

export function init(): Promise<any> {
  return Promise.try(detectAssetsDir)
    .then((assetsDir: string) => {
      config.env().cdn.urlWithTimestamp = config.env().cdn.url + '/' + assetsDir;
      return I18nService.initLocalization(assetsDir);
    })
    .then(() => config.isTest() ? Promise.resolve() : Models.initialize())
    .then(() => JobService.start())
    .catch((err) => {
      logger.error('Error while initialization: ' + err);
      return Promise.reject(err);
    })
  ;
}

function detectAssetsDir(): Promise<string> {
  const webappStaticDir: string = __dirname + '/public';

  return Promise.promisify(fs.readdir)(webappStaticDir)
    .then(function (files: Array<string>) {
      const times = _.filter(files, function (file: string) {
        return !isNaN(parseInt(file));
      });

      if (times.length === 0) {
        throw new Error("Webapp path not found: " + webappStaticDir);
      }

      return times[times.length - 1];
    }).catch(function (err) {
      if (config.isLocal() || config.isTest()) {
        logger.warn('Ignored error', err);
        return Promise.resolve();
      }
      return Promise.reject(err);
    })
}