/// <reference path="../typings/tsd.d.ts"/>

const logger = require('../logger/logger')(__filename);
import {InterceptorGlobal, InterceptorInterface} from 'routing-controllers';

@InterceptorGlobal({priority: 1})
export default class ResultTypeInterceptor implements InterceptorInterface {

  intercept(request: any, response: any, content: any) {
    const Bluebird = require('bluebird');

    if (content instanceof Bluebird) {
      logger.warn('Please check response (type: bluebird) on request to: ' + request.originalUrl);
      return new Promise((resolve, reject) => {
        content
          .then(data => resolve(data))
          .catch(err => reject(err));
      });
    }

    if (typeof content === 'string') {
      if (!request.originalUrl.startsWith('/api/aws/generate-signature')) {
        if (content !== 'OK') {
          logger.warn('Please check response (type: string) on request to: ' + request.originalUrl);
        }
      }
    }

    return content;
  }
}
