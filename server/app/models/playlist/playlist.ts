/// <reference path="../../typings/tsd.d.ts"/>

import * as Sequelize from 'sequelize';
import * as Ticksa from 'ticksa';
import {
  Content,
  Image,
  Tickser
} from '../models';

module Playlist {

  export interface Attributes {
    id?: number;
    name: string;
    tickserId: number;
    published: boolean;
    deleted: boolean;
    coverImageId: number;
    backgroundImageId: number;
    orderedContents: number[];
    createdAt?: Date;
    updatedAt?: Date;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    tickser?: Tickser.Instance;
    backgroundImage?: Image.Instance;
    coverImage?: Image.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions): Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    countByCoverImageId(coverImageId: number): Promise<number>;
    countByBackgroundImageId(backgroundImageId: number): Promise<number>;
    findAllByTickserId(tickserId: number): Promise<Instance[]>;
  }

  export class Scopes {
    public static IS_PUBLISHED: string = 'IS_PUBLISHED';
    public static IS_NOT_DELETED: string = 'IS_NOT_DELETED';
    public static INCLUDE_COVER_IMAGE: string = 'INCLUDE_COVER_IMAGE';
    public static INCLUDE_BACKGROUND_IMAGE: string = 'INCLUDE_BACKGROUND_IMAGE';
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    const methods: ClassMethods = {
      countByCoverImageId: function (coverImageId: number): Promise<number> {
        return Model.count({
          where: {
            coverImageId: coverImageId
          }
        });
      },
      countByBackgroundImageId: function (backgroundImageId: number): Promise<number> {
        return Model.count({
          where: {
            backgroundImageId: backgroundImageId
          }
        });
      },
      findAllByTickserId: function (tickserId) {
        return this.findAll({
          where: {
            tickserId: tickserId
          }
        });
      },
      associate: function (models) {
        Model.belongsTo(models.Tickser.Model, {
          as: 'tickser',
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });
        Model.belongsTo(models.Image.Model, {
          as: 'coverImage',
          foreignKey: {name: 'coverImageId', field: 'cover_image_id'}
        });
        Model.belongsTo(models.Image.Model, {
          as: 'backgroundImage',
          foreignKey: {name: 'backgroundImageId', field: 'background_image_id'}
        });
      },
      addScopes: function (models) {
        Model.addScope(Scopes.IS_PUBLISHED, {
          where: {
            published: true
          }
        });
        Model.addScope(Scopes.IS_NOT_DELETED, {
          where: {
            deleted: false
          }
        });
        Model.addScope(Scopes.INCLUDE_COVER_IMAGE, {
          include: [
            {
              model: models.Image.Model.scope(models.Image.Scopes.INCLUDE_FILE),
              as: 'coverImage',
            }
          ]
        });
        Model.addScope(Scopes.INCLUDE_BACKGROUND_IMAGE, {
          include: [
            {
              model: models.Image.Model.scope(models.Image.Scopes.INCLUDE_FILE),
              as: 'backgroundImage',
            }
          ]
        });
      }
    };

    Model = <Class> connection.define<Instance, Attributes>('Playlist', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
      },
      name: {
        type: Sequelize.TEXT,
        field: 'name'
      },
      published: {
        type: Sequelize.BOOLEAN,
        field: 'published'
      },
      deleted: {
        type: Sequelize.BOOLEAN,
        field: 'deleted'
      },
      coverImageId: {
        type: Sequelize.BIGINT,
        field: 'cover_image_id'
      },
      backgroundImageId: {
        type: Sequelize.BIGINT,
        field: 'background_image_id'
      },
      orderedContents: {
        type: Sequelize.ARRAY(Sequelize.BIGINT),
        field: 'ordered_contents'
      },
      createdAt: {
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: methods
    });
    return Model;
  }
}

export = Playlist;