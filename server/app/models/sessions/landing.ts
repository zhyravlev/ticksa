/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Tickser = Models.Tickser;

module Landing {
  export interface Attributes {
    id?: number;
    tickserId?: number;
    date?: Date;
    url?: string;
    ipAddress?: string;
    ipLocation?: string;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    registerAnonymousLanding(url: string, ip: string, location: string) : Promise<Instance>;
    registerTickserLanding(tickser: Tickser.Instance, url: string, ip: string, location: string) : Promise<Instance>;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('Landing', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id'
      },
      date: {
        type: Sequelize.DATE,
        field: 'date'
      },
      url: {
        type: Sequelize.TEXT,
        field: 'url'
      },
      ipAddress: {
        type: Sequelize.STRING(50),
        field: 'ip_address'
      },
      ipLocation: {
        type: Sequelize.STRING(50),
        field: 'ip_location'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (index) {
          Model.belongsTo(index.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
        },
        registerAnonymousLanding: function (url, ip, location) {
          return this.create({
            tickserId: null,
            date: new Date(),
            url: url,
            ipAddress: ip,
            ipLocation: location
          });
        },
        registerTickserLanding: function (tickser, url, ip, location) {
          return this.create({
            tickserId: tickser.id,
            date: new Date(),
            url: url,
            ipAddress: ip,
            ipLocation: location
          });
        }
      }
    });
    return Model;
  }
}

export = Landing;

