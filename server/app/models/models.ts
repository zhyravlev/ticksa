/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import Sequelize = require('sequelize');
import Promise = require('bluebird');
import Ticksa = require('ticksa');

import connection = require('../connection');
import config = require('../config');

export import Account = require('./financial/account');
export import CashIO = require('./financial/cash-io');
export import Currency = require('./financial/currency');
export import TicksTransaction = require('./financial/ticks-transaction');
export import TicksAmount = require('./financial/ticks-amount');
export import Country = require('./financial/country');

export import LemonwayWallet = require('./financial/lemonway/lemonway-wallet');
export import LemonwayCashIn = require('./financial/lemonway/lemonway-cash-in');
export import LemonwayCashCallback = require('./financial/lemonway/lemonway-cash-callback');
export import InfinCashIn = require('./financial/infin/infin-cash-in');
export import InfinCallback = require('./financial/infin/infin-callback');
export import PayPalCashIn = require('./financial/paypal/paypal-cash-in');
export import PayPalReturn = require('./financial/paypal/paypal-return');
export import ExchangeRate = require('./financial/exchange-rate');

export import Notification = require('./notification');

export import Tickser = require('./tickser');
export import TickserSocial = require('./tickser-social');
export import TickserReferral = require('./tickser-referral');
export import TickserChannel = require('./tickser-channel');
export import TickserVoucher = require('./voucher/tickser-voucher');
export import TickserChannelRank = require('./tickser-channel-rank');
export import TickserCondition = require('./tickser-condition');

export import Channel = require('./channel/channel');
export import ChannelInvite = require('./channel/channel-invite');
export import ChannelSubscription = require('./channel/channel-subscription');
export import ChannelRevenue = require('./channel/channel-revenue');

export import Content = require('./content/content');
export import ContentText = require('./content/content-text');
export import ContentImage = require('./content/content-image');
export import ContentLink = require('./content/content-link');
export import ContentVideo = require('./content/content-video');
export import ContentAccess = require('./content/content-access');
export import ContentTag = require('./content/content-tag');
export import History = require('./content/history');
export import Tag = require('./content/tag');
export import Category = require('./content/category');
export import Language = require('./language');
export import Condition = require('./content/condition');
export import ContentCondition = require('./content/content-condition');

export import Image = require('./library/image');
export import Video = require('./library/video');
export import File = require('./library/file');
export import VideoFile = require('./library/video-file');
export import VideoJob = require('./library/video-job');

export import Feed = require('./feed/feed');

export import Voucher = require('./voucher/voucher');
export import VoucherActivity = require('./voucher/voucher-activity');
export import GlobalConfig = require('./global-config');
export import AdminComment = require('./admin/comment');
export import Landing = require('./sessions/landing');

export import EmailRecipient = require('./email/email-recipient');
export import EmailFilter = require('./email/email-filter');

export import LoggedEvent = require('./event/logged-event');

export import Playlist = require('./playlist/playlist');

function initializeModels() {
  var modelsList: Ticksa.Model<any, any>[] = [];

  modelsList.push(Account.initialize(connection));
  modelsList.push(CashIO.initialize(connection));
  modelsList.push(Currency.initialize(connection));
  modelsList.push(LemonwayWallet.initialize(connection));
  modelsList.push(LemonwayCashIn.initialize(connection));
  modelsList.push(LemonwayCashCallback.initialize(connection));
  modelsList.push(Notification.initialize(connection));
  modelsList.push(Country.initialize(connection));

  modelsList.push(Tickser.initialize(connection));
  modelsList.push(TickserSocial.initialize(connection));
  modelsList.push(TickserReferral.initialize(connection));
  modelsList.push(TickserChannel.initialize(connection));
  modelsList.push(TickserVoucher.initialize(connection));
  modelsList.push(TickserChannelRank.initialize(connection));

  modelsList.push(TicksTransaction.initialize(connection));
  modelsList.push(TicksAmount.initialize(connection));
  modelsList.push(ExchangeRate.initialize(connection));
  modelsList.push(InfinCashIn.initialize(connection));
  modelsList.push(InfinCallback.initialize(connection));
  modelsList.push(PayPalCashIn.initialize(connection));
  modelsList.push(PayPalReturn.initialize(connection));

  modelsList.push(Content.initialize(connection));
  modelsList.push(ContentText.initialize(connection));
  modelsList.push(ContentImage.initialize(connection));
  modelsList.push(ContentLink.initialize(connection));
  modelsList.push(ContentVideo.initialize(connection));
  modelsList.push(ContentAccess.initialize(connection));
  modelsList.push(ContentTag.initialize(connection));
  modelsList.push(History.initialize(connection));
  modelsList.push(Tag.initialize(connection));
  modelsList.push(Category.initialize(connection));
  modelsList.push(Language.initialize(connection));
  modelsList.push(Condition.initialize(connection));
  modelsList.push(ContentCondition.initialize(connection));
  modelsList.push(TickserCondition.initialize(connection));

  modelsList.push(Image.initialize(connection));
  modelsList.push(Video.initialize(connection));
  modelsList.push(File.initialize(connection));
  modelsList.push(VideoFile.initialize(connection));
  modelsList.push(VideoJob.initialize(connection));

  modelsList.push(Channel.initialize(connection));
  modelsList.push(ChannelInvite.initialize(connection));
  modelsList.push(ChannelSubscription.initialize(connection));
  modelsList.push(ChannelRevenue.initialize(connection));

  modelsList.push(Feed.initialize(connection));
  modelsList.push(GlobalConfig.initialize(connection));

  modelsList.push(Voucher.initialize(connection));
  modelsList.push(VoucherActivity.initialize(connection));

  modelsList.push(AdminComment.initialize(connection));
  modelsList.push(Landing.initialize(connection));

  modelsList.push(EmailRecipient.initialize(connection));
  modelsList.push(EmailFilter.initialize(connection));

  modelsList.push(LoggedEvent.initialize(connection));

  modelsList.push(Playlist.initialize(connection));

  modelsList.forEach(function (model) {
    var models = module.exports;

    model.associate(models);
    if (model.hasOwnProperty('addScopes')) {
      model.addScopes(models);
    }
  });
}

var forceSync = process.env.resetDB || config.env().database.forceSync || false;
var initializeSystemData = process.env.initDB || config.env().database.initDB || false;

export function initialize(): Promise<any> {
  initializeModels();
  let init = require('../init/init');

  return connection.sync({force: forceSync}).then(function () {
    logger.info('Sequelize sync done');

    if (initializeSystemData) {
      logger.info('Initialize system started');
      return init.initData();
    }
  }).then(function () {
    return init.initGlobalConfig();
  }).then(function () {
    logger.info('Initialize database done');
  });
}

export function getConnection(): Sequelize.Sequelize {
  return connection;
}
