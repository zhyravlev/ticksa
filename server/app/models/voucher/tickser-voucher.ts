/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Content = Models.Content;
import Tickser = Models.Tickser;
import Video = Models.Video;
import Image = Models.Image;

module TickserVoucher {
  export interface Attributes {
    tickserId?: number;
    voucherId?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('TickserVoucher', {
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
        primaryKey: true
      },
      voucherId: {
        type: Sequelize.BIGINT,
        field: 'voucher_id',
        primaryKey: true
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {

        }
      }
    });
    return Model;
  }
}

export = TickserVoucher;