/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import TickserVoucher = Models.TickserVoucher;
import Tickser = Models.Tickser;

module Voucher {
  export interface Attributes {
    id?: number;
    code?: string;
    ticksAmount?: number;
    startDate?: Date;
    stopDate?: Date;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    tickserVouchers?: TickserVoucher.Instance[];
    ticksers?: Tickser.Instance[];
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('Voucher', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: Sequelize.STRING(255),
        field: 'code',
        allowNull: false,
        unique: true
      },
      ticksAmount: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'ticks_amount',
        allowNull: false
      },
      startDate: {
        type: Sequelize.DATE,
        field: 'start_date'
      },
      stopDate: {
        type: Sequelize.DATE,
        field: 'stop_date'
      },
      series: {
        type: Sequelize.STRING(25),
        field: 'series'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.hasMany(models.TickserVoucher.Model, {as: 'tickserVouchers', foreignKey: {name: 'voucherId', field: 'voucher_id'}});
          Model.belongsToMany(models.Tickser.Model, {
            as: 'ticksers',
            through: models.TickserVoucher.Model,
            foreignKey: {name: 'voucherId', field: 'voucher_id'}
          });
        }
      }
    });
    return Model;
  }
}

export = Voucher;