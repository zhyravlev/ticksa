/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Voucher = Models.Voucher;
import Tickser = Models.Tickser;

module VoucherActivity {

  export enum Type {
    CHECK = 0,
    APPLY = 1
  }

  export enum Result {
    VALID_CODE = 0,
    INVALID_CODE = 1,
    BANNED = 2,
    SUCCESSFULLY_APPLIED = 3,
    DATE_LIMITED = 4,
    ALREADY_APPLIED = 5,
    VOUCHERS_COUNT_EXCEEDED = 6,
    ERROR = 7,

    // Do not delete it!
    // Used for historical reasons. not to lose stored information during manual changes.
    // Such values used not in codebase but in database.
    MANUALLY_CHANGED_WAS_INVALID_CODE = 101,
    MANUALLY_CHANGED_WAS_DATE_LIMITED = 104
  }

  export interface Attributes {
    id?: number;
    tickserId?: number;
    code?: string;
    voucherId?: number;
    type?: Type;
    result?: Result;
    error?: string;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    voucher?: Voucher.Instance;
    tickser?: Tickser.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('VoucherActivity', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
        allowNull: false
      },
      code: {
        type: Sequelize.STRING(255),
        field: 'code'
      },
      voucherId: {
        type: Sequelize.BIGINT,
        field: 'voucher_id'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      result: {
        type: Sequelize.INTEGER,
        field: 'result'
      },
      error: {
        type: Sequelize.STRING(255),
        field: 'error'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
          Model.belongsTo(models.Voucher.Model, {as: 'voucher', foreignKey: {name: 'voucherId', field: 'voucher_id'}});
        }
      }
    });
    return Model;
  }
}

export = VoucherActivity;