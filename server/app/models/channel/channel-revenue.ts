/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import moment = require('moment');

import Models = require('../models');
import TickserChannel = Models.TickserChannel;
import Tickser = Models.Tickser;
import Channel = Models.Channel;
import Account = Models.Account;

module ChannelRevenue {

  export interface Attributes {
    tickserId?: number;
    channelId?: number;
    accountId?: number;
    percent?: number;
    expiryDate?: Date;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    channel?: Channel.Instance;
    tickser?: Tickser.Instance;
    account?: Account.Instance;
    isExpired(): boolean;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions): Class;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('ChannelRevenue', {
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
        primaryKey: true
      },
      channelId: {
        type: Sequelize.BIGINT,
        field: 'channel_id',
        primaryKey: true
      },
      accountId: {
        type: Sequelize.BIGINT,
        field: 'account_id',
        primaryKey: true
      },
      percent: {
        type: Sequelize.DECIMAL(3, 1),
        field: 'percent'
      },
      expiryDate: {
        type: Sequelize.DATE,
        field: 'expiry_date'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      scopes: {},
      instanceMethods: {
        isExpired: function (): boolean {
          return this.expiryDate ? moment().isAfter(this.expiryDate) : false;
        }
      },
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Tickser.Model, {
            as: 'tickser',
            foreignKey: {name: 'tickserId', field: 'tickser_id'}
          });
          Model.belongsTo(models.Channel.Model, {
            as: 'channel',
            foreignKey: {name: 'channelId', field: 'channel_id'}
          });
          Model.belongsTo(models.Channel.Model, {
            as: 'account',
            foreignKey: {name: 'accountId', field: 'account_id'}
          });
        },
      }
    });
    return Model;
  }
}

export = ChannelRevenue;