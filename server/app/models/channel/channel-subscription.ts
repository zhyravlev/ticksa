/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Tickser = Models.Tickser;
import Channel = Models.Channel;

module ChannelInvite {
  export enum OwnerType  {
    CREATOR = 0,
    MANAGER = 1,
    PARTICIPANT = 2
  }
  export interface Attributes {
    tickserId?: number;
    channelId?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('ChannelSubscription', {
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
        primaryKey: true
      },
      channelId: {
        type: Sequelize.BIGINT,
        field: 'channel_id',
        primaryKey: true
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function(models) {

        }
      }
    });
    return Model;
  }
}

export = ChannelInvite;