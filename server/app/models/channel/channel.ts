/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import TickserChannel = Models.TickserChannel;
import Tickser = Models.Tickser;
import Content = Models.Content;
import ChannelInvite = Models.ChannelInvite;
import ChannelSubscription = Models.ChannelSubscription;
import ChannelRevenue = Models.ChannelRevenue;
import Image = Models.Image;

module Channel {

  export enum Status  {
    ACTIVE = 0,
    DEACTIVATED = 1,
    SUSPENDED = 2
  }

  export enum Type {
    PUBLIC = 0,
    PRIVATE = 1
  }

  export interface Where extends Sequelize.WhereOptions {
    id?: number;
    status?: Channel.Status;
    type?: Channel.Type;
  }

  export interface Attributes {
    id?: number;
    name?: string;
    description?: string;
    type?: Type;
    status?: Status;
    backgroundImageId?: number;
    sortedContent?: number[];
    verified?: boolean;
    shareIncome?: boolean;
    createdAt?: Date;
    updatedAt?: Date;
    coverImageId?: number;
    showRankedList?: boolean;
    showRankedListDate?: Date;
    allowVoting?: boolean;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    tickserChannels?: TickserChannel.Instance[];
    ticksers?: Tickser.Instance[];
    invites?: ChannelInvite.Instance[];
    contents?: Content.Instance[];
    subscribers?: Tickser.Instance[];
    subscriptions?: ChannelSubscription.Instance[];
    revenues?: ChannelRevenue.Instance[];
    backgroundImage?: Image.Instance;
    coverImage?: Image.Instance;

    setRevenues: Sequelize.HasManySetAssociationsMixin<ChannelRevenue.Instance, number>;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions): Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    countByCoverImageId(coverImageId: number): Promise<number>;
    countByChannelBackgroundId(backgroundImageId: number): Promise<number>;
    findAllByChannelBackgroundIds(backgroundImageIds: number[]): Promise<Instance[]>;
    findAllByCoverImageIds(coverImageIds: number[]): Promise<Instance[]>;
  }

  export class Scopes {
    public static IS_ACTIVE: string = 'isActive';  // ToDo [d.borisov] use this constant instead of string 'isActive'
    public static IS_PUBLIC: string = 'isPublic';  // ToDo [d.borisov] use this constant instead of string 'isPublic'
    public static REVENUES: string = 'REVENUES';
    public static INCLUDE_REVENUES_WITH_TICKSERS: string = 'INCLUDE_REVENUES_WITH_TICKSERS';
    public static INCLUDE_COVER_IMAGE: string = 'INCLUDE_COVER_IMAGE';
    public static INCLUDE_BACKGROUND_IMAGE: string = 'INCLUDE_BACKGROUND_IMAGE';
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    var methods: ClassMethods = {
      countByCoverImageId: function (coverImageId: number): Promise<number> {
        return Model.count({
          where: {
            coverImageId: coverImageId
          }
        });
      },
      countByChannelBackgroundId: function (backgroundImageId: number): Promise<number> {
        return Model.count({
          where: {
            backgroundImageId: backgroundImageId
          }
        });
      },
      findAllByChannelBackgroundIds: function (backgroundImageIds: number[]): Promise<Channel.Instance[]> {
        return Model.findAll({
          where: {
            backgroundImageId: {
              $in: backgroundImageIds
            }
          },
          attributes: ['backgroundImageId']
        });
      },

      findAllByCoverImageIds: function (coverImageIds: number[]): Promise<Channel.Instance[]> {
        return Model.findAll({
          where: {
            coverImageId: {
              $in: coverImageIds
            }
          },
          attributes: ['coverImageId']
        });
      },
      associate: function (models) {
        Model.hasMany(models.TickserChannel.Model, {
          as: 'tickserChannels',
          foreignKey: {name: 'channelId', field: 'channel_id'}
        });
        Model.belongsToMany(models.Tickser.Model, {
          as: 'ticksers',
          through: models.TickserChannel.Model,
          foreignKey: {name: 'channelId', field: 'channel_id'}
        });
        Model.hasMany(models.ChannelInvite.Model, {
          as: 'invites',
          foreignKey: {name: 'channelId', field: 'channel_id'}
        });
        Model.hasMany(models.Content.Model, {as: 'contents', foreignKey: {name: 'channelId', field: 'channel_id'}});
        Model.hasMany(models.ChannelRevenue.Model, {
          as: 'revenues',
          foreignKey: {name: 'channelId', field: 'channel_id'}
        });
        Model.belongsToMany(models.Tickser.Model, {
          as: 'subscribers',
          through: models.ChannelSubscription.Model,
          foreignKey: {name: 'channelId', field: 'channel_id'}
        });
        Model.hasMany(models.ChannelSubscription.Model, {
          as: 'subscriptions',
          foreignKey: {name: 'channelId', field: 'channel_id'}
        });
        Model.belongsTo(models.Image.Model, {
          as: 'backgroundImage',
          foreignKey: {name: 'backgroundImageId', field: 'background_image_id'}
        });
        Model.belongsTo(models.Image.Model, {
          as: 'coverImage',
          foreignKey: {name: 'coverImageId', field: 'cover_image_id'}
        });
      },
      addScopes: function (models) {
        Model.addScope(Scopes.IS_ACTIVE, {
          where: {
            status: Status.ACTIVE
          }
        });
        Model.addScope(Scopes.IS_PUBLIC, {
          where: {
            type: Type.PUBLIC
          }
        });
        Model.addScope(Scopes.REVENUES, {
          include: [
            {
              required: false,
              model: models.ChannelRevenue.Model,
              as: 'revenues'
            }
          ]
        });
        Model.addScope(Scopes.INCLUDE_REVENUES_WITH_TICKSERS, {
          include: [
            {
              required: false,
              model: models.ChannelRevenue.Model,
              as: 'revenues',
              include: [
                {
                  required: false,
                  model: models.Tickser.Model,
                  as: 'tickser'
                }
              ]
            }
          ]
        });
        Model.addScope(Scopes.INCLUDE_COVER_IMAGE, {
          include: [
            {
              model: models.Image.Model.scope(models.Image.Scopes.INCLUDE_FILE),
              as: 'coverImage',
            }
          ]
        });
        Model.addScope(Scopes.INCLUDE_BACKGROUND_IMAGE, {
          include: [
            {
              model: models.Image.Model.scope(models.Image.Scopes.INCLUDE_FILE),
              as: 'backgroundImage',
            }
          ]
        });
      }
    };

    Model = <Class> connection.define('Channel', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING(50),
        field: 'name'
      },
      description: {
        type: Sequelize.TEXT,
        field: 'description'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      backgroundImageId: {
        type: Sequelize.BIGINT,
        field: 'background_image_id'
      },
      sortedContent: {
        type: Sequelize.JSONB,
        field: 'sorted_content'
      },
      verified: {
        type: Sequelize.BOOLEAN,
        field: 'verified',
        defaultValue: false
      },
      shareIncome: {
        type: Sequelize.BOOLEAN,
        field: 'share_income'
      },
      createdAt: {
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at'
      },
      coverImageId: {
        type: Sequelize.BIGINT,
        field: 'cover_image_id'
      },
      showRankedList: {
        type: Sequelize.BOOLEAN,
        field: 'show_ranked_list',
        defaultValue: false,
      },
      showRankedListDate: {
        type: Sequelize.DATE,
        field: 'show_ranked_list_date',
      },
      allowVoting: {
        type: Sequelize.BOOLEAN,
        field: 'allow_voting',
        defaultValue: false,
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: methods
    });

    return Model;
  }
}

export = Channel;