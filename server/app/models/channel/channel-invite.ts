/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Tickser = Models.Tickser;
import Channel = Models.Channel;

module ChannelInvite {
  export enum OwnerType  {
    CREATOR = 0,
    MANAGER = 1,
    PARTICIPANT = 2
  }
  export interface Attributes {
    id?: number;
    email?: string;
    ownerType?: OwnerType;
    channelId?: number;
    tickserId?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    tickser?: Tickser.Instance;
    channel?: Channel.Instance;
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('ChannelInvite', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      email: {
        type: Sequelize.STRING,
        field: 'email'
      },
      ownerType: {
        type: Sequelize.INTEGER,
        field: 'owner_type'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Channel.Model, {
            as: 'channel',
            foreignKey: {name: 'channelId', field: 'channel_id'}
          });
          Model.belongsTo(models.Tickser.Model, {
            as: 'tickser',
            foreignKey: {name: 'tickserId', field: 'tickser_id'}
          });
        }
      }
    });
    return Model;
  }
}

export = ChannelInvite;