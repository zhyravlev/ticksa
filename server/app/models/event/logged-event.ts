/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import _ = require('lodash');

import Models = require('../../models/models');
import Content = Models.Content;
import Channel = Models.Channel;
import Tickser = Models.Tickser;

module LoggedEvent {

  export interface Event {
    type: string;
    data: any
  }

  export class EventType {
    public static CONTENT_CHANNEL_CHANGED = 'CONTENT_CHANNEL_CHANGED';
    public static TICKER_CHANNEL_RANK_SHOULD_BE_NEGATIVE = 'TICKER_CHANNEL_RANK_SHOULD_BE_NEGATIVE';
  }

  export class EventBuilder {

    public static contentChannelChanged(contentId: number, oldChannelId: number, newChannelId: number): Event {
      return {
        type: EventType.CONTENT_CHANNEL_CHANGED,
        data: {
          content_id: contentId,
          old_channe_id: oldChannelId,
          new_channel_id: newChannelId
        }
      }
    }

    public static tickerChannelRankShouldBeNegative(tickserId: number, channelId: number, negativeBalance: number): Event {
      return {
        type: EventType.TICKER_CHANNEL_RANK_SHOULD_BE_NEGATIVE,
        data: {
          tickser_id: tickserId,
          channel_id: channelId,
          balance: negativeBalance
        }
      }
    }
  }

  export class Scopes {
  }

  export interface Attributes {
    event: Event;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    let methods: ClassMethods = {
      associate: function (models) {
      },
      addScopes: function (models) {
      }
    };

    Model = <Class> connection.define('LoggedEvent', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      event: {
        type: Sequelize.JSONB,
        field: 'event',
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: methods
    });
    return Model;
  }
}

export = LoggedEvent;