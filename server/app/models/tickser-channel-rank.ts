/// <reference path="../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import _ = require('lodash');

import Models = require('../models/models');
import Tickser = Models.Tickser;
import Channel = Models.Channel;

module TickserChannelRank {

  export class Scopes {
    public static INCLUDE_TICKSER = 'INCLUDE_TICKSER';
    public static INCLUDE_CHANNEL = 'INCLUDE_CHANNEL';
    public static INCLUDE_ALL = [Scopes.INCLUDE_TICKSER, Scopes.INCLUDE_CHANNEL];
  }

  export interface Attributes {
    earnedTicks?: number | Sequelize.literal;
    tickserId?: number;
    channelId?: number;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    tickser?: Tickser.Instance;
    channel?: Channel.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    findAllByChannelId(channelId: number, options?: Sequelize.FindOptions): Promise<Instance[]>;
    findOneByTickserIdAndChannelId(tickserId: number, channelId: number, options?: Sequelize.FindOptions): Promise<Instance>;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    let methods: ClassMethods = {
      associate: function (models) {
        Model.belongsTo(models.Tickser.Model, {
          as: 'tickser',
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });
        Model.belongsTo(models.Channel.Model, {
          as: 'channel',
          foreignKey: {name: 'channelId', field: 'channel_id'}
        });
      },
      addScopes: function (models) {
        Model.addScope(Scopes.INCLUDE_TICKSER, function () {
          return {
            include: [{
              model: models.Tickser.Model.scope(models.Tickser.Scopes.INCLUDE_PROFILE_IMAGE),
              as: 'tickser',
            }],
          }
        });
        Model.addScope(Scopes.INCLUDE_CHANNEL, function () {
          return {
            include: [{
              model: models.Channel.Model,
              as: 'channel',
            }],
          }
        });
      },
      findAllByChannelId: function (channelId: number, options?: Sequelize.FindOptions): Promise<Instance[]> {
        let opt = _.merge({
          where: {
            channelId: channelId
          }
        }, options);
        return this.findAll(opt);
      },
      findOneByTickserIdAndChannelId: function (tickserId: number, channelId: number, options?: Sequelize.FindOptions): Promise<Instance> {
        let opt = _.merge({
          where: {
            tickserId: tickserId,
            channelId: channelId,
          }
        }, options);
        return this.findOne(opt);
      }
    };

    Model = <Class> connection.define('TickserChannelRank', {
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
        primaryKey: true
      },
      channelId: {
        type: Sequelize.BIGINT,
        field: 'channel_id',
        primaryKey: true
      },
      earnedTicks: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'earned_ticks',
        allowNull: false
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: methods
    });
    return Model;
  }
}

export = TickserChannelRank;