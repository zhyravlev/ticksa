/// <reference path="../typings/tsd.d.ts"/>
import _ = require('lodash');
import Sequelize = require('sequelize');
import Promise = require('bluebird');
import Ticksa = require('ticksa');
import Models = require('./models');
import Condition = Models.Condition;
import Tickser = Models.Tickser;

module TickserCondition {

  export class Scopes {
    public static INCLUDE_CONDITION: string = 'includeCondition';
  }

  export interface Attributes {
    tickserId?: number;
    conditionId?: number;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    tickser?: Tickser.Instance;
    condition?: Condition.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    findConditionsForTickser(tickserId: number): Promise<Instance[]>
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {

    var methods: ClassMethods = {
      findConditionsForTickser: function (tickserId: number): Promise<Instance[]> {
        return this.findAll({
          where: {
            tickserId: tickserId
          }
        })
      },
      addScopes: function (models) {
        Model.addScope(Scopes.INCLUDE_CONDITION, function () {
          return {
            include: [
              {
                model: models.Condition.Model,
                as: 'condition',
              }
            ]
          }
        });
      },
      associate: function (models) {
        Model.belongsTo(models.Condition.Model, {as: 'condition', foreignKey: {name: 'conditionId', field: 'condition_id'}});
        Model.belongsTo(models.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
      }
    };

    Model = <Class> connection.define<Instance, Attributes>('TickserCondition', {
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
        primaryKey: true
      },
      conditionId: {
        type: Sequelize.BIGINT,
        field: 'condition_id',
        primaryKey: true
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: methods,
      indexes: [{
        name: 'TickserCondition_tickser_id_idx',
        fields: ['tickser_id']
      }]
    });
    return Model;
  }

}

export = TickserCondition;