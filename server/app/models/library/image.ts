/// <reference path="../../typings/tsd.d.ts"/>
import _ = require('lodash');
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import File = Models.File;
import Tickser = Models.Tickser;

module Image {

  export class Scopes {
    public static INCLUDE_FILE: string = 'file';
    public static MAIN_ATTRIBUTES_AND_FILE_MAIN_ATTRIBUTES: string = 'MAIN_ATTRIBUTES_AND_FILE_MAIN_ATTRIBUTES';
  }

  export interface Attributes {
    id?: number;
    name?: string;
    tickserId?: number;
    fileId?: number;
    created_at?: Date;
    updated_at?: Date;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    file?: File.Instance;
    tickser?: Tickser.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    findAllByIds(ids: number[], options?: Sequelize.FindOptions): Promise<Instance[]>
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    let classMethods = {
      findAllByIds: function (ids: number[], options?: Sequelize.FindOptions): Promise<Instance[]> {
        var whereIn = {
          where: {
            id: {$in: ids}
          }
        };
        var opts = _.merge(whereIn, options);
        return this.findAll(opts)
      },
      associate: function (models) {
        Model.belongsTo(models.File.Model, {as: 'file', foreignKey: {name: 'fileId', field: 'file_id'}});
        Model.belongsTo(models.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
      },
      addScopes: function (models: any) {
        Model.addScope(Scopes.INCLUDE_FILE, {
          include: [
            {
              model: models.File.Model,
              as: 'file'
            }
          ]
        });
        Model.addScope(Scopes.MAIN_ATTRIBUTES_AND_FILE_MAIN_ATTRIBUTES, {
          attributes: ['id', 'name', 'created_at', 'updated_at'],
          include: [
            {
              required: true,
              model: models.File.Model,
              as: 'file',
              attributes: ['key', 'name']
            }
          ]
        });
      }
    };
    Model = <Class> connection.define<Instance, Attributes>('Image', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.TEXT,
        field: 'name'
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
        allowNull: false
      },
      fileId: {
        type: Sequelize.BIGINT,
        field: 'file_id',
        allowNull: false
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: classMethods
    });
    return Model;
  }
}

export = Image;