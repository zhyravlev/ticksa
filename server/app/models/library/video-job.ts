/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Video = Models.Video;

module VideoJob {
  export enum Status {
    SUBMITTED = 0,
    STARTED = 1,
    COMPLETE = 2,
    ERROR = 3
  }

  export interface Attributes {
    id?: number;
    videoId?: number;
    transcoderJobId?: number;
    submittedAt?: Date;
    startedAt?: Date;
    completedAt?: Date;
    errorAt?: Date;
    status?: Status;
    errorMessage?: string;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    video?: Video.Instance;
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('VideoJob', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      videoId: {
        type: Sequelize.BIGINT,
        field: 'video_id'
      },
      transcoderJobId: {
        type: Sequelize.TEXT,
        field: 'transcoder_job_id'
      },
      submittedAt: {
        type: Sequelize.DATE,
        field: 'submitted_at'
      },
      startedAt: {
        type: Sequelize.DATE,
        field: 'started_at'
      },
      completedAt: {
        type: Sequelize.DATE,
        field: 'completed_at'
      },
      errorAt: {
        type: Sequelize.DATE,
        field: 'error_at'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      errorMessage: {
        type: Sequelize.TEXT,
        field: 'error_message'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Video.Model, {as: 'video', foreignKey: {name: 'videoId', field: 'video_id'}});
        }
      }
    });
    return Model;
  }
}

export = VideoJob;