/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');

module File {

  export interface Attributes {
    id?: number;
    key?: string;
    name?: string;
    extension?: string;
    size?: number;
    isUploaded?: boolean;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {

  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('File', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      key: {
        type: Sequelize.TEXT,
        field: 'key',
        allowNull: false
      },
      name: {
        type: Sequelize.TEXT,
        field: 'name'
      },
      extension: {
        type: Sequelize.TEXT,
        field: 'extension'
      },
      size: {
        type: Sequelize.BIGINT,
        field: 'size'
      },
      isUploaded: {
        type: Sequelize.BOOLEAN,
        field: 'is_uploaded'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {

        }
      }
    });
    return Model;
  }
}

export = File;