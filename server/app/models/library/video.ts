/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import File = Models.File;
import Tickser = Models.Tickser;
import VideoJob = Models.VideoJob;
import VideoFile = Models.VideoFile;

module Video {

  export enum TranscodingStatus {
    START = 0,
    COMPLETE = 1,
    ERROR = 2
  }

  export interface Attributes {
    id?: number;
    tickserId?: number;
    name?: string;
    transcodingStatus?: TranscodingStatus;
    durationInMillis?: number;
    thumbnailImageId?: number;
    created_at?: Date;
    updated_at?: Date;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    thumbnailImage?: File.Instance;
    tickser?: Tickser.Instance;
    videoJob?: VideoJob.Instance;
    videoFiles: VideoFile.Instance[];
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export class Scopes {
    public static INCLUDE_VIDEO_FILES = 'INCLUDE_VIDEO_FILES';
    public static INCLUDE_THUMBNAIL_IMAGE = 'INCLUDE_THUMBNAIL_IMAGE';

    public static MAIN_ATTRIBUTES_AND_FILES_MAIN_ATTRIBUTES: string = 'MAIN_ATTRIBUTES_AND_FILES_MAIN_ATTRIBUTES';
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('Video', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
        allowNull: false
      },
      name: {
        type: Sequelize.STRING(255),
        field: 'name'
      },
      transcodingStatus: {
        type: Sequelize.INTEGER,
        field: 'transcoding_status'
      },
      durationInMillis: {
        type: Sequelize.BIGINT,
        field: 'duration_in_millis'
      },
      thumbnailImageId: {
        type: Sequelize.BIGINT,
        field: 'thumbnail_image_id'
      }

    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models: any) {
          Model.hasMany(models.VideoFile.Model, {as: 'videoFiles', foreignKey: {name: 'videoId', field: 'video_id'}});
          Model.belongsTo(models.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
          Model.hasOne(models.VideoJob.Model, {as: 'videoJob', foreignKey: {name: 'videoId', field: 'video_id'}});
          Model.belongsTo(models.File.Model, {
            as: 'thumbnailImage',
            foreignKey: {name: 'thumbnailImageId', field: 'thumbnail_image_id'}
          });
        },
        addScopes: function (models: any) {
          Model.addScope(Scopes.INCLUDE_VIDEO_FILES, {
            include: [
              {
                model: models.VideoFile.Model,
                as: 'videoFiles',
                include: [
                  {
                    model: models.File.Model,
                    as: 'file'
                  }
                ]
              }
            ]
          });
          Model.addScope(Scopes.INCLUDE_THUMBNAIL_IMAGE, {
            include: [
              {
                model: models.File.Model,
                as: 'thumbnailImage',
              }
            ]
          });
          Model.addScope(Scopes.MAIN_ATTRIBUTES_AND_FILES_MAIN_ATTRIBUTES, {
            attributes: ['id', 'name', 'created_at', 'updated_at', 'transcodingStatus', 'durationInMillis'],
            include: [
              {
                required: false,
                model: models.File.Model,
                as: 'thumbnailImage',
                attributes: ['key']
              },
              {
                required: false,
                model: models.VideoFile.Model,
                as: 'videoFiles',
                attributes: ['height', 'width', 'bitrate'],
                include: [
                  {
                    required: false,
                    model: models.File.Model,
                    as: 'file',
                    attributes: ['size', 'key']
                  }
                ]
              }
            ]
          });
        }
      }
    });
    return Model;
  }
}

export = Video;