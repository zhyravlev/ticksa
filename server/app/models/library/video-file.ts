/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import File = Models.File;
import Video = Models.Video;

module VideoFile {

  export enum Quality {
    Q360P = 0,
    Q720P = 1,
  }

  export interface Attributes {
    fileId: number;
    videoId: number;
    bitrate: number;
    width: number;
    height: number;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    file?: File.Instance;
    video?: Video.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('VideoFile', {
      height: {
        type: Sequelize.INTEGER,
        field: 'height'
      },
      width: {
        type: Sequelize.INTEGER,
        field: 'width'
      },
      bitrate: {
        type: Sequelize.INTEGER,
        field: 'bitrate'
      },
      videoId: {
        type: Sequelize.BIGINT,
        field: 'video_id',
        primaryKey: true
      },
      fileId: {
        type: Sequelize.BIGINT,
        field: 'file_id',
        primaryKey: true
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.File.Model, {as: 'file', foreignKey: {name: 'fileId', field: 'file_id'}});
          Model.belongsTo(models.Video.Model, {as: 'video', foreignKey: {name: 'videoId', field: 'video_id'}});
        }
      }
    });
    return Model;
  }
}

export = VideoFile;