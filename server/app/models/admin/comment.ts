/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Content = Models.Content;
import Tickser = Models.Tickser;
import Video = Models.Video;
import Image = Models.Image;

module Comment {
  export enum Status {
    CREATED = 0,
    NEEDS_VERIFICATION = 1,
    VERIFIED = 2,
    ARCHIVED = 3
  }
  export enum Type {
    GENERAL = 0,
    REQUEST = 1,
    COMPLAINT = 2,
    SUGGESTION = 3,
    ADMIN = 4
  }
  export enum ResultCode {
    NEW = 0,
    COMPLETED = 1,
    PENDING = 2,
    OTHER = 3
  }
  export enum Queue {
    ADMIN = 0,
    USER = 1,
    FINANCE = 2,
    TECHNICAL = 3,
    LEGAL = 4,
    SALES = 5,
    RESPONSE = 6
  }
  export interface Attributes {
    id?: number;
    entityId?: number;
    entityName?: string;
    queue?: Queue | number;
    resultCode?: ResultCode | number;
    text?: string;
    type?: Type | number;
    status?: Status| number;
    parentId?: number;
    createdBy?: number;
    updateBy?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('Comment', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      entityId: {
        type: Sequelize.BIGINT,
        field: 'entity_id',
        allowNull: false
      },
      entityName: {
        type: Sequelize.STRING(255),
        field: 'entity_name',
        allowNull: false
      },
      queue: {
        type: Sequelize.INTEGER,
        field: 'queue'
      },
      resultCode: {
        type: Sequelize.INTEGER,
        field: 'result_code'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      text: {
        type: Sequelize.STRING(255),
        field: 'text'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      parentId: {
        type: Sequelize.BIGINT,
        field: 'parent_id'
      },
      createdBy: {
        type: Sequelize.BIGINT,
        field: 'created_by',
        allowNull: false
      },
      updateBy: {
        type: Sequelize.BIGINT,
        field: 'update_by'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (index) {
        }
      }
    });
    return Model;
  }
}

export = Comment;