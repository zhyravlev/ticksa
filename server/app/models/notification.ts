/// <reference path="../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa')
import Promise = require('bluebird');

import connection = require('../connection');
import Models = require('./models');

module Notification {
  export enum Type {
    MESSAGE = 0,
    CHANNEL_EDIT_INVITE = 1,
    CHANNEL_SUBSCRIBE = 2,
    GRANT_FREE_TICKS = 3,
    GRANT_COMMISSION_TICKS = 4,
    CHANNEL_SHARED_REVENUE_CHANGED = 5,
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    countByTickserId(tickserId: number): Promise<number>;
    findAllByTickserId(tickserId: number): Promise<Instance[]>;
    findOneByTickserId(tickserId: number): Promise<Instance>
    findOneByIdAndTickser(notificationId: number, tickserId: number): Promise<Instance>;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }

  export interface Attributes {
    id?: number;
    title?: string;
    content?: string;
    type?: Type;
    metadata?: any;
    viewed?: boolean;
    tickserId?: number;
  }

  export var Model: Class;
  export function initialize(connection: Sequelize.Sequelize): Notification.Class {
    Model = <Notification.Class> connection.define<Notification.Instance, Notification.Attributes>('Notification', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      title: {
        type: Sequelize.TEXT,
        field: 'title'
      },
      content: {
        type: Sequelize.TEXT,
        field: 'content'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      metadata: {
        type: Sequelize.JSON,
        field: 'metadata'
      },
      viewed: {
        type: Sequelize.BOOLEAN,
        field: 'viewed',
        allowNull: false,
        defaultValue: false
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      scopes: {
        notViewed: {
          where: {
            viewed: false,
          }
        },
        tickser: {
          include: [
            {
              required: true,
              model: Models.Tickser.Model,
              as: 'tickser'
            }
          ]
        }
      },
      classMethods: {
        countByTickserId: function(tickserId: number) {
          return this.count({where: {
            tickserId: tickserId,
          }});
        },
        findAllByTickserId: function(tickserId: number) {
          return this.findAll({
            where: {
              tickserId: tickserId
            }
          });
        },
        findOneByTickserId: function(tickserId: number) {
          return this.findOne({
            where: {
              tickserId: tickserId
            }
          });
        },
        findOneByIdAndTickser: function(notificationId: number, tickserId: number) {
          return this.findOne({
            where: {
              id: notificationId,
              tickserId: tickserId
            }
          })
        },
        associate: function(models) {
          this.belongsTo(models.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
        }
      }
    });
    return Model;
  }
}
export = Notification;
