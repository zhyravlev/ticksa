/// <reference path="../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('./models');
import Content = Models.Content;
import Tickser = Models.Tickser;
import Video = Models.Video;
import Image = Models.Image;

module GlobalConfig {
  export enum Type {
    INTEGER = 0,
    DECIMAL = 1,
    STRING = 2
  }
  export interface Attributes {
    id?: number;
    field?: string;
    type?: Type;
    dataInteger?: number;
    dataDecimal?: number;
    dataString?: string;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('GlobalConfig', {

      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      field: {
        type: Sequelize.TEXT,
        field: 'field'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      dataInteger: {
        type: Sequelize.INTEGER,
        field: 'data_integer'
      },
      dataDecimal: {
        type: Sequelize.DECIMAL,
        field: 'data_decimal'
      },
      dataString: {
        type: Sequelize.TEXT,
        field: 'data_string'
      }
    }, {
      underscored: true,
      timestamps: false,
      freezeTableName: true,
      classMethods: {
        associate: function (index) {

        }
      }
    });
    return Model;
  }
}

export = GlobalConfig;