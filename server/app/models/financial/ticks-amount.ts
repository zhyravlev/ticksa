/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');

module TicksAmount {

  export enum Type {
    FREE = 0,
    PAID = 1,
  }

  export enum Reason {
    DEFAULT = 0,
    REVENUE_SHARE = 1,
    BEHALF_PAYING = 2,
    VAT = 3,
  }

  export interface Attributes {
    id?: number;
    ticksTransactionId?: number;
    accountId: number;
    amount: number;
    type: Type;
    reason: Reason;
    vatCountryId?: number,
    vat?: number,
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions): Class;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('TicksAmount', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      ticksTransactionId: {
        type: Sequelize.BIGINT,
        field: 'ticks_transaction_id'
      },
      accountId: {
        type: Sequelize.BIGINT,
        field: 'account_id'
      },
      amount: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'amount'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type',
        allowNull: false,
        defaultValue: Type.FREE
      },
      reason: {
        type: Sequelize.INTEGER,
        field: 'reason',
        allowNull: false,
        defaultValue: Reason.DEFAULT
      },
      vatCountryId: {
        type: Sequelize.BIGINT,
        field: 'vat_country_id'
      },
      vat: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'vat',
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.TicksTransaction.Model, {
            as: 'ticksTransaction',
            foreignKey: {name: 'ticksTransactionId', field: 'ticks_transaction_id'}
          });
          Model.belongsTo(models.Account.Model, {as: 'account', foreignKey: {name: 'accountId', field: 'account_id'}});
          Model.belongsTo(models.Country.Model, {as: 'vatCountry', foreignKey: {name: 'vatCountryId', field: 'vat_country_id'}});
        }
      }
    });
    return Model;
  }
}

export = TicksAmount;





