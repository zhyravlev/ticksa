import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../../models');
import TicksAmount = Models.TicksAmount;
import CashIO = Models.CashIO;

module PayPalReturn {
  export enum Type {
    COMPLETE = 0,
    CANCEL = 1
  }
  export interface Attributes {
    id?: number;
    type?: Type;
    params?: string,
    date?: Date,
    err?: string,
    cashInId?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('PayPalReturn', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      params: {
        type: Sequelize.STRING,
        field: 'params'
      },
      date: {
        type: Sequelize.DATE,
        field: 'date'
      },
      err: {
        type: Sequelize.TEXT,
        field: 'return_err'
      },
      cashInId: {
        type: Sequelize.BIGINT,
        field: 'cash_in_id'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.PayPalCashIn.Model, {
            as: 'cashIn',
            foreignKey: {name: 'cashInId', field: 'cash_in_id'}
          });
        }
      }
    });
    return Model;
  }
}
export = PayPalReturn;