import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../../models');
import TicksAmount = Models.TicksAmount;
import CashIO = Models.CashIO;

module PayPalCashIn {

  // this enum should have such unique long name because of strange TypeScript Compiler behavior
  export enum PayPalStatus {
    PENDING = 0,
    REGISTERED = 1,
    CANCELED = 2,
    APPROVED_BUT_NOT_COMPLETED = 3,
    COMPLETED = 4
  }

  export interface Attributes {
    id?: number;
    status?: PayPalStatus;
    payPalId?: string,
    url?: string,
    createdDate?: Date,
    initRequest?: string,
    initResponse?: string,
    initErr?: string,
    executionResponse?: string,
    executionDate?: Date,
    executionErr?: string
    cashIoId?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    cashIo: CashIO.Instance;
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('PayPalCashIn', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      payPalId: {
        type: Sequelize.TEXT,
        field: 'paypal_id'
      },
      url: {
        type: Sequelize.TEXT,
        field: 'url'
      },
      createdDate: {
        type: Sequelize.DATE,
        field: 'created_date'
      },
      initRequest: {
        type: Sequelize.TEXT,
        field: 'init_request'
      },
      initResponse: {
        type: Sequelize.TEXT,
        field: 'init_response'
      },
      initErr: {
        type: Sequelize.TEXT,
        field: 'init_err'
      },
      executionResponse: {
        type: Sequelize.TEXT,
        field: 'exec_response'
      },
      executionDate: {
        type: Sequelize.DATE,
        field: 'exec_date'
      },
      executionErr: {
        type: Sequelize.TEXT,
        field: 'exec_err'
      },
      cashIoId: {
        type: Sequelize.BIGINT,
        field: 'cash_io_id'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.CashIO.Model, {as: 'cashIo', foreignKey: {name: 'cashIoId', field: 'cash_io_id'}});
          Model.hasMany(models.PayPalReturn.Model, {as: 'returns', foreignKey: 'cashInId'});
        }
      }
    });
    return Model;
  }
}
export = PayPalCashIn;
