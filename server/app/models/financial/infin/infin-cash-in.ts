import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../../models');
import TicksAmount = Models.TicksAmount;
import CashIO = Models.CashIO;

module InfinCashIn {
  export enum Status {
    CREATED = 0,
    NOTIFIED = 1
  }
  export interface Attributes {
    id?: number;
    url?: string;
    status?: Status;
    countryCode?: string;
    amount?: number;
    cashIoId?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    cashIo: CashIO.Instance;
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('InfinCashIn', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      url: {
        type: Sequelize.TEXT,
        field: 'url'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      // stored - to be checked with same parameter of callback request
      countryCode: {
        type: Sequelize.STRING(2),
        field: 'country_code'
      },
      // stored - to be checked with same parameter of callback request
      amount: {
        type: Sequelize.DECIMAL,
        field: 'amount'
      },
      cashIoId: {
        type: Sequelize.BIGINT,
        field: 'cash_io_id'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.CashIO.Model, {as: 'cashIo', foreignKey: {name: 'cashIoId', field: 'cash_io_id'}});
          Model.hasMany(models.InfinCallback.Model, {as: 'callbacks', foreignKey: 'infinCashInId'});
        }
      }
    });
    return Model;
  }
}

export = InfinCashIn;