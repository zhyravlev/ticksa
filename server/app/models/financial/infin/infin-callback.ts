import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../../models');
import TicksAmount = Models.TicksAmount;
import CashIO = Models.CashIO;

module InfinCallback {
  export enum Type {
    VALID = 0,
    REPEATED = 1,
    INVALID_NO_P4 = 2,
    INVALID_P4 = 3,
    INVALID_NO_SESSION_ID = 4,
    INVALID_SESSION_ID = 5,
    INVALID_PARAMS = 6
  }
  export interface Attributes {
    id?: number;
    type?: Type;
    url?: string;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    cashIO: CashIO.Instance;
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('InfinCallback', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      url: {
        type: Sequelize.TEXT,
        field: 'url'
      },
      infinCashInId: {
        type: Sequelize.BIGINT,
        field: 'infin_cash_in_id'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.InfinCashIn.Model, {
            as: 'infinCashIn',
            foreignKey: {name: 'infinCashInId', field: 'infin_cash_in_id'}
          });
        }
      }
    });
    return Model;
  }
}
export = InfinCallback;