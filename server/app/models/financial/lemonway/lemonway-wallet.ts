/// <reference path="../../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Promise = require('bluebird');
import Ticksa = require('ticksa');
import Models = require('../../models');
import Account = Models.Account;

module LemonwayWallet {

  export enum Status {
    CREATED = 0,
    ERROR = 1,
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    findOneByAccountId(accountId: number): Promise<Instance>;
  }
  export interface Attributes {
    id?: number;
    email?: string;
    walletId: string;
    accountId: number;
    lastName: string;
    firstName: string;
    status?: Status;
    initRequest?: string;
    initResponse?: string;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    account?: Account.Instance;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Models.LemonwayWallet.Class {
    Model = <Models.LemonwayWallet.Class> connection.define<Models.LemonwayWallet.Instance, Models.LemonwayWallet.Attributes>("LemonwayWallet", {
      id: {
        type: Sequelize.BIGINT,
        field: 'id',
        primaryKey: true,
        autoIncrement: true
      },
      accountId: {
        type: Sequelize.BIGINT,
        field: 'account_id',
      },
      walletId: {
        type: Sequelize.STRING,
        field: 'wallet_id',
      },
      lastName: {
        type: Sequelize.STRING,
        field: 'last_name'
      },
      firstName: {
        type: Sequelize.STRING,
        field: 'first_name'
      },
      email: {
        type: Sequelize.STRING,
        field: 'email'
      },
      initRequest: {
        type: Sequelize.TEXT,
        field: 'init_request'
      },
      initResponse: {
        type: Sequelize.TEXT,
        field: 'init_response'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        findOneByAccountId: function (accountId: number) {
          return this.findOne({where: {accountId: accountId}});
        },
        associate: function(index) {
          Model.belongsTo(index.Account.Model, {as: 'account', foreignKey: {name: 'accountId', field: 'account_id'}});
        }
      }
    });
    return Model;
  }
}

export = LemonwayWallet;
