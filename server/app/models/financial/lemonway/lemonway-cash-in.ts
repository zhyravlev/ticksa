/// <reference path="../../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Promise = require('bluebird');
import Ticksa = require('ticksa');
import Models = require('../../models')

import LemonwayCashCallback = Models.LemonwayCashCallback;
import CashIO = Models.CashIO;
import Currency = Models.Currency;

module LemonwayCashIn {

  export enum Status {
    CREATED = 0,
    COMPLETED = 1,
    FAILED = 2,

    // according to APP-680 it was used, but according to APP-675 it became unused.
    // left because historically we have such data
    COMPLETED_BUT_NOT_SENT = 3
  }

  export enum Type {
    WITH_CARD = 0,
    WITHOUT_CARD = 1,
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    findOneByWkToken(wkToken: string): Promise<Instance>;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    getCallbacks: Sequelize.HasManyGetAssociationsMixin<LemonwayCashCallback.Instance>;
    setCallbacks: Sequelize.HasManySetAssociationsMixin<LemonwayCashCallback.Instance, number>;
    addCallbacks: Sequelize.HasManyAddAssociationsMixin<LemonwayCashCallback.Instance, number>;
    addCallback: Sequelize.HasManyAddAssociationMixin<LemonwayCashCallback.Instance, number>;
    createCallback: Sequelize.HasManyCreateAssociationMixin<LemonwayCashCallback.Attributes>;
    removeCallback: Sequelize.HasManyRemoveAssociationMixin<LemonwayCashCallback.Instance, number>;
    removeCallbacks: Sequelize.HasManyRemoveAssociationsMixin<LemonwayCashCallback.Instance, number>;
    hasCallback: Sequelize.HasManyHasAssociationMixin<LemonwayCashCallback.Instance, number>;
    hasCallbacks: Sequelize.HasManyHasAssociationsMixin<LemonwayCashCallback.Instance, number>;
    countCallbacks: Sequelize.HasManyCountAssociationsMixin;
    getCashIo: Sequelize.BelongsToGetAssociationMixin<CashIO.Instance>;
  }

  export interface Attributes {
    id?: number;
    initRequest: string;
    initResponse: string;
    status: Status;
    type: Type;
    cashIoId: number;
    wkToken: string;

    cashIo?: CashIO.Instance;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>("LemonwayCashIn", {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      wkToken: {
        type: Sequelize.STRING,
        field: 'wk_token',
      },
      initRequest: {
        type: Sequelize.TEXT,
        field: 'init_request',
      },
      initResponse: {
        type: Sequelize.TEXT,
        field: 'init_response',
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      cashIoId: {
        type: Sequelize.BIGINT,
        field: 'cash_io_id',
        allowNull: false
      },
      // according to APP-680 it was used, but according to APP-675 it became unused.
      // left because historically we have such data
      sendPaymentRequest: {
        type: Sequelize.TEXT,
        field: 'send_payment_request',
      },
      // according to APP-680 it was used, but according to APP-675 it became unused.
      // left because historically we have such data
      sendPaymentResponse: {
        type: Sequelize.TEXT,
        field: 'send_payment_response',
      }
    }, {
      underscored: true,
      freezeTableName: true,
      scopes: {
        cashIo: function() {
          return {
            include: [
              {
                required: true,
                model: Models.CashIO.Model,
                as: 'cashIo'
              }
            ]
          }
        },
        cashIoWithCurrencies: function() {
          return {
            include: [
              {
                required: true,
                model: Models.CashIO.Model,
                as: 'cashIo',
                include: [
                  {
                    required: true,
                    model: Models.Currency.Model,
                    as: 'feesCurrency'
                  },
                  {
                    required: true,
                    model: Models.Currency.Model,
                    as: 'userCurrency'
                  }
                ]
              }
            ]
          }
        }
      },
      classMethods: {
        findOneByWkToken: function (wkToken: string) {
          return this.findOne({
            where: {
              wkToken: wkToken,
            }
          })
        },
        associate: function (index): void {
          Model.belongsTo(index.CashIO.Model, {as: 'cashIo', foreignKey: {name: 'cashIoId', field: 'cash_io_id'}});
          Model.hasMany(index.LemonwayCashCallback.Model, {as: 'callbacks', foreignKey: 'cashInId'});
        }
      }
    });
    return Model;
  }
}

export = LemonwayCashIn;
