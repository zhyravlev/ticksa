/// <reference path="../../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Promise = require('bluebird');
import Ticksa = require('ticksa');
import Models = require('../../models')
import LemonwayCashIn = Models.LemonwayCashIn;

import connection = require('../../../connection')

module LemonwayCashCallback {
  export enum Type {
    SUCCESS = 0,
    ERROR = 1,
    CANCEL = 2,
    REPEATED = 3,
  }
  export enum Method {
    GET = 0,
    POST = 1,
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    getCashIn: Sequelize.BelongsToGetAssociationMixin<LemonwayCashIn.Instance>;
  }
  export interface Attributes {
    id?: number;
    params: string;
    type?: Type;
    method?: Method;
    cashInId: number;
    transDetailRequest?: string;
    transDetailResponse?: string;
    message?: string;
  }

  export var Model: Class;


  export function initialize(connection: Sequelize.Sequelize): Models.LemonwayCashCallback.Class {
    Model = <Models.LemonwayCashCallback.Class> connection.define<Models.LemonwayCashCallback.Instance, Models.LemonwayCashCallback.Attributes>("LemonwayCashCallback", {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      params: {
        type: Sequelize.TEXT,
        field: 'params',
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      method: {
        type: Sequelize.INTEGER,
        field: 'method'
      },
      cashInId: {
        type: Sequelize.BIGINT,
        field: 'cash_in_id',
        allowNull: false
      },
      transDetailRequest: {
        type: Sequelize.TEXT,
        field: 'trans_detail_request',
      },
      transDetailResponse: {
        type: Sequelize.TEXT,
        field: 'trans_detail_response',
      },
      message: {
        type: Sequelize.TEXT,
        field: 'message',
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function(index) {
          Model.belongsTo(index.LemonwayCashIn.Model, {as: 'cashIn', foreignKey: {name: 'cashInId', field: 'cash_in_id'}});
        }
      }
    });
    return Model;
  }
}

export = LemonwayCashCallback;

