import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import TicksAmount = Models.TicksAmount;

module TicksTransaction {
  export enum Type {
    ACCESS = 0,
    CREATE = 1,
    EXTEND_CONTENT = 2,
    LIKE = 3,
    DISLIKE = 4,
    GRANT_FREE_TICKS = 5,
    UPLOAD_VIDEO = 6,
    UPLOAD_IMAGE = 7,
    REFUND = 8,
    CHANNEL_DONATE = 9,
  }

  export enum Status {
    NEW = 0,
    DONE = 1
  }

  export interface Attributes {
    id?: number;
    feedId?: number;
    contentId?: number;
    channelId?: number;
    type?: Type;
    status?: Status;
    amount?: number;
    createdAt?: Date;
    updatedAt?: Date;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    createTicksAmount: Sequelize.HasManyCreateAssociationMixin<TicksAmount.Attributes>;
    ticksAmounts: TicksAmount.Instance[]
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    findTheLatestByContentIdAccountIdTypeAndTime(contentId: number, accountId: number, type: Type, time: Date): Promise<Instance[]>;
    findAllByContentId(contentId: number): Promise<Instance[]>;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('TicksTransaction', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      feedId: {
        type: Sequelize.BIGINT,
        field: 'feed_id'
      },
      contentId: {
        type: Sequelize.BIGINT,
        field: 'content_id'
      },
      channelId: {
        type: Sequelize.BIGINT,
        field: 'channel_id'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      processedDate: {
        type: Sequelize.DATE,
        field: 'processed_date'
      },
      amount: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'amount',
        allowNull: false
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Content.Model, {
            as: 'content',
            foreignKey: {name: 'contentId', field: 'content_id'}
          });
          Model.belongsTo(models.Channel.Model, {
            as: 'channel',
            foreignKey: {name: 'channelId', field: 'channel_id'}
          });
          Model.hasMany(models.TicksAmount.Model, {
            as: 'ticksAmounts',
            foreignKey: {name: 'ticksTransactionId', field: 'ticks_transaction_id'}
          });
        },
        findAllByContentId: function (contentId) {
          return this.findAll({
            where: {
              contentId: contentId
            }
          });
        },
        findTheLatestByContentIdAccountIdTypeAndTime: function (contentId, accountId, type, time) {
          return this.scope('ticksAmounts').findAll({
            where: {
              contentId: contentId,
              type: type,
              created_at: {$gt: time}
            },
            order: [
              ['created_at', 'DESC'],
              ['id', 'DESC']
            ],
            limit: 1
          });
        }
      },
      scopes: {
        ticksAmounts: function () {
          return {
            include: [
              {
                model: Models.TicksAmount.Model,
                as: 'ticksAmounts'
              }
            ]
          }
        }
      }
    });
    return Model;
  }
}

export = TicksTransaction;