/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');

module Currency {
  export const IsoCode = {
    EURO: 'EUR',
    US_DOLLAR: 'USD',
    BRITISH_POUND: 'GBP'
  };
  export interface Attributes {
    id?: number;
    name?: string;
    iso4217Code?: string;
    symbol?: string;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {

  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Models.Currency.Class> connection.define<Models.Currency.Instance, Models.Currency.Attributes>('Currency', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.TEXT,
        field: 'name'
      },
      iso4217Code: {
        type: Sequelize.STRING(3),
        field: 'iso_4217_code'
      },
      symbol: {
        type: Sequelize.TEXT,
        field: 'symbol'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function () {

        }
      },
      scopes: {
        EUR: {
          where: {
            iso4217Code: Models.Currency.IsoCode.EURO
          }
        }
      }
    });
    return Model;
  }
}

export = Currency;

