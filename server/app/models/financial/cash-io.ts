/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Promise = require('bluebird');
import Ticksa = require('ticksa');
import Models = require('../models');
import Account = Models.Account;
import Country = Models.Country;
import Tickser = Models.Tickser;
import Currency = Models.Currency;
import InfinCashIn = Models.InfinCashIn;
import PayPalCashIn = Models.PayPalCashIn;
import LemonwayCashIn = Models.LemonwayCashIn;

module CashIO {

  export enum Status {
    PENDING = 0,
    STARTED = 1,
    FAILED = 2,
    COMPLETED = 3,
    ON_HOLD = 4,
    FINAL = 5,
    TRANSFERRED = 6
  }
  export enum Type {
    CASH_IN = 0,
    CASH_OUT = 1,
  }

  export enum PaymentType {
    INFIN = 0,
    PAYPAL = 1,
    LEMONWAY = 2,
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    country: Country.Instance;
    account: Account.Instance;
    tickser: Tickser.Instance;
    userCurrency: Currency.Instance;
    feesCurrency: Currency.Instance;
    infinCashIn: InfinCashIn.Instance;
    payPalCashIn: PayPalCashIn.Instance;
    lemonwayCashIn: LemonwayCashIn.Instance;

    getLemonwayCashIn: Sequelize.HasOneGetAssociationMixin<LemonwayCashIn.Instance>;
    setLemonwayCashIn: Sequelize.HasOneSetAssociationMixin<LemonwayCashIn.Instance, number>;
    createLemonwayCashIn: Sequelize.HasOneCreateAssociationMixin<LemonwayCashIn.Attributes>;
  }

  export interface Attributes {
    id?: number;
    accountId?: number;
    tickserId?: number;
    userCurrencyId?: number;
    userCurrencyAmount?: number;
    tickserPhoneNumber?: string;
    phoneCountryCode?: string;
    tickserEmail?: string;
    countryId?: number;
    status?: Status;
    type?: Type;
    paymentType?: PaymentType;
    receivedAmount?: number;
    receivedTicks?: number;
    feesCurrencyId?: number;
    feesAmount?: number;
    VATAmount?: number;
    NETAmount?: number;
    error?: string;
    exchangeRateToEuro?: number;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Models.CashIO.Class {
    Model = <Models.CashIO.Class> connection.define<Models.CashIO.Instance, Models.CashIO.Attributes>('CashIO', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      accountId: {
        type: Sequelize.BIGINT,
        field: 'account_id'
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id'
      },
      userCurrencyId: {
        type: Sequelize.BIGINT,
        field: 'user_currency_id'
      },
      userCurrencyAmount: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'user_currency_amount'
      },
      tickserPhoneNumber: {
        type: Sequelize.STRING,
        field: 'tickser_phone_number'
      },
      phoneCountryCode: {
        type: Sequelize.STRING,
        field: 'phone_country_code'
      },
      tickserEmail: {
        type: Sequelize.STRING,
        field: 'tickser_email'
      },
      countryId: {
        type: Sequelize.BIGINT,
        field: 'country_id'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      paymentType: {
        type: Sequelize.INTEGER,
        field: 'payment_type'
      },
      receivedAmount: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'received_amount'
      },
      receivedTicks: {
        type: Sequelize.INTEGER,
        field: 'received_ticks'
      },
      feesCurrencyId: {
        type: Sequelize.BIGINT,
        field: 'fees_currency_id'
      },
      feesAmount: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'fees_amount'
      },
      VATAmount: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'vat_amount'
      },
      NETAmount: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'net_amount'
      },
      error: {
        type: Sequelize.TEXT,
        field: 'error'
      },
      exchangeRateToEuro: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'exchange_rate_to_euro'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      scopes: {
        isCashIn: function () {
          return {
            where: {
              type: Models.CashIO.Type.CASH_IN,
            }
          }
        },
        feesCurrency: function () {
          return {
            include: [{
              model: Models.Currency.Model,
              as: 'feesCurrency'
            }]
          }
        },
        userCurrency: function () {
          return {
            include: [{
              required: true,
              model: Models.Currency.Model,
              as: 'userCurrency'
            }]
          }
        },
        country: function () {
          return {
            include: [{
              required: true,
              model: Models.Country.Model,
              as: 'country'
            }]
          }
        },
        tickser: function () {
          return {
            include: [{
              required: true,
              model: Models.Tickser.Model,
              as: 'tickser'
            }]
          }
        },
        account: function () {
          return {
            include: [{
              required: true,
              model: Models.Account.Model,
              as: 'account'
            }]
          }
        },
      },
      classMethods: {
        associate: function (models): void {
          Model.belongsTo(models.Account.Model, {as: 'account', foreignKey: {name: 'accountId', field: 'account_id'}});
          Model.belongsTo(models.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
          Model.belongsTo(models.Currency.Model, {
            as: 'userCurrency',
            foreignKey: {name: 'userCurrencyId', field: 'user_currency_id'}
          });
          Model.belongsTo(models.Currency.Model, {
            as: 'feesCurrency',
            foreignKey: {name: 'feesCurrencyId', field: 'fees_currency_id'}
          });
          Model.belongsTo(models.Country.Model, {as: 'country', foreignKey: {name: 'countryId', field: 'country_id'}});
          Model.hasOne(models.InfinCashIn.Model, {as: 'infinCashIn', foreignKey: 'cashIoId'});
          Model.hasOne(models.PayPalCashIn.Model, {as: 'payPalCashIn', foreignKey: 'cashIoId'});
          Model.hasOne(models.LemonwayCashIn.Model, {as: 'lemonwayCashIn', foreignKey: 'cashIoId'});
        }
      }
    });

    return Model;
  }
}
export = CashIO;