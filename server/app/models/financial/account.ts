/// <reference path="../../typings/tsd.d.ts"/>
import _ = require('lodash');
import Sequelize = require('sequelize');
import Promise = require('bluebird');
import Ticksa = require('ticksa');
import Models = require('../models');
import Tickser = Models.Tickser;
import Currency = Models.Currency;
import TicksAmount = Models.TicksAmount;

module Account {
  export class Scopes {
    public static INCLUDE_TICKSER: string = 'includeTickser';
  }

  export enum AutoTopUpMethod {
    INFIN = 0,
    PAYPAL = 1,
  }

  export enum Status {
    NORMAL = 0,
    SUSPENDED = 1,
    PENDING_PROBLEM = 2,
  }

  // this enum should have such unique long name because of strange TypeScript Compiler behavior
  export enum AccountType {
    CORPORATE = 0,
    PRIVATE = 1,
    SYSTEM = 2,
  }

  export enum SystemAccount {
    GENERAL_ACCOUNT = -1,
    FREE_TICKS_ACCOUNT = -2,
    COMMISSION_TICKS_ACCOUNT = -3,
    VOUCHER_ACCOUNT = -4,
    VAT_GLOBAL = -5,
  }

  export enum PreferredPaymentMethod {
    INFIN = 0,
    PAYPAL = 1,
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    findOneByTickserId(tickserId: number, options?: Sequelize.FindOptions): Promise<Instance>;
    findAllByTickserIds(tickserId: number[]): Promise<Instance[]>;
    findAllByIds(accountIds: number[], options?: Sequelize.FindOptions): Promise<Instance[]>;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    tickser?: Tickser.Instance;
    currency?: Currency.Instance;
    ticksAmounts?: TicksAmount.Instance[];
  }

  export interface Attributes {
    id?: number,
    tickserId?: number,
    totalTicksDebited?: number,
    totalTicksCredited?: number,
    totalFreeTicks?: number,
    currentBalance?: number,
    currentBalanceFreeTicks?: number,
    currentBalanceEarnedTicks?: number,
    balanceUpdated?: Date,
    preferredPaymentMethod?: number,
    autoAuthorize?: boolean,
    autoAuthorizeMaxTicks?: number,
    currencyId?: number,
    autoTopupAmount?: number,
    autoTopupMethod?: AutoTopUpMethod,
    status?: Status,
    type?: AccountType,
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('Account', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id'
      },

      totalTicksDebited: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'total_ticks_debited'
      },
      totalTicksCredited: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'total_ticks_credited'
      },
      totalFreeTicks: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'total_free_ticks'
      },
      currentBalance: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'current_balance'
      },
      currentBalanceFreeTicks: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'current_balance_free_ticks'
      },
      currentBalanceEarnedTicks: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'current_balance_earned_ticks',
        defaultValue: 0
      },
      balanceUpdated: {
        type: Sequelize.DATE,
        field: 'balance_updated'
      },
      preferredPaymentMethod: {
        type: Sequelize.INTEGER,
        field: 'preferred_payment_method'
      },
      autoAuthorize: {
        type: Sequelize.BOOLEAN,
        field: 'auto_authorize'
      },
      autoAuthorizeMaxTicks: {
        type: Sequelize.INTEGER,
        field: 'auto_authorize_max_ticks'
      },
      currencyId: {
        type: Sequelize.BIGINT,
        field: 'currency_id'
      },
      autoTopupAmount: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'auto_topup_amount'
      },
      autoTopupMethod: {
        type: Sequelize.INTEGER,
        field: 'auto_topup_method'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      }

    }, {
      underscored: true,
      freezeTableName: true,
      scopes: {
        isNormal: function(){
          return {
            where: {
              status: Account.Status.NORMAL
            }
          }
        },
        generalSystemAccount: function() {
          return {
            where: {
              id: Account.SystemAccount.GENERAL_ACCOUNT
            }
          }
        },
        freeTicksSystemAccount: function() {
          return {
            where: {
              id: Account.SystemAccount.FREE_TICKS_ACCOUNT
            }
          }
        }
      },
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
          Model.belongsTo(models.Currency.Model, {as: 'currency', foreignKey: {name: 'currencyId', field: 'currency_id'}});
          Model.hasMany(models.TicksAmount.Model, {as: 'ticksAmounts', foreignKey: {name: 'accountId', field: 'account_id'}});
        },
        findOneByTickserId: function (tickserId: number, options?: Sequelize.FindOptions): Promise<Instance> {
          let query = _.merge({
            where: {
              tickserId: tickserId
            }
          }, options);

          return this.findOne(query);
        },
        findAllByTickserIds: function (tickserIds: number[]): Promise<Instance[]> {
          return this.findAll({
            where: {
              tickserId: {
                $in: tickserIds
              }
            }
          });
        },
        findAllByIds: function (accountIds: number[], options?: Sequelize.FindOptions): Promise<Instance[]> {
          const query = _.merge({
            where: {
              id: {
                $in: accountIds
              }
            }
          }, options);

          return this.findAll(query);
        },
        addScopes: function (models) {
          Model.addScope(Scopes.INCLUDE_TICKSER, function () {
            return {
              include: [
                {
                  model: models.Tickser.Model,
                  as: 'tickser',
                }
              ]
            }
          });
        }
      }
    });
    return Model;
  }
}

export = Account;


