/// <reference path="../../typings/tsd.d.ts"/>

import _ = require('lodash');
import Sequelize = require('sequelize');
import Ticksa = require('ticksa')
import Models = require('../models')

module Country {

  export interface Attributes {
    id?: number,
    name?: string,
    a2?: string,
    a3?: string,
    number?: number,
    dialingCode?: string,
    currencyId?: number,
    VAT?: number,
    bookingCode?: string,
    continent?: string
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    findByA2Code(a2: string, options?: Sequelize.FindOptions): Promise<Instance>;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Models.Country.Class {
    Model = <Models.Country.Class> connection.define<Models.Country.Instance, Models.Country.Attributes>('Country', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.TEXT,
        field: 'name'
      },
      a2: {
        type: Sequelize.STRING(2),
        field: 'a_2'
      },
      a3: {
        type: Sequelize.STRING(3),
        field: 'a_3'
      },
      number: {
        type: Sequelize.STRING,
        field: 'number'
      },
      dialingCode: {
        type: Sequelize.STRING,
        field: 'dialing_code'
      },
      currencyId: {
        type: Sequelize.BIGINT,
        field: 'currency_id'
      },
      VAT: {
        type: Sequelize.NUMERIC(3,2),
        field: 'vat'
      },
      bookingCode: {
        type: Sequelize.TEXT,
        field: 'booking_code'
      },
      continent: {
        type: Sequelize.TEXT,
        field: 'continent'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (index) {
          Model.belongsTo(index.Currency.Model, {as: 'currency', foreignKey: {name: 'currencyId', field: 'currency_id'}});
        },
        findByA2Code: function(a2: string, options?: Sequelize.FindOptions): Promise<Instance> {
          const query = _.merge({
            where: {
              a2: countryCodeToUpperCase(a2)
            }
          }, options);

          return this.findOne(query);
        }
      }
    });
    return Model;
  }

  export function countryCodeToUpperCase(code: string): string {
    return code == null || !code.toUpperCase ? null : code.toUpperCase();
  }

}
export = Country;

