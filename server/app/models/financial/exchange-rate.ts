import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import TicksAmount = Models.TicksAmount;

module ExchangeRate {
  export interface Attributes {
    id?: number;
    currencyOneId?: number;
    currencyTwoId?: number;
    exchangeRate?: number;
    validFrom?: Date;
    validTo?: Date;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('ExchangeRate', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      currencyOneId: {
        type: Sequelize.BIGINT,
        field: 'currency_one_id'
      },
      currencyTwoId: {
        type: Sequelize.BIGINT,
        field: 'currency_two_id'
      },
      exchangeRate: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'exchange_rate'
      },
      validFrom: {
        type: Sequelize.DATE,
        field: 'valid_from'
      },
      validTo: {
        type: Sequelize.DATE,
        field: 'valid_to'
      }

    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Currency.Model, {as: 'currencyOne', foreignKey: {name: 'currencyOneId', field: 'currency_one_id'}});
          Model.belongsTo(models.Currency.Model, {as: 'currencyTwo', foreignKey: {name: 'currencyTwoId', field: 'currency_two_id'}});
        }
      },
      scopes: {
        valid: function () {
          return {
            where: {
              validTo: null
            }
          }
        }
      }
    });
    return Model;
  }
}

export = ExchangeRate;