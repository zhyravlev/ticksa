/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Content = Models.Content;
import Account = Models.Account;

module ContentAccess {

  export class Scopes {
    public static INCLUDE_ACCOUNT: string = 'includeAccount';
  }

  export interface Attributes {
    id?: number;
    hasAccess?: boolean;
    contentId?: number;
    accountId?: number;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    content?: Content.Instance;
    account?: Account.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    countByAccountIdAndContentId(accountId, contentId): Promise<number>;
    findOneByAccountIdAndContentId(accountId, contentId): Promise<Instance>;
    findAllByAccountIdAndContentIds(accountId, contentId): Promise<Instance[]>;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('ContentAccess', {
      accountId: {
        type: Sequelize.BIGINT,
        field: 'account_id',
        primaryKey: true
      },
      contentId: {
        type: Sequelize.BIGINT,
        field: 'content_id',
        primaryKey: true
      },
      hasAccess: {
        type: Sequelize.BOOLEAN,
        field: 'has_access'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      indexes: [{
        name: 'account_id_content_id_idx',
        unique: true,
        fields: ['account_id', 'content_id']
      }, {
        name: 'ContentAccess_content_id_idx',
        fields: ['content_id']
      }],
      classMethods: {
        countByAccountIdAndContentId: function (accountId, contentId) {
          return this.count({
            where: {
              accountId: accountId,
              contentId: contentId
            },
          });
        },
        findOneByAccountIdAndContentId: function (accountId, contentId) {
          return this.findOne({
            where: {
              accountId: accountId,
              contentId: contentId
            },
          });
        },
        findAllByAccountIdAndContentIds: function (accountId, contentIds) {
          return this.findAll({
            where: {
              accountId: accountId,
              contentId: {$in: contentIds}
            },
          });
        },
        associate: function (models) {
          Model.belongsTo(models.Content.Model, {as: 'content', foreignKey: {name: 'contentId', field: 'content_id'}});
          Model.belongsTo(models.Account.Model, {as: 'account', foreignKey: {name: 'accountId', field: 'account_id'}});
        },
        addScopes: function (models) {
          Model.addScope(Scopes.INCLUDE_ACCOUNT, function () {
            return {
              include: [
                {
                  model: models.Account.Model,
                  as: 'account',
                }
              ]
            }
          });
        }
      }
    });
    return Model;
  }
}

export = ContentAccess;
