/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Content = Models.Content;
import Video = Models.Video;

module ContentVideo {

  export class Scopes {
    public static INCLUDE_VIDEO: string = 'includeVideo';
  }

  export interface Attributes {
    id?: number;
    url?: string;
    contentId?: number;
    videoId?: number;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    content?: Content.Instance;
    video?: Video.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    countByVideoId(videoId: number): Promise<number>;

    findAllByVideoIds(videoIds: number[]): Promise<Instance[]>;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {

    var classMethods: ClassMethods = {
      countByVideoId: function (videoId: number): Promise<number> {
        return Model.count({
          where: {
            videoId: videoId
          }
        });
      },
      findAllByVideoIds: function (videoIds: number[]) {
        return Model.findAll({
          where: {
            videoId: {
              $in: videoIds
            }
          },
          attributes: ['videoId']
        });
      },
      addScopes: function (models) {
        Model.addScope(Scopes.INCLUDE_VIDEO, function () {
          return {
            include: [
              {
                model: models.Video.Model.scope([models.Video.Scopes.INCLUDE_VIDEO_FILES, models.Video.Scopes.INCLUDE_THUMBNAIL_IMAGE]),
                as: 'video',
              }
            ]
          }
        });
      },
      associate: function (models) {
        Model.belongsTo(models.Content.Model, {as: 'content', foreignKey: {name: 'contentId', field: 'content_id'}});
        Model.belongsTo(models.Video.Model, {as: 'video', foreignKey: {name: 'videoId', field: 'video_id'}});
      }
    };
    Model = <Class> connection.define('ContentVideo', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      videoId: {
        type: Sequelize.BIGINT,
        field: 'video_id'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: classMethods,
    });
    return Model;
  }
}

export = ContentVideo;