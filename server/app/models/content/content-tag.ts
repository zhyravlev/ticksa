/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Content = Models.Content;
import Tag = Models.Tag;

module ContentTag {
  export interface Attributes {
    tagId?: number;
    contentId?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    content?: Content.Instance;
    tag?: Tag.Instance;
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('ContentTag', {
      tag_id: {
        type: Sequelize.BIGINT,
        field: 'tag_id',
        primaryKey: true
      },
      content_id: {
        type: Sequelize.BIGINT,
        field: 'content_id',
        primaryKey: true
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Tag.Model, {as: 'tag', foreignKey: {name: 'tagId', field: 'tag_id'}});
          Model.belongsTo(models.Content.Model, {as: 'content', foreignKey: {name: 'contentId', field: 'content_id'}});
        }
      }
    });
    return Model;
  }
}

export = ContentTag;