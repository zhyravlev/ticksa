/// <reference path="../../typings/tsd.d.ts"/>
import _ = require('lodash');
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import {Content} from "../models";

module Condition {

  export class Scopes {
    public static INCLUDE_CONTENTS: string = 'INCLUDE_CONTENTS';
  }

  export enum Type {
    PURCHASE = 0,
  }

  export interface ValueAttribute {
    type: Type;
    contentId: number;
  }

  export interface Attributes {
    id?: number;
    value?: ValueAttribute;
    createdAt?: Date;
    updatedAt?: Date;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    contents?: Content.Instance[];
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    findAllByContentAndType(contentId: number, type: Condition.Type, options?: Sequelize.FindOptions): Promise<Instance[]>
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {

    var methods: ClassMethods = {
      findAllByContentAndType: function (contentId: number, type: Condition.Type, options?: Sequelize.FindOptions): Promise<Instance[]> {
        const query = _.merge({
          where: {
            value: {
              $contains: {
                type: type,
                contentId: contentId,
              }
            }
          }
        }, options);

        return this.findAll(query);
      },
      addScopes: function (models) {
        Model.addScope(Scopes.INCLUDE_CONTENTS, function () {
          return {
            include: [
              {
                model: models.Content.Model,
                as: 'contents',
              }
            ]
          }
        });
      },
      associate: function (models) {
        Model.belongsToMany(models.Content.Model, {
          as: 'contents',
          through: models.ContentCondition.Model,
          foreignKey: {name: 'conditionId', field: 'condition_id'}
        })
      }
    };

    Model = <Class> connection.define<Instance, Attributes>('Condition', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      value: {
        type: Sequelize.JSONB,
        field: 'value',
        allowNull: false,
        unique: true
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: methods,
      indexes: [{
        name: 'Condition_value_idx',
        // unique: true,  // GIN doesnt support unique indexes
        fields: ['value'],
        method: 'GIN'
      }]
    });
    return Model;
  }

}

export = Condition;