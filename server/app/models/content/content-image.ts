/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Content = Models.Content;

module ContentImage {

  export class Scopes {
    public static INCLUDE_IMAGE: string = 'INCLUDE_IMAGE';
  }

  export interface Attributes {
    id?: number;
    imageId?: number;
    position?: number;
    contentId?: number;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    content?: Content.Instance;
    image?: any;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    countByImageId(imageId: number): Promise<number>;
    findAllByImageIds(imageIds: number[]): Promise<Instance[]>;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    var methods: ClassMethods = {
      countByImageId(imageId: number): Promise<number> {
        return Model.count({
          where: {
            imageId: imageId
          }
        });
      },
      findAllByImageIds(imageIds: number[]): Promise<ContentImage.Instance[]> {
        return Model.findAll({
          where: {
            imageId: {
              $in: imageIds
            }
          },
          attributes: ['imageId']
        });
      },
      addScopes: function (models) {
        Model.addScope(Scopes.INCLUDE_IMAGE, function () {
          return {
            include: [
              {
                model: models.Image.Model.scope([models.Image.Scopes.INCLUDE_FILE]),
                as: 'image',
              }
            ]
          }
        });
      },
      associate(models): void {
        Model.belongsTo(models.Content.Model, {as: 'content', foreignKey: {name: 'contentId', field: 'content_id'}});
        Model.belongsTo(models.Image.Model, {as: 'image', foreignKey: {name: 'imageId', field: 'image_id'}});
      }
    };

    Model = <Class> connection.define('ContentImage', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      imageId: {
        type: Sequelize.BIGINT,
        field: 'image_id'
      },
      position: {
        type: Sequelize.INTEGER,
        field: 'position',
        defaultValue: 0
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: methods
    });
    return Model;
  }
}

export = ContentImage;