/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Content = Models.Content;

module ContentText {
  export interface Attributes {
    id?: number;
    text?: string;
    contentId?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    content?: Content.Instance;
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('ContentText', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      text: {
        type: Sequelize.TEXT,
        field: 'text'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Content.Model, {as: 'content', foreignKey: {name: 'contentId', field: 'content_id'}});
        }
      }
    });
    return Model;
  }
}

export = ContentText;