/// <reference path="../../typings/tsd.d.ts"/>
import _ = require('lodash');
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Condition = Models.Condition;
import Content = Models.Content;
import TickserCondition = Models.TickserCondition;

module ContentCondition {

  export class Scopes {
    public static INCLUDE_CONDITION: string = 'includeCondition';
  }

  export interface Attributes {
    contentId?: number;
    conditionId?: number;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    content?: Content.Instance;
    condition?: Condition.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    findUnmetForTickser(tickserId: number): Promise<Instance[]>;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {

    var methods: ClassMethods = {
      findUnmetForTickser(tickserId: number): Promise<Instance[]> {
        let options: Sequelize.FindOptions = {
          where: {
            conditionId: {
              $notIn: Sequelize.literal('( SELECT tc.condition_id ' +
                'FROM "TickserCondition" as tc WHERE tc.tickser_id = ' + tickserId + ')')
            }
          }
        };
        return this.scope(Scopes.INCLUDE_CONDITION).findAll(options);
      },
      addScopes: function (models) {
        Model.addScope(Scopes.INCLUDE_CONDITION, function () {
          return {
            include: [
              {
                model: models.Condition.Model,
                as: 'condition',
              }
            ]
          }
        });
      },
      associate: function (models) {
        Model.belongsTo(models.Condition.Model, {as: 'condition', foreignKey: {name: 'conditionId', field: 'condition_id'}});
        Model.belongsTo(models.Content.Model, {as: 'content', foreignKey: {name: 'contentId', field: 'content_id'}});
      }
    };

    Model = <Class> connection.define<Instance, Attributes>('ContentCondition', {
      contentId: {
        type: Sequelize.BIGINT,
        field: 'content_id',
        primaryKey: true
      },
      conditionId: {
        type: Sequelize.BIGINT,
        field: 'condition_id',
        primaryKey: true
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: methods,
      indexes: [{
        name: 'ContentCondition_condition_id_idx',
        fields: ['condition_id']
      }]
    });
    return Model;
  }

}

export = ContentCondition;