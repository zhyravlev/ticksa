/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Content = Models.Content;

module History {

  export interface Attributes {
    id?: number;
    tickserId?: number;
    contentId?: number;
    createdAt?: Date;
    updatedAt?: Date;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    content?: Content.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('History', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
        allowNull: false
      },
      contentId: {
        type: Sequelize.BIGINT,
        field: 'content_id',
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Content.Model, {as: 'content', foreignKey: {name: 'contentId', field: 'content_id'}});
        }
      }
    });
    return Model;
  }
}

export = History;