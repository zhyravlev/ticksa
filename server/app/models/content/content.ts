/// <reference path="../../typings/tsd.d.ts"/>
import _ = require('lodash');
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import ContentText = Models.ContentText;
import ContentImage = Models.ContentImage;
import ContentLink = Models.ContentLink;
import ContentVideo = Models.ContentVideo;
import Tag = Models.Tag;
import Channel = Models.Channel;
import Image = Models.Image;
import Video = Models.Video;
import Tickser = Models.Tickser;
import Language = Models.Language;
import Category = Models.Category;
import Condition = Models.Condition;
import ContentCondition = Models.ContentCondition;
import Country = Models.Country;

module Content {

  export enum Status  {
    ACTIVE = 0,
    INACTIVE = 1,
    DEACTIVATED = 2,
    REPORTED = 3,
    FOR_REVIEW = 4,
    SUSPENDED = 5
  }

  export enum Type {
    TEXT = 0,
    IMAGE = 1,
    LINK = 2,
    VIDEO = 3
  }

  export class Scopes {
    public static INCLUDE_TICKSER: string = 'includeTickser';
    public static INCLUDE_CHANNEL_WITH_IMAGES: string = 'INCLUDE_CHANNEL_WITH_IMAGES';
    public static INCLUDE_REQUIRED_ACTIVE_CHANNEL_FOR_TICKSER: string = 'INCLUDE_REQUIRED_ACTIVE_CHANNEL_FOR_TICKSER';
    public static INCLUDE_REQUIRED_PUBLIC_ACTIVE_CHANNEL: string = 'INCLUDE_REQUIRED_PUBLIC_ACTIVE_CHANNEL';
    public static INCLUDE_REQUIRED_ACTIVE_CHANNEL: string = 'INCLUDE_REQUIRED_ACTIVE_CHANNEL';
    public static INCLUDE_TAGS: string = 'INCLUDE_TAGS';
    public static INCLUDE_TEASER_IMAGE: string = 'includeTeaserImage';
    public static INCLUDE_TEASER_VIDEO: string = 'INCLUDE_TEASER_VIDEO';
    public static INCLUDE_CONDITIONS: string = 'includeConditions';
    public static INCLUDE_CONTENT_DATA_IMAGES: string = 'includeContentDataImages';
    public static INCLUDE_CONTENT_DATA_TEXT: string = 'includeContentDataText';
    public static INCLUDE_CONTENT_DATA_LINK: string = 'includeContentDataLink';
    public static INCLUDE_CONTENT_DATA_VIDEO: string = 'includeContentDataVideo';
    public static INCLUDE_LANGUAGE: string = 'INCLUDE_LANGUAGE';
    public static INCLUDE_CATEGORY: string = 'INCLUDE_CATEGORY';
    public static IS_PUBLISHED: string = 'isPublished';
    public static IS_ACTIVE: string = 'isActive';
    public static IS_NOT_EXPIRED: string = 'isNotExpired';
    public static IS_ACTIVE_OR_INACTIVE: string = 'isActiveOrInactive';
    public static INCLUDE_COMMON_DATA: any = [
      Scopes.INCLUDE_TAGS,
      Scopes.INCLUDE_LANGUAGE,
      Scopes.INCLUDE_CATEGORY,
      Scopes.INCLUDE_TEASER_IMAGE,
      Scopes.INCLUDE_TEASER_VIDEO
    ];
  }

  export interface Attributes {
    id?: number;
    position_in_discover?: number;
    likes?: number;
    dislikes?: number;
    title?: string;
    type?: Type;
    description?: string;
    teaserText?: string;
    teaserImageId?: number;
    teaserVideoId?: number;
    keywords?: string;
    status?: Status;
    tickPrice?: number;
    expiryDate?: Date;
    published?: boolean;
    rootContentId?: number;
    commentCount?: number | Sequelize.literal;
    videoLength?: number;
    textLength?: number;
    imageCount?: number;
    campaign?: boolean;
    campaignPercent?: number;
    geographicCountries?: string[];

    tickserId?: number;
    channelId?: number;
    languageId?: number;
    categoryId?: number;
    randomPosition?: number | Sequelize.literal;
    createdAt?: Date;
    updatedAt?: Date;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    contentText?: ContentText.Instance;
    contentLink?: ContentLink.Instance;
    contentVideo?: ContentVideo.Instance;
    parentContent?: Content.Instance;
    teaserImage?: Image.Instance;
    teaserVideo?: Video.Instance;
    channel?: Channel.Instance;

    contentImages?: ContentImage.Instance[];
    conditions?: Condition.Instance[];
    tags?: Tag.Instance[];
    tickser?: Tickser.Instance;
    category?: Category.Instance;
    language?: Language.Instance;

    isValidCountry(countryA2Code: string): boolean;
    addConditions: Sequelize.BelongsToManyAddAssociationsMixin<Condition.Instance, number, {}>;
    removeConditions: Sequelize.BelongsToManyRemoveAssociationsMixin<Condition.Instance, number>;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions): Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    countByTeaserImageId(teaserImageId: number): Promise<number>;
    countByTeaserVideoId(teaserVideoId: number): Promise<number>;

    findAllByIds(ids: number[], options?: Sequelize.FindOptions): Promise<Instance[]>;
    findAllByTeaserImageIds(teaserImageId: number[]): Promise<Instance[]>;
    findAllByTeaserVideoIds(teaserVideoIds: number[]): Promise<Instance[]>;
  }

  export let Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {

    let methods: ClassMethods = {
      countByTeaserImageId: function (teaserImageId: number): Promise<number> {
        return Model.count({
          where: {
            teaserImageId: teaserImageId
          }
        });
      },
      countByTeaserVideoId: function (teaserVideoId: number): Promise<number> {
        return Model.count({
          where: {
            teaserVideoId: teaserVideoId
          }
        });
      },
      findAllByTeaserVideoIds: function (teaserVideoIds: number[]): Promise<Instance[]> {
        return Model.findAll({
          where: {
            teaserVideoId: {
              $in: teaserVideoIds
            }
          },
          attributes: ['teaserVideoId'],
        });
      },
      findAllByTeaserImageIds: function (teaserImageIds: number[]): Promise<Instance[]> {
        return Model.findAll({
          where: {
            teaserImageId: {
              $in: teaserImageIds
            }
          },
          attributes: ['teaserImageId'],
        });
      },
      findAllByIds: function (ids: number[], options?: Sequelize.FindOptions): Promise<Instance[]> {
        let opt = _.merge({
          where: {
            id: {
              $in: ids
            }
          },
        }, options);
        return this.findAll(opt);
      },
      addScopes: function (models) {
        Model.addScope(Scopes.INCLUDE_TICKSER, function () {
          return {
            include: [
              {
                model: models.Tickser.Model,
                as: 'tickser',
              }
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_CHANNEL_WITH_IMAGES, function () {
          return {
            include: [
              {
                model: models.Channel.Model.scope([
                  models.Channel.Scopes.INCLUDE_COVER_IMAGE,
                  models.Channel.Scopes.INCLUDE_BACKGROUND_IMAGE
                ]),
                as: 'channel',
              },
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_REQUIRED_ACTIVE_CHANNEL_FOR_TICKSER, function (channelId: number, tickserId: number) {
          return {
            include: [
              {
                required: true,
                model: models.Channel.Model.scope([
                  models.Channel.Scopes.INCLUDE_COVER_IMAGE,
                  models.Channel.Scopes.INCLUDE_BACKGROUND_IMAGE
                ]),
                as: 'channel',
                where: {
                  id: channelId,
                  status: models.Channel.Status.ACTIVE
                },
                include: [
                  {
                    required: true,
                    model: models.TickserChannel.Model,
                    as: 'tickserChannels',
                    where: {
                      tickserId: tickserId
                    }
                  }
                ]

              },
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_REQUIRED_PUBLIC_ACTIVE_CHANNEL, function (channelId: number) {
          const whereOptions: Sequelize.WhereOptions = {
            status: models.Channel.Status.ACTIVE,
            type: models.Channel.Type.PUBLIC
          };

          if (channelId != null) {
            whereOptions['id'] = channelId;
          }

          return {
            include: [
              {
                required: true,
                model: models.Channel.Model.scope([
                  models.Channel.Scopes.INCLUDE_COVER_IMAGE,
                  models.Channel.Scopes.INCLUDE_BACKGROUND_IMAGE
                ]),
                as: 'channel',
                where: whereOptions
              },
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_REQUIRED_ACTIVE_CHANNEL, function () {
          return {
            include: [
              {
                required: true,
                model: models.Channel.Model.scope([
                  models.Channel.Scopes.INCLUDE_COVER_IMAGE,
                  models.Channel.Scopes.INCLUDE_BACKGROUND_IMAGE
                ]),
                as: 'channel',
                where: {
                  status: models.Channel.Status.ACTIVE
                }
              },
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_TAGS, function () {
          return {
            include: [
              {
                model: models.Tag.Model,
                as: 'tags',
              },
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_TEASER_IMAGE, function () {
          return {
            include: [
              {
                model: models.Image.Model.scope(models.Image.Scopes.INCLUDE_FILE),
                as: 'teaserImage',
              },
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_TEASER_VIDEO, function () {
          return {
            include: [
              {
                required: false,
                model: models.Video.Model.scope([models.Video.Scopes.INCLUDE_VIDEO_FILES, models.Video.Scopes.INCLUDE_THUMBNAIL_IMAGE]),
                as: 'teaserVideo'
              }
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_CONDITIONS, function () {
          return {
            include: [
              {
                model: models.Condition.Model,
                as: 'conditions',
              }
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_CONTENT_DATA_IMAGES, function () {
          return {
            include: [
              {
                required: false,
                model: models.ContentImage.Model.scope(models.ContentImage.Scopes.INCLUDE_IMAGE),
                as: 'contentImages'
              }
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_CONTENT_DATA_TEXT, function () {
          return {
            include: [
              {
                required: false,
                model: models.ContentText.Model,
                as: 'contentText'
              }
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_CONTENT_DATA_LINK, function () {
          return {
            include: [
              {
                required: false,
                model: models.ContentLink.Model,
                as: 'contentLink'
              }
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_CONTENT_DATA_VIDEO, function () {
          return {
            include: [
              {
                required: false,
                model: models.ContentVideo.Model.scope(models.ContentVideo.Scopes.INCLUDE_VIDEO),
                as: 'contentVideo'
              }
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_LANGUAGE, function () {
          return {
            include: [
              {
                required: false,
                model: models.Language.Model,
                as: 'language'
              }
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_CATEGORY, function () {
          return {
            include: [
              {
                required: false,
                model: models.Category.Model,
                as: 'category'
              }
            ]
          }
        });
        Model.addScope(Scopes.IS_PUBLISHED, function () {
          return {
            where: {
              published: true
            }
          }
        });
        Model.addScope(Scopes.IS_ACTIVE, function () {
          return {
            where: {
              status: Status.ACTIVE
            }
          }
        });
        Model.addScope(Scopes.IS_ACTIVE_OR_INACTIVE, function () {
          return {
            where: {
              $or: [
                {status: Status.ACTIVE},
                {status: Status.INACTIVE}
              ]
            }
          }
        });
        Model.addScope(Scopes.IS_NOT_EXPIRED, function () {
          return {
            where: {
              expiryDate: {
                $gte: new Date()
              }
            }
          }
        });
      },
      associate: function (models) {
        Model.hasOne(models.ContentText.Model, {
          as: 'contentText',
          foreignKey: {name: 'contentId', field: 'content_id'}
        });
        Model.hasOne(models.ContentLink.Model, {
          as: 'contentLink',
          foreignKey: {name: 'contentId', field: 'content_id'}
        });
        Model.hasOne(models.ContentVideo.Model, {
          as: 'contentVideo',
          foreignKey: {name: 'contentId', field: 'content_id'}
        });
        Model.hasOne(models.Content.Model, {as: 'parentContent', foreignKey: {name: 'parentId', field: 'parent_id'}});

        Model.hasMany(models.ContentImage.Model, {
          as: 'contentImages',
          foreignKey: {name: 'contentId', field: 'content_id'}
        });
        Model.hasMany(models.TicksTransaction.Model, {
          as: 'ticksTransaction',
          foreignKey: {name: 'contentId', field: 'content_id'}
        });
        Model.hasMany(models.Content.Model, {as: 'childContents', foreignKey: {name: 'parentId', field: 'parent_id'}});
        Model.hasMany(models.History.Model, {as: 'history', foreignKey: {name: 'contentId', field: 'content_id'}});
        Model.hasMany(models.Feed.Model, {as: 'feeds', foreignKey: {name: 'contentId', field: 'content_id'}});

        Model.belongsTo(models.Channel.Model, {as: 'channel', foreignKey: {name: 'channelId', field: 'channel_id'}});
        Model.belongsTo(models.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
        Model.belongsTo(models.Category.Model, {
          as: 'category',
          foreignKey: {name: 'categoryId', field: 'category_id'}
        });
        Model.belongsTo(models.Language.Model, {
          as: 'language',
          foreignKey: {name: 'languageId', field: 'language_id'}
        });

        Model.belongsTo(models.Image.Model, {
          as: 'teaserImage',
          foreignKey: {name: 'teaserImageId', field: 'teaser_image_id'}
        });
        Model.belongsTo(models.Video.Model, {
          as: 'teaserVideo',
          foreignKey: {name: 'teaserVideoId', field: 'teaser_video_id'}
        });

        Model.belongsToMany(models.Tag.Model, {
          as: 'tags',
          through: models.ContentTag.Model,
          foreignKey: {name: 'contentId', field: 'content_id'}
        });

        Model.belongsToMany(models.Condition.Model, {
          as: 'conditions',
          through: models.ContentCondition.Model,
          foreignKey: {name: 'contentId', field: 'content_id'}
        });
      },
    };

    Model = <Class> connection.define<Instance, Attributes>('Content', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      position_in_discover: {
        type: Sequelize.INTEGER,
        field: 'position_in_discover'
      },
      likes: {
        type: Sequelize.BIGINT,
        field: 'likes'
      },
      dislikes: {
        type: Sequelize.BIGINT,
        field: 'dislikes'
      },
      title: {
        type: Sequelize.TEXT,
        field: 'title'
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      description: {
        type: Sequelize.TEXT,
        field: 'description'
      },
      teaserText: {
        type: Sequelize.TEXT,
        field: 'teaser_text'
      },
      teaserImageId: {
        type: Sequelize.BIGINT,
        field: 'teaser_image_id'
      },
      teaserVideoId: {
        type: Sequelize.BIGINT,
        field: 'teaser_video_id'
      },
      keywords: {
        type: Sequelize.STRING,
        field: 'keywords'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      tickPrice: {
        type: Sequelize.DECIMAL(18, 6),
        field: 'tick_price',
        // validate: {
        //   min: 1
        // }
      },
      expiryDate: {
        type: Sequelize.DATE,
        field: 'expiry_date'
      },
      published: {
        type: Sequelize.BOOLEAN,
        field: 'published'
      },
      rootContentId: {
        type: Sequelize.BIGINT,
        field: 'root_content_id'
      },
      commentCount: {
        type: Sequelize.INTEGER,
        field: 'comment_count'
      },
      videoLength: {
        type: Sequelize.INTEGER,
        field: 'video_length'
      },
      textLength: {
        type: Sequelize.INTEGER,
        field: 'text_length'
      },
      imageCount: {
        type: Sequelize.INTEGER,
        field: 'image_count'
      },
      campaign: {
        type: Sequelize.BOOLEAN,
        field: 'campaign',
        defaultValue: false
      },
      campaignPercent: {
        type: Sequelize.NUMERIC(3, 2),
        field: 'campaign_percent',
        validate: {
          min: 0.00,
          max: 1
        }
      },
      randomPosition: {
        type: Sequelize.INTEGER,
        field: 'random_position'
      },
      geographicCountries: {
        type: Sequelize.ARRAY(Sequelize.STRING(2)),
        field: 'geographic_countries',
        comment: 'ISO_3166-1_alpha-2 codes'
      },
      createdAt: {
        type: Sequelize.DATE,
        field: 'created_at'
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at'
      },
      categoryId: {
        type: Sequelize.BIGINT,
        field: 'category_id'
      },
      languageId: {
        type: Sequelize.BIGINT,
        field: 'language_id'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      instanceMethods: {
        isValidCountry: function (countryA2Code: string): boolean {
          if (this.geographicCountries == null) {
            return true;
          }

          if (_.isEmpty(countryA2Code)) {
            return false;
          }

          return _.includes(this.geographicCountries, Country.countryCodeToUpperCase(countryA2Code));
        }
      },
      classMethods: methods,
      indexes: [{
        name: 'Content_geographic_countries_idx',
        fields: ['geographic_countries'],
        method: 'GIN'  // https://www.postgresql.org/docs/9.4/static/indexes-types.html
      }, {
        name: 'Content_id_idx',
        unique: true,
        fields: ['id']
      }]
    });
    return Model;
  }

  export const RANDOM_TO_SHUFFLE_CONTENTS_IN_DISCOVER: number = 1000;

  export function randomNumberToShuffle() {
    return Math.floor(Math.random() * Content.RANDOM_TO_SHUFFLE_CONTENTS_IN_DISCOVER);
  }

}

export = Content;