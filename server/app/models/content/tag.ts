/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Content = Models.Content;

module Tag {
  export interface Attributes {
    id?: number;
    name?: string;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    contents?: Content.Instance[];
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('Tag', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.TEXT,
        field: 'name'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsToMany(models.Content.Model, {as: 'contents', through: models.ContentTag.Model, foreignKey: {name: 'tagId', field: 'tag_id'}});
        }
      }
    });
    return Model;
  }
}

export = Tag;