/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Content = Models.Content;
import Tickser = Models.Tickser;
import Video = Models.Video;
import Image = Models.Image;

module Feed {

  export enum Status {
    ACTIVE = 0,
    INACTIVE = 1,
    DEACTIVATED = 2,
    SUSPENDED = 3
  }

  export interface Attributes {
    id?: number;
    level?: number;
    text?: string;
    contentId?: number;
    tickserId?: number;
    parentId?: number;
    videoId?: number;
    imageId?: number;
    status?: Status;
    likes?: number;
    dislikes?: number;
    created_at?: Date;
    updated_at?: Date;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    content?: Content.Instance;
    tickser?: Tickser.Instance;
    parent?: Instance;
    video?: Video.Instance;
    image?: Image.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;

  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    countByImageId(imageId: number): Promise<number>;
    countByVideoId(videoId: number): Promise<number>;

    findAllByVideoIds(videoIds: number[]): Promise<Instance[]>;
    findAllByImageIds(imageIds: number[]): Promise<Instance[]>;
  }

  export class Scopes {
    public static IMAGE: string = 'image';  // ToDo [d.borisov] is it used?
    public static VIDEO: string = 'video';  // ToDo [d.borisov] is it used?
    public static ATTR_IMAGE_ID: string = 'ATTR_IMAGE_ID';
    public static ATTR_VIDEO_ID: string = 'ATTR_VIDEO_ID';
    public static CONTENT: string = 'CONTENT';
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    var methods: ClassMethods = {
      countByImageId: function (imageId: number): Promise<number> {
        return Model.count({
          where: {
            imageId: imageId
          },
        });
      },

      countByVideoId: function (videoId: number): Promise<number> {
        return Model.count({
          where: {
            videoId: videoId
          },
        });
      },

      findAllByVideoIds: function (videoIds: number[]): Promise<Instance[]> {
        return Model.scope(Scopes.ATTR_VIDEO_ID).findAll({
          where: {
            videoId: {
              $in: videoIds
            }
          },
        });
      },

      findAllByImageIds: function (imageIds: number[]): Promise<Instance[]> {
        return Model.scope(Scopes.ATTR_IMAGE_ID).findAll({
          where: {
            imageId: {
              $in: imageIds
            }
          },
        });
      },

      associate: function (models) {
        Model.belongsTo(models.Content.Model, {as: 'content', foreignKey: {name: 'contentId', field: 'content_id'}});
        Model.belongsTo(models.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickserId'}});
        Model.belongsTo(models.Feed.Model, {as: 'parent', foreignKey: {name: 'parentId', field: 'parent_id'}});
        Model.belongsTo(models.Video.Model, {as: 'video', foreignKey: {name: 'videoId', field: 'video_id'}});
        Model.belongsTo(models.Image.Model, {as: 'image', foreignKey: {name: 'imageId', field: 'image_id'}});
      },

      addScopes: function (models) {
        Model.addScope(Scopes.IMAGE, {
          include: [
            {
              required: true,
              model: models.Image.Model,
              as: 'image',
            }
          ]
        });
        Model.addScope(Scopes.VIDEO, {
          include: [
            {
              required: true,
              model: models.Video.Model,
              as: 'video',
            }
          ]
        });
        Model.addScope(Scopes.ATTR_VIDEO_ID, {
          attributes: ['videoId']
        });
        Model.addScope(Scopes.ATTR_IMAGE_ID, {
          attributes: ['imageId']
        });
        Model.addScope(Scopes.CONTENT, {
          include: [
            {
              required: true,
              model: models.Content.Model,
              as: 'content'
            }
          ]
        });
      }
    };

    Model = <Class> connection.define('Feed', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      level: {
        type: Sequelize.INTEGER,
        field: 'level'
      },
      text: {
        type: Sequelize.STRING(1000),
        field: 'text'
      },
      contentId: {
        type: Sequelize.BIGINT,
        field: 'content_id'
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id'
      },
      parentId: {
        type: Sequelize.BIGINT,
        field: 'parent_id'
      },
      videoId: {
        type: Sequelize.BIGINT,
        field: 'video_id'
      },
      imageId: {
        type: Sequelize.BIGINT,
        field: 'image_id'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      likes: {
        type: Sequelize.BIGINT,
        field: 'likes'
      },
      dislikes: {
        type: Sequelize.BIGINT,
        field: 'dislikes'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: methods
    });
    return Model;
  }
}

export = Feed;