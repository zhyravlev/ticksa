/// <reference path="../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models/models');
import Tickser = Models.Tickser;
import Channel = Models.Channel;

module TickserChannel {

  export class Scopes {
    public static INCLUDE_CHANNEL: string = 'INCLUDE_CHANNEL';
  }

  export enum OwnerType {
    CREATOR = 0,
    MANAGER = 1,
    PARTICIPANT = 2
  }

  export interface Attributes {
    ownerType?: OwnerType;
    tickserId?: number;
    channelId?: number;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    tickser?: Tickser.Instance;
    channel?: Channel.Instance;
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    findAllByTickserId(tickserId: number): Promise<Instance[]>;
    findAllByTickserIdAndChannelId(tickserId, channelId): Promise<Instance[]>;
    findOneByTickserIdAndChannelId(tickserId, channelId): Promise<Instance>;
    findOneByChannelId(channelId: number): Promise<Instance>;
    countByTickserIdAndChannelId(tickserId, channelId): Promise<number>;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('TickserChannel', {
      ownerType: {
        type: Sequelize.INTEGER,
        field: 'owner_type'
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id',
        primaryKey: true
      },
      channelId: {
        type: Sequelize.BIGINT,
        field: 'channel_id',
        primaryKey: true
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        addScopes: function (models) {
          Model.addScope(Scopes.INCLUDE_CHANNEL, function () {
            return {
              include: [
                {
                  model: models.Channel.Model,
                  as: 'channel'
                }
              ]
            }
          });
        },
        associate: function (models) {
          Model.belongsTo(models.Tickser.Model, {
            as: 'tickser',
            foreignKey: {name: 'tickserId', field: 'tickser_id'}
          });
          Model.belongsTo(models.Channel.Model, {
            as: 'channel',
            foreignKey: {name: 'channelId', field: 'channel_id'}
          });
        },
        findAllByTickserId: function (tickserId) {
          return this.findAll({
            where: {
              tickserId: tickserId
            }
          });
        },
        findAllByTickserIdAndChannelId: function (tickserId, channelId) {
          return this.findAll({
            where: {
              tickserId: tickserId,
              channelId: channelId,
            }
          });
        },
        findOneByTickserIdAndChannelId: function (tickserId, channelId) {
          return this.findOne({
            where: {
              tickserId: tickserId,
              channelId: channelId,
            }
          });
        },
        findOneByChannelId(channelId: number): Promise<Instance> {
          return this.findOne({
            where: {
              channelId: channelId,
            }
          });
        },
        countByTickserIdAndChannelId: function (tickserId, channelId) {
          return this.count({
            where: {
              tickserId: tickserId,
              channelId: channelId,
            }
          });
        },
      },
      scopes: {
        channel: function () {
          return {
            include: [
              {
                required: true,
                model: Models.Channel.Model,
                as: 'channel'
              }
            ]
          }
        }
      }
    });
    return Model;
  }
}

export = TickserChannel;