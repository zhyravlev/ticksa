/// <reference path="../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('./models');
import Content = Models.Content;

module Language {
  export class Scopes {
    public static ORDER_POSITION: string = 'orderByPosition';
  }
  export interface Attributes {
    id?: number;
    name?: string;
    key?: string;
    position?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define<Instance, Attributes>('Language', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.TEXT,
        field: 'name'
      },
      key: {
        type: Sequelize.TEXT,
        field: 'key'
      },
      position: {
        type: Sequelize.BIGINT,
        field: 'position'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
        },
        addScopes: function (models) {
          Model.addScope(Scopes.ORDER_POSITION, function () {
            return {
              order: [
                ['position', 'ASC'],
                ['name', 'ASC'],
              ]
            }
          }, {override: true});
        },
      }
    });
    return Model;
  }
}

export = Language;