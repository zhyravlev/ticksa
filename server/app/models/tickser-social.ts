import Sequelize = require('sequelize');
import Promise = require('bluebird');
import Ticksa = require('ticksa');

import Models = require('./models')
import Tickser = Models.Tickser;

module TickserSocial {

  export enum Type {
    GOOGLE = 0,
    FACEBOOK = 1,
    TWITTER = 2
  }

  export class Scopes {
    public static INCLUDE_TICKSER: string = 'INCLUDE_TICKSER';
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
  }

  export interface Attributes {
    id?: number;
    type: Type;
    socialId: string;
    tickserId: number;

    tickser?: Tickser.Instance;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    let methods: ClassMethods = {
      associate: function (models) {
        Model.belongsTo(models.Tickser.Model, {
          as: 'tickser',
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });
      },
      addScopes: function (models) {
        Model.addScope(Scopes.INCLUDE_TICKSER, {
          include: [
            {
              required: true,
              model: models.Tickser.Model,
              as: 'tickser'
            }
          ]
        });
      }
    };

    Model = <TickserSocial.Class> connection.define<Instance, Attributes>('TickserSocial', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type'
      },
      socialId: {
        type: Sequelize.TEXT,
        field: 'social_id'
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: methods
    });
    return Model;
  }
}
export = TickserSocial;