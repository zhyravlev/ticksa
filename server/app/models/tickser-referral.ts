/// <reference path="../typings/tsd.d.ts"/>

import _ = require('lodash');
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models/models');
import Tickser = Models.Tickser;

module TickserReferral {
  export enum Type {
    MESSAGE = 0,
    CHANNEL_EDIT_INVITE = 1
  }
  export interface Attributes {
    id?: number;
    tickserId?: number;
    referrerId?: number;
    commissionTicks?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    tickser?: Tickser.Instance;
    referrer?: Tickser.Instance;
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
    findOneByTickserId(tickserId: number, options?: Sequelize.FindOptions): Promise<Instance>;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('TickserReferral', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id'
      },
      referrerId: {
        type: Sequelize.BIGINT,
        field: 'referrer_id'
      },
      commissionTicks: {
        type: Sequelize.NUMERIC(18, 6),
        field: 'commission_ticks'
      }
    }, {
      underscored: true,
      freezeTableName: true,
      scopes: {
        referrer: function () {
          return {
            include: [
              {
                model: Tickser.Model,
                as: 'referrer',
              }
            ]
          }
        }
      },
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Tickser.Model, {as: 'tickser', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
          Model.belongsTo(models.Tickser.Model, {
            as: 'referrer',
            foreignKey: {name: 'referrerId', field: 'referrer_id'}
          });
        },
        findOneByTickserId: function (tickserId: number, options?: Sequelize.FindOptions): Promise<Instance> {
          let opt = _.merge({
            where: {
              tickserId: tickserId
            }
          }, options);

          return this.findOne(opt);
        }
      }
    });
    return Model;
  }
}

export = TickserReferral;