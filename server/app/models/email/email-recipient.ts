/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Tickser = Models.Tickser;

import ValueSelector = require('../../utils/value-selector');

module EmailRecipient {

  export enum Status {
    NEW = 0,
    FAILED = 1,
    SUCCESS = 2
  }

  export enum Reason {
    APP = 0,
    TAA = 1,
    TEST = 2
  }

  export type PossibleLanguage = 'English' |
    'German' |
    'French' |
    'Russian' |
    'Hungarian' |
    'Italian' |
    'Czech' |
    'Slovak' |
    'Dutch' |
    'Romanian' |
    'Polish' |
    'Spanish';

  // this dictionary matches language returned by 'locale' library to EmailRecipient.PossibleLanguage
  const HTTP_LANGUAGE_TO_POSSIBLE_LANGUAGE: ValueSelector.KeyValueDictionary<PossibleLanguage> =
    ValueSelector.KeyValueDictionary.forDictionary<PossibleLanguage>({
      en: 'English',
      de: 'German',
      fr: 'French',
      ru: 'Russian',
      hu: 'Hungarian',
      it: 'Italian',
      cs: 'Czech',
      sk: 'Slovak',
      nl: 'Dutch',
      ro: 'Romanian',
      po: 'Polish',
      es: 'Spanish',
    });

  export const LANGUAGE_SELECTOR: ValueSelector.ValueSelector<PossibleLanguage> =
    new ValueSelector.ValueSelector<PossibleLanguage>(HTTP_LANGUAGE_TO_POSSIBLE_LANGUAGE, 'English');

  export const Template = {
    CONTACT_TICKSA: 'CONTACT_TICKSA',
    REGISTRATION: 'REGISTRATION',
    PASSWORD_CHANGED: 'PASSWORD_CHANGED',
    FORGOT_PASSWORD: 'FORGOT_PASSWORD',
    EMAIL_CHANGED: 'EMAIL_CHANGED',
    PHONE_VERIFIED: 'PHONE_VERIFIED',
    PHONE_VERIFIED_NO_PROMO_TICKS: 'PHONE_VERIFIED_NO_PROMO_TICKS',
    CHANNEL_INVITE_TO_TICKSER: 'CHANNEL_INVITE_TO_TICKSER',
    CHANNEL_INVITE_TO_EMAIL: 'CHANNEL_INVITE_TO_EMAIL',
    CONTENT_STATUS: 'CONTENT_STATUS',
    TICKSER_STATUS_SUSPENDED: 'TICKSER_STATUS_SUSPENDED',
    THANK_YOU_FOR_REPORT: 'THANK_YOU_FOR_REPORT',
    THANK_YOU_FOR_FEEDBACK: 'THANK_YOU_FOR_FEEDBACK',
    THANK_YOU_FOR_SUGGESTION: 'THANK_YOU_FOR_SUGGESTION',
    THANK_YOU_FOR_COMPLAINT: 'THANK_YOU_FOR_COMPLAINT',
    CASH_IN_SUCCESSFUL: 'CASH_IN_SUCCESSFUL',
    REVENUE_SHARED: 'REVENUE_SHARED',
    REVENUE_SHARED_NO_DATE_EXPIRED: 'REVENUE_SHARED_NO_DATE_EXPIRED',
    CONTENT_CARD: 'CONTENT_CARD'
  };

  export interface Attributes {
    id?: number;
    templateName?: string;
    tickserId?: number;
    email?: string;
    status?: Status;
    reason?: Reason;
    params?: string;
    errorMessage?: string;
    parsedTemplate?: string;
    parsedSubject?: string;
    createdById?: number;
    language?: PossibleLanguage;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }

  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions): Class;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('EmailRecipient', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      templateName: {
        type: Sequelize.STRING,
        field: 'template_name'
      },
      tickserId: {
        type: Sequelize.BIGINT,
        field: 'tickser_id'
      },
      email: {
        type: Sequelize.STRING,
        field: 'email',
        allowNull: false
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      reason: {
        type: Sequelize.INTEGER,
        field: 'reason',
        defaultValue: Reason.APP
      },
      params: {
        type: Sequelize.TEXT,
        field: 'params'
      },
      errorMessage: {
        type: Sequelize.TEXT,
        field: 'error_message'
      },
      infoMessage: {
        type: Sequelize.TEXT,
        field: 'info_message'
      },
      parsedTemplate: {
        type: Sequelize.TEXT,
        field: 'parsed_template'
      },
      parsedSubject: {
        type: Sequelize.STRING,
        field: 'parsed_subject'
      },
      createdById: {
        type: Sequelize.BIGINT,
        field: "created_by",
        allowNull: false
      },
      updatedById: {
        type: Sequelize.BIGINT,
        field: "update_by"
      },
      language: {
        type: Sequelize.STRING,
        field: 'language'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      indexes: [{
        name: 'tickser_id_idx',
        fields: ['tickser_id']
      }],
      classMethods: {
        associate: function (models) {

        }
      }
    });
    return Model;
  }
}

export = EmailRecipient;