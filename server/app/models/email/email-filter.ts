/// <reference path="../../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import Models = require('../models');
import Tickser = Models.Tickser;

module EmailFilter {
  export enum Type {
    EQUALS = 0,
    REG_EXP = 1,
  }
  export enum Reason {
    ADMIN = 0,
    SUBSCRIPTION = 1
  }
  export interface Attributes {
    id?: number;
    value?: string;
    type?: Type;
    reason?: Reason;
    createdById?: number;
  }
  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
  }
  export interface Class extends Ticksa.Model<Instance, Attributes> {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions) : Class;
  }
  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Class {
    Model = <Class> connection.define('EmailFilter', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      value: {
        type: Sequelize.STRING,
        field: 'value',
        allowNull: false
      },
      type: {
        type: Sequelize.INTEGER,
        field: 'type',
        allowNull: false,
        defaultValue: Type.EQUALS
      },
      reason: {
        type: Sequelize.INTEGER,
        field: 'reason',
        allowNull: false,
        defaultValue: Reason.ADMIN
      },
      createdById: {
        type: Sequelize.BIGINT,
        field: "created_by",
        allowNull: false
      },
      updatedById: {
        type: Sequelize.BIGINT,
        field: "update_by"
      },
    }, {
      underscored: true,
      freezeTableName: true,
      classMethods: {
        associate: function (models) {
          Model.belongsTo(models.Tickser.Model, {as: 'createdBy', foreignKey: {name: 'createdById', field: 'created_by'}});
          Model.belongsTo(models.Tickser.Model, {as: 'updatedBy', foreignKey: {name: 'updatedById', field: 'update_by'}});
        }
      },
      scopes: {
        subscription: function () {
          return {
            where: {
              type: Type.EQUALS,
              reason: Reason.SUBSCRIPTION,
            }
          }
        },
      }
    });
    return Model;
  }
}

export = EmailFilter;