/// <reference path="../typings/tsd.d.ts"/>
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');

import Models = require('./models');
import Image = Models.Image;
import Account = Models.Account;
import Country = Models.Country;
import TickserSocial = Models.TickserSocial;
import Channel = Models.Channel;
import TickserChannel = Models.TickserChannel;
import {TickserReferral} from './models';

module Tickser {
  export enum Status {
    NEW = 0,
    CONFIRMED = 1,
    DEACTIVATED = 2,
    ACTIVE = 3,
    SUSPENDED = 4,
  }

  export enum SystemTicksers {
    SYSTEM = 1,  // TODO. currently hardcoded. should be created
    EMAILER = 1,  // TODO. currently hardcoded.
  }

  export class Scopes {
    public static IS_ACTIVE_OR_CONFIRMED = 'isActiveOrConfirmed';
    public static INCLUDE_ACCOUNT: string = 'account';
    public static INCLUDE_PROFILE_IMAGE: string = 'profileImage';
  }

  export interface Class extends Ticksa.Model<Instance, Attributes>, ClassMethods {
    scope(options?: string | Array<string> | Sequelize.ScopeOptions | Sequelize.WhereOptions): Class;
  }

  export interface Instance extends Sequelize.Instance<Attributes>, Attributes {
    toJSON(): any;
    checkIfActive(): boolean;
    checkIfSuspended(): boolean;

    account?: Account.Instance;
    country?: Country.Instance
    profileImage?: Image.Instance;
    socials?: TickserSocial.Instance[];

    addChannel: Sequelize.BelongsToManyAddAssociationMixin<Channel.Instance, number, TickserChannel.Attributes>
  }

  export interface ClassMethods extends Ticksa.ClassMethods {
    findOneByUsername(username: string) : Promise<Instance>;
    findOneByEmail(email: string) : Promise<Instance>;
    setLastLogin(userId: number): Promise<[number, Array<Instance>]>;
    setLoginIpAndCountry(userId: number, loginIp: string, loginIpCountry: string): Promise<[number, Array<Instance>]>;
    countUniqueVerifiedPhones(userId: number, phone: string): Promise<number>;
    countByProfileImageId(profileImageId: number): Promise<number>;
    findAllByProfileImageIds(profileImageIds: number[]): Promise<Instance[]>;
  }

  export interface Attributes {
    id?: number;
    notificationCount?: number | Sequelize.literal;
    username?: string;
    email?: string;
    phone?: string;
    phoneVerificationCode?: string;
    isPhoneVerifiedAtLeastOnce?: boolean;
    isPhoneVerified?: boolean;
    name?: string;
    password?: string;
    preferences?: {};
    socialImageUrl?: string;
    profileImageId?: number;
    language?: string;
    countryId?: number;
    changePasswordUrl?: string;
    confirmationUrl?: string;
    status?: Status;
    confirmedDate?: Date;
    terminatedDate?: Date;
    lastLoginDate?: Date;
    registrationDate?: Date;
    timezone?: string;
    theme?: string;
    notificationMethod?: string;
    externalMediaEnabled?: boolean;
    systemChannelId?: number;
    defaultChannelId?: number;
    registerRedirectUrl?: string;
    isAdmin?: boolean;
    isTester?: boolean;
    loginIpAddress?: string;
    loginIpAddressCountry?: string;
    categoryFilter?: number[];
    languageFilter?: number[];

    account?: Account.Instance;
    referral?: TickserReferral.Instance;
  }

  export var Model: Class;

  export function initialize(connection: Sequelize.Sequelize): Models.Tickser.Class {
    let methods: ClassMethods = {
      associate: function (models) {
        Model.hasMany(models.TickserSocial.Model, {
          as: 'socials',
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });
        Model.hasMany(models.ChannelInvite.Model, {
          as: 'invites',
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });
        Model.hasMany(models.Notification.Model, {
          as: 'notifications',
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });
        Model.hasMany(models.Content.Model, {as: 'contents', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
        Model.hasMany(models.Feed.Model, {as: 'feeds', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
        Model.hasMany(models.Image.Model, {as: 'images', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
        Model.hasMany(models.Video.Model, {as: 'videos', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
        Model.hasMany(models.VoucherActivity.Model, {
          as: 'vouchersActivities',
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });

        Model.belongsTo(models.Country.Model, {as: 'country', foreignKey: {name: 'countryId', field: 'country_id'}});
        Model.belongsToMany(models.Channel.Model, {
          as: 'subscriptions',
          through: models.ChannelSubscription.Model,
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });
        Model.belongsToMany(models.Channel.Model, {
          as: 'channels',
          through: models.TickserChannel.Model,
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });
        Model.belongsToMany(models.Voucher.Model, {
          as: 'vouchers',
          through: models.TickserVoucher.Model,
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });
        Model.hasOne(models.Account.Model, {as: 'account', foreignKey: {name: 'tickserId', field: 'tickser_id'}});
        Model.hasOne(models.TickserReferral.Model, {
          as: 'referral',
          foreignKey: {name: 'tickserId', field: 'tickser_id'}
        });
        Model.belongsTo(models.Image.Model, {
          as: 'profileImage',
          constraints: false, // https://github.com/sequelize/sequelize/issues/1717 Cyclic dependency to Image
          foreignKey: {name: 'profileImageId', field: 'profile_image_id'}
        });
      },
      addScopes: function (models) {
        Model.addScope(Scopes.INCLUDE_PROFILE_IMAGE, function () {
          return {
            include: [
              {
                required: false,
                model: models.Image.Model.scope(models.Image.Scopes.INCLUDE_FILE),
                as: 'profileImage',
              }
            ]
          }
        });
        Model.addScope(Scopes.IS_ACTIVE_OR_CONFIRMED, {
          where: {
            $or: [
              {
                status: Status.ACTIVE,
              },
              {
                status: Status.CONFIRMED
              }
            ]
          }
        });
        Model.addScope(Scopes.INCLUDE_ACCOUNT, {
          include: [
            {
              required: true,
              model: models.Account.Model,
              as: 'account'
            }
          ]
        });
      },
      findOneByUsername: function (username: string) : Promise<Instance> {
        return this.findOne({
          where: {
            username: username
          }
        });
      },
      findOneByEmail: function (email) {
        return this.findOne({
          where: {
            email: email
          }
        });
      },
      setLastLogin: function (userId: number): Promise<[number, Array<Instance>]> {
        return this.update({
          lastLoginDate: new Date()
        }, {
          where: {
            id: userId
          }
        });
      },
      setLoginIpAndCountry: function (userId: number, loginIp: string, loginIpCountry: string): Promise<[number, Array<Instance>]> {
        return this.update({
          loginIpAddress: loginIp,
          loginIpAddressCountry: loginIpCountry
        }, {
          where: {
            id: userId,
            loginIpAddress: null,
            loginIpAddressCountry: null
          }
        });
      },
      countUniqueVerifiedPhones: function (userId: number, phone: string): Promise<number> {
        return this.count({
          where: {
            id: {$ne: userId},
            phone: phone,
            isPhoneVerified: true
          }
        });
      },
      countByProfileImageId: function (profileImageId: number): Promise<number> {
        return Model.count({
          where: {
            profileImageId: profileImageId
          }
        });
      },
      findAllByProfileImageIds: function (profileImageIds: number[]): Promise<Instance[]> {
        return Model.findAll({
          where: {
            profileImageId: {
              $in: profileImageIds
            }
          },
          attributes: ['profileImageId']
        });
      }
    };

    Model = <Class> connection.define<Instance, Attributes>('Tickser', {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      notificationCount: {
        type: Sequelize.INTEGER,
        field: 'notification_count'
      },
      username: {
        type: Sequelize.STRING(50),
        field: 'username',
        unique: true
      },
      email: {
        type: Sequelize.STRING(50),
        field: 'email'
      },
      phone: {
        type: Sequelize.STRING(50),
        field: 'phone'
      },
      phoneVerificationCode: {
        type: Sequelize.STRING(4),
        field: 'phone_verification_code'
      },
      isPhoneVerifiedAtLeastOnce: {
        type: Sequelize.BOOLEAN,
        field: 'is_phone_verified_at_least_once'
      },
      isPhoneVerified: {
        type: Sequelize.BOOLEAN,
        field: 'is_phone_verified'
      },
      name: {
        type: Sequelize.STRING,
        field: 'name'
      },
      password: {
        type: Sequelize.STRING,
        field: 'password'
      },
      preferences: {
        type: Sequelize.JSON,
        field: 'preferences'
      },
      socialImageUrl: {
        type: Sequelize.STRING(255),
        field: 'social_image_url'
      },
      profileImageId: {
        type: Sequelize.BIGINT,
        field: 'profile_image_id'
      },
      language: {
        type: Sequelize.STRING(50),
        field: 'language'
      },
      countryId: {
        type: Sequelize.BIGINT,
        field: 'country_id'
      },
      changePasswordUrl: {
        type: Sequelize.STRING(255),
        field: 'change_password_url'
      },
      confirmationUrl: {
        type: Sequelize.STRING,
        field: 'confirmation_url'
      },
      status: {
        type: Sequelize.INTEGER,
        field: 'status'
      },
      confirmedDate: {
        type: Sequelize.DATE,
        field: 'confirmed_date'
      },
      terminatedDate: {
        type: Sequelize.DATE,
        field: 'terminated_date'
      },
      lastLoginDate: {
        type: Sequelize.DATE,
        field: 'last_login_date'
      },
      registrationDate: {
        type: Sequelize.DATE,
        field: 'created_date'
      },
      timezone: {
        type: Sequelize.STRING,
        field: 'timezone'
      },
      theme: {
        type: Sequelize.STRING,
        field: 'theme'
      },
      notificationMethod: {
        type: Sequelize.STRING,
        field: 'notification_method'
      },
      externalMediaEnabled: {
        type: Sequelize.BOOLEAN,
        field: 'external_medial_enabled'
      },
      systemChannelId: {
        type: Sequelize.BIGINT,
        field: 'system_channel_id'
      },
      defaultChannelId: {
        type: Sequelize.BIGINT,
        field: 'default_channel_id'
      },
      registerRedirectUrl: {
        type: Sequelize.STRING(255),
        field: 'register_redirect_url'
      },
      isAdmin: {
        type: Sequelize.BOOLEAN,
        field: 'is_admin',
        allowNull: false,
        defaultValue: false,
      },
      isTester: {
        type: Sequelize.BOOLEAN,
        field: 'is_tester',
        allowNull: false,
        defaultValue: false,
      },
      loginIpAddress: {
        type: Sequelize.STRING(50),
        field: 'login_ip_address'
      },
      loginIpAddressCountry: {
        type: Sequelize.STRING(50),
        field: 'login_ip_address_country'
      },
      categoryFilter: {
        type: Sequelize.JSONB,
        field: 'category_filter'
      },
      languageFilter: {
        type: Sequelize.JSONB,
        field: 'language_filter'
      },
    }, {
      underscored: true,
      freezeTableName: true,
      instanceMethods: {
        toJSON: function () {
          var values = this.get();
          delete values.password;
          return values;
        },
        checkIfActive: function () {
          return this.status == Status.CONFIRMED || this.status == Status.ACTIVE;
        },
        checkIfSuspended: function () {
          return this.status == Status.SUSPENDED;
        },

      },
      classMethods: methods
    });
    return Model;
  }
}

export = Tickser;


