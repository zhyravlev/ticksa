/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import * as request from 'request';
import * as Promise from 'bluebird';
import * as Ticksa from 'ticksa';

import * as config from '../config';
import * as conn from '../connection';
import * as RegistrationService from '../services/register';
import * as TickserService from '../services/tickser-service';
import * as SecurityService from '../services/security-service';
import * as Errors from '../errors';
import {
  Tickser,
  TickserSocial
} from '../models/models'
import {IFacebookMeResponse} from '../dto/facebook/facebook-messages';
const fb = require('fb');

export class TokenHeaderValidationResult {
  public tickser: Tickser.Instance;
  public error: any;

  constructor(tickser: Tickser.Instance, error: any) {
    this.tickser = tickser;
    this.error = error;
  }

  static validToken(tickser: Tickser.Instance): TokenHeaderValidationResult {
    return new TokenHeaderValidationResult(tickser, null);
  }

  static invalidToken(error: any): TokenHeaderValidationResult {
    return new TokenHeaderValidationResult(null, error);
  }
}

export async function validateTokenHeader(tokenHeader: string): Promise<TokenHeaderValidationResult> {
  if (tokenHeader == null) {
    return TokenHeaderValidationResult.invalidToken(new Errors.Unauthorized('No auth token provided'));
  }

  const tickser: Tickser.Instance = await findTickserByToken(tokenHeader);
  if (tickser == null) {
    return TokenHeaderValidationResult.invalidToken(new Errors.Unauthorized('Unknown user'));
  }

  if (!tickser.checkIfActive()) {
    return TokenHeaderValidationResult.invalidToken(new Errors.Unauthorized('Not confirmed/active'));
  }

  return TokenHeaderValidationResult.validToken(tickser);
}

export function local(username: string, password: string, req: Ticksa.Request): Promise<Token> {
  return TickserService.findSimpleTickserByUsername(username).then(function (tickser) {
    return Promise.all([
      SecurityService.comparePassword(password, tickser.password),
      updateLastLoginAndGenerateToken(tickser, req.local.geoip)
    ]).spread<Token>(function (isCompare: boolean, token: Token) {
      return token;
    });
  });
}

export function google(idToken, referrerId, req: Ticksa.Request): Promise<Token> {
  return new Promise<Token>(function (resolve, reject) {
    request.get('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + idToken, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(parseGoogleData(body, req, referrerId));
      }
    });
  });
}

function parseGoogleData(body, req: Ticksa.Request, referrerId): Promise<Token> {
  var googleData = JSON.parse(body);
  return TickserSocial.Model.findOne({
    where: {
      socialId: googleData.sub,
      type: TickserSocial.Type.GOOGLE
    },
    include: [
      {
        required: true,
        model: Tickser.Model,
        as: 'tickser'
      }
    ]
  })
    .then(function (tickserSocial: TickserSocial.Instance) {
      if (tickserSocial != null) {
        return generateTokenByTickserSocial(tickserSocial, req.local);
      }

      return new Promise<Token>(function (resolve, reject) {
        const imgUrl = 'https://www.googleapis.com/plus/v1/people/' + googleData.sub +
          '?fields=image' +
          '&key=' + config.env().security.google.publicKey;

        request.get(imgUrl, function (error, response, body) {
          if (error) {
            return reject(error);
          }

          let socialImageUrl = null;
          if (response.statusCode == 200) {
            const imageData = JSON.parse(body);
            socialImageUrl = imageData.image.url.split('?')[0];
          }

          resolve(generateUserSocialForGoogle(googleData, socialImageUrl, referrerId, req.local));
        });
      });
    });
}

function generateUserSocialForGoogle(googleData, socialImageUrl, referrerId, local: Ticksa.IRequestLocalData): Promise<Token> {
  return conn.transaction(function (t) {
    return Tickser.Model.scope([Tickser.Scopes.IS_ACTIVE_OR_CONFIRMED]).findOneByUsername(googleData.email)
      .then(function (tickser) {
        if (tickser == null) {
          const params = RegistrationService.SocialUserGenerationParams.forGoogle(
            googleData, socialImageUrl, referrerId, local
          );
          return RegistrationService.generateUserSocial(params, t);
        }

        return RegistrationService.generateSocial(tickser.id, TickserSocial.Type.GOOGLE, googleData.sub, t)
          .then(() => tickser);
      });
  })
    .then(function (tickser: Tickser.Instance) {
      return updateLastLoginAndGenerateToken(tickser, local.geoip);
    });
}

export function facebook(signedRequest: string, accessToken: string, referrerId, req: Ticksa.Request): Promise<any> {
  return Promise.resolve(fb.parseSignedRequest(signedRequest, config.env().security.facebook.secret))
    .then(function (data: any) {
      if (!data) {
        throw new Errors.Unauthorized();
      }

      var socialId = data.user_id;
      return TickserSocial.Model.scope(TickserSocial.Scopes.INCLUDE_TICKSER).findOne(
        {
          where: {
            socialId: socialId,
            type: TickserSocial.Type.FACEBOOK
          }
        }
      );
    })
    .then(function (tickserSocial: TickserSocial.Instance) {
      if (tickserSocial) {
        return generateTokenByTickserSocial(tickserSocial, req.local);
      }

      return new Promise(function (resolve, reject) {
        fb.setAccessToken(accessToken);
        fb.api('/me', {fields: ['id', 'name', 'email', 'picture']}, function (response) {
          if (response.error) {
            reject(new Errors.Unauthorized());
          }

          resolve(generateUserSocialForFacebook(response, referrerId, req.local));
        });
      })
    });
}

export function generateUserSocialForFacebook(facebookMeResponse: IFacebookMeResponse, referrerId: number, local: Ticksa.IRequestLocalData): Promise<any> {
  return Promise.try(function () {
    return facebookMeResponse.picture.data.is_silhouette ? null : facebookMeResponse.picture.data.url;
  })
    .then(function (socialImageUrl: string) {
      return conn.transaction(function (t) {
        return Tickser.Model.scope([Tickser.Scopes.IS_ACTIVE_OR_CONFIRMED]).findOneByUsername(facebookMeResponse.email)
          .then(function (tickser) {
            if (tickser == null) {
              const params = RegistrationService.SocialUserGenerationParams.forFacebook(
                facebookMeResponse, socialImageUrl, referrerId, local
              );

              return RegistrationService.generateUserSocial(params, t);
            }

            return RegistrationService.generateSocial(tickser.id, TickserSocial.Type.FACEBOOK, facebookMeResponse.id, t)
              .then(() => tickser);
          })
      });
    })
    .then(function (tickser: Tickser.Instance) {
      return updateLastLoginAndGenerateToken(tickser, local.geoip);
    });
}

export function facebookOAuth(code: string, redirectUri: string): Promise<IFacebookMeResponse> {
  return new Promise<IFacebookMeResponse>((resolve, reject) => {
    fb.api('oauth/access_token', {
      client_id: config.env().security.facebook.id,
      client_secret: config.env().security.facebook.secret,
      code: code,
      redirect_uri: redirectUri
    }, function (accessTokenResponse: any) {

      if (!accessTokenResponse || accessTokenResponse.error) {
        return reject({cmd: 'access_token', response: accessTokenResponse});
      }

      fb.setAccessToken(accessTokenResponse.access_token);
      fb.api('/me', {
        fields: ['id', 'name', 'email', 'picture']
      }, function (meResponse: IFacebookMeResponse) {
        if (!meResponse || meResponse.error) {
          return reject({cmd: 'me', response: meResponse});
        }
        return resolve(meResponse);
      });
    });
  });
}

export interface Token {
  token: string
}

export function generateTokenByTickserSocial(tickserSocial: TickserSocial.Instance, local: Ticksa.IRequestLocalData): Promise<Token> {
  if (tickserSocial.tickser.checkIfActive()) {
    return updateLastLoginAndGenerateToken(tickserSocial.tickser, local.geoip);
  }

  if (tickserSocial.tickser.checkIfSuspended()) {
    throw new Errors.SuspendedUser();
  }

  throw new Errors.Unauthorized();
}

function updateLastLoginAndGenerateToken(tickser: Tickser.Instance, geoip: Ticksa.IGeoipData): Promise<Token> {
  return Promise.all([
    Tickser.Model.setLastLogin(tickser.id),
    TickserService.setLoginIpAndCountryById(tickser.id, geoip)
  ]).spread<Token>(function (): Token {
    const newToken = SecurityService.generateToken(tickser.id, tickser.username);
    return {token: newToken};
  }).catch(function (err) {
    logger.warn(err);
    throw new Errors.Unauthorized();
  })
}

export function findTickserByToken(token: string): Promise<Tickser.Instance> {
  return SecurityService.verifyToken(token).then(function (decoded: SecurityService.TokenData) {
    decoded.username = decoded.username.toLowerCase();
    return Tickser.Model.scope(Tickser.Scopes.INCLUDE_ACCOUNT).findOneByUsername(decoded.username);
  })
}
