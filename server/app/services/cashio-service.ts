/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import Promise = require('bluebird')
import q = require('q');
import express = require('express');
import _ = require('lodash');
import moment = require('moment');
import ticksa = require('ticksa');
var conn = require('../connection');
import config = require('../config');

import Errors = require('../errors');

import LemonwayService = require('./lemonway-service');
import LemonwayCashInService = require('./lemonway-cashin-service');
import MailService = require('./mail-service');
import TicksTransactionService = require('./ticks-transaction-service');

import Models = require('../models/models');
import Account = Models.Account;
import Tickser = Models.Tickser;
import CashIO = Models.CashIO;
import LemonwayCashIn = Models.LemonwayCashIn;
import LemonwayWallet = Models.LemonwayWallet;
import Country = Models.Country;
import ExchangeRate = Models.ExchangeRate;
import InfinCashIn = Models.InfinCashIn;
import InfinCallback = Models.InfinCallback;
import Currency = Models.Currency;
import EmailRecipient = Models.EmailRecipient;


export function startInfinTransaction(moneyAmount: number, countryCode: string, tickser: Tickser.Instance): Promise<number> {
  return conn.transaction(function (t) {

    return Account.Model.scope(['isNormal']).findOne({
      where: {
        tickserId: tickser.id
      },
      transaction: t
    }).then(function (account) {
      if (tickser.countryId == null) {
        return Promise.reject({status: 400, message: 'Tickser has no country selected'});
      }

      var cashIO: CashIO.Attributes = {
        accountId: account.id,
        tickserId: tickser.id,
        userCurrencyAmount: moneyAmount,
        tickserEmail: tickser.email,
        phoneCountryCode: countryCode.toLowerCase(),
        countryId: tickser.countryId,
        status: CashIO.Status.PENDING,
        type: CashIO.Type.CASH_IN,
        paymentType: CashIO.PaymentType.INFIN
      };

      if (tickser.phone) {
        cashIO.tickserPhoneNumber = tickser.phone;
      }

      return CashIO.Model.create(cashIO, {
        transaction: t
      }).then(function (createdCashIO: CashIO.Instance) {
        return createdCashIO.id;
      });
    });
  })
}


export function earnedToPurchasedTicksCashOut(ticks, user): any {
  return conn.transaction(function (t) {
    var defTransaction = q.defer();
    if (user.countryId == null) {
      logger.error('Earned to purchased ticks error. User has does not have country. ID ' + user.id);
      defTransaction.reject('User does not country');
      return defTransaction.promise;
    }
    q.spread([Account.Model.scope(['isNormal']).findOne({
      where: {
        tickserId: user.id
      },
      transaction: t
    })], function (account: any) {
      if (account.currentBalanceEarnedTicks < ticks) {
        defTransaction.reject('Not enough earned ticks in account');
        return defTransaction.promise;
      }
      account.currentBalanceEarnedTicks -= ticks;
      var brutto = computeEurFromTicks(ticks);
      var cashOut: CashIO.Attributes = {
        receivedAmount: brutto,
        countryId: user.countryId,
        accountId: account.id,
        tickserId: user.id,
        status: CashIO.Status.COMPLETED,
        type: CashIO.Type.CASH_OUT
      };
      q.spread([account.save({transaction: t}), CashIO.Model.create(cashOut, {transaction: t})], function (updatedAccount, createdCashIo) {
        defTransaction.resolve(createdCashIo);
      }, function (err) {
        defTransaction.reject(err);
      })
    }).catch(function (err) {
      defTransaction.reject(err);
    });
    return defTransaction.promise;
  })
}

export function earnedToPurchasedTicksCashIn(cashIoId: number): Promise<CashIO.Instance> {

  return conn.transaction(function (t) {

    return CashIO.Model.scope(['account', 'country']).findById(cashIoId)
      .then(function (cashOut: CashIO.Instance) {
        var vat = 0;
        var netto = cashOut.receivedAmount;
        cashOut.account.currentBalance += computeTicksFromEur(netto);
        var cashIn: CashIO.Attributes = {
          receivedAmount: cashOut.receivedAmount,
          VATAmount: vat,
          NETAmount: netto,
          countryId: cashOut.country.id,
          accountId: cashOut.account.id,
          tickserId: cashOut.tickserId,
          status: CashIO.Status.COMPLETED,
          type: CashIO.Type.CASH_OUT
        };


        var saveAccount = cashOut.account.save({transaction: t});
        var createCashIo = CashIO.Model.create(cashIn, {transaction: t});

        return Promise.all([saveAccount, createCashIo]).spread(function (updatedAccount, createdCashIo) {
          return createdCashIo;
        })
      });

  });
}

export function startPayPalTransaction(moneyAmount, currencyId, user) {
  var defTransaction = q.defer();
  return conn.transaction(function (t) {
    if (user.countryId == null) {
      defTransaction.reject({status: 400, message: 'Tickser has no country selected'});
      return defTransaction.promise;
    }


    q.spread([
        Currency.Model.findOne({
          where: {
            id: currencyId
          },
          transaction: t
        }),
        Account.Model.scope(['isNormal']).findOne({
          where: {
            tickserId: user.id
          },
          transaction: t
        })
      ],
      function (currency: any, account: any) {

        var cashIO = {
          accountId: account.id,
          tickserId: user.id,
          userCurrencyId: currency.id,
          userCurrencyAmount: moneyAmount,
          countryId: user.countryId,
          tickserEmail: user.email,
          status: CashIO.Status.PENDING,
          type: CashIO.Type.CASH_IN,
          paymentType: CashIO.PaymentType.PAYPAL
        };

        CashIO.Model.create(cashIO, {
          transaction: t
        }).then(function (createdCashIO) {
          defTransaction.resolve(createdCashIO.id)
        }, function (err) {
          defTransaction.reject(err);
        });
      }, function (err) {
        defTransaction.reject(err);
      });

    return defTransaction.promise;
  })
}

export function finalizeCashIO(cashIOId: number): Promise<CashIO.Instance> {
  logger.info('Transaction finalize called for cashIO ID: ' + cashIOId);

  return <Promise<CashIO.Instance>> conn.transaction(function (t) {
    return CashIO.Model.scope(['feesCurrency', 'userCurrency', 'country', 'tickser']).findOne({
      where: {
        id: cashIOId
      },
      transaction: t
    }).then(function (cashIO: CashIO.Instance) {

      if (!cashIO) {
        return Promise.reject('No CashIO with id ' + cashIOId);
      }

      if (cashIO.status !== CashIO.Status.COMPLETED) {
        return Promise.reject('CashIO transaction not COMPLETED ID: ' + cashIO.id);
      }

      if (cashIO.feesCurrency && cashIO.feesCurrency.iso4217Code.toUpperCase() != 'EUR') {
        return Promise.reject('Fees must be in EUR. CashIO ID: ' + cashIO.id);
      }

      return updateAccountAndCashIO(cashIO, t)
    }).then(function (result: UpdateAccountAndCashIOResult) {
      activateTickserIfRequired(result.account.tickserId);
      sendMailAfterFinalize(result);
      logger.info('Finalized CashIO transaction. ID: ' + result.cashIO.id);
      return result.cashIO;
    });
  }).catch(function (err) {
    setCashIOError(err, cashIOId);
    logger.error('Error finalizing CashIO transaction. ID: ' + cashIOId, err);
    return Promise.reject(err);
  });
}

function sendMailAfterFinalize(result: UpdateAccountAndCashIOResult) {
  //TODO: Get data from cashIo

  var mailParams = {
    user_name: result.cashIO.tickser.name,
    received_amount: result.cashIO.userCurrencyAmount + ' ' + result.cashIO.userCurrency.iso4217Code,
    fees_amount: result.cashIO.feesAmount ? result.cashIO.feesAmount.toFixed(2) : 0,
    net_amount: result.cashIO.NETAmount ? result.cashIO.NETAmount.toFixed(2) : 0,
    ticks: result.cashIO.receivedTicks,
    transaction_id: result.cashIO.id,
    date: moment(result.account.balanceUpdated).format('YYYY-MM-DD HH:mm:ss'),
  };
  MailService.sendMailToTickser(result.cashIO.tickser, EmailRecipient.Template.CASH_IN_SUCCESSFUL, mailParams);
}

export function activateTickserIfRequired(tickserId) {
  Tickser.Model.findOne({
    where: {
      id: tickserId
    }
  }).then(function (tickser) {
    if (tickser.status == Tickser.Status.CONFIRMED) {
      tickser.status = Tickser.Status.ACTIVE;
      tickser.save();
    }
  }).catch(function (err) {
    logger.error(err);
  });
}

interface UpdateAccountAndCashIOResult {
  account: any,
  cashIO: CashIO.Instance,
}

function updateAccountAndCashIO(cashIO: CashIO.Instance, t): Promise<UpdateAccountAndCashIOResult> {
  return Promise.try<any>(function () {
    var getAccount = Account.Model.scope(['isNormal']).findOne({
      where: {
        id: cashIO.accountId
      },
      transaction: t
    });

    var getExchangeResult = resolveAmountToEur(cashIO.userCurrencyAmount, cashIO.userCurrency.id);

    return Promise.all([getAccount, getExchangeResult]).spread(function (account: any, exchangeResult: ExchangeResult) {
      var receivedAmount = exchangeResult.amount;
      var feesAmount = cashIO.feesAmount || 0;
      var netAmount = receivedAmount - feesAmount;

      cashIO.VATAmount = null;
      cashIO.NETAmount = netAmount;
      cashIO.receivedAmount = receivedAmount;
      cashIO.exchangeRateToEuro = exchangeResult.rate;
      cashIO.receivedTicks = computeTicksFromEur(netAmount);

      account.currentBalance += cashIO.receivedTicks;
      account.balanceUpdated = new Date();
      cashIO.status = CashIO.Status.FINAL;

      return Promise.join(
        account.save({transaction: t}),
        cashIO.save({transaction: t}),
        function (updatedAccount: any, updatedCashIO: CashIO.Instance) {
          var result: UpdateAccountAndCashIOResult = {
            account: updatedAccount,
            cashIO: updatedCashIO,
          };
          return result
        })
    });
  });
}

export interface ExchangeResult {
  amount: number;
  rate: number;
}

export function resolveAmountToEur(amount: number, currencyId: number, t?: any): Promise<ExchangeResult> {
  var findCurrency = Currency.Model.findOne({
    where: {
      id: currencyId
    },
    transaction: t
  });
  var findEuro = Currency.Model.scope(['EUR']).findOne({
    transaction: t
  });

  return Promise.all([findCurrency, findEuro])
    .spread<ExchangeResult>(function (userCurrency: Currency.Instance, euroCurrency: Currency.Instance) {
      if (userCurrency == null || euroCurrency == null) {
        return Promise.reject('Currency not found id:' + currencyId);
      }

      if (userCurrency.id == euroCurrency.id) {
        return Promise.resolve({amount: amount, rate: null});
      }

      return ExchangeRate.Model.scope(['valid']).findOne({
        where: {
          currencyOneId: userCurrency.id,
          currencyTwoId: euroCurrency.id
        },
        transaction: t
      }).then(function (exchangeRate: ExchangeRate.Instance) {
        let rate = exchangeRate ? exchangeRate.exchangeRate : 1;
        return Promise.resolve({amount: amount * rate, rate: rate});
      });
    });
}
export function setCashIOError(error, cashIOId) {
  return CashIO.Model.update(
    {
      error: JSON.stringify(error)
    }, {
      where: {
        id: cashIOId
      }
    });
}


export function computeTicksFromEur(euros) {
  return euros * config.global.euroToTicksRate;
}

export function computeEurFromTicks(ticks) {
  return ticks * 0.01;
}

export interface LwTransactionResult {
  withCard?: boolean;
  cardForm?: string;
  transactionId?: number;
}

export interface LemonwayTransactionCardParams {
  moneyAmount: number;
  cardId: string;
}

export function startLemonwayTransactionWithCardId(params: LemonwayTransactionCardParams, req: ticksa.Request): Promise<LwTransactionResult> {
  var lemonwayService = LemonwayService.request(req);
  return lemonwayService.moneyInWithCardId(params.moneyAmount, params.cardId, req.local.user.account.id)
    .then(function (result: LemonwayService.MoneyInWithCardIdResult) {

      var createLwCashInParams: LemonwayCashInService.CreateLwCashInParams = {
        user: req.local.user,
        moneyAmount: params.moneyAmount,
        initRequest: JSON.stringify({MoneyInWithCardId: result.moneyInRequest}),
        initResponse: JSON.stringify({MoneyInWithCardId: result.moneyInResponse})
      };

      if (!result.success) {
        return LemonwayCashInService.createLwCashIn(createLwCashInParams, LemonwayCashIn.Type.WITH_CARD, LemonwayCashIn.Status.FAILED);
      }

      return LemonwayCashInService.createLwCashIn(createLwCashInParams, LemonwayCashIn.Type.WITH_CARD, LemonwayCashIn.Status.COMPLETED);
    }).then(function (lwCashIn: LemonwayCashIn.Instance) {
      if (lwCashIn.status != LemonwayCashIn.Status.COMPLETED) {
        return Promise.reject('CashIO transaction not COMPLETED ID: ' + lwCashIn.cashIoId);
      }
      return finalizeCashIO(lwCashIn.cashIoId);
    }).then(function (cashIO: CashIO.Instance) {
      var transResult: LwTransactionResult = {
        withCard: true,
        transactionId: cashIO.id,
      };
      return transResult;
    });
}

export interface StartLwTransactionParams {
  firstName: string;
  lastName: string;
  moneyAmount: number;
}

export function startLemonwayTransaction(params: StartLwTransactionParams, req: ticksa.Request): Promise<LwTransactionResult> {
  var lemonwayCashIn: LemonwayCashIn.Instance;
  var wallet: LemonwayWallet.Instance;
  var moneyInResult: LemonwayService.MoneyInWebInitAndGetFormResult = {};

  var lemonwayService = LemonwayService.request(req);

  var createLwCashInParams: LemonwayCashInService.CreateLwCashInParams = {
    user: req.local.user,
    moneyAmount: params.moneyAmount,
  };

  var createCashIn = LemonwayCashInService.createLwCashIn(createLwCashInParams, LemonwayCashIn.Type.WITHOUT_CARD);
  var getWalletOrRegister = lemonwayService.getWalletOrRegister(params.firstName, params.lastName, req.local.user.email, req.local.user.account.id);

  return createCashIn.then(function (createdLwCashIn: LemonwayCashIn.Instance) {
    lemonwayCashIn = createdLwCashIn;
    return getWalletOrRegister;
  }).then(function (createdWallet: LemonwayWallet.Instance) {
    wallet = createdWallet;

    if (wallet.status == LemonwayWallet.Status.ERROR) {
      lemonwayCashIn.initRequest = JSON.stringify(wallet.initRequest);
      lemonwayCashIn.initResponse = JSON.stringify(wallet.initResponse);
      lemonwayCashIn.status = LemonwayCashIn.Status.FAILED;
      return lemonwayCashIn.save();
    }

    return lemonwayService.moneyInWebInitAndGetForm(wallet.walletId, params.moneyAmount, lemonwayCashIn.wkToken)
      .then(function (result: LemonwayService.MoneyInWebInitAndGetFormResult) {
        moneyInResult = result;

        lemonwayCashIn.initRequest = JSON.stringify(moneyInResult.request);
        lemonwayCashIn.initResponse = JSON.stringify(moneyInResult.response);

        if (!moneyInResult.success) {
          lemonwayCashIn.status = LemonwayCashIn.Status.FAILED;
        }

        return lemonwayCashIn.save();
      });
  }).then(function (updatedLwCashIn: LemonwayCashIn.Instance) {
    if (updatedLwCashIn.status == LemonwayCashIn.Status.FAILED) {
      throw new Errors.ServerError('Lemonway transaction failed');
    }

    var transResult: LwTransactionResult = {
      withCard: false,
      cardForm: moneyInResult.cardForm,
    };
    return transResult;
  });
}

export function findCashIoByIdAndAccount(id: number, req: ticksa.Request): Promise<CashIO.Instance> {
  return CashIO.Model.scope('isCashIn').find({
    where: {
      id: id,
      tickserId: req.local.user.id,
      accountId: req.local.user.account.id,
    }
  }).then(function (cashIO: CashIO.Instance) {
    if (!cashIO) {
      throw new Errors.NotFound('Transaction not found');
    }

    return cashIO;
  });
}