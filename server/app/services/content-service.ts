/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import * as moment from 'moment';
import Moment = moment.Moment;
import Duration = moment.Duration;
const nl2br = require('nl2br');

import * as util from 'util';
import * as _ from 'lodash';
import * as jade from 'jade';
import * as Promise from 'bluebird';
import * as sequelize from 'sequelize';
import * as Ticksa from 'ticksa';

import * as Cache from './redis-cache';
import * as Errors from '../errors';
import * as conn from '../connection';
import * as config from '../config';
import {
  Country,
  TicksTransaction,
  TickserReferral,
  Channel,
  ContentCondition,
  Content,
  TickserCondition
} from '../models/models';

import * as CountryService from '../services/country-service';
import * as TicksTransactionsService from '../services/ticks-transaction-service';
import * as ContentAccessService from '../services/content-access-service';
import * as ConditionService from '../services/condition-service';
import * as AwsService from '../services/aws';
import * as UtilitiesService from '../services/utilities';
import * as I18nService from './i18n-service';
import * as ContentMapper from '../mappers/content-mapper';
import {ContentFilter} from '../utils/request-utils';

/*
 * Routines to construct caching keys.
 * Available symbols for keys: http://redis.io/topics/data-types-intro#redis-keys
 */
class RedisCacheKey {

  // current version of cache - it is for versioning
  private static CACHE_VERSION = 'v1';

  private static KEY_ALL_CONTENTS_SEARCH: string = 'contents';

  public static getKeyAllContentsSearch(pageable: Ticksa.IPageable<ContentFilter>, countryCode: string, tickserConditionIds: number[]): string {
    return this.CACHE_VERSION + ':'
      + this.KEY_ALL_CONTENTS_SEARCH + ':'
      + pageable.limit + '_'
      + pageable.offset + '_'
      + pageable.searchString + '_'
      + countryCode + '_'
      + pageable.filter.categories.sort().join('-') + '_'
      + pageable.filter.languages.sort().join('-') + '_'
      + tickserConditionIds.sort().join('-');
  }
}

export interface UserMeta {
  tickserId: number,
  countryA2Code: string
}

/**
 * sort content ids = by position_in_discover  +  5 last contents  +  random_position
 * this done to show the first 5 most recent contents and correct pagination
 * please remove this shit :)
 *
 * @param pageable Pagination configuration.
 * @param userMeta Additional user metadata.
 * @returns {Promise<number[]>}
 */
export async function findContentIds(pageable: Ticksa.IPageable<ContentFilter>, userMeta: UserMeta): Promise<number[]> {
  const tickserId = userMeta.tickserId;
  const userCountryA2Code = userMeta.countryA2Code;

  const unmetContentIds: number[] = [];
  if (!_.isUndefined(tickserId)) {
    const conditions = await ContentCondition.Model.findUnmetForTickser(userMeta.tickserId);
    _.forEach(conditions, (c: ContentCondition.Instance) => unmetContentIds.push(c.contentId));
  }

  const needToSearch: boolean = pageable.searchString && pageable.searchString.length > 0;
  const replacements = {
    limit: pageable.limit,
    offset: pageable.offset,
    limitByCreated: 5,
    status: Content.Status.ACTIVE,
    published: true,
    expiryDate: new Date(),
    search: needToSearch ? '%' + pageable.searchString + '%' : null,
    categories: pageable.filter.categories,
    languages: pageable.filter.languages,
    unmetContentIds: unmetContentIds,
    geographicCountries: userCountryA2Code == null ? null : [Country.countryCodeToUpperCase(userCountryA2Code)],

    channelStatus: Channel.Status.ACTIVE,
    channelType: Channel.Type.PUBLIC
  };

  let where = '';
  if (needToSearch) {
    const title = 'cont.title ILIKE :search';
    const description = 'cont.description ILIKE :search';
    const teaser = 'cont.teaser_text ILIKE :search';
    const channelName = 'ch.name ILIKE :search';

    const or = [title, description, teaser, channelName];
    where = ' AND (' + or.join(' OR ') + ') ';
  }

  if (pageable.filter.categories && pageable.filter.categories.length > 0) {
    where = where + ' AND cont.category_id IN (:categories)';
  }

  if (pageable.filter.languages && pageable.filter.languages.length > 0) {
    where = where + ' AND cont.language_id IN (:languages)';
  }

  if (_.isUndefined(tickserId)) {
    where = where + ' AND (SELECT count(*) FROM "ContentCondition" as cc WHERE cc.content_id = cont.id) = 0';
  } else {
    if (!_.isEmpty(unmetContentIds)) {
      // :unmetContentIds should not be an empty array because sql syntax "field NOT IN ()" is invalid
      where = where + ' AND cont.id NOT IN (:unmetContentIds)';
    }
  }

  if (userCountryA2Code != null) {
    // here we have to explicitly cast to array type. this is how psql works in this specific case
    where = where + ' AND (cont.geographic_countries @> ARRAY [:geographicCountries] :: VARCHAR(2) [] OR cont.geographic_countries IS NULL) ';
  }

  // some details
  // 1) in by_position_in_discover and by_created_at '100000 as random' is used to let all contents from them be next
  //    to any content from by_random_position (in final ordering)
  // 2) in by_created_at and by_random_position '100000 as position_in_discover' is used to let all contents from them
  //    be next to any content from by_position_in_discover (in final ordering)
  // 3) in by_random_position '100 years' is used to let all contents from by_random_position be "earlier" than any
  //    content from by_created_at (in final ordering)
  const query = 'with ' +
    'by_position_in_discover as (' +
    '   SELECT cont.id as content_id, cont.position_in_discover, cont.created_at as created, 100000 as random' +
    '   FROM "Content" as cont ' +
    '   INNER JOIN "Channel" AS ch ON cont.channel_id = ch.id AND ch.status = :channelStatus AND ch.type = :channelType' +
    '   WHERE cont.position_in_discover NOTNULL AND cont.status = :status ' +
    '   AND cont.published = :published AND cont.expiry_date >= :expiryDate ' + where +
    '   ORDER BY cont.position_in_discover), ' +
    'by_created_at as (' +
    '   SELECT cont.id as content_id, 100000 as position_in_discover, cont.created_at as created, 100000 as random' +
    '   FROM "Content" as cont' +
    '   INNER JOIN "Channel" AS ch ON cont.channel_id = ch.id AND ch.status = :channelStatus AND ch.type = :channelType' +
    '   WHERE cont.position_in_discover ISNULL AND cont.status = :status AND cont.published = :published' +
    '   AND cont.expiry_date >= :expiryDate ' + where +
    '   ORDER BY cont.created_at DESC limit :limitByCreated),' +
    'by_random_position as (' +
    '   SELECT cont.id as content_id, 100000 as position_in_discover, NOW() - INTERVAL \'100 years\' as created,  cont.random_position as random' +
    '   FROM "Content" as cont' +
    '   INNER JOIN "Channel" AS ch ON cont.channel_id = ch.id AND ch.status = :channelStatus AND ch.type = :channelType' +
    '   WHERE cont.position_in_discover ISNULL AND  cont.status = :status AND cont.published = :published ' +
    '   AND cont.expiry_date >= :expiryDate ' + where +
    '   AND cont.id not in (select content_id from by_created_at)' +
    '   ORDER by cont.random_position), ' +
    'all_contents as (' +
    '   SELECT content_id, position_in_discover, created, random FROM by_position_in_discover ' +
    '   UNION SELECT content_id, position_in_discover, created, random FROM by_created_at ' +
    '   UNION SELECT content_id, position_in_discover, created, random FROM by_random_position) ' +

    'SELECT content_id from all_contents ' +
    'ORDER by position_in_discover, created DESC, random, content_id ' +
    'OFFSET :offset ' +
    'LIMIT :limit; ';

  const start: Moment = moment();
  const result: {content_id: number}[] = await conn.query(query, {
    replacements: replacements,
    type: sequelize.QueryTypes.SELECT
  });
  const stop: Moment = moment();
  const duration: Duration = moment.duration(stop.diff(start));
  logger.info('findContentIds'
    + '. limit: ' + pageable.limit
    + '. offset: ' + pageable.offset
    + '. searchString: ' + pageable.searchString
    + '. languages: [' + pageable.filter.languages.join(',') + ']'
    + '. categories: [' + pageable.filter.categories.join(',') + ']'
    + '. country: ' + userCountryA2Code
    + '. conditions: [' + unmetContentIds.join(',') + ']'
    + '. start: ' + start.format()
    + '. stop: ' + stop.format()
    + '. duration: ' + duration.asSeconds()
  );

  return _.pluck(result, 'content_id');
}

function findContentFromDB(pageable: Ticksa.IPageable<ContentFilter>, userMeta: UserMeta): Promise<Content.Instance[]> {
  return findContentIds(pageable, userMeta)
    .then(function (ids: number[]) {
      if (!ids || ids.length == 0) {
        return [];
      }
      const scopes: any = Content.Scopes.INCLUDE_COMMON_DATA.concat([
        {method: [Content.Scopes.INCLUDE_REQUIRED_PUBLIC_ACTIVE_CHANNEL, null]}
      ]);

      return Content.Model.scope(scopes).findAllByIds(ids)
        .then(function (contents: Content.Instance[]) {
          const orderByIds = {};
          _.forEach<any>(ids, function (id: number) {
            orderByIds[id] = ids.indexOf(id);
          });

          return _.sortBy<Content.Instance, number>(contents, function (content: Content.Instance) {
            return orderByIds[content.id];
          });
        });
    });

}

export async function getAllContentsPagingAndSearch(pageable: Ticksa.IPageable<ContentFilter>, userMeta: UserMeta): Promise<Content.Instance[]> {

  if (config.isLocal() || config.isTest()) {
    return await findContentFromDB(pageable, userMeta);
  }

  let tickserConditionIds: number[] = [];
  if (userMeta.tickserId != null) {
    const conditions = await TickserCondition.Model.findConditionsForTickser(userMeta.tickserId);
    tickserConditionIds = _.pluck(conditions, 'conditionId');
  }

  const key: string = RedisCacheKey.getKeyAllContentsSearch(pageable, userMeta.countryA2Code, tickserConditionIds);

  let result;
  try {
    result = await Cache.get(key);
  } catch (e) {
    result = await findContentFromDB(pageable, userMeta);
    Cache.setSequelizeData(key, result, 10);
  }

  return result;
}

export function findFullContentById(contentId: number): Promise<Content.Instance> {
  const scopes: any = [
    Content.Scopes.INCLUDE_LANGUAGE,
    Content.Scopes.INCLUDE_CATEGORY,
    Content.Scopes.INCLUDE_TEASER_IMAGE,
    Content.Scopes.INCLUDE_TEASER_VIDEO,
    Content.Scopes.INCLUDE_TAGS,
    Content.Scopes.INCLUDE_TICKSER,
    Content.Scopes.INCLUDE_CONTENT_DATA_TEXT,
    Content.Scopes.INCLUDE_CONTENT_DATA_LINK,
    Content.Scopes.INCLUDE_CONTENT_DATA_VIDEO,
    Content.Scopes.INCLUDE_CONTENT_DATA_IMAGES,
    Content.Scopes.INCLUDE_CONDITIONS,
    Content.Scopes.INCLUDE_REQUIRED_ACTIVE_CHANNEL
  ];

  return Content.Model.scope(scopes).findById(contentId);
}

export async function findContentsByChannel(channelId: number, pageable: Ticksa.IPageable<any>, userMeta: UserMeta): Promise<Content.Instance[]> {
  const conditions = await ContentCondition.Model.findUnmetForTickser(userMeta.tickserId);
  const unmetIds: number[] = _.map(conditions, (c: ContentCondition.Instance) => c.contentId);

  const andOptions: sequelize.WhereOptions[] = [];
  if (!_.isEmpty(unmetIds)) {
    andOptions.push({
      id: {$notIn: unmetIds}
    });
  }

  if (!_.isEmpty(userMeta.countryA2Code)) {
    const codeInCorrectCase = Country.countryCodeToUpperCase(userMeta.countryA2Code);
    andOptions.push({
      geographicCountries: {
        $or: {
          $contains: [codeInCorrectCase],
          $eq: null
        }
      }
    });
  }

  const scopes: any = Content.Scopes.INCLUDE_COMMON_DATA.concat([
    Content.Scopes.IS_PUBLISHED,
    Content.Scopes.IS_ACTIVE,
    Content.Scopes.IS_NOT_EXPIRED,
    {method: [Content.Scopes.INCLUDE_REQUIRED_PUBLIC_ACTIVE_CHANNEL, channelId]}
  ]);
  const contents: Content.Instance[] = await Content.Model.scope(scopes).findAll({
    where: {
      $and: andOptions
    },
    limit: pageable.limit,
    offset: pageable.offset,
    order: [
      ['created_at', 'DESC']
    ]
  });

  if (!contents || contents.length == 0) {
    return [];
  }

  const sortedContent = contents[0].channel.sortedContent;
  return sortContentBySortedIds(contents, sortedContent);
}

export async function findContentForVoteboard(channelId: number, pageable: Ticksa.IPageable<any>): Promise<ContentMapper.IVoteboardContent[]> {
  const scopes = [Content.Scopes.IS_ACTIVE, Content.Scopes.IS_NOT_EXPIRED, Content.Scopes.IS_PUBLISHED,
    Content.Scopes.INCLUDE_TEASER_IMAGE];
  const contents: Content.Instance[] = await Content.Model.scope(scopes).findAll({
    limit: pageable.limit,
    offset: pageable.offset,
    order: [
      ['likes', 'DESC']
    ],
    where: {
      channelId: channelId,
      likes: {
        $gt: 0
      }
    }
  });

  return _.map(contents, (c: Content.Instance) => {
    return ContentMapper.mapToVoteboardContent(c);
  });
}

export function findFullContentByIdAndChannelId(contentId: number, channelId: number): Promise<Content.Instance> {
  const scopes: any = Content.Scopes.INCLUDE_COMMON_DATA.concat([
    Content.Scopes.IS_ACTIVE,
    Content.Scopes.IS_NOT_EXPIRED,
    Content.Scopes.IS_PUBLISHED,
    {method: [Content.Scopes.INCLUDE_REQUIRED_PUBLIC_ACTIVE_CHANNEL, channelId]}
  ]);
  return Content.Model.scope(scopes).findById(contentId);
}

export function findNotFullContentByIdForSharing(contentId: number): Promise<Content.Instance> {
  const scopes: any = [
    Content.Scopes.IS_ACTIVE,
    Content.Scopes.IS_NOT_EXPIRED,
    Content.Scopes.IS_PUBLISHED,
    Content.Scopes.INCLUDE_TAGS,
    Content.Scopes.INCLUDE_TEASER_IMAGE,
    Content.Scopes.INCLUDE_TEASER_VIDEO,
    {method: [Content.Scopes.INCLUDE_REQUIRED_PUBLIC_ACTIVE_CHANNEL, null]}
  ];

  return Content.Model.scope(scopes).findById(contentId);
}

export async function refundContent(userId: number, accountId: number, contentId: number): Promise<TicksTransaction.Instance> {
  const content: Content.Instance = await Content.Model.findById(contentId);
  if (content == null) {
    throw new Errors.NotFound('Content not found');
  }

  return conn.transaction(async(t) => {
    await ContentAccessService.updateContentAccess(accountId, contentId, false, t);
    const transaction: TicksTransaction.Instance = await TicksTransactionsService.processRefundContentTransaction(contentId, userId, content.channelId, t);
    await ConditionService.removeTickserPurchaseConditions(userId, contentId, t);
    return transaction;
  });
}

export async function payContent(userId: number, accountId: number, contentId: number, geoip: Ticksa.IGeoipData) {

  const content = await Content.Model.scope([Content.Scopes.IS_ACTIVE, Content.Scopes.IS_PUBLISHED]).findOne({
    where: {id: contentId},
  });

  if (content == null) {
    throw new Errors.NotFound('Content not found');
  }

  if (content.tickserId == userId) {
    throw new Errors.BadRequest('User is creator of content');
  }

  if (!content.isValidCountry(geoip.countryA2Code)) {
    throw Errors.ContentForbidden.geographyLimited('Content not available in your country');
  }

  const isSatisfies = await ConditionService.doesUserSatisfyCondition(content.id, userId);
  if (!isSatisfies) {
    throw Errors.ContentForbidden.notAvailable('Content not available');
  }

  return conn.transaction(async function (t) {
    await ContentAccessService.updateContentAccess(accountId, contentId, true, t);

    const referral = await TickserReferral.Model.findOneByTickserId(userId);

    const referralId = !referral || referral.referrerId == content.tickserId || !content.campaign
      ? null
      : referral.referrerId;
    await createTicksTransactionsForPayContent(content, userId, referralId, t);
    await ConditionService.createTickserPurchaseCondition(userId, contentId, t);
  });
}

function createTicksTransactionsForPayContent(content: Content.Instance, userId: number, referrerId: number, t: sequelize.Transaction) {
  let params: TicksTransactionsService.TwoWayTransactionParams = {
    buyerTickserId: userId,
    sellerTickserId: content.tickserId,
    campaignTickserId: referrerId,
    contentId: content.id,
    amount: content.tickPrice,
    type: TicksTransaction.Type.ACCESS,
    t: t,
  };

  return TicksTransactionsService.processTwoWayTransaction(params)
    .then(function (result: TicksTransactionsService.TwoWayTransactionResult) {
      return Promise.resolve({ticks: result.buyerAccount.currentBalance});
    });
}

export function sortContentBySortedIds(contents: Content.Instance[], sortedIds: number[]): Content.Instance[] {
  if (_.isEmpty(sortedIds)) {
    return contents;
  }

  contents = _.sortBy(contents, (content: Content.Instance) => {
    const index = _.indexOf(sortedIds, content.id);
    // If the id is not in `sortedIds`, put it at the end
    // (`sortedIds.length` is greater than any index in the `sortedIds`).
    // Otherwise, return the `index` so the order matches the list.
    return (index === -1) ? sortedIds.length : index;
  });
  return contents;
}

export interface RenderContentCardResult {
  renderedCard: string;
  content: Content.Instance;
}

export function renderContentCard(contentId: number, queryLanguage: string): Promise<RenderContentCardResult> {
  return findFullContentById(contentId).then(function (content: Content.Instance) {
    if (content == null) {
      throw new Errors.NotFound('Content not found');
    }

    let renderOptions = {
      teaserImage: content.teaserImage ? AwsService.getResizeImageUrl(content.teaserImage.file.key, 300) : null,
      teaserVideo: content.teaserVideo ? AwsService.getResizeImageUrl(content.teaserVideo.thumbnailImage.key, 300) : null,
      linkToContent: generateContentShortLink(content.id, content.tickserId),
      channelIsVerified: content.channel.verified,
      channelImage: content.channel.coverImage ? AwsService.getResizeImageUrl(content.channel.coverImage.file.key, 32, 32) : null,
      channelName: content.channel.name,
      contentInfo: getContentInfo(content, queryLanguage),
      contentTitle: content.title,
      contentTeaserText: nl2br(content.teaserText, false),
      buttonText: I18nService.t('CONTENT_VIEW_BTN', queryLanguage) + ' ' + content.tickPrice,
      priceHint: "(=" + calculateTickPriceLabel(content.tickPrice) + ")",
      CDN_STATIC_FOLDER: config.env().aws.s3.cloudFront
    };

    const renderedCard = jade.renderFile(config.getTemplateFilePath('content_card_template.jade'), renderOptions);

    return {
      renderedCard: renderedCard,
      content: content
    }
  });
}

function getContentInfo(content: Content.Instance, language: string) {
  switch (content.type) {
    case Content.Type.TEXT:
      return Math.ceil(content.textLength / 1000) + ' ' + I18nService.t('MINUTE_READ', language);
    case Content.Type.IMAGE:
      return content.imageCount + ' ' + I18nService.t('TEASER_IMAGE_COUNT', language);
    case Content.Type.VIDEO:
      return Math.ceil(content.videoLength / 1000 / 60) + ' ' + I18nService.t('VIDEO_LENGTH', language);
    default:
      return '';
  }
}

export function generateContentShortLink(contentId: number, tickserId: number): string {
  let encodedContentId = UtilitiesService.encodeNumberToRadix64(contentId);
  let encodedTickserId = UtilitiesService.encodeNumberToRadix64(tickserId);

  return config.env().server.url + '/s/' + encodedContentId + '-' + encodedTickserId;
}

export function calculateTickPriceLabel(ticksPrice: number): string {

  let selectLegend = function (ticks) {
    if (ticks == 1) {
      return 'cent';
    }
    else if (ticks <= 50) {
      return 'cents';
    }
    else {
      return '€';
    }
  };

  let selectAmount = function (ticks) {
    let num = Number(ticks);
    if (isNaN(num)) {
      return '0';
    }

    let cents = Math.round(num);
    if (cents <= 50) {
      return cents.toFixed();
    }

    let eur = cents * 0.01;

    if (cents % 100 == 0) {
      return eur.toFixed();
    }
    else if (cents % 10 == 0) {
      return eur.toFixed(1);
    }
    else {
      return eur.toFixed(2);
    }
  };

  return selectAmount(ticksPrice) + ' ' + selectLegend(ticksPrice);
}

// [d.borisov]
// it looks to complicated. this method is a kind of hack
// there are few non-obvious places that exist in order to let query:
//  * content that filtered (ilike) by its text fields OR it's channel name
//  * include teasers (image or video)
//
// IT NEEDS TEST FOR STABILISATION!!!!
//
export async function searchContent(query: string, userMeta: UserMeta): Promise<Content.Instance[]> {
  const conditions = await ContentCondition.Model.findUnmetForTickser(userMeta.tickserId);
  const unmetIds: number[] = _.map(conditions, (c: ContentCondition.Instance) => c.contentId);

  const visibleForUserOptions: sequelize.WhereOptions[] = [];
  if (!_.isEmpty(unmetIds)) {
    visibleForUserOptions.push({
      id: {$notIn: unmetIds}
    });
  }

  if (!_.isEmpty(userMeta.countryA2Code)) {
    const codeInCorrectCase = Country.countryCodeToUpperCase(userMeta.countryA2Code);
    visibleForUserOptions.push({
      geographicCountries: {
        $or: {
          $contains: [codeInCorrectCase],
          $eq: null
        }
      }
    });
  }

  const searchQuery: sequelize.WhereOptions = {
    $or: {
      title: {$iLike: '%' + query + '%'},
      description: {$iLike: '%' + query + '%'},
      teaserText: {$iLike: '%' + query + '%'},
      "$channel.name$": {$iLike: '%' + query + '%'}
    }
  };

  const options: sequelize.FindOptions = {
    where: {
      $and: [
        searchQuery,
        {
          $or: [{
            tickserId: userMeta.tickserId   // TODO [dborisov] here can be a lack when chanell become managed by miltiple ticksers
          }, {
            $and: visibleForUserOptions
          }]
        }
      ]
    },
    // [d.borisov]
    // strange, but it is impossible to use Content.Scopes.INCLUDE_REQUIRED_ACTIVE_CHANNEL
    // it is some kind of hack - i do not know why it works in this way
    include: [
      {
        model: Channel.Model.scope([Channel.Scopes.IS_ACTIVE, Channel.Scopes.IS_PUBLIC]),
        as: 'channel'
      },
    ],
    order: [
      ['created_at', 'DESC']
    ],
    limit: 20
  };

  const scopes = [
    Content.Scopes.IS_PUBLISHED,
    Content.Scopes.IS_ACTIVE,
    Content.Scopes.IS_NOT_EXPIRED,
    Content.Scopes.INCLUDE_TEASER_IMAGE,
    Content.Scopes.INCLUDE_TEASER_VIDEO
  ];

  return Content.Model.scope(scopes).findAll(options);
}

export async function getContentsFilteredByUserAvailability(userMeta: UserMeta, contentIds: number[]): Promise<Content.Instance[]> {
  const conditions = await ContentCondition.Model.findUnmetForTickser(userMeta.tickserId);
  const unmetIds: number[] = _.map(conditions, (c: ContentCondition.Instance) => c.contentId);

  const visibleForUserOptions: sequelize.WhereOptions[] = [];
  if (!_.isEmpty(unmetIds)) {
    visibleForUserOptions.push({
      id: {$notIn: unmetIds}
    });
  }

  if (!_.isEmpty(userMeta.countryA2Code)) {
    const codeInCorrectCase = Country.countryCodeToUpperCase(userMeta.countryA2Code);
    visibleForUserOptions.push({
      geographicCountries: {
        $or: {
          $contains: [codeInCorrectCase],
          $eq: null
        }
      }
    });
  }

  // it constructs a query to search for contents that:
  //  - queried tickser owns them
  //  - or queried tickser satisfies content conditions (CUG and geographic fencing)
  const options: sequelize.FindOptions = {
    where: {
      $and: [
        {
          id: {
            $in: contentIds
          }
        },
        {
          $or: [{
            tickserId: userMeta.tickserId   // TODO [dborisov] here can be a lack when chanell become managed by miltiple ticksers
          }, {
            $and: visibleForUserOptions
          }]
        }
      ]
    }
  };

  const scopes: any = Content.Scopes.INCLUDE_COMMON_DATA.concat([
    Content.Scopes.IS_PUBLISHED,
    Content.Scopes.IS_ACTIVE,
    Content.Scopes.IS_NOT_EXPIRED,
    {method: [Content.Scopes.INCLUDE_REQUIRED_PUBLIC_ACTIVE_CHANNEL, null]}
  ]);

  return Content.Model.scope(scopes).findAll(options);
}

export async function areValidGeographicCountries(countries: string[]): Promise<boolean> {
  if (countries == null) {
    return true;
  }

  if (!util.isArray(countries)) {
    return false;
  }

  if (countries.length == 0) {
    return false;
  }

  const allCountries: Country.Attributes[] = await CountryService.getAllCountries();
  const allCountriesCodes: string[] = _.map(allCountries, c => c.a2);
  const invalidCodes = _.difference(countries, allCountriesCodes);

  return invalidCodes.length == 0;
}