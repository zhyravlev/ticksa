/// <reference path="../typings/tsd.d.ts"/>

import * as Promise from 'bluebird';
import {
  Account,
  TicksAmount
} from '../models/models';

export function updateBalanceAfterRefund(account: Account.Instance, ticksAmount: TicksAmount.Instance): void {
  if (ticksAmount.type === TicksAmount.Type.FREE && ticksAmount.amount < 0) {
    if (ticksAmount.reason == TicksAmount.Reason.REVENUE_SHARE) {
      account.currentBalanceEarnedTicks -= ticksAmount.amount;
    } else {
      account.currentBalanceFreeTicks -= ticksAmount.amount;
    }
    account.totalTicksDebited += ticksAmount.amount;

  } else if (ticksAmount.type === TicksAmount.Type.PAID && ticksAmount.amount < 0) {
    if (ticksAmount.reason == TicksAmount.Reason.REVENUE_SHARE) {
      account.currentBalanceEarnedTicks -= ticksAmount.amount;
    } else {
      account.currentBalance -= ticksAmount.amount;
    }
    account.totalTicksDebited += ticksAmount.amount;

  } else {
    account.currentBalanceEarnedTicks -= ticksAmount.amount;
    account.totalTicksCredited -= ticksAmount.amount;
  }
  account.balanceUpdated = new Date();
}

export function findNormalAccountByTickserId(tickserId): Promise<Account.Instance> {
  return Account.Model.scope(['isNormal']).findOne({
    where: {
      tickserId: tickserId
    },
  });
}