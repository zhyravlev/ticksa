/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import q = require('q');
import _ = require('lodash');
import crypto = require('crypto');
import request = require('request');
import Promise = require('bluebird');
import path = require('path');
import fs = require('fs')

import config = require('../config');

import Models = require('../models/models');
import VideoJob = Models.VideoJob;

import uuid = require('node-uuid');
const AWS = require('aws-sdk');

AWS.config.update({
  region: config.env().aws.region,
  accessKeyId: config.env().aws.access_key_id,
  secretAccessKey: config.env().aws.secret_access_key
});

var s3 = new AWS.S3();
var elasticTranscoder = new AWS.ElasticTranscoder({apiVersion: '2012-09-25'});
var qualityForThumbnails = config.env().aws.transcoder.qualityForThumbnails;

export function processThumbnails(key: string) {

  //extension is set in transcoder PRESET
  var thumbnailKeyFirst = key + '_thumbnail-' + pad(1, 5) + '.' + config.env().aws.transcoder.qualities[qualityForThumbnails].thumbnailExtension;
  var thumbnailKeySecond = key + '_thumbnail-' + pad(2, 5) + '.' + config.env().aws.transcoder.qualities[qualityForThumbnails].thumbnailExtension;
  //check if second thumbnail exists
  s3.getObject({
    Bucket: config.env().aws.s3.bucket,
    Key: thumbnailKeySecond
  }, function (err, data) {

    //second thumbnail exists, copy it over first
    if (err == null) {
      s3.copyObject({
        ACL: 'public-read',
        // StorageClass: 'STANDARD', // By default Amazon Transcoder uses 'STANDARD'
        Bucket: config.env().aws.s3.bucket,
        CopySource: config.env().aws.s3.bucket + '/' + thumbnailKeySecond,
        Key: thumbnailKeyFirst
      }, function (err, data) {
        if (err == null) {
          logger.info('Copied thumbnail ' + thumbnailKeyFirst);
          deleteThumbnails(key, 2);
        } else {
          logger.info('Error copying thumbnail ' + thumbnailKeyFirst);
        }
      })
    }
  });
}

export interface UploadThumbnailResult {
  fileId?: number;
  key?: string;
  ext?: string;
  name?: string;
  size?: number;
  error?: any;
}

export function uploadThumbnail(fileId: number, videoFileKey: string, fileExtension: string, pathToFile: string): Promise<UploadThumbnailResult> {
  let result: UploadThumbnailResult = {
    fileId: fileId
  };

  return new Promise(function (resolve, reject) {
    const thumbnailKey: string = videoFileKey + '_thumbnail-' + uuid.v4() + '.' + fileExtension;

    fs.readFile(pathToFile, function (err, fileData: Buffer) {
      if (err) {
        result.error = err;
        resolve(result);
        return;
      }

      let params = {
        ACL: 'public-read',
        // StorageClass: 'STANDARD', // By default Amazon Transcoder uses 'STANDARD'
        Bucket: config.env().aws.s3.bucket,
        Key: thumbnailKey,
        ContentType: 'image/png',
        Body: fileData,  // buffer
      };

      s3.putObject(params, function (err, data) {
        if (err) {
          result.error = err;
          resolve(result);
          return;
        }

        if (!fileData) {
          result.error = 'File data not found';
          resolve(result);
          return;
        }

        result.key = thumbnailKey;
        result.ext = fileExtension;
        result.size = fileData.length;
        result.name = thumbnailKey;

        resolve(result);
      })
    });
  });
}

function deleteThumbnails(key: string, startingIndex: number) {

  var thumbnailKey = key + '_thumbnail-' + pad(startingIndex, 5) + '.' + config.env().aws.transcoder.qualities[qualityForThumbnails].thumbnailExtension;
  s3.getObjectAcl({
    Bucket: config.env().aws.s3.bucket,
    Key: thumbnailKey
  }, function (err, data) {
    if (err) {
      logger.info('Deleted all thumbnails for file. Key: ' + key);
    } else {
      deleteFile(thumbnailKey).then(function (result) {
        deleteThumbnails(key, startingIndex + 1);
      }, function (err) {
        //nothing
      });
    }
  })
}

function pad(num, size) {
  var s = num + "";
  while (s.length < size) s = "0" + s;
  return s;
}


export function deleteFile(key: string): Promise<any> {
  return new Promise(function (resolve, reject) {
    s3.deleteObject({
      Bucket: config.env().aws.s3.bucket,
      Key: key
    }, function (err, data) {
      if (err) {
        logger.error('Error deleting file form S3. Key: ' + key, err);
        reject(err);
      } else {
        logger.info('Deleted file from S3. Key: ' + key);
        resolve(data);
      }
    });
  });
}


export function createTranscoderJob(s3Key: string, videoId: number, qualities: Array<string>, t): q.Promise<any> {
  var def = q.defer();
  var params = {
    Input: {
      Key: s3Key,
      FrameRate: 'auto',
      Resolution: 'auto',
      AspectRatio: 'auto',
      Interlaced: 'auto',
      Container: 'auto'
    },
    PipelineId: config.env().aws.transcoder.pipeline,
    Outputs: [],
    UserMetadata: {
      videoId: videoId.toString()
    }
  };

  qualities.forEach(function (quality) {
    var output: any = {
      Key: uuid.v4() + '-' + quality + '.mp4',
      PresetId: config.env().aws.transcoder.qualities[quality].preset
    };

    if (quality == qualityForThumbnails) {
      output.ThumbnailPattern = output.Key + '_thumbnail-{count}';
    }
    params.Outputs.push(output);
  });

  elasticTranscoder.createJob(params, function (err, data) {
    var status = VideoJob.Status.SUBMITTED;
    var errorAt = null;
    var submittedAt = new Date();
    var transcoderJobId = null;
    var errorMessage = null;
    if (err) {
      logger.error('Error submitting job to AET', err);
      status = VideoJob.Status.ERROR;
      errorMessage = JSON.stringify(err);
      errorAt = new Date();
    } else {
      logger.info('Created AWS transcoding job', data);
      transcoderJobId = data.Job.Id;
    }

    VideoJob.Model.create({
      status: status,
      submittedAt: submittedAt,
      errorAt: errorAt,
      transcoderJobId: transcoderJobId,
      videoId: videoId,
      errorMessage: errorMessage
    }, {transaction: t}).then(function (job) {
      def.resolve(job);
    }, function (err) {
      logger.error('Error creating VideoJob', err);
      def.reject(err);
    })

  });

  return def.promise;
}

export function rotateImage(oldKey: string, deg: number): Promise<string> {
  let isValid = _.includes([90, 180, 270], deg);
  if (!isValid) {
    return Promise.reject('Invalid degree');
  }

  return Promise.try(function () {
    let rotateImageUrl = getRotateImageUrl(oldKey, deg);
    return addFileByUrl(rotateImageUrl);
  });
}

function addFileByUrl(url: string): Promise<string> {
  return new Promise<string>(function (resolve, reject) {
    request({
      url: url,
      encoding: null
    }, function (err, res, body) {
      if (err) {
        return reject(err);
      }

      let key = uuid.v4() + path.extname(url);

      s3.putObject({
        ACL: 'public-read',
        Bucket: config.env().aws.s3.bucket,
        Key: key,
        ContentType: res.headers['content-type'],
        ContentLength: res.headers['content-length'],
        Body: body  // buffer
      }, function (err) {
        if (err) {
          reject(err);
        } else {
          resolve(key);
        }
      });
    })
  });
}

export function generateSignature(toSign: string): string {
  const signature = crypto
    .createHmac('sha1', config.env().aws.secret_access_key)
    .update(toSign)
    .digest('base64');
  return signature;
}

export function getRotateImageUrl(key: string, deg: number): string {
  return config.env().aws.s3.nginx + '/rotate/' + deg + '/' + key;
}

export function getResizeImageUrl(key: string, width: number, height?: number): string {
  return config.env().aws.s3.nginx + '/resize/' + width + 'x' + (height || '-') + '/' + key;
}