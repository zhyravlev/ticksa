/// <reference path="../typings/tsd.d.ts"/>
import _ = require('lodash');
import Promise = require("bluebird");
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');

import Errors = require('../errors');
import Models = require('../../app/models/models');
import Tickser = Models.Tickser;
import TickserChannelRank = Models.TickserChannelRank;
import Channel = Models.Channel;
import LoggedEvent = Models.LoggedEvent;

import LoggedEventService = require('./logged-event-service');
import * as TickserChannelRankMapper from '../mappers/tickser-channel-rank-mapper';

export function updateRank(tickserId: number, channelId: number, ticksAmount: number, t: Sequelize.Transaction): Promise<any> {
  return TickserChannelRank.Model.findOneByTickserIdAndChannelId(tickserId, channelId, {transaction: t})
    .then(function (rank: TickserChannelRank.Instance) {
      if (rank == null) {
        let rankAttributes: TickserChannelRank.Attributes = {
          tickserId: tickserId,
          channelId: channelId,
          earnedTicks: ticksAmount >= 0 ? ticksAmount : 0,
        };
        let createRank: Promise<TickserChannelRank.Instance> = TickserChannelRank.Model.create(rankAttributes, {transaction: t});

        let loggedEvent: Promise<LoggedEvent.Instance> = ticksAmount >= 0
          ? Promise.resolve<LoggedEvent.Instance>(null)
          : LoggedEventService.createEventAboutNegativeAmount(tickserId, channelId, ticksAmount, t);

        return Promise.all([createRank, loggedEvent]);
      }

      let newAmount = <number> rank.earnedTicks + ticksAmount;
      rank.earnedTicks = newAmount >= 0 ? newAmount : 0;

      let updateRank: Promise<TickserChannelRank.Instance> = rank.save({transaction: t});
      let loggedEvent: Promise<LoggedEvent.Instance> = newAmount >= 0
        ? Promise.resolve<LoggedEvent.Instance>(null)
        : LoggedEventService.createEventAboutNegativeAmount(tickserId, channelId, newAmount, t);

      return Promise.all([updateRank, loggedEvent]);
    });
}

export function findRankedList(channelId: number, pageable: Ticksa.IPageable<void>): Promise<TickserChannelRankMapper.ITickserChannelRank[]> {
  return TickserChannelRank.Model.scope(TickserChannelRank.Scopes.INCLUDE_TICKSER).findAllByChannelId(channelId, {
    where: {
      earnedTicks: {
        $gt: 0
      }
    },
    limit: pageable.limit,
    offset: pageable.offset,
    order: [
      ['earnedTicks', 'DESC'],
      ['updated_at', 'DESC']
    ],
  }).then(function (ranks: TickserChannelRank.Instance[]) {
    return TickserChannelRankMapper.mapAsList(ranks);
  })
}

export function resetRankedListByChannelId(channelId: number): Promise<Channel.Instance> {
  return Models.getConnection().transaction(function (t) {

    return Channel.Model.findById(channelId, {transaction: t})
      .then(function (channel: Channel.Instance) {
        if (!channel) {
          throw new Errors.NotFound('Channel not found');
        }
        return resetRankedListByChannel(channel, t);
      });
  })
}

export function resetRankedListByChannel(channel: Channel.Instance, t: Sequelize.Transaction): Promise<Channel.Instance> {
  let destroyChannelRank = TickserChannelRank.Model.destroy({
    where: {
      channelId: channel.id
    },
    transaction: t
  });

  channel.showRankedListDate = new Date();
  let updateChannel = channel.save({transaction: t});

  return Promise.all([destroyChannelRank, updateChannel])
    .spread<Channel.Instance>(function (destroyChannelRank: any, updatedChannel: Channel.Instance) {
      return updatedChannel;
    });
}