/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import * as _ from 'lodash';
import * as Promise from 'bluebird';
import * as Errors from '../errors';
import {
  Playlist,
  Content,
  Tickser,
  TickserChannel
} from '../models/models';
import * as PlaylistMessages from '../dto/playlist/playlist-messages';
import {PermissionMap, getContentPermissionMap} from './content-access-service';
import {UserMeta, getContentsFilteredByUserAvailability, sortContentBySortedIds} from './content-service';

export default class PlaylistService {

  public static async createUserPlaylist(userMeta: UserMeta, playlistRequest: PlaylistMessages.IPlaylistUserPostRequest): Promise<Playlist.Instance> {
    await PlaylistService.assertThatEveryContentIsAvailableForUser(userMeta, playlistRequest.contentsIds);

    const newPlaylist: Playlist.Attributes = {
      tickserId: userMeta.tickserId,
      name: playlistRequest.name,
      coverImageId: playlistRequest.coverImageId,
      backgroundImageId: playlistRequest.backgroundImageId,
      published: true,
      deleted: false,
      orderedContents: playlistRequest.contentsIds
    };
    return Playlist.Model.create(newPlaylist);
  }

  public static getUserPlaylists(tickser: Tickser.Instance): Promise<Playlist.Instance[]> {
    return Playlist.Model.scope([
      Playlist.Scopes.INCLUDE_COVER_IMAGE,
      Playlist.Scopes.IS_NOT_DELETED
    ]).findAllByTickserId(tickser.id);
  }

  public static async updateUserPlaylist(userMeta: UserMeta, playlistId: number, playlistRequest: PlaylistMessages.IPlaylistUserPostRequest): Promise<void> {
    const playlist: Playlist.Instance = await Playlist.Model.scope([
      Playlist.Scopes.IS_NOT_DELETED
    ]).findById(playlistId);

    PlaylistService.assertPlaylistAndCreator(playlist, userMeta.tickserId);
    await PlaylistService.assertThatEveryContentIsAvailableForUser(userMeta, playlistRequest.contentsIds);

    playlist.name = playlistRequest.name;
    playlist.coverImageId = playlistRequest.coverImageId;
    playlist.backgroundImageId = playlistRequest.backgroundImageId;
    playlist.published = playlistRequest.published;
    playlist.orderedContents = playlistRequest.contentsIds;

    await playlist.save();
  }

  public static async deleteUserPlaylist(tickser: Tickser.Instance, playlistId: number): Promise<void> {
    const playlist: Playlist.Instance = await Playlist.Model.scope([
      Playlist.Scopes.IS_NOT_DELETED
    ]).findById(playlistId);

    PlaylistService.assertPlaylistAndCreator(playlist, tickser.id);
    playlist.deleted = true;
    await playlist.save();
  }

  public static async getUserPlaylistAndContents(userMeta: UserMeta, playlistId: number): Promise<UserPlaylistAndContents> {
    const playlist: Playlist.Instance = await Playlist.Model.scope([
      Playlist.Scopes.IS_NOT_DELETED,
      Playlist.Scopes.INCLUDE_BACKGROUND_IMAGE,
      Playlist.Scopes.INCLUDE_COVER_IMAGE
    ]).findById(playlistId);

    PlaylistService.assertPlaylistAndCreator(playlist, userMeta.tickserId);

    const contents: Content.Instance[] = await getContentsFilteredByUserAvailability(userMeta, playlist.orderedContents);
    return {
      playlist: playlist,
      contents: sortContentBySortedIds(contents, playlist.orderedContents)
    }
  }

  public static async getPlaylistAndContentsWithPermissions(userMeta: UserMeta, accountId: number, playlistId: number): Promise<PlaylistAndContentsWithPermissions> {
    const playlist: Playlist.Instance = await Playlist.Model.scope([
      Playlist.Scopes.IS_PUBLISHED,
      Playlist.Scopes.IS_NOT_DELETED,
      Playlist.Scopes.INCLUDE_BACKGROUND_IMAGE,
      Playlist.Scopes.INCLUDE_COVER_IMAGE
    ]).findById(playlistId);

    if (playlist == null) {
      throw new Errors.NotFound('Playlist not found');
    }

    const [contents, tickserChannels] = await Promise.all([
      getContentsFilteredByUserAvailability(userMeta, playlist.orderedContents),
      TickserChannel.Model.findAllByTickserId(userMeta.tickserId)
    ]);
    const permissionMap: PermissionMap = await getContentPermissionMap(accountId, contents, tickserChannels);
    return {
      playlist: playlist,
      contents: sortContentBySortedIds(contents, playlist.orderedContents),
      permissionMap: permissionMap
    }
  }

  private static assertPlaylistAndCreator(playlist: Playlist.Instance, tickserId: number): void {
    if (playlist == null) {
      throw new Errors.NotFound('Playlist not found');
    }

    if (playlist.tickserId != tickserId) {
      throw new Errors.Forbidden('Only creator can have access');
    }
  }

  private static async assertThatEveryContentIsAvailableForUser(userMeta: UserMeta, contentIds: number[]): Promise<boolean> {
    if (contentIds.length == 0) {
      return true;
    }

    const contents: Content.Instance[] = await getContentsFilteredByUserAvailability(userMeta, contentIds);
    if (contents.length != contentIds.length) {
      logger.error(
        'Different count of ids and got contents. ' +
        'Initial: ' + contentIds + '. ' +
        'Got: ' + _.map(contents, c => c.id)
      );
      throw new Errors.BadRequest('Incorrect content ids');
    }

    return true;
  }
}

export interface UserPlaylistAndContents {
  playlist: Playlist.Instance,
  contents: Content.Instance[]
}

export interface PlaylistAndContentsWithPermissions {
  playlist: Playlist.Instance,
  contents: Content.Instance[],
  permissionMap: PermissionMap
}
