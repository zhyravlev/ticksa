/// <reference path="../typings/tsd.d.ts"/>
import _ = require('lodash');
import Promise = require("bluebird");
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');

import Errors = require('../errors');
import Models = require('../../app/models/models');
import LoggedEvent = Models.LoggedEvent;

export function createEventAboutNegativeAmount(tickserId: number,
                                               channelId: number,
                                               amount: number,
                                               t: Sequelize.Transaction): Promise<LoggedEvent.Instance> {
  let loggedEventAttributes: LoggedEvent.Attributes = {
    event: LoggedEvent.EventBuilder.tickerChannelRankShouldBeNegative(tickserId, channelId, amount)
  };

  return LoggedEvent.Model.create(loggedEventAttributes, {transaction: t});
}

export function createEventAboutChannelChange(contentId: number,
                                              oldChannelId: number,
                                              newChannelId: number,
                                              t: Sequelize.Transaction): Promise<LoggedEvent.Instance> {
  let loggedEventAttributes: LoggedEvent.Attributes = {
    event: LoggedEvent.EventBuilder.contentChannelChanged(contentId, oldChannelId, newChannelId)
  };

  return LoggedEvent.Model.create(loggedEventAttributes, {transaction: t});
}