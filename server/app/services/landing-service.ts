/// <reference path="../typings/tsd.d.ts"/>
import GeoipService = require('./geoip-service')
import Promise = require("bluebird");
import Ticksa = require("ticksa");

import Models = require('../models/models');
import Landing = Models.Landing;

export function registerForAnonymous(url: string, req: Ticksa.Request): Promise<any> {
  return new Promise(function (resolve, reject) {
    resolve(Landing.Model.registerAnonymousLanding(url, req.local.geoip.ip, req.local.geoip.countryA2Code));
  });
}

export function registerForTickser(tickser: any, url: string, req: Ticksa.Request): Promise<any> {
  return new Promise(function (resolve, reject) {
    resolve(Landing.Model.registerTickserLanding(tickser, url, req.local.geoip.ip, req.local.geoip.countryA2Code));
  });
}
