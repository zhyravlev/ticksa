/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import _ = require('lodash');
import fs = require('fs');
import i18next = require('i18next');
import path = require('path');
import Promise = require('bluebird');
import config = require('../config');

import ValueSelector = require('../utils/value-selector');

module i18n {

  let LANGUAGES_SELECTOR: ValueSelector.ValueSelector<string>;
  let I18N_OBJECT: I18next.I18n;

  export function initLocalization(assetsDir: string): Promise<any> {
    const rawPath = config.isLocal() || config.isTest()
      ? __dirname + '/../../../webapp/src/assets/languages'
      : __dirname + '/../public/' + assetsDir + '/assets/languages';

    const normalizedPath = path.normalize(rawPath);
    logger.info('dir: ' + normalizedPath);

    const readdir = Promise.promisify<string[], string>(fs.readdir);
    return readdir(normalizedPath).then(function (files: string[]) {
      const resources: I18next.ResourceStore = {};
      const availableLanguages: ValueSelector.KeyValueDictionary<string> = new ValueSelector.KeyValueDictionary<string>();

      files.forEach(function (file: string) {
        let langName = file.replace('.json', '').toLowerCase();
        resources[langName] = {
          translation: require(normalizedPath + '/' + file)
        };
        availableLanguages.set(langName, langName);
      });

      const availableLanguagesNames = availableLanguages.getKeys();
      let defaultLanguage: string = 'en';
      if (availableLanguagesNames.length > 0) {
        if (!_.contains(availableLanguagesNames, 'en')) {
          defaultLanguage = _.first(availableLanguagesNames);
        }
        logger.info('available languages: ' + availableLanguagesNames + '. default: ' + defaultLanguage);
      } else {
        logger.error('ATTENTION! no translations found')
      }

      I18N_OBJECT = i18next.init({
        saveMissing: false,
        debug: false,
        resources: resources,
        fallbackLng: defaultLanguage
      });
      LANGUAGES_SELECTOR = new ValueSelector.ValueSelector<string>(availableLanguages, defaultLanguage);
    });
  }

  export function t(key: string, language: string): string|any|Array<any> {
    if (I18N_OBJECT == null) {
      return key;
    }

    if (LANGUAGES_SELECTOR == null) {
      return I18N_OBJECT.t(key, {lng: language});
    }

    const lang: string = LANGUAGES_SELECTOR.select(language);
    return I18N_OBJECT.t(key, {lng: lang});
  }

  export function getAvailableLanguages(): string[] {
    if (LANGUAGES_SELECTOR == null) {
      return [];
    }

    return LANGUAGES_SELECTOR.getKeys();
  }
}

export = i18n;