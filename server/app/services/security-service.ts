/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import bcrypt = require('bcrypt');
import jwt = require('jsonwebtoken');
import express = require('express');
import Promise = require('bluebird');
import ticksa = require('ticksa');
import Errors = require('../errors');

import Models = require('../models/models');
import Tickser = Models.Tickser;

import utilities = require('../services/utilities');
import config = require('../config');

export function createPassword(password: string): Promise<string> {
  return new Promise<any>(function (resolve, reject) {
    bcrypt.hash(password, 10, function (err, hash) {
      if (err || !hash) {
        reject(err);
      } else {
        resolve(hash);
      }
    });
  })
}

export function comparePassword(comparablePassword: string, passwordHash: string): Promise<boolean> {
  return new Promise<boolean>(function (resolve, reject) {
    bcrypt.compare(comparablePassword, passwordHash, function (err, result) {
      if (err || !result) {
        reject(new Errors.InvalidCredentials());
      } else {
        resolve(true);
      }
    });
  });
}

export interface TokenData {
  exp: number;
  iat: number;
  encId: string;   // ATTENTION! It is only used for logging purposes.
  username: string;
}

export interface EmailTokenData {
  email: string,
  recipientId: number,
  tickserId: number
}

export function generateToken(tickserId: number, username: string): string {
  // ATTENTION!
  //
  // It is used and decrypted only in webapp-logger - only to log tickserId.
  // In other routes it seems like to be unnecessary overhead because of Authorization.authenticate method
  // which queries DB to get Tickser by his username.
  const encryptedId: string = utilities.encodeNumberToRadix64(tickserId);

  return generateTokenForEncryptedIdAndUsername(encryptedId, username);
}

export function generateEmailToken(data: EmailTokenData): string {
  try {
    return jwt.sign(
      data,
      config.env().mail.secretKey,
      {
        expiresIn: config.env().security.tokenExpirationIn
      });
  } catch (err) {
    logger.error('Error while generating token', err);
    throw new Errors.ServerError(err);
  }
}

export function generateTokenForEncryptedIdAndUsername(encryptedTickserId: string, username: string): string {
  try {
    return jwt.sign(
      {
        encId: encryptedTickserId,
        username: username
      },
      config.env().security.secret,
      {
        expiresIn: config.env().security.tokenExpirationIn
      });
  } catch (err) {
    logger.error('Error while generating token', err);
    throw new Errors.ServerError(err);
  }
}

export function verifyToken(token: string): Promise<TokenData> {
  return new Promise<TokenData>(function(resolve, reject) {
    jwt.verify(token, config.env().security.secret, function (err, decoded) {
      if (err) {
        logger.error('JWT verify error: ' + err + '. for token: ' + token);
        reject(err);
      } else {
        resolve(decoded);
      }
    });
  });
}

export function verifyEmailToken(token: string): Promise<EmailTokenData> {
  return new Promise<EmailTokenData>(function(resolve, reject) {
    jwt.verify(token, config.env().mail.secretKey, function (err, decoded) {
      if (err) {
        logger.error('JWT verify error: ' + err + '. for email token: ' + token);
        reject(err);
      } else {
        resolve(decoded);
      }
    });
  });
}

export function decodeToken(token: string): TokenData {
  return jwt.decode(token);
}