/// <reference path="../typings/tsd.d.ts"/>
import Promise = require("bluebird");
import Ticksa = require('ticksa');
import Errors = require('../errors')

import Models = require('../models/models');
import Tickser = Models.Tickser;

/**
 * Returns simple Tickser entity without any additional includes by username,
 *
 * @param username {string} User name
 * @returns {Promise<Tickser.Instance>}
 */
export function findSimpleTickserByUsername(username: string): Promise<Tickser.Instance> {
  return Tickser.Model.findOneByUsername(username).then(function (tickser: Tickser.Instance) {
    if (!tickser) {
      throw new Errors.UnknownUser();
    }
    if (tickser.checkIfSuspended()) {
      throw new Errors.SuspendedUser();
    }
    if (!tickser.checkIfActive()) {
      throw new Errors.InvalidCredentials();
    }
    if (!tickser.password) {
      throw new Errors.InvalidCredentials();
    }
    return tickser;
  });
}

export function setLoginIpAndCountryById(id: number, geoip: Ticksa.IGeoipData): Promise<any> {
  return Tickser.Model.setLoginIpAndCountry(id, geoip.ip, geoip.countryA2Code)
}