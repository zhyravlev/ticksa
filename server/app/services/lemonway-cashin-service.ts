/// <reference path="../typings/tsd.d.ts"/>
import Promise = require('bluebird')
import conn = require('../connection')
import E = require('../errors')

import lemonway = require('../api/lemonway/lemonway')

import Models = require('../models/models');
import CashIO = Models.CashIO;
import LemonwayCashIn = Models.LemonwayCashIn;
import LemonwayCashCallback = Models.LemonwayCashCallback;
import Currency = Models.Currency;
import Account = Models.Account;

import LemonwayService = require('./lemonway-service')
import AccountService = require('./account-service');
import LemonwayWallet = require('../models/financial/lemonway/lemonway-wallet');

export interface CallbackHandlingResult {
  cashInFinalized?: boolean;
  lwCashIn?: LemonwayCashIn.Instance;
  lwCashCallback?: LemonwayCashCallback.Instance;
}

export function handleCallbackAndFinalizeCashIn(response: lemonway.FinalizeCardPaymentPostResponse,
                                                type: LemonwayCashCallback.Type,
                                                method: LemonwayCashCallback.Method): Promise<CallbackHandlingResult> {

  var wkToken = response.response_wkToken;

  return LemonwayCashIn.Model.scope('cashIo').findOneByWkToken(wkToken).then(function (lwCashIn: LemonwayCashIn.Instance) {
    if (!lwCashIn) {
      throw new E.BadRequest('LemonwayCashIn with wkToken: ' + wkToken + ' not found!')
    }

    var handlingResult: CallbackHandlingResult = {
      lwCashIn: lwCashIn,
    };

    handlingResult.lwCashCallback = LemonwayCashCallback.Model.build({
      cashInId: lwCashIn.id,
      params: JSON.stringify(response),
      method: method,
    });

    if (handlingResult.lwCashIn.status != LemonwayCashIn.Status.CREATED) {
      handlingResult.lwCashCallback.type = LemonwayCashCallback.Type.REPEATED;
      handlingResult.cashInFinalized = false;
      return Promise.resolve(handlingResult);
    }

    if (type == LemonwayCashCallback.Type.CANCEL) {
      handlingResult.lwCashCallback.type = LemonwayCashCallback.Type.CANCEL;
      handlingResult.lwCashIn.status = LemonwayCashIn.Status.FAILED;
      handlingResult.lwCashIn.cashIo.status = CashIO.Status.FAILED;
      handlingResult.cashInFinalized = false;
      return Promise.resolve(handlingResult);
    }

    if (type == LemonwayCashCallback.Type.ERROR) {
      handlingResult.lwCashCallback.type = LemonwayCashCallback.Type.ERROR;
      handlingResult.lwCashIn.status = LemonwayCashIn.Status.FAILED;
      handlingResult.lwCashIn.cashIo.status = CashIO.Status.FAILED;
      handlingResult.cashInFinalized = false;
      return Promise.resolve(handlingResult);
    }

    return getDetailsSetFeesAndMarkCashIoCompleted(response, handlingResult);
  })
    .then(function (result: CallbackHandlingResult) {
      if (result.lwCashCallback.type == LemonwayCashCallback.Type.REPEATED) {
        return result.lwCashCallback.save().then(function (updatedLwCallback: LemonwayCashCallback.Instance) {
          result.lwCashCallback = updatedLwCallback;
          return result;
        })
      }

      return conn.transaction(function (t) {
        var saveCashIn = result.lwCashIn.save({transaction: t});
        var saveCashCallback = result.lwCashCallback.save({transaction: t});
        var saveCashIo = result.lwCashIn.cashIo.save({transaction: t});

        return Promise.join<any>(saveCashIn, saveCashCallback, saveCashIo,
          function (updatedLwCashIn: LemonwayCashIn.Instance, updatedLwCallback: LemonwayCashCallback.Instance) {
            result.lwCashIn = updatedLwCashIn;
            result.lwCashCallback = updatedLwCallback;
            return result;
          });
      });
    });
}

function getDetailsSetFeesAndMarkCashIoCompleted(response: lemonway.FinalizeCardPaymentPostResponse,
                                                 handlingResult: CallbackHandlingResult): Promise<CallbackHandlingResult> {
  return LemonwayService.request().getMoneyInTransDetails(response.response_transactionId)
    .then(function (transResult: LemonwayService.GetMoneyInTransDetailsResult) {
      handlingResult.lwCashCallback.transDetailRequest = JSON.stringify(transResult.request);
      handlingResult.lwCashCallback.transDetailResponse = JSON.stringify(transResult.response);
      handlingResult.lwCashCallback.message = JSON.stringify(transResult.message);

      if (!transResult.success) {
        handlingResult.lwCashCallback.type = LemonwayCashCallback.Type.ERROR;
        handlingResult.lwCashIn.status = LemonwayCashIn.Status.FAILED;
        handlingResult.lwCashIn.cashIo.status = CashIO.Status.FAILED;
        handlingResult.cashInFinalized = false;
        return Promise.resolve(handlingResult);
      }

      handlingResult.lwCashCallback.type = LemonwayCashCallback.Type.SUCCESS;
      handlingResult.lwCashIn.status = LemonwayCashIn.Status.COMPLETED;
      handlingResult.lwCashIn.cashIo.status = CashIO.Status.COMPLETED;

      handlingResult.cashInFinalized = true;
      return Promise.resolve(handlingResult);
    });
}

export function createLwCallback(wkToken: string, params: string, type: LemonwayCashCallback.Type, method: LemonwayCashCallback.Method): Promise<LemonwayCashCallback.Instance> {

  return LemonwayCashIn.Model.findOneByWkToken(wkToken).then(function (lwCashIn: LemonwayCashIn.Instance) {
    if (!lwCashIn) {
      return Promise.reject({status: 400, message: 'LemonwayCashIn with WK_TOKEN: ' + wkToken + " not found!"});
    }

    return LemonwayCashCallback.Model.create({
      cashInId: lwCashIn.id,
      params: JSON.stringify(params),
      type: type,
      method: method,
    });
  });
}

export interface CreateLwCashInParams {
  user: any;
  moneyAmount: number;
  initRequest?: string;
  initResponse?: string;
}

export function createLwCashIn(params: CreateLwCashInParams, type: LemonwayCashIn.Type, status?: LemonwayCashIn.Status): Promise<LemonwayCashIn.Instance> {
  var findCurrency = Currency.Model.scope('EUR').findOne();
  var findAccount = AccountService.findNormalAccountByTickserId(params.user.id);
  return conn.transaction(function (t) {
    return Promise.all([findCurrency, findAccount])
      .spread(function (euroCurrency: Currency.Instance, account: Account.Instance) {

        var cashIoStatus: CashIO.Status;
        var cashInStatus: LemonwayCashIn.Status;

        if (status == LemonwayCashIn.Status.COMPLETED) {
          cashIoStatus = CashIO.Status.COMPLETED;
          cashInStatus = LemonwayCashIn.Status.COMPLETED;
        } else if (status == LemonwayCashIn.Status.FAILED) {
          cashIoStatus = CashIO.Status.FAILED;
          cashInStatus = LemonwayCashIn.Status.FAILED;
        } else {
          cashIoStatus = CashIO.Status.PENDING;
          cashInStatus = LemonwayCashIn.Status.CREATED;
        }

        var cashIO: CashIO.Attributes = {
          accountId: account.id,
          tickserId: params.user.id,
          userCurrencyId: euroCurrency.id,
          userCurrencyAmount: params.moneyAmount,
          feesCurrencyId: euroCurrency.id,
          feesAmount: calculateFees(params.moneyAmount),
          countryId: params.user.countryId,
          tickserEmail: params.user.email,
          status: cashIoStatus,
          type: CashIO.Type.CASH_IN,
          paymentType: CashIO.PaymentType.LEMONWAY
        };

        return CashIO.Model.create(cashIO, {
          transaction: t
        }).then(function (cashIO: CashIO.Instance) {

          var lemonwayCashIn: LemonwayCashIn.Attributes = {
            status: cashInStatus,
            type: type,
            cashIoId: cashIO.id,
            initRequest: params.initRequest,
            initResponse: params.initResponse,
            wkToken: LemonwayService.generateWkToken(cashIO.id)
          };

          return LemonwayCashIn.Model.create(lemonwayCashIn, {
            transaction: t
          });
        })
      })
  });
}

// global fees on LW = 1,8 % + 0,15 Euro per Credit Card transaction world wide.
export function calculateFees(amount: number): number {
  return amount / 100 * 1.8 + 0.15;
}

