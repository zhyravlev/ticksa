import moment = require('moment');
import Promise = require("bluebird");
import Sequelize = require("sequelize");
import Ticksa = require("ticksa");

import config = require("../config");
import Models = require("../models/models");

import Notification = Models.Notification;
import Tickser = Models.Tickser;
import TickserChannel = Models.TickserChannel;
import ChannelInvite = Models.ChannelInvite;

export function createForChannelSubscribe(tickser: any, channelId: number, t: Sequelize.Transaction): Promise<Notification.Instance> {
  return TickserChannel.Model.findOne({
      where: {
        channelId: channelId,
      },
      transaction: t
    }
  ).then(function (tickserChannel) {
      if (!tickserChannel) {
        return Promise.reject({status: 404, message: 'Channel not found'});
      }

      return Notification.Model.create({
        title: 'Subscription',
        content: tickser.username + ' has subscribed to your channel',
        type: Notification.Type.CHANNEL_SUBSCRIBE,
        tickserId: tickserChannel.tickserId,
      }, {transaction: t});
    });
}

export function createForGrandFreeTicks(tickserId: number, amount: number, t: Sequelize.Transaction): Promise<Notification.Instance> {
  return Notification.Model.create({
    type: Notification.Type.GRANT_FREE_TICKS,
    tickserId: tickserId,
    metadata: {
      amount: amount,
    }
  }, {transaction: t});
}

export function createForGrantCommissionTicks(tickserId: number, amount: number, t: Sequelize.Transaction): Promise<Notification.Instance> {
  return Notification.Model.create({
    type: Notification.Type.GRANT_COMMISSION_TICKS,
    tickserId: tickserId,
    metadata: {
      amount: amount,
    }
  }, {transaction: t});
}

export interface ChannelInviteNotificationParams {
  tickserId: number;
  tickserName: string;
  channelName: string;
  inviteOwnerType: ChannelInvite.OwnerType;
  inviteId: number;
  t: Sequelize.Transaction;
}

export function createForChannelInviteNotification(params: ChannelInviteNotificationParams) {
  var subject = 'You are invited for contribution to channel "' + params.channelName + '"';
  var content = 'Hi ' + params.tickserName + ',\n\nyou have been invited to participate as '
    + params.inviteOwnerType + ' on the channel ' + params.channelName + '.\n\nCheckout the details on Ticksa.\n\nStay awesome!';

  return Notification.Model.create({
    title: subject,
    content: content,
    type: Notification.Type.CHANNEL_EDIT_INVITE,
    tickserId: params.tickserId,
    metadata: {channelInviteId: params.inviteId}
  }, {transaction: params.t});
}

export function createForChannelSharedRevenueChanged(tickserId: number, channelName: string, percent: number, expiryDate: Date, t: Sequelize.Transaction): Promise<Notification.Instance> {
  return Notification.Model.create({
    type: Notification.Type.CHANNEL_SHARED_REVENUE_CHANGED,
    tickserId: tickserId,
    metadata: {
      percent: percent,
      channelName: channelName,
      expiryDate: expiryDate ? moment(expiryDate).format(config.DATE_FORMAT) : null,
    }
  }, {transaction: t});
}

export function viewAllNotificationByTickser(tickserId: number): Promise<any> {
  return Notification.Model.update({viewed: true}, {
    where: {
      tickserId: tickserId,
      viewed: false,
      type: {
        $notIn: [Notification.Type.MESSAGE, Notification.Type.CHANNEL_EDIT_INVITE],
      }
    }
  });
}