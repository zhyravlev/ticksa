/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import Promise = require("bluebird");
import express = require('express');
import _ = require('lodash');
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');

import config = require('../config');
import Errors = require('../errors');
import Models = require('../models/models');
import Image = Models.Image;
import File = Models.File;

import AwsService = require('../services/aws');

export interface RotateParam {
  id: number;
  deg: number;
}

export function rotateImages(params: RotateParam[], tickserId: number): Promise<any> {
  let imageIds = _.pluck(params, 'id');
  let options: Sequelize.FindOptions = {
    where: {
      tickserId: tickserId
    }
  };
  return Image.Model.scope(Image.Scopes.INCLUDE_FILE).findAllByIds(imageIds, options).then(function (images: Image.Instance[]) {
    if (images.length != imageIds.length) {
      throw new Errors.NotFound('Image not found');
    }

    let rotateImages: Promise<any>[] = _.map(images, function (image: Image.Instance) {
      let param: RotateParam = _.findWhere(params, {id: image.id});
      return rotateAndSaveFile(image.file, param.deg);
    });

    return Promise.all(rotateImages);
  })
}

function rotateAndSaveFile(file: File.Instance, deg: number): Promise<any> {
  let oldKey = file.key;

  return AwsService.rotateImage(oldKey, deg).then(function (key: string) {
    file.key = key;
    return file.save().then(function () {
      return AwsService.deleteFile(oldKey);
    });
  }).catch(function (err) {
    logger.error('Error rotation file ID: ' + file.id, err);
    return Promise.resolve();
  });
}
