/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import q = require('q');
import _ = require('lodash');
import Promise = require("bluebird");

import config = require('../config');
import conn = require('../connection');

import Models = require('../models/models');
import File = Models.File;
import Image = Models.Image;
import Video = Models.Video;
import VideoFile = Models.VideoFile;
import VideoJob = Models.VideoJob;

import AWSService = require('../services/aws');
var AWS = require('aws-sdk');

var sqs = new AWS.SQS({apiVersion: '2012-11-05'});
var elasticTranscoder = new AWS.ElasticTranscoder({apiVersion: '2012-09-25'});
var qualityForThumbnails = config.env().aws.transcoder.qualityForThumbnails;

export function processMessage() {
  sqs.receiveMessage({
    QueueUrl: config.env().aws.transcoder.sqs.url,
    MaxNumberOfMessages: 1,
    VisibilityTimeout: 300,
    WaitTimeSeconds: 20
  }, function (err, data) {
    try {
      // If there are any messages to get
      if (err) {
        logger.info('Error queue', err);
        return;
      }

      if(!data.Messages) {
        return;
      }

      var message = data.Messages[0];
      if(!message || !message.Body) {
        return;
      }

      var body = JSON.parse(message.Body);
      if(!body.Message) {
        return;
      }

      var notification = JSON.parse(body.Message);
      if (!notification) {
        return;
      }

      logger.info('Message received. State: ' + notification.state + ', jobId: ' + notification.jobId + ', videoId: ' + notification.userMetadata ? notification.userMetadata.videoId : null);

      processNotification(notification).then(function (result) {
        removeMessageFromQueue(message, notification);
      }).catch(function (err) {
        logger.error('Error processing notification', err);
        removeMessageFromQueue(message, notification);
      });
    } finally {
      process.nextTick(processMessage);
    }
  });
}

function removeMessageFromQueue(message, notification) {
  sqs.deleteMessage({
    QueueUrl: config.env().aws.transcoder.sqs.url,
    ReceiptHandle: message.ReceiptHandle
  }, function (err) {
    logger.info('Message removed from queue. State: ' + notification.state + ', jobId: ' + notification.jobId + ', videoId: ' + notification.userMetadata ? notification.userMetadata.videoId : null);
  });
}

function processNotification(notification): Promise<any> {
  return Promise.try(function () {
    var videoId = notification.userMetadata ? notification.userMetadata.videoId : null;
    var jobId = notification.jobId;
    var state = notification.state;

    switch (state) {
      case 'PROGRESSING':
        return processProgress(jobId);
      case 'COMPLETED':
        return new Promise(function (resolve, reject) {
          elasticTranscoder.readJob({Id: jobId}, function (err, data) {
            if (err) {
              reject(err);
            } else {
              resolve(processComplete(data.Job));
            }
          });
        });
      case 'WARNING':
        logger.info('Transcoder warning', notification);
        return Promise.resolve('Ignore warning');
      case 'ERROR':
        return new Promise(function (resolve, reject) {
          elasticTranscoder.readJob({Id: jobId}, function (err, data) {
            if (err) {
              reject(err);
            } else {
              resolve(processError(data.Job));
            }
          });
        });
      default:
        return Promise.reject('Unknown state: ' + state);
    }
  });
}

function processError(jobData): Promise<any> {

  return Promise.resolve(VideoJob.Model.findOne({
    where: {
      transcoderJobId: jobData.Id
    }
  }).then(function (job: VideoJob.Instance) {
    if (!job) {
      return Promise.reject('Job with ID:' + jobData.Id + ' not found.')
    }

    job.errorAt = new Date();
    job.errorMessage = JSON.stringify(jobData);

    var saveJob = job.save();
    var updateVideo = (Video.Model.update({
      transcodingStatus: Video.TranscodingStatus.ERROR
    }, {
      where: {
        id: job.videoId
      }
    }));

    return Promise.all([saveJob, updateVideo]).spread(function (updatedJob: VideoJob.Instance, updatedVideo: Video.Instance) {
      return updatedJob;
    }).catch(function (err) {
      logger.info('Job error saved. JobId ' + jobData.Id);
      return Promise.reject(err);
    });
  })).then(function (result) {
    return Promise.resolve('OK')
  }).catch(function (err) {
    logger.error('Error updating job error. JobId ' + jobData.Id);
    return Promise.reject(err)
  });
}

function processComplete(jobData: any): Promise<any> {
  return Promise.resolve(VideoJob.Model.findOne({
    where: {
      transcoderJobId: jobData.Id
    }
  })).then(function (job) {
    if (!job) {
      return Promise.reject('Job with ID:' + jobData.Id + ' not found.')
    }

    return conn.transaction(function (t) {
      return executeJob(job, jobData, t);
    });
  }).then(function (result) {
    logger.info('Job completed. JobId ' + jobData.Id);
    return Promise.resolve('OK');
  }).catch(function (err) {
    logger.error('Error updating job complete. JobId ' + jobData.Id, err);
    return Promise.reject(err);
  });
}

function getOutputFilePromisesFromJob(job, jobData, t): Array<Promise<any>> {
  var outputs = jobData.Outputs;
  var filePromises: Array<Promise<any>> = [];

  _.forEach<any>(outputs, function (output) {
    if (output.Status !== 'Complete') {
      logger.warn('Incomplete output jobId: ' + jobData.id + ', preset: ' + output.PresetId);
      return;
    }
    var file = {
      key: output.Key,
      isUploaded: true,
      size: output.FileSize,
      extension: 'mp4'
    };


    var presetData: any = _.find<any>(config.env().aws.transcoder.qualities, 'preset', output.PresetId);

    var promise = Promise.resolve(File.Model.create(file, {transaction: t}))
      .then(function (createdFile) {
        var newVideoFile: VideoFile.Attributes = {
          fileId: createdFile.id,
          videoId: job.videoId,
          bitrate: presetData.bitrate,
          width: output.Width,
          height: output.Height
        };
        return VideoFile.Model.create(newVideoFile, {transaction: t})
      }).then(function (createdVideoFile) {
        return createdVideoFile;
      });

    filePromises.push(promise);

  });
  return filePromises;
}

function executeJob(job, jobData, t) {
  var outputFilePromises = getOutputFilePromisesFromJob(job, jobData, t);

  return Promise.all(outputFilePromises).then(function (files) {
    return Video.Model.findOne({
      where: {
        id: job.videoId
      },
      transaction: t
    }).then(function (video) {
      return calculateDuration(jobData, job).then(function (duration) {
        return createFile(video, duration, jobData, t);
      });
    }).then(function (updatedVideo) {
      return saveJobAndDeleteFileFromAws(job, jobData, t);
    });
  });
}

function calculateDuration(jobData, job) {
  return new Promise(function (resolve, reject) {
    var timings = jobData.Timing;
    if (timings) {
      job.completedAt = new Date(timings.FinishTimeMillis);
      job.submittedAt = new Date(timings.SubmitTimeMillis);
      job.startedAt = new Date(timings.StartTimeMillis);
    }
    var detected: any = _.get(jobData, 'Input.DetectedProperties');
    var duration = null;
    if (detected) {
      duration = Math.round(detected.DurationMillis);
    }
    resolve(duration);
  });
}

function createFile(video, duration, jobData, t) {
  video.durationInMillis = duration;
  video.transcodingStatus = Video.TranscodingStatus.COMPLETE;

  return File.Model.create({
    key: jobData.Output.Key + '_thumbnail-00001.' + config.env().aws.transcoder.qualities[qualityForThumbnails].thumbnailExtension,
    extension: 'jpg',
    name: video.name + '_thumbnail',
    size: 0,
    isUploaded: true
  }, {transaction: t}).then(function (createdFile) {
    video.thumbnailImageId = createdFile.id;
    return video.save({transaction: t})
  }).catch(function (err) {
    logger.error('Error creating file entities');
    return Promise.reject(err);
  });
}

function saveJobAndDeleteFileFromAws(job, jobData, t) {
  job.status = VideoJob.Status.COMPLETE;
  return job.save({transaction: t}).then(function (updatedJob) {
    AWSService.deleteFile(jobData.Input.Key).then(function (result) {
      logger.info('Deleted source file for job ' + job.id);
    }).catch(function (err) {
      logger.info('Error deleting source file for job ' + job.id);
    });
    AWSService.processThumbnails(jobData.Output.Key);
    return updatedJob;
  });
}

function processProgress(jobId): Promise<any> {
  return VideoJob.Model.findOne({
    where: {
      transcoderJobId: jobId
    }
  }).then(function (job) {
    if (!job) {
      return Promise.reject('Job with ID:' + jobId + ' not found.')
    }
    job.status = VideoJob.Status.STARTED;
    job.startedAt = new Date();
    return job.save();
  }).then(function (updatedJob) {
    logger.info('Job in progress. JobId: ' + jobId);
    return Promise.resolve('OK')
  }).catch(function (err) {
    logger.error('Error updating job progress. JobId ' + jobId, err);
    return Promise.reject(err);
  });
}

