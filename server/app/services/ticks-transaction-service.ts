/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import * as Promise from 'bluebird';
import * as _ from 'lodash';
import * as Sequelize from 'sequelize';
import * as moment from 'moment';

import * as Errors from '../errors';
import * as config from '../config';

import * as AccountService from '../services/account-service';
import * as NotificationService from '../services/notification-service';
import * as ChannelService from '../services/channel-service';
import * as TickserChannelRankService from '../services/tickser-channel-rank-service';

import {
  Country,
  Tickser,
  Account,
  Channel,
  ChannelRevenue,
  TicksAmount,
  TicksTransaction,
  TickserChannelRank
} from '../models/models';
import Moment = moment.Moment;

export async function processRefundContentTransaction(contentId: number, tickserId: number, channelId: number, t: Sequelize.Transaction): Promise<TicksTransaction.Instance> {
  const account: Account.Instance = await Account.Model.findOneByTickserId(tickserId, {transaction: t});
  if (account == null) {
    throw new Errors.NotFound('Account not found');
  }

  const ticksTransactions: TicksTransaction.Instance[] = await getLatestAccessTransactionsForTheLast30Seconds(contentId, account.id);
  if (!ticksTransactions || ticksTransactions.length == 0) {
    throw new Errors.BadRequest('Refund is not possible');
  }

  const lastPaymentTransaction: TicksTransaction.Instance = ticksTransactions[0];
  const refundTransaction: TicksTransaction.Instance = await createRefundTransactionAndUpdateAccounts(lastPaymentTransaction, t);

  if (lastPaymentTransaction.amount >= 0) {
    const channel: Channel.Instance = await Channel.Model.findById(channelId, {transaction: t});
    if (channel == null) {
      throw new Errors.NotFound('Channel not found');
    }
    if (!channel.showRankedList) {
      return;
    }
    await TickserChannelRankService.updateRank(tickserId, channelId, -lastPaymentTransaction.amount, t);
  }

  return refundTransaction;
}

export function getLatestAccessTransactionsForTheLast30Seconds(contentId: number,
                                                               accountId: number,
                                                               now?: Moment): Promise<TicksTransaction.Instance[]> {
  let atNow = now ? now.clone() : moment();
  return TicksTransaction.Model.findTheLatestByContentIdAccountIdTypeAndTime(
    contentId,
    accountId,
    TicksTransaction.Type.ACCESS,
    atNow.subtract(30, 'seconds').toDate());
}

export interface GrandFreeTicksResult {
  ticksTransaction: any,
  userAccount: Account.Instance
}

export interface GrantFreeTicksParams {
  tickserId: number;
  systemAccountId: number;
  amount: number;
  t: Sequelize.Transaction;
}

export function processGrantFreeTicksTransaction(params: GrantFreeTicksParams): Promise<GrandFreeTicksResult> {
  let findSystemAccount = Account.Model.findById(params.systemAccountId, {transaction: params.t});
  let findUserAccount = Account.Model.findOneByTickserId(params.tickserId, {transaction: params.t});

  return Promise.all([findUserAccount, findSystemAccount])
    .spread<GrandFreeTicksResult>(function (userAccount: Account.Instance, systemAccount: Account.Instance) {

      if (!userAccount || !systemAccount) {
        throw new Errors.NotFound('Account not found')
      }

      userAccount.totalFreeTicks += params.amount;
      userAccount.currentBalanceFreeTicks += params.amount;

      let ticksAmounts: TicksAmount.Attributes[] = [
        {
          accountId: userAccount.id,
          amount: params.amount,
          type: TicksAmount.Type.FREE,
          reason: TicksAmount.Reason.DEFAULT
        },
        {
          accountId: systemAccount.id,
          amount: -1 * params.amount,
          type: TicksAmount.Type.FREE,
          reason: TicksAmount.Reason.DEFAULT
        }
      ];

      userAccount.balanceUpdated = new Date();

      let ticksTransaction = {
        contentId: null,
        feedId: null,
        amount: params.amount,
        type: TicksTransaction.Type.GRANT_FREE_TICKS,
        status: TicksTransaction.Status.NEW
      };

      let createTransaction = createTicksTransaction(ticksTransaction, ticksAmounts, params.t);
      let saveUserAccount = userAccount.save({transaction: params.t});
      let createNotification = NotificationService.createForGrandFreeTicks(userAccount.tickserId, params.amount, params.t);

      return Promise.all([createTransaction, saveUserAccount, createNotification]).spread<GrandFreeTicksResult>(function (createdTicksTransaction, updatedUserAccount) {
        return {ticksTransaction: createdTicksTransaction, userAccount: updatedUserAccount};
      });
    });
}

export function processOneWayTransaction(debitedTickserId: number, contentId: number, feedId: number, amount: number, type: number, transaction: Sequelize.Transaction): any {

  return Promise.all([
    Account.Model.findOneByTickserId(debitedTickserId, {transaction: transaction})
    //Account.Model.scope(['generalSystemAccount']).findOne({transaction: transaction}),
    //Account.Model.scope(['freeTicksSystemAccount']).findOne({transaction: transaction}),
  ]).spread(function (debitedAccount: Account.Instance) {

    let enoughTicks = checkTicksInAccount(debitedAccount, amount);

    if (!enoughTicks) {
      throw new Errors.PaymentRequired();
    }

    let ticksPayed = 0;
    let ticksAmounts: TicksAmount.Attributes[] = [];
    //process free ticks if debited user has any
    if (debitedAccount.currentBalanceFreeTicks != 0) {

      let ticksDebited: number = debitedAccount.currentBalanceFreeTicks >= amount ? amount : debitedAccount.currentBalanceFreeTicks;
      let adminCost = ticksDebited;
      ticksPayed = ticksDebited;

      debitedAccount.currentBalanceFreeTicks -= ticksDebited;
      debitedAccount.totalTicksDebited += ticksDebited;

      //freeTicksSystemAccount.currentBalance += adminCost;
      //freeTicksSystemAccount.totalTicksCredited += adminCost;

      ticksAmounts.push({
        accountId: debitedAccount.id,
        amount: -1 * ticksDebited,
        type: TicksAmount.Type.FREE,
        reason: TicksAmount.Reason.DEFAULT
      });

      ticksAmounts.push({
        accountId: Account.SystemAccount.FREE_TICKS_ACCOUNT,
        amount: adminCost,
        type: TicksAmount.Type.FREE,
        reason: TicksAmount.Reason.DEFAULT
      });
    }

    //process rest of the amount if free ticks did not pay for all
    if (ticksPayed < amount) {

      let ticksDebited = amount - ticksPayed;
      let adminCost = ticksDebited;

      debitedAccount.currentBalance -= ticksDebited;
      debitedAccount.totalTicksDebited += ticksDebited;


      //generalSystemAccount.currentBalance += adminCost;
      //generalSystemAccount.totalTicksCredited += adminCost;

      ticksAmounts.push({
        accountId: debitedAccount.id,
        amount: -1 * ticksDebited,
        type: TicksAmount.Type.PAID,
        reason: TicksAmount.Reason.DEFAULT
      });

      ticksAmounts.push({
        accountId: Account.SystemAccount.GENERAL_ACCOUNT,
        amount: adminCost,
        type: TicksAmount.Type.PAID,
        reason: TicksAmount.Reason.DEFAULT
      });

    }

    debitedAccount.balanceUpdated = new Date();

    let ticksTransaction = {
      contentId: contentId,
      feedId: feedId,
      type: type,
      amount: amount,
      status: TicksTransaction.Status.NEW
    };

    return Promise.all([
      createTicksTransaction(ticksTransaction, ticksAmounts, transaction),
      debitedAccount.save({transaction: transaction}),
      //generalSystemAccount.save({transaction: transaction}),
      //freeTicksSystemAccount.save({transaction: transaction})
    ]).spread(function (ticksTransaction: TicksTransaction.Instance, debitedAccount: Account.Instance) {
      return Promise.resolve({ticksTransaction: ticksTransaction, debitedAccount: debitedAccount});
    });
  });
}

interface CreditedAndSharingTAResult {
  ticksCreditedWithoutShared: number;
  ticksAmounts: TicksAmount.Attributes[]
}

function constructRevenueSharingAmounts(creditedAccountId: number, ticksCredited: number,
                                        ticksAmountType: TicksAmount.Type,
                                        channel: Channel.Instance): CreditedAndSharingTAResult {
  if (!channel || !channel.shareIncome || channel.revenues.length == 0) {
    return {
      ticksCreditedWithoutShared: ticksCredited,
      ticksAmounts: []
    }
  }

  let ticksAmounts: TicksAmount.Attributes[] = [];
  let sumOfSharedAmounts = 0;

  _.forEach(channel.revenues, function (revenue: ChannelRevenue.Instance) {
    if (revenue.isExpired()) {
      return;
    }

    let sharedAmount = ticksCredited / 100 * revenue.percent;
    sumOfSharedAmounts += sharedAmount;

    ticksAmounts.push({
      accountId: revenue.accountId,
      amount: sharedAmount,
      type: ticksAmountType,
      reason: TicksAmount.Reason.REVENUE_SHARE
    });
  });

  if (ticksCredited < sumOfSharedAmounts) {
    throw new Errors.ServerError('Incorrect calculation shared income for channel revenues');
  }

  ticksAmounts.unshift({
    accountId: creditedAccountId,
    amount: -sumOfSharedAmounts,
    type: ticksAmountType,
    reason: TicksAmount.Reason.REVENUE_SHARE
  });

  return {
    ticksCreditedWithoutShared: ticksCredited - sumOfSharedAmounts,
    ticksAmounts: ticksAmounts
  }
}

function updateRevenueSharedAccounts(ticksAmounts: TicksAmount.Attributes[], transaction: Sequelize.Transaction): Promise<any> {
  if (ticksAmounts.length == 0) {
    return Promise.resolve();
  }

  let accountsIds: number[] = _(ticksAmounts)
    .map(ticksAmount => ticksAmount.accountId)
    .uniq()
    .value();

  return Account.Model.findAllByIds(accountsIds)
    .then((accounts: Account.Instance[]) => {

      if (accounts == null || accounts.length != accountsIds.length) {
        throw new Errors.ServerError(
          'Error while updating revenue shared account. Found account count doesnt equal ticksAmount count.'
          + accounts.length + ' and ' + ticksAmounts.length
        );
      }

      _.forEach(ticksAmounts, function (ticksAmount: TicksAmount.Instance) {
        let account = _.findWhere(accounts, {id: ticksAmount.accountId});
        if (!account) {
          throw new Errors.ServerError('Error while updating revenue shared account. Cant find tickAmount for accountId=' + ticksAmount.accountId);
        }
        account.currentBalanceEarnedTicks += ticksAmount.amount;
        account.totalTicksCredited += ticksAmount.amount;
        account.balanceUpdated = new Date();
      });

      let updates: Promise<any>[] = _.map(accounts, function (account: Account.Instance): Promise<Account.Instance> {
        return account.save({transaction: transaction})
      });
      return Promise.all(updates);
    });
}

export interface TwoWayTransactionParams {
  buyerTickserId: number,
  sellerTickserId: number,
  campaignTickserId?: number;
  behalfPayingTickserId?: number;
  contentId?: number,
  feedId?: number,
  donateChannelId?: number,
  amount: number,
  type: TicksTransaction.Type,
  t: Sequelize.Transaction
}

export interface TwoWayTransactionResult {
  ticksTransaction: TicksTransaction.Instance,
  buyerAccount: Account.Instance,
  sellerAccount: Account.Instance,
  campaignAccount?: Account.Instance
}

export async function processTwoWayTransaction(transactionParams: TwoWayTransactionParams): Promise<TwoWayTransactionResult> {

  let findCampaignAccount: Promise<Account.Instance> = transactionParams.campaignTickserId != null
    ? Account.Model.findOneByTickserId(transactionParams.campaignTickserId, {transaction: transactionParams.t})
    : Promise.resolve<Account.Instance>(null);

  let findBehalfPayingAccount: Promise<Account.Instance> = transactionParams.behalfPayingTickserId != null
    ? Account.Model.findOneByTickserId(transactionParams.behalfPayingTickserId, {transaction: transactionParams.t})
    : Promise.resolve<Account.Instance>(null);

  let findChannelByContent = transactionParams.contentId != null
    ? ChannelService.findChannelByContentId(transactionParams.contentId, {transaction: transactionParams.t})
    : Promise.resolve<Channel.Instance>(null);

  let findChannelForDonate = transactionParams.donateChannelId != null
    ? Channel.Model.scope(Channel.Scopes.REVENUES).findById(transactionParams.donateChannelId, {transaction: transactionParams.t})
    : Promise.resolve<Channel.Instance>(null);

  return Promise.all([
    Account.Model.findOneByTickserId(transactionParams.buyerTickserId, {transaction: transactionParams.t}),
    Account.Model.findOneByTickserId(transactionParams.sellerTickserId, {transaction: transactionParams.t}),
    findCampaignAccount,
    findBehalfPayingAccount,
    findChannelByContent,
    findChannelForDonate
  ]).spread<TwoWayTransactionResult>(async function (buyerAccount: Account.Instance,
                                                     sellerAccount: Account.Instance,
                                                     campaignAccount: Account.Instance,
                                                     behalfPayingAccount: Account.Instance,
                                                     contentChannel: Channel.Instance,
                                                     channelForDonate: Channel.Instance) {
    if (!buyerAccount) {
      throw new Errors.NotFound('Account not found');
    }

    if (transactionParams.amount > 0) {
      return processTwoWayTransactionWithNormalAmount(buyerAccount, sellerAccount, campaignAccount, behalfPayingAccount, contentChannel, channelForDonate, transactionParams);
    }

    if (transactionParams.amount < 0) {
      return processTwoWayTransactionWithNegativeAmount(buyerAccount, sellerAccount, campaignAccount, behalfPayingAccount, contentChannel, channelForDonate, transactionParams);
    }

    return processTwoWayTransactionWithZeroAmount(buyerAccount, sellerAccount, campaignAccount, behalfPayingAccount, contentChannel, channelForDonate, transactionParams);
  })
}

async function processTwoWayTransactionWithNormalAmount(buyerAccount: Account.Instance,
                                                        sellerAccount: Account.Instance,
                                                        campaignAccount: Account.Instance,
                                                        behalfPayingAccount: Account.Instance,
                                                        contentChannel: Channel.Instance,
                                                        channelForDonate: Channel.Instance,
                                                        transactionParams: TwoWayTransactionParams): Promise<TwoWayTransactionResult> {

  const isEnoughTicks = checkTicksInAccount(buyerAccount, transactionParams.amount);
  if (!isEnoughTicks) {
    // FIXME: [y.tolmachev] Need more informative error message
    throw new Errors.PaymentRequired();
  }

  const isCanProcess = canProcessToWayTransaction(buyerAccount, sellerAccount, campaignAccount, transactionParams.type);
  if (!isCanProcess) {
    // FIXME: [y.tolmachev] Need more informative error message
    throw new Errors.BadRequest('Cant buy or participate in buying');
  }

  let ticksPayed: number = 0;
  let ticksAmounts: TicksAmount.Attributes[] = [];
  let campaignCommissionTicks: number = 0;
  let channel: Channel.Instance = (channelForDonate != null) ? channelForDonate : contentChannel;

  // process free ticks if buyer has any
  if (buyerAccount.currentBalanceFreeTicks != 0) {

    let ticksDebited: number = buyerAccount.currentBalanceFreeTicks >= transactionParams.amount ? transactionParams.amount : buyerAccount.currentBalanceFreeTicks;
    let adminCost = ticksDebited * config.global.adminCostFreeTicksPercent;
    let ticksCampaign = campaignAccount ? ticksDebited * config.global.campaignFreeTicksPercent : 0;
    let ticksCredited = ticksDebited - adminCost - ticksCampaign;
    ticksPayed = ticksDebited;

    let creditedAndSharingResult = constructRevenueSharingAmounts(
      sellerAccount.id, ticksCredited, TicksAmount.Type.FREE, channel);

    buyerAccount.currentBalanceFreeTicks -= ticksDebited;
    buyerAccount.totalTicksDebited += ticksDebited;

    sellerAccount.currentBalanceEarnedTicks += creditedAndSharingResult.ticksCreditedWithoutShared;
    sellerAccount.totalTicksCredited += ticksCredited;
    sellerAccount.totalTicksDebited += ticksCredited - creditedAndSharingResult.ticksCreditedWithoutShared;

    ticksAmounts.push({
      accountId: buyerAccount.id,
      amount: -1 * ticksDebited,
      type: TicksAmount.Type.FREE,
      reason: TicksAmount.Reason.DEFAULT
    });

    if (behalfPayingAccount) {
      // this record means that recipient gets moneys transferred to him
      ticksAmounts.push({
        accountId: behalfPayingAccount.id,
        amount: ticksDebited,
        type: TicksAmount.Type.FREE,
        reason: TicksAmount.Reason.BEHALF_PAYING
      });
      // this record means that recipient spends money to the channel
      ticksAmounts.push({
        accountId: behalfPayingAccount.id,
        amount: -1 * ticksDebited,
        type: TicksAmount.Type.FREE,
        reason: TicksAmount.Reason.BEHALF_PAYING
      });
    }

    ticksAmounts.push({
      accountId: Account.SystemAccount.FREE_TICKS_ACCOUNT,
      amount: adminCost,
      type: TicksAmount.Type.FREE,
      reason: TicksAmount.Reason.DEFAULT
    });
    ticksAmounts.push({
      accountId: sellerAccount.id,
      amount: ticksCredited,
      type: TicksAmount.Type.FREE,
      reason: TicksAmount.Reason.DEFAULT
    });

    if (campaignAccount) {
      campaignAccount.currentBalanceEarnedTicks += ticksCampaign;
      campaignAccount.totalTicksCredited += ticksCampaign;
      campaignCommissionTicks += ticksCampaign;

      ticksAmounts.push({
        accountId: campaignAccount.id,
        amount: ticksCampaign,
        type: TicksAmount.Type.FREE,
        reason: TicksAmount.Reason.DEFAULT
      });
    }

    ticksAmounts = ticksAmounts.concat(creditedAndSharingResult.ticksAmounts);
  }

  // process rest of the amount if free ticks did not pay for all
  if (ticksPayed < transactionParams.amount) {
    const ticksDebitedAfterFree = transactionParams.amount - ticksPayed;

    const needVAT: boolean = transactionParams.type == TicksTransaction.Type.ACCESS;
    const vatResult: VatCalculationResult = await calculateVATByTickserCountry(needVAT, ticksDebitedAfterFree, sellerAccount.tickserId);

    const ticksDebited = ticksDebitedAfterFree - vatResult.amount;
    const adminCost = ticksDebited * config.global.adminCostPercent;
    const ticksCampaign = campaignAccount ? ticksDebited * config.global.campaignTicksPercent : 0;
    const ticksCredited = ticksDebited - adminCost - ticksCampaign;

    const creditedAndSharingResult = constructRevenueSharingAmounts(
      sellerAccount.id, ticksCredited, TicksAmount.Type.PAID, channel);

    buyerAccount.currentBalance -= ticksDebitedAfterFree;
    buyerAccount.totalTicksDebited += ticksDebitedAfterFree;

    sellerAccount.currentBalanceEarnedTicks += creditedAndSharingResult.ticksCreditedWithoutShared;
    sellerAccount.totalTicksCredited += ticksCredited;
    sellerAccount.totalTicksDebited += ticksCredited - creditedAndSharingResult.ticksCreditedWithoutShared;

    ticksAmounts.push({
      accountId: buyerAccount.id,
      amount: -1 * ticksDebited,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.DEFAULT
    });

    if (needVAT) {
      // VAT is paid by buyer
      ticksAmounts.push({
        accountId: buyerAccount.id,
        amount: -1 * vatResult.amount,
        type: TicksAmount.Type.PAID,
        reason: TicksAmount.Reason.VAT,
        vatCountryId: vatResult.vatCountryId,
        vat: vatResult.vat,
      });

      ticksAmounts.push({
        accountId: Account.SystemAccount.VAT_GLOBAL,
        amount: vatResult.amount,
        type: TicksAmount.Type.PAID,
        reason: TicksAmount.Reason.VAT,
        vatCountryId: vatResult.vatCountryId,
        vat: vatResult.vat,
      });
    }

    if (behalfPayingAccount) {
      // this record means that recipient gets moneys transferred to him
      ticksAmounts.push({
        accountId: behalfPayingAccount.id,
        amount: ticksDebited,
        type: TicksAmount.Type.PAID,
        reason: TicksAmount.Reason.BEHALF_PAYING
      });
      // this record means that recipient spends money to the channel
      ticksAmounts.push({
        accountId: behalfPayingAccount.id,
        amount: -1 * ticksDebited,
        type: TicksAmount.Type.PAID,
        reason: TicksAmount.Reason.BEHALF_PAYING
      });
    }

    ticksAmounts.push({
      accountId: Account.SystemAccount.GENERAL_ACCOUNT,
      amount: adminCost,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.DEFAULT
    });
    ticksAmounts.push({
      accountId: sellerAccount.id,
      amount: ticksCredited,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.DEFAULT
    });

    if (campaignAccount) {
      campaignAccount.currentBalanceEarnedTicks += ticksCampaign;
      campaignAccount.totalTicksCredited += ticksCampaign;
      campaignAccount.balanceUpdated = new Date();
      campaignCommissionTicks += ticksCampaign;

      ticksAmounts.push({
        accountId: campaignAccount.id,
        amount: ticksCampaign,
        type: TicksAmount.Type.PAID,
        reason: TicksAmount.Reason.DEFAULT
      });

    }

    ticksAmounts = ticksAmounts.concat(creditedAndSharingResult.ticksAmounts);
  }

  buyerAccount.balanceUpdated = sellerAccount.balanceUpdated = new Date();

  let ticksTransaction = {
    contentId: transactionParams.contentId,
    feedId: transactionParams.feedId,
    channelId: transactionParams.donateChannelId,
    type: transactionParams.type,
    amount: transactionParams.amount,
    status: TicksTransaction.Status.NEW
  };

  let saveCampaignAccountAndCreateNotification = Promise.resolve<Account.Instance>(null);
  if (campaignAccount) {
    let saveCampaignAccount = campaignAccount.save({transaction: transactionParams.t});
    let createNotification = NotificationService.createForGrantCommissionTicks(campaignAccount.tickserId, campaignCommissionTicks, transactionParams.t);
    saveCampaignAccountAndCreateNotification = Promise.all([saveCampaignAccount, createNotification])
      .spread<Account.Instance>(function (campaignAccount: Account.Instance) {
        return campaignAccount;
      });
  }

  let rankedTickserId = behalfPayingAccount ? behalfPayingAccount.tickserId : transactionParams.buyerTickserId;
  let updateChannelRank = (channel != null && channel.showRankedList)
    ? TickserChannelRankService.updateRank(rankedTickserId, channel.id, transactionParams.amount, transactionParams.t)
    : Promise.resolve();

  return Promise.all([
    createTicksTransaction(ticksTransaction, ticksAmounts, transactionParams.t),
    buyerAccount.save({transaction: transactionParams.t}),
    sellerAccount.save({transaction: transactionParams.t}),
    saveCampaignAccountAndCreateNotification,
    updateChannelRank
  ]).spread<TwoWayTransactionResult>(function (ticksTransaction: TicksTransaction.Instance,
                                               buyerAccount: Account.Instance,
                                               sellerAccount: Account.Instance,
                                               campaignAccount: Account.Instance,
                                               updatedRank: TickserChannelRank.Instance) {

    let amountsAboutSharing: TicksAmount.Attributes[] = _.filter(ticksAmounts,
      tickAmount => tickAmount.reason == TicksAmount.Reason.REVENUE_SHARE && tickAmount.amount > 0);

    return updateRevenueSharedAccounts(amountsAboutSharing, transactionParams.t).then(function () {
      return Promise.resolve({
        ticksTransaction: ticksTransaction,
        buyerAccount: buyerAccount,
        sellerAccount: sellerAccount,
        campaignAccount: campaignAccount
      });
    });
  });
}

function processTwoWayTransactionWithZeroAmount(buyerAccount: Account.Instance,
                                                sellerAccount: Account.Instance,
                                                campaignAccount: Account.Instance,
                                                behalfPayingAccount: Account.Instance,
                                                contentChannel: Channel.Instance,
                                                channelForDonate: Channel.Instance,
                                                transactionParams: TwoWayTransactionParams): Promise<TwoWayTransactionResult> {

  let ticksAmounts: TicksAmount.Attributes[] = [];
  let channel: Channel.Instance = (channelForDonate != null) ? channelForDonate : contentChannel;

  const creditedAndSharingResult = constructRevenueSharingAmounts(sellerAccount.id, 0, TicksAmount.Type.PAID, channel);

  ticksAmounts.push({
    accountId: buyerAccount.id,
    amount: 0,
    type: TicksAmount.Type.PAID,
    reason: TicksAmount.Reason.DEFAULT
  });

  if (behalfPayingAccount) {
    ticksAmounts.push({
      accountId: behalfPayingAccount.id,
      amount: 0,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.BEHALF_PAYING
    });
    ticksAmounts.push({
      accountId: behalfPayingAccount.id,
      amount: 0,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.BEHALF_PAYING
    });
  }

  ticksAmounts.push({
    accountId: Account.SystemAccount.GENERAL_ACCOUNT,
    amount: 0,
    type: TicksAmount.Type.PAID,
    reason: TicksAmount.Reason.DEFAULT
  });
  ticksAmounts.push({
    accountId: sellerAccount.id,
    amount: 0,
    type: TicksAmount.Type.PAID,
    reason: TicksAmount.Reason.DEFAULT
  });

  if (campaignAccount) {
    campaignAccount.balanceUpdated = new Date();

    ticksAmounts.push({
      accountId: campaignAccount.id,
      amount: 0,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.DEFAULT
    });
  }

  ticksAmounts = ticksAmounts.concat(creditedAndSharingResult.ticksAmounts);

  buyerAccount.balanceUpdated = sellerAccount.balanceUpdated = new Date();

  let ticksTransaction = {
    contentId: transactionParams.contentId,
    feedId: transactionParams.feedId,
    channelId: transactionParams.donateChannelId,
    type: transactionParams.type,
    amount: transactionParams.amount,
    status: TicksTransaction.Status.NEW
  };

  let saveCampaignAccountAndCreateNotification = Promise.resolve<Account.Instance>(null);
  if (campaignAccount) {
    let saveCampaignAccount = campaignAccount.save({transaction: transactionParams.t});
    let createNotification = NotificationService.createForGrantCommissionTicks(campaignAccount.tickserId, 0, transactionParams.t);
    saveCampaignAccountAndCreateNotification = Promise.all([saveCampaignAccount, createNotification])
      .spread<Account.Instance>(function (campaignAccount: Account.Instance) {
        return campaignAccount;
      });
  }

  let rankedTickserId = behalfPayingAccount ? behalfPayingAccount.tickserId : transactionParams.buyerTickserId;
  let updateChannelRank = (channel != null && channel.showRankedList)
    ? TickserChannelRankService.updateRank(rankedTickserId, channel.id, transactionParams.amount, transactionParams.t)
    : Promise.resolve();

  return Promise.all([
    createTicksTransaction(ticksTransaction, ticksAmounts, transactionParams.t),
    buyerAccount.save({transaction: transactionParams.t}),
    sellerAccount.save({transaction: transactionParams.t}),
    saveCampaignAccountAndCreateNotification,
    updateChannelRank
  ]).spread<TwoWayTransactionResult>(function (ticksTransaction: TicksTransaction.Instance,
                                               buyerAccount: Account.Instance,
                                               sellerAccount: Account.Instance,
                                               campaignAccount: Account.Instance,
                                               updatedRank: TickserChannelRank.Instance) {

    const amountsAboutSharing: TicksAmount.Attributes[] = _.filter(ticksAmounts,
      tickAmount => tickAmount.reason == TicksAmount.Reason.REVENUE_SHARE && tickAmount.amount == 0);

    return updateRevenueSharedAccounts(amountsAboutSharing, transactionParams.t).then(function () {
      return Promise.resolve({
        ticksTransaction: ticksTransaction,
        buyerAccount: buyerAccount,
        sellerAccount: sellerAccount,
        campaignAccount: campaignAccount
      });
    });
  });
}

function processTwoWayTransactionWithNegativeAmount(buyerAccount: Account.Instance,
                                                    sellerAccount: Account.Instance,
                                                    campaignAccount: Account.Instance,
                                                    behalfPayingAccount: Account.Instance,
                                                    contentChannel: Channel.Instance,
                                                    channelForDonate: Channel.Instance,
                                                    transactionParams: TwoWayTransactionParams): Promise<TwoWayTransactionResult> {

  const amount = Math.abs(transactionParams.amount);
  const isEnoughTicks = checkTicksInAccount(sellerAccount, amount);
  const isCanProcess = canProcessToWayTransaction(sellerAccount, buyerAccount, campaignAccount, transactionParams.type);

  if (!isEnoughTicks) {
    // FIXME: [y.tolmachev] Need more informative error message
    throw new Errors.PaymentRequired();
  }

  if (!isCanProcess) {
    // FIXME: [y.tolmachev] Need more informative error message
    throw new Errors.BadRequest('Cant buy or participate in buying');
  }

  let ticksPayed: number = 0;
  let ticksAmounts: TicksAmount.Attributes[] = [];
  let campaignCommissionTicks: number = 0;
  let channel: Channel.Instance = (channelForDonate != null) ? channelForDonate : contentChannel;

  // process free ticks if debited user has any
  if (sellerAccount.currentBalanceFreeTicks > 0) {

    let ticksDebited: number = sellerAccount.currentBalanceFreeTicks >= amount ? amount : sellerAccount.currentBalanceFreeTicks;
    let adminCost = ticksDebited * config.global.adminCostFreeTicksPercent;
    let ticksCampaign = campaignAccount ? ticksDebited * config.global.campaignFreeTicksPercent : 0;
    let ticksCredited = ticksDebited - adminCost - ticksCampaign;
    ticksPayed = ticksDebited;

    sellerAccount.currentBalanceFreeTicks -= ticksDebited;
    sellerAccount.totalTicksDebited += ticksDebited;

    buyerAccount.currentBalanceEarnedTicks += ticksCredited;
    buyerAccount.totalTicksCredited += ticksCredited;

    ticksAmounts.push({
      accountId: sellerAccount.id,
      amount: -1 * ticksDebited,
      type: TicksAmount.Type.FREE,
      reason: TicksAmount.Reason.DEFAULT
    });

    if (behalfPayingAccount) {
      ticksAmounts.push({
        accountId: behalfPayingAccount.id,
        amount: ticksDebited,
        type: TicksAmount.Type.FREE,
        reason: TicksAmount.Reason.BEHALF_PAYING
      });
      ticksAmounts.push({
        accountId: behalfPayingAccount.id,
        amount: -1 * ticksDebited,
        type: TicksAmount.Type.FREE,
        reason: TicksAmount.Reason.BEHALF_PAYING
      });
    }

    ticksAmounts.push({
      accountId: Account.SystemAccount.FREE_TICKS_ACCOUNT,
      amount: adminCost,
      type: TicksAmount.Type.FREE,
      reason: TicksAmount.Reason.DEFAULT
    });
    ticksAmounts.push({
      accountId: buyerAccount.id,
      amount: ticksCredited,
      type: TicksAmount.Type.FREE,
      reason: TicksAmount.Reason.DEFAULT
    });

    if (campaignAccount) {
      campaignAccount.currentBalanceEarnedTicks += ticksCampaign;
      campaignAccount.totalTicksCredited += ticksCampaign;
      campaignCommissionTicks += ticksCampaign;

      ticksAmounts.push({
        accountId: campaignAccount.id,
        amount: ticksCampaign,
        type: TicksAmount.Type.FREE,
        reason: TicksAmount.Reason.DEFAULT
      });
    }
  }

  // process rest of the amount if free ticks did not pay for all
  if (ticksPayed < amount) {

    let ticksDebited = amount - ticksPayed;
    let adminCost = ticksDebited * config.global.adminCostPercent;
    let ticksCampaign = campaignAccount ? ticksDebited * config.global.campaignTicksPercent : 0;
    let ticksCredited = ticksDebited - adminCost - ticksCampaign;

    sellerAccount.currentBalance -= ticksDebited;
    sellerAccount.totalTicksDebited += ticksDebited;

    buyerAccount.currentBalanceEarnedTicks += ticksCredited;
    buyerAccount.totalTicksCredited += ticksCredited;

    ticksAmounts.push({
      accountId: sellerAccount.id,
      amount: -1 * ticksDebited,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.DEFAULT
    });

    if (behalfPayingAccount) {
      ticksAmounts.push({
        accountId: behalfPayingAccount.id,
        amount: ticksDebited,
        type: TicksAmount.Type.PAID,
        reason: TicksAmount.Reason.BEHALF_PAYING
      });
      ticksAmounts.push({
        accountId: behalfPayingAccount.id,
        amount: -1 * ticksDebited,
        type: TicksAmount.Type.PAID,
        reason: TicksAmount.Reason.BEHALF_PAYING
      });
    }

    ticksAmounts.push({
      accountId: Account.SystemAccount.GENERAL_ACCOUNT,
      amount: adminCost,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.DEFAULT
    });
    ticksAmounts.push({
      accountId: buyerAccount.id,
      amount: ticksCredited,
      type: TicksAmount.Type.PAID,
      reason: TicksAmount.Reason.DEFAULT
    });

    if (campaignAccount) {
      campaignAccount.currentBalanceEarnedTicks += ticksCampaign;
      campaignAccount.totalTicksCredited += ticksCampaign;
      campaignAccount.balanceUpdated = new Date();
      campaignCommissionTicks += ticksCampaign;

      ticksAmounts.push({
        accountId: campaignAccount.id,
        amount: ticksCampaign,
        type: TicksAmount.Type.PAID,
        reason: TicksAmount.Reason.DEFAULT
      });
    }
  }

  sellerAccount.balanceUpdated = buyerAccount.balanceUpdated = new Date();

  let ticksTransaction = {
    contentId: transactionParams.contentId,
    feedId: transactionParams.feedId,
    channelId: transactionParams.donateChannelId,
    type: transactionParams.type,
    amount: transactionParams.amount,
    status: TicksTransaction.Status.NEW
  };

  let saveCampaignAccountAndCreateNotification = Promise.resolve<Account.Instance>(null);
  if (campaignAccount) {
    let saveCampaignAccount = campaignAccount.save({transaction: transactionParams.t});
    let createNotification = NotificationService.createForGrantCommissionTicks(campaignAccount.tickserId, campaignCommissionTicks, transactionParams.t);
    saveCampaignAccountAndCreateNotification = Promise.all([saveCampaignAccount, createNotification])
      .spread<Account.Instance>(function (campaignAccount: Account.Instance) {
        return campaignAccount;
      });
  }

  let rankedTickserId = behalfPayingAccount ? behalfPayingAccount.tickserId : transactionParams.buyerTickserId;

  return Promise.all([
    createTicksTransaction(ticksTransaction, ticksAmounts, transactionParams.t),
    sellerAccount.save({transaction: transactionParams.t}),
    buyerAccount.save({transaction: transactionParams.t}),
    saveCampaignAccountAndCreateNotification
  ]).spread<TwoWayTransactionResult>(function (ticksTransaction: TicksTransaction.Instance,
                                               buyerAccount: Account.Instance,
                                               sellerAccount: Account.Instance,
                                               campaignAccount: Account.Instance) {
    return Promise.resolve({
      ticksTransaction: ticksTransaction,
      buyerAccount: buyerAccount,
      sellerAccount: sellerAccount,
      campaignAccount: campaignAccount
    });
  });
}

function createRefundTransactionAndUpdateAccounts(paymentTransaction: TicksTransaction.Instance, t: Sequelize.Transaction): Promise<TicksTransaction.Instance> {
  const accountIds: number[] = _(paymentTransaction.ticksAmounts)
    .map(tickAmount => tickAmount.accountId)
    .filter(accountId => accountId > 0)
    .value();

  return Account.Model.findAllByIds(accountIds, {transaction: t})
    .then(function (accounts: Account.Instance[]) {
      const ticksAmountsForRefund: TicksAmount.Attributes[] = [];

      _.forEach(paymentTransaction.ticksAmounts, function (ticksAmount: TicksAmount.Instance) {
        ticksAmountsForRefund.push({
          accountId: ticksAmount.accountId,
          amount: -1 * ticksAmount.amount,
          type: ticksAmount.type,
          reason: ticksAmount.reason
        });

        if (ticksAmount.accountId > 0) {
          const account: Account.Instance = _.findWhere(accounts, {id: ticksAmount.accountId});
          if (account == null) {
            throw new Errors.ServerError('Account not found while refund for id:' + ticksAmount.accountId);
          }
          AccountService.updateBalanceAfterRefund(account, ticksAmount);
        }
      });
      const updateAccountPromises = _.map(accounts, account => account.save({transaction: t}));

      return Promise.all(updateAccountPromises).then(function () {
        let refundTransaction: TicksTransaction.Attributes = {
          contentId: paymentTransaction.contentId,
          amount: paymentTransaction.amount,
          type: TicksTransaction.Type.REFUND,
          status: TicksTransaction.Status.NEW
        };
        return createTicksTransaction(refundTransaction, ticksAmountsForRefund, t);
      });
    });
}

interface VatCalculationResult {
  vatCountryId: number;
  vat: number;
  amount: number;
}

async function calculateVATByTickserCountry(needVAT: boolean, amount: number, tickserId: number): Promise<VatCalculationResult> {
  if (!needVAT) {
    return {
      vat: null,
      vatCountryId: null,
      amount: 0,
    }
  }

  const tickser: Tickser.Instance = await Tickser.Model.findById(tickserId);
  if (tickser.countryId == null) {
    return {
      vat: null,
      vatCountryId: null,
      amount: 0,
    };
  }

  const country: Country.Instance = await Country.Model.findById(tickser.countryId);
  const resultAmount = calculateVAT(amount, country.VAT);
  return {
    vat: country.VAT,
    vatCountryId: country.id,
    amount: resultAmount,
  };
}

export function calculateVAT(amount: number, vat: number): number {
  return amount - Math.round((amount / (1 + vat)) * 100) / 100;
}

function checkTicksInAccount(account, amount) {
  return account.currentBalance + account.currentBalanceFreeTicks >= amount;
}

function createTicksTransaction(ticksTransaction: TicksTransaction.Attributes,
                                ticksAmounts: TicksAmount.Attributes[],
                                t: Sequelize.Transaction): Promise<TicksTransaction.Instance> {

  return TicksTransaction.Model.create(ticksTransaction, {transaction: t}).then(function (transaction: TicksTransaction.Instance) {
    let createTicksAmounts: Promise<any>[] =
      _.map(ticksAmounts, function (tickAmount: TicksAmount.Attributes) {
        return transaction.createTicksAmount(tickAmount, {transaction: t});
      });
    return Promise.all(createTicksAmounts).then(function (ticksAmounts: TicksAmount.Instance[]) {
      transaction.ticksAmounts = ticksAmounts;
      return transaction;
    });
  });
}

function canProcessToWayTransaction(fromAccount: Account.Instance,
                                    toAccount: Account.Instance,
                                    campaignAccount: Account.Instance,
                                    type: TicksTransaction.Type): boolean {
  // can donate any channel (and your own)
  if (type == TicksTransaction.Type.CHANNEL_DONATE) {
    return true;
  }

  // can like/dislike your own content or feed
  if (type == TicksTransaction.Type.LIKE || type == TicksTransaction.Type.DISLIKE) {
    return true;
  }

  return campaignAccount != null
    ? fromAccount.id != toAccount.id && fromAccount.id != campaignAccount.id && toAccount.id != campaignAccount.id
    : fromAccount.id != toAccount.id;
}