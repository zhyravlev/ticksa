/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import _ = require('lodash');
import moment = require('moment');
import Promise = require("bluebird");
import Sequelize = require('sequelize');

import Models = require('../models/models');
import TicksTransaction = Models.TicksTransaction;
import ContentAccess = Models.ContentAccess;
import Content = Models.Content;
import Tickser = Models.Tickser;
import Channel = Models.Channel;
import TickserChannel = Models.TickserChannel;

import conn = require('../connection');

export interface Permission {
  isManager: boolean;
  hasAccess: boolean;
}

export interface PermissionMap {
  [contentId: number]: Permission;
}

export function getContentPermissionMap(accountId: number,
                                        contents: Content.Instance[],
                                        tickserChannels: TickserChannel.Instance[]
): Promise<PermissionMap> {

  const permissionMap: PermissionMap = {};

  _.forEach<any>(contents, content => {
    var isManager = _.some(tickserChannels, {channelId: content.channelId});
    permissionMap[content.id] = {isManager: isManager, hasAccess: false};
  });

  const contentIds = Object.keys(permissionMap);

  return ContentAccess.Model.findAllByAccountIdAndContentIds(accountId, contentIds).then(function (accessList) {
    _.forEach<any>(accessList, function (contentAccess: ContentAccess.Instance) {
      permissionMap[contentAccess.contentId].hasAccess = contentAccess.hasAccess;
    });
    return permissionMap;
  });
}

export function updateContentAccess(accountId: number, contentId: number, hasAccess: boolean, t: Sequelize.Transaction) {
  return ContentAccess.Model.countByAccountIdAndContentId(accountId, contentId).then(function (count) {
    if (count == 1) {
      return ContentAccess.Model.update({
          hasAccess: hasAccess,
        }, {
          where: {
            accountId: accountId,
            contentId: contentId,
            hasAccess: {$ne: hasAccess}
          },
          transaction: t,
        }
      )
        .then(function (updated) {
          if (!updated || updated[0] == 0) {
            return Promise.reject({status: 409, message: 'Conflict update'})
          }
        })
        .catch(function (err) {
          logger.warn('ContentAccess updating error! accountId: ' + accountId + ', contentId: ' + contentId);
          return Promise.reject(err);
        })
    } else {
      return ContentAccess.Model.create({
        accountId: accountId,
        contentId: contentId,
        hasAccess: true
      }, {transaction: t}).catch(function (err) {
        logger.warn('ContentAccess creation error! accountId: ' + accountId + ', contentId: ' + contentId);
        return Promise.reject(err);
      });
    }
  });
}

