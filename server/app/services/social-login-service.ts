/*
 * According to
 * https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow?locale=en_US#exchangecode
 * when application interacts with FB API it should pass initial redirect_url that was defined on login request.
 *
 * Functions of this module construct redirect_url explicitly.
 *
 * Server handlers are described in /controllers/social-login-controller.ts
 */

export function facebookLoginRedirectUriForContentTeaser(referrerId: any, contentId: any): string {
  const template: string = '/social/login/facebook/ref-:referrerId-content-:contentId';
  return template.replace(':referrerId', referrerId).replace(':contentId', contentId);
}

export function facebookLoginRedirectUriForContentList(): string {
  return '/social/login/facebook/content-list';
}

export function facebookLoginRedirectUriForContentAdd(contentType: any): string {
  return '/social/login/facebook/content-add-type-' + contentType;
}