/// <reference path="../typings/tsd.d.ts"/>
import {NotFound} from '../errors';
const logger = require('../logger/logger')(__filename);

import q = require('q');
import _ = require('lodash');
import config = require('../config');

import Models = require('../models/models');
import CashIO = Models.CashIO;
import PayPalCashIn = Models.PayPalCashIn;
import Currency = Models.Currency;
import PayPalReturn = Models.PayPalReturn;
import {ServerError} from '../errors';
import {BadRequest} from '../errors';

var conn = require('../connection');
const paypal = require('paypal-rest-sdk');

var payPalConfig: any = {};

init();

export function init() {
  payPalConfig = config.env().finance.payPal;
  paypal.configure(payPalConfig.api);
}


export function initializePayPalCashIn(cashIoId) {
  var def = q.defer();
  pendingPayPalPayment(cashIoId).then(function (pendingSuccess: any) {
    if (pendingSuccess.type === "alreadyUrl") {
      def.resolve({url: pendingSuccess.alreadyUrl});
      return;
    }

    registerPaymentIntoPayPal(pendingSuccess.pendingPayPalCashIn).then(function (registrationSuccess:any) {
      def.resolve({url: registrationSuccess.payPalUrl});
    }, function (registrationCancel) {
      if (registrationCancel.type === "fail") {
        def.reject({err: registrationCancel.msg});
      } else {
        def.reject({err: registrationCancel.err});
      }
    });

  }, function (pendingCancel) {
    if (pendingCancel.type === "fail") {
      def.reject({err: pendingCancel.msg});
    } else {
      def.reject({err: pendingCancel.err});
    }
  });
  return def.promise;
}

export function completePayPalCashIn(params): q.Promise<number> {
  var def = q.defer<number>();

  logReturnAndGetPayPalCashIn(params, PayPalReturn.Type.COMPLETE).then(function (data: any) {
    executePayPalPayment(data.payPalCashIn, data.payPalReturn, params).then(function (data: {cashIOId: number}) {
      def.resolve(data.cashIOId);
    }, function (err) {
      def.reject(err);
    });
  }, function (err) {
    def.reject(err);
  });

  return def.promise;
}

export function cancelPayPalCashIn(payment) {
  var def = q.defer();
  logReturnAndGetPayPalCashIn(payment, PayPalReturn.Type.CANCEL).then(function (data: any) {
    var payPalCashIn = data.payPalCashIn;

    if (payPalCashIn.status == PayPalCashIn.PayPalStatus.APPROVED_BUT_NOT_COMPLETED || payPalCashIn.status == PayPalCashIn.PayPalStatus.COMPLETED) {
      logger.error('Trying to cancel already completed PayPal payment. p: ' + payment);
      def.reject({err: 'Invalid payment status'});
      return;
    }

    payPalCashIn.status = PayPalCashIn.PayPalStatus.CANCELED;

    payPalCashIn.save().then(function (updated) {
      def.resolve();
    }, function (err) {
      def.reject(err);
    });
  }, function (err) {
    def.reject(err);
  });

  return def.promise;
}


function pendingPayPalPayment(cashIoId) {
  var def = q.defer();
  conn.transaction(function (t) {
    CashIO.Model.findOne({
        where: {
          id: cashIoId
        },
        include: [
          {
            required: false,
            model: PayPalCashIn.Model,
            as: 'payPalCashIn'
          },
          {
            required: true,
            model: Currency.Model,
            as: 'userCurrency'
          }
        ],
        transaction: t
      }
    ).then(function (cashIo: CashIO.Instance) {
      if (cashIo == null) {
        logger.error('Cant intialize PayPal cashin - no cashIo with id ' + cashIoId);
        def.reject({type: "fail", msg: 'Cant find cashIO with id ' + cashIoId});
        return;
      }

      if (cashIo.type !== CashIO.Type.CASH_IN) {
        logger.error('Cant initialize PayPal cashin - cashIo with id ' + cashIoId + ' has type ' + cashIo.type);
        def.reject({type: "fail", msg: 'CashIO with id ' + cashIoId + ' has invalid type'});
        return;
      }

      if (cashIo.status !== CashIO.Status.PENDING) {
        if (cashIo.payPalCashIn) {
          logger.info('Cant reintialize PayPal cashin - cashIo with id ' + cashIoId + ' is already successfully inialized');
          def.resolve({type: "alreadyUrl", alreadyUrl: cashIo.payPalCashIn.url});
        } else {
          logger.error('Cant reintialize PayPal cashin - cashIo with id ' + cashIoId + ' is already initialized with errors');
          def.reject({type: "fail", msg: 'CashIO with id ' + cashIoId + ' has no payPalCashIn'});
        }
        return;
      }

      PayPalCashIn.Model.create({
        status: PayPalCashIn.PayPalStatus.PENDING,
        cashIoId: cashIo.id,
        createdDate: new Date()
      }, {transaction: t}).then(function (pendingPayPalCashIn: PayPalCashIn.Instance) {
        pendingPayPalCashIn.cashIo = cashIo;
        def.resolve({type: "pending", pendingPayPalCashIn: pendingPayPalCashIn});
      }, function (err) {
        def.reject({type: "err", err: err});
      });

    }, function (err) {
      def.reject({type: "err", err: JSON.stringify(err)});
    });
    return def.promise;
  });
  return def.promise;
}

function registerPaymentIntoPayPal(payPalCashIn: PayPalCashIn.Instance) {
  let cancelUrl;
  let returnUrl;

  if (config.isLocal() || config.isTest()) {
    cancelUrl = config.env().server.url + "/api/paypal/cancel/" + payPalCashIn.id;
    returnUrl = config.env().server.url + "/api/paypal/complete/" + payPalCashIn.id;
  } else {
    cancelUrl = config.env().finance.payPal.paymentServerUrl + "/paypal/cancel/" + payPalCashIn.id;
    returnUrl = config.env().finance.payPal.paymentServerUrl + "/paypal/complete/" + payPalCashIn.id;
  }

  var payPalPayment = {
    "intent": "sale",
    "payer": {
      "payment_method": "paypal"
    },
    "redirect_urls": {
      "return_url": returnUrl,
      "cancel_url": cancelUrl
    },
    "transactions": [{
      "amount": {
        "total": payPalCashIn.cashIo.userCurrencyAmount,
        "currency": payPalCashIn.cashIo.userCurrency.iso4217Code
      },
      "item_list": {
        "items": [
          {
            "quantity": "1",
            "name": "Ticksa payment",
            "price": payPalCashIn.cashIo.userCurrencyAmount,
            "currency": payPalCashIn.cashIo.userCurrency.iso4217Code,
            "description": "Money amount will be transformed into ticks"
          }
        ]
      },
      "description": "Payment for Ticksa. " + payPalCashIn.cashIo.userCurrencyAmount + ' ' + payPalCashIn.cashIo.userCurrency.iso4217Code,
      "custom": "ID." + payPalCashIn.id
    }]
  };

  var def = q.defer();
  paypal.payment.create(payPalPayment, {}, function (err, resp) {
    if (err) {
      logger.error('Payment API call failed for registration. payPalCashIn id=' + payPalCashIn.id + '. ' + err);

      logRegistrationError(payPalCashIn, JSON.stringify(payPalPayment), JSON.stringify(err)).then(function (data) {
        def.reject({type: "fail", msg: 'Payment API call failed'});
      }, function (err) {
        def.reject({type: "fail", msg: 'Payment API call failed'});
      });

      return;
    }

    if (!resp) {
      logger.error('Invalid payment API registration call result. payPalCashIn id=' + payPalCashIn.id);

      logRegistrationError(payPalCashIn, JSON.stringify(payPalPayment), "NO RESPONSE").then(function (data) {
        def.reject({type: "fail", msg: 'Invalid payment API call result'});
      }, function (err) {
        def.reject({type: "fail", msg: 'Invalid payment API call result'});
      });

      return;
    }

    var url: any = _.find(resp.links, 'rel', 'approval_url');
    if (!url) {
      logger.error('Invalid payment API registration response - no approval_url. payPalCashIn id=' + payPalCashIn.id);

      logRegistrationError(payPalCashIn, JSON.stringify(payPalPayment), "NO approval_url IN " + JSON.stringify(resp)).then(function (data) {
        def.reject({type: "fail", msg: 'Invalid payment API response'});
      }, function (err) {
        def.reject({type: "fail", msg: 'Invalid payment API response'});
      });

      return;
    }

    var defTransaction = q.defer();
    conn.transaction(function (t) {
      payPalCashIn.status = PayPalCashIn.PayPalStatus.REGISTERED;
      payPalCashIn.payPalId = resp.id;
      payPalCashIn.url = url.href;
      payPalCashIn.initRequest = JSON.stringify(payPalPayment);
      payPalCashIn.initResponse = JSON.stringify(resp);

      payPalCashIn.cashIo.status = CashIO.Status.STARTED;

      q.spread([
        payPalCashIn.save({transaction: t}),
        payPalCashIn.cashIo.save({transaction: t})
      ], function (updatedPayPalCashIn:any, updatedCashIo:any) {
        defTransaction.resolve(updatedPayPalCashIn.url);
      }, function (err) {
        defTransaction.reject(err);
      });

      return defTransaction.promise;
    });

    defTransaction.promise.then(function (data) {
      def.resolve({payPalUrl: data});
    }, function (err) {
      def.reject(err);
    });
  });

  return def.promise;
}

function executePayPalPayment(payPalCashIn, payPalReturn, params): Promise<{cashIOId: number}> {

  var defTransaction = q.defer();
  return conn.transaction(function (t) {

    if (payPalCashIn.payPalId != params.paymentId) {
      logger.error('Unequal PayPal payment id while executing. In DB: ' + payPalCashIn.payPalId + '. In request: ' + params.paymentId + '. id: ' + payPalCashIn.id);
      logCompletionError(payPalReturn, 'Unequal PayPal payment ids');
      defTransaction.reject({err: 'Unequal PayPal payment ids'});
    }

    else if (payPalCashIn.status == PayPalCashIn.PayPalStatus.APPROVED_BUT_NOT_COMPLETED || payPalCashIn.status == PayPalCashIn.PayPalStatus.COMPLETED) {
      logger.info('PayPalCashIn completion repeatedly called for id: ' + payPalCashIn.id + ', status: ' + payPalCashIn.status);
      logCompletionError(payPalReturn, 'Repeatedly called');
      defTransaction.reject({err: 'Repeatedly called'});
    }

    else if (payPalCashIn.status != PayPalCashIn.PayPalStatus.REGISTERED && payPalCashIn.status != PayPalCashIn.PayPalStatus.CANCELED) {
      logger.error('PayPal payment invalid call for id: ' + payPalCashIn.id + ', status: ' + payPalCashIn.status);
      logCompletionError(payPalReturn, 'Invalid status');
      defTransaction.reject({err: 'Invalid status'});
    }

    else {
      var payer = {payer_id: params.payerId};
      paypal.payment.execute(payPalCashIn.payPalId, payer, {}, function (err, resp) {
        payPalCashIn.executionDate = new Date();

        if (err) {
          logger.error('Payment API call failed for execution. payPalCashIn id=' + payPalCashIn.id + '. ' + err);
          logExecutionError(payPalCashIn, JSON.stringify(err));
          defTransaction.reject({err: JSON.stringify(err)});
        }

        else if (!resp) {
          logger.error('Invalid payment API execution call result. payPalCashIn id=' + payPalCashIn.id);
          logExecutionError(payPalCashIn, "NO RESPONSE");
          defTransaction.reject({err: 'Invalid payment API call result'});
        }
        else {
          payPalCashIn.executionResponse = JSON.stringify(resp);

          if (resp.state != 'approved') {
            logger.error('PayPal does not approve payment execution. payPalCashIn id=' + payPalCashIn.id);
            logExecutionError(payPalCashIn, "Not approved. Details: " + payPalCashIn.executionResponse);
            defTransaction.reject({err: 'Not approved'});
          }
          else {
            var def = q.defer();
            payPalCashIn.status = PayPalCashIn.PayPalStatus.APPROVED_BUT_NOT_COMPLETED;
            if (resp.transactions && resp.transactions[0]) {
              var trans = resp.transactions[0];
              if (trans.related_resources && trans.related_resources[0]) {
                var resource = trans.related_resources[0];
                if (resource.sale && resource.sale.state) {
                  var sale = resource.sale;
                  if (sale.state == 'completed') {
                    payPalCashIn.status = PayPalCashIn.PayPalStatus.COMPLETED;
                  }

                  if (sale.transaction_fee == null) {
                    logger.error('Transaction fee was null for paypal cash in');
                  }
                  Currency.Model.findOne({
                    where: {
                      iso4217Code: sale.transaction_fee ? sale.transaction_fee.currency : null
                    }
                  }).then(function (currency) {
                    if (currency != null) {
                      payPalCashIn.cashIo.status = CashIO.Status.COMPLETED;
                      payPalCashIn.cashIo.userCurrencyAmount = parseFloat(sale.amount.total);
                      payPalCashIn.cashIo.feesAmount = parseFloat(sale.transaction_fee.value);
                      payPalCashIn.cashIo.feesCurrencyId = currency.id;
                      def.resolve(payPalCashIn);
                    } else {
                      logger.error('No currency ' + sale.transaction_fee.currency + ' for paypal transaction fees in db.');
                      def.reject('No currency ' + sale.transaction_fee.currency + ' for paypal transaction fees in db.');
                    }
                  }, function (err) {
                    def.reject(err);
                  });
                } else {
                  def.resolve(payPalCashIn);
                }
              } else {
                def.resolve(payPalCashIn);
              }
            } else {
              def.resolve(payPalCashIn);
            }

            def.promise.then(function () {
              q.spread([
                payPalCashIn.save({transaction: t}),
                payPalCashIn.cashIo.save({transaction: t})
              ], function (updatedPayPalCashIn:any, updatedCashIO:any) {
                defTransaction.resolve({cashIOId: updatedCashIO.id});
              }, function (err) {
                logger.error('Cant save completion result for PayPalCashIn id=' + payPalCashIn.id);
                defTransaction.reject(err);
              });
            }, function (err) {
              defTransaction.reject(err);
            });

          }


        }

      });
    }
    return defTransaction.promise;
  });
}


function logReturnAndGetPayPalCashIn(params, type) {
  var def = q.defer();

  PayPalReturn.Model.create({
    type: type,
    params: JSON.stringify(params),
    date: new Date()
  }).then(function (createdReturn) {
    PayPalCashIn.Model.findOne({
      where: {
        id: params.id
      },
      include: [
        {
          required: true,
          model: CashIO.Model,
          as: 'cashIo'
        }
      ]
    }).then(function (payPalCashIn: PayPalCashIn.Instance) {
      if(!payPalCashIn) {
        return def.reject(new BadRequest());
      }

      createdReturn.cashInId = payPalCashIn.id;

      createdReturn.save().then(function (updatedReturn) {
        def.resolve({payPalCashIn: payPalCashIn, payPalReturn: updatedReturn});
      }, function (err) {
        logger.error('Cant link PayPalReturn with PayPalCashIn. p: ' + params + '. t: ' + type);
        def.reject(err);
      });

    }, function (err) {
      logger.error('Cant find PayPalCashIn to be linked with PayPalReturn. p: ' + params + '. t: ' + type);

      createdReturn.err = 'No PayPalCashIn';
      createdReturn.save().then(function (updatedReturn) {
        def.reject(err);
      }, function (err) {
        def.reject(err);
      })
    });
  }, function (err) {
    logger.error('Cant log PayPal payment return. p: ' + params + '. t: ' + type);
    def.reject(err);
  });

  return def.promise;
}


function logRegistrationError(payPalCashIn, request, error) {
  var def = q.defer();

  payPalCashIn.initRequest = request;
  payPalCashIn.initErr = error;

  payPalCashIn.save().then(function (updated) {
    def.resolve();
  }, function (err) {
    logger.error('Cant save registration error for PayPalCashIn id=' + payPalCashIn.id + '. req: ' + request + '. err:' + error);
    def.reject(err);
  });

  return def.promise;
}

function logCompletionError(payPalReturn, error) {
  var def = q.defer();

  payPalReturn.err = error;

  payPalReturn.save().then(function (data) {
    def.resolve();
  }, function (err) {
    logger.error('Cant save return error for PayPalReturn id=' + payPalReturn.id + '. error:' + error);
    def.reject(err);
  });

  return def.promise;
}

function logExecutionError(payPalCashIn, error) {
  var def = q.defer();

  payPalCashIn.executionErr = error;

  payPalCashIn.save().then(function (data) {
    def.resolve();
  }, function (err) {
    logger.error('Cant save execution error for PayPalCashIn id=' + payPalCashIn.id + '. err:' + error);
    def.reject(err);
  });

  return def.promise;
}
