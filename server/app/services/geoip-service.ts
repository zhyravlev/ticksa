/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import geoip = require('geoip-lite');
import Ticksa = require('ticksa');

import Models = require('../models/models')
import Country = Models.Country;
import CountryService = require('../services/country-service');

import config = require('../config');

/*
 * We have problem!
 *  - 'geoip-lite' library return country from the list: http://www.geonames.org/countries/
 *  - possible countries are stored in 'Country' table in DB
 * Lists of countries are not equal.
 */
module GeoipService {

  const IP_FOR_LOCAL_AND_TEST: string = '52.42.88.98';

  export class GeoipData implements Ticksa.IGeoipData {
    public ip: string;
    public countryA2Code: string;

    constructor(ip: string, countryA2Code: string) {
      this.ip = ip;
      this.countryA2Code = countryA2Code;
    }

    // 1) AWS ElasticLoadBalancing provides X-Forwarded headers
    //    http://docs.aws.amazon.com/ElasticLoadBalancing/latest/DeveloperGuide/x-forwarded-headers.html
    //    https://en.wikipedia.org/wiki/X-Forwarded-For
    // 2) Nginx proxy provides x-forwarded-for header too
    static async getFromRequest(req: Ticksa.Request): Promise<Ticksa.IGeoipData> {
      const remote_ip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress).split(/,\s*/)[0];

      const ip = config.isLocal() || config.isTest()
         ? IP_FOR_LOCAL_AND_TEST
         : remote_ip;

      let country = null;

      let isELBHealthChecker = false;

      if (req.headers['user-agent'] != null) {
        isELBHealthChecker = !!req.headers['user-agent'].match(/ELB-HealthChecker/);
      }

      if (isELBHealthChecker) {
        country = 'US';
      }
      else {
        const geo = geoip.lookup(ip);
        country = geo ? geo.country : null;

        if (country == null) {
          logger.error('Can\'t detect country for IP: ' + ip + (geo == null ? ' (geo is null)' : ''));
        }
      }


      // TODO [dborisov] it should be fixed - in Ticksa DB there is no description for country 'Russian Federation'
      if (country == 'RU') {
        if (config.isQA() || config.isStaging() || config.isProduction()) {
          country = 'US';
        }
      }

      const validCountry: Country.Attributes = await CountryService.getValidCountry(country);
      return new GeoipData(ip, Country.countryCodeToUpperCase(validCountry.a2));
    }
  }
}

export = GeoipService;
