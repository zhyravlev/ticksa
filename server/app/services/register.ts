/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import * as _ from 'lodash';
import * as Ticksa from 'ticksa';
import * as uuid from 'node-uuid';
import * as Sequelize from 'sequelize';
import * as Promise from 'bluebird';
import * as requestPromise from 'request-promise';
import * as Errors from '../errors';
import * as config from '../config';
import {
  Account,
  Country,
  Tickser,
  TickserSocial,
  Channel,
  ChannelInvite,
  TickserChannel,
  TickserReferral
} from '../models/models';
import * as TicksTransactionService from './ticks-transaction-service';
import * as NotificationService from './notification-service';
import * as RegistrationMessages from '../dto/registration/registration-messages';
import * as SecurityService from '../services/security-service';
import {IFacebookMeResponse} from '../dto/facebook/facebook-messages';

//require the Twilio module and create a REST client
const twilioClient = require('twilio')(config.env().sms.accountSid, config.env().sms.authToken);

export function generateSocial(tickserId: number, type: number, socialId: string, t: any): Promise<TickserSocial.Instance> {
  return TickserSocial.Model.create({
    tickserId: tickserId,
    type: type,
    socialId: socialId
  }, {
    transaction: t
  })
}

export function sendVerificationSms(number: string, code: string): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    twilioClient.messages.create({
      to: number,
      from: config.env().sms.phoneNumber,
      body: 'Ticksa verification code: ' + code
    }, function (err, message) {
      if (err) {
        logger.error('Error sending SMS. Number: ' + number, err);
        reject(err);
      } else {
        logger.info('SMS sent. Number ' + number + '. sid: ' + message.sid);
        resolve(message.sid);
      }
    });
  });
}

export async function generateUser(request: RegistrationMessages.IUserRegistrationRequest, local: Ticksa.IRequestLocalData): Promise<Tickser.Instance> {
  const recapchaUrl = 'https://www.google.com/recaptcha/api/siteverify?secret=' +
    config.env().recaptcha.secret + '&response=' + request.recaptcha;
  const rpResponse: any = await requestPromise.post(recapchaUrl);
  const response = JSON.parse(rpResponse);

  if (!response.success) {
    throw new Errors.BadRequest('Recaptcha error');
  }

  const password = await SecurityService.createPassword(request.password);
  const country: Country.Instance = await Country.Model.findByA2Code(local.geoip.countryA2Code);
  const createdUser: Tickser.Instance = await Tickser.Model.create({
    username: request.username.toLowerCase(),
    email: request.username,
    name: request.name,
    password: password,
    confirmationUrl: uuid.v4(),
    status: Tickser.Status.NEW,
    language: request.language,
    registerRedirectUrl: request.redirectUrl,
    countryId: (country != null) ? country.id : null,
    registrationDate: new Date(),
    preferences: {}
  });

  if (request.referrerId != null) {
    await TickserReferral.Model.create({
      commissionTicks: 0.0,
      tickserId: createdUser.id,
      referrerId: request.referrerId
    })
  }

  return createdUser;
}

export class SocialUserGenerationParams {
  public email: string;
  public name: string;
  public socialImageUrl: string;
  public socialType: number;
  public socialId: string;
  public referrerId: number;
  public lang: string;
  public geoip: Ticksa.IGeoipData;

  public static forFacebook(facebookMeResponse: IFacebookMeResponse, socialImageUrl: string, referrerId: number, local: Ticksa.IRequestLocalData): SocialUserGenerationParams {
    const result: SocialUserGenerationParams = new SocialUserGenerationParams();
    result.email = facebookMeResponse.email;
    result.name = facebookMeResponse.name;
    result.socialImageUrl = socialImageUrl;
    result.socialType = TickserSocial.Type.FACEBOOK;
    result.socialId = facebookMeResponse.id;
    result.referrerId = referrerId;
    result.lang = local.language;
    result.geoip = local.geoip;
    return result;
  }

  public static forGoogle(googleData: any, socialImageUrl: string, referrerId: number, local: Ticksa.IRequestLocalData): SocialUserGenerationParams {
    const result: SocialUserGenerationParams = new SocialUserGenerationParams();
    result.email = googleData.email;
    result.name = googleData.name;
    result.socialImageUrl = socialImageUrl;
    result.socialType = TickserSocial.Type.GOOGLE;
    result.socialId = googleData.sub;
    result.referrerId = referrerId;
    result.lang = local.language;
    result.geoip = local.geoip;
    return result;
  }
}

export async function generateUserSocial(params: SocialUserGenerationParams,
                                         t: Sequelize.Transaction): Promise<Tickser.Instance> {

  if (params.email == null || params.socialType == null || params.socialId == null) {
    throw new Errors.BadRequest('Email, social type and social id must not be null');
  }

  const country: Country.Instance = await Country.Model.findByA2Code(params.geoip.countryA2Code);
  const createdUser: Tickser.Instance = await Tickser.Model.create({
    username: params.email.toLowerCase(),
    email: params.email,
    name: params.name,
    socialImageUrl: params.socialImageUrl,
    status: Tickser.Status.CONFIRMED,
    countryId: (country != null) ? country.id : null,
    registrationDate: new Date(),
    preferences: {}
  }, {
    transaction: t
  });

  await generateSocial(createdUser.id, params.socialType, params.socialId, t);

  if (params.referrerId != null) {
    await TickserReferral.Model.create({
      commissionTicks: 0.0,
      referrerId: params.referrerId,
      tickserId: createdUser.id
    }, {
      transaction: t
    });
  }

  await confirmRegistration(createdUser, params.lang, t);
  return createdUser;
}

export async function confirmRegistration(tickser: Tickser.Instance, language: string, t: Sequelize.Transaction): Promise<Tickser.Instance> {
  const createdChannel: Channel.Instance = await Channel.Model.create(
    {
      name: constructDefaultChannelName(tickser.name, language),
      description: 'Enter description',
      type: Channel.Type.PUBLIC,
      status: Channel.Status.ACTIVE
    },
    {transaction: t});

  tickser.status = Tickser.Status.CONFIRMED;
  tickser.confirmedDate = new Date();
  tickser.confirmationUrl = '';
  tickser.systemChannelId = createdChannel.id;
  tickser.defaultChannelId = createdChannel.id;

  const account = {
    tickserId: tickser.id,
    totalTicksDebited: 0.0,
    totalTicksCredited: 0.0,
    totalFreeTicks: 0.0,
    currentBalance: 0.0,
    autoTopupAmount: 0.0,
    balanceUpdated: new Date(),
    status: Account.Status.NORMAL,
    type: Account.AccountType.PRIVATE,
    autoAuthorize: true,
    autoAuthorizeMaxTicks: 0.0,
    currentBalanceFreeTicks: 0.0
  };

  const updateTickser = tickser.save({transaction: t});
  const createAccount = Account.Model.create(account, {transaction: t});
  const addChannel = tickser.addChannel(createdChannel, {
    transaction: t,
    ownerType: TickserChannel.OwnerType.CREATOR
  });
  const findTickserReferral = TickserReferral.Model.scope('referrer').findOneByTickserId(tickser.id, {transaction: t});

  const [savedTickser, createdAccount, channel, tickserReferral] = await Promise.all([updateTickser, createAccount, addChannel, findTickserReferral]);
  const updatedTickser: Tickser.Instance = <Tickser.Instance> savedTickser;

  let grantFreeTicksToReferrer: Promise<any>;
  if (tickserReferral != null) {
    const refParams: TicksTransactionService.GrantFreeTicksParams = {
      tickserId: tickserReferral.referrer.id,
      systemAccountId: Account.SystemAccount.FREE_TICKS_ACCOUNT,
      amount: 10,
      t: t,
    };
    grantFreeTicksToReferrer = TicksTransactionService.processGrantFreeTicksTransaction(refParams);
  } else {
    grantFreeTicksToReferrer = Promise.resolve();
  }

  const params: TicksTransactionService.GrantFreeTicksParams = {
    tickserId: tickser.id,
    systemAccountId: Account.SystemAccount.FREE_TICKS_ACCOUNT,
    amount: config.global.registrationFreeTicks,
    t: t,
  };

  const grantFreeTicksToUser = TicksTransactionService.processGrantFreeTicksTransaction(params);

  await Promise.all([grantFreeTicksToReferrer, grantFreeTicksToUser]);
  if (tickserReferral) {
    logger.info('Granted free ticks to user. Referrer Id ' + tickserReferral.referrer.id);
  }
  logger.info('Granted free ticks to user. Id ' + createdAccount.id);

  const channelInvites: ChannelInvite.Instance[] = await ChannelInvite.Model.findAll({
    where: {email: updatedTickser.username},
    transaction: t
  });

  const channelIds = _.pluck(channelInvites, 'channelId');

  const channels: Channel.Instance[] = await Channel.Model.findAll({where: {id: {$in: channelIds}}});

  const promises: Promise<any>[] = [];
  updatedTickser.notificationCount = channelInvites.length;
  promises.push(updatedTickser.save({transaction: t}));

  _.forEach(channels, function (channel: Channel.Instance) {
    const channelInvite = _.findWhere(channelInvites, {channelId: channel.id});
    promises.push(updateInviteAndCreateNotification(updatedTickser, channel, channelInvite, t));
  });

  await Promise.all(promises);
  return updatedTickser;
}

function updateInviteAndCreateNotification(tickser: Tickser.Instance, channel: Channel.Instance,
                                           channelInvite: ChannelInvite.Instance, t: Sequelize.Transaction) {
  let params: NotificationService.ChannelInviteNotificationParams = {
    tickserId: tickser.id,
    tickserName: tickser.name,
    channelName: channel.name,
    inviteOwnerType: channelInvite.ownerType,
    inviteId: channelInvite.id,
    t: t,
  };
  channelInvite.tickserId = tickser.id;

  let createNotification = NotificationService.createForChannelInviteNotification(params);
  let saveInvite = channelInvite.save({transaction: t});

  return Promise.all([createNotification, saveInvite]);
}

function constructDefaultChannelName(userName: string, language: string) {
  switch (language) {
    case 'en':
      return userName + '\'s Channel';

    case 'de':
      return userName + ' Kanäle';

    case 'hu':
      return userName + ' Csatornája';

    case 'sk':
      return userName + ' Kanál';

    default:
      return userName + '\'s Channel';
  }
}