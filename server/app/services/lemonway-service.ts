/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import Promise = require('bluebird');
import express = require('express');
import _ = require('lodash');
import ticksa = require('ticksa');
import Errors = require('../errors');
import config = require('../config');
import lemonway = require('../api/lemonway/lemonway');

import Models = require('../models/models');
import LemonwayWallet = Models.LemonwayWallet;

var apiConfig: lemonway.ApiConfig = {
  directKitWSDLFile: config.env().finance.lemonway.directKitWSDLFile,
  webkitUrl: config.env().finance.lemonway.webkitUrl,
  returnUrl: config.env().server.url + '/api/lemonway/callback/success',
  errorUrl: config.env().server.url + '/api/lemonway/callback/error',
  cancelUrl: config.env().server.url + '/api/lemonway/callback/cancel',
  wlLogin: config.env().finance.lemonway.wlLogin,
  wlPass: config.env().finance.lemonway.wlPass,
};

lemonway.createApi(apiConfig);

export interface MoneyInWebInitAndGetFormResult {
  request?: {
    MoneyInWebInit?: lemonway.MoneyInWebInitRequest;
    GetCardForm?: lemonway.GetCardFormRequest;
  }
  response?: {
    MoneyInWebInit?: lemonway.MoneyInWebInitResponse;
    GetCardForm?: lemonway.GetCardFormResponse;
    message?: string;
  }
  cardForm?: string;
  success?: boolean;
}

export interface MoneyInWithCardIdResult {
  moneyInRequest?: lemonway.MoneyInWithCardIdRequest;
  moneyInResponse?: lemonway.MoneyInWithCardIdResponse;
  success?: boolean;
}

export interface GetMoneyInTransDetailsResult {
  request: lemonway.GetMoneyInTransDetailsRequest;
  response?: lemonway.GetMoneyInTransDetailsResponse;
  success?: boolean;
  message?: string;
}

export interface WalletDetails {
  cards?: CardDetail[];
  walletId?: string;
}
export interface CardDetail {
  id: string;
  num: string;
}


export function request(req?: ticksa.Request): Service {

  var requestConfig: lemonway.RequestConfig = {
    language: req ? req.local.language : 'en',
    walletIp: req ? req.headers['x-forwarded-for'] || req.connection.remoteAddress : '127.0.0.1',
    walletUa: req ? req.headers['user-agent'] : 'Mozilla/5.0 (X11; Linux x86_64)'
  };

  logger.info('requestConfig:', requestConfig);
  return new Service(requestConfig);
}

function getEnvPrefix() {
  return config.getMode().toString().substring(0, 1);
}

export function generateWkToken(cashIoId: number): string {
  return cashIoId + getEnvPrefix() + Math.random().toString(36).slice(2);
}

function generateWalletId(accountId: number): string {
  return 'ticksa' + accountId + getEnvPrefix();
}

export function generateWalletEmail(email: string): string {
  if (config.isProduction()) {
    return email;
  }
  return getEnvPrefix() + '.' + email;
}

export class Service {
  private requestConfig: lemonway.RequestConfig;

  constructor(requestConfig: lemonway.RequestConfig) {
    this.requestConfig = requestConfig;
  }

  public moneyInWebInitAndGetForm(walletId: string, moneyAmount: number, wkToken: string): Promise<MoneyInWebInitAndGetFormResult> {
    var that = this;
    var result: MoneyInWebInitAndGetFormResult = {
      request: {},
      response: {},
    };

    var initRequest: lemonway.MoneyInWebInitRequest = {
      wallet: walletId,
      amountTot: moneyAmount.toFixed(2),
      amountCom: moneyAmount.toFixed(2),
      wkToken: wkToken.toString(),
      autoCommission: lemonway.Bool.FALSE,
      registerCard: lemonway.Bool.TRUE,
    };

    result.request.MoneyInWebInit = initRequest;

    return lemonway.api.MoneyInWebInit(initRequest, that.requestConfig)
      .then(function (initResponse: lemonway.MoneyInWebInitResponse) {

        result.response.MoneyInWebInit = initResponse;

        var getCardFormReq: lemonway.GetCardFormRequest = {
          moneyInToken: initResponse.MONEYINWEB.TOKEN,
          cssUrl: Service.getLemonwayFormCssUrl(),
        };

        result.request.GetCardForm = getCardFormReq;
        return lemonway.api.GetCardForm(getCardFormReq, that.requestConfig);
      }).then(function (formResponse: lemonway.GetCardFormResponse) {

        result.response.GetCardForm = formResponse;

        if (formResponse.statusCode != 200) {
          return Promise.reject('Error getting form');
        }

        var cardForm = formResponse.body;
        if (!cardForm || cardForm == '0007') {
          return Promise.reject('Error getting form');
        }
        cardForm = Service.changeLemonwayFormImages(cardForm);

        result.cardForm = cardForm;
        result.success = true;
        return result;
      }).catch(function (err) {
        result.success = false;
        result.response.message = JSON.stringify(err);
        return result;
      });
  }

  public moneyInWithCardId(moneyAmount: number, cardId: string, accountId: number): Promise<MoneyInWithCardIdResult> {
    var that = this;
    var result: MoneyInWithCardIdResult = {};

    return LemonwayWallet.Model.findOneByAccountId(accountId).then(function (wallet: LemonwayWallet.Instance) {
      if (!wallet) {
        return Promise.reject({status: 400, message: 'Wallet not found'})
      }
      var req: lemonway.MoneyInWithCardIdRequest = {
        wallet: wallet.walletId,
        cardId: cardId,
        amountTot: moneyAmount.toFixed(2),
        amountCom: moneyAmount.toFixed(2),
        autoCommission: lemonway.Bool.FALSE,
      };

      result.moneyInRequest = req;

      return lemonway.api.MoneyInWithCardId(req, that.requestConfig);
    }).then(function (response: lemonway.MoneyInWithCardIdResponse) {
      result.moneyInResponse = response;

      if (response.TRANS.HPAY.STATUS != lemonway.TransactionStatus.SUCCESS) {
        result.success = false;
        return result;
      }

      result.success = true;
      return result;
    }).catch(function (err) {
      result.moneyInResponse = err;
      return result;
    });
  }
  public sendPayment(request: lemonway.SendPaymentRequest) {
    return lemonway.api.SendPayment(request, this.requestConfig);
  }

  public getMoneyInTransDetails(transactionId: string): Promise<GetMoneyInTransDetailsResult> {
    var that = this;

    var req: lemonway.GetMoneyInTransDetailsRequest = {
      transactionId: transactionId,
    };

    var result: GetMoneyInTransDetailsResult = {
      request: req
    };

    return lemonway.api.GetMoneyInTransDetails(req, that.requestConfig).then(function (response: lemonway.GetMoneyInTransDetailsResponse) {
      result.response = response;

      var hpays = response.TRANS.HPAY;
      if (!hpays || hpays.length == 0) {
        result.success = false;
        result.message = "GetMoneyInTransDetailsResponse: HPAY length = 0 or undefined";
        return result;
      }

      if (that.isHpaysSuccesful(hpays)) {
        result.success = true;
      } else {
        result.success = false;
      }

      if (hpays.length > 1) {
        result.message = "WARNING: GetMoneyInTransDetailsResponse: HPAY length = " + hpays.length + ". ";
        if (result.success) {
          result.message += "All HPAY is successful";
        } else {
          result.message += "One HPAY from the list is not successful";
        }
      }

      return result;
    }).catch(function (err) {
      result.success = false;
      result.response = err;
      return result;
    })
  }

  private isHpaysSuccesful(hpays: lemonway.Hpay[]) {
    var allHpayIsSuccessful = true;
    hpays.forEach(function (hpay: lemonway.Hpay) {
      if (hpay.STATUS != lemonway.TransactionStatus.SUCCESS) {
        allHpayIsSuccessful = false;
      }
    });

    return allHpayIsSuccessful;
  }

  public getWalletDetails(accountId: number): Promise<WalletDetails> {
    var that = this;
    return LemonwayWallet.Model.findOneByAccountId(accountId).then(function (wallet: LemonwayWallet.Instance) {
      if (!wallet) {
        throw new Errors.NotFound('Wallet not found');
      }

      if (wallet.status == LemonwayWallet.Status.ERROR) {
        throw new Errors.BadRequest('Wallet not ready to use');
      }

      return lemonway.api.GetWalletDetails({wallet: wallet.walletId}, that.requestConfig);
    }).then(function (response: lemonway.GetWalletDetailsResponse) {
      var details: WalletDetails = {};
      if (response.WALLET.CARDS) {
        details.cards = _.map<lemonway.CardObj, CardDetail>(response.WALLET.CARDS.CARD, function (card: lemonway.CardObj) {
          var cardDetail: CardDetail = {
            id: card.ID,
            num: card.EXTRA.NUM,
          };
          return cardDetail;
        });
      }
      details.walletId = response.WALLET.ID;
      return details;
    });
  }

  public getWalletOrRegister(firstName: string, lastName: string, email: string, accountId: number): Promise<LemonwayWallet.Instance> {
    var that = this;

    return LemonwayWallet.Model.findOneByAccountId(accountId).then(function (wallet: LemonwayWallet.Instance) {
      if (!wallet) {
        return that.registerWallet(firstName, lastName, email, accountId);
      }

      if (wallet.status == LemonwayWallet.Status.ERROR) {
        return that.registerWalletByInstance(wallet);
      }

      return wallet;
    });
  }


  public registerWallet(firstName: string, lastName: string, email: string, accountId: number): Promise<LemonwayWallet.Instance> {
    var wallet = LemonwayWallet.Model.build({
      email: generateWalletEmail(email),
      firstName: firstName,
      lastName: lastName,
      accountId: accountId,
      walletId: generateWalletId(accountId),
    });

    return this.registerWalletByInstance(wallet);
  }

  public registerWalletByInstance(wallet: LemonwayWallet.Instance): Promise<LemonwayWallet.Instance> {
    var that = this;

    var req: lemonway.RegisterWalletRequest = {
      wallet: wallet.walletId,
      clientMail: wallet.email,
      clientFirstName: wallet.firstName,
      clientLastName: wallet.lastName,
    };

    wallet.initRequest = JSON.stringify(req);

    return <Promise<LemonwayWallet.Instance>> lemonway.api.RegisterWallet(req, that.requestConfig).then(function (response: lemonway.RegisterWalletResponse) {
      wallet.status = LemonwayWallet.Status.CREATED;
      wallet.initResponse = JSON.stringify(response);
      return wallet.save();
    }).catch(function (err) {
      wallet.initResponse = JSON.stringify(err);

      if (err.Code == "152") { //CLIENT ALREADY EXISTS
        wallet.status = LemonwayWallet.Status.CREATED;
        return wallet.save();
      }

      wallet.status = LemonwayWallet.Status.ERROR;
      return wallet.save();
    });
  }

  private static getLemonwayFormCssUrl() {
    const baseUrl = config.isLocal()
      ? 'http://localhost:3001'
      : config.env().cdn.urlWithTimestamp;

    return baseUrl + '/assets/lemonway/style.css';
  }

  // Replace old-school LW images with more modern ones stored in Ticksa static files.
  private static changeLemonwayFormImages(cardForm: string): string {
    const baseUrl = config.isLocal()
      ? 'http://localhost:3001'
      : config.env().cdn.urlWithTimestamp;

    return cardForm
      .replace(new RegExp('/images/logo/', 'g'), baseUrl + '/assets/images/')
      .replace(new RegExp('.gif', 'g'), '.png');
  }
}