/// <reference path="../typings/tsd.d.ts"/>


import q = require('q');
import _ = require('lodash');

import config = require('../config');
var ioredis = require('ioredis');

var redis = ioredis(
  config.env().redis.port,
  config.env().redis.host,
  {
    // This is the default value of `retryStrategy`
    retryStrategy: function (times) {
      return Math.min(times * 2000, 60000);
    }
  });


export function sequelizeInstanceToJSON(instance) {
  if (instance == null) {
    return null;
  }
  if (instance.constructor === Array) {
    return _.map(instance, function (item:any) {
      return item.toJSON();
    })
  } else {
    return instance.toJSON();
  }
}

export function setSequelizeData(key, data, ttl) {
  data = sequelizeInstanceToJSON(data);
  var def = q.defer();
  redis.setex(key, ttl, JSON.stringify(data), function (err, res) {
    if (err) {
      def.reject(err);
    } else {
      def.resolve(data);
    }
  });
  return def.promise;
}

export function get(key) {
  var def = q.defer();
  redis.get(key, function (err, result) {
    if (err != null || result == null) {
      def.reject(err);
    } else {
      def.resolve(JSON.parse(result));
    }
  });
  return def.promise;
}

export function set(key, data, ttl) {
  var def = q.defer();
  redis.setex(key, ttl, JSON.stringify(data), function (err, res) {
    if (err) {
      def.reject(err);
    } else {
      def.resolve(data);
    }
  });
  return def.promise;
}

export function invalidate(key) {
  var def = q.defer();
  redis.del(key, function (err, result) {
    if (err) {
      def.reject(err);
    } else {
      def.resolve(result);
    }
  });
  return def.promise;
}