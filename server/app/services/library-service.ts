/// <reference path="../typings/tsd.d.ts"/>
import Promise = require("bluebird");
import _ = require('lodash');
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');

import Models = require('../models/models');
import Channel = Models.Channel;
import Content = Models.Content;
import ContentVideo = Models.ContentVideo;
import ContentImage = Models.ContentImage;
import Image = Models.Image;
import Video = Models.Video;
import Feed = Models.Feed;
import Tickser = Models.Tickser;

export function getUsedVideoIds(ids: number[]): Promise<number[]> {
  const getContents = Content.Model.findAllByTeaserVideoIds(ids);
  const getContentVideos = ContentVideo.Model.findAllByVideoIds(ids);
  const getFeeds = Feed.Model.findAllByVideoIds(ids);

  return Promise.all([getContents, getContentVideos, getFeeds])
    .spread<number[]>(function (contents: Content.Instance[], contentVideos: ContentVideo.Instance[], feeds: Feed.Instance[]) {
    const usedContentIds = _.pluck(contents, 'teaserVideoId');
    const usedContentVideoIds = _.pluck(contentVideos, 'videoId');
    const usedFeedIds = _.pluck(feeds, 'videoId');

    return _.union(usedContentIds, usedContentVideoIds, usedFeedIds);
  });
}

export function getUsedImageIds(ids: number[]): Promise<number[]> {
  const getProfileImages = Tickser.Model.findAllByProfileImageIds(ids);
  const getChannelBackgrounds = Channel.Model.findAllByChannelBackgroundIds(ids);
  const getTeasers = Content.Model.findAllByTeaserImageIds(ids);
  const getImages = ContentImage.Model.findAllByImageIds(ids);
  const getFeeds = Feed.Model.findAllByImageIds(ids);
  const getChannelCovers = Channel.Model.findAllByCoverImageIds(ids);

  return Promise.all([getProfileImages, getChannelBackgrounds, getTeasers, getImages, getFeeds, getChannelCovers])
    .spread<number[]>(function (profileImages: Tickser.Instance[], channelBackgrounds: Channel.Instance[],
                                teasers: Content.Instance[], images: ContentImage.Instance[],
                                feeds: Feed.Instance[], covers: Channel.Instance[]): number[] {

    const usedProfileImagesIds: number[] = _.pluck(profileImages, 'profileImageId');
    const usedBackgroundIds: number[] = _.pluck(channelBackgrounds, 'backgroundImageId');
    const usedTeaserIds: number[] = _.pluck(teasers, 'teaserImageId');
    const usedImageIds: number[] = _.pluck(images, 'imageId');
    const usedFeedIds: number[] = _.pluck(feeds, 'imageId');
    const userCoverIds: number[] = _.pluck(covers, 'coverImageId');

    return _.union(usedProfileImagesIds, usedBackgroundIds, usedTeaserIds, usedImageIds, usedFeedIds, userCoverIds);
  });
}