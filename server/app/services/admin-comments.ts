/// <reference path="../typings/tsd.d.ts"/>

import express = require('express');
import q = require('q');
import config = require('../config');
import LocationUtils = require('../utils/ticksa-location-utils');

import Models = require('../models/models');
import Tickser = Models.Tickser;
import AdminComment = Models.AdminComment;
import Content = Models.Content;
import EmailRecipient = Models.EmailRecipient;

import MailService = require('./mail-service');

var conn = require('../connection');


export function createFeedback(user: Tickser.Instance, type: number, text: string): Promise<AdminComment.Instance> {
  return AdminComment.Model.create({
    entityId: user.id,
    entityName: 'Tickser',
    queue: AdminComment.Queue.USER,
    resultCode: AdminComment.ResultCode.NEW,
    status: AdminComment.Status.CREATED,
    type: type,
    text: text,
    createdBy: user.id
  }).then(function (createdComment: AdminComment.Instance) {
    var params = {
      user_name: user.name,
      feedback_number: createdComment.id
    };

    var templateName = EmailRecipient.Template.THANK_YOU_FOR_FEEDBACK;

    if (type == AdminComment.Type.COMPLAINT) {
      templateName = EmailRecipient.Template.THANK_YOU_FOR_COMPLAINT;

    } else if (type == AdminComment.Type.SUGGESTION) {
      templateName = EmailRecipient.Template.THANK_YOU_FOR_SUGGESTION;
    }

    MailService.sendMailToTickser(user, templateName, params);
    return createdComment;
  });
}

export function createComplaint(user: Tickser.Instance, contentId: number, text: string): Promise<AdminComment.Instance> {

  return AdminComment.Model.create({
    entityId: contentId,
    entityName: 'Content',
    queue: AdminComment.Queue.USER,
    resultCode: AdminComment.ResultCode.NEW,
    status: AdminComment.Status.CREATED,
    type: AdminComment.Type.COMPLAINT,
    text: text,
    createdBy: user.id
  }).then(function (createdComment: AdminComment.Instance) {
    return Content.Model.findById(contentId).then(function (content) {
      var params = {
        user_name: user.name,
        content_url: config.env().server.url + LocationUtils.tl('/channels/' + content.channelId + '/contents/' + content.id),
        content_title: content.title,
        report_number: createdComment.id
      };
      MailService.sendMailToTickser(user, EmailRecipient.Template.THANK_YOU_FOR_REPORT, params);
      return createdComment;
    });
  });
}

export function createReply(userId: number, recipientId: number, text: string): Promise<AdminComment.Instance> {
  return AdminComment.Model.create({
    entityId: recipientId,
    entityName: 'EmailRecipient',
    queue: AdminComment.Queue.RESPONSE,
    resultCode: AdminComment.ResultCode.NEW,
    status: AdminComment.Status.CREATED,
    type: AdminComment.Type.GENERAL,
    text: text,
    createdBy: userId

  });
}