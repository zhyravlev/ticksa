/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import * as _ from 'lodash';
import * as util from 'util';
import * as Sequelize from 'sequelize';
import * as Promise from 'bluebird';

import {Content} from '../models/models';
import {Condition} from '../models/models';
import {ContentCondition} from '../models/models';
import {TickserCondition} from '../models/models';
import {ContentAccess} from '../models/models';

import * as ConditionMapper from '../mappers/condition-mapper';
import * as Errors from '../errors';

export async function createTickserPurchaseCondition(tickserId: number, contentId: number, t: Sequelize.Transaction): Promise<void> {
  const conditions = await Condition.Model.findAllByContentAndType(contentId, Condition.Type.PURCHASE, {transaction: t});

  if (_.isEmpty(conditions)) {
    return;
  }

  if (conditions.length > 1) {
    const c: Condition.Attributes[] = _.map(conditions, c => c.get({plain: true}));
    logger.error('Unique constraint violated'
      + '. tickserId: ' + tickserId
      + '. contentId: ' + contentId
      + '. conditions: ' + util.inspect(c)
    );
  }

  const ids = _.pluck(conditions, 'id');

  await TickserCondition.Model.destroy({
    where: {
      tickserId: tickserId,
      conditionId: {$in: ids}
    },
    transaction: t
  });

  const tickserConditions: TickserCondition.Attributes[] = _.map(conditions, (condition: Condition.Instance) => {
    return {
      tickserId: tickserId,
      conditionId: condition.id
    }
  });

  await TickserCondition.Model.bulkCreate(tickserConditions, {transaction: t});
}

export async function removeTickserPurchaseConditions(tickserId: number, contentId: number, t: Sequelize.Transaction): Promise<void> {
  const conditions = await Condition.Model.findAllByContentAndType(contentId, Condition.Type.PURCHASE, {transaction: t});

  if (_.isEmpty(conditions)) {
    return;
  }

  if (conditions.length > 1) {
    const c: Condition.Attributes[] = _.map(conditions, c => c.get({plain: true}));
    logger.error('Unique constraint violated'
      + '. tickserId: ' + tickserId
      + '. contentId: ' + contentId
      + '. conditions: ' + util.inspect(c)
    );
  }

  const ids = _.pluck(conditions, 'id');

  await TickserCondition.Model.destroy({
    where: {
      tickserId: tickserId,
      conditionId: {$in: ids}
    },
    transaction: t
  });
}

export async function doesUserSatisfyCondition(contentId: number, tickserId: number): Promise<boolean> {
  const conditions = await ContentCondition.Model.findUnmetForTickser(tickserId);
  return !_.findWhere(conditions, {contentId: contentId});
}

export async function createContentConditions(content: Content.Instance,
                                              conditions: ConditionMapper.IConditionRequest[],
                                              t: Sequelize.Transaction): Promise<void> {
  return actualizeContentConditions(content, conditions, t);
}


export async function updateContentConditions(content: Content.Instance,
                                              conditions: ConditionMapper.IConditionRequest[],
                                              t: Sequelize.Transaction): Promise<void> {

  if (_.isUndefined(content.conditions)) {
    throw new Errors.ServerError('Invalid server logic of querying content [' + content.id + ']');
  }

  return actualizeContentConditions(content, conditions, t);
}

async function actualizeContentConditions(content: Content.Instance,
                                          conditions: ConditionMapper.IConditionRequest[],
                                          t: Sequelize.Transaction): Promise<void> {

  if (_.isEmpty(conditions) && _.isEmpty(content.conditions)) {
    return Promise.resolve();
  }

  if (_.isEmpty(conditions)) {
    await unlinkConditionsFromContent(content, content.conditions, t);
    return Promise.resolve();
  }

  const conditionsToUnlink: Condition.Instance[] = _.filter(content.conditions, (c: Condition.Instance) => {
    const actualCondition: ConditionMapper.IConditionRequest = _.findWhere(conditions,
      {type: c.value.type, content: {id: c.value.contentId}}
    );
    return actualCondition == null;
  });

  if (!_.isEmpty(conditionsToUnlink)) {
    await unlinkConditionsFromContent(content, conditionsToUnlink, t);
  }

  const newUniqueConditions: ConditionMapper.IConditionRequest[] = [];
  _.forEach(conditions, (conditionRequest: ConditionMapper.IConditionRequest) => {
    const c: Condition.Instance = _.findWhere(content.conditions,
      {value: {type: conditionRequest.type, contentId: conditionRequest.content.id}}
    );

    if (c == null) {
      // to be honest - this check should not exists. conditions from webapp should be unique
      // but it is OK to do such recheck
      const alreadyInNew = _.findWhere(newUniqueConditions,
        {type: conditionRequest.type, content: {id: conditionRequest.content.id}}
      );
      if (alreadyInNew == null) {
        newUniqueConditions.push(conditionRequest);
      }
    }
  });

  if (_.isEmpty(newUniqueConditions)) {
    return Promise.resolve();
  }

  // new conditions (for this content) can already exist in DB, because they can be created earlier
  const alreadyInDB: Condition.Instance[] = await findConditionsAlreadyExistInDB(newUniqueConditions, t);

  const conditionsToBeCreated: ContentCondition.Attributes[] = [];
  _.forEach(newUniqueConditions, (c: ConditionMapper.IConditionRequest) => {
    const alreadyExistsInDB = _.findWhere(alreadyInDB, {value: {type: c.type, contentId: c.content.id}});
    if (alreadyExistsInDB == null) {
      conditionsToBeCreated.push(<ContentCondition.Attributes>{
        value: {
          type: c.type,
          contentId: c.content.id,
        }
      });
    }
  });

  if (_.isEmpty(conditionsToBeCreated)) {
    await linkConditionsToContent(content, alreadyInDB, t);
    return Promise.resolve();
  }

  const createdConditions = await Condition.Model.bulkCreate(conditionsToBeCreated, {transaction: t, returning: true});
  const createTickserConditions = createTicksersPurchaseConditions(createdConditions, t);
  const addConditionsToContent = linkConditionsToContent(content, alreadyInDB.concat(createdConditions), t);

  await Promise.all([createTickserConditions, addConditionsToContent]);
  return Promise.resolve()
}

function findConditionsAlreadyExistInDB(requests: ConditionMapper.IConditionRequest[], t: Sequelize.Transaction): Promise<Condition.Instance[]> {
  const findOptions: Sequelize.FindOptions = {
    where: {
      $or: []
    },
    transaction: t
  };
  _.forEach(requests, (c: ConditionMapper.IConditionRequest) => {
    findOptions.where['$or'].push({
      value: {
        $contains: {
          type: c.type,
          contentId: c.content.id,
        }
      }
    })
  });
  return Condition.Model.findAll(findOptions);
}

async function linkConditionsToContent(content: Content.Instance, conditions: Condition.Instance[], t: Sequelize.Transaction): Promise<void> {
  return content.addConditions(conditions, {transaction: t});
}

async function unlinkConditionsFromContent(content: Content.Instance, conditions: Condition.Instance[], t: Sequelize.Transaction): Promise<void> {
  return content.removeConditions(conditions, {transaction: t});
}

interface ConditionPair {
  contentId: number,
  conditionId: number
}

async function createTicksersPurchaseConditions(conditions: Condition.Instance[],
                                                t: Sequelize.Transaction): Promise<void> {

  const contentIds: number[] = _.map(conditions, c => c.value.contentId);
  const accesses = await ContentAccess.Model.scope(ContentAccess.Scopes.INCLUDE_ACCOUNT).findAll({
    where: {
      contentId: {
        $in: contentIds
      },
      hasAccess: true
    },
    transaction: t
  });

  const ticksersConditions: TickserCondition.Attributes[] = [];
  _.forEach(conditions, (c: Condition.Instance) => {
    const ticksersIds: number[] = _.chain(accesses)
      .filter((a: ContentAccess.Instance) => a.contentId == c.value.contentId)
      .map((a: ContentAccess.Instance) => a.account.tickserId)
      .value();

    _.forEach(ticksersIds, tickserId => {
      ticksersConditions.push({
        tickserId: tickserId,
        conditionId: c.id
      })
    });
  });

  await TickserCondition.Model.bulkCreate(ticksersConditions, {transaction: t});
  return Promise.resolve();
}
