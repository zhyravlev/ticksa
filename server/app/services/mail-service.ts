/// <reference path="../typings/tsd.d.ts"/>

import config = require('../config');
import Sequelize = require('sequelize');

import Errors = require('../errors');

import Models = require('../models/models');
import Tickser = Models.Tickser;
import EmailRecipient = Models.EmailRecipient;
import Content = Models.Content;

import ContentService = require('../services/content-service');

export function sendMailToTickser(tickser: Tickser.Instance, templateName: string, params: any, options?: Sequelize.CreateOptions): Promise<EmailRecipient.Instance> {
  return EmailRecipient.Model.create({
    tickserId: tickser.id,
    email: tickser.email,
    templateName: templateName,
    language: null,
    params: JSON.stringify(params),
    status: EmailRecipient.Status.NEW,
    reason: EmailRecipient.Reason.APP,
    createdById: Tickser.SystemTicksers.EMAILER
  }, options);
}

export function sendMailWithLanguageToTickser(tickser: Tickser.Instance,
                                              templateName: string,
                                              language: EmailRecipient.PossibleLanguage,
                                              params: any,
                                              options?: Sequelize.CreateOptions): Promise<EmailRecipient.Instance> {
  return EmailRecipient.Model.create({
    tickserId: tickser.id,
    email: tickser.email,
    templateName: templateName,
    language: language,
    params: JSON.stringify(params),
    status: EmailRecipient.Status.NEW,
    reason: EmailRecipient.Reason.APP,
    createdById: Tickser.SystemTicksers.EMAILER
  }, options);
}

export function sendMailToContactWithTicksa(email: string, message: string): Promise<EmailRecipient.Instance> {
  var params = {
    message: message,
    email: email
  };

  return EmailRecipient.Model.create({
    tickserId: null,
    email: "contact@ticksa.com",
    templateName: EmailRecipient.Template.CONTACT_TICKSA,
    language: null,
    params: JSON.stringify(params),
    status: EmailRecipient.Status.NEW,
    reason: EmailRecipient.Reason.APP,
    createdById: Tickser.SystemTicksers.EMAILER
  });
}

export function sendMailIfNoTickser(email: any, templateName: string, params: any): Promise<EmailRecipient.Instance> {
  return EmailRecipient.Model.create({
    tickserId: null,
    email: email,
    templateName: templateName,
    language: null,
    params: JSON.stringify(params),
    status: EmailRecipient.Status.NEW,
    reason: EmailRecipient.Reason.APP,
    createdById: Tickser.SystemTicksers.EMAILER
  });
}
