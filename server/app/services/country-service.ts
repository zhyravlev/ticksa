/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import _ = require('lodash');
import Promise = require('bluebird');
import Ticksa = require('ticksa');

import Models = require('../models/models');
import Country = Models.Country;

import ValueSelector = require('../utils/value-selector');

export function getAllCountries(): Promise<Country.Attributes[]> {
  return CountriesFromDB.getInstance()
    .then(fromDb => fromDb.getValidCountries());
}

export function getValidCountry(countryCode: string): Promise<Country.Attributes> {
  return CountriesFromDB.getInstance()
    .then(fromDb => fromDb.getValidCountry(countryCode));
}

/*
 * We have problem!
 *  - 'geoip-lite' library return country from the list: http://www.geonames.org/countries/
 *  - possible countries are stored in 'Country' table in DB
 * Lists of countries are not equal.
 */
class CountriesFromDB {

  private static NO_COUNTRY_DATA_IN_LOGS: ValueSelector.KeyValueDictionaryStringifier<Country.Attributes> =
    function (): ValueSelector.KeyValueDictionaryStringifierResult {
      return {
        needString: false
      };
    };

  private static PROMISE: Promise<CountriesFromDB> = null;

  private static DEFAULT_EMPTY_COUNTRY: Country.Attributes = {
    name: null,
    a2: null,
    a3: null,
    number: null,
    dialingCode: null,
    currencyId: null,
    VAT: null,
    bookingCode: null
  };

  private countries: ValueSelector.ValueSelector<Country.Attributes>;

  constructor(countries: ValueSelector.ValueSelector<Country.Attributes>) {
    this.countries = countries;
  }

  getValidCountries(): Country.Attributes[] {
    return this.countries.getValues();
  }

  getValidCountry(countryCode: string): Country.Attributes {
    return this.countries.select(Country.countryCodeToUpperCase(countryCode));
  }

  public static getInstance() {
    if (CountriesFromDB.PROMISE == null) {
      CountriesFromDB.PROMISE = CountriesFromDB.initialize();
    }
    return CountriesFromDB.PROMISE;
  } 
  
  static initialize(): Promise<CountriesFromDB> {
    return new Promise<CountriesFromDB>(function (resolve, reject) {
      try {
        resolve(CountriesFromDB.readFromDBAndCache());
      } catch (err) {
        logger.error('CountriesReadFromDB not initialized', err);
        reject(err);
      }
    })
  }

  static readFromDBAndCache(): Promise<CountriesFromDB> {
    return Country.Model.findAll()
      .then(function (countries: Country.Instance[]) {

        const dictionary: ValueSelector.KeyValueDictionary<Country.Attributes> =
          new ValueSelector.KeyValueDictionary(CountriesFromDB.NO_COUNTRY_DATA_IN_LOGS);

        _.forEach(countries, function (c: Country.Instance) {
          dictionary.set(Country.countryCodeToUpperCase(c.a2), c.get({plain: true, clone: true}));
        });

        const selector = new ValueSelector.ValueSelector<Country.Attributes>(dictionary, CountriesFromDB.DEFAULT_EMPTY_COUNTRY);
        return new CountriesFromDB(selector);
      });
  }
}
