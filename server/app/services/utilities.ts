/// <reference path="../typings/tsd.d.ts"/>

import q = require('q');

import config = require('../config');

export function dateAdd(date: any, interval: string, units: number): Date {
  var ret = new Date(date);
  switch(interval.toLowerCase()) {
    case 'year'   :  ret.setFullYear(ret.getFullYear() + units);  break;
    case 'quarter':  ret.setMonth(ret.getMonth() + 3*units);  break;
    case 'month'  :  ret.setMonth(ret.getMonth() + units);  break;
    case 'week'   :  ret.setDate(ret.getDate() + 7*units);  break;
    case 'day'    :  ret.setDate(ret.getDate() + units);  break;
    case 'hour'   :  ret.setTime(ret.getTime() + units*3600000);  break;
    case 'minute' :  ret.setTime(ret.getTime() + units*60000);  break;
    case 'second' :  ret.setTime(ret.getTime() + units*1000);  break;
    default       :  ret = undefined;  break;
  }
  return ret;
}

export function encodeNumberToRadix64(number: number) {
  return Radix64.fromNumber(number);
}

export function decodeNumberFromRadix64(radix: string) {
  return Radix64.toNumber(radix);
}

var Radix64 = {

  _Rixits :
//   0       8       16      24      32      40      48      56     63
//   v       v       v       v       v       v       v       v      v
    "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._",
  fromNumber : function(number) {
    if (isNaN(Number(number)) || number === null ||
      number === Number.POSITIVE_INFINITY)
      throw "The input is not valid";
    if (number < 0)
      throw "Can't represent negative numbers now";

    var rixit; // like 'digit', only in some non-decimal radix
    var residual = Math.floor(number);
    var result = '';
    while (true) {
      rixit = residual % 64;
      result = this._Rixits.charAt(rixit) + result;
      residual = Math.floor(residual / 64);
      if (residual == 0)
        break;
    }
    return result;
  },

  toNumber : function(rixits) {
    var result = 0;
    rixits = rixits.split('');
    for (var e in rixits) {
      result = (result * 64) + this._Rixits.indexOf(rixits[e]);
    }
    return result;
  }
};