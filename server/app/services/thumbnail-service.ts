/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import _ = require('lodash');
import fs = require('fs');
import moment = require('moment');
import path = require('path');
import Promise = require('bluebird');
import stream = require('stream');

import Sequelize = require('sequelize');
import config = require('../config');

import Models = require('../models/models');
import Content = Models.Content;
import VideoFile = Models.VideoFile;
import File = Models.File;
import Video = Models.Video;

import AwsService = require('../services/aws');

const ffmpeg = require('fluent-ffmpeg');
const csv = require('csv');
import CsvUtils = require('../utils/csv-utils');

const thumbnailFileExtension = 'png';
const thumbPath = path.normalize(__dirname + '/../../cli/thumbnails');
const columns = {
  fileId: 'fileId',
  videoId: 'videoId',
  status: 'status',
  filePath: 'filePath',
  oldKey: 'oldKey',
  newKey: 'newKey',
  errorMessage: 'errorMessage'
};

enum LogStatus {
  SUCCESS = 0,
  ERROR = 1,
}

interface ProcessThumbLog {
  fileId: number;
  videoFileKey: string;        // it is excluded from data written in csv. see tests to understand how it works
  videoId: number;
  status?: LogStatus;
  filePath?: string;
  oldKey?: string;
  newKey?: string;
  errorMessage?: string;
}

export function generateById(videoId: number) {
  return Video.Model.scope(Video.Scopes.INCLUDE_VIDEO_FILES).findById(videoId)
    .then(function (video: Video.Instance) {
      if (!isValidVideoForPreparingThumbnails(video)) {
        throw new Error('Video is invalid to prepare thumbnails');
      }
      return generateThumbnails([video]);
    }).then(function () {
      logger.info('Thumbnail generation finished')
    });
}

export function generateAll(): Promise<any> {
  return generateWithPaging(0).then(function () {
    logger.info('Thumbnail generation finished')
  });
}

export function generateWithPaging(offset: number): Promise<any> {
  logger.info('OFFSET IS: ' + offset);

  const limit = 20;
  const options: Sequelize.FindOptions = {
    where: {
      transcodingStatus: Video.TranscodingStatus.COMPLETE,
    },
    order: [
      ['id', 'ASC']
    ],
    limit: limit,
    offset: offset,
  };

  return Video.Model.scope(Video.Scopes.INCLUDE_VIDEO_FILES).findAll(options)
    .then(function (videos: Video.Instance[]) {
      if (videos.length == 0) {
        return Promise.resolve();
      }

      const validVideos: Video.Instance[] = _.filter(videos, video => {
        const isValid: boolean = isValidVideoForPreparingThumbnails(video);
        if (!isValid) {
          logger.info('Video is not valid: ' + video.id)
        }
        return isValid;
      });

      if (validVideos.length == 0) {
        return generateWithPaging(offset + limit);
      }

      return generateThumbnails(validVideos).then(function () {
        return generateWithPaging(offset + limit);
      });
    })
}

function generateThumbnails(videos: Video.Instance[]): Promise<any> {

  let processingFiles: Promise<ProcessThumbLog>[] = [];
  _.forEach(videos, function (video: Video.Instance) {
    _.forEach(video.videoFiles, function (videoFile: VideoFile.Instance) {
      processingFiles.push(processVideoThumbnail(video, videoFile));
    });
  });

  return Promise.all(processingFiles).then(function (logs: ProcessThumbLog[]) {
    const uploadFiles: Promise<AwsService.UploadThumbnailResult>[] = _.map(logs,
      (log: ProcessThumbLog) =>
        AwsService.uploadThumbnail(log.fileId, log.videoFileKey, thumbnailFileExtension, thumbPath + '/' + log.filePath)
    );

    return Promise.all(uploadFiles).then(function (uploadFiles: AwsService.UploadThumbnailResult[]) {
      let updateFiles: Promise<any>[] = [];
      _.forEach(logs, function (log: ProcessThumbLog) {
        if (log.status == LogStatus.ERROR) {
          return;
        }

        let uploadedThumbnail = _.findWhere(uploadFiles, {fileId: log.fileId});

        if (!uploadedThumbnail) {
          log.status = LogStatus.ERROR;
          log.errorMessage = 'Uploaded file not found!';
          return;
        }

        log.newKey = uploadedThumbnail.key;
        if (uploadedThumbnail.error) {
          log.status = LogStatus.ERROR;
          log.errorMessage = uploadedThumbnail.error;
          return;
        }

        updateFiles.push(saveThumbnail(log, uploadedThumbnail));
      });

      return Promise.all(updateFiles);
    }).then(function () {
      return processLog(logs);
    });
  })
}

function processVideoThumbnail(video: Video.Instance, videoFile: VideoFile.Instance): Promise<ProcessThumbLog> {
  const prefix: string = '[For videoId ' + video.id + ' and fileId ' + videoFile.file.id + ']';

  const log: ProcessThumbLog = {
    fileId: videoFile.file.id,
    videoId: video.id,
    videoFileKey: videoFile.file.key
  };

  const thumbFileName = 'thumbnail-vid-' + video.id
    + '-bt-' + videoFile.bitrate
    + '-fid-' + videoFile.file.id
    + '-' + moment.now().toString()
    + '.' + thumbnailFileExtension;

  const timestamps: string[] = (video.durationInMillis > 60000) ? ['60'] : ['0'];

  return new Promise<ProcessThumbLog>(function (resolve, reject) {
    ffmpeg(config.env().aws.s3.cloudFront + '/' + videoFile.file.key)
      .on('filenames', function (filenames) {
        logger.info(prefix + ' Will generate ' + filenames.join(', '));
      })
      .on('end', function () {
        logger.info(prefix + ' Screenshots taken');
        log.status = LogStatus.SUCCESS;
        log.filePath = thumbFileName;
        resolve(log);
      })
      .on('error', function (err, stdout, stderr) {
        logger.error(prefix + ' Cannot process video: ' + err.message);
        log.status = LogStatus.ERROR;
        log.errorMessage = err.message;
        resolve(log);
      })
      .screenshots({
        timestamps: timestamps,
        folder: thumbPath,
        filename: thumbFileName
      });
  });
}

function saveThumbnail(log: ProcessThumbLog, uploadResult: AwsService.UploadThumbnailResult): Promise<any> {
  return Video.Model.scope(Video.Scopes.INCLUDE_THUMBNAIL_IMAGE).findById(log.videoId)
    .then(function (video: Video.Instance): Promise<any> {
      if (!video.thumbnailImage) {
        log.status = LogStatus.ERROR;
        log.errorMessage = 'Thumbnail image not found';
        return Promise.resolve();
      }

      const thumbnailImage: File.Instance = video.thumbnailImage;

      log.oldKey = thumbnailImage.key;
      thumbnailImage.key = uploadResult.key;
      //thumbnailImage.size = uploadResult.size;   // on date 2016-07-06 size of thumbnail file is not stored
      return thumbnailImage.save();
    }).catch(function (err) {
      log.status = LogStatus.ERROR;
      log.errorMessage = err;
      return Promise.resolve();
    });
}

function processLog(logs: ProcessThumbLog[]): Promise<any> {
  return new Promise(function (resolve, reject) {
    const stringifier = csv.stringify({header: true, columns: columns});
    _.forEach(logs, function (log: ProcessThumbLog) {
      stringifier.write(log);
    });

    const logFile: string = thumbPath + "/thumbnail-log-" + moment.now().toString() + ".csv";
    const stream: fs.WriteStream = fs.createWriteStream(logFile, {encoding: 'utf8'});

    const castedToParentClass: stream.Transform = CsvUtils.castToParentClass(stringifier);
    castedToParentClass
      .pipe(stream)
      .once('error', function (err) {
        logger.error('Error while creating report file', err);
        reject(err);
      })
      .once('finish', function () {
        logger.info('Report file created', logFile);
        resolve();
      });
    castedToParentClass.end();
  });
}

function isValidVideoForPreparingThumbnails(video: Video.Instance): boolean {
  return video.videoFiles.length == 1;
}