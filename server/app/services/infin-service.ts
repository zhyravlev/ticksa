/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import q = require('q');
import _ = require('lodash');
import md5 = require('md5');
import Promise = require('bluebird');
const param = require('node-jquery-param');

import ticksa = require('ticksa');
import Errors = require('../errors');

import Models = require('../models/models');
import CashIO = Models.CashIO;
import Currency = Models.Currency;
import InfinCashIn = Models.InfinCashIn;
import InfinCallback = Models.InfinCallback;

import config = require('../config');
import conn = require('../connection');

export function initializeInfinCashIn(cashIoId: number) {
  return conn.transaction(function (t) {
    let findCashIo = CashIO.Model.findOne({
        where: {
          id: cashIoId
        },
        include: [
          {
            required: false,
            model: InfinCashIn.Model,
            as: 'infinCashIn'
          }
        ],
        transaction: t
      }
    );

    return findCashIo.then(function (cashIo: CashIO.Instance) {
      if (cashIo == null) {
        logger.error('Cant intialize infin cashin - no cashIo with id ' + cashIoId);
        throw new Errors.ServerError('Cant find cashIO with id ' + cashIoId);
      }

      if (cashIo.type !== CashIO.Type.CASH_IN) {
        logger.error('Cant initialize infin cashin - cashIo with id ' + cashIoId + ' has type ' + cashIo.type);
        throw new Errors.ServerError('CashIO with id ' + cashIoId + ' has invalid type');
      }

      if (cashIo.status !== CashIO.Status.PENDING) {
        if (!cashIo.infinCashIn) {
          logger.error('Cant reintialize infin cashin - cashIo with id ' + cashIoId + ' is already initialized with errors');
          throw new Errors.ServerError('CashIO with id ' + cashIoId + ' has no infinCashIn');
        }

        logger.warn('Cant reintialize infin cashin - cashIo with id ' + cashIoId + ' is already successfully inialized');
        return {url: cashIo.infinCashIn.url};
      }

      return InfinCashIn.Model.create({
        status: InfinCashIn.Status.CREATED,
        cashIoId: cashIo.id,
        amount: cashIo.userCurrencyAmount,
        countryCode: cashIo.phoneCountryCode
      }, {transaction: t}).then(function (createdInfinCashIn: InfinCashIn.Instance) {
        createdInfinCashIn.url = createUrlForCashIn(createdInfinCashIn, cashIo.tickserPhoneNumber);
        cashIo.status = CashIO.Status.STARTED;

        const saveCashIn = createdInfinCashIn.save({transaction: t});
        const saveCashIo = cashIo.save({transaction: t});

        return Promise.all([saveCashIn, saveCashIo]).spread(function (updatedInfinCashIn: InfinCashIn.Instance, updatedCashIo: CashIO.Instance) {
          return {url: updatedInfinCashIn.url};
        });
      });
    });
  });
}

export function handleInfinCallback(req): q.Promise<number> {
  var def = q.defer<number>();

  var verifier = CallbackRequestVerifier(req);
  verifier.verifyRequestAndReturnCashIn().then(function (verification: any) {

    if (verification.res === 'ok') {
      finishInfinCashIn(verification.infinCashIn)
        .then(function (result) {
          def.resolve(result);
        }, function (err) {
          def.reject(err);
        });
    } else {
      def.reject({res: 'err'});
    }

  }, function (err) {
    def.reject(err);
  });

  return def.promise;
}

interface InfinRequestParameters {
  api_key: string,
  session_id: number,
  amount: number,
  country_code: string,
  infin_sn: string,
  infin_an?: string,
}

interface InfinLocalResponseEmulationParameters {
  session_id: number,
  amount: number,
  country_code: string,
  infinpy_currency: string
}

function createUrlForCashIn(infinCashIn, tickserPhoneNumber) {
  var infin = config.env().finance.infin;

  // in test regime skip Infin real service. let client app send callback immediately via GET request
  if (infin.mode !== 'prod') {
    var responseFromInfin: InfinLocalResponseEmulationParameters = {
      session_id: infinCashIn.id,
      amount: infinCashIn.amount,
      country_code: infinCashIn.countryCode,
      infinpy_currency: 'EUR'
    };

    return config.env().server.url + '/api/infin/callback?' + param(responseFromInfin);
  }

  var requestParameters: InfinRequestParameters = {
    api_key: infin.apiKey,
    session_id: infinCashIn.id,
    amount: infinCashIn.amount,
    country_code: infinCashIn.countryCode,
    infin_sn: infin.serviceName
  };

  if (tickserPhoneNumber) {
    requestParameters.infin_an = tickserPhoneNumber;
  }

  return infin.url + '?' + param(requestParameters);
}


var CallbackRequestVerifier = function (req) {
  var query = req.query;
  var params = {
    sessionId: query.session_id,
    amount: query.amount,
    countryCode: query.country_code,
    currencyCode: query.infinpy_currency
  };

  function verifyRequestAndReturnCashIn() {
    var def = q.defer();

    var infinCallback: any = {
      url: req.url
    };

    if (!params.sessionId) {
      infinCallback.type = InfinCallback.Type.INVALID_NO_SESSION_ID;
      logCallback(infinCallback);
      def.reject({res: 'err'});
      return def.promise;
    }

    var verification = verifyCashInCallbackP4(query, config.env().finance);
    if (verification.res != 'ok') {
      logger.error('Error while Infin callback verification', verification);
      infinCallback.type = verification.code;
      logCallback(infinCallback);
      def.reject({res: 'err'});
      return def.promise;
    }

    q.spread([Currency.Model.findOne({
      where: {
        iso4217Code: params.currencyCode
      }
    }),
      InfinCashIn.Model.findOne({
        where: {id: params.sessionId},
        include: [
          {
            required: true,
            model: CashIO.Model,
            as: 'cashIo'
          }
        ]
      })], function (currency: any, infinCashIn: any) {

      if (infinCashIn == null) {
        infinCallback.type = InfinCallback.Type.INVALID_SESSION_ID;
        logCallback(infinCallback);
        def.reject({res: 'err'});
        return;
      }

      infinCallback.infinCashInId = infinCashIn.id;

      if ((infinCashIn.countryCode != params.countryCode) || (infinCashIn.amount != params.amount)) {
        infinCallback.type = InfinCallback.Type.INVALID_PARAMS;
        logCallback(infinCallback);
        def.reject({res: 'err'});
        return;
      }

      if (infinCashIn.status == InfinCashIn.Status.NOTIFIED) {
        infinCallback.type = InfinCallback.Type.REPEATED;
        logCallback(infinCallback);
        def.reject({res: 'repeated'});
        return;
      }

      infinCallback.type = InfinCallback.Type.VALID;
      infinCashIn.cashIo.userCurrencyId = currency.id;

      logCallback(infinCallback);

      def.resolve({
        res: 'ok',
        infinCashIn: infinCashIn
      });

    }, function (err) {
      logger.error(err);
      def.reject(err);
    });

    return def.promise;
  }

  function logCallback(toSave: InfinCallback.Attributes) {
    InfinCallback.Model.create(toSave);
  }

  return {
    verifyRequestAndReturnCashIn: verifyRequestAndReturnCashIn
  };
};


function finishInfinCashIn(infinCashIn) {
  return conn.transaction(function (t) {
    infinCashIn.status = InfinCashIn.Status.NOTIFIED;
    infinCashIn.cashIo.status = CashIO.Status.COMPLETED;

    return Promise.all([
      infinCashIn.save({transaction: t}),
      infinCashIn.cashIo.save({transaction: t})
    ])
      .spread(function (updatedInfinCashIn: InfinCashIn.Instance, updatedCashIO: CashIO.Instance) {
        return Promise.resolve(updatedCashIO.id);
      });
  });
}

export interface InfinHashVerificationResult {
  res: string,
  code?: InfinCallback.Type,
  checkString?: string,
  md5?: string,
  p4?: string
}

export function verifyCashInCallbackP4(reqQuery: any, financeConfig: ticksa.Configuration.FinanceConfig): InfinHashVerificationResult {
  var infin = financeConfig.infin;

  if (config.isLocal()) {
    return {
      res: 'ok'
    };
  }

  if (!reqQuery.hasOwnProperty('p4')) {
    return {
      res: 'err',
      code: InfinCallback.Type.INVALID_NO_P4
    };
  }

  var params: any = _.chain<any>(reqQuery)
    .map(function (val, name) {
      return {
        name: name,
        val: val
      };
    })
    .filter(function (p) {
      return reqQuery.hasOwnProperty(p.name);
    })
    .filter(function (p) {
      return p.name != 'p4';
    })
    .sortBy(function (p) {
      return p.name;
    })
    .reduce<any>((memo, p) => memo + p.val, '')
    .value();

  var checkString = params + infin.secretKey;
  var MD5 = md5(checkString);

  if (MD5 !== reqQuery.p4) {
    return {
      res: 'err',
      code: InfinCallback.Type.INVALID_P4,
      checkString: checkString,
      md5: MD5,
      p4: reqQuery.p4
    };
  }

  return {
    res: 'ok'
  };
}