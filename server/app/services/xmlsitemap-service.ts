/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import * as moment from 'moment';
import Moment = moment.Moment;
import Duration = moment.Duration;
import * as _ from 'lodash';
import * as Promise from 'bluebird';
import * as sequelize from 'sequelize';
import * as conn from '../connection';
import {
  Channel,
  Content
} from '../models/models';

export async function findContentIds(): Promise<number[]> {
  const replacements = {
    status: Content.Status.ACTIVE,
    published: true,
    expiryDate: new Date(),
    channelStatus: Channel.Status.ACTIVE,
    channelType: Channel.Type.PUBLIC
  };

  const query = 'with ' +
    'by_created_at AS (' +
    '   SELECT cont.id AS content_id, cont.created_at AS created' +
    '   FROM "Content" AS cont' +
    '   INNER JOIN "Channel" AS ch ON cont.channel_id = ch.id AND ch.status = :channelStatus AND ch.type = :channelType' +
    '   WHERE cont.status = :status AND cont.published = :published' +
    '   AND cont.expiry_date >= :expiryDate' +
    '   AND (SELECT COUNT(*) FROM "ContentCondition" AS cc WHERE cc.content_id = cont.id) = 0' +
    '   ORDER BY cont.created_at DESC),' +
    'all_contents AS (' +
    '   SELECT content_id, created FROM by_created_at) ' +

    'SELECT content_id FROM all_contents ' +
    'ORDER by created DESC';

  const start: Moment = moment();
  const result: {content_id: number}[] = await conn.query(query, {
    replacements: replacements,
    type: sequelize.QueryTypes.SELECT
  });
  const stop: Moment = moment();
  const duration: Duration = moment.duration(stop.diff(start));

  logger.info('findContentIds. start: %s. stop: %s. duration: %s', start.format(), stop.format(), duration.asSeconds());

  return _.pluck(result, 'content_id');
}

function findContentFromDB(): Promise<Content.Instance[]> {
  return findContentIds()
    .then(function (ids: number[]) {
      if (!ids || ids.length == 0) {
        return [];
      }

      const scopes: any = [
        {method: [Content.Scopes.INCLUDE_REQUIRED_PUBLIC_ACTIVE_CHANNEL, null]}
      ];
      return Content.Model.scope(scopes).findAllByIds(ids)
        .then(function (contents: Content.Instance[]) {
          const orderByIds = {};
          _.forEach<any>(ids, function (id: number) {
            orderByIds[id] = ids.indexOf(id);
          });

          return _.sortBy<Content.Instance, number>(contents, function (content: Content.Instance) {
            return orderByIds[content.id];
          });
        });
    });
}

export async function getAllContentsForSitemap(): Promise<Content.Instance[]> {
  return await findContentFromDB();
}
