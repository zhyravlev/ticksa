/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import * as jade from 'jade';
import * as _ from 'lodash';
import * as ContentService from '../services/content-service';
import * as Errors from '../errors';
import * as config from '../config';
import {Content} from '../models/models';

module SharingService {

  export interface ParsingResult<T> {
    isSuitable: boolean;
    data?: T;
  }

  export interface ContentQuery {
    contentId: number;
  }

  export module Parser {

    export function isQueryForContent(escapedFragment: string): ParsingResult<ContentQuery> {
      if (!escapedFragment) {
        return doesntMatch<ContentQuery>();
      }

      try {
        const decoded: string = decodeURIComponent(escapedFragment);

        let match: string[];
        let regexp: RegExp;

        regexp = CONTENT_TEASER_QUERY();
        match = regexp.exec(decoded);
        if (!!match && match.length > 0) {
          return {
            isSuitable: true,
            data: {
              contentId: parseNumberOrThrowException(match, 1, regexp)
            }
          }
        }

        regexp = CONTENT_PAGE_QUERY();
        match = regexp.exec(decoded);
        if (!!match && match.length > 0) {
          return {
            isSuitable: true,
            data: {
              contentId: parseNumberOrThrowException(match, 1, regexp)
            }
          }
        }

        logger.warn('Incident during parsing: ' + escapedFragment + '. No coincidences.');
        return doesntMatch<ContentQuery>();
      } catch (err) {
        logger.error('Error during parsing: ' + escapedFragment + '. Err: ' + err);
        return doesntMatch<ContentQuery>();
      }
    }

    function doesntMatch<T>(): ParsingResult<T> {
      return <ParsingResult<T>> {
        isSuitable: false
      }
    }

    function parseNumberOrThrowException(match: any, index: number, regexp: RegExp): number {
      const num: number = Number(match[index]);

      if (isNaN(num)) {
        throw new Error('[sharing-service.ts] Error during parsing result ' + JSON.stringify(match)
          + '. index: ' + index
          + '. regexp: ' + regexp
        );
      }

      return num;
    }

    // for:  /teaser/1/3?referrerId=7  and  /teaser/1/4
    function CONTENT_TEASER_QUERY(): RegExp {
      return /^\/teaser\/\d+\/(\d+)(\?referrerId\=\d+)?$/gi;
    }

    // for:  /channels/1/contents/2
    function CONTENT_PAGE_QUERY(): RegExp {
      return /^\/channels\/\d+\/contents\/(\d+)$/gi;
    }

  }

  export module Crawler {

    export function isSocialNetworkCrawler(userAgent: string): boolean {
      if (userAgent == null) {
        return false;
      }

      // copied from: https://gist.github.com/srfrnk/5f67771e218fa772fc21
      const CRAWLER_SOCIAL_NETWORK_USER_AGENTS: string[] = [
        'facebookexternalhit',
        'twitterbot',
        'facebot',
        //'rogerbot',
        //'linkedinbot',
        //'embedly',
        //'quora link preview',
        //'showyoubot',
        //'outbrain',
        //'pinterest'
      ];

      const isSocialNetworkCrawler: boolean = _.some(CRAWLER_SOCIAL_NETWORK_USER_AGENTS, ua => userAgent.toLowerCase().indexOf(ua.toLowerCase()) !== -1);
      if (!isSocialNetworkCrawler) {
        return false;
      }

      logger.info('Social Network crawler hit! ' + userAgent);
      return true;
    }

    export function isSearchEngineCrawler(userAgent: string): boolean {
      const CRAWLER_SEARCH_ENGINE_USER_AGENTS: string[] = [
        'googlebot',
        'yahoo',
        'bingbot',
        'baiduspider',
        'DuckDuckBot',
        'YandexBot',
        'ia_archiver', // Alexa Crawler
        'Slurp', // Yahoo web crawler Slurp
      ];

      const isSearhEngineCrawler: boolean = _.some(CRAWLER_SEARCH_ENGINE_USER_AGENTS, ua => userAgent.toLowerCase().indexOf(ua.toLowerCase()) !== -1);
      if (!isSearhEngineCrawler) {
        return false;
      }

      logger.info('Search Engine crawler hit! ' + userAgent);
      return true;
    }

    export async function getResponseToSocialNetworkCrawler(contentId: number): Promise<string> {
      return await getResponseToCrawler(contentId, false);
    }

    export async function getResponseToSearchEngineCrawler(contentId: number): Promise<string> {
      return await getResponseToCrawler(contentId, true);
    }

    async function getResponseToCrawler(contentId: number, isSearchEngine: boolean): Promise<string> {
      const content: Content.Instance = await ContentService.findNotFullContentByIdForSharing(contentId);
      if (content == null) {
        throw new Errors.NotFound();
      }

      let teaserImageKey = null;
      if (content.teaserVideo) {
        teaserImageKey = content.teaserVideo.thumbnailImage.key
      }
      if (content.teaserImage) {
        teaserImageKey = content.teaserImage.file.key;
      }

      const description = _.trim(content.teaserText).length == 0 ? content.title : content.teaserText;

      if (isSearchEngine) {
        return jade.renderFile(config.getTemplateFilePath('crawler/content-search-engine.jade'), {
            data: {
              url: config.env().server.url + '/api/social/contents/' + content.channel.id + '/' + content.id,
              title: content.title,
              description: description,
              channelName: content.channel.name,
              teaserImage: teaserImageKey ? config.env().aws.s3.cloudFront + '/' + teaserImageKey : null,
              defaultImage: config.env().cdn.urlWithTimestamp + '/assets/images/ticksaLogo.png'
            }
          }
        );
      }

      return jade.renderFile(config.getTemplateFilePath('social/content-share.jade'), {
          data: {
            url: config.env().server.url + '/api/social/contents/' + content.channel.id + '/' + content.id,
            title: content.title,
            description: description,
            teaserImage: teaserImageKey ? config.env().aws.s3.cloudFront + '/' + teaserImageKey : null,
            defaultImage: config.env().cdn.urlWithTimestamp + '/assets/images/ticksaLogo.png'
          }

        }
      );
    }
  }
}

export = SharingService;