/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import _ = require('lodash');
import moment = require('moment');
import Sequelize = require("sequelize");
import Promise = require("bluebird");
import Ticksa = require("ticksa");

import ChannelMessages = require('../dto/channel/channel-messages');

import MailService = require('../services/mail-service');
import NotificationService = require('../services/notification-service');
import TicksTransactionService = require('../services/ticks-transaction-service');
import TickserChannelRankService = require('../services/tickser-channel-rank-service');

import conn = require("../connection");
import config = require("../config");
import Errors = require("../errors");
import Models = require('../models/models');
import Channel = Models.Channel;
import ChannelInvite = Models.ChannelInvite;
import ChannelSubscription = Models.ChannelSubscription;
import Content = Models.Content;
import Feed = Models.Feed;
import Tickser = Models.Tickser;
import TickserChannel = Models.TickserChannel;
import TicksTransaction = Models.TicksTransaction;
import TickserChannelRank = Models.TickserChannelRank;
import Notification = Models.Notification;
import Image = Models.Image;
import File = Models.File;
import EmailRecipient = Models.EmailRecipient;
import ChannelRevenue = Models.ChannelRevenue;
import Account = Models.Account;

export async function createChannel(tickserId: number, channelRequest: ChannelMessages.IChannelPostRequest) {
  await conn.transaction(async (t) => {
    const createdChannel = await Channel.Model.create({
      name: channelRequest.name,
      description: channelRequest.description,
      coverImageId: channelRequest.coverImageId,
      backgroundImageId: channelRequest.backgroundImageId,
      type: channelRequest.type,
      status: Channel.Status.ACTIVE,
      shareIncome: channelRequest.shareIncome,
      allowVoting: channelRequest.allowVoting,
      showRankedList: channelRequest.showRankedList
    }, {transaction: t});

    const createTickserChannel = TickserChannel.Model.create({
      ownerType: TickserChannel.OwnerType.CREATOR,
      tickserId: tickserId,
      channelId: createdChannel.id
    }, {transaction: t});
    const createRevenues = createRevenuesForChannel(tickserId, createdChannel, channelRequest.revenues, [], t);

    return Promise.all([createTickserChannel, createRevenues]);
  });
}

export function sortChannelContent(channelId: number, tickserId: number, contentIds: number[]): Promise<any> {
  return TickserChannel.Model.scope('channel').findOneByTickserIdAndChannelId(tickserId, channelId)
    .then(function (tickserChannel: TickserChannel.Instance) {
      if (!tickserChannel) {
        throw new Errors.Forbidden();
      }

      let options: Sequelize.CountOptions = {
        where: {
          channelId: channelId,
          id: {$in: contentIds}
        }
      };

      return Content.Model.count(options).then(function (count: number) {
        if (contentIds.length != count) {
          logger.error("#sortChannelContent(), invalid contentIds", {
            contentIds: contentIds,
            contentIdsCount: contentIds ? contentIds.length : 0,
            realContentCount: count
          });
          throw new Errors.BadRequest();
        }

        let channel = tickserChannel.channel;
        channel.sortedContent = contentIds;
        return channel.save();
      });
    })
}

export function findChannelByContentId(contentId?: number, options?: Sequelize.FindOptions): Promise<Channel.Instance> {
  if (contentId == null) {
    return Promise.resolve(null);
  }

  return Content.Model.findById(contentId, options)
    .then(function (content: Content.Instance) {
      return Channel.Model.scope(Channel.Scopes.REVENUES).findById(content.channelId, options);
    });
}

export function donateChannel(channelId: number, tickserId: number, amount: number, behalfTickserId?: number): Promise<any> {
  return TickserChannel.Model.findOneByChannelId(channelId).then(function(tickserChannel: TickserChannel.Instance) {
    if(!tickserChannel) {
      throw new Errors.NotFound('Channel not found');
    }

    return Models.getConnection().transaction(function (t) {
      let params: TicksTransactionService.TwoWayTransactionParams = {
        buyerTickserId: tickserId,
        sellerTickserId: tickserChannel.tickserId,
        donateChannelId: tickserChannel.channelId,
        behalfPayingTickserId: behalfTickserId,
        amount: amount,
        type: TicksTransaction.Type.CHANNEL_DONATE,
        t: t
      };

      return TicksTransactionService.processTwoWayTransaction(params)
    })
  })
}

export async function createRevenuesForChannel(currentTickserId: number, channel: Channel.Instance,
                                        revenues: ChannelRevenue.Attributes[],
                                        oldRevenues: ChannelRevenue.Instance[],
                                        t: Sequelize.Transaction): Promise<ChannelRevenue.Instance[]> {
  if (!revenues || revenues.length == 0) {
    return [];
  }

  let tickserIds = _(revenues)
    .map(revenue => revenue.tickserId)
    .uniq()
    .value();

  if (revenues.length != tickserIds.length) {
    throw new Errors.BadRequest('Duplicated ticksers in revenues sharing: ' + JSON.stringify(revenues));
  }

  const ticksers = await Tickser.Model.scope(Tickser.Scopes.INCLUDE_ACCOUNT)
    .findAll({
      where: {
        id: {
          $in: tickserIds
        }
      }
    });
  if (ticksers.length != revenues.length) {
    throw new Errors.BadRequest('Incorrect revenues');
  }

  let revenuesCreations: Promise<any>[] = _.map(revenues, (revenue: ChannelRevenue.Attributes) => {

    let tickser = _.findWhere(ticksers, {id: revenue.tickserId});

    if (!tickser) {
      throw new Errors.BadRequest('Incorrect revenues');
    }

    if (revenue.tickserId == currentTickserId) {
      throw new Errors.BadRequest('You can not share with yourself');
    }

    let oldRevenue = _.findWhere(oldRevenues, {tickserId: revenue.tickserId});
    if (!oldRevenue) {
      return createRevenueAndNotification(revenue, tickser, channel, t)
    }

    let isChanged: boolean = oldRevenue.percent !== revenue.percent
      || moment(revenue.expiryDate).diff(moment(oldRevenue.expiryDate)) != 0;

    return isChanged
      ? createRevenueAndNotification(revenue, tickser, channel, t)
      : createRevenue(revenue, tickser, channel, t);
  });

  return Promise.all(revenuesCreations);
}


async function createRevenue(revenue: ChannelRevenue.Attributes, tickser: Tickser.Instance, channel: Channel.Instance,
                             t: Sequelize.Transaction): Promise<any> {
  revenue.accountId = tickser.account.id;
  revenue.channelId = channel.id;
  return ChannelRevenue.Model.create(revenue, {transaction: t});
}


async function createRevenueAndNotification(revenue: ChannelRevenue.Attributes, tickser: Tickser.Instance,
                                            channel: Channel.Instance, t: Sequelize.Transaction): Promise<any> {
  revenue.accountId = tickser.account.id;
  revenue.channelId = channel.id;
  await ChannelRevenue.Model.create(revenue, {transaction: t});
  let templateName = !revenue.expiryDate
    ? EmailRecipient.Template.REVENUE_SHARED_NO_DATE_EXPIRED
    : EmailRecipient.Template.REVENUE_SHARED;

  let params = {
    user_name: tickser.name,
    channel_name: channel.name,
    percentage: revenue.percent,
    expiry_date: !revenue.expiryDate ? null : moment(revenue.expiryDate).format(config.DATE_FORMAT)
  };

  let email = MailService.sendMailToTickser(tickser, templateName, params, {transaction: t});
  let notification = NotificationService.createForChannelSharedRevenueChanged(
    revenue.tickserId, channel.name, revenue.percent, revenue.expiryDate, t);

  return Promise.all([email, notification]);
}