/// <reference path="../../typings/tsd.d.ts"/>
const logger = require('../../logger/logger')(__filename);

import Promise = require('bluebird');
import request = require('request');
import http = require('http');
import util = require('util')
import fs = require('fs');
import path = require('path');

var soap = require('soap');

export var api: Api = null;

export function createApi(apiConfig: ApiConfig) {
  if (api != null) {
    return;
  }
  var wsdl = path.join(__dirname, apiConfig.directKitWSDLFile);
  logger.info('wsdl file: ' + wsdl);

  soap.createClient(wsdl, function (err, soapClient: any) {
    if (err) {
      return logger.error(err.toString());
    }
    api = new Api(soapClient, apiConfig);
  });
}

export class Api {
  private soapClient: any;
  private apiConfig: ApiConfig;

  constructor(soapClient: any, apiConfig: ApiConfig) {
    this.soapClient = soapClient;
    this.apiConfig = apiConfig;
    this.apiConfig.language = apiConfig.language || 'en';
  }

  public RegisterWallet(req: RegisterWalletRequest, requestConfig: RequestConfig): Promise<RegisterWalletResponse> {
    this.buildRequest(req, requestConfig, '1.1');
    return Promise.promisify<RegisterWalletResponse, RegisterWalletRequest>(this.soapClient.RegisterWallet)(req)
      .then(this.handleError);
  }

  public GetWalletDetails(req: GetWalletDetailsRequest, requestConfig: RequestConfig): Promise<GetWalletDetailsResponse> {
    this.buildRequest(req, requestConfig, '1.8');
    return Promise.promisify<GetWalletDetailsResponse, GetWalletDetailsRequest>(this.soapClient.GetWalletDetails)(req)
      .then(this.handleError);
  }

  public MoneyInWebInit(req: MoneyInWebInitRequest, requestConfig: RequestConfig): Promise<MoneyInWebInitResponse> {
    this.buildRequest(req, requestConfig, '1.3');
    this.buildCallbackRequest(req);
    return Promise.promisify<MoneyInWebInitResponse, MoneyInWebInitRequest>(this.soapClient.MoneyInWebInit)(req)
      .then(this.handleError);
  }

  public MoneyInWithCardId(req: MoneyInWithCardIdRequest, requestConfig: RequestConfig): Promise<MoneyInWithCardIdResponse> {
    this.buildRequest(req, requestConfig, '1.1');
    return Promise.promisify<MoneyInWithCardIdResponse, MoneyInWithCardIdRequest>(this.soapClient.MoneyInWithCardId)(req)
      .then(this.handleError);
  }

  public GetMoneyInTransDetails(req: GetMoneyInTransDetailsRequest, requestConfig: RequestConfig): Promise<GetMoneyInTransDetailsResponse> {
    this.buildRequest(req, requestConfig, '1.8');
    return Promise.promisify<GetMoneyInTransDetailsResponse, GetMoneyInTransDetailsRequest>(this.soapClient.GetMoneyInTransDetails)(req)
      .then(this.handleError);
  }

  public GetCardForm(req: GetCardFormRequest, requestConfig: RequestConfig): Promise<GetCardFormResponse> {
    req.language = req.language || requestConfig.language;
    var url = LemonwayApiUtils.constructWebkitUrl(this.apiConfig.webkitUrl, req.moneyInToken, req.cssUrl, req.language);
    return Promise.promisify<GetCardFormResponse, string>(request.get)(url);
  }

  public SendPayment(req: SendPaymentRequest, requestConfig: RequestConfig): Promise<SendPaymentResponse> {
    this.buildRequest(req, requestConfig, '1.0');
    return Promise.promisify<SendPaymentResponse, SendPaymentRequest>(this.soapClient.SendPayment)(req)
      .then(this.handleError);
  }

  public handleError(response: Response): Promise<any> {
    var keys = Object.keys(response);
    if (!keys || keys.length == 0) {
      return Promise.reject(response)
    }
    var result: any = response[keys[0]];

    if (!result) {
      return Promise.reject(response);
    }

    if (result['E']) {
      return Promise.reject(result['E']);
    }
    return result;
  }

  private buildCallbackRequest(req: CallbackRequest) {
    req.cancelUrl = req.cancelUrl || this.apiConfig.cancelUrl;
    req.returnUrl = req.returnUrl || this.apiConfig.returnUrl;
    req.errorUrl = req.errorUrl || this.apiConfig.errorUrl;
  }

  private buildRequest(req: Request, requestConfig: RequestConfig, version: string) {
    req.wlLogin = req.wlLogin || this.apiConfig.wlLogin;
    req.wlPass = req.wlPass || this.apiConfig.wlPass;
    req.language = requestConfig.language;
    req.version = version;
    req.walletIp = requestConfig.walletIp;
    req.walletUa = requestConfig.walletUa;
  }
}

export class LemonwayApiUtils {

  static constructWebkitUrl(webkitUrl: string, moneyInToken: string, cssUrl: string, lang: string): string {
    return util.format('%s?moneyintoken=%s&p=%s&lang=%s', webkitUrl, moneyInToken, encodeURIComponent(cssUrl), lang);
  }
}

export interface ApiConfig extends RequestConfig {
  directKitWSDLFile: string;
  webkitUrl: string;
  returnUrl: string;
  errorUrl: string;
  cancelUrl: string;
  wlLogin: string;
  wlPass: string;
}

export interface RequestConfig {
  language?: string;
  walletIp?: string;
  walletUa?: string;
}

export interface Request {
  wlLogin?: string;
  wlPass?: string;
  language?: string;
  walletIp?: string;
  walletUa?: string;
  version?: string | number;
}

export interface CallbackRequest {
  returnUrl?:string;
  cancelUrl?:string;
  errorUrl?:string;
}

export interface LemonwayError {
  Error: string;
  Code: string;
  Msg: string;
  Prio: string;
}

export interface Response {
  E: LemonwayError;
}

export interface RegisterWalletRequest extends Request {
  wallet: string;
  clientMail: string;
  clientFirstName: string;
  clientLastName: string;
}

export interface RegisterWalletResponse extends Response {
  WALLET: {
    ID: string;
    LWID: string;
  }
}

export interface GetWalletDetailsRequest extends Request {
  wallet?: string;
  email?: string;
}

export interface GetWalletDetailsResponse extends Response {
  WALLET:{
    ID:string;
    BAL:string;
    NAME:string;
    EMAIL:string;
    DOCS: {
      DOC: [{
        ID:string;
        S:DocumentStatus;
        TYPE:DocumentType;
        VD: Date;
      }];
    };
    IBANS: {
      IBAN: [{
        ID:string;
        S:IBANStatus;
        DATA:string;
        SWIFT:string;
        HOLDER:string;
      }];
    };
    STATUS:string;
    BLOCKED:Blocked;
    SDDMANDATES: {
      SDDMANDATE: [{
        ID:number;
        S:SddMandateStatus;
        DATA:string;
        SWIFT:string;
      }];
    };
    LWID:string;
    CARDS: {
      CARD: CardObj[];
    };
  }
}

export interface CardObj {
  ID:string;
  EXTRA:{
    IS3DS:string;
    CTRY:string;
    AUTH:string;
    NUM:string;
    EXP:string;
    TYP:string;
  };
}
/**
 Status of the document:
 1: received only
 2: received and accepted
 3: received and NOT accepted
 4: replaced by another document
 5: expired document but validated
 6: Wrong type
 7: Wrong name
 */
export enum DocumentStatus {
  RECEIVED_ONLY = 1,
  RECEIVED_AND_ACCEPTED = 2,
  RECEIVED_AND_NOT_ACCEPTED = 3,
  RECEIVED_BY_ANOTHER_DOCUMENT = 4,
  EXPIRED_DOCUMENT_BUT_VALIDATED = 5,
  WRONG_TYPE = 6,
  WRONG_NAME = 7,
}
/**
 Type of document:
 0: proof of ID
 1: proof of domiciliation
 2: IBAN
 7: proof of registry commerce number for enterprises only
 11 to 20: other
 21: SDD mandate
 */
export enum DocumentType {
  PROOF_OF_ID = 0,
  PROOF_OF_DOMICILIATION = 1,
  IBAN = 2,
  PROOF_OF_REGISTRY_COMMERCE_NUMBER_FOR_ENTERPRISES_ONLY = 7,
  TO_20_OTHER = 11,
  SDD_MANDATE = 24,
}
/**
 Only status 5 allows the use of the IBAN for a money-out.
 Non exhaustive list of other status :
 4: waiting to be verified by Lemon Way
 6: rejected by bank
 8: deactivated
 9: rejected
 */
export enum IBANStatus {
  ALLOW = 5,
  WAITING_TO_BE_VERIFIED_BY_LW = 4,
  REJECT_BY_BANK = 6,
  DEACTIVATED = 8,
  REJECT = 9,
}

/***
 0: not yet approved by Lemon Way
 5: can be used, actual debit will happen after 6 working days
 6: can be used, actual debit will happen after 3 working days
 8: deactivated
 9: rejected
 */
export enum SddMandateStatus {
  NOT_YET_APPROVED_BY_LW = 0,
  CAN_BE_USED_AFTER_6_DAYS = 5,
  CAN_BE_USED_AFTER_3_DAYS = 6,
  DEACTIVATED = 8,
  REJECT = 9,
}
/**
 Indicates if wallet is blocked or not:
 0: not blocked
 1: blocked
 */
export enum Blocked {
  NOT_BLOCKED = 0,
  BLOCKED = 1,
}

export enum Bool {
  FALSE = 0,
  TRUE = 1,
}

export class PredefinedWallet {

  /*
   SC - name of main Ticksa wallet in Lemon Way environment
   */
  public static SC: string = 'SC';

}


export interface MoneyInWebInitRequest extends Request, CallbackRequest {
  wallet:string;
  amountTot:string;
  amountCom?:string;
  comment?:string;
  useRegisteredCard?:string;
  wkToken:string;
  autoCommission:Bool;
  registerCard?:Bool;
}

export interface MoneyInWebInitResponse extends Response {
  MONEYINWEB:{
    TOKEN:string;
    ID:string;
    CARD:{
      ID:string;
    };
  }
}

export interface GetCardFormRequest extends Request {
  moneyInToken: string;
  cssUrl?: string;
  language?: string;
}

export interface GetCardFormResponse extends http.IncomingMessage {
  body: any;
}

export interface MoneyInWithCardIdRequest extends Request {
  wallet: string;
  cardId: string;
  amountTot: string;
  amountCom?: string;
  autoCommission:Bool;
}
/**
 3: money-in successful
 4: error
 0: waiting for finalization (since version 1.2)
 16: Reservation made successfully, awaiting validation.(Only with differed payments)
 */
export enum TransactionStatus {
  SUCCESS = 3,
  ERROR = 4,
  WAITING_FOR_FINALIZATION = 0,
  RESERV_MADE_SUCCESS = 16,
}


export interface Hpay {
  ID:string;
  MLABEL:string;
  DATE: string;
  SEN: string;
  REC: string;
  DEB: string;
  CRED: string;
  COM: string;
  MSG: string;
  STATUS: TransactionStatus;
  EXTRA:{
    IS3DS:string;
    CTRY:string;
    AUTH:string;
  };
  INT_MSG:string;
  SCHEDULED_DATE:string;
}


export interface MoneyInWithCardIdResponse extends Response {
  TRANS:{
    HPAY: Hpay;
  }
}

export enum ResponseCode {
  SUCCESS,
  ERROR,
}

export interface FinalizeCardPaymentPostResponse {
  type: string,
  response_code: ResponseCode;
  response_wkToken: string;
  response_msg?: string;
  response_transactionId: string;
  response_transactionAmount: number;
  response_transactionMessage: string;
}

export interface GetMoneyInTransDetailsRequest extends Request {
  transactionId?:string;
  transactionComment?:string;
  transactionMerchantToken?:string;
  startDate?:string;
  endDate?:string
}

export interface GetMoneyInTransDetailsResponse extends Response {
  TRANS:{
    HPAY: Hpay[];
  }
}

export interface SendPaymentRequest extends Request {
  debitWallet:string;
  creditWallet:string;
  amount:string;
  message?:string;
  scheduledDate?:string
  privateData?:string
}

export interface SendPaymentResponse extends Response {
  TRANS:{
    HPAY: Hpay;
  }
}