/// <reference path="../typings/tsd.d.ts"/>

import Ticksa = require('ticksa');
import uuid = require("node-uuid");

const continuationLocalStorage = require('continuation-local-storage');
const createNamespace = continuationLocalStorage.createNamespace;
const getNamespace = continuationLocalStorage.getNamespace;

const REQUEST_ID_NAMESPACE = 'request-id';
const nmspc = createNamespace(REQUEST_ID_NAMESPACE);

// https://datahero.com/blog/2014/05/22/node-js-preserving-data-across-async-callbacks/
export function registerRequestIdMiddleware(req: Ticksa.Request, res: Ticksa.Response, next: Ticksa.Next) {
  const rid: string = uuid.v4();

  nmspc.bindEmitter(req);
  nmspc.bindEmitter(res);

  nmspc.run(function () {
    nmspc.set('rid', rid);
    next();
  });
}

// We faced with problem - after Sequelize makes query one cant get data from CLS namespace.
// https://github.com/sequelize/sequelize/issues/1627
//
// For now (2016-08-02) we leave it as is. Waiting for solutions.
export function getRequestId(): string {
  return nmspc.get('rid');
}