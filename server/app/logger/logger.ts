/// <reference path="../typings/tsd.d.ts"/>

import * as winston from 'winston';
import * as moment from 'moment';
import * as fs from 'fs';
import * as path from 'path';
import * as util from 'util';

import * as Ticksa from 'ticksa';
import * as config from '../config';
import * as RequestId from './request-id';

export = logger;

function logger(label: string): winston.LoggerInstance {
  try {
    if (fs.statSync(label).isFile()) {
      label = path.basename(label).replace(new RegExp('\.js$', 'gi'), '.ts');
    }
  } catch (_) {
  }

  const transports: winston.TransportInstance[] = [];
  config.env().log.transports.forEach(t => {
    switch (t.type) {

      case 'console':
        transports.push(consoleTransport(label, t.level));
        return;

      default:
        return;
    }
  });

  return new (winston.Logger)({
    transports: transports
  });
}

function consoleTransport(label: string, level: Ticksa.Configuration.LogLevelType): winston.ConsoleTransportInstance {
  return new (winston.transports.Console)({
    label: label,
    level: level,
    colorize: true,
    timestamp: function () {
      return moment().format('YYYY-MM-DD HH:mm:ss.SSS');
    },
    formatter: function (options) {
      const message: string = options.message != null
        ? options.message.replace(/[\r\n]+/gm, '\\n').replace(/\t+/g, '\\t')
        : '';
      const meta: string = (options.meta != null) && (Object.keys(options.meta).length > 0)
        ? '\t' + util.inspect(options.meta).replace(/[\r\n]+/gm, '\\n')
        : '';
      const requestIdValue: string = RequestId.getRequestId();
      const requestId: string = requestIdValue != null
        ? requestIdValue
        : 'no reqId';

      return '[' + options.timestamp() + ']\t' +
        '[' + options.level + ']\t' +
        '[' + options.label + ']\t' +
        '[' + requestId + ']\t' +
        message +
        meta;
    }
  });
}

// function fileTransport(): winston.FileTransportInstance {
  // return new (winston.transports.File)({
  //   filename: 'somefile.log',
  //   level: 'silly'
  // })
// }
