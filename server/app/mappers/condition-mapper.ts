/// <reference path="../typings/tsd.d.ts"/>

import _ = require('lodash');
import Promise = require('bluebird');

import Models = require('../models/models');
import {Condition, Content} from "../models/models";

export interface IConditionRequest {
  type: Condition.Type;
  content: {
    id: number
  };
}

export interface IConditionResponse {
  type: Condition.Type;
  content: {
    id: number,
    title: string,
    teaserImage: {
      imageId: number,
      name: string,
      key: string
    },
    channel: {
      id: number,
      name: string,
      verified: boolean,
      coverImage: {
        id: number,
        key: string
      },
      background: {
        id: number,
        key: string
      }
    }
  }
}

export async function map(conditions: Condition.Instance[]): Promise<IConditionResponse[]> {

  if (_.isEmpty(conditions)) {
    return [];
  }

  const contentIds = _.map(conditions, (c: Condition.Instance) => {
    return c.value.contentId;
  });

  const scopes = [Content.Scopes.INCLUDE_CHANNEL_WITH_IMAGES, Content.Scopes.INCLUDE_TEASER_IMAGE];
  const contents = await Content.Model.scope(scopes).findAllByIds(contentIds);

  return _.map(conditions, (c: Condition.Instance) => {
    let content = _.findWhere(contents, {id: c.value.contentId});
    return {
      type: c.value.type,
      content: {
        id: content.id,
        title: content.title,
        teaserImage: content.teaserImage ? {
          imageId: content.teaserImage.id,
          name: content.teaserImage.name,
          key: content.teaserImage.file.key
        } : null,
        channel: {
          id: content.channel.id,
          name: content.channel.name,
          verified: content.channel.verified,
          coverImage: content.channel.coverImage ? {
            id: content.channel.coverImage.id,
            key: content.channel.coverImage.file.key
          } : null,
          background: content.channel.backgroundImage ? {
            id: content.channel.backgroundImage.id,
            key: content.channel.backgroundImage.file.key
          } : null
        },
      }
    };
  });
}