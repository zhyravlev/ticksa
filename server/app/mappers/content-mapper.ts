/// <reference path="../typings/tsd.d.ts"/>

import {Content} from "../models/models";

import * as AwsService from "../services/aws";

export interface IVoteboardContent {
  title: string;
  teaserText: string;
  description: string
  likes: number;
  teaserImageUrl: string;
  id: number,
  channelId: number
}

export function mapToVoteboardContent(content: Content.Instance): IVoteboardContent {
  return {
    id: content.id,
    channelId: content.channelId,
    title: content.title,
    teaserText: content.teaserText,
    description: content.description,
    likes: content.likes,
    teaserImageUrl: content.teaserImage ? AwsService.getResizeImageUrl(content.teaserImage.file.key, 100) : null
  };
}