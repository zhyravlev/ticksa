/// <reference path="../typings/tsd.d.ts"/>
import Models = require('../models/models');
import Channel = Models.Channel;
import Category = Models.Category;

export interface ICategory {
  id?: number;
  name?: string;
  key?: string;
}

export function map(id: number, category?: Category.Instance): ICategory {
  if (!category) {
    return {id: id};
  }

  return {
    id: category.id,
    name: category.name,
    key: category.key
  };
}