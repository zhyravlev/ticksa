/// <reference path="../typings/tsd.d.ts"/>
import Models = require('../models/models');
import Tickser = Models.Tickser;
import AwsService = require('../services/aws');

export interface ITickserSimple {
  id: number;
  name: string;
  profileImageUrl: string;
}

export function mapToSimple(tickser: Tickser.Instance): ITickserSimple {
  return {
    id: tickser.id,
    name: tickser.name,
    profileImageUrl: tickser.profileImage
      ? AwsService.getResizeImageUrl(tickser.profileImage.file.key, 120)
      : tickser.socialImageUrl,
  };
}