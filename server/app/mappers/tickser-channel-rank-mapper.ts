/// <reference path="../typings/tsd.d.ts"/>
import _ = require('lodash');

import Models = require('../models/models');
import Tickser = Models.Tickser;
import TickserChannelRank = Models.TickserChannelRank;

import TickserMapper = require('./tickser-mapper');
import ChannelMapper = require('./channel-mapper');

export interface ITickserChannelRank {
  tickser: TickserMapper.ITickserSimple
  tickserId: number
  channelId: number;
  earnedTicks: number;
}

export function mapAsList(ranks: TickserChannelRank.Instance[]): ITickserChannelRank[] {
  return _.map(ranks, function(rank: TickserChannelRank.Instance) {
    return map(rank);
  })
}


export function map(rank: TickserChannelRank.Instance): ITickserChannelRank {
  return {
    earnedTicks: <number> rank.earnedTicks,
    channelId: rank.channelId,
    tickserId: rank.tickserId,
    tickser: rank.tickser ? TickserMapper.mapToSimple(rank.tickser): null
  }
}