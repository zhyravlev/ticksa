/// <reference path="../typings/tsd.d.ts"/>

import {CashIO} from '../models/models';

export interface ICategory {
  accountId?: number;
  tickserId?: number;
  userCurrencyId?: number;
  userCurrencyAmount?: number;
  tickserPhoneNumber?: string;
  phoneCountryCode?: string;
  tickserEmail?: string;
  countryId?: number;
  status?: CashIO.Status;
  type?: CashIO.Type;
  paymentType?: CashIO.PaymentType;
  receivedAmount?: number;
  receivedTicks?: number;
  feesCurrencyId?: number;
  feesAmount?: number;
  VATAmount?: number;
  NETAmount?: number;
  error?: string;
  exchangeRateToEuro?: number;
}

export function map(cashIo?: CashIO.Instance): ICategory {

  return {
    accountId: cashIo.accountId,
    tickserId: cashIo.tickserId,
    userCurrencyId: cashIo.userCurrencyId,
    userCurrencyAmount: cashIo.userCurrencyAmount,
    tickserPhoneNumber: cashIo.tickserPhoneNumber,
    phoneCountryCode: cashIo.phoneCountryCode,
    tickserEmail: cashIo.tickserEmail,
    countryId: cashIo.countryId,
    status: cashIo.status,
    type: cashIo.type,
    paymentType: cashIo.paymentType,
    receivedAmount: cashIo.receivedAmount,
    receivedTicks: cashIo.receivedTicks,
    feesCurrencyId: cashIo.feesCurrencyId,
    feesAmount: cashIo.feesAmount,
    VATAmount: cashIo.VATAmount,
    NETAmount: cashIo.NETAmount,
    error: cashIo.error,
    exchangeRateToEuro: cashIo.exchangeRateToEuro
  }
}