/// <reference path="../typings/tsd.d.ts"/>
import Models = require('../models/models');
import Language = Models.Language;

export interface ILanguage {
  id?: number;
  name?: string;
  key?: string;
}

export function map(id: number, language?: Language.Instance): ILanguage {
  if (!language) {
    return {id: id};
  }

  return {
    id: language.id,
    name: language.name,
    key: language.key
  }
}