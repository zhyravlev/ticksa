/// <reference path="../typings/tsd.d.ts"/>
import Models = require('../models/models');
import Channel = Models.Channel;

export interface IChannelSimple {
  id: number;
  name: string;
  shareIncome: boolean;
  showRankedList: boolean;
  showRankedListDate: Date;
}

export function mapToSimple(channel: Channel.Instance): IChannelSimple {
  return {
    id: channel.id,
    name: channel.name,
    shareIncome: channel.shareIncome,
    showRankedList: channel.showRankedList,
    showRankedListDate: channel.showRankedListDate,
  };
}