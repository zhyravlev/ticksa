/// <reference path="./typings/tsd.d.ts"/>
const logger = require('./logger/logger')(__filename);

import "reflect-metadata";
import path = require('path');
import fs = require('fs');
import _ = require('lodash');
import Promise = require("bluebird");
import express = require('express');
import jade = require('jade');
import expressValidator = require('express-validator')
import Ticksa = require('ticksa');
import Models = require('./models/models');
import Errors = require('./errors');
import config = require('./config');
import context = require('./context');

import I18nService = require('./services/i18n-service');
import GeoipService = require('./services/geoip-service');
import RequestUtils = require('./utils/request-utils');

import {useExpressServer} from "routing-controllers";

// method needs to be called explicitly, because if it is called while loading a 'queue-reader' module than it won't
// work. because TypeScript compiler thinks that this module is never used and excludes 'queue-reader' from app.js
import queueReader = require('./services/queue-reader');
queueReader.processMessage();

import Morgan = require('./logger/morgan-logger');
import RequestId = require('./logger/request-id');

import compress = require('compression');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var cors = require('cors');

var bodyParser = require('body-parser');

let app = express();

context.init()
  .catch(err => {
    logger.error('Have to stop application. Error: ', err);
    process.exit(1);
  });

app.use(compress());
app.use(RequestId.registerRequestIdMiddleware);

// After issue APP-314 static files are returned by nginx.
// So if this headers are changed nginx configuration should be updated too.
app.use('/public', express.static(__dirname + '/public', {
  setHeaders: function (res, path) {
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.setHeader('Cache-Control', 'public,s-max-age=31536000,max-age=86400')
  }
}));

app.use(Morgan.logger);
app.use(cors());
app.use(bodyParser.json());

app.use(expressValidator());
app.use(bodyParser.urlencoded({extended: false}));
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(cookieParser());

app = useExpressServer(app, {
  controllerDirs: [__dirname + "/controllers/*.js"],
  middlewareDirs: [__dirname + "/middlewares/*.js"],
  interceptorDirs: [__dirname + "/interceptors/*.js"],
  defaultErrorHandler: true,
});

module.exports = app;
