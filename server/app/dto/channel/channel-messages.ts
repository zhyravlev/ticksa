/// <reference path="../../typings/tsd.d.ts"/>

import jsonValidator = require('../validator/json-validator');
import _ = require('lodash');
import moment = require('moment');

import Errors = require('../../errors');
import Models = require('../../models/models');
import Channel = Models.Channel;
import ChannelRevenue = Models.ChannelRevenue;

module ChannelMessages {

  export interface IChannelGetResponse {
    id: number;
    name: string;
    description: string;
    coverImage: {
      id: number,
      key: string
    };
    subscriberCount: number;
    subscribed: boolean;
    type: number;
    status: number;
    createdAt: Date;
    updatedAt: Date;
    ownerType: number;
    backgroundImage: {
      id: number;
      key: string;
    };
    verified: boolean;
    shareIncome: boolean;
    showRankedList: boolean;
    showRankedListDate: Date;
    allowVoting: boolean;
    revenues?: IChannelRevenue[];
  }

  export class ChannelGetResponse {

    static toJson(channel: Channel.Instance, subscriberCount?: number, subscribed?: boolean, ownerType?: number): IChannelGetResponse {
      let result: IChannelGetResponse = {
        id: channel.id,
        name: channel.name,
        description: channel.description,
        type: channel.type,
        status: channel.status,
        createdAt: channel.createdAt,
        updatedAt: channel.updatedAt,

        ownerType: ownerType,
        subscriberCount: subscriberCount,
        subscribed: subscribed,

        coverImage: channel.coverImage ? {
          id: channel.coverImage.id,
          key: channel.coverImage.file.key
        } : null,
        backgroundImage: channel.backgroundImage ? {
          id: channel.backgroundImage.file.id,
          key: channel.backgroundImage.file.key
        } : null,

        verified: channel.verified,

        shareIncome: channel.shareIncome,
        showRankedList: channel.showRankedList,
        showRankedListDate: channel.showRankedListDate,
        allowVoting: channel.allowVoting,
        revenues: channel.revenues ? _.map(channel.revenues, function (revenue: ChannelRevenue.Instance) {
          return {
            percent: revenue.percent,
            expiryDate: revenue.expiryDate,
            isExpired: revenue.isExpired(),
            tickser: {
              id: revenue.tickser.id,
              email: revenue.tickser.email
            }
          }
        }) : null
      };
      return result;
    }
  }

  export interface IChannelPostRequest {
    name: string;
    description: string;
    type: number;
    backgroundImageId: number;
    coverImageId: number;
    shareIncome: boolean;
    showRankedList: boolean;
    allowVoting: boolean;
    revenues: ChannelRevenue.Attributes[]
  }

  export class ChannelPostRequest {

    static parseFromJSON(json: any): IChannelPostRequest {
      jsonValidator.validateRequired(['name', 'type'], json);

      let request: IChannelPostRequest = {
        name: json.name,
        description: json.description,
        type: json.type,
        backgroundImageId: json.backgroundImageId,
        coverImageId: json.coverImageId,
        shareIncome: json.shareIncome,
        showRankedList: json.showRankedList,
        allowVoting: json.allowVoting,
        revenues: json.revenues ? _.map(json.revenues, function (revenue: IChannelRevenue) {
          return ChannelRevenueMapper.parseFromJSON(revenue);
        }) : []
      };

      if (request.revenues.length > 5) {
        throw new Errors.Validation('Revenues must be less than 5');
      }

      var total = _.reduce(request.revenues, function (total: number, revenue: ChannelRevenue.Attributes) {
        return total + revenue.percent;
      }, 0);

      if (total > 100.0) {
        throw new Errors.Validation('Incorrect percentage amount  ');
      }
      return request;
    }
  }

  export interface IChannelRevenue {
    percent?: number;
    expiryDate?: Date;
    isExpired?: boolean;
    tickser: {
      id: number;
      email: string;
    };
  }

  export class ChannelRevenueMapper {
    static parseFromJSON(json: IChannelRevenue): ChannelRevenue.Attributes {
      let request: ChannelRevenue.Attributes = {
        tickserId: json.tickser.id,
        percent: json.percent,
        expiryDate: json.expiryDate,
      };

      return request;
    }
  }
}

export = ChannelMessages;
