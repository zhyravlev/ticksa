/// <reference path="../../typings/tsd.d.ts"/>

import jsonValidator = require('../validator/json-validator');

module VoucherMessages {

  export class TickserRights {
    canCheck: boolean;
    banned: boolean;
    alreadyMaxVouchers: boolean;

    constructor(canCheck: boolean, banned: boolean, alreadyMaxVouchers: boolean) {
      this.canCheck = canCheck;
      this.banned = banned;
      this.alreadyMaxVouchers = alreadyMaxVouchers;
    }
    
    static active() {
      return new TickserRights(true, undefined, undefined);
    }

    static banned() {
      return new TickserRights(undefined, true, undefined);
    }

    static maxVouchers() {
      return new TickserRights(undefined, undefined, true);
    }
  }

  export class VoucherStatus {
    validCode: boolean;
    invalidCode: boolean;
    successfullyApplied: boolean;
    alreadyApplied: boolean;

    constructor(validCode: boolean, invalidCode: boolean, successfullyApplied: boolean, alreadyApplied: boolean) {
      this.validCode = validCode;
      this.invalidCode = invalidCode;
      this.successfullyApplied = successfullyApplied;
      this.alreadyApplied = alreadyApplied;
    }

    static empty() {
      return new VoucherStatus(undefined, undefined, undefined, undefined);
    }
  }

  export class VoucherPostRequest {
    code: string;
    type: number;

    constructor(code: string, type: number) {
      this.code = code;
      this.type = type;
    }

    static parseFromJSON(json, type): VoucherPostRequest {
      jsonValidator.validateRequired(['code'], json);
      return new VoucherPostRequest(json.code, type);
    }
  }

  export class VoucherPostResponse {
    rights: TickserRights;
    status: VoucherStatus;
    ticks: number;

    constructor(rights: VoucherMessages.TickserRights, status: VoucherStatus, ticks?: number) {
      this.rights = rights;
      this.status = status;
      this.ticks = ticks;
    }

    static banned(): VoucherPostResponse {
      return new VoucherPostResponse(TickserRights.banned(), VoucherStatus.empty());
    }

    static vouchersCountExceeded(): VoucherPostResponse {
      return new VoucherPostResponse(TickserRights.maxVouchers(), VoucherStatus.empty());
    }

    static alreadyApplied(): VoucherPostResponse {
      return new VoucherPostResponse(TickserRights.active(), new VoucherStatus(undefined, undefined, undefined, true));
    }

    static invalidCode(): VoucherPostResponse {
      return new VoucherPostResponse(TickserRights.active(), new VoucherStatus(undefined, true, undefined, undefined));
    }

    static bannedBecauseInvalidCode(): VoucherPostResponse {
      return new VoucherPostResponse(TickserRights.banned(), new VoucherStatus(undefined, true, undefined, undefined));
    }

    static validCode(voucher): VoucherPostResponse {
      return new VoucherPostResponse(TickserRights.active(), new VoucherStatus(true, undefined, undefined, undefined), voucher.ticksAmount);
    }

    static successfullyApplied(voucher): VoucherPostResponse {
      return new VoucherPostResponse(TickserRights.active(), new VoucherStatus(undefined, undefined, true, undefined), voucher.ticksAmount);
    }

    static maxVouchersCountBecauseApplied(voucher): VoucherPostResponse {
      return new VoucherPostResponse(TickserRights.maxVouchers(), new VoucherStatus(undefined, undefined, true, undefined), voucher.ticksAmount);
    }
  }
}

export = VoucherMessages;
