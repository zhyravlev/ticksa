/// <reference path="../../typings/tsd.d.ts"/>

import _ = require('lodash');
import jsonValidator = require('../validator/json-validator');

import Models = require('../../models/models');
import Tickser = Models.Tickser;
import AwsService = require('../../services/aws');

module AccountMessages {

  export class PreferencesUpdateRequest {
    preferences: Object;

    constructor(preferences: any) {
      this.preferences = preferences;
    }

    static parseFromJSON(json): PreferencesUpdateRequest {
      jsonValidator.validateRequired(['preferences'], json);
      return new PreferencesUpdateRequest(json.preferences);
    }
  }

  export interface IProfileUpdateRequest {
    name: string;
    language: string;
    phone: string;
    profileImageId: number;
    autoAuthorizeMaxTicks: number;
    autoAuthorize: boolean;
    defaultChannelId: number;
    paymentMethod: number;
    currencyId: number;
    countryId: number;
  }

  export class ProfileUpdateRequest {

    static parseFromJSON(json): IProfileUpdateRequest {
      jsonValidator.validateRequired(['autoAuthorize', 'defaultChannelId'], json);

      let request: IProfileUpdateRequest = {
        name: json.name,
        language: json.language,
        phone: json.phone,
        profileImageId: json.profileImage ? json.profileImage.id : null,
        autoAuthorizeMaxTicks: json.autoAuthorizeMaxTicks,
        autoAuthorize: json.autoAuthorize,
        defaultChannelId: json.defaultChannelId,
        paymentMethod: json.paymentMethod,
        currencyId: json.currencyId,
        countryId: json.countryId,
      };

      return request;
    }
  }

  export interface IAccountInfo {
    id: number;
    username: string;
    email: string;
    name: string;
    language: string;
    country: {
      id: number;
      codeA2: string;
      name: string;
      dialingCode: string;
      currencyId: number;
    };
    timezone: string;
    theme: string;
    externalMediaEnabled: boolean;
    notificationMethod: string;
    systemChannelId: number;
    defaultChannelId: number;
    notificationCount: any;
    phone: string;
    isPhoneVerified: boolean;
    verificationCodeExists: boolean;
    profileImageUrl: string;
    socialImageUrl: string;
    profileImage: {
      id: number;
      key: string;
    }
    preferences: any;
    status: number;

    financialAccount: {
      autoAuthorizeMaxTicks: number;
      autoTopupAmount: number;
      autoTopupMethod: number;
      autoAuthorize: boolean;
      currentBalance: {
        currentBalance: number;
        currentBalanceFreeTicks: number;
        currentBalanceEarnedTicks: number;
      };
      paymentMethod: number;
      currencyId: number;
    };

    socialAccounts: Array<{
      id: number;
      type: number;
      socialId: string;
    }>;
    categoryFilter?: number[];
    languageFilter?: number[];

    oldPasswordExists: boolean;
  }

  export class AccountInfo {
    static parseFromDB(tickser: Tickser.Instance): IAccountInfo {

      let info: IAccountInfo = {
        id: tickser.id,
        username: tickser.username,
        email: tickser.email,
        name: tickser.name,
        language: tickser.language,
        country: tickser.country ? {
          id: tickser.country.id,
          name: tickser.country.name,
          codeA2: tickser.country.a2,
          dialingCode: tickser.country.dialingCode,
          currencyId: tickser.country.currencyId
        } : null,
        timezone: tickser.timezone,
        theme: tickser.theme,
        externalMediaEnabled: tickser.externalMediaEnabled,
        notificationMethod: tickser.notificationMethod,
        systemChannelId: tickser.systemChannelId,
        defaultChannelId: tickser.defaultChannelId,
        notificationCount: tickser.notificationCount,
        phone: tickser.phone,
        isPhoneVerified: tickser.isPhoneVerified,
        verificationCodeExists: tickser.phoneVerificationCode != null,
        socialImageUrl: tickser.socialImageUrl,
        profileImage: tickser.profileImage ? {
          id: tickser.profileImage.id,
          key: tickser.profileImage.file.key
        } : null,
        profileImageUrl: tickser.profileImage
          ? AwsService.getResizeImageUrl(tickser.profileImage.file.key, 120)
          : tickser.socialImageUrl,
        preferences: tickser.preferences != null ? tickser.preferences : {},
        status: tickser.status,

        financialAccount: tickser.account ? {
          autoAuthorize: tickser.account.autoAuthorize,
          autoAuthorizeMaxTicks: tickser.account.autoAuthorizeMaxTicks,
          autoTopupAmount: tickser.account.autoTopupAmount,
          autoTopupMethod: tickser.account.autoTopupMethod,
          currentBalance: {
            currentBalance: tickser.account.currentBalance,
            currentBalanceFreeTicks: tickser.account.currentBalanceFreeTicks,
            currentBalanceEarnedTicks: tickser.account.currentBalanceEarnedTicks
          },
          paymentMethod: tickser.account.preferredPaymentMethod,
          currencyId: tickser.account.currencyId
        } : null,

        socialAccounts: _.map(tickser.socials, function (social: any) {
          return {id: social.id, type: social.type, socialId: social.socialId};
        }),

        categoryFilter: tickser.categoryFilter,
        languageFilter: tickser.languageFilter,

        oldPasswordExists: !_.isEmpty(tickser.password)
      };

      return info;
    }
  }

}

export = AccountMessages