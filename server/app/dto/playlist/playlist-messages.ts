/// <reference path="../../typings/tsd.d.ts"/>

import * as _ from 'lodash';
import * as jsonValidator from '../validator/json-validator';
import * as ContentMessages from '../content/content-messages'
import {
  Playlist,
  Content
} from '../../models/models';
import {PermissionMap, Permission} from '../../services/content-access-service';

module PlaylistMessages {

  // to show common playlist data (e.g. in sidebar)
  export interface IPlaylistSimpleGetResponse {
    id: number;
    name: string;
    coverImage: {
      id: number,
      key: string
    };
  }

  export class PlaylistSimpleGetResponse {
    static toJson(playlist: Playlist.Instance): IPlaylistSimpleGetResponse {
      return {
        id: playlist.id,
        name: playlist.name,
        coverImage: playlist.coverImage ? {
          id: playlist.coverImage.id,
          key: playlist.coverImage.file.key
        } : null
      };
    }
  }

  // to show playlist for end user (e.g. on playlist page)
  export interface IPlaylistGetResponse extends IPlaylistSimpleGetResponse {
    backgroundImage: {
      id: number;
      key: string;
    };
    contents: ContentMessages.IContentTeaserResponse[];
  }

  export class PlaylistGetResponse {
    static toJson(playlist: Playlist.Instance, contents: Content.Instance[], permissionMap: PermissionMap): IPlaylistGetResponse {
      const simple: IPlaylistSimpleGetResponse = PlaylistSimpleGetResponse.toJson(playlist);

      return {
        id: simple.id,
        name: simple.name,
        coverImage: simple.coverImage,

        backgroundImage: playlist.backgroundImage ? {
          id: playlist.backgroundImage.id,
          key: playlist.backgroundImage.file.key
        } : null,

        contents: _.map(contents, c => {
          const permission: Permission = permissionMap[c.id];
          return ContentMessages.ContentTeaserResponse.parseFromDB(
            c,
            permission != null ? permission.isManager : false,
            permission != null ? permission.hasAccess : false
          )
        })
      };
    }
  }

  // to show full playlist data (e.g. on playlist edit page)
  export interface IPlaylistUserGetResponse extends IPlaylistGetResponse {
    published: boolean;
  }

  export class PlaylistUserGetResponse {
    static toJson(playlist: Playlist.Instance, contents: Content.Instance[]): IPlaylistUserGetResponse {
      const response: IPlaylistSimpleGetResponse = PlaylistSimpleGetResponse.toJson(playlist);
      return {
        id: response.id,
        name: response.name,
        coverImage: response.coverImage,

        backgroundImage: playlist.backgroundImage ? {
          id: playlist.backgroundImage.id,
          key: playlist.backgroundImage.file.key
        } : null,
        contents: _.map(contents, c => ContentMessages.ContentTeaserResponse.parseFromDB(c, false, false)),
        published: playlist.published
      };
    }
  }

  // to create new playlist
  export interface IPlaylistUserPostRequest {
    name: string;
    backgroundImageId: number;
    coverImageId: number;
    published: boolean;
    contentsIds: number[];
  }

  export class PlaylistUserPostRequest {

    static parseFromJSON(json: any): IPlaylistUserPostRequest {
      jsonValidator.validateRequired(['name', 'published', 'contentsIds'], json);

      return {
        name: json.name,
        backgroundImageId: json.backgroundImageId,
        coverImageId: json.coverImageId,
        published: json.published,
        contentsIds: json.contentsIds == null ? [] : json.contentsIds
      };
    }
  }

  // Response on successful playlist creation
  export interface IPlaylistUserPostResponse {
    id: number;
  }

  export class PlaylistUserPostResponse {
    static toJson(playlist: Playlist.Instance): PlaylistUserPostResponse {
      return {
        id: playlist.id,
      };
    }
  }
}

export = PlaylistMessages;
