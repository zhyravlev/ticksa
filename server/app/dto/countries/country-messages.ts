/// <reference path="../../typings/tsd.d.ts"/>

import {Country} from "../../models/models";

module CountryMessages {

  export interface ICountryGetResponse {
    id: number;
    name: string;
    codeA2: string;
    codeA3: string;
    dialingCode: string;
    currencyId: number;
  }

  export class CountryGetResponse {

    public static parseFromDB(data: Country.Attributes): ICountryGetResponse {
      return {
        id: data.id,
        name: data.name,
        codeA2: data.a2,
        codeA3: data.a3,
        dialingCode: data.dialingCode,
        currencyId: data.currencyId
      }
    }
  }
}

export = CountryMessages;
