/// <reference path="../../typings/tsd.d.ts"/>

import * as Promise from 'bluebird';
import * as moment from 'moment';
import * as UtilitiesService from '../../services/utilities';
import {Content} from '../../models/models';

module XMLSitemapMessages {

  export interface IContentXMLResponse {
    [key: string]: { lastmod: string }
  }

  export class ContentXMLResponse {

    static async parseFromDB(content: Content.Instance): Promise<IContentXMLResponse> {
      const shortLink = ContentXMLResponse.generateSearchEngineContentShortLink(content.id);

      const xmlNode = <IContentXMLResponse>{};
      xmlNode[shortLink] = {lastmod: moment(content.updatedAt).format('YYYY-MM-DD')};

      return xmlNode;
    }

    static generateSearchEngineContentShortLink(contentId: number): string {
      return '/q/' + UtilitiesService.encodeNumberToRadix64(contentId);
    }
  }
}

export = XMLSitemapMessages;
