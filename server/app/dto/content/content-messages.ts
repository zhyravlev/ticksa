/// <reference path="../../typings/tsd.d.ts"/>
import _ = require('lodash');
import Promise = require('bluebird');

import jsonValidator = require('../validator/json-validator');
import LanguageMapper = require('../../mappers/language-mapper');
import CategoryMapper = require('../../mappers/category-mapper');
import ConditionMapper = require('../../mappers/condition-mapper');

import Models = require('../../models/models');
import Video = Models.Video;
import Content = Models.Content;
import Tag = Models.Tag;
import History = Models.History;
import Condition = Models.Condition;
import Country = Models.Country;

module ContentMessages {

  export interface IHistoryGetResponse {
    id: number;
    createdAt: Date;
    content: {
      id: number;
      title: string;
      teaserText: string;
      teaserImageKey: string;
      type: number;
      channel: {
        id: number;
        name: string;
        verified: boolean;
      }
    };
  }

  export class HistoryGetResponse {

    static parseFromDB(historyItem: History.Instance): IHistoryGetResponse {
      var teaserImageKey = null;
      if (historyItem.content.teaserImage != null) {
        teaserImageKey = historyItem.content.teaserImage.file.key;
      } else if (historyItem.content.teaserVideo != null) {
        teaserImageKey = historyItem.content.teaserVideo.thumbnailImage != null ? historyItem.content.teaserVideo.thumbnailImage.key : null;
      }

      let result: IHistoryGetResponse = {
        id: historyItem.id,
        createdAt: historyItem.createdAt,
        content: {
          id: historyItem.content.id,
          title: historyItem.content.title,
          teaserText: historyItem.content.teaserText,
          teaserImageKey: teaserImageKey,
          type: historyItem.content.type,
          channel: {
            id: historyItem.content.channel.id,
            name: historyItem.content.channel.name,
            verified: historyItem.content.channel.verified
          }
        }
      };

      return result;
    }
  }

  export interface IContentFullResponse {
    id: number;
    channelId: number;
    title: string;
    type: number;
    likes: number;
    dislikes: number;
    tickPrice: number;
    manager: boolean;
    published: boolean;
    commentCount: number;
    tags: Array<{
      id: number;
      name: string;
    }>;
    campaign: boolean;
    campaignPercent: number;
    teaserText: string;
    teaserImage: {
      imageId: number;
      name: string;
      key: string;
    };
    teaserVideo: {
      id: number;
      name: string;
      transcoded: boolean;
      thumbnailImageKey: string;
      videoFiles: Array<{
        height: number;
        width: number;
        key: string;
        bitrate: number;
      }>
    };

    description: string;
    expiresAt: Date;
    createdAt: Date;
    updatedAt: Date;

    creator: {
      id: number;
      name: string;
    };
    contentImages: Array<{
      imageId: number;
      name: string;
      key: string;
    }>;
    contentLink: {
      url: string;
    };
    contentText: {
      text: string;
    };
    contentVideo: {
      id: number;
      name: string;
      transcoded: boolean;
      thumbnailImageKey: string;
      videoFiles: Array<{
        height: number;
        width: number;
        key: string;
        bitrate: number;
      }>
    };
    conditions: ConditionMapper.IConditionResponse[],
    geographicCountries: IGeographicCountryDTO[],
    secondsFromBuy: number;
    category: CategoryMapper.ICategory;
    language: LanguageMapper.ILanguage;
  }

  export class ContentFullResponse {

    // Magic number that says to webapp that refund is not possible (bigger than 30 seconds)
    public static TOO_MUCH_TIME_PASSED: number = 31;
    private static REFUND_IMPOSSIBLE_BECAUSE_MANAGER: number = ContentFullResponse.TOO_MUCH_TIME_PASSED;

    constructor() {
    }

    static async parseFromDB(content: Content.Instance, manager: boolean, secondsFromBuy?: number): Promise<IContentFullResponse> {

      const conditions = await ConditionMapper.map(content.conditions);

      return {
        id: content.id,
        channelId: content.channelId,
        title: content.title,
        type: content.type,
        likes: content.likes,
        dislikes: content.dislikes,
        tickPrice: content.tickPrice,
        manager: manager,
        published: content.published,
        commentCount: <any> content.commentCount,
        tags: _.map(content.tags, function (tag: any) {
          return {id: tag.id, name: tag.name}
        }),
        campaign: content.campaign,
        campaignPercent: content.campaignPercent ? content.campaignPercent * 100 : null,
        teaserText: content.teaserText,
        teaserImage: content.teaserImage ? {
          imageId: content.teaserImage.id,
          name: content.teaserImage.name,
          key: content.teaserImage.file.key
        } : null,
        teaserVideo: content.teaserVideo ? {
          id: content.teaserVideo.id,
          name: content.teaserVideo.name,
          transcoded: content.teaserVideo.transcodingStatus == Video.TranscodingStatus.COMPLETE,
          thumbnailImageKey: content.teaserVideo.thumbnailImage ? content.teaserVideo.thumbnailImage.key : null,
          videoFiles: _.map(content.teaserVideo.videoFiles, function (videoFile: any) {
            return {
              height: videoFile.height,
              width: videoFile.width,
              key: videoFile.file.key,
              bitrate: videoFile.bitrate
            }
          })
        } : null,

        description: content.description,
        expiresAt: content.expiryDate,
        createdAt: content.createdAt,
        updatedAt: content.updatedAt,

        creator: {id: content.tickser.id, name: content.tickser.name},
        contentImages: _.map(content.contentImages, function (contentImage: any) {
          return {imageId: contentImage.image.id, name: contentImage.image.name, key: contentImage.image.file.key}
        }),
        contentLink: content.contentLink ? {url: content.contentLink.url} : null,
        contentText: content.contentText ? {text: content.contentText.text} : null,
        contentVideo: content.contentVideo ? {
          id: content.contentVideo.video.id,
          name: content.contentVideo.video.name,
          transcoded: content.contentVideo.video.transcodingStatus == Video.TranscodingStatus.COMPLETE,
          thumbnailImageKey: content.contentVideo.video.thumbnailImage ? content.contentVideo.video.thumbnailImage.key : null,
          videoFiles: _.map(content.contentVideo.video.videoFiles, function (videoFile: any) {
            return {
              height: videoFile.height,
              width: videoFile.width,
              key: videoFile.file.key,
              bitrate: videoFile.bitrate
            }
          })
        } : null,
        conditions: conditions,
        geographicCountries: geographicCountries(content.geographicCountries),
        secondsFromBuy: manager ? this.REFUND_IMPOSSIBLE_BECAUSE_MANAGER : secondsFromBuy,
        language: LanguageMapper.map(content.languageId, content.language),
        category: CategoryMapper.map(content.categoryId, content.category)
      };

      function geographicCountries(geographicCountries: string[]): IGeographicCountryDTO[] {
        if (geographicCountries == null) {
          return null;
        }
        return _.map(geographicCountries, function (key: string) {
          return {key: Country.countryCodeToUpperCase(key)};
        });
      }
    }
  }

  export interface IContentTeaserResponse {
    id: number;
    title: string;
    type: number;
    likes: number;
    dislikes: number;
    tickPrice: number;
    textLength: number;
    imageCount: number;
    videoLength: number;
    bought: boolean;
    manager: boolean;
    published: boolean;
    createdAt: Date;
    updatedAt: Date;
    haveCondition: boolean;
    haveGeographicLimited: boolean;

    tags: Tag.Attributes[];
    channel: {
      id: number;
      name: string;
      verified: boolean;
      coverImage: {
        id: number;
        key: string;
      };
      background: {
        id: number;
        key: string;
      }
    };
    teaserText: string;
    teaserImage: {
      name: string;
      key: string;
    };
    teaserVideo: {
      name: string;
      transcoded: boolean;
      thumbnailImageKey: string;
      videoFiles: Array<{
        height: number;
        width: number;
        key: string;
        bitrate: number;
      }>
    };
  }

  export class ContentTeaserResponse {

    static parseFromDB(content: Content.Instance, isManager: boolean, isBought: boolean): IContentTeaserResponse {
      let response: IContentTeaserResponse = {
        id: content.id,
        title: content.title,
        type: content.type,
        likes: content.likes,
        dislikes: content.dislikes,
        tickPrice: content.tickPrice,
        textLength: content.textLength,
        imageCount: content.imageCount,
        videoLength: content.videoLength,
        bought: isBought,
        manager: isManager,
        published: content.published,
        createdAt: content.createdAt,
        updatedAt: content.updatedAt,
        haveCondition: !_.isEmpty(content.conditions),
        haveGeographicLimited: !_.isEmpty(content.geographicCountries),
        channel: {
          id: content.channel.id,
          name: content.channel.name,
          verified: content.channel.verified,
          coverImage: content.channel.coverImage ? {
            id: content.channel.coverImage.id,
            key: content.channel.coverImage.file.key
          } : null,
          background: content.channel.backgroundImage ? {
            id: content.channel.backgroundImage.id,
            key: content.channel.backgroundImage.file.key
          } : null
        },
        tags: _.map(content.tags, function (tag: Tag.Instance) {
          return tag.get({plain: true, clone: true});
        }),
        teaserText: content.teaserText,
        teaserImage: content.teaserImage ? {name: content.teaserImage.name, key: content.teaserImage.file.key} : null,
        teaserVideo: content.teaserVideo ? {
          name: content.teaserVideo.name,
          transcoded: content.teaserVideo.transcodingStatus == Video.TranscodingStatus.COMPLETE,
          thumbnailImageKey: content.teaserVideo.thumbnailImage ? content.teaserVideo.thumbnailImage.key : null,
          videoFiles: _.map(content.teaserVideo.videoFiles, function (videoFile: any) {
            return {
              height: videoFile.height,
              width: videoFile.width,
              key: videoFile.file.key,
              bitrate: videoFile.bitrate
            }
          })
        } : null,
      };

      return response;
    }
  }

  export class ContentVideoPostDto {
    videoId: number;
    videoLength: number;

    constructor(videoId: number, videoLength: number) {
      this.videoId = videoId;
      this.videoLength = videoLength;
    }

    static parseFromJSON(json): ContentVideoPostDto {
      jsonValidator.validateRequired(['videoId'], json);
      return new ContentVideoPostDto(json.videoId, json.videoLength);
    }
  }

  export class ContentImagePostDto {
    imageId: number;
    position: number;

    constructor(imageId: number, position: number) {
      this.imageId = imageId;
      this.position = position;
    }

    static parseFromJSON(json): ContentImagePostDto {
      jsonValidator.validateRequired(['imageId'], json);
      return new ContentImagePostDto(json.imageId, json.position);
    }
  }

  export class ContentTextPostDto {
    text: string;

    constructor(text: string) {
      this.text = text;
    }

    static parseFromJSON(json): ContentTextPostDto {
      jsonValidator.validateRequired(['text'], json);
      return new ContentTextPostDto(json.text);
    }
  }

  export class ContentLinkPostDto {
    url: string;

    constructor(url: string) {
      this.url = url;
    }

    static parseFromJSON(json): ContentLinkPostDto {
      jsonValidator.validateRequired(['url'], json);
      return new ContentLinkPostDto(json.url);
    }
  }

  export interface IGeographicCountryDTO {
    key: string;
  }

  export interface IContentPostRequest {
    title: string;
    teaserText: string;
    teaserImageId: number;
    teaserVideoId: number;
    type: number;
    description: string;
    published: boolean;
    tickPrice: number;
    tags: {
      id: number
    }[];
    conditions: ConditionMapper.IConditionRequest[];
    campaign: boolean;
    campaignPercent: number;
    channelId: number;
    languageId: number;
    categoryId: number;
    geographicCountries: string[];

    contentImages: ContentImagePostDto[];
    contentVideo: ContentVideoPostDto;
    contentLink: ContentLinkPostDto;
    contentText: ContentTextPostDto;
  }

  export class ContentPostRequest {

    static parseFromJSON(json): IContentPostRequest {
      jsonValidator.validateRequired(['title', 'type', 'tickPrice', 'languageId', 'categoryId'], json);
      return {
        title: json.title,
        teaserText: json.teaserText,
        teaserImageId: json.teaserImageId,
        teaserVideoId: json.teaserVideoId,
        type: json.type,
        description: json.description,
        tickPrice: json.tickPrice,
        tags: json.tags,
        campaign: json.campaign,
        campaignPercent: json.campaignPercent ? json.campaignPercent / 100 : null,
        channelId: json.channelId,
        published: json.published,
        contentImages: contentImages(json.contentImages),
        conditions: conditions(json.conditions),
        geographicCountries: geographicCountries(json.geographicCountries),
        contentVideo: json.contentVideo ? ContentVideoPostDto.parseFromJSON(json.contentVideo) : null,
        contentLink: json.contentLink ? ContentLinkPostDto.parseFromJSON(json.contentLink) : null,
        contentText: json.contentText ? ContentTextPostDto.parseFromJSON(json.contentText) : null,
        languageId: json.languageId,
        categoryId: json.categoryId,
      };

      function geographicCountries(countries: IGeographicCountryDTO[]): string[] {
        if (countries == null) {
          return null;
        }
        return _.chain(countries)
          .map((c: IGeographicCountryDTO) => {
            jsonValidator.validateRequired(['key'], c);
            return Country.countryCodeToUpperCase(c.key);
          })
          .uniq()
          .value();
      }

      function contentImages(images: any): ContentImagePostDto[] {
        if (images == null) {
          return null;
        }
        return _.map(images, contentImage => ContentImagePostDto.parseFromJSON(contentImage));
      }

      function conditions(conditions: ConditionMapper.IConditionRequest[]): ConditionMapper.IConditionRequest[] {
        if (conditions == null) {
          return null;
        }
        return _.map(conditions, (condition: ConditionMapper.IConditionRequest) => {
          jsonValidator.validateRequired(['type', 'content'], condition);
          return {type: condition.type, content: {id: condition.content.id}};
        });
      }
    }
  }

  export interface IContentSearchResponse {
    id: number;
    title: string;
    type: Content.Type;
    createdAt: Date;
    tickPrice: number;
    teaserText: string;
    teaserImage: {
      imageId: number,
      name: string,
      key: string
    },
    teaserVideo: {
      thumbnailImageKey: string;  // TODO [d.borisov] because the issue in ContentService.searchContent it is empty
    },
    channel: {
      id: number;
      name: string;
      coverImage: {
        id: number;
        key: string;
      };
      background: {
        id: number;
        key: string;
      }
    };
  }

  export class ContentSearchResponse {
    public static parseToJson(content: Content.Instance): IContentSearchResponse {
      return {
        id: content.id,
        title: content.title,
        type: content.type,
        createdAt: content.createdAt,
        tickPrice: content.tickPrice,
        teaserText: content.teaserText,
        teaserImage: content.teaserImage ? {
          imageId: content.teaserImage.id,
          name: content.teaserImage.name,
          key: content.teaserImage.file.key
        } : null,
        teaserVideo : content.teaserVideo ? {
          thumbnailImageKey: content.teaserVideo.thumbnailImage ? content.teaserVideo.thumbnailImage.key : null
        } : null,
        channel: {
          id: content.channel.id,
          name: content.channel.name,
          coverImage: content.channel.coverImage ? {
            id: content.channel.coverImage.id,
            key: content.channel.coverImage.file.key
          } : null,
          background: content.channel.backgroundImage ? {
            id: content.channel.backgroundImage.id,
            key: content.channel.backgroundImage.file.key
          } : null
        },
      }
    }
  }
}

export = ContentMessages;
