/// <reference path="../../typings/tsd.d.ts"/>

import jsonValidator = require('../validator/json-validator');
import Promise = require("bluebird");
import _ = require('lodash');

import Models = require('../../models/models')
import Image = Models.Image;
import Video = Models.Video;
import VideoFile = Models.VideoFile;

module LibraryMessages {

  export class MediaGetResponse {
    private static TYPE_IMAGE: number = 0;
    private static TYPE_VIDEO: number = 1;

    type: number;

    createdAt: Date;
    image: ImageGetResponse;
    video: VideoGetResponse;

    constructor(type: number, image: ImageGetResponse, video: VideoGetResponse) {
      this.type = type;
      if (type == 0) {
        this.createdAt = image.createdAt;
      } else {
        this.createdAt = video.createdAt;
      }
      this.image = image;
      this.video = video;
    }

    public static forImage(image: Image.Instance): MediaGetResponse {
      return new MediaGetResponse(MediaGetResponse.TYPE_IMAGE, new ImageGetResponse(image), null);
    }

    public static forVideo(video: Video.Instance): MediaGetResponse {
      return new MediaGetResponse(MediaGetResponse.TYPE_VIDEO, null, new VideoGetResponse(video));
    }
  }

  export class FileDto {
    name: string;
    extension: string;
    key: string;
    size: number;
    isUploaded: boolean;

    constructor(name: string, extension: string, key: string, size: number, isUploaded: boolean) {
      this.name = name;
      this.extension = extension;
      this.key = key;
      this.size = size;
      this.isUploaded = isUploaded;
    }

    static parseFromJSON(json): FileDto {
      jsonValidator.validateRequired(['name', 'key', 'size', 'isUploaded'], json, 'file');
      return new FileDto(json.name, json.extension, json.key, json.size, json.isUploaded);
    }
  }

  export class ImageGetResponse {
    id: number;
    name: string;
    fileName: string;
    key: string;
    createdAt: Date;
    updatedAt: Date;

    constructor(image: Image.Instance) {
      if (!image) {
        return;
      }

      this.id = image.id;
      this.name = image.name;
      this.fileName = image.file.name;
      this.key = image.file.key;
      this.createdAt = image.created_at;
      this.updatedAt = image.updated_at;
    }

  }

  export class ImagePostRequest {
    name: string;
    file: FileDto;

    constructor(name: string, file: FileDto) {
      this.name = name;
      this.file = file;
    }

    static parseFromJSON(json): ImagePostRequest {
      jsonValidator.validateRequired(['name', 'file'], json);
      return new ImagePostRequest(json.name, FileDto.parseFromJSON(json.file));
    }
  }

  export class VideoPostRequest {
    name: string;
    file: FileDto;

    constructor(name: string, file: FileDto) {
      this.name = name;
      this.file = file;
    }

    static parseFromJSON(json): VideoPostRequest {
      jsonValidator.validateRequired(['name', 'file'], json);
      return new VideoPostRequest(json.name, FileDto.parseFromJSON(json.file));
    }
  }

  export interface VideoFilesGetResponse {
    key: string,
    width: number,
    height: number,
    size: number,
    bitrate: number
  }

  export class VideoGetResponse {
    id: number;
    name: string;
    durationInMillis: number;
    transcodingStatus: number;
    createdAt: Date;
    updatedAt: Date;
    thumbnailImageKey: string;
    videoFiles: VideoFilesGetResponse[];

    constructor(video: Video.Instance) {
      this.id = video.id;
      this.name = video.name;
      this.durationInMillis = video.durationInMillis;
      this.transcodingStatus = video.transcodingStatus;
      this.createdAt = video.created_at;
      this.updatedAt = video.updated_at;
      this.thumbnailImageKey = video.thumbnailImage ? video.thumbnailImage.key : null;
      this.videoFiles = _.map(video.videoFiles, function (videoFile: VideoFile.Instance): VideoFilesGetResponse {
        return {
          key: videoFile.file.key,
          width: videoFile.width,
          height: videoFile.height,
          size: videoFile.file.size,
          bitrate: videoFile.bitrate
        }
      });
    }
  }
}

export = LibraryMessages;
