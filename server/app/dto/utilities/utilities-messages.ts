/// <reference path="../../typings/tsd.d.ts"/>

export = UtilitiesMessages;

module UtilitiesMessages {

  export class EncodeNumberGetResponse {
    encoded: string;
    constructor(encoded) {
      this.encoded = encoded;
    }
  }

  export class EncodeContentAndUserIdGetResponse {
    contentId: string;
    userId: string;
    constructor(contentId, userId) {
      this.contentId = contentId;
      this.userId = userId;
    }
  }

  export class EncodePlaylistIdGetResponse {
    playlistId: string;
    constructor(playlistId) {
      this.playlistId = playlistId;
    }
  }
}
