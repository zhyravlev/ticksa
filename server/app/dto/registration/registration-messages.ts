/// <reference path="../../typings/tsd.d.ts"/>

import * as jsonValidator from '../validator/json-validator';

module RegistrationMessages {

  export interface IUserRegistrationRequest {
    username: string;
    password: string;
    name: string;
    recaptcha: string;
    language: string;
    redirectUrl: string;
    referrerId: number
  }

  export class UserRegistrationRequest {

    static parseFromJSON(json): IUserRegistrationRequest {
      jsonValidator.validateRequired(['username', 'password', 'name', 'recaptcha'], json);

      return {
        username: json.username,
        name: json.name,
        password: json.password,
        recaptcha: json.recaptcha,
        referrerId: json.referrerId,
        redirectUrl: json.redirectUrl,
        language: json.language
      };
    }
  }

}

export = RegistrationMessages