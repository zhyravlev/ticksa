/// <reference path="../../typings/tsd.d.ts"/>

module FacebookMessages {

  export interface IFacebookMeResponse {
    id: string;
    name: string;
    email: string;
    picture: {
      data: {
        is_silhouette: boolean;
        url: string
      }
    },
    error?: any
  }

}

export = FacebookMessages