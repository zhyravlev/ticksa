/// <reference path="../../typings/tsd.d.ts"/>
const logger = require('../../logger/logger')(__filename);

import * as _ from 'lodash';
import * as util from 'util';
import * as Errors from '../../errors';

export function validateRequired(requiredFields:Array<string>, body:any, entity?: string) {

  _.forEach(requiredFields, field => {
    if (body[field] == null) {
      var fieldName = entity ? entity + '.' + field : field;
      logger.error('Field "' + fieldName + '" is required, but not found in ' + util.inspect(body));
      throw new Errors.BadRequest('Field "' + fieldName + '" is required');
    }
  });

  return {success: 'All required fields validated'}
}

export function validateNotNull(json) {
  if (json == null) {
    throw new Errors.BadRequest('Data must not be null');
  }
  return {success: 'Data is not null'}
}
