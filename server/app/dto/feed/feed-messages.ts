import jsonValidator = require('../validator/json-validator');
import _ = require('lodash');
import AwsService = require('../../services/aws');

import Models = require('../../models/models');
import Video = Models.Video;
import Feed = Models.Feed;

module FeedMessages {

  export class FeedPostRequest {
    text: string;
    parentId: number;
    videoId: number;
    imageId: number;

    constructor(text: string, parentId: number, videoId: number, imageId: number) {
      this.text = text;
      this.parentId = parentId;
      this.videoId = videoId;
      this.imageId = imageId;
    }

    static parseFromJSON(json): FeedPostRequest {
      jsonValidator.validateRequired([], json);
      return new FeedPostRequest(json.text, json.parentId, json.videoId, json.imageId);
    }
  }

  export interface IFeedGetResponse {
    id: number;
    text: string;
    likes: number;
    dislikes: number;
    createdAt: Date;
    updatedAt: Date;
    parentId: number;
    creator: {
      id: number;
      name: string;
      email: string;
      profileImageUrl: string;
    };
    image: {
      name: string;
      key: string;
    };
    video:{
      name: string;
      transcoded: boolean;
      thumbnailImageKey: string;
      videoFiles: Array<{
        key: string;
        bitrate: number;
      }>
    };
  }

  export class FeedGetResponse {
    static parseFromDB(feed: Feed.Instance): IFeedGetResponse {

      let response: IFeedGetResponse = {
        id: feed.id,
        text: feed.text,
        likes: feed.likes,
        dislikes: feed.dislikes,
        createdAt: feed.created_at,
        updatedAt: feed.updated_at,
        parentId: feed.parentId,
        creator: {
          id: feed.tickser.id,
          name: feed.tickser.name,
          email: feed.tickser.email,
          profileImageUrl: feed.tickser.profileImage
            ? AwsService.getResizeImageUrl(feed.tickser.profileImage.file.key, 120)
            : feed.tickser.socialImageUrl
        },
        image: feed.image ? {name: feed.image.name, key: feed.image.file.key} : null,
        video: feed.video ? {
          name: feed.video.name,
          transcoded: feed.video.transcodingStatus == Video.TranscodingStatus.COMPLETE,
          thumbnailImageKey: feed.video.thumbnailImage ? feed.video.thumbnailImage.key : null,
          videoFiles: _.map(feed.video.videoFiles, function (videoFile: any) {
            return {key: videoFile.file.key, bitrate: videoFile.bitrate}
          })
        } : null
      };

      return response;
    }
  }
}

export = FeedMessages;
