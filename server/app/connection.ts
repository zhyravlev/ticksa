/// <reference path="./typings/tsd.d.ts"/>
const logger = require('./logger/logger')('sequelize');

import _ = require('lodash');
import Sequelize = require('sequelize');
import config = require('./config');

const pg = require('pg');
pg.defaults.parseInt8 = true;

require('pg-parse-float')(pg);

// logging option is declared in current file because if it does in configuration files then it leads to
// circular dependencies issues
const options: Sequelize.Options = _.cloneDeep(config.env().database.settings);
options.logging = logger.info;

const connection: Sequelize.Sequelize = new Sequelize(
  config.env().database.name,
  config.env().database.username,
  config.env().database.password,
  options);

export = connection;
