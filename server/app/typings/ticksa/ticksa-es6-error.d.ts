/// <reference path="../node/node.d.ts" />

// copied from here: http://stackoverflow.com/questions/23568606
/* tslint:disable */

declare module 'es6-error' {

  module ES6Error {

    // here we explicitly define fields of Error object
    interface ES6Error extends Error {
      message: string;
      name: string;
      stack: string;
    }

    interface ES6ErrorStatic extends ES6Error {
      new(msg: string): ES6Error;
    }
  }

  var ES6Error: ES6Error.ES6ErrorStatic;

  export = ES6Error;
}
