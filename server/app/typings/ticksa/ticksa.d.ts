/// <reference path="../tsd.d.ts" />

/* tslint:disable */

declare module "ticksa" {

  import express = require('express');
  import Sequelize = require('sequelize');
  import ExpressValidator = require('express-validator');

  module Ticksa {

    import RequestValidation = ExpressValidator.RequestValidation;

    interface Next extends express.NextFunction {
    }

    interface Response extends express.Response {
    }

    interface Request extends RequestValidation, express.Request {
      local: IRequestLocalData;
      assertValid(): void;
    }

    interface IRequestLocalData {
      language: string;
      geoip: IGeoipData;
      user?: any;
      forLogging?: {
        tickserId: number
      }
    }

    interface IGeoipData {
      ip: string;
      countryA2Code: string;
    }

    export interface Model<TInstance, TAttributes> extends Sequelize.Model<TInstance, TAttributes>, ClassMethods {
      addScope(name: string, scope: Sequelize.FindOptions | Function, options?: {override: boolean});
    }

    export interface ClassMethods {
      associate(models): void;
      addScopes?(models): void;
    }

    export interface IPageable<T> {
      limit: number;
      offset: number;
      searchString: string;
      filter: T;
    }

    module Configuration {

      interface GlobalSettings {
        maxChannels?: number;
        contentExpiryExtensionTimeInSeconds?: number;
        extendContentPrice?: number;
        contentExpiryTimeInSeconds?: number;
        contentPostPrice?: number;
        feedPostPrice?: number;
        videoPer200MBTickPrice?: number;
        euroToTicksRate?: number;
        adminCostFreeTicksPercent?: number;
        campaignFreeTicksPercent?: number;
        adminCostPercent?: number;
        campaignTicksPercent?: number;
        voucherInvalidChecksToBan?: number;
        voucherMaxCount?: number;
        registrationFreeTicks?: number;
      }

      interface SmsConfig {
        accountSid: string;
        authToken: string;
        phoneNumber: string;
      }

      interface MailConfig {
        secretKey: string;
      }

      interface SecurityConfig {
        secret: string;
        tokenExpirationIn: string; // https://github.com/rauchg/ms.js
        facebook: {
          secret: string;
          id: string;
        };
        google: {
          secret: string;
          id: string;
          publicKey: string;
        };
        twitter: {
          secret: string;
          id: string;
        }
      }

      interface RecaptchaConfig {
        id: string;
        secret: string;
      }

      interface RedisConfig {
        host: string;
        port: number;
      }
      
      interface DatabaseConfig {
        name: string;
        username: string;
        password: string;
        settings: Sequelize.Options;
        forceSync?: boolean;
        initDB?: boolean;
      }

      type InfinMode = 'prod' | 'test';

      interface InfinFinanceConfig {
        mode: InfinMode;
        secretKey: string;
        serviceName?: string;
        apiKey?: string;
        url?: string;
      }

      interface PayPalFinanceConfig {
        paymentServerUrl: string;
        api: {
          mode?: string;
          host: string;
          client_id: string;
          client_secret: string;
        }
      }

      interface LemonwayFinanceConfig {
        wlLogin: string;
        wlPass: string;
        directKitWSDLFile: string;
        webkitUrl: string;
      }

      interface FinanceConfig {
        infin: InfinFinanceConfig;
        payPal: PayPalFinanceConfig;
        lemonway: LemonwayFinanceConfig;
      }

      interface CDNConfig {
        url: string;
        urlWithTimestamp?: string;
      }

      interface ServerConfig {
        port: string;
        url: string;
      }

      interface AWSConfig {
        s3: {
          url: string;
          bucket: string;
          cloudFront: string;
          nginx: string;
        },
        region: string;
        access_key_id: string;
        secret_access_key: string;
        transcoder: {
          pipeline: string;
          sqs: {
            url: string;
          },
          qualityForThumbnails: string;
          qualities: {
            G720P: {
              preset: string;
              bitrate: number;
              thumbnailExtension: string;
            },
            G360P16x9: {
              preset: string;
              bitrate: number;
              thumbnailExtension: string;
            }
          }
        }
      }

      type LogTransportType = 'console';

      // https://github.com/winstonjs/winston#logging-levels
      type LogLevelType = 'error' | 'warn' | 'info' | 'verbose' | 'debug' | 'silly';

      interface LogTransport {
        type: LogTransportType;
        level: LogLevelType;
      }

      interface LogConfig {
        transports: LogTransport[];
      }

      interface SiteVerificationConfig {
        name: string,
        value: string,
      }

      interface EnvConfig {
        cdn: CDNConfig;
        server: ServerConfig;
        recaptcha: RecaptchaConfig;
        mail: MailConfig;
        security: SecurityConfig;
        sms: SmsConfig;
        aws: AWSConfig;
        redis: RedisConfig;
        database: DatabaseConfig;
        finance: FinanceConfig;
        log: LogConfig;
        siteVerification: SiteVerificationConfig[];
      }

      interface FullConfiguration {
        global: GlobalSettings;
        env(): EnvConfig;
        isLocal(): boolean;
        isTest(): boolean;
        isQA(): boolean;
        isStaging(): boolean;
        isProduction(): boolean;
        getMode(): string;
        getTemplateFilePath(path: string): string;
        DATE_FORMAT: string;
        DATE_TIME_FORMAT: string;
        test: EnvConfig;
        local: EnvConfig;
        qa: EnvConfig;
        staging: EnvConfig;
        production: EnvConfig;
      }
    }
  }

  export = Ticksa;
}

