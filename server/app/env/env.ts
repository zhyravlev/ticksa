/// <reference path="../typings/tsd.d.ts"/>

const POSSIBLE_ENVIRONMENTS: string[] = [
  'local',
  'qa',
  'staging',
  'production'
  //'test'                // disabled because server never starts with this environment
];

export function throwExceptionIfInvalidEnv() {
  if (POSSIBLE_ENVIRONMENTS.indexOf(process.env.mode) === -1) {
    throw new Error('Environment variable mode must be in array: ' + POSSIBLE_ENVIRONMENTS);
  }
}
