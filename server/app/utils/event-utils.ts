/// <reference path="../typings/tsd.d.ts"/>

import events = require('events');
import Promies = require('bluebird');

export function waitForOnce(emitter: events.EventEmitter, event: string, timeout: number, fn: Function): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    let timer: NodeJS.Timer;

    const handler = function () {
      clearTimeout(timer);
      fn.apply(this, arguments);
      resolve();
    };

    timer = setTimeout(function () {
      emitter.removeListener(event, handler);
      reject(new Error('Event timeout expired ' + timeout));
    }, timeout);

    emitter.on(event, handler);
  });
}