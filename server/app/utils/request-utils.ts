/// <reference path="../typings/tsd.d.ts"/>
import _ = require('lodash');
import jade = require('jade');
import Ticksa = require('ticksa');
import config = require('../config');

module RequestUtils {

  export function getPageable(req: Ticksa.Request): Ticksa.IPageable<void> {
    const DEFAULT_PAGE_SIZE: number = 10;

    const limit: number = req.query.pageSize ? parseInt(req.query.pageSize) : DEFAULT_PAGE_SIZE;
    const offset: number = req.query.page ? parseInt(req.query.page) * limit : 0;
    const searchString: string = req.query.searchString;

    return {
      limit: limit,
      offset: offset,
      searchString: searchString,
      filter: null
    }
  }

  export interface ContentFilter {
    languages: number[];
    categories: number[];
  }

  export function getPageableContentFilter(req: Ticksa.Request): Ticksa.IPageable<ContentFilter> {
    const pageable: Ticksa.IPageable<void> = getPageable(req);
    const filter: ContentFilter = {
      languages: getArrayFilterValue(req.query.filter, 'languages'),
      categories: getArrayFilterValue(req.query.filter, 'categories')
    };

    return {
      limit: pageable.limit,
      offset: pageable.offset,
      searchString: pageable.searchString,
      filter: filter
    }
  }

  export function renderIndexView(): string {
    if (!config.env().cdn.urlWithTimestamp) {
      return 'Server error! No webapp static data found. Please email us: support@ticksa.com';
    }

    return jade.renderFile(config.getTemplateFilePath('index.jade'), {
      data: {
        baseUrl: config.env().cdn.urlWithTimestamp,
        mode: config.getMode(),
        siteVerification: config.env().siteVerification
      }
    });
  }

  function getArrayFilterValue(filter: any, key: string): any[] {
    if (!filter) {
      return [];
    }

    const value = filter[key];

    if (value == null) {
      return [];
    }

    if (_.isArray(value)) {
      return value;
    }

    return [value];
  }
}

export = RequestUtils;