/*
 * This function creates correct link for browsers.
 *
 * According to APP-757 in webapp Ticksa uses Angular's feature "html5mode"
 * https://code.angularjs.org/1.4.8/docs/guide/$location#hashbang-and-html5-modes
 *
 * There are older browsers that did not implement history.pushState feature, so Angular htl5mode will not work.
 * To let them correctly work with Ticksa links - server application always creates them with hashbang (#!).
 *
 * So if user's browser has history.pushState feature then url became without hashbang. Angular will do it on
 * client-side.  E.g.  http://ticksa.com/#!/teaser/1/4   ->   http://ticksa.com/teaser/1/4
 *
 * Hashbang is chosen because FB crawler makes correct requests with it.
 * Server will be requested to urls with param "_escaped_fragment_"
 * https://developers.google.com/webmasters/ajax-crawling/docs/specification#bidirectional-between--url-to-_escaped_fragment_-url
 *
 * E.g. http://ticksa.com/#!/channels/1/contents/2   ->  http://ticksa.com/?_escaped_fragment_=%2Fchannels%2F1%2Fcontents%2F2
 *
 *
 * There is a documentation that sais that Angular can automatically change url in modern/legacy browsers. So basically
 * we can just take out hashbang
 * https://code.angularjs.org/1.4.8/docs/guide/$location#hashbang-and-html5-modes#sending-links-among-different-browsers
 *
 * For now (2016-06-10) we leave hashbang in urls.
 */
export function tl(str: string): string {
  return "/#!" + str;
}