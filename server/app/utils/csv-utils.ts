/// <reference path="../typings/tsd.d.ts"/>

import stream = require('stream');

// see 'csv' library for details.
// node_modules/csv/node_modules/csv-stringify/lib/index.js#153  -  util.inherits(Stringifier, stream.Transform);
export function castToParentClass(stringifier: any): stream.Transform {
  return <stream.Transform> stringifier;
}