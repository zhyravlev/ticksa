/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import _ = require('lodash');
import util = require('util');

export interface KeyValueDictionaryContainer<T> {
  [key: string]: T;
}

export interface KeyValueDictionaryStringifierResult {
  needString: boolean,
  stringValue?: string
}

export interface KeyValueDictionaryStringifier<T> {
  (value: T): KeyValueDictionaryStringifierResult;
}

export class KeyValueDictionary<T> {

  private static DEFAULT_TO_STR = container => util.inspect(container);

  private static TO_STR_WITH_STRINGIFIER = (stringifier) => {
    return function (container): string {
      const keys: string[] = _.keys(container);
      const strings = _.map<string, string>(keys, k =>{
        const res = stringifier(container[k]);
        return res.needString
          ? util.format('%s: %s', k, res.stringValue)
          : util.format('%s', k);
      });

      return util.format('{%s}', strings.join(', '));
    };
  };

  private container: KeyValueDictionaryContainer<T>;
  private toStr: (container: KeyValueDictionaryContainer<T>) => string;

  constructor(stringifier?: KeyValueDictionaryStringifier<T>) {
    this.container = {};
    this.toStr = stringifier == null
      ? KeyValueDictionary.DEFAULT_TO_STR
      : KeyValueDictionary.TO_STR_WITH_STRINGIFIER(stringifier);
  }

  public static forDictionary<T>(dict: KeyValueDictionaryContainer<T>): KeyValueDictionary<T> {
    const result = new KeyValueDictionary<T>();
    _.keys(dict).forEach(k => result.set(k, dict[k]));
    return result;
  }

  public static forStringifierAndDictionary<T>(stringifier: KeyValueDictionaryStringifier<T>, dict: KeyValueDictionaryContainer<T>): KeyValueDictionary<T> {
    const result = new KeyValueDictionary<T>(stringifier);
    _.keys(dict).forEach(k => result.set(k, dict[k]));
    return result;
  }

  public set(key: string, value: T): void {
    this.container[key] = value;
  }

  public get(key: string): T {
    return this.container[key];
  }

  public getKeys(): string[] {
    return _.keys(this.container);
  }

  public getValues(): T[] {
    return _.values<T>(this.container);
  }

  public toString(): string {
    return this.toStr(this.container);
  }
}

export class ValueSelector<T> {

  private possibleElements: KeyValueDictionary<T>;
  private defaultElement: T;

  constructor(possibleElements: KeyValueDictionary<T>, defaultElement: T) {
    this.possibleElements = possibleElements;
    this.defaultElement = defaultElement;
  }

  public select(key: string): T {
    const possibleValue: T = this.possibleElements.get(key);
    if (possibleValue != null) {
      return possibleValue;
    }

    logger.warn('There is no value for key: ' + key
      + '. Possible elements: ' + this.possibleElements.toString()
      + '. Default chosen: ' + util.inspect(this.defaultElement)
    );
    return this.defaultElement;
  }

  public getKeys(): string[] {
    return this.possibleElements.getKeys();
  }

  public getValues(): T[] {
    return this.possibleElements.getValues();
  }
}