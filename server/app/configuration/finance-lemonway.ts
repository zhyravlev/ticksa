/// <reference path="../typings/tsd.d.ts"/>

import Ticksa = require('ticksa');

export const CREDENTIALS = {
  wlLogin: 'society',
  wlPass: '123456',
};

export const WSDL_FILES = {
  local: 'lemonway-local.wsdl.xml',
  qa: 'lemonway-qa.wsdl.xml',
  staging: 'lemonway-staging.wsdl.xml',
  production: 'lemonway-production.wsdl.xml'
};

export const WEBKIT_URLS = {
  local: 'http://payment.ticksa.com:2104',
  qa: 'http://payment.ticksa.com:2103',
  staging: 'http://payment.ticksa.com:2102',
  production: 'http://payment.ticksa.com:2101'
};