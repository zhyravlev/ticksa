/// <reference path="../typings/tsd.d.ts"/>

import Ticksa = require('ticksa');
import common = require('./common');
import financeInfin = require('./finance-infin');
import financePaypal = require('./finance-paypal');
import financeLemonway = require('./finance-lemonway');

const CDN: Ticksa.Configuration.CDNConfig = {
  url: 'http://localhost:3000/public'
};

const SERVER: Ticksa.Configuration.ServerConfig = {
  port: '3000',
  url: 'http://localhost:3000'
};

const DATABASE: Ticksa.Configuration.DatabaseConfig = {
  name: 'ticksa-test',
  username: 'postgres',
  password: '1234',
  settings: {
    host: 'localhost',
    port: 5432,
    dialect: 'postgres',
    benchmark: true,

    pool: {
      maxConnections: 6,
      minConnections: 1,
      maxIdleTime: 5000
    }

  },
  forceSync: true,
  initDB: true
};

const REDIS: Ticksa.Configuration.RedisConfig = {
  host: '127.0.0.1',
  port: 6379
};

const AWS: Ticksa.Configuration.AWSConfig = {
  s3: {
    url: 'https://s3-us-west-2.amazonaws.com',
    bucket: 'ticksa-dev',
    cloudFront: 'https://da7pflu8zosoc.cloudfront.net',
    nginx: 'https://d4w2yt0on2z6n.cloudfront.net',
  },
  region: 'us-west-2',
  access_key_id: 'AKIAJSKYZAVBPV7G2ZKA',
  secret_access_key: '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',
  transcoder: {
    pipeline: '1443016854403-7xdcnh',
    sqs: {
      url: 'https://sqs.us-west-2.amazonaws.com/183143154056/ticksa-transcoding-local'
    },
    qualityForThumbnails: 'G360P16x9',
    qualities: {
      G720P: {
        preset: "1444733169534-iqzb5c",
        bitrate: 2400,
        thumbnailExtension: 'jpg'
      },
      G360P16x9: {
        preset: "1463064853814-uybkzw",
        bitrate: 720,
        thumbnailExtension: 'png'
      }
    }
  }
};

const FINANCE: Ticksa.Configuration.FinanceConfig = {
  infin: financeInfin.DEVELOPMENT,
  payPal: financePaypal.LOCAL,
  lemonway: {
    wlLogin: financeLemonway.CREDENTIALS.wlLogin,
    wlPass: financeLemonway.CREDENTIALS.wlPass,
    directKitWSDLFile: financeLemonway.WSDL_FILES.local,
    webkitUrl: financeLemonway.WEBKIT_URLS.local
  }
};

const LOG: Ticksa.Configuration.LogConfig = {
  transports: [
     // {
     //   type: 'console',
     //   level: 'silly'
     // }
  ]
};

const SITE_VERIFICATION: Ticksa.Configuration.SiteVerificationConfig[] = [
  { name: 'google-site-verification', value: ''}, // google
  { name: 'msvalidate.01', value: ''} // bing
];

export const CONFIGURATION: Ticksa.Configuration.EnvConfig = {
  cdn: CDN,
  server: SERVER,
  aws: AWS,
  redis: REDIS,
  database: DATABASE,
  finance: FINANCE,
  log: LOG,
  recaptcha: common.RECAPTCHA,
  mail: common.MAIL,
  security: common.SECURITY,
  sms: common.SMS,
  siteVerification: SITE_VERIFICATION
};