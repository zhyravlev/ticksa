/// <reference path="../typings/tsd.d.ts"/>

import Ticksa = require('ticksa');

// Twilio Credentials
export const SMS: Ticksa.Configuration.SmsConfig = {
  accountSid: 'ACce88735927b2288a5240312b108b1123',
  authToken: 'e30c6b70bde3c69aa71a27d06a0ea07b',
  phoneNumber: '+4915735986555'
};

export const MAIL: Ticksa.Configuration.MailConfig = {
  secretKey: 'D1M3lLfLgC8kp78Y8Qyd'
};

export const SECURITY: Ticksa.Configuration.SecurityConfig = {
  secret: 'pk56nmas34vV',
  tokenExpirationIn: '10d',
  facebook: {
    secret: 'c32c5a5c1a80a815564a03842442c6a3',
    id: '1687957454782898'
  },
  google: {
    secret: '7lv4WMRZ0kTCQo_OqHeSGmEP',
    id: '620387887013-bp6r9rk7gn7h32quv5v7anlropddpq98.apps.googleusercontent.com',
    publicKey: 'AIzaSyB_rtDPu2AX12hoJ_K28h1JUTkr33OA8vU'
  },
  twitter: {
    secret: 'yZsj6LdY6LR3DBy2fgacbuo4y',
    id: 'aOcNVqBY1g8YvzlpRlkPrknnACluNAN9KD8qT3CEcuaeIlsDRK'
  }
};

export const RECAPTCHA: Ticksa.Configuration.RecaptchaConfig = {
  id: '6Ld3LA8TAAAAADv7zX1FOX96cOZNjeeZj7KI8_Wx',
  secret: '6Ld3LA8TAAAAADxURqaWPpX9IxzBCKnwjcE7ys6u'
};