/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';

import * as configTest from './config-test';
import * as configLocal from './config-local';
import * as configQA from './config-qa';
import * as configStaging from'./config-staging';
import * as configProduction from './config-production';

module Configuration {
  export const TEST: Ticksa.Configuration.EnvConfig = configTest.CONFIGURATION;
  export const LOCAL: Ticksa.Configuration.EnvConfig = configLocal.CONFIGURATION;
  export const QA: Ticksa.Configuration.EnvConfig = configQA.CONFIGURATION;
  export const STAGING: Ticksa.Configuration.EnvConfig = configStaging.CONFIGURATION;
  export const PRODUCTION: Ticksa.Configuration.EnvConfig = configProduction.CONFIGURATION;
}

export = Configuration;