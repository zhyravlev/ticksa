/// <reference path="../typings/tsd.d.ts"/>

import Ticksa = require('ticksa');
import common = require('./common');
import financeInfin = require('./finance-infin');
import financePaypal = require('./finance-paypal');
import financeLemonway = require('./finance-lemonway');

const CDN: Ticksa.Configuration.CDNConfig = {
  url: 'https://d2a26exmn60ooi.cloudfront.net'
};

const SERVER: Ticksa.Configuration.ServerConfig = {
  port: '3000',
  url: 'http://qa.ticksa.com'
};

const DATABASE: Ticksa.Configuration.DatabaseConfig = {
  name: 'qa',
  username: 'app_qa',
  password: 'f5dd8bb7063',
  settings: {
    host: 'pgmanager.ticksa.com',
    port: 6789,
    dialect: 'postgres',
    benchmark: true,

    pool: {
      maxConnections: 3,
      minConnections: 1,
      maxIdleTime: 5000
    }
  }
};

const REDIS: Ticksa.Configuration.RedisConfig = {
  host: 'cache.ticksa.com',
  port: 6381
};

const AWS: Ticksa.Configuration.AWSConfig = {
  s3: {
    url: 'https://s3-us-west-2.amazonaws.com',
    bucket: 'ticksa-qa',
    cloudFront: 'https://d24igwv6mu24pz.cloudfront.net',
    nginx: 'https://d3k7vzalrkl263.cloudfront.net',
  },
  region: 'us-west-2',
  access_key_id: 'AKIAJSKYZAVBPV7G2ZKA',
  secret_access_key: '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',
  transcoder: {
    pipeline: '1474544456296-5hseaa',
    sqs: {
      url: 'https://sqs.us-west-2.amazonaws.com/183143154056/ticksa-transcoding-test'
    },
    qualityForThumbnails: 'G360P16x9',
    qualities: {
      G720P: {
        preset: "1444733169534-iqzb5c",
        bitrate: 2400,
        thumbnailExtension: 'jpg'
      },
      G360P16x9: {
        preset: "1463064853814-uybkzw",
        bitrate: 720,
        thumbnailExtension: 'png'
      }
    }
  }
};

const FINANCE: Ticksa.Configuration.FinanceConfig = {
  infin: financeInfin.DEVELOPMENT,
  payPal: financePaypal.QA,
  lemonway: {
    wlLogin: financeLemonway.CREDENTIALS.wlLogin,
    wlPass: financeLemonway.CREDENTIALS.wlPass,
    directKitWSDLFile: financeLemonway.WSDL_FILES.qa,
    webkitUrl: financeLemonway.WEBKIT_URLS.qa
  }
};

const LOG: Ticksa.Configuration.LogConfig = {
  transports: [{
    type: 'console',
    level: 'silly'
  }]
};

const SITE_VERIFICATION: Ticksa.Configuration.SiteVerificationConfig[] = [
  { name: 'google-site-verification', value: 'HY7Z4vu0ZzCAJiBOlYe33478Dik_JTPF4Gu0k09rIj8'}, // google
  { name: 'msvalidate.01', value: '7EFEAE1A7606CC25CA10996F83430D85'} // bing
];

export const CONFIGURATION: Ticksa.Configuration.EnvConfig = {
  cdn: CDN,
  server: SERVER,
  aws: AWS,
  redis: REDIS,
  database: DATABASE,
  finance: FINANCE,
  log: LOG,
  recaptcha: common.RECAPTCHA,
  mail: common.MAIL,
  security: common.SECURITY,
  sms: common.SMS,
  siteVerification: SITE_VERIFICATION
};