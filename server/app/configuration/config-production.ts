/// <reference path="../typings/tsd.d.ts"/>

import Ticksa = require('ticksa');
import common = require('./common');
import financeInfin = require('./finance-infin');
import financePaypal = require('./finance-paypal');
import financeLemonway = require('./finance-lemonway');

const CDN: Ticksa.Configuration.CDNConfig = {
  url: 'https://d3u3f37j7l8o2d.cloudfront.net'
};

const SERVER: Ticksa.Configuration.ServerConfig = {
  port: '3000',
  url: 'https://www.ticksa.com'
};

const DATABASE: Ticksa.Configuration.DatabaseConfig = {
  name: 'production',
  username: 'app_production',
  password: '5cea0f65397',
  settings: {
    host: 'pgmanager.ticksa.com',
    port: 6789,
    dialect: 'postgres',
    benchmark: true,

    pool: {
      maxConnections: 3,
      minConnections: 1,
      maxIdleTime: 5000
    }
  }
};

const REDIS: Ticksa.Configuration.RedisConfig = {
  host: 'cache.ticksa.com',
  port: 6379
};

const AWS: Ticksa.Configuration.AWSConfig = {
  s3: {
    url: 'https://s3-us-west-2.amazonaws.com',
    bucket: 'ticksa-production',
    cloudFront: 'https://da7pflu8zosoc.cloudfront.net',
    nginx: 'https://d4w2yt0on2z6n.cloudfront.net',
  },
  region: 'us-west-2',
  access_key_id: 'AKIAJSKYZAVBPV7G2ZKA',
  secret_access_key: '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',
  transcoder: {
    pipeline: '1474553915557-6ti9z6',
    sqs: {
      url: 'https://sqs.us-west-2.amazonaws.com/183143154056/ticksa-transcoding-production'
    },
    qualityForThumbnails: 'G360P16x9',
    qualities: {
      G720P: {
        preset: "1444733169534-iqzb5c",
        bitrate: 2400,
        thumbnailExtension: 'jpg'
      },
      G360P16x9: {
        preset: "1463064853814-uybkzw",
        bitrate: 720,
        thumbnailExtension: 'png'
      }
    }
  }
};

const FINANCE: Ticksa.Configuration.FinanceConfig = {
  infin: financeInfin.PRODUCTION,
  payPal: financePaypal.PRODUCTION,
  lemonway: {
    wlLogin: financeLemonway.CREDENTIALS.wlLogin,
    wlPass: financeLemonway.CREDENTIALS.wlPass,
    directKitWSDLFile: financeLemonway.WSDL_FILES.production,
    webkitUrl: financeLemonway.WEBKIT_URLS.production
  }
};

const LOG: Ticksa.Configuration.LogConfig = {
  transports: [{
    type: 'console',
    level: 'silly'
  }]
};

const SITE_VERIFICATION: Ticksa.Configuration.SiteVerificationConfig[] = [
  { name: 'google-site-verification', value: 'RpbTqhj_31DeQjCmfaA7jMuzL2hN0TowUx1G7zmiNzA'}, // google
  { name: 'msvalidate.01', value: '78236A94BD0EB0BDE011A45D96FF9B38'} // bing
];

export const CONFIGURATION: Ticksa.Configuration.EnvConfig = {
  cdn: CDN,
  server: SERVER,
  aws: AWS,
  redis: REDIS,
  database: DATABASE,
  finance: FINANCE,
  log: LOG,
  recaptcha: common.RECAPTCHA,
  mail: common.MAIL,
  security: common.SECURITY,
  sms: common.SMS,
  siteVerification: SITE_VERIFICATION
};