/// <reference path="../typings/tsd.d.ts"/>

import Ticksa = require('ticksa');

const SANDBOX_API = {
  host: "api.sandbox.paypal.com",
  client_id: "AfcwRXXjn85W2RJuIBrwTp_aPsXS08zwYrok8i9j-axZkNdLC6rqyVk0R035KMDI6eoODvSlmVx3HGGb",
  client_secret: "ELdXn-v89pC0tp3Wktk6CX6ItwaFBj16WZV2X3gnL3zUiRN6i0IXazCXJuZPjbZ4buIGtzNBLRhaO7Rg"
};

export const LOCAL: Ticksa.Configuration.PayPalFinanceConfig = {
  paymentServerUrl: '', // Local configuration will be set in paypal-service.ts.
  api: SANDBOX_API
};

export const QA: Ticksa.Configuration.PayPalFinanceConfig = {
  paymentServerUrl: 'http://payment.ticksa.com:5003',
  api: SANDBOX_API
};

export const STAGING: Ticksa.Configuration.PayPalFinanceConfig = {
  paymentServerUrl: 'http://payment.ticksa.com:5002',
  api: SANDBOX_API
};

export const PRODUCTION: Ticksa.Configuration.PayPalFinanceConfig = {
  paymentServerUrl: 'http://payment.ticksa.com:5001',
  api: {
    mode: "live",
    host: "api.paypal.com",
    client_id: "AYe5I3AznjcxuY7lWUlXcQxgvlfMzFVes7-AEScycofBOv7nxjJoqnA7pGkYnEYfTdUxTPOOjVHJMOVM",
    client_secret: "EEdc0IR-miKlBSrqLlDO1nCkZ5DRONOAbh-q0rffRJ8M0181EzBwPzQEmHvtYMOoJiem3spCPz1YLz32"
  }
};