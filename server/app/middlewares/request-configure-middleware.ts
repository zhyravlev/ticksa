/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import * as GeoipService from '../services/geoip-service';
import {MiddlewareGlobalBefore} from 'routing-controllers/index';
import {MiddlewareInterface} from 'routing-controllers/index';
import {BadRequest} from '../errors';
import BaseMiddleware from './base-middleware';
const locale = require('locale');


@MiddlewareGlobalBefore()
export default class RequestConfigureMiddleware extends BaseMiddleware implements MiddlewareInterface {

  constructor() {
    super(__filename);
  }

  use(req: Ticksa.Request, res: any, next?: (err?: any) => any): any {
    GeoipService.GeoipData.getFromRequest(req)
      .then(geoipData => {
        const locales = new locale.Locales(req.headers['accept-language']);
        const language = locales.best() ? locales.best().language : 'en';

        req.local = {
          language: language,
          geoip: geoipData
        };

        req.assertValid = function () {
          const errors = req.validationErrors();
          if (errors) {
            throw new BadRequest(null, errors);
          }
        };

        next();
      })
      .catch(next);
  }

}
