/// <reference path="../typings/tsd.d.ts"/>
import * as Ticksa from 'ticksa';
import * as config from '../config';
import * as util from 'util';
import {BaseError} from '../errors';
import {MiddlewareGlobalAfter, ErrorMiddlewareInterface} from 'routing-controllers/index';
import BaseMiddleware from './base-middleware';

@MiddlewareGlobalAfter({priority: 1})
export default class ErrorMiddleware extends BaseMiddleware implements ErrorMiddlewareInterface {

  constructor() {
    super(__filename);
  }

  async error(err: any, req: Ticksa.Request, res: Ticksa.Response, next?: Ticksa.Next): Promise<void> {
    const isItAnExceptionObject = err instanceof Error;
    const isItHttpError = err instanceof BaseError;

    const errorDescription = {
      status: this.status(isItAnExceptionObject, isItHttpError, err),
      name: this.name(isItHttpError, err),
      message: this.message(req, isItHttpError, err),
      stack: this.stack(isItAnExceptionObject, err),
      body: this.body(isItHttpError, err)
    };

    if (errorDescription.status == 500 || errorDescription.status == 400) {
      this.logger.error('Error while handling request to [' + req.url + ']'
        + ' got error: name [' + errorDescription.name + ']'
        + ', message: [' + errorDescription.message + ']'
        + ', stack [' + errorDescription.stack + ']'
      );
    }

    if (config.isProduction()) {
      delete errorDescription.stack;
    }

    res.status(errorDescription.status).json(errorDescription);
  }

  private safeToString(obj: any, url: string): string {
    try {
      return util.inspect(obj);
    } catch (err) {
      this.logger.error('while describing error for request to [' + url + ']'
        + ' got exception [' + err + ']'
        + ' for object [' + obj + ']'
      );
      return '' + obj;
    }
  }

  private status(isItAnExceptionObject: boolean, isItHttpError: any, err: any): number {
    if (!isItAnExceptionObject) {
      return 500;
    }

    if (!isItHttpError) {
      return 500;
    }

    return err.status;
  }

  private name(isItHttpError: any, err: any): string {
    if (!isItHttpError) {
      return 'ServerError';
    }

    return err.name;
  }

  private message(req: Ticksa.Request, isItHttpError: any, err: any): string {
    if (!isItHttpError) {
      return this.safeToString(err, req.url);
    }

    return err.message;
  }

  private stack(isItExceptionObject: any, err: any): string {
    if (!isItExceptionObject) {
      return null;
    }

    return err.stack;
  }

  private body(isItHttpError: any, err: any): string {
    if (!isItHttpError) {
      return null;
    }

    return err.body;
  }
}