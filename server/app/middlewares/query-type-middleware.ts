/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import {MiddlewareGlobalBefore} from 'routing-controllers/index';
import {MiddlewareInterface} from 'routing-controllers/index';
import BaseMiddleware from './base-middleware';

const queryType = require('query-types');

@MiddlewareGlobalBefore()
export default class QueryTypeMiddleware extends BaseMiddleware implements MiddlewareInterface {

  constructor() {
    super(__filename);
  }

  use(req: Ticksa.Request, res: any, next?: (err?: any) => any): any {
    if(req.originalUrl.startsWith('/api/infin/callback')) {
      return next();
    }

    return queryType.middleware()(req, res, next);
  }

}
