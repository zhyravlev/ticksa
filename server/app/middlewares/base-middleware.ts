/// <reference path="../typings/tsd.d.ts"/>

import * as winston from 'winston';
import * as config from '../config';
import logger = require('../logger/logger');

export default class BaseMiddleware {
  protected logger: winston.LoggerInstance;
  protected config = config;

  constructor(filename: string) {
    this.logger = logger(filename);
  }

}