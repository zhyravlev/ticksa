/// <reference path="../typings/tsd.d.ts"/>

import * as url from 'url';
import * as Ticksa from 'ticksa';
import {MiddlewareGlobalBefore} from 'routing-controllers/index';
import {MiddlewareInterface} from 'routing-controllers/index';
import BaseMiddleware from './base-middleware';
import * as RequestUtils from '../utils/request-utils';
import SocialSharingController from '../controllers/social-sharing-controller';

@MiddlewareGlobalBefore({priority: 0})
export default class IndexOnNoRoutedMiddleware extends BaseMiddleware implements MiddlewareInterface {

  constructor() {
    super(__filename);
  }

  use(req: Ticksa.Request, res: any, next?: (err?: any) => any): any {
    if (IndexOnNoRoutedMiddleware.isRegisterRouteUrl(req.originalUrl)) {
      return next();
    }

    try {
      const requestedUrl: url.Url = url.parse(req.originalUrl);

      const isRequestToRootByCrawler = (requestedUrl.pathname === '/')
        && (req.query[SocialSharingController.ESCAPED_FRAGMENT_PARAM] != null);

      if (isRequestToRootByCrawler) {
        return next();
      }

    } catch (e) {
      this.logger.error('Error while detecting routing destination: ' + e + '. originalUrl: ' + req.originalUrl);
      return next();
    }

    res.send(RequestUtils.renderIndexView());
  }

  private static ROUTES_PREFIXES: string[] = [
    '/api/',
    '/social/',
    '/s/',
    '/channels/',
    '/teaser/',
    '/public/',
    '/q/',
    '/sitemap.xml'
  ];

  // it is public to let it be tested
  public static isRegisterRouteUrl(originalUrl: string): boolean {
    for (const prefix of IndexOnNoRoutedMiddleware.ROUTES_PREFIXES) {
      if (originalUrl.startsWith(prefix)) {
        return true;
      }
    }
    return false;
  }

}