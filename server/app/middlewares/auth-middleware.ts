/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import {Middleware, MiddlewareInterface} from 'routing-controllers';
import BaseMiddleware from './base-middleware';
import * as AuthenticationService from '../services/authentication-service';

@Middleware()
export default class AuthMiddleware extends BaseMiddleware implements MiddlewareInterface {

  constructor() {
    super(__filename);
  }

  use(req: Ticksa.Request, res: any, next?: (err?: any) => any): any {
    const tokenHeader = req.headers['token'];

    AuthenticationService.validateTokenHeader(tokenHeader)
      .then((tokenValidation: AuthenticationService.TokenHeaderValidationResult) => {
        if (tokenValidation.error != null) {
          return next(tokenValidation.error);
        }

        req.local.user = tokenValidation.tickser;
        next();
      })
      .catch(next);
  }

}
