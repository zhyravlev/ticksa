/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import BaseController from './base-controller';
import {JsonController, Req, Res, Get} from 'routing-controllers/index';
import * as LocationUtils from '../utils/ticksa-location-utils';
import * as AuthenticationService from '../services/authentication-service';
import * as SocialLoginService from '../services/social-login-service';
import {
  Tickser,
  TickserSocial,
  Content,
  Channel
} from '../models/models'
import {IFacebookMeResponse} from '../dto/facebook/facebook-messages';

const fb = require('fb');

@JsonController()
export default class SocialLoginController extends BaseController {

  private CONTENT_TYPE__IMAGE: number = 1;

  constructor() {
    super(__filename);
  }

  @Get('/social/login/facebook/content-list')
  async finishFacebookLoginAndGoToContentList(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response) {
    const isOK = this.checkCustomAndCommonPreconditions(req);
    if (!isOK) {
      res.redirect(this.config.env().server.url + LocationUtils.tl('/login'));
      return;
    }

    const redirectUri = this.config.env().server.url
      + SocialLoginService.facebookLoginRedirectUriForContentList();

    try {
      const meResponse: IFacebookMeResponse = await AuthenticationService.facebookOAuth(req.query.code, redirectUri);
      const token: AuthenticationService.Token = await this.executeFacebookLogin(req, meResponse, null);
      res.cookie('TicksaToken', token.token, {maxAge: 900000});
      res.redirect(this.config.env().server.url + LocationUtils.tl('/contents'));
    } catch (err) {
      this.logger.error('Error: ' + JSON.stringify(err));
      res.redirect(this.config.env().server.url + LocationUtils.tl('/login'));
    }
  }

  @Get('/social/login/facebook/content-add-type-:contentType')
  async finishFacebookLoginAndGoToContentAdd(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response) {
    req.checkParams('contentType', 'Empty contentType param').notEmpty(); // will be checked in function checkCustomAndCommonPreconditions

    const isOK = this.checkCustomAndCommonPreconditions(req);
    if (!isOK) {
      res.redirect(this.config.env().server.url + LocationUtils.tl('/login'));
      return;
    }

    const rawContentType = Number(req.params.contentType);  // it can be a Number or string 'no'
    const contentType: number = isNaN(rawContentType) ? this.CONTENT_TYPE__IMAGE : rawContentType;

    const redirectUri = this.config.env().server.url
      + SocialLoginService.facebookLoginRedirectUriForContentAdd(req.params.contentType);

    try {
      const meResponse: IFacebookMeResponse = await AuthenticationService.facebookOAuth(req.query.code, redirectUri);
      const token: AuthenticationService.Token = await this.executeFacebookLogin(req, meResponse, null);
      const tickser: Tickser.Instance = await AuthenticationService.findTickserByToken(token.token);
      const redirectLocation: string = await this.chooseRedirectLocationForContentAdd(tickser.defaultChannelId, contentType);

      res.cookie('TicksaToken', token.token, {maxAge: 900000});
      res.redirect(this.config.env().server.url + redirectLocation);
    } catch (err) {
      this.logger.error('Error: ' + JSON.stringify(err));
      res.redirect(this.config.env().server.url + LocationUtils.tl('/login'));
    }
  }

  @Get('/social/login/facebook/ref-:referrerId-content-:contentId')
  async finishFacebookLoginAndGoToContentTeaser(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response) {
    req.checkParams('referrerId', 'Empty referrerId param').notEmpty(); // will be checked in function checkCustomAndCommonPreconditions
    req.checkParams('contentId', 'Empty contentId param').notEmpty(); // will be checked in function checkCustomAndCommonPreconditions

    const isOK = this.checkCustomAndCommonPreconditions(req);
    if (!isOK) {
      res.redirect(this.config.env().server.url + LocationUtils.tl('/login'));
      return;
    }

    const rawReferrerId = Number(req.params.referrerId);  // it can be a Number or string 'no'
    const rawContentId = Number(req.params.contentId);    // it can be a Number or string 'no'

    const referrerId: number = isNaN(rawReferrerId) ? null : rawReferrerId;
    const contentId: number = isNaN(rawContentId) ? null : rawContentId;

    const redirectUri = this.config.env().server.url
      + SocialLoginService.facebookLoginRedirectUriForContentTeaser(req.params.referrerId, req.params.contentId);

    try {
      const meResponse: IFacebookMeResponse = await AuthenticationService.facebookOAuth(req.query.code, redirectUri);
      const token: AuthenticationService.Token = await this.executeFacebookLogin(req, meResponse, referrerId);
      const redirectLocation: string = await this.chooseRedirectLocationForContentTeaser(contentId);

      res.cookie('TicksaToken', token.token, {maxAge: 900000});
      res.redirect(this.config.env().server.url + redirectLocation);
    } catch (err) {
      this.logger.error('Error: ' + JSON.stringify(err));
      res.redirect(this.config.env().server.url + LocationUtils.tl('/login'));
    }
  }

  /*
   * Few details about FB flow.
   *
   * After getting user redirected back to our redirect_uri (routings of current file) we expect that params exist:
   *   - code
   *   - granted_scopes
   *
   * That is because when we called FB API we have passed
   *   - scope=email,public_profile
   *   - response_type=code,granted_scopes
   * (https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow?locale=en_US#logindialog)
   *
   * When one get 'code' it should be exchanged to 'access token'
   * (https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow?locale=en_US#exchangecode)
   *
   * After getting 'access token' it is possible to query data from FB.
   */
  checkCustomAndCommonPreconditions(req: Ticksa.Request): boolean {

    // ATTENTION!
    // There are additional conditions to be checked. They are specified before current function call.

    req.checkQuery('code', 'Empty code query param').notEmpty();
    req.checkQuery('granted_scopes', 'Empty granted_scopes query param').notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      this.logger.error('Errors: ' + JSON.stringify(errors));
      return false;
    }

    const grantedScopes: string[] = req.query.granted_scopes.split(',');
    if (grantedScopes.indexOf('email') == -1 || grantedScopes.indexOf('public_profile') == -1) {
      this.logger.error('Errors: not enough rights granted - ' + JSON.stringify(grantedScopes));
      return false;
    }

    return true;
  }

  async executeFacebookLogin(req: Ticksa.Request, facebookMe: IFacebookMeResponse, referrerId: number): Promise<AuthenticationService.Token> {
    const tickserSocial: TickserSocial.Instance = await TickserSocial.Model.scope(TickserSocial.Scopes.INCLUDE_TICKSER).findOne({
      where: {
        socialId: facebookMe.id,
        type: TickserSocial.Type.FACEBOOK
      }
    });

    if (tickserSocial != null) {
      return await AuthenticationService.generateTokenByTickserSocial(tickserSocial, req.local);
    }

    return await AuthenticationService.generateUserSocialForFacebook(facebookMe, referrerId, req.local);
  }

  async chooseRedirectLocationForContentAdd(channelId: number, contentType: number): Promise<string> {
    if (channelId == null) {
      this.logger.error('channelId was not passed');
      return Promise.resolve(LocationUtils.tl('/login'));
    }

    const channel: Channel.Instance = await Channel.Model.findById(channelId);

    if (channel == null) {
      this.logger.error('channeId of not existing channel: ' + channelId);
      return Promise.resolve(LocationUtils.tl('/login'));
    }

    const path = '/channels/:channelId/add-content/:contentType'
      .replace(':channelId', channelId.toString())
      .replace(':contentType', contentType.toString());

    return Promise.resolve(LocationUtils.tl(path));
  }

  chooseRedirectLocationForContentTeaser(contentId: number): Promise<string> {
    if (contentId == null) {
      this.logger.error('contentId was not passed');
      return Promise.resolve(LocationUtils.tl('/login'));
    }

    return Content.Model.scope([Content.Scopes.IS_ACTIVE, Content.Scopes.IS_NOT_EXPIRED, Content.Scopes.IS_PUBLISHED]).findById(contentId)
      .then((content: Content.Instance) => {
        if (content == null) {
          this.logger.error('contentId of not existing content: ' + contentId);
          return Promise.resolve(LocationUtils.tl('/login'));
        }

        return Promise.resolve(LocationUtils.tl('/teaser/' + content.channelId + '/' + content.id));
      });
  }
}
