/// <reference path="../typings/tsd.d.ts"/>

import * as _ from 'lodash';
import * as Sequelize from 'sequelize';
import * as Ticksa from 'ticksa';
import * as Errors from '../errors';
import * as conn from '../connection';
import {
  Notification,
  Tickser,
  Channel,
  ChannelInvite,
  ChannelSubscription,
  TickserChannel,
  EmailRecipient,
  ChannelRevenue
} from '../models/models';
import * as MailService from '../services/mail-service';
import * as ChannelService from '../services/channel-service';
import * as ContentService from '../services/content-service';
import * as NotificationService from '../services/notification-service';
import * as TickserChannelRankService from '../services/tickser-channel-rank-service';
import * as ChannelMessages from '../dto/channel/channel-messages';
import * as RequestUtils from '../utils/request-utils';
import BaseController from './base-controller';
import {JsonController, Get, Req, Post, Put, Delete, UseBefore} from 'routing-controllers/index';
import AuthMiddleware from '../middlewares/auth-middleware';

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class ChannelController extends BaseController {
  constructor() {
    super(__filename);
  }

  @Get('/user/subscriptions')
  async getUserSubscriptions(@Req() req: Ticksa.Request) {
    const channels = await Channel.Model.scope([Channel.Scopes.IS_ACTIVE, Channel.Scopes.INCLUDE_COVER_IMAGE]).findAll({
      include: [
        {
          required: true,
          model: ChannelSubscription.Model,
          as: 'subscriptions',
          where: {
            tickserId: req.local.user.id
          }
        },
        {
          required: false,
          model: TickserChannel.Model,
          as: 'tickserChannels',
          where: {
            tickserId: req.local.user.id
          }
        }
      ]
    });

    return _.map(channels, channel => {
      return ChannelMessages.ChannelGetResponse.toJson(
        channel,
        null,
        true,
        null
      );
    });
  }

  @Get('/user/channels')
  async getUserActiveChannels(@Req() req: Ticksa.Request) {
    const activeChannels = await Channel.Model.scope([Channel.Scopes.IS_ACTIVE, Channel.Scopes.INCLUDE_COVER_IMAGE]).findAll({
      include: [
        {
          required: true,
          model: TickserChannel.Model,
          as: 'tickserChannels',
          where: {
            tickserId: req.local.user.id
          }
        }
      ]
    });

    return _.map(activeChannels, channel => {
      return ChannelMessages.ChannelGetResponse.toJson(channel);
    });
  }

  @Get('/user/channels/:channelId')
  async getUserActiveChannelById(@Req() req: Ticksa.Request) {
    const scopes = [
      Channel.Scopes.IS_ACTIVE,
      Channel.Scopes.INCLUDE_BACKGROUND_IMAGE,
      Channel.Scopes.INCLUDE_COVER_IMAGE,
      Channel.Scopes.INCLUDE_REVENUES_WITH_TICKSERS
    ];
    const findChannel = Channel.Model.scope(scopes).findById(req.params.channelId);
    const findTickserChannel = TickserChannel.Model.findOne({
      where: {
        tickserId: req.local.user.id,
        channelId: req.params.channelId
      }
    });
    const [channel, tickserChannel] = await Promise.all([findChannel, findTickserChannel]);

    if (channel == null) {
      throw new Errors.NotFound('Channel not found');
    }

    if (tickserChannel == null) {
      throw new Errors.Forbidden();
    }

    return ChannelMessages.ChannelGetResponse.toJson(
      channel,
      null,
      null,
      tickserChannel.ownerType
    );
  }

  @Post('/user/channels')
  async postChannel(@Req() req: Ticksa.Request) {
    const channelRequest = ChannelMessages.ChannelPostRequest.parseFromJSON(req.body);
    const maxChannels = this.config.global.maxChannels;
    const count = await TickserChannel.Model.count({
      where: {
        tickserId: req.local.user.id,
        ownerType: TickserChannel.OwnerType.CREATOR
      },
      include: [
        {
          required: true,
          model: Channel.Model,
          as: 'channel',
          where: {
            status: Channel.Status.ACTIVE
          }
        }
      ]
    });

    if (count >= maxChannels) {
      throw new Errors.BadRequest('Reached maximum number of channels');
    }

    await ChannelService.createChannel(req.local.user.id, channelRequest);
    return this.ok();
  }

  @Put('/user/channels/:channelId')
  async updateChannel(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.assertValid();

    await assertChannelCreatorOrManager(req);

    const channel = await Channel.Model.findById(req.params.channelId);


    if (!channel) {
      throw new Errors.NotFound('Channel not found');
    }
    let channelRequest = ChannelMessages.ChannelPostRequest.parseFromJSON(req.body);
    let isResetRankedList = channel.showRankedList && !channelRequest.showRankedList;
    let isUpdateRankedListDate = !channel.showRankedList && channelRequest.showRankedList;

    channel.name = channelRequest.name;
    channel.description = channelRequest.description;
    channel.type = channelRequest.type;
    channel.coverImageId = channelRequest.coverImageId;
    channel.backgroundImageId = channelRequest.backgroundImageId;
    channel.shareIncome = channelRequest.shareIncome;
    channel.showRankedList = channelRequest.showRankedList;
    channel.allowVoting = channelRequest.allowVoting;
    if (isUpdateRankedListDate) {
      channel.showRankedListDate = new Date();
    }

    await conn.transaction(async(t: Sequelize.Transaction) => {
      let saveChannel = channel.save({transaction: t});
      let resetChannelRankedList: Promise<Channel.Instance> = saveChannel.then(function (channel: Channel.Instance) {
        return isResetRankedList
          ? TickserChannelRankService.resetRankedListByChannel(channel, t)
          : Promise.resolve<Channel.Instance>(channel)
      });

      let findOldRevenues = ChannelRevenue.Model.findAll({
        where: {
          channelId: channel.id
        },
        transaction: t
      });

      const [updatedChannel, oldRevenues] = await Promise.all([resetChannelRankedList, findOldRevenues]);
      await ChannelRevenue.Model.destroy({
        where: {
          channelId: updatedChannel.id
        }, transaction: t
      });
      await ChannelService.createRevenuesForChannel(req.local.user.id, updatedChannel, channelRequest.revenues, oldRevenues, t)
    });

    return this.ok();
  }

  @Get('/channels/:channelId')
  async getChannel(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.assertValid();

    let scopes = [Channel.Scopes.IS_ACTIVE, Channel.Scopes.INCLUDE_COVER_IMAGE, Channel.Scopes.INCLUDE_BACKGROUND_IMAGE];
    var findChannel = Channel.Model.scope(scopes).findOne({
      where: {
        id: req.params.channelId
      },
    });
    var findTickserChannel = TickserChannel.Model.findOne({
      where: {
        channelId: req.params.channelId,
        tickserId: req.local.user.id
      }
    });
    var countSubscriptions = ChannelSubscription.Model.count({
      where: {
        channelId: req.params.channelId
      }
    });
    var findChannelSubscription = ChannelSubscription.Model.findOne({
      where: {
        tickserId: req.local.user.id,
        channelId: req.params.channelId
      }
    });

    const [channel, tickserChannel, subscribersCount, mySubscription] = await Promise.all([
      findChannel,
      findTickserChannel,
      countSubscriptions,
      findChannelSubscription
    ]);

    if (channel == null) {
      throw new Errors.NotFound();
    }

    let channelGetResponse = ChannelMessages.ChannelGetResponse.toJson(
      channel,
      subscribersCount,
      mySubscription != null,
      tickserChannel ? tickserChannel.ownerType : null
    );

    return channelGetResponse;
  }

  @Get('/channels/:channelId/ranked-list')
  async getChannelRankedList(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.assertValid();

    let pageable = RequestUtils.getPageable(req);

    return TickserChannelRankService.findRankedList(req.params.channelId, pageable);
  }

  @Get('/channels/:channelId/voting-list')
  async getChannelVotingList(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.assertValid();

    let pageable = RequestUtils.getPageable(req);

    return ContentService.findContentForVoteboard(req.params.channelId, pageable);
  }

  @Post('/channels/:channelId/reset-ranked-list')
  async resetChannelRankedList(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.assertValid();

    await assertChannelCreatorOrManager(req);

    await TickserChannelRankService.resetRankedListByChannelId(req.params.channelId);
    return this.ok();
  }

  @Post('/channels/:channelId/subscribe')
  async subscribeToChannel(@Req() req: Ticksa.Request) {
    await conn.transaction(async(t) => {
      await ChannelSubscription.Model.destroy({
        where: {
          tickserId: req.local.user.id,
          channelId: req.params.channelId
        },
        transaction: t
      });

      const createSubscription = ChannelSubscription.Model.create({
        tickserId: req.local.user.id,
        channelId: req.params.channelId
      }, {transaction: t});
      const notificate = NotificationService.createForChannelSubscribe(req.local.user, req.params.channelId, t);

      return Promise.all([createSubscription, notificate]);
    });
    return this.ok();
  }

  @Post('/channels/:channelId/unsubscribe')
  async unsubscribeToChannel(@Req() req: Ticksa.Request) {
    await ChannelSubscription.Model.destroy({
      where: {
        tickserId: req.local.user.id,
        channelId: req.params.channelId
      }
    });
    return this.ok();
  }

  @Post('/channels/:channelId/sort-content')
  async sortContent(@Req() req: Ticksa.Request) {
    req.checkBody('ids[]', 'Invalid params').notEmpty();
    req.checkParams('channelId', 'Invalid params').notEmpty();
    req.assertValid();

    await assertChannelCreatorOrManager(req);

    const ids: number[] = req.body['ids[]'];
    await ChannelService.sortChannelContent(req.params.channelId, req.local.user.id, ids);
    return this.ok();
  }

  @Post('/channels/:channelId/donate')
  async donateChannel(@Req() req: Ticksa.Request) {
    req.checkBody('amount', 'Invalid params').notEmpty().isNumeric();
    req.checkParams('channelId', 'Invalid params').notEmpty();
    req.assertValid();

    await ChannelService.donateChannel(req.params.channelId, req.local.user.id, req.body.amount, req.body.behalfTickserId);
    return this.ok();
  }

  @Post('/user/channels/:channelId/invite')
  async inviteForEdit(@Req() req: Ticksa.Request) {
    await assertChannelCreatorOrManager(req);

    var invite = req.body;
    if (invite.ownerType != ChannelInvite.OwnerType.MANAGER && invite.ownerType != ChannelInvite.OwnerType.PARTICIPANT) {
      throw new Errors.BadRequest('Can only invite to channel as managers or participants');
    }

    const findChannel = Channel.Model.findOne({where: {id: req.params.channelId}});
    const findTickser = Tickser.Model.findOne({where: {username: invite.email}});

    const [channel, user] = await Promise.all([findChannel, findTickser]);
    const userId = user ? user.id : null;

    await conn.transaction(async(t) => {
      const createdChannelInvite: ChannelInvite.Instance = await ChannelInvite.Model.create({
        ownerType: invite.ownerType,
        channelId: channel.id,
        email: invite.email,
        tickserId: userId
      }, {transaction: t});

      var username = user != null ? (' ' + user.name) : '';
      var subject = 'You are invited for contribution to channel "' + channel.name + '"';
      var content = 'Hi' + username + ',\n\nyou have been invited to contribute on the channel "' + channel.name + '".\n\nCheckout the details on Ticksa.\n\nStay awesome!';

      if (user) {
        user.notificationCount = <number>user.notificationCount + 1;
        await Notification.Model.create({
          title: subject,
          content: content,
          type: Notification.Type.CHANNEL_EDIT_INVITE,
          tickserId: user.id,
          metadata: {channelInviteId: createdChannelInvite.id}
        }, {transaction: t});

        const params: any = {
          channel_name: channel.name,
          user_name: user.name,
          url: this.config.env().server.url
        };

        await MailService.sendMailToTickser(user, EmailRecipient.Template.CHANNEL_INVITE_TO_TICKSER, params);
        await user.save({transaction: t});
      } else {
        const params: any = {
          channel_name: channel.name,
          url: this.config.env().server.url
        };
        await MailService.sendMailIfNoTickser(invite.email, EmailRecipient.Template.CHANNEL_INVITE_TO_EMAIL, params);
      }
    });

    return this.ok();
  }

  @Delete('/user/channels/:channelId')
  async deactivateChannel(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid params').notEmpty();
    req.assertValid();

    await assertChannelCreator(req);

    const tickser: Tickser.Instance = req.local.user;

    if (tickser.systemChannelId == req.params.channelId) {
      throw new Errors.BadRequest('Cannot delete system channel')
    }

    if (req.params.channelId == tickser.defaultChannelId) {
      tickser.defaultChannelId = tickser.systemChannelId;
    }

    const updateChannel = Channel.Model.update({
      status: Channel.Status.DEACTIVATED
    }, {
      where: {
        id: req.params.channelId
      }
    });
    const saveUser = tickser.save();

    await Promise.all([updateChannel, saveUser]);
    return this.ok();
  }
}

async function assertChannelCreatorOrManager(req: Ticksa.Request) {
  const tickserChannel: TickserChannel.Instance = await TickserChannel.Model.findOne({
    where: {
      tickserId: req.local.user.id,
      channelId: req.params.channelId
    },
    include: [
      {
        required: true,
        model: Channel.Model,
        as: 'channel',
        attributes: ['id', 'status'],
        where: {
          status: Channel.Status.ACTIVE
        }
      }
    ]
  });

  const isCreatorOrManager: boolean = tickserChannel != null &&
    (
      tickserChannel.ownerType == TickserChannel.OwnerType.CREATOR ||
      tickserChannel.ownerType == TickserChannel.OwnerType.MANAGER
    );

  if (!isCreatorOrManager) {
    throw new Errors.Forbidden('Only manager or creator can have access');
  }
}

async function assertChannelCreator(req: Ticksa.Request) {
  const tickserChannel: TickserChannel.Instance = await TickserChannel.Model.findOne({
    where: {
      tickserId: req.local.user.id,
      channelId: req.params.channelId
    },
    include: [
      {
        required: true,
        model: Channel.Model,
        as: 'channel',
        attributes: ['id', 'status'],
        where: {
          status: Channel.Status.ACTIVE
        }
      }
    ]
  });

  const isCreator: boolean = tickserChannel != null && (tickserChannel.ownerType == TickserChannel.OwnerType.CREATOR);
  if (!isCreator) {
    throw new Errors.Forbidden('Only creator can have access');
  }
}