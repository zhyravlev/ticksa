/// <reference path="../typings/tsd.d.ts"/>

import * as _ from 'lodash';
import * as Ticksa from 'ticksa';
import * as PlaylistMessages from '../dto/playlist/playlist-messages';
import {
  Tickser,
  Playlist
} from '../models/models';
import {UseBefore, JsonController, Req, Put, Get, Post, Delete} from 'routing-controllers/index';
import AuthMiddleware from '../middlewares/auth-middleware';
import BaseController from './base-controller';
import PlaylistService from '../services/playlist-service';
import * as UtilService from '../services/utilities';
import {
  UserPlaylistAndContents,
  PlaylistAndContentsWithPermissions
} from '../services/playlist-service';
import {UserMeta} from '../services/content-service';
import * as Errors from '../errors';

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class PlaylistController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Post('/user/playlists')
  async postUserPlaylist(@Req() req: Ticksa.Request) {
    const userMeta: UserMeta = {countryA2Code: req.local.geoip.countryA2Code, tickserId: req.local.user.id};
    const playlistRequest = PlaylistMessages.PlaylistUserPostRequest.parseFromJSON(req.body);
    const playlist: Playlist.Instance = await PlaylistService.createUserPlaylist(userMeta, playlistRequest);
    return PlaylistMessages.PlaylistUserPostResponse.toJson(playlist);
  }

  @Get('/user/playlists')
  async getUserPlaylists(@Req() req: Ticksa.Request) {
    const playlists = await PlaylistService.getUserPlaylists(req.local.user);
    return _.map(playlists, playlist => PlaylistMessages.PlaylistSimpleGetResponse.toJson(playlist));
  }

  @Put('/user/playlists/:playlistId')
  async updateUserPlaylist(@Req() req: Ticksa.Request) {
    req.checkParams('playlistId', 'Invalid param').notEmpty();
    req.assertValid();

    const user: Tickser.Instance = req.local.user;
    const userMeta: UserMeta = {countryA2Code: req.local.geoip.countryA2Code, tickserId: user.id};
    const playlistId = req.params.playlistId;
    const playlistRequest = PlaylistMessages.PlaylistUserPostRequest.parseFromJSON(req.body);

    await PlaylistService.updateUserPlaylist(userMeta, playlistId, playlistRequest);
    return this.ok();
  }

  @Delete('/user/playlists/:playlistId')
  async deleteUserPlaylist(@Req() req: Ticksa.Request) {
    req.checkParams('playlistId', 'Invalid param').notEmpty();
    req.assertValid();

    const user: Tickser.Instance = req.local.user;
    const playlistId = req.params.playlistId;

    await PlaylistService.deleteUserPlaylist(user, playlistId);
    return this.ok();
  }

  @Get('/user/playlists/:playlistId')
  async getUserPlaylist(@Req() req: Ticksa.Request) {
    req.checkParams('playlistId', 'Invalid param').notEmpty();
    req.assertValid();

    const user: Tickser.Instance = req.local.user;
    const userMeta: UserMeta = {countryA2Code: req.local.geoip.countryA2Code, tickserId: user.id};
    const playlistId = req.params.playlistId;

    const playlistAndContents: UserPlaylistAndContents = await PlaylistService.getUserPlaylistAndContents(userMeta, playlistId);
    return PlaylistMessages.PlaylistUserGetResponse.toJson(playlistAndContents.playlist, playlistAndContents.contents);
  }

  @Get('/playlists/:playlistId')
  async getPlaylist(@Req() req: Ticksa.Request) {
    req.checkParams('playlistId', 'Invalid param').notEmpty();
    req.assertValid();

    const user: Tickser.Instance = req.local.user;
    const userMeta: UserMeta = {countryA2Code: req.local.geoip.countryA2Code, tickserId: user.id};
    const playlistId = req.params.playlistId;

    const playlistAndContentsWithPermissions: PlaylistAndContentsWithPermissions =
      await PlaylistService.getPlaylistAndContentsWithPermissions(userMeta, user.account.id, playlistId);

    return PlaylistMessages.PlaylistGetResponse.toJson(
      playlistAndContentsWithPermissions.playlist,
      playlistAndContentsWithPermissions.contents,
      playlistAndContentsWithPermissions.permissionMap
    );
  }

  @Post('/playlists/decode')
  async postDecodePlaylistId(@Req() req: Ticksa.Request) {
    req.checkBody('encodedPlaylistId', 'Invalid param').notEmpty();
    req.assertValid();

    const playlistId: number = UtilService.decodeNumberFromRadix64(req.body.encodedPlaylistId);
    const playlist: Playlist.Instance = await Playlist.Model.scope([
      Playlist.Scopes.IS_NOT_DELETED,
      Playlist.Scopes.IS_PUBLISHED
    ]).findById(playlistId);

    if (playlist == null) {
      throw new Errors.NotFound('Playlist not found')
    }

    return {
      playlistId: playlist.id
    };
  }
}
