/// <reference path="../typings/tsd.d.ts"/>

const expressSitemap = require('express-sitemap');
import * as _ from 'lodash';
import * as url from 'url';
import * as Ticksa from 'ticksa';
import * as XMLSitemapMessages from '../dto/xmlsitemap/xmlsitemap-messages';
import * as UtilitiesService from '../services/utilities';
import * as LocationUtils from '../utils/ticksa-location-utils';
import * as Errors from '../errors';
import {Content} from '../../app/models/models';
import * as XMLSitemapService from '../services/xmlsitemap-service';
import {Controller, Get, Req, Res} from 'routing-controllers/index';
import BaseController from './base-controller';
import {Crawler} from '../services/sharing-service';

import * as moment from 'moment';
import Moment = moment.Moment;
import Duration = moment.Duration;

@Controller()
export default class XmlSitemapController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/q/:encodedData')
  async getContentForSearchEngineByShortLink(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response) {
    req.checkParams('encodedData', 'Invalid param').notEmpty();
    req.assertValid();

    const contentId: number = UtilitiesService.decodeNumberFromRadix64(req.params.encodedData);
    const userAgent = req.headers['user-agent'];

    if (Crawler.isSocialNetworkCrawler(userAgent)) {
      const data = await Crawler.getResponseToSocialNetworkCrawler(contentId);
      return res.send(data);
    }

    if (Crawler.isSearchEngineCrawler(userAgent)) {
      const data = await Crawler.getResponseToSearchEngineCrawler(contentId);
      return res.send(data);
    }

    const content: Content.Instance = await Content.Model.findOne({
      attributes: ['id', 'channelId'],
      where: {
        id: contentId
      }
    });

    if (content == null) {
      throw new Errors.NotFound('Content not found');
    }

    res.redirect(LocationUtils.tl('/teaser/' + content.channelId + '/' + content.id));
  }

  @Get('/sitemap.xml')
  async getSitemap(@Res() res: Ticksa.Response) {
    const contents: Content.Instance[] = await XMLSitemapService.getAllContentsForSitemap();

    if ((contents == null) || (contents.length == 0)) {
      this.logger.warn('xml contents: none');
      return res.send([]);
    }

    const start: Moment = moment();
    const promises = _.map(contents, content => XMLSitemapMessages.ContentXMLResponse.parseFromDB(content));
    const XMLContentsArray = await Promise.all(promises);
    const stop: Moment = moment();

    const duration: Duration = moment.duration(stop.diff(start));
    this.logger.info('Query contents from DB.'
      + ' start: ' + start.format()
      + '. stop: ' + stop.format()
      + '. duration: %s' + duration.asSeconds()
    );

    if (duration.asSeconds() >= 10) {
      this.logger.error('Exceeded time to generate XML Sitemap');
    }

    // Convert array to object
    const XMLContenstObj = _.reduce(XMLContentsArray, (memo, current) => _.extend(memo, current), {});

    const serviceURLParsed = url.parse(this.config.env().server.url);

    return expressSitemap({
      http: _.trim(serviceURLParsed.protocol, ':'),
      url: serviceURLParsed.host,
      map: _.mapValues(XMLContenstObj, function () {
        return ['get'];
      }),
      route: XMLContenstObj
    }).XMLtoWeb(res);
  }

}
