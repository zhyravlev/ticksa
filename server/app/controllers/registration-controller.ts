/// <reference path="../typings/tsd.d.ts"/>

import * as uuid from 'node-uuid';
import * as Ticksa from 'ticksa';
import * as conn from '../connection';

import BaseController from './base-controller';
import {JsonController, Post, Req, Get} from 'routing-controllers';
import {BodyParam, Res} from 'routing-controllers/index';
import {Tickser, EmailRecipient} from '../models/models';
import * as SecurityService from '../services/security-service';
import * as MailService from '../services/mail-service';
import * as RegisterService from '../services/register';
import * as LocationUtils from '../utils/ticksa-location-utils';
import * as RegistrationMessages from '../dto/registration/registration-messages';
import * as RegistrationService from '../services/register';
import * as Errors from '../errors';

@JsonController('/api')
export default class RegistrationController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Post('/registrations/register')
  async register(@Req() req: Ticksa.Request) {
    const request = RegistrationMessages.UserRegistrationRequest.parseFromJSON(req.body);
    const tickser: Tickser.Instance = await RegistrationService.generateUser(request, req.local);

    const params = {
      user_name: tickser.name,
      url: this.config.env().server.url + '/api/registrations/confirm?url=' + tickser.confirmationUrl
    };

    await MailService.sendMailToTickser(tickser, EmailRecipient.Template.REGISTRATION, params);
    return this.ok();
  }

  @Get('/registrations/check-email')
  async checkEmail(@Req() req: Ticksa.Request) {
    const tickser: Tickser.Instance = await Tickser.Model.findOneByUsername(req.query.email);
    if (tickser == null) {
      throw new Errors.BadRequest();
    }
    return this.ok();
  }

  @Get('/registrations/confirm')
  async confirmRegistration(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response) {
    req.checkQuery('url', 'Invalid param').notEmpty();
    req.assertValid();

    const tickser: Tickser.Instance = await Tickser.Model.findOne({
      where: {
        confirmationUrl: req.query.url,
        status: Tickser.Status.NEW
      }
    });

    if (tickser == null) {
      throw new Errors.UserNotFound();
    }

    const confirmedTickser: Tickser.Instance = await conn.transaction((t) => {
      return RegisterService.confirmRegistration(tickser, req.local.language, t);
    });

    res.cookie('TicksaToken', SecurityService.generateToken(confirmedTickser.id, confirmedTickser.username), {maxAge: 900000});
    if (confirmedTickser.registerRedirectUrl != null) {
      res.redirect(confirmedTickser.registerRedirectUrl);
    } else {
      res.redirect(this.config.env().server.url + LocationUtils.tl('/login'));
    }
  }

  @Get('/registrations/resend')
  async resendConfirmationUrl(@Req() req: Ticksa.Request) {
    req.checkQuery('username', 'Invalid param').notEmpty();
    req.assertValid();

    const tickser: Tickser.Instance = await Tickser.Model.findOne({
      where: {
        username: req.query.username,
        status: Tickser.Status.NEW
      }
    });

    if (tickser == null) {
      throw new Errors.UserNotFound();
    }

    const params = {
      user_name: tickser.name,
      url: this.config.env().server.url + '/api/registrations/confirm?url=' + tickser.confirmationUrl
    };

    await MailService.sendMailToTickser(tickser, EmailRecipient.Template.REGISTRATION, params);
    return this.ok();
  }

  @Post('/registrations/forgot-password')
  async forgotPassword(@Req() req: Ticksa.Request, @BodyParam('email') email: string) {
    req.checkBody('email', 'Invalid param').notEmpty();
    req.assertValid();

    const tickser: Tickser.Instance = await Tickser.Model.findOneByEmail(email);
    if (tickser == null) {
      throw new Errors.UserNotFound();
    }

    tickser.changePasswordUrl = uuid.v4();
    await tickser.save();
    const params = {
      url: this.config.env().server.url + LocationUtils.tl('/change-password/' + tickser.changePasswordUrl)
    };

    await MailService.sendMailToTickser(tickser, EmailRecipient.Template.FORGOT_PASSWORD, params);
    return this.ok();
  }

  @Post('/registrations/new-password')
  async newPassword(@Req() req: Ticksa.Request) {
    req.checkBody('url', 'Invalid param').notEmpty();
    req.checkBody('password', 'Invalid param').notEmpty();
    req.assertValid();

    const tickser: Tickser.Instance = await Tickser.Model.findOne({
      where: {
        changePasswordUrl: req.body.url
      }
    });

    if (tickser == null) {
      throw new Errors.UserNotFound();
    }

    tickser.password = await SecurityService.createPassword(req.body.password);
    await tickser.save();

    const params = {
      user_name: tickser.name
    };

    await MailService.sendMailToTickser(tickser, EmailRecipient.Template.PASSWORD_CHANGED, params);
    return this.ok();
  }
}