/// <reference path="../typings/tsd.d.ts"/>
import * as Ticksa from 'ticksa';
import * as Errors from '../errors';
import {JsonController, Req, Post, Get} from 'routing-controllers';
import BaseController from './base-controller';

import * as AdminCommentsService from '../services/admin-comments';
import * as SecurityService from '../services/security-service';
import {EmailFilter, AdminComment} from '../models/models';

@JsonController('/api')
export default class EmailController extends BaseController {
  constructor() {
    super(__filename);
  }

  @Get('/emails/verify-token')
  async sendTokenData(@Req() req: Ticksa.Request): Promise<SecurityService.EmailTokenData> {
    return await this.verifyToken(req);
  }

  @Get('/emails/check-subscription')
  async checkSubscription(@Req() req: Ticksa.Request): Promise<any> {
    const tokenData: SecurityService.EmailTokenData = await this.verifyToken(req);

    const filter: EmailFilter.Instance = await EmailFilter.Model
      .scope(['subscription'])
      .findOne({where: {value: tokenData.email}});

    return (filter != null) ? {subscription: false} : {subscription: true};
  }

  @Post('/emails/reply')
  async reply(@Req() req: Ticksa.Request): Promise<any> {
    const tokenData: SecurityService.EmailTokenData = await this.verifyToken(req);
    if ((tokenData.tickserId == null) || (tokenData.recipientId == null)) {
      throw new Errors.BadRequest('Invalid token');
    }

    const comment: AdminComment.Instance = await AdminCommentsService.createReply(tokenData.tickserId, tokenData.recipientId, req.body.text);

    return {commentId: comment.id};
  }

  @Post('/emails/subscribe')
  async subscribe(@Req() req: Ticksa.Request) {
    const tokenData: SecurityService.EmailTokenData = await this.verifyToken(req);
    await EmailFilter.Model.scope(['subscription']).destroy({where: {value: tokenData.email}});
    return {subscription: true};
  }

  @Post('/emails/unsubscribe')
  async unsubscribe(@Req() req: Ticksa.Request) {
    const tokenData: SecurityService.EmailTokenData = await this.verifyToken(req);
    if (tokenData.tickserId == null) {
      throw new Errors.BadRequest('Invalid token');
    }

    const filter: EmailFilter.Instance = await EmailFilter.Model.scope(['subscription']).findOne({where: {value: tokenData.email}});

    if (filter == null) {
      const newFilter = {
        value: tokenData.email,
        type: EmailFilter.Type.EQUALS,
        reason: EmailFilter.Reason.SUBSCRIPTION,
        createdById: tokenData.tickserId,
      };
      await EmailFilter.Model.create(newFilter);
    }

    return {subscription: false};
  }

  async verifyToken(req: Ticksa.Request): Promise<SecurityService.EmailTokenData> {
    const token = req.query.token || req.body.token;
    if (token == null) {
      throw new Errors.BadRequest('Token not found');
    }

    try {
      return await SecurityService.verifyEmailToken(token);
    } catch (ignored) {
      throw new Errors.BadRequest();
    }
  }
}
