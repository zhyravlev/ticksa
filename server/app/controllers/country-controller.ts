/// <reference path="../typings/tsd.d.ts"/>

import * as _ from 'lodash';
import * as CountryMessages from '../dto/countries/country-messages';
import * as CountryService from '../services/country-service';
import {JsonController, Get} from 'routing-controllers';
import BaseController from './base-controller';
import {Country} from '../models/models';

@JsonController('/api')
export default class CountryController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/countries')
  async getWalletDetails() {
    const countries: Country.Attributes[] = await CountryService.getAllCountries();
    return _.map(countries, c => CountryMessages.CountryGetResponse.parseFromDB(c));
  }
}