/// <reference path="../typings/tsd.d.ts"/>
import _ = require('lodash');
import ticksa = require('ticksa');
import Errors = require('../errors');

import AuthService = require('../services/authentication-service');
import SecurityService = require('../services/security-service');
import TickserService = require('../services/tickser-service');
import BaseController from './base-controller';

import {JsonController, Param, Body, Get, Post, Put, Delete, Res, Req, UseBefore} from "routing-controllers";

import Models = require('../models/models');
import Tickser = Models.Tickser;
import Notification = Models.Notification;
import ChannelInvite = Models.ChannelInvite;
import TickserChannel = Models.TickserChannel;

import NotificationService = require('../services/notification-service');
import conn = require('../connection');
import {NotFound} from '../errors';
import AuthMiddleware from '../middlewares/auth-middleware';

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class NotificationController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/notifications')
  async getNotificationsForUser(@Req() req: ticksa.Request) {
    const notifications = await Notification.Model.scope('notViewed').findAllByTickserId(req.local.user.id);
    return _.map(notifications, function (n: Notification.Instance) {
      return n.get();
    });
  }

  @Get('/notifications/count')
  async getCountNotificationsForUser(@Req() req: ticksa.Request) {
    const user: Tickser.Instance = req.local.user;

    const count: number = await Notification.Model.scope('notViewed').countByTickserId(user.id);
    return {count: count};
  }

  @Get('/notifications/:notificationId')
  async getNotification(@Param('notificationId') notificationId: number, @Req() req: ticksa.Request) {
    const user: Tickser.Instance = req.local.user;
    const notification: Notification.Instance = await Notification.Model.findOneByIdAndTickser(notificationId, user.id);

    if (!notification) {
      throw new NotFound();
    }

    return notification.get();
  }

  @Post('/notifications/view')
  async viewNotifications(@Req() req: ticksa.Request) {
    const user: Tickser.Instance = req.local.user;
    await NotificationService.viewAllNotificationByTickser(user.id);
    const count: number = await Notification.Model.scope('notViewed').countByTickserId(user.id);

    return {count: count};
  }
}