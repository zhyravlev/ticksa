/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import BaseController from './base-controller';
import {JsonController, Get} from 'routing-controllers';

@JsonController('/api')
export default class ConfigController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/global-config')
  async getConfig(): Promise<Ticksa.Configuration.GlobalSettings> {
    return this.config.global;
  }

}