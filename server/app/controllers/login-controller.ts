/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import * as Errors from '../errors';

import {Body, Post, Req} from "routing-controllers";
import {QueryParam,BodyParam,HeaderParam, JsonController} from 'routing-controllers/index';
import BaseController from './base-controller';

import * as AuthService from '../services/authentication-service';
import * as SecurityService from '../services/security-service';
import * as TickserService from '../services/tickser-service';

import {Tickser} from '../models/models';

interface User {
  username: string;
  password: string;
}

@JsonController('/api')
export default class LoginController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Post('/login')
  async login(@Body() user: User, @Req() req: Ticksa.Request) {
    if (!user.username || !user.password) {
      throw new Errors.BadRequest('No username or password provided');
    }

    return AuthService.local(user.username.toLowerCase(), user.password, req);
  }

  @Post('/login/google')
  async google(@BodyParam('idToken') idToken: string, @QueryParam('referral') referrerId: number, @Req() req: Ticksa.Request) {
    return AuthService.google(idToken, referrerId, req)
  }

  @Post('/login/facebook')
  async facebook(@BodyParam('signedRequest') signedRequest: string, @BodyParam('accessToken') accessToken: string,
        @QueryParam('referral') referrerId: number, @Req() req: Ticksa.Request) {
    return AuthService.facebook(signedRequest, accessToken, referrerId, req);
  }

  @Post('/login/refresh-token')
  async refreshToken(@HeaderParam('token') token: string): Promise<{token: string}> {
    const decoded: SecurityService.TokenData = await SecurityService.verifyToken(token);
    if (decoded.encId != null) {
      const newToken = SecurityService.generateTokenForEncryptedIdAndUsername(decoded.encId, decoded.username);
      return {token: newToken};
    }

    const tickser: Tickser.Instance = await TickserService.findSimpleTickserByUsername(decoded.username);
    const newToken = SecurityService.generateToken(tickser.id, tickser.username);
    return {token: newToken};
  }
}