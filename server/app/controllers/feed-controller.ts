/// <reference path="../typings/tsd.d.ts"/>

import * as _ from 'lodash';
import * as Ticksa from 'ticksa';
import * as Sequelize from 'sequelize';
import {
  Get,
  Post,
  Put,
  JsonController,
  UseBefore,
  Req
} from 'routing-controllers/index';

import * as conn from '../connection';
import * as Errors from '../errors';
import BaseController from './base-controller';
import {
  Tickser,
  TicksTransaction,
  Image,
  Video,
  Feed,
  Content
} from '../models/models'
import * as RequestUtils from '../utils/request-utils';
import * as TicksTransactionsService from '../services/ticks-transaction-service';
import * as FeedMessages from '../dto/feed/feed-messages';
import AuthMiddleware from '../middlewares/auth-middleware';

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class FeedController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Post('/contents/:contentId/feeds')
  async postFeed(@Req() req: Ticksa.Request) {
    let feed = FeedMessages.FeedPostRequest.parseFromJSON(req.body);
    let level: number = 0;

    if (feed.parentId != null) {
      const parentFeed: Feed.Instance = await Feed.Model.findOne({
        where: {
          id: feed.parentId
        }
      });
      level = parentFeed.level + 1;
    }

    const feedAttrs = {
      tickserId: req.local.user.id,
      contentId: req.params.contentId,
      parentId: feed.parentId,
      videoId: feed.videoId,
      imageId: feed.imageId,
      text: feed.text,
      likes: 0,
      dislikes: 0,
      level: level,
      status: Feed.Status.ACTIVE
    };


    await conn.transaction(async(t) => {
      const [createdFeed, updatedContent] = await Promise.all([
        Feed.Model.create(feedAttrs, {transaction: t}),
        Content.Model.update(<Content.Attributes>{commentCount: conn.literal('comment_count + 1')}, {
          where: {id: req.params.contentId},
          transaction: t
        })
      ]);

      return await TicksTransactionsService.processOneWayTransaction(req.local.user.id, null,
        createdFeed.id, this.config.global.feedPostPrice, TicksTransaction.Type.CREATE, t);
    });

    return this.ok();
  }

  @Get('/contents/:contentId/feeds')
  async getFeedsForContent(@Req() req: Ticksa.Request) {
    const pageable: Ticksa.IPageable<void> = RequestUtils.getPageable(req);

    const options: Sequelize.FindOptions = {
      limit: pageable.limit,
      offset: pageable.offset,
      order: [
        ['likes', 'DESC'],
        ['created_at', 'DESC']
      ],
      where: {
        contentId: req.params.contentId,
        status: Feed.Status.ACTIVE
      },
      include: [
        {
          model: Tickser.Model.scope(Tickser.Scopes.INCLUDE_PROFILE_IMAGE),
          as: 'tickser',
        },
        {
          model: Image.Model.scope(Image.Scopes.INCLUDE_FILE),
          as: 'image',
        },
        {
          model: Video.Model.scope([Video.Scopes.INCLUDE_THUMBNAIL_IMAGE, Video.Scopes.INCLUDE_VIDEO_FILES]),
          as: 'video',
        }
      ]
    };
    const feeds = await Feed.Model.findAll(options);

    return _.map(feeds, (feed: Feed.Instance) => {
      return FeedMessages.FeedGetResponse.parseFromDB(feed)
    })
  }

  @Post('/feeds/:feedId/like')
  async likeFeed(@Req() req: Ticksa.Request) {
    return await conn.transaction(async function (t) {
      const feed = await Feed.Model.findOne({
        where: {
          id: req.params.feedId
        },
        transaction: t
      });

      feed.likes += 1;

      let params: TicksTransactionsService.TwoWayTransactionParams = {
        buyerTickserId: req.local.user.id,
        sellerTickserId: feed.tickserId,
        feedId: feed.id,
        amount: 1.0,
        type: TicksTransaction.Type.LIKE,
        t: t
      };

      const result = await TicksTransactionsService.processTwoWayTransaction(params);
      await feed.save({transaction: t});

      return {ticks: result.buyerAccount.currentBalance};
    })
  }

  @Post('/feeds/:feedId/dislike')
  async dislikeFeed(@Req() req: Ticksa.Request) {
    return await conn.transaction(async function (t) {
      const feed = await Feed.Model.findOne({
        where: {
          id: req.params.feedId
        },
        transaction: t
      });

      feed.dislikes += 1;

      let params: TicksTransactionsService.TwoWayTransactionParams = {
        buyerTickserId: req.local.user.id,
        sellerTickserId: feed.tickserId,
        feedId: feed.id,
        amount: 1.0,
        type: TicksTransaction.Type.DISLIKE,
        t: t
      };

      const result = await TicksTransactionsService.processTwoWayTransaction(params);
      await feed.save({transaction: t});

      return {ticks: result.buyerAccount.currentBalance};
    })
  }

  @Get('/user/feeds')
  async getFeedsForUser(@Req() req: Ticksa.Request) {
    const feeds = await Feed.Model.findAll({
      where: {
        tickserId: req.local.user.id,
        status: Feed.Status.ACTIVE
      },
      include: [
        {
          model: Tickser.Model,
          as: 'tickser',
        },
        {
          model: Image.Model.scope(Image.Scopes.INCLUDE_FILE),
          as: 'image',
        },
        {
          model: Video.Model.scope([Video.Scopes.INCLUDE_THUMBNAIL_IMAGE, Video.Scopes.INCLUDE_VIDEO_FILES]),
          as: 'video',
        }
      ]
    });

    return _.map(feeds, (feed: Feed.Instance) => {
      return FeedMessages.FeedGetResponse.parseFromDB(feed)
    });
  }

  @Get('/user/feeds/:feedId')
  async getUserFeed(@Req() req: Ticksa.Request) {
    const feed = await Feed.Model.findOne({
      where: {
        id: req.params.feedId,
        tickserId: req.local.user.id,
        status: Feed.Status.ACTIVE
      },
      include: [
        {
          model: Tickser.Model.scope(Tickser.Scopes.INCLUDE_PROFILE_IMAGE),
          as: 'tickser',
        },
        {
          model: Image.Model.scope(Image.Scopes.INCLUDE_FILE),
          as: 'image',
        },
        {
          model: Video.Model.scope([Video.Scopes.INCLUDE_THUMBNAIL_IMAGE, Video.Scopes.INCLUDE_VIDEO_FILES]),
          as: 'video',
        }
      ]
    });

    if (!feed) {
      throw new Errors.NotFound('Feed not found')
    }

    return FeedMessages.FeedGetResponse.parseFromDB(feed);
  }

  @Put('/user/feeds/:feedId')
  async updateFeed(@Req() req: Ticksa.Request) {
    const feedRequest = FeedMessages.FeedPostRequest.parseFromJSON(req.body);
    await Feed.Model.update({
        text: feedRequest.text,
        imageId: feedRequest.imageId ? feedRequest.imageId : null,
        videoId: feedRequest.videoId ? feedRequest.videoId : null
      },
      {
        where: {
          id: req.params.feedId,
          tickserId: req.local.user.id,
          status: Feed.Status.ACTIVE
        }
      });

    return this.ok();
  }
}
