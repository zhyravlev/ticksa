/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import * as util from '../services/utilities';
import * as UtilitiesMessages from '../dto/utilities/utilities-messages';
import BaseController from './base-controller';
import {JsonController} from 'routing-controllers/index';
import {Req, Get} from 'routing-controllers/index';
import AuthMiddleware from '../middlewares/auth-middleware';
import {UseBefore} from 'routing-controllers/index';

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class UtilitiesController extends BaseController {
  constructor() {
    super(__filename);
  }

  @Get('/utilities/encode-number')
  getEncodeNumber(@Req() req: Ticksa.Request) {
    return new UtilitiesMessages.EncodeNumberGetResponse(util.encodeNumberToRadix64(req.query.number));
  }

  @Get('/utilities/encode-content-user-id')
  getEncodeContentUserId(@Req() req: Ticksa.Request) {
    return new UtilitiesMessages.EncodeContentAndUserIdGetResponse(
      util.encodeNumberToRadix64(req.query.contentId),
      util.encodeNumberToRadix64(req.query.userId)
    );
  }

  @Get('/utilities/encode-playlist-id')
  getEncodePlaylistId(@Req() req: Ticksa.Request) {
    return new UtilitiesMessages.EncodePlaylistIdGetResponse(util.encodeNumberToRadix64(req.query.playlistId));
  }
}