/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import * as _ from 'lodash';
import * as requestPromise from 'request-promise';
import * as sequelize from 'sequelize';

import * as conn from '../connection';
import * as Errors from '../errors';
import * as LibraryMessages from '../dto/library/library-messages';
import * as TicksTransactionService from '../services/ticks-transaction-service';
import * as ImageService from '../services/image-service';
import * as LibraryService from '../services/library-service';
import * as AWSService from '../services/aws';

import {
  Tickser,
  TicksTransaction,
  Channel,
  Content,
  ContentVideo,
  ContentImage,
  Image,
  Video,
  VideoFile,
  File,
  Feed,
  Playlist
} from '../models/models';

import BaseController from './base-controller';
import {Get, Post, Delete, JsonController, Req, UseBefore} from 'routing-controllers/index';
import AuthMiddleware from '../middlewares/auth-middleware';

enum MediaType {
  IMAGE = 0,
  VIDEO = 1
}

enum FileUsed {
  USED = 1,
  UNUSED = 0
}

interface QueryFilter {
  mediaTypes?: MediaType[],
  used?: FileUsed,
  order?: {
    createdAt: string
  }
}

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class LibraryController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/library/media')
  async getMedia(@Req() req: Ticksa.Request) {
    const user: Tickser.Instance = req.local.user;
    const filter: QueryFilter = <QueryFilter> req.query.filter || {};
    const needImages: boolean = _.include(filter.mediaTypes, MediaType.IMAGE);
    const needVideos: boolean = _.include(filter.mediaTypes, MediaType.VIDEO);

    const getMedia: Promise<any>[] = [];

    if (needImages) {
      const findImages = Image.Model.scope(Image.Scopes.MAIN_ATTRIBUTES_AND_FILE_MAIN_ATTRIBUTES).findAll({
        where: {
          tickserId: user.id
        }
      });
      getMedia.push(findImages);
    } else {
      getMedia.push(Promise.resolve([]));
    }

    if (needVideos) {
      const findVideos = Video.Model.scope(Video.Scopes.MAIN_ATTRIBUTES_AND_FILES_MAIN_ATTRIBUTES).findAll({
        where: {
          tickserId: user.id
        }
      });
      getMedia.push(findVideos);
    } else {
      getMedia.push(Promise.resolve([]));
    }

    const [images, videos] = await Promise.all(getMedia);

    if (filter.used == null) {
      return getMediaResponse(images, videos, filter);
    }

    const getUsedVideoIds: Promise<number[]> = (needVideos)
      ? LibraryService.getUsedVideoIds(_.pluck(videos, 'id'))
      : Promise.resolve([]);
    const getUsedImageIds: Promise<number[]> = (needImages)
      ? LibraryService.getUsedImageIds(_.pluck(images, 'id'))
      : Promise.resolve([]);

    const [usedVideoIds, usedImageIds] = await Promise.all([getUsedVideoIds, getUsedImageIds]);

    const filteredVideosByIds: Video.Instance[] = _.filter(videos, (video: Video.Instance) => {
      return filterByUsed(video.id, usedVideoIds, filter.used);
    });
    const filteredImagesByIds: Image.Instance[] = _.filter(images, (image: Image.Instance) => {
      return filterByUsed(image.id, usedImageIds, filter.used);
    });


    return getMediaResponse(filteredImagesByIds, filteredVideosByIds, filter);
  }

  @Post('/library/images')
  async uploadImage(@Req() req: Ticksa.Request) {
    const image = LibraryMessages.ImagePostRequest.parseFromJSON(req.body);
    const price = this.calculateFilePrice(image.file.size);

    const createdImage: Image.Instance = await conn.transaction(async(t) => {
      const processTransaction = TicksTransactionService.processOneWayTransaction(req.local.user.id, null, null, price,
        TicksTransaction.Type.UPLOAD_IMAGE, t);

      const createFileAndImage = createFileAndLibImage(image, req.local.user.id, t);
      const [ticksTransaction, createdImage] = await Promise.all([processTransaction, createFileAndImage]);

      return createdImage;
    });

    return new LibraryMessages.ImageGetResponse(createdImage);
  }

  @Post('/library/images-multiple')
  async uploadImages(@Req() req: Ticksa.Request) {
    const requestImages: LibraryMessages.ImagePostRequest[] = _.map(req.body, image => {
      return LibraryMessages.ImagePostRequest.parseFromJSON(image);
    });

    const createdImages: Image.Instance[] = await conn.transaction(async(t) => {
      const createdFilePromises: Promise<Image.Instance>[] = [];
      let totalSize = 0;
      _.forEach(requestImages, image => {
        createdFilePromises.push(createFileAndLibImage(image, req.local.user.id, t));
        totalSize += image.file.size;
      });
      const createdImages: Image.Instance[] = await Promise.all(createdFilePromises);
      const amount = this.calculateFilePrice(totalSize);

      await TicksTransactionService.processOneWayTransaction(req.local.user.id, null, null, amount,
        TicksTransaction.Type.UPLOAD_IMAGE, t);
      return createdImages;
    });

    const responseImages: LibraryMessages.ImageGetResponse[] = _.map(createdImages, createdImage => {
      return new LibraryMessages.ImageGetResponse(createdImage);
    });
    return responseImages;
  }

  @Post('/library/images/rotate')
  async rotateImages(@Req() req: Ticksa.Request) {
    req.checkBody('data', 'Invalid params').notEmpty();
    req.assertValid();

    const params: ImageService.RotateParam[] = req.body.data;

    await ImageService.rotateImages(params, req.local.user.id);
    return this.ok();
  }

  @Get('/library/images')
  async getImages(@Req() req: Ticksa.Request) {
    const images: Image.Instance[] = await Image.Model.scope(Image.Scopes.MAIN_ATTRIBUTES_AND_FILE_MAIN_ATTRIBUTES).findAll({
      where: {
        tickserId: req.local.user.id
      },
      order: [
        ['created_at', 'DESC'],
      ]
    });

    const responseImages: LibraryMessages.ImageGetResponse[] = _.map(images, image => {
      return new LibraryMessages.ImageGetResponse(image);
    });

    return responseImages;
  }

  @Delete('/library/images/:imageId')
  async deleteImage(@Req() req: Ticksa.Request) {
    req.checkParams('imageId', 'Invalid params').notEmpty();
    req.assertValid();

    const image: Image.Instance = await Image.Model.scope(Image.Scopes.INCLUDE_FILE).findOne({
      where: {
        id: req.params.imageId
      }
    });

    if (!image) {
      throw new Errors.NotFound('Image not found');
    }

    const canDelete = await canDeleteImage(image);

    if (!canDelete) {
      throw new Errors.BadRequest('Image used');
    }
    await destroyImageFiles(image);
    return this.ok();
  }

  @Post('/library/videos')
  async uploadVideo(@Req() req: Ticksa.Request) {
    const user: Tickser.Instance = req.local.user;

    const videoPostRequest: LibraryMessages.VideoPostRequest = LibraryMessages.VideoPostRequest.parseFromJSON(req.body);
    const url: any = this.config.env().aws.s3.url + '/' + this.config.env().aws.s3.bucket + '/' + videoPostRequest.file.key;
    const response: any = await requestPromise.head(url);

    let contentLength;
    try {
      contentLength = response['content-length'];
    } catch (e) {
      this.logger.error('Problem while analyzing response on HEAD request to define uploaded video file size',
        {
          initialRequest: videoPostRequest,
          urlOfHead: url,
          responseOnHead: response,
          exception: e,
          exceptionStack: e.stack
        }
      );
      throw new Errors.BadRequest('Error while reading video size');
    }

    if (isNaN(contentLength)) {
      this.logger.error('Problem while analyzing response on HEAD request to define uploaded video file size. ' +
        '"Content-Length" is not a number: ' + contentLength);
      throw new Errors.BadRequest('Error while reading video size. Invalid length');
    }

    const videoSize = parseFloat(contentLength);
    const price = this.calculateFilePrice(videoSize);

    const video: Video.Instance = await conn.transaction(async(t) => {
      const createVideo = Video.Model.create({
          tickserId: user.id,
          name: videoPostRequest.name,
          transcodingStatus: Video.TranscodingStatus.START
        },
        {transaction: t});

      const processOneWayTransaction = TicksTransactionService.processOneWayTransaction(
        user.id, null, null, price, TicksTransaction.Type.UPLOAD_VIDEO, t);

      const [createdLibVideo, transactionResponse] = await Promise.all([createVideo, processOneWayTransaction]);
      await AWSService.createTranscoderJob(videoPostRequest.file.key, createdLibVideo.id, ['G360P16x9'], t);
      return createdLibVideo;
    });

    return new LibraryMessages.VideoGetResponse(video);
  }

  @Get('/library/video/:videoId')
  async getVideo(@Req() req: Ticksa.Request) {
    const video: Video.Instance = await Video.Model.scope(Video.Scopes.MAIN_ATTRIBUTES_AND_FILES_MAIN_ATTRIBUTES).findOne({
      where: {
        tickserId: req.local.user.id,
        id: req.params.videoId
      }
    });

    if (!video) {
      throw new Errors.NotFound('Video not found')
    }

    return new LibraryMessages.VideoGetResponse(video);
  }

  @Delete('/library/videos/:videoId')
  async deleteVideo(@Req() req: Ticksa.Request) {
    const video: Video.Instance = await Video.Model.scope(Video.Scopes.INCLUDE_VIDEO_FILES).findOne({
      where: {
        id: req.params.videoId,
        tickserId: req.local.user.id
      }
    });

    if (!video) {
      throw new Errors.NotFound('Video not found')
    }

    const canDelete = await canDeleteVideo(video);
    if (!canDelete) {
      throw new Errors.BadRequest('Video used in content');
    }
    await destroyVideoFiles(video);
    return this.ok();
  }

  @Get('/library/videos')
  async getVideos(@Req() req: Ticksa.Request) {
    const videos: Video.Instance[] = await Video.Model.scope(Video.Scopes.MAIN_ATTRIBUTES_AND_FILES_MAIN_ATTRIBUTES).findAll({
      where: {
        tickserId: req.local.user.id
      },
      order: [
        ['created_at', 'DESC'],
      ],
    });
    const responseVideos: LibraryMessages.VideoGetResponse[] = _.map(videos, video => {
      return new LibraryMessages.VideoGetResponse(video);
    });

    return responseVideos;
  }

  @Get('/library/calculate-video-price')
  async calculateVideoPrice(@Req() req: Ticksa.Request) {
    return {price: this.calculateFilePrice(req.query.size)};
  }

  @Get('/library/calculate-image-price')
  async calculateImagePrice(@Req() req: Ticksa.Request) {
    return {price: this.calculateFilePrice(req.query.size)};
  }

  private calculateFilePrice(size) {
    return (Math.ceil(size / (200 * 1000000.0))) * this.config.global.videoPer200MBTickPrice;
  }
}

function getMediaResponse(images, videos, filter): LibraryMessages.MediaGetResponse[] {
  const imageResponses: LibraryMessages.MediaGetResponse[] = _.map(images, (image: Image.Instance) => {
    return LibraryMessages.MediaGetResponse.forImage(image);
  });

  const videoResponses: LibraryMessages.MediaGetResponse[] = _.map(videos, (video: Video.Instance) => {
    return LibraryMessages.MediaGetResponse.forVideo(video);
  });

  const mediaResponses: LibraryMessages.MediaGetResponse[] = _.union(imageResponses, videoResponses);
  const order: string = (filter.order && filter.order.createdAt) ? filter.order.createdAt : 'desc';
  const media: LibraryMessages.MediaGetResponse[] = _.sortByOrder(mediaResponses, ['createdAt'], [order]);

  return media;
}

function destroyVideoFiles(video: Video.Instance): Promise<void> {
  return conn.transaction({
    deferrable: sequelize.Deferrable.SET_DEFERRED
  }, async function (t: sequelize.Transaction) {

    const fileKeysToDelete: string[] = [];
    const videoFilePromises: Promise<number>[] = [];
    const promises: Promise<number>[] = [];

    _.forEach(video.videoFiles, function (videoFile: VideoFile.Instance) {
      fileKeysToDelete.push(videoFile.file.key);
      videoFilePromises.push(destroyVideoByVideoIdAndFileId(videoFile.videoId, videoFile.fileId, t));
      promises.push(destroyFileById(videoFile.fileId, t));
    });
    promises.push(destroyLibVideoById(video.id, t));

    await Promise.all(videoFilePromises);
    await Promise.all(promises);
    _.forEach(fileKeysToDelete, function (videoFileKey: string) {
      AWSService.deleteFile(videoFileKey);
    });
  });
}

function destroyImageFiles(image: Image.Instance): Promise<void> {
  return conn.transaction({
    deferrable: sequelize.Deferrable.SET_DEFERRED
  }, async function (t) {
    await Promise.all([
      destroyLibImageById(image.id, t),
      destroyFileById(image.file.id, t)
    ]);
    AWSService.deleteFile(image.file.key);
  });
}

function destroyVideoByVideoIdAndFileId(videoId: number, fileId: number, t: sequelize.Transaction): Promise<number> {
  return VideoFile.Model.destroy({
    where: {
      videoId: videoId,
      fileId: fileId
    },
    transaction: t
  });
}

function destroyFileById(fileId: number, t: sequelize.Transaction): Promise<number> {
  return File.Model.destroy({
    where: {
      id: fileId
    },
    transaction: t
  });
}

function destroyLibVideoById(videoId: number, t: sequelize.Transaction): Promise<number> {
  return Video.Model.destroy({
    where: {
      id: videoId
    },
    transaction: t
  });
}

function destroyLibImageById(imageId: number, t: sequelize.Transaction): Promise<number> {
  return Image.Model.destroy({
    where: {
      id: imageId
    },
    transaction: t
  });
}

async function canDeleteVideo(video: Video.Instance): Promise<boolean> {
  const promises: Promise<number>[] = [];
  promises.push(Content.Model.countByTeaserVideoId(video.id));
  promises.push(ContentVideo.Model.countByVideoId(video.id));
  promises.push(Feed.Model.countByVideoId(video.id));

  const counters: number[] = await Promise.all(promises);

  let canDelete = true;
  _.forEach(counters, (count: number) => {
    if (count > 0) {
      canDelete = false;
    }
  });

  return canDelete;
}

async function canDeleteImage(image: Image.Instance): Promise<boolean> {
  const promises: Promise<number>[] = [];
  promises.push(Tickser.Model.countByProfileImageId(image.id));
  promises.push(Content.Model.countByTeaserImageId(image.id));
  promises.push(ContentImage.Model.countByImageId(image.id));
  promises.push(Feed.Model.countByImageId(image.id));
  promises.push(Channel.Model.countByChannelBackgroundId(image.id));
  promises.push(Channel.Model.countByCoverImageId(image.id));
  promises.push(Playlist.Model.countByCoverImageId(image.id));
  promises.push(Playlist.Model.countByBackgroundImageId(image.id));

  const counters: number[] = await Promise.all(promises);

  let canDelete = true;
  _.forEach(counters, (count: number) => {
    if (count > 0) {
      canDelete = false;
    }
  });

  return canDelete;
}

function createFileAndLibImage(image, userId, t): Promise<Image.Instance> {
  return createFileByImage(image, t)
    .then(function (createdFile) {
      return createLibImageByImageAndFile(image, createdFile, userId, t)
        .then(function (createdImage) {
          createdImage.file = createdFile;
          return createdImage;
        });
    });
}

function createFileByImage(image, t): Promise<File.Instance> {
  return File.Model.create({
    key: image.file.key,
    name: image.file.name,
    extension: image.file.extension,
    size: image.file.size,
    isUploaded: image.file.isUploaded
  }, {transaction: t});
}

function createLibImageByImageAndFile(image, createdFile, userId, t): Promise<Image.Instance> {
  return Image.Model.create({
    name: image.name,
    fileId: createdFile.id,
    tickserId: userId,
  }, {transaction: t});
}

function filterByUsed<T>(id: T, usedIds: T[], isUsed: FileUsed): boolean {
  var containsInUsed = _.include(usedIds, id);

  if (isUsed == FileUsed.UNUSED) {
    return !containsInUsed;
  }
  if (isUsed == FileUsed.USED) {
    return containsInUsed
  }
  return true;
}