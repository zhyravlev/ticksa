/// <reference path="../typings/tsd.d.ts"/>
import _ = require("lodash");
import rp = require('request-promise');
import Ticksa = require("ticksa");
import AccountMessages = require('../dto/account/account-messages');
import TicksTransactionService = require('../services/ticks-transaction-service');
import SecurityService = require('../services/security-service');

import Models = require('../models/models');
import Account = Models.Account;
import Tickser = Models.Tickser;
import Country = Models.Country;
import TickserSocial = Models.TickserSocial;
import Content = Models.Content;
import Currency = Models.Currency;
import EmailRecipient = Models.EmailRecipient;

const fb = require('fb');
const bcrypt = require('bcrypt');

import conn = require('../connection');
import RegistrationService = require('../services/register');
import MailService = require('../services/mail-service');
import I18NService = require('../services/i18n-service');
import BaseController from './base-controller';
import {JsonController} from 'routing-controllers/index';
import {Req, Res, Body, HttpCode, Delete, Put, Get, Post, QueryParam} from 'routing-controllers/index';
import {BadRequest, Unauthorized, NotFound, Forbidden, ServerError} from '../errors';
import AuthMiddleware from '../middlewares/auth-middleware';
import {UseBefore} from 'routing-controllers/index';

interface ChangePasswordBody {
  newPassword: string;
  oldPassword: string;
}

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class AccountController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Delete('/account/terminate')
  async terminateAccount(@Req() req: Ticksa.Request) {
    const tickser: Tickser.Instance = req.local.user;
    tickser.status = Tickser.Status.DEACTIVATED;
    tickser.terminatedDate = new Date();
    await tickser.save();

    await Content.Model.update({
      status: Content.Status.DEACTIVATED
    }, {
      where: {
        tickserId: tickser.id
      }
    });
    return this.ok();
  }

  @Put('/account/profile/update')
  async updateProfile(@Req() req: Ticksa.Request) {
    var profileRequest = AccountMessages.ProfileUpdateRequest.parseFromJSON(req.body);
    var phoneVerified = (req.local.user.phone == profileRequest.phone) && req.local.user.isPhoneVerified;

    var tickserData: Tickser.Attributes = {
      name: profileRequest.name,
      language: profileRequest.language,
      phone: profileRequest.phone,
      profileImageId: profileRequest.profileImageId,
      defaultChannelId: profileRequest.defaultChannelId,
      countryId: profileRequest.countryId,
      isPhoneVerified: phoneVerified
    };

    var accountData: Account.Attributes = {
      autoAuthorize: profileRequest.autoAuthorize,
      autoAuthorizeMaxTicks: profileRequest.autoAuthorizeMaxTicks,
      preferredPaymentMethod: profileRequest.paymentMethod,
      currencyId: profileRequest.currencyId
    };

    var updateTickser = Tickser.Model.update(tickserData, {
      where: {
        id: req.local.user.id
      }
    });
    var updateAccount = Account.Model.update(accountData, {
      where: {
        tickserId: req.local.user.id
      }
    });

    await Promise.all([updateTickser, updateAccount]);

    return this.ok();
  }

  @Put('/account/preferences/update')
  async updatePreferences(@Req() req: Ticksa.Request) {
    var preferencesRequest = AccountMessages.PreferencesUpdateRequest.parseFromJSON(req.body);
    await  Tickser.Model.update({
      preferences: preferencesRequest.preferences
    }, {
      where: {
        id: req.local.user.id
      }
    });

    return this.ok();
  }

  @Get('/account/info')
  async getAccountInfo(@Req() req: Ticksa.Request): Promise<AccountMessages.IAccountInfo> {
    if (req.local.user == null) {
      throw new Unauthorized();
    }

    req.local.user.password = undefined;
    const tickser: Tickser.Instance = await Tickser.Model.scope(Tickser.Scopes.INCLUDE_PROFILE_IMAGE).findOne({
      where: {
        id: req.local.user.id
      },
      include: [
        {
          required: false,
          model: Country.Model,
          as: 'country'
        },
        {
          required: false,
          model: TickserSocial.Model,
          as: 'socials'
        },
        {
          required: true,
          model: Account.Model,
          as: 'account'
        }
      ]
    });


    if (tickser == null) {
      throw new NotFound('User not found!');
    }

    return AccountMessages.AccountInfo.parseFromDB(tickser)
  }

  @Get('/account/currencies')
  async getCurrencies(@Req() req: Ticksa.Request) {
    const currencies: Currency.Instance[] = await Currency.Model.findAll();
    return _.map(currencies, (currency: Currency.Instance) => {
      return {id: currency.id, name: currency.name, symbol: currency.symbol, code: currency.iso4217Code};
    });
  }

  @Post('/account/save-phone-and-send-verification-code')
  async savePhoneAndSendVerificationCode(@Req() req: Ticksa.Request) {
    const user = req.local.user;
    const phone = req.body.phone;

    req.checkBody('phone', 'Invalid param').notEmpty();
    req.assertValid();

    const count: number = await Tickser.Model.countUniqueVerifiedPhones(user.id, user.phone);
    if (count > 0) {
      throw new BadRequest('Already verified by someone');
    }

    const number = (Math.floor(Math.random() * 9000) + 1000).toString();
    user.phoneVerificationCode = number;
    user.phone = phone;
    await RegistrationService.sendVerificationSms(phone, number);
    await user.save();

    return this.ok();
  }

  @Post('/account/verify-phone')
  async verifyPhone(@Req() req: Ticksa.Request, @QueryParam('code') code: string) {
    req.checkQuery('code', 'Invalid param').notEmpty();
    req.assertValid();

    const user: Tickser.Instance = req.local.user;

    if (user.phoneVerificationCode != code) {
      throw new BadRequest('Incorrect verification code');
    }

    const result: {firstTime: boolean} = await conn.transaction(async function (t) {
      user.isPhoneVerified = true;
      user.phoneVerificationCode = null;

      if (user.isPhoneVerifiedAtLeastOnce) {
        await user.save({transaction: t});
        return {firstTime: false};
      }

      user.isPhoneVerifiedAtLeastOnce = true;

      let params: TicksTransactionService.GrantFreeTicksParams = {
        tickserId: user.id,
        systemAccountId: Account.SystemAccount.FREE_TICKS_ACCOUNT,
        amount: 10,
        t: t
      };

      var grandFreeTicks = TicksTransactionService.processGrantFreeTicksTransaction(params);
      var saveUser = user.save({transaction: t});

      await Promise.all([grandFreeTicks, saveUser]);
      return {firstTime: true};
    });

    var params = {
      user_name: user.name,
      number: user.phone
    };

    var template = result.firstTime ? EmailRecipient.Template.PHONE_VERIFIED : EmailRecipient.Template.PHONE_VERIFIED_NO_PROMO_TICKS;
    MailService.sendMailToTickser(user, template, params);
    return result;
  }

  @Post('/account/change-password')
  async changePassword(@Req() req: Ticksa.Request, @Body() body: ChangePasswordBody) {
    req.checkBody('newPassword', 'Invalid param').notEmpty();
    req.assertValid();

    const tickser: Tickser.Instance = req.local.user;

    if (tickser.password == null) {
      if (!body.newPassword) {
        throw new Forbidden('Incorrect password');
      }
    }
    else {
      if (!body.newPassword || !body.oldPassword) {
        throw new Forbidden('Incorrect password');
      }

      await SecurityService.comparePassword(body.oldPassword, tickser.password);
    }

    const hash = await SecurityService.createPassword(body.newPassword);
    tickser.password = hash;
    await tickser.save();

    var params = {
      user_name: tickser.name
    };

    MailService.sendMailToTickser(tickser, EmailRecipient.Template.PASSWORD_CHANGED, params);
    return this.ok();
  }

  @Post('/account/connect-social/google')
  async connectSocialGoogle(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response) {
    const body: any = await rp.get('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + req.body.idToken);
    const socialId = JSON.parse(body).sub;
    const tickserSocial = await TickserSocial.Model.findOne({
      where: {
        socialId: socialId
      }
    });

    if (tickserSocial != null && tickserSocial.tickserId != req.local.user.id) {
      throw new BadRequest('This social account is already tied to another user');
    }
    await this.updateSocialConnection(TickserSocial.Type.GOOGLE, req.local.user.id, socialId);

    return this.ok();
  }

  @Post('/account/connect-social/facebook')
  async connectSocialFacebook(@Req() req: Ticksa.Request) {
    const data = fb.parseSignedRequest(req.body.signedRequest, this.config.env().security.facebook.secret);
    if (!data) {
      throw new ServerError('fb::parseSignedRequest data not found')
    }

    const socialId = data.user_id;
    const tickserSocial = await TickserSocial.Model.findOne({
      where: {
        socialId: socialId
      }
    });

    if (tickserSocial != null && tickserSocial.tickserId != req.local.user.id) {
      throw new BadRequest('This social account is already tied to another user');
    }

    await this.updateSocialConnection(TickserSocial.Type.FACEBOOK, req.local.user.id, socialId);

    return this.ok();
  }

  @Post('/account/connect-social/google-disconnect')
  async disconnectSocialGoogle(@Req() req: Ticksa.Request) {
    await TickserSocial.Model.destroy({
      where: {
        type: TickserSocial.Type.GOOGLE,
        tickserId: req.local.user.id
      }
    });
    return this.ok();
  }

  @Post('/account/connect-social/facebook-disconnect')
  async disconnectSocialFacebook(@Req() req: Ticksa.Request) {
    await TickserSocial.Model.destroy({
      where: {
        type: TickserSocial.Type.FACEBOOK,
        tickserId: req.local.user.id
      }
    });
    return this.ok();
  }

  async updateSocialConnection(type: TickserSocial.Type, tickserId: number, socialId: string): Promise<void> {
    const result: TickserSocial.Instance = await conn.transaction(async function (t) {
      await TickserSocial.Model.destroy({
        where: {
          tickserId: tickserId,
          type: type
        },
        transaction: t
      });
      return TickserSocial.Model.create({
        tickserId: tickserId,
        type: type,
        socialId: socialId
      }, {transaction: t});
    });

    this.logger.info('Connected user ' + result.tickserId + ' to ' + result.type);
  }


}
