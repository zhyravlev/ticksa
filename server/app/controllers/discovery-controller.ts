/// <reference path="../typings/tsd.d.ts"/>
import _ = require('lodash');
import Ticksa = require('ticksa');
import Errors = require('../errors');

import conn = require('../connection');
import ContentMessages = require('../dto/content/content-messages');

import Models = require('../models/models');
import Content = Models.Content;

import * as RequestUtils from "../utils/request-utils";
import ContentService = require('../services/content-service');
import BaseController from './base-controller';
import {JsonController, Req, Get} from 'routing-controllers';

@JsonController('/api')
export default class DiscoveryController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/discovery')
  async getDiscoveryWithoutLogin(@Req() req: Ticksa.Request): Promise<ContentMessages.IContentTeaserResponse[]> {
    const pageable: Ticksa.IPageable<RequestUtils.ContentFilter> = RequestUtils.getPageableContentFilter(req);
    const userMeta: ContentService.UserMeta = {countryA2Code: req.local.geoip.countryA2Code, tickserId: null};

    const contents: Content.Instance[] = await ContentService.getAllContentsPagingAndSearch(pageable, userMeta);
    if (!contents || contents.length == 0) {
      return Promise.resolve([]);
    }

    const contentTeasers: ContentMessages.IContentTeaserResponse[] =
      _.map(contents, content => ContentMessages.ContentTeaserResponse.parseFromDB(content, false, false));

    return contentTeasers;
  }

}