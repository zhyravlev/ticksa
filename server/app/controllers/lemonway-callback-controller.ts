/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import * as Errors from '../errors';
import * as lemonway from '../api/lemonway/lemonway';

import * as LemonwayCashInService from '../services/lemonway-cashin-service';
import * as CashIOService from '../services/cashio-service';
import * as LocationUtils from '../utils/ticksa-location-utils';

import {JsonController, Get, Post, Res, Req} from "routing-controllers";
import BaseController from './base-controller';
import {LemonwayCashCallback, LemonwayCashIn} from '../models/models';

@JsonController('/api')
export default class LemonwayCallbackController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Post('/lemonway/callback/:type')
  async handleLemonwayPostRequest(@Req() req: Ticksa.Request) {
    const callbackType = req.params.type;

    const type: LemonwayCashCallback.Type = LemonwayCallbackController.getCallbackTypeOrNull(callbackType);
    if (type == null) {
      this.logger.warn('handleLemonwayPostRequest: invalid type: ', callbackType);
      throw new Errors.BadRequest('Invalid params');
    }

    req.checkBody('response_code', 'Invalid param').notEmpty();
    req.checkBody('response_wkToken', 'Invalid param').notEmpty();
    if (type == LemonwayCashCallback.Type.SUCCESS) {
      req.checkBody('response_transactionId', 'Invalid param').notEmpty();
      req.checkBody('response_transactionAmount', 'Invalid param').notEmpty();
    }

    const errors = req.validationErrors();
    if (errors) {
      this.logger.warn('handleLemonwayPostRequest: ', errors);
      throw new Errors.BadRequest();
    }

    const response: lemonway.FinalizeCardPaymentPostResponse = {
      type: callbackType,
      response_code: req.body.response_code == '0000' ? lemonway.ResponseCode.SUCCESS : lemonway.ResponseCode.ERROR,
      response_wkToken: req.body.response_wkToken,
      response_transactionId: req.body.response_transactionId,
      response_msg: req.body.response_msg,
      response_transactionAmount: req.body.response_transactionAmount,
      response_transactionMessage: req.body.response_transactionMessage,
    };

    const handlingResult: LemonwayCashInService.CallbackHandlingResult = await LemonwayCashInService.handleCallbackAndFinalizeCashIn(response, type, LemonwayCashCallback.Method.POST);
    if (!handlingResult.cashInFinalized) {
      return this.ok();
    }

    await CashIOService.finalizeCashIO(handlingResult.lwCashIn.cashIoId);
    return this.ok();
  }

  @Get('/lemonway/callback/:type')
  async handleLemonwayGetRequest(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response): Promise<void> {
    const type: LemonwayCashCallback.Type = LemonwayCallbackController.getCallbackTypeOrNull(req.params.type);
    if (type == null) {
      this.logger.warn('handleLemonwayGetRequest: invalid type: ', req.params.type);
      throw new Errors.BadRequest('Invalid params');
    }

    if (type == LemonwayCashCallback.Type.CANCEL) {
      res.redirect(this.config.env().server.url + LocationUtils.tl('/financial/payment-callback?cancel=true'));
      return;
    }

    try {
      const lwCallback: LemonwayCashCallback.Instance = await LemonwayCashInService.createLwCallback(
        req.query.response_wkToken, JSON.stringify(req.params), type, LemonwayCashCallback.Method.GET
      );
      const lwCashIn: LemonwayCashIn.Instance = await LemonwayCashIn.Model.findById(lwCallback.cashInId);
      res.redirect(this.config.env().server.url + LocationUtils.tl('/financial/payment-callback?transactionId=' + lwCashIn.cashIoId));

    } catch (err) {
      this.logger.error('Error handling LW GET callback', err);
      res.redirect(this.config.env().server.url + LocationUtils.tl('/financial/payment-callback?error=true'));
    }
  }

  private static getCallbackTypeOrNull(stringType: string): LemonwayCashCallback.Type {
    switch (stringType) {
      case 'success':
        return LemonwayCashCallback.Type.SUCCESS;
      case 'error':
        return LemonwayCashCallback.Type.ERROR;
      case 'cancel':
        return LemonwayCashCallback.Type.CANCEL;
    }
    return null;
  }

}