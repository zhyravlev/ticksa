/// <reference path="../typings/tsd.d.ts"/>

import logger = require('../logger/logger');
import * as winston from 'winston';
import * as config from '../config';
import * as RequestUtils from '../utils/request-utils';

export default class BaseController {
  protected logger: winston.LoggerInstance;
  protected config = config;

  constructor(filename: string) {
    this.logger = logger(filename);
  }

  protected ok() {
    return 'OK';
  }

  protected index(): string {
    return RequestUtils.renderIndexView();
  }

}