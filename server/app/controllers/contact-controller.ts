/// <reference path="../typings/tsd.d.ts"/>
import Ticksa = require('ticksa');

import BaseController from './base-controller';
import {JsonController, Req, Get} from 'routing-controllers';

import MailService = require('../services/mail-service');

@JsonController('/api')
export default class ContactController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/contact/message')
  async sendMessage(@Req() req: Ticksa.Request): Promise<Ticksa.Configuration.GlobalSettings> {
    await MailService.sendMailToContactWithTicksa(req.body.from, req.body.message);
    return this.ok();
  }

}