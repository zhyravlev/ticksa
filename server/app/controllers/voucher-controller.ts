/// <reference path="../typings/tsd.d.ts"/>
import _ = require('lodash');
import request = require('request');
import Ticksa = require('ticksa');

import BaseController from './base-controller';
import {JsonController, Req, Post, Get} from 'routing-controllers/index';
import {NotFound} from '../errors';

import VoucherMessages = require('../dto/voucher/voucher-messages');
import TicksTransactionService = require('../services/ticks-transaction-service');

import conn = require('../connection');

import Models = require('../models/models');
import Account = Models.Account;
import Tickser = Models.Tickser;
import Voucher = Models.Voucher;
import TickserVoucher = Models.TickserVoucher;
import VoucherActivity = Models.VoucherActivity;
import AuthMiddleware from '../middlewares/auth-middleware';
import {UseBefore} from 'routing-controllers/index';


@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class VoucherController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/voucher/rights')
  async getTickserRights(@Req() req: Ticksa.Request) {
    const tickser: TickserWithRestrictions = await this.getTickserWithRestrictions(req.local.user.id);
    return new VoucherMessages.TickserRights(tickser.canCheck(), tickser.isBanned, tickser.isMaxVouchers)
  }

  @Post('/voucher/check')
  async checkVoucher(@Req() req: Ticksa.Request) {
    const request: VoucherMessages.VoucherPostRequest =
      VoucherMessages.VoucherPostRequest.parseFromJSON(req.body, VoucherActivity.Type.CHECK);
    try {
      const voucherCheckResult: VoucherCheckResult = await this.checkVoucherCode(req.local.user.id, request);
      if (voucherCheckResult.errorResponse) {
        return voucherCheckResult.errorResponse;
      } else {
        this.logVoucherActivity(req.local.user, request, VoucherActivity.Result.VALID_CODE, voucherCheckResult.voucher);
        return VoucherMessages.VoucherPostResponse.validCode(voucherCheckResult.voucher);
      }
    } catch (err) {
      this.logVoucherActivityError(req.local.user, request, JSON.stringify(err));
      throw err;
    }
  }

  @Post('/voucher/apply')
  async applyVoucher(@Req() req: Ticksa.Request) {
    const request: VoucherMessages.VoucherPostRequest =
      VoucherMessages.VoucherPostRequest.parseFromJSON(req.body, VoucherActivity.Type.APPLY);
    try {
      const voucherCheckResult: VoucherCheckResult = await this.checkVoucherCode(req.local.user.id, request);
      if (voucherCheckResult.errorResponse) {
        return voucherCheckResult.errorResponse;
      }

      await this.grantFreeTicks(req.local.user, voucherCheckResult.voucher);

      this.logVoucherActivity(req.local.user, request, VoucherActivity.Result.SUCCESSFULLY_APPLIED, voucherCheckResult.voucher);

      if (voucherCheckResult.availableMoreVouchersCount == 1) {
        return VoucherMessages.VoucherPostResponse.maxVouchersCountBecauseApplied(voucherCheckResult.voucher);
      } else {
        return VoucherMessages.VoucherPostResponse.successfullyApplied(voucherCheckResult.voucher);
      }
    } catch (err) {
      this.logVoucherActivityError(req.local.user, request, JSON.stringify(err));
      throw err;
    }
  }

  async grantFreeTicks(tickser, voucher): Promise<TicksTransactionService.GrandFreeTicksResult> {
    return conn.transaction(async (t) => {
      const params: TicksTransactionService.GrantFreeTicksParams = {
        tickserId: tickser.id,
        systemAccountId: Account.SystemAccount.VOUCHER_ACCOUNT,
        amount: voucher.ticksAmount,
        t: t
      };

      const grantFreeTicks = TicksTransactionService.processGrantFreeTicksTransaction(params);
      const createVoucher = TickserVoucher.Model.create({
        tickserId: tickser.id,
        voucherId: voucher.id
      }, {transaction: t});


      const [transactionData, tickserVoucher] = await Promise.all([grantFreeTicks, createVoucher]);
      return transactionData;
    });
  }


  async getTickserWithRestrictions(id: number) {
    const tickser = await Tickser.Model.findOne(
      {
        where: {
          id: id
        },
        include: [
          {
            required: false,
            model: Voucher.Model,
            as: 'vouchers'
          },
          {
            required: false,
            model: VoucherActivity.Model,
            as: 'vouchersActivities'
          }
        ]
      }
    );

    if (!tickser) {
      throw new NotFound('User not found');
    }

    return TickserWithRestrictions.parseFromDb(tickser, this.config.global.voucherInvalidChecksToBan,
      this.config.global.voucherMaxCount)
  }


  async checkVoucherCode(userId: number, request: VoucherMessages.VoucherPostRequest): Promise<VoucherCheckResult> {
    const tickser: TickserWithRestrictions = await this.getTickserWithRestrictions(userId);

    if (tickser.isBanned) {
      this.logVoucherActivity(tickser.tickser, request, VoucherActivity.Result.BANNED);
      return VoucherCheckResult.banned();
    }

    if (tickser.isMaxVouchers) {
      this.logVoucherActivity(tickser.tickser, request, VoucherActivity.Result.VOUCHERS_COUNT_EXCEEDED);
      return VoucherCheckResult.vouchersCountExceeded();
    }

    var matchedVoucher: any = _.find(tickser.tickser.vouchers, {'code': request.code});
    if (matchedVoucher) {
      this.logVoucherActivity(tickser.tickser, request, VoucherActivity.Result.ALREADY_APPLIED, matchedVoucher);
      return VoucherCheckResult.alreadyApplied();
    }

    else {
      const voucher = await Voucher.Model.findOne({
        where: {
          code: request.code
        }
      });

      if (!voucher) {
        this.logVoucherActivity(tickser.tickser, request, VoucherActivity.Result.INVALID_CODE);
        if (tickser.availableInvalidCodesTryouts == 1) {
          return VoucherCheckResult.bannedBecauseInvalidCode();
        } else {
          return VoucherCheckResult.invalidCode();
        }
      }

      else {
        if (voucher.startDate && (new Date() < voucher.startDate)) {
          this.logVoucherActivity(tickser.tickser, request, VoucherActivity.Result.DATE_LIMITED, voucher);
          if (tickser.availableInvalidCodesTryouts == 1) {
            return VoucherCheckResult.bannedBecauseInvalidCode();
          } else {
            return VoucherCheckResult.invalidCode();
          }
        }

        else if (voucher.stopDate && (new Date() > voucher.stopDate)) {
          this.logVoucherActivity(tickser.tickser, request, VoucherActivity.Result.DATE_LIMITED, voucher);
          if (tickser.availableInvalidCodesTryouts == 1) {
            return VoucherCheckResult.bannedBecauseInvalidCode();
          } else {
            return VoucherCheckResult.invalidCode();
          }
        }

        else {
          return VoucherCheckResult.validCode(voucher, tickser.availableMoreVouchersCount);
        }
      }
    }

  }

  logVoucherActivity(tickser: any, request: VoucherMessages.VoucherPostRequest, result: number, voucher?: any, error?: string): Promise<VoucherActivity.Instance> {
    const attrs: VoucherActivity.Attributes = {
      tickserId: tickser.id,
      code: request.code,
      type: request.type,
      result: result,
      voucherId: !!voucher ? voucher.id : null,
      error: !!error ? error : null
    };

    return VoucherActivity.Model.create(attrs);
  }


  async logVoucherActivityError(tickser: any, request: VoucherMessages.VoucherPostRequest, error: string) {
    return this.logVoucherActivity(tickser, request, VoucherActivity.Result.ERROR, undefined, error)
  }
}

class TickserWithRestrictions {
  tickser: any;
  isBanned: boolean;
  isMaxVouchers: boolean;
  availableInvalidCodesTryouts: number;
  availableMoreVouchersCount: number;

  constructor(tickser: any,
              availableInvalidCodesTryouts: number,
              availableMoreVouchersCount: number) {
    this.tickser = tickser;
    this.isBanned = availableInvalidCodesTryouts <= 0;
    this.isMaxVouchers = availableMoreVouchersCount <= 0;
    this.availableInvalidCodesTryouts = availableInvalidCodesTryouts;
    this.availableMoreVouchersCount = availableMoreVouchersCount;
  }

  canCheck(): boolean {
    return !this.isBanned && !this.isMaxVouchers;
  }

  static parseFromDb(tickser: any, invalidChecksToBan: number, maxVouchersCount: number): TickserWithRestrictions {
    var invalids = _.filter(tickser.vouchersActivities, {'result': VoucherActivity.Result.INVALID_CODE});
    var limited = _.filter(tickser.vouchersActivities, {'result': VoucherActivity.Result.DATE_LIMITED});

    return new TickserWithRestrictions(
      tickser,
      invalidChecksToBan - invalids.length - limited.length,
      maxVouchersCount - tickser.vouchers.length
    );
  }

}

class VoucherCheckResult {
  errorResponse: VoucherMessages.VoucherPostResponse;
  voucher: any;
  availableMoreVouchersCount: number;

  constructor(errorResponse: VoucherMessages.VoucherPostResponse, voucher?: any, availableMoreVouchersCount?: number) {
    this.errorResponse = errorResponse;
    this.voucher = voucher;
    this.availableMoreVouchersCount = availableMoreVouchersCount;
  }

  static banned() {
    return new VoucherCheckResult(VoucherMessages.VoucherPostResponse.banned());
  }

  static vouchersCountExceeded() {
    return new VoucherCheckResult(VoucherMessages.VoucherPostResponse.vouchersCountExceeded());
  }

  static alreadyApplied() {
    return new VoucherCheckResult(VoucherMessages.VoucherPostResponse.alreadyApplied());
  }

  static invalidCode() {
    return new VoucherCheckResult(VoucherMessages.VoucherPostResponse.invalidCode());
  }

  static bannedBecauseInvalidCode() {
    return new VoucherCheckResult(VoucherMessages.VoucherPostResponse.bannedBecauseInvalidCode());
  }

  static validCode(voucher, availableMoreVouchersCount) {
    return new VoucherCheckResult(undefined, voucher, availableMoreVouchersCount);
  }
}