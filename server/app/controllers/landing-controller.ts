/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import {JsonController, Req, Post} from 'routing-controllers';
import * as LandingService from '../services/landing-service';
import * as SecurityService from '../services/security-service';
import BaseController from './base-controller';
import {Tickser} from '../models/models';

@JsonController('/api')
export default class LandingController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Post('/landing')
  async landing(@Req() req: Ticksa.Request) {
    const url = req.body.url;

    if (url == null) {
      this.logger.error('Invalid landing registration request - without URL');
      return this.ok();
    }

    if (req.headers['token'] == null) {
      await LandingService.registerForAnonymous(url, req);
      return this.ok();
    }

    let data: SecurityService.TokenData;
    try {
      data = await SecurityService.verifyToken(req.headers['token']);
    } catch (err) {
      this.logger.error('Invalid landing registration request - cant verify token', req.headers['token']);
      await LandingService.registerForAnonymous(url, req);
      return this.ok();
    }

    const tickser: Tickser.Instance = await Tickser.Model.findOneByUsername(data.username.toLowerCase());
    await LandingService.registerForTickser(tickser, url, req);
    return this.ok();
  }
}