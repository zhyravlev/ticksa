/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import * as LemonwayService from '../services/lemonway-service';
import BaseController from './base-controller';
import {JsonController, Req, Get} from 'routing-controllers';
import AuthMiddleware from '../middlewares/auth-middleware';
import {UseBefore} from 'routing-controllers/index';

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class LemonwayController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/lemonway/wallet-details')
  async getWalletDetails(@Req() req: Ticksa.Request) {
    const details: LemonwayService.WalletDetails = await LemonwayService.request(req).getWalletDetails(req.local.user.account.id);
    return details;
  }
}