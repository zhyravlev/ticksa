/// <reference path="../typings/tsd.d.ts"/>

import {QueryParam, Get, Controller} from "routing-controllers";
import {UseBefore} from 'routing-controllers/index';
import * as AwsService from '../services/aws';
import BaseController from './base-controller';
import AuthMiddleware from '../middlewares/auth-middleware';

@Controller('/api')
@UseBefore(AuthMiddleware)
export default class AwsController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/aws/generate-signature')
  async generateSignature(@QueryParam('to_sign') toSign: string) {
    this.logger.debug('generateSignature for: ' + toSign);
    return AwsService.generateSignature(toSign);
  }

}