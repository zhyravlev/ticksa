/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import * as Errors from '../errors';
import * as LocationUtils from '../utils/ticksa-location-utils';
import * as UtilService from '../services/utilities';
import {Content} from '../models/models';
import BaseController from './base-controller';
import {Get, Req, Res, Controller} from 'routing-controllers/index';
import {ParsingResult, ContentQuery, Parser, Crawler} from '../services/sharing-service';

@Controller()
export default class SocialSharingController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/s/:encodedData')
  async getShareAsLink(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response) {
    req.checkParams('encodedData', 'Invalid param').notEmpty();
    req.assertValid();

    const encodedData: string = req.params.encodedData.split('-');
    const contentId: number = UtilService.decodeNumberFromRadix64(encodedData[0]);
    const referrerId: number = UtilService.decodeNumberFromRadix64(encodedData[1]);
    const userAgent = req.headers['user-agent'];

    if (Crawler.isSocialNetworkCrawler(userAgent)) {
      const data = await Crawler.getResponseToSocialNetworkCrawler(contentId);
      return res.send(data);
    }

    if (Crawler.isSearchEngineCrawler(userAgent)) {
      const data = await Crawler.getResponseToSearchEngineCrawler(contentId);
      return res.send(data);
    }

    const content: Content.Instance = await Content.Model.findOne({
      attributes: ['id', 'channelId'],
      where: {
        id: contentId
      }
    });

    if (content == null) {
      throw new Errors.NotFound('Content not found');
    }

    res.redirect(LocationUtils.tl('/teaser/' + content.channelId + '/' + content.id + '?referrerId=' + referrerId));
  }

  @Get('/channels/:channelId/contents/:contentId')
  async getContentPage(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response): Promise<any> {
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    const contentId = req.params.contentId;
    const userAgent = req.headers['user-agent'];

    if (Crawler.isSocialNetworkCrawler(userAgent)) {
      const data = await Crawler.getResponseToSocialNetworkCrawler(contentId);
      return res.send(data);
    }

    if (Crawler.isSearchEngineCrawler(userAgent)) {
      const data = await Crawler.getResponseToSearchEngineCrawler(contentId);
      return res.send(data);
    }

    return res.send(this.index());
  }

  @Get('/channels/:channelId/contents')
  async getContents(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response): Promise<any> {
    return res.send(this.index());
  }

  @Get('/teaser/:channelId/:contentId')
  async getContentTeaser(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response): Promise<any> {
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    const contentId = req.params.contentId;
    const userAgent = req.headers['user-agent'];

    if (Crawler.isSocialNetworkCrawler(userAgent)) {
      const data = await Crawler.getResponseToSocialNetworkCrawler(contentId);
      return res.send(data);
    }

    if (Crawler.isSearchEngineCrawler(userAgent)) {
      const data = await Crawler.getResponseToSearchEngineCrawler(contentId);
      return res.send(data);
    }

    return res.send(this.index());
  }

  public static ESCAPED_FRAGMENT_PARAM: string = '_escaped_fragment_';

  /*
   * According to APP-757.
   *
   * Such references can be queried if user uses legacy browser. In such cases he sees ulrs with
   * hashbangs (#!). If he copies link to FB (e.g. http://ticksa.com/#!/channels/1/contents/2)
   * then FB's crawler will query Ticksa by link with _escaped_fragment_ parameter
   * (e.g. http://ticksa.com/?_escaped_fragment_=%2Fchannels%2F1%2Fcontents%2F2)
   *
   * https://code.angularjs.org/1.4.8/docs/guide/$location#crawling-your-app
   */
  @Get('/')
  async getIndex(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response): Promise<any> {
    const escFrag = req.query[SocialSharingController.ESCAPED_FRAGMENT_PARAM];
    if (escFrag == null) {
      return res.send(this.index());
    }

    const queryForContent: ParsingResult<ContentQuery> = Parser.isQueryForContent(escFrag);
    if (!queryForContent.isSuitable) {
      return res.send(this.index());
    }

    const contentId = queryForContent.data.contentId;
    const userAgent = req.headers['user-agent'];

    if (Crawler.isSocialNetworkCrawler(userAgent)) {
      const data = await Crawler.getResponseToSocialNetworkCrawler(contentId);
      return res.send(data);
    }

    if (Crawler.isSearchEngineCrawler(userAgent)) {
      const data = await Crawler.getResponseToSearchEngineCrawler(contentId);
      return res.send(data);
    }

    this.logger.warn('Strange! Someone who is not a crawler queries like crawler does.'
      + ' url: ' + req.url
      + '. ua: ' + req.headers['user-agent']);

    return res.send(this.index());
  }
}

