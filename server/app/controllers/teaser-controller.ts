/// <reference path="../typings/tsd.d.ts"/>
import Ticksa = require('ticksa');
import BaseController from './base-controller';
import {JsonController, Req, Get} from 'routing-controllers';
import {Content} from '../models/models';

import ContentMessages = require('../dto/content/content-messages');
import ContentService = require('../services/content-service');
import {NotFound} from '../errors';
import {ContentForbidden} from '../errors';

@JsonController('/api')
export default class TeaserController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/teaser/channels/:channelId/contents/:contentId')
  async getContentTeaserNotLoggedIn(@Req() req: Ticksa.Request): Promise<ContentMessages.IContentTeaserResponse> {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    const channelId = req.params.channelId;
    const contentId = req.params.contentId;
    const scopes: any = Content.Scopes.INCLUDE_COMMON_DATA.concat([
      Content.Scopes.INCLUDE_CONDITIONS,
      Content.Scopes.IS_PUBLISHED,
      Content.Scopes.IS_ACTIVE,
      Content.Scopes.IS_NOT_EXPIRED,
      {method: [Content.Scopes.INCLUDE_REQUIRED_PUBLIC_ACTIVE_CHANNEL, channelId]}
    ]);
    const content: Content.Instance = await Content.Model.scope(scopes).findById(contentId);

    if (content == null) {
      throw new NotFound('Content not found');
    }

    if (!content.isValidCountry(req.local.geoip.countryA2Code)) {
      throw ContentForbidden.geographyLimited('Content not available in your country');
    }

    if (content.conditions.length > 0) {
      throw ContentForbidden.notAvailable('Content not available');
    }

    return ContentMessages.ContentTeaserResponse.parseFromDB(content, false, false);
  }

}