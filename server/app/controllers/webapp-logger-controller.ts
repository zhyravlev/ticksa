/// <reference path="../typings/tsd.d.ts"/>
import _ = require('lodash');
import util = require('util')

import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import {JsonController, Req, Post} from 'routing-controllers';

import SecurityService = require('../services/security-service');
import UtilService = require('../services/utilities');

import BaseController from './base-controller';
import {BadRequest} from '../errors';

interface LogFromWebapp {
  type: string;
  message: string;
  encId: string;
}

@JsonController('/api')
export default class WebappLoggerController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Post('/logger/log')
  async log(@Req() req: Ticksa.Request): Promise<any> {
    const token = req.headers['token'];
    const logs: LogFromWebapp[] = req.body.logs || [];
    if (logs.length == 0) {
      return this.ok();
    }

    let tickserId: number = null;
    if (token != null) {
      const tokenData = SecurityService.decodeToken(token);
      tickserId = UtilService.decodeNumberFromRadix64(tokenData.encId);
    }

    logs.forEach(log => this.logMessage(log, tickserId));

    return this.ok();
  }

  logMessage(log: LogFromWebapp, tickserId: number) {
    const logMessage = this.constructStoredIdInfo(tickserId, log.encId) + ' ' + this.parseMessage(log.message);
    const message = this.formatMessage(logMessage, tickserId);
    this.logger.log(log.type, message);
  }

  formatMessage(msg: string, tickserId?: number): string {
    if (tickserId != null) {
      return util.format('(tid: %d) %s', tickserId, msg);
    }

    return util.format('(tid: no) %s', msg);
  }

  parseMessage(content: any): string {
    if (content == null) {
      return '';
    }

    if (typeof content != 'string') {
      // util.inspect() is used according to this article:
      // http://www.bennadel.com/blog/2829-string-interpolation-using-util-format-and-util-inspect-in-node-js.htm
      return util.inspect(content);
    }

    return content;
  }


  // this function prevents information duplication - if tickerId for specific log stored in webapp is the same as
  // tickserId of request's maker
  constructStoredIdInfo(loggingTickserId: number, encIdForSpecificLog: string): string {
    if ((loggingTickserId == null) && (encIdForSpecificLog == null)) {
      return '';
    }
    if (encIdForSpecificLog == null) {
      return '(storedTid: no)';
    }

    const tid: number = UtilService.decodeNumberFromRadix64(encIdForSpecificLog);
    if (loggingTickserId != tid) {
      return util.format('(storedTid: %d)', tid);
    }

    return '';
  }
}


