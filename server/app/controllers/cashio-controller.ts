/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import {CashIO, Currency} from '../models/models';
import {Body, JsonController, Get, Req, Post, UseBefore, BodyParam} from 'routing-controllers/index';
import * as CashIOMapper from '../mappers/cashio-mapper';
import * as CashIOService from '../services/cashio-service';
import * as InfinService from '../services/infin-service';
import * as PayPalService from '../services/paypal-service';
import BaseController from './base-controller';
import {ServerError} from '../errors';
import AuthMiddleware from '../middlewares/auth-middleware';

interface StartInfinBody {
  card: number;
  countryCode: string;
  moneyAmount: string;
}

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class CashIoController extends BaseController {
  constructor() {
    super(__filename);
  }

  @Get('/cashio/:id')
  async getById(@Req() req: Ticksa.Request) {
    req.checkParams('id', 'Invalid param').notEmpty();
    req.assertValid();

    const cashIO: CashIO.Instance = await CashIOService.findCashIoByIdAndAccount(req.params.id, req);
    return CashIOMapper.map(cashIO);
  }

  @Post('/cashio/cashin/infin')
  async startInfinTransaction(@Req() req: Ticksa.Request, @Body() body: StartInfinBody) {
    req.checkBody('countryCode', 'Invalid param').notEmpty();
    req.checkBody('moneyAmount', 'Invalid param').notEmpty();
    req.checkBody('moneyAmount', 'Invalid param').isNumeric();
    req.assertValid();

    const moneyAmount = parseFloat(body.moneyAmount);
    const createdCashIOId: number = await CashIOService.startInfinTransaction(moneyAmount, body.countryCode, req.local.user);
    const data = await InfinService.initializeInfinCashIn(createdCashIOId);

    return data;
  }

  @Post('/cashio/cashin/paypal')
  async startPaypal(@Req() req: Ticksa.Request, @BodyParam('moneyAmount') moneyAmount: number) {
    req.checkBody('moneyAmount', 'Invalid param').notEmpty();
    req.checkBody('moneyAmount', 'Invalid param').isNumeric();
    req.assertValid();

    const euroCurrency: Currency.Instance = await Currency.Model.scope('EUR').findOne();
    if (!euroCurrency) {
      throw new ServerError('Currency EUR not found');
    }

    const createdCashIOId: number = await CashIOService.startPayPalTransaction(moneyAmount, euroCurrency.id, req.local.user);
    const data = await PayPalService.initializePayPalCashIn(createdCashIOId);

    return data;
  }

  @Post('/cashio/transfer/earned-purchased')
  async earnedPurchased(@Req() req: Ticksa.Request) {
    req.checkBody('tickAmount', 'Invalid param').notEmpty();
    req.assertValid();

    const cashOut: CashIO.Instance = await CashIOService.earnedToPurchasedTicksCashOut(req.body.tickAmount, req.local.user);
    const cashIn: CashIO.Instance = await CashIOService.earnedToPurchasedTicksCashIn(cashOut.id);

    return CashIOMapper.map(cashIn);
  }

  @Post('/cashio/cashin/lemonway')
  async startLemonwayTransaction(@Req() req: Ticksa.Request) {
    req.checkBody('moneyAmount', 'Invalid param').notEmpty();
    req.assertValid();

    const card = req.body.card;

    if (card != null) {
      const cardParams: CashIOService.LemonwayTransactionCardParams = {
        cardId: card,
        moneyAmount: req.body.moneyAmount,
      };
      const transResult: CashIOService.LwTransactionResult = await CashIOService.startLemonwayTransactionWithCardId(cardParams, req);
      return transResult;
    }

    const params: CashIOService.StartLwTransactionParams = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      moneyAmount: req.body.moneyAmount,
    };
    const transResult: CashIOService.LwTransactionResult = await CashIOService.startLemonwayTransaction(params, req);
    return transResult;
  }
}
