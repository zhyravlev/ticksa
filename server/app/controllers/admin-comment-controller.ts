/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import * as AdminCommentsService from '../services/admin-comments';

import {Tickser} from '../models/models';
import {BadRequest} from '../errors';
import BaseController from './base-controller';
import {JsonController, HttpCode, Req, Post, UseBefore} from 'routing-controllers/index';
import AuthMiddleware from '../middlewares/auth-middleware';

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class AdminCommentController extends BaseController {

  constructor() {
    super(__filename);
  }

  @HttpCode(200)
  @Post('/admin/contents/:contentId/flag-as-inappropriate')
  async flagAsInappropriate(@Req() req: Ticksa.Request) {
    if (req.local.user.status != Tickser.Status.ACTIVE) {
      throw new BadRequest('Only active users can report content')
    }

    await AdminCommentsService.createComplaint(req.local.user, req.params.contentId, req.body.text);
    return this.ok();
  }

  @HttpCode(200)
  @Post('/admin/feedback')
  async postFeedback(@Req() req: Ticksa.Request) {
    req.checkBody('type', 'Invalid params').notEmpty();
    req.assertValid();

    await AdminCommentsService.createFeedback(req.local.user, req.body.type, req.body.text);
    return this.ok();
  }

}