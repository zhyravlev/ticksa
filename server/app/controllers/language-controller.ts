/// <reference path="../typings/tsd.d.ts"/>
import _ = require('lodash');
import Sequelize = require('sequelize');
import Ticksa = require('ticksa');
import {JsonController, Req, Get} from 'routing-controllers';

import LanguageMapper = require('../mappers/language-mapper');

import Models = require('../models/models');
import Language = Models.Language;
import BaseController from './base-controller';


@JsonController('/api')
export default class LanguageController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/languages/search')
  async languageSearch(@Req() req: Ticksa.Request): Promise<LanguageMapper.ILanguage[]> {
    req.checkQuery('q', 'Invalid param').notEmpty();
    req.assertValid();

    var query: Sequelize.FindOptions = {
      where: {name: {$iLike: req.query.q + '%'}},
      limit: 10
    };

    const languages: Language.Instance[] = await Language.Model.scope(Language.Scopes.ORDER_POSITION).findAll(query);
    const iLanguages: LanguageMapper.ILanguage[] = _.map(languages, lang => LanguageMapper.map(lang.id, lang));
    return iLanguages;
  }

  @Get('/languages')
  async getLanguages(@Req() req: Ticksa.Request): Promise<LanguageMapper.ILanguage[]> {
    const languages: Language.Instance[] = await Language.Model.scope(Language.Scopes.ORDER_POSITION).findAll();
    const iLanguages: LanguageMapper.ILanguage[] = _.map(languages, lang => LanguageMapper.map(lang.id, lang));
    return iLanguages;
  }

}