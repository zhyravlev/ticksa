/// <reference path="../typings/tsd.d.ts"/>

import * as _ from 'lodash';
import * as Sequelize from 'sequelize';
import * as Ticksa from 'ticksa';
import {JsonController, Req, Get} from 'routing-controllers';
import * as CategoryMapper from '../mappers/category-mapper';
import BaseController from './base-controller';
import {Category} from '../models/models';

@JsonController('/api')
export default class CategoryController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/categories/search')
  async searchCategories(@Req() req: Ticksa.Request): Promise<CategoryMapper.ICategory[]> {
    req.checkQuery('q', 'Invalid param').notEmpty();
    req.assertValid();

    var query: Sequelize.FindOptions = {
      where: {name: {$iLike: req.query.q + '%'}},
      limit: 10
    };

    const categories: Category.Instance[] = await Category.Model.scope(Category.Scopes.ORDER_POSITION).findAll(query);
    const iCategories: CategoryMapper.ICategory[] = _.map(categories, cat => CategoryMapper.map(cat.id, cat));
    return iCategories;
  }

  @Get('/categories')
  async getCategories(): Promise<CategoryMapper.ICategory[]> {
    const categories: Category.Instance[] = await Category.Model.scope(Category.Scopes.ORDER_POSITION).findAll();
    const iCategories: CategoryMapper.ICategory[] = _.map(categories, cat => CategoryMapper.map(cat.id, cat));
    return iCategories;
  }

}