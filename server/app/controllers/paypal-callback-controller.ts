/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import * as Errors from '../errors';
import * as CashIOService from '../services/cashio-service';
import * as PayPalService from '../services/paypal-service';
import * as LocationUtils from '../utils/ticksa-location-utils';

import BaseController from './base-controller';
import {JsonController, Get, Res, Req} from 'routing-controllers';

@JsonController('/api')
export default class PaypalCallbackController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/paypal/complete/:paypalCashInId')
  async handleCompletePaypalCallback(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response) {
    req.checkParams('paypalCashInId', 'Invalid param').notEmpty();
    req.checkQuery('paymentId', 'Invalid param').notEmpty();
    req.checkQuery('token', 'Invalid param').notEmpty();
    req.checkQuery('PayerID', 'Invalid param').notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      this.logger.warn('handleCompletePaypalCallback: ', errors);
      throw new Errors.BadRequest();
    }

    const data = {
      id: req.params.paypalCashInId,
      paymentId: req.query.paymentId,
      token: req.query.token,
      payerId: req.query.PayerID
    };

    try {
      const cashIOId: number = await PayPalService.completePayPalCashIn(data);
      await CashIOService.finalizeCashIO(cashIOId);
      res.redirect(this.config.env().server.url + LocationUtils.tl('/financial/payment-callback?transactionId=' + cashIOId));
    } catch (err) {
      this.logger.error(err);
      res.redirect(this.config.env().server.url + LocationUtils.tl('/financial/payment-callback?error=true'));
    }
  }

  @Get('/paypal/cancel/:paypalCashInId')
  async handleCancelPaypalCallback(@Req() req: Ticksa.Request, @Res() res: Ticksa.Response) {

    // it seems like only 'token' field will be sent from PayPal when user cancel payment
    req.checkParams('paypalCashInId', 'Invalid param').notEmpty();
    req.checkQuery('token', 'Invalid param').notEmpty();

    const errors = req.validationErrors();
    if (errors) {
      this.logger.warn('handleCancelPaypalCallback: ', errors);
      throw new Errors.BadRequest();
    }

    const data = {
      id: req.params.paypalCashInId,
      paymentId: req.query.paymentId,
      token: req.query.token,
      payerId: req.query.PayerID
    };

    try {
      await PayPalService.cancelPayPalCashIn(data);
      res.redirect(this.config.env().server.url + LocationUtils.tl('/financial/payment-callback?cancel=true'));
    } catch (err) {
      res.redirect(this.config.env().server.url + LocationUtils.tl('/financial/payment-callback?error=true'));
    }

  }

}