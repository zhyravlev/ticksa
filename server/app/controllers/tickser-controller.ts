/// <reference path="../typings/tsd.d.ts"/>

import {UseBefore} from 'routing-controllers/index';
import {JsonController, QueryParam, Req, Get} from "routing-controllers";
import * as Ticksa from 'ticksa';
import * as Errors from '../errors';
import AuthMiddleware from '../middlewares/auth-middleware';
import BaseController from './base-controller';
import {Tickser} from  '../models/models';

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class TickserController extends BaseController {
  constructor() {
    super(__filename);
  }

  @Get('/ticksers/q')
  async search(@Req() req: Ticksa.Request, @QueryParam('email') email: string) {
    req.checkQuery('email', 'Invalid param').notEmpty();
    req.assertValid();

    const tickser: Tickser.Instance = await Tickser.Model.scope([Tickser.Scopes.IS_ACTIVE_OR_CONFIRMED]).findOneByEmail(email);

    if (tickser == null) {
      throw new Errors.NotFound('Tickser not found');
    }

    return tickser.toJSON();
  }
}