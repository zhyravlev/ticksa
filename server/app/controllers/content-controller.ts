/// <reference path="../typings/tsd.d.ts"/>

import * as moment from 'moment';
import Moment = moment.Moment;
import Duration = moment.Duration;

import * as _ from 'lodash';
import * as util from 'util';
import * as Sequelize from 'sequelize';
import * as Ticksa from 'ticksa';
import * as Errors from '../errors';
import * as conn from '../connection';
import * as Utilities from '../services/utilities';
import * as TicksTransactionsService from '../services/ticks-transaction-service';
import * as ContentMessages from '../dto/content/content-messages';
import {
  Tickser,
  TicksTransaction,
  TickserChannel,
  Channel,
  ContentImage,
  ContentLink,
  ContentVideo,
  ContentText,
  ContentTag,
  History,
  Content,
  EmailRecipient
} from '../models/models';
import * as ContentService from '../services/content-service';
import * as ContentAccessService from '../services/content-access-service';
import * as LoggedEventService from '../services/logged-event-service';
import * as MailService from '../services/mail-service';
import * as ConditionService from '../services/condition-service';
import * as RequestUtils from '../utils/request-utils';
import {ContentFilter} from '../utils/request-utils';
import BaseController from './base-controller';
import {JsonController, Post, UseBefore, Req, Put, Get, Delete} from 'routing-controllers/index';
import AuthMiddleware from '../middlewares/auth-middleware';

@JsonController('/api')
@UseBefore(AuthMiddleware)
export default class ContentController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Post('/user/channels/:channelId/contents')
  async postContent(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.assertValid();

    await assertChannelCreatorOrManageOrParticipant(req);

    const channelId = req.params.channelId;
    const tickserId = req.local.user.id;
    const contentRequest = ContentMessages.ContentPostRequest.parseFromJSON(req.body);

    const areValidGeographicCountries = await ContentService.areValidGeographicCountries(contentRequest.geographicCountries);

    if (!areValidGeographicCountries) {
      this.logger.error('Invalid geographic fencing: ' + util.inspect(contentRequest.geographicCountries));
      throw new Errors.BadRequest('Invalid geographic fencing countries list');
    }

    const content = Content.Model.build(<Content.Attributes>{
      channelId: channelId,
      tickserId: tickserId,
      likes: 0,
      dislikes: 0,
      title: contentRequest.title,
      type: contentRequest.type,
      description: contentRequest.description,
      teaserText: contentRequest.teaserText,
      teaserImageId: contentRequest.teaserImageId,
      teaserVideoId: contentRequest.teaserVideoId,
      status: Content.Status.ACTIVE,
      tickPrice: contentRequest.tickPrice,
      expiryDate: Utilities.dateAdd(new Date(), 'second', this.config.global.contentExpiryTimeInSeconds),
      published: contentRequest.published,
      campaign: contentRequest.campaign,
      campaignPercent: contentRequest.campaignPercent,
      categoryId: contentRequest.categoryId,
      languageId: contentRequest.languageId,
      commentCount: 0,
      randomPosition: Content.randomNumberToShuffle(),
      geographicCountries: contentRequest.geographicCountries,
    });

    let contentAssociation: ContentText.Instance | ContentVideo.Instance | ContentLink.Instance;
    let contentImages: ContentImage.Instance[];

    switch (content.type) {
      case Content.Type.IMAGE:
        content.imageCount = contentRequest.contentImages.length;
        contentImages = _.map(contentRequest.contentImages,
          (i: ContentMessages.ContentImagePostDto) => ContentImage.Model.build({
            imageId: i.imageId,
            position: i.position
          })
        );
        break;

      case Content.Type.TEXT:
        content.textLength = contentRequest.contentText.text.length;
        contentAssociation = ContentText.Model.build({text: contentRequest.contentText.text});
        break;

      case Content.Type.VIDEO:
        content.videoLength = contentRequest.contentVideo.videoLength;
        contentAssociation = ContentVideo.Model.build({videoId: contentRequest.contentVideo.videoId});
        break;

      case Content.Type.LINK:
        contentAssociation = ContentLink.Model.build({url: contentRequest.contentLink.url});
        break;
    }

    const newCreatedContent: Content.Instance = await conn.transaction(async(t: Sequelize.Transaction) => {
      const createdContent = <Content.Instance>(await content.save({transaction: t}));

      const promises: Promise<any>[] = [];

      if (contentAssociation != null) {
        contentAssociation.contentId = createdContent.id;
        promises.push(contentAssociation.save({transaction: t}));
      }

      _.forEach(contentImages, (image: ContentImage.Instance) => {
        image.contentId = createdContent.id;
        promises.push(image.save({transaction: t}));
      });

      _.forEach(contentRequest.tags, (tag: {id: number}) => {
        promises.push(ContentTag.Model.create({
          tagId: tag.id,
          contentId: createdContent.id
        }, {
          transaction: t
        }));
      });

      const updateConditions = ConditionService.createContentConditions(createdContent, contentRequest.conditions, t);
      promises.push(updateConditions);

      const payForCreation = TicksTransactionsService.processOneWayTransaction(
        tickserId, createdContent.id, null, this.config.global.contentPostPrice, TicksTransaction.Type.CREATE, t);
      promises.push(payForCreation);

      await Promise.all(promises);
      return createdContent;
    });

    return {
      contentId: newCreatedContent.id,
      tickserId: newCreatedContent.tickserId
    };
  }

  @Put('/user/channels/:channelId/contents/:contentId')
  async updateContent(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    await assertChannelCreatorOrManageOrParticipant(req);

    const contentId = req.params.contentId;
    const contentRequest = ContentMessages.ContentPostRequest.parseFromJSON(req.body);

    const areValidGeographicCountries = ContentService.areValidGeographicCountries(contentRequest.geographicCountries);

    if (!areValidGeographicCountries) {
      this.logger.error('Invalid geographic fencing: ' + util.inspect(contentRequest.geographicCountries));
      throw new Errors.BadRequest('Invalid geographic fencing countries list');
    }

    await conn.transaction(async function (t: Sequelize.Transaction): Promise<void> {
      const scopes = [
        Content.Scopes.INCLUDE_CONTENT_DATA_IMAGES,
        Content.Scopes.INCLUDE_CONTENT_DATA_TEXT,
        Content.Scopes.INCLUDE_CONTENT_DATA_LINK,
        Content.Scopes.INCLUDE_CONTENT_DATA_VIDEO,
        Content.Scopes.INCLUDE_CONDITIONS
      ];

      const content: Content.Instance = await Content.Model
        .scope(scopes)
        .findById(contentId, {transaction: t});

      if (content == null) {
        this.logger.error('Attempt to update nonexistent content: ' + req.url);
        throw new Errors.NotFound('Content not found');
      }

      const promises: Promise<any>[] = [];

      if (content.channelId != contentRequest.channelId) {
        promises.push(
          LoggedEventService.createEventAboutChannelChange(content.id, content.channelId, contentRequest.channelId, t)
        );
      }

      content.title = contentRequest.title;
      content.description = contentRequest.description;
      content.teaserText = contentRequest.teaserText;
      content.teaserImageId = contentRequest.teaserImageId;
      content.teaserVideoId = contentRequest.teaserVideoId;
      content.tickPrice = contentRequest.tickPrice;
      content.published = contentRequest.published;
      content.campaign = contentRequest.campaign;
      content.campaignPercent = contentRequest.campaignPercent;
      content.channelId = contentRequest.channelId;
      content.categoryId = contentRequest.categoryId;
      content.languageId = contentRequest.languageId;
      content.geographicCountries = contentRequest.geographicCountries;

      switch (content.type) {

        case Content.Type.IMAGE:
          content.imageCount = contentRequest.contentImages.length;
          await ContentImage.Model.destroy({
            where: {
              contentId: content.id
            },
            transaction: t
          });
          _.forEach(contentRequest.contentImages, (contentImage: ContentMessages.ContentImagePostDto) => {
            promises.push(
              ContentImage.Model.create({
                contentId: content.id,
                imageId: contentImage.imageId,
                position: contentImage.position
              }, {transaction: t})
            );
          });
          break;

        case Content.Type.TEXT:
          content.textLength = contentRequest.contentText.text.length;
          content.contentText.text = contentRequest.contentText.text;
          promises.push(content.contentText.save({transaction: t}));
          break;

        case Content.Type.VIDEO:
          content.videoLength = contentRequest.contentVideo.videoLength;
          content.contentVideo.videoId = contentRequest.contentVideo.videoId;
          promises.push(content.contentVideo.save({transaction: t}));
          break;

        case Content.Type.LINK:
          content.contentLink.url = contentRequest.contentLink.url;
          promises.push(content.contentLink.save({transaction: t}));
          break;
      }

      if (!_.isEmpty(content.tags)) {
        await ContentTag.Model.destroy({
          where: {
            contentId: content.id
          },
          transaction: t
        });
      }
      _.forEach(contentRequest.tags, (tag: {id: number}) => {
        promises.push(ContentTag.Model.create({
            tagId: tag.id,
            contentId: content.id
          }, {transaction: t})
        );
      });

      promises.push(ConditionService.updateContentConditions(content, contentRequest.conditions, t));
      promises.push(content.save({transaction: t}));

      await Promise.all(promises);
    });

    return this.ok();
  }

  @Post('/user/channels/:channelId/contents/:contentId/publish')
  async publishContent(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    await assertChannelCreatorOrManageOrParticipant(req);
    await Content.Model.update(<Content.Attributes>{
      published: true
    }, {
      where: {
        id: req.params.contentId,
        channelId: req.params.channelId
      }
    });

    return this.ok();
  }

  @Post('/user/channels/:channelId/contents/:contentId/unpublish')
  async unpublishContent(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    await assertChannelCreatorOrManageOrParticipant(req);

    await Content.Model.update(<Content.Attributes>{
      published: false
    }, {
      where: {
        id: req.params.contentId,
        channelId: req.params.channelId,
        status: Content.Status.ACTIVE
      }
    });

    return this.ok();
  }

  @Get('/user/channels/:channelId/contents')
  async getContentsByChannelIdForUser(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.assertValid();

    const channelId = req.params.channelId;
    const user = req.local.user;
    const pageable = RequestUtils.getPageable(req);
    const scopes: any = [
      Content.Scopes.IS_ACTIVE,
      Content.Scopes.INCLUDE_TAGS,
      Content.Scopes.INCLUDE_TEASER_IMAGE,
      Content.Scopes.INCLUDE_TEASER_VIDEO,
      Content.Scopes.INCLUDE_CONDITIONS,
      {method: [Content.Scopes.INCLUDE_REQUIRED_ACTIVE_CHANNEL_FOR_TICKSER, channelId, user.id]}
    ];

    const contents: Content.Instance[] = await Content.Model.scope(scopes).findAll({
      order: [
        ['created_at', 'DESC']
      ],
      limit: pageable.limit,
      offset: pageable.offset
    });

    if (!contents || contents.length == 0) {
      return [];
    }

    const sortedContent = contents[0].channel.sortedContent;
    const sortContents = ContentService.sortContentBySortedIds(contents, sortedContent);

    return _.map(sortContents, content => ContentMessages.ContentTeaserResponse.parseFromDB(content, true, true));
  }

  @Get('/contents')
  async getContents(@Req() req: Ticksa.Request) {
    const pageable: Ticksa.IPageable<ContentFilter> = RequestUtils.getPageableContentFilter(req);
    const user = req.local.user;
    const userMeta: ContentService.UserMeta = {countryA2Code: req.local.geoip.countryA2Code, tickserId: user.id};

    const getContents = ContentService.getAllContentsPagingAndSearch(pageable, userMeta);
    const findTickserChannels = TickserChannel.Model.findAllByTickserId(user.id);
    const updateTickserFilter = updateTickserFilters(user, pageable);

    const [contents, tickserChannels] = await Promise.all([getContents, findTickserChannels, updateTickserFilter]);

    if (!contents || contents.length == 0) {
      return Promise.resolve([]);
    }

    const permissionMap: ContentAccessService.PermissionMap = await ContentAccessService.getContentPermissionMap(user.account.id, contents, tickserChannels);
    const contentTeasers: ContentMessages.ContentTeaserResponse[] = _.map(contents, function (content: any) {
      const permission = permissionMap[content.id];
      return ContentMessages.ContentTeaserResponse.parseFromDB(content, permission.isManager, permission.hasAccess);
    });

    return contentTeasers;
  }

  @Get('/contents/search')
  async getContentsSearch(@Req() req: Ticksa.Request) {
    req.checkQuery('q', 'Invalid param').notEmpty();
    req.assertValid();

    const searchQuery = req.query.q;
    const userId = req.local.user.id;
    const userMeta: ContentService.UserMeta = {countryA2Code: req.local.geoip.countryA2Code, tickserId: userId};

    const contents = await ContentService.searchContent(searchQuery, userMeta);
    return _.map(contents, content => {
      return ContentMessages.ContentSearchResponse.parseToJson(content);
    });
  }

  @Get('/channels/:channelId/contents')
  async getContentsForChannel(@Req() req: Ticksa.Request) {
    const accountId = req.local.user.account.id;
    const userId = req.local.user.id;
    const channelId = req.params.channelId;
    const pageable = RequestUtils.getPageable(req);
    const userMeta: ContentService.UserMeta = {countryA2Code: req.local.geoip.countryA2Code, tickserId: userId};

    const [contents, tickserChannels] = await Promise.all([
      ContentService.findContentsByChannel(channelId, pageable, userMeta),
      TickserChannel.Model.findAllByTickserId(userId)
    ]);

    const permissionMap = await ContentAccessService.getContentPermissionMap(accountId, contents, tickserChannels);

    return _.map(contents, function (content: Content.Instance) {
      const permission = permissionMap[content.id];
      return ContentMessages.ContentTeaserResponse.parseFromDB(content, permission.isManager, permission.hasAccess);
    });
  }

  @Get('/channels/:channelId/contents/:contentId/teaser')
  async getContentTeaser(@Req() req: Ticksa.Request) {
    const contentId = req.params.contentId;
    const channelId = req.params.channelId;
    const user = req.local.user;

    const findContent = ContentService.findFullContentByIdAndChannelId(contentId, channelId);
    const findTickserChannels = TickserChannel.Model.findAllByTickserIdAndChannelId(user.id, channelId);
    const [content, tickserChannels] = await Promise.all([findContent, findTickserChannels]);

    if (content == null) {
      throw new Errors.NotFound('Content not found');
    }

    const permissionMap = await ContentAccessService.getContentPermissionMap(user.account.id, [content], tickserChannels);
    const permission = permissionMap[content.id];

    if (!permission.isManager && !permission.hasAccess) {
      if (!content.isValidCountry(req.local.geoip.countryA2Code)) {
        throw Errors.ContentForbidden.geographyLimited('Content not available in your country');
      }
      const isSatisfies = await ConditionService.doesUserSatisfyCondition(content.id, user.id);
      if (!isSatisfies) {
        throw Errors.ContentForbidden.notAvailable('Content not available');
      }
    }

    const teaserResponse = await ContentMessages.ContentTeaserResponse.parseFromDB(content, permission.isManager, permission.hasAccess);
    return teaserResponse;
  }

  @Post('/contents/:contentId/extend')
  async extendContent(@Req() req: Ticksa.Request) {
    const content = await conn.transaction(async(t) => {
      const content: Content.Instance = await Content.Model.findOne({where: {id: req.params.contentId}, transaction: t});
      content.expiryDate = Utilities.dateAdd(content.expiryDate, 'second', this.config.global.contentExpiryExtensionTimeInSeconds);

      const processTransaction = TicksTransactionsService.processOneWayTransaction(req.local.user.id, content.id, null,
        this.config.global.extendContentPrice, TicksTransaction.Type.EXTEND_CONTENT, t);
      const saveContent = content.save({transaction: t});

      const [updatedContent, transactionResult] = await Promise.all([saveContent, processTransaction]);

      return updatedContent;
    });

    return {expiresAt: content.expiryDate};
  }

  @Post('/contents/:contentId/pay')
  async payForContent(@Req() req: Ticksa.Request) {
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    const user = req.local.user;
    const contentId = parseInt(req.params.contentId);

    await ContentService.payContent(user.id, user.account.id, contentId, req.local.geoip);
    return this.ok();
  }

  @Post('/contents/:contentId/like')
  async likeContent(@Req() req: Ticksa.Request) {
    let likes = 1;
    if (!isNaN(req.body.likes)) {
      likes = parseInt(req.body.likes);
    }

    const result: TicksTransactionsService.TwoWayTransactionResult = await conn.transaction(async(t) => {
      const content = await Content.Model.findOne({
        where: {
          id: req.params.contentId
        },
        transaction: t
      });


      content.likes += likes;
      this.logger.info('content likes', content.likes);

      let params: TicksTransactionsService.TwoWayTransactionParams = {
        buyerTickserId: req.local.user.id,
        sellerTickserId: content.tickserId,
        contentId: content.id,
        amount: likes * 1.0,
        type: TicksTransaction.Type.LIKE,
        t: t
      };

      const result: TicksTransactionsService.TwoWayTransactionResult = await TicksTransactionsService.processTwoWayTransaction(params);
      await content.save({transaction: t});
      return result;
    });

    return {ticks: result.buyerAccount.currentBalance};
  }

  @Post('/contents/:contentId/dislike')
  async dislikeContent(@Req() req: Ticksa.Request) {
    let dislikes = 1;
    if (!isNaN(req.body.dislikes)) {
      dislikes = parseInt(req.body.dislikes);
    }

    const result: TicksTransactionsService.TwoWayTransactionResult = await conn.transaction(async(t) => {
      const content = await Content.Model.findOne({
        where: {
          id: req.params.contentId
        },
        transaction: t
      });

      content.dislikes += dislikes;

      let params: TicksTransactionsService.TwoWayTransactionParams = {
        buyerTickserId: req.local.user.id,
        sellerTickserId: content.tickserId,
        contentId: content.id,
        amount: dislikes * 1.0,
        type: TicksTransaction.Type.DISLIKE,
        t: t
      };

      const result: TicksTransactionsService.TwoWayTransactionResult = await TicksTransactionsService.processTwoWayTransaction(params);
      await content.save({transaction: t});
      return result;
    });

    return {ticks: result.buyerAccount.currentBalance};
  }

  @Post('/contents/:contentId/refund')
  async refundContent(@Req() req: Ticksa.Request) {
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    let contentId = parseInt(req.params.contentId);
    let user = req.local.user;

    await ContentService.refundContent(user.id, user.account.id, contentId);
    return this.ok();
  }

  @Post('/contents/:contentId/send-content-card-to-me')
  async sendContentCardToMe(@Req() req: Ticksa.Request) {
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    const queryLanguage: string = req.local.language;
    const emailLanguage: EmailRecipient.PossibleLanguage = EmailRecipient.LANGUAGE_SELECTOR.select(queryLanguage);

    const result: ContentService.RenderContentCardResult = await ContentService.renderContentCard(req.params.contentId, queryLanguage);
    const user: Tickser.Instance = req.local.user;

    let params = {
      user_name: user.name,
      content_title: result.content.title,
      content_link: ContentService.generateContentShortLink(result.content.id, result.content.tickserId),
      content_card: result.renderedCard,
    };

    await MailService.sendMailWithLanguageToTickser(user, EmailRecipient.Template.CONTENT_CARD, emailLanguage, params);
    return this.ok();
  }

  @Get('/contents/:contentId/html-card')
  async getContentHtmlCard(@Req() req: Ticksa.Request) {
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    const result: ContentService.RenderContentCardResult = await ContentService.renderContentCard(req.params.contentId, req.local.language);
    return {
      renderedCard: result.renderedCard
    }
  }

  @Get('/channels/:channelId/contents/:contentId')
  async getFullContent(@Req() req: Ticksa.Request) {
    const user = req.local.user;
    const channelId = req.params.channelId;
    const contentId = req.params.contentId;
    const accountId = user.account.id;

    const findContent = ContentService.findFullContentById(contentId);
    const findTickserChannels = TickserChannel.Model.findAllByTickserIdAndChannelId(user.id, channelId);

    const [content, tickserChannels] = await Promise.all([findContent, findTickserChannels]);


    if (content == null) {
      throw new Errors.NotFound('No content found');
    }

    const permissionMap: ContentAccessService.PermissionMap = await ContentAccessService.getContentPermissionMap(user.account.id, [content], tickserChannels);

    const hasAccess = permissionMap[contentId].hasAccess;
    const isManager = permissionMap[contentId].isManager;
    if (!hasAccess && !isManager) {
      throw new Errors.Validation('Access Denied');
    }

    await History.Model.destroy({
      where: {
        tickserId: user.id,
        contentId: content.id
      }
    });
    await History.Model.create({
      tickserId: user.id,
      contentId: content.id
    });

    if (isManager) {
      const result: ContentMessages.IContentFullResponse = await ContentMessages.ContentFullResponse.parseFromDB(content, isManager);
      return result;
    }

    const now: Moment = moment();
    const latestTransactions: TicksTransaction.Instance[] = await TicksTransactionsService.getLatestAccessTransactionsForTheLast30Seconds(contentId, accountId, now);

    let secondsFromBuy: number = ContentMessages.ContentFullResponse.TOO_MUCH_TIME_PASSED;
    if (latestTransactions && latestTransactions.length > 0) {
      const dateOfTransaction: Moment = moment(latestTransactions[0].createdAt);
      const duration: Duration = moment.duration(now.diff(dateOfTransaction));
      secondsFromBuy = duration.asSeconds();
    }

    const result: ContentMessages.IContentFullResponse = await ContentMessages.ContentFullResponse.parseFromDB(content, isManager, secondsFromBuy);
    return result;
  }

  @Delete('/channels/:channelId/contents/:contentId')
  async deactivateContent(@Req() req: Ticksa.Request) {
    req.checkParams('channelId', 'Invalid param').notEmpty();
    req.checkParams('contentId', 'Invalid param').notEmpty();
    req.assertValid();

    await assertChannelCreatorOrManageOrParticipant(req);

    await Content.Model.update(<Content.Attributes>{
      status: Content.Status.DEACTIVATED
    }, {
      where: {
        id: req.params.contentId,
        channelId: req.params.channelId
      }
    });
    return this.ok();
  }

  @Get('/history')
  async getContentHistory(@Req() req: Ticksa.Request) {
    const user = req.local.user;
    const findHistory: Promise<History.Instance[]> = History.Model.findAll({
      where: {
        tickserId: user.id
      },
      order: [
        ['created_at', 'DESC']
      ],
      include: [
        {
          required: true,
          model: Content.Model.scope([
            Content.Scopes.IS_ACTIVE,
            Content.Scopes.INCLUDE_TEASER_IMAGE,
            Content.Scopes.INCLUDE_TEASER_VIDEO,
            Content.Scopes.INCLUDE_REQUIRED_ACTIVE_CHANNEL
          ]),
          as: 'content'
        }
      ]
    });
    const findTickserChannels: Promise<TickserChannel.Instance[]> = TickserChannel.Model.findAllByTickserId(user.id);

    const [tickserChannels, historyItems] = await Promise.all([findTickserChannels, findHistory]);

    const contents: Content.Instance[] = _.map(historyItems, (historyItem: History.Instance) => {
      return historyItem.content;
    });

    const permissionMap = await ContentAccessService.getContentPermissionMap(user.account.id, contents, tickserChannels);
    let availableHistory: ContentMessages.HistoryGetResponse[] = _.map(historyItems, (historyItem: History.Instance) => {
      let permission = permissionMap[historyItem.contentId];
      if (permission.isManager || permission.hasAccess) {
        return ContentMessages.HistoryGetResponse.parseFromDB(historyItem);
      }
    });

    return availableHistory;
  }
}

function updateTickserFilters(tickser: Tickser.Instance, pageable: Ticksa.IPageable<ContentFilter>): Promise<any> {
  let sortedLanguageFilter = tickser.languageFilter ? tickser.languageFilter.sort() : [];
  let sortedCategoryFilter = tickser.categoryFilter ? tickser.categoryFilter.sort() : [];

  let sortedPageableLanguageFilter = pageable.filter.languages;
  let sortedPageableCategoryFilter = pageable.filter.categories;

  let languagesIsChanged = !_.isEqual(sortedLanguageFilter, sortedPageableLanguageFilter);
  let categoriesIsChanged = !_.isEqual(sortedCategoryFilter, sortedPageableCategoryFilter);

  if (!languagesIsChanged && !categoriesIsChanged) {
    return Promise.resolve();
  }

  tickser.languageFilter = sortedPageableLanguageFilter;
  tickser.categoryFilter = sortedPageableCategoryFilter;
  return tickser.save();
}

async function assertChannelCreatorOrManageOrParticipant(req: Ticksa.Request) {
  const tickserChannel: TickserChannel.Instance = await TickserChannel.Model.findOne({
    where: {
      tickserId: req.local.user.id,
      channelId: req.params.channelId
    },
    include: [
      {
        required: true,
        model: Channel.Model,
        as: 'channel',
        attributes: ['id', 'status'],
        where: {
          status: Channel.Status.ACTIVE
        }
      }
    ]
  });

  const isCreatorOrManagerOrParticipant: boolean = tickserChannel != null &&
    (
      tickserChannel.ownerType == TickserChannel.OwnerType.CREATOR ||
      tickserChannel.ownerType == TickserChannel.OwnerType.MANAGER ||
      tickserChannel.ownerType == TickserChannel.OwnerType.PARTICIPANT
    );

  if (!isCreatorOrManagerOrParticipant) {
    throw new Errors.Forbidden('Only participant, manager or creator can have access');
  }
}