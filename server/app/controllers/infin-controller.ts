/// <reference path="../typings/tsd.d.ts"/>

import * as Ticksa from 'ticksa';
import {JsonController, Req, Post, Get} from 'routing-controllers';
import BaseController from './base-controller';
import * as InfinService from '../services/infin-service';
import * as CashIOService from '../services/cashio-service';

@JsonController('/api')
export default class InfinController extends BaseController {

  constructor() {
    super(__filename);
  }

  @Get('/infin/callback')
  @Post('/infin/callback')
  async callback(@Req() req: Ticksa.Request): Promise<any> {
    this.logger.info('Infin callback called');

    try {
      const cashIOId: number = await InfinService.handleInfinCallback(req);
      await CashIOService.finalizeCashIO(cashIOId);
    } catch (e) {
      this.logger.warn('Error during handling infin callback. Method: %s', req.method);
    }

    return this.ok();
  }

}


