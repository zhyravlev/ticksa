/// <reference path="../typings/tsd.d.ts"/>
import Models = require('../models/models')
import Account = Models.Account;
import GlobalConfig = Models.GlobalConfig;

var Accounts = [
  //general system account
  {
    id: Account.SystemAccount.GENERAL_ACCOUNT,
    totalTicksDebited: 0.0,
    totalTicksCredited: 0.0,
    totalFreeTicks: 0.0,
    currentBalance: 0.0,
    currentBalanceFreeTicks: 0.0,
    currentBalanceEarnedTicks: 0.0,
    balanceUpdated: new Date(),
    status: Account.Status.NORMAL,
    type: Account.AccountType.SYSTEM,
  },
  //free ticks system account
  {
    id: Account.SystemAccount.FREE_TICKS_ACCOUNT,
    totalTicksDebited: 0.0,
    totalTicksCredited: 0.0,
    totalFreeTicks: 0.0,
    currentBalance: 0.0,
    currentBalanceFreeTicks: 0.0,
    currentBalanceEarnedTicks: 0.0,
    balanceUpdated: new Date(),
    status: Account.Status.NORMAL,
    type: Account.AccountType.SYSTEM,
  },
  {
    id: Account.SystemAccount.COMMISSION_TICKS_ACCOUNT,
    totalTicksDebited: 0.0,
    totalTicksCredited: 0.0,
    totalFreeTicks: 0.0,
    currentBalance: 0.0,
    currentBalanceFreeTicks: 0.0,
    currentBalanceEarnedTicks: 0.0,
    balanceUpdated: new Date(),
    status: Account.Status.NORMAL,
    type: Account.AccountType.SYSTEM,
  },
  {
    id: Account.SystemAccount.VOUCHER_ACCOUNT,
    totalTicksDebited: 0.0,
    totalTicksCredited: 0.0,
    totalFreeTicks: 0.0,
    currentBalance: 0.0,
    currentBalanceFreeTicks: 0.0,
    currentBalanceEarnedTicks: 0.0,
    balanceUpdated: new Date(),
    status: Account.Status.NORMAL,
    type: Account.AccountType.SYSTEM,
  },
  {
    id: Account.SystemAccount.VAT_GLOBAL,
    totalTicksDebited: 0.0,
    totalTicksCredited: 0.0,
    totalFreeTicks: 0.0,
    currentBalance: 0.0,
    currentBalanceFreeTicks: 0.0,
    currentBalanceEarnedTicks: 0.0,
    balanceUpdated: new Date(),
    status: Account.Status.NORMAL,
    type: Account.AccountType.SYSTEM,
  },
];

export = Accounts;