/// <reference path="../typings/tsd.d.ts"/>

export = countries;

var countries = [
  {
    "name": "Afghanistan",
    "a2": "AF",
    "a3": "AFG",
    "number": "4",
    "dialingCode": "93",
    "currencyCode": "AFN",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Albania",
    "a2": "AL",
    "a3": "ALB",
    "number": "8",
    "dialingCode": "355",
    "currencyCode": "ALL",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Algeria",
    "a2": "DZ",
    "a3": "DZA",
    "number": "12",
    "dialingCode": "213",
    "currencyCode": "DZD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Andorra",
    "a2": "AD",
    "a3": "AND",
    "number": "20",
    "dialingCode": "376",
    "currencyCode": "EUR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Angola",
    "a2": "AO",
    "a3": "AGO",
    "number": "24",
    "dialingCode": "244",
    "currencyCode": "AOA",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Antigua and Barbuda",
    "a2": "AG",
    "a3": "ATG",
    "number": "28",
    "dialingCode": "1-268",
    "currencyCode": "XCD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Argentina",
    "a2": "AR",
    "a3": "ARG",
    "number": "32",
    "dialingCode": "54",
    "currencyCode": "ARS",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Armenia",
    "a2": "AM",
    "a3": "ARM",
    "number": "51",
    "dialingCode": "374",
    "currencyCode": "AMD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Australia",
    "a2": "AU",
    "a3": "AUS",
    "number": "36",
    "dialingCode": "61",
    "currencyCode": "AUD",
    "VAT": 0.0,
    "bookingCode": "4295005"
  },
  {
    "name": "Austria",
    "a2": "AT",
    "a3": "AUT",
    "number": "40",
    "dialingCode": "43",
    "currencyCode": "EUR",
    "VAT": 0.2,
    "bookingCode": "4286000"
  },
  {
    "name": "Azerbaijan",
    "a2": "AZ",
    "a3": "AZE",
    "number": "31",
    "dialingCode": "994",
    "currencyCode": "AZN",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Bahamas",
    "a2": "BS",
    "a3": "BHS",
    "number": "44",
    "dialingCode": "1-242",
    "currencyCode": "BSD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Bahrain",
    "a2": "BH",
    "a3": "BHR",
    "number": "48",
    "dialingCode": "973",
    "currencyCode": "BHD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Bangladesh",
    "a2": "BD",
    "a3": "BGD",
    "number": "50",
    "dialingCode": "880",
    "currencyCode": "BDT",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Barbados",
    "a2": "BB",
    "a3": "BRB",
    "number": "52",
    "dialingCode": "1-246",
    "currencyCode": "BBD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Belarus",
    "a2": "BY",
    "a3": "BLR",
    "number": "112",
    "dialingCode": "375",
    "currencyCode": "BYR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Belgium",
    "a2": "BE",
    "a3": "BEL",
    "number": "56",
    "dialingCode": "32",
    "currencyCode": "EUR",
    "VAT": 0.21,
    "bookingCode": "4286001"
  },
  {
    "name": "Belize",
    "a2": "BZ",
    "a3": "BLZ",
    "number": "84",
    "dialingCode": "501",
    "currencyCode": "BZD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Benin",
    "a2": "BJ",
    "a3": "BEN",
    "number": "204",
    "dialingCode": "229",
    "currencyCode": "XOF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Bhutan",
    "a2": "BT",
    "a3": "BTN",
    "number": "64",
    "dialingCode": "975",
    "currencyCode": "BTN",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Bolivia",
    "a2": "BO",
    "a3": "BOL",
    "number": "68",
    "dialingCode": "591",
    "currencyCode": "BOB",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Bosnia and Herzegovina",
    "a2": "BA",
    "a3": "BIH",
    "number": "70",
    "dialingCode": "387",
    "currencyCode": "BAM",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Botswana",
    "a2": "BW",
    "a3": "BWA",
    "number": "72",
    "dialingCode": "267",
    "currencyCode": "BWP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Brazil",
    "a2": "BR",
    "a3": "BRA",
    "number": "76",
    "dialingCode": "55",
    "currencyCode": "BRL",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Bulgaria",
    "a2": "BG",
    "a3": "BGR",
    "number": "100",
    "dialingCode": "359",
    "currencyCode": "BGN",
    "VAT": 0.2,
    "bookingCode": "4286002"
  },
  {
    "name": "Burkina Faso",
    "a2": "BF",
    "a3": "BFA",
    "number": "854",
    "dialingCode": "226",
    "currencyCode": "XOF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Burundi",
    "a2": "BI",
    "a3": "BDI",
    "number": "108",
    "dialingCode": "257",
    "currencyCode": "BIF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Cambodia",
    "a2": "KH",
    "a3": "KHM",
    "number": "116",
    "dialingCode": "855",
    "currencyCode": "KHR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Cameroon",
    "a2": "CM",
    "a3": "CMR",
    "number": "120",
    "dialingCode": "237",
    "currencyCode": "XAF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Canada",
    "a2": "CA",
    "a3": "CAN",
    "number": "124",
    "dialingCode": "1",
    "currencyCode": "CAD",
    "VAT": 0.0,
    "bookingCode": "4295007"
  },
  {
    "name": "Central African Republic",
    "a2": "CF",
    "a3": "CAF",
    "number": "140",
    "dialingCode": "236",
    "currencyCode": "XAF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Chad",
    "a2": "TD",
    "a3": "TCD",
    "number": "148",
    "dialingCode": "235",
    "currencyCode": "XAF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Chile",
    "a2": "CL",
    "a3": "CHL",
    "number": "152",
    "dialingCode": "56",
    "currencyCode": "CLP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "China",
    "a2": "CN",
    "a3": "CHN",
    "number": "156",
    "dialingCode": "86",
    "currencyCode": "CNY",
    "VAT": 0.0,
    "bookingCode": "4295009"
  },
  {
    "name": "Colombia",
    "a2": "CO",
    "a3": "COL",
    "number": "170",
    "dialingCode": "57",
    "currencyCode": "COP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Comoros",
    "a2": "KM",
    "a3": "COM",
    "number": "174",
    "dialingCode": "269",
    "currencyCode": "KMF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Costa Rica",
    "a2": "CR",
    "a3": "CRI",
    "number": "188",
    "dialingCode": "506",
    "currencyCode": "CRC",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Croatia",
    "a2": "HR",
    "a3": "HRV",
    "number": "191",
    "dialingCode": "385",
    "currencyCode": "HRK",
    "VAT": 0.25,
    "bookingCode": "4286011"
  },
  {
    "name": "Cuba",
    "a2": "CU",
    "a3": "CUB",
    "number": "192",
    "dialingCode": "53",
    "currencyCode": "CUP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Cyprus",
    "a2": "CY",
    "a3": "CYP",
    "number": "196",
    "dialingCode": "357",
    "currencyCode": "EUR",
    "VAT": 0.19,
    "bookingCode": "4286027"
  },
  {
    "name": "Czech Republic",
    "a2": "CZ",
    "a3": "CZE",
    "number": "203",
    "dialingCode": "420",
    "currencyCode": "CZK",
    "VAT": 0.21,
    "bookingCode": "4286024"
  },
  {
    "name": "Denmark",
    "a2": "DK",
    "a3": "DNK",
    "number": "208",
    "dialingCode": "45",
    "currencyCode": "DKK",
    "VAT": 0.25,
    "bookingCode": "4286003"
  },
  {
    "name": "Djibouti",
    "a2": "DJ",
    "a3": "DJI",
    "number": "262",
    "dialingCode": "253",
    "currencyCode": "DJF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Dominica",
    "a2": "DM",
    "a3": "DMA",
    "number": "212",
    "dialingCode": "1-767",
    "currencyCode": "XCD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Dominican Republic",
    "a2": "DO",
    "a3": "DOM",
    "number": "214",
    "dialingCode": "1-809,1-829,1-849",
    "currencyCode": "DOP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Ecuador",
    "a2": "EC",
    "a3": "ECU",
    "number": "218",
    "dialingCode": "593",
    "currencyCode": "USD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Egypt",
    "a2": "EG",
    "a3": "EGY",
    "number": "818",
    "dialingCode": "20",
    "currencyCode": "EGP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "El Salvador",
    "a2": "SV",
    "a3": "SLV",
    "number": "222",
    "dialingCode": "503",
    "currencyCode": "USD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Equatorial Guinea",
    "a2": "GQ",
    "a3": "GNQ",
    "number": "226",
    "dialingCode": "240",
    "currencyCode": "XAF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Eritrea",
    "a2": "ER",
    "a3": "ERI",
    "number": "232",
    "dialingCode": "291",
    "currencyCode": "ERN",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Estonia",
    "a2": "EE",
    "a3": "EST",
    "number": "233",
    "dialingCode": "372",
    "currencyCode": "EUR",
    "VAT": 0.2,
    "bookingCode": "4286005"
  },
  {
    "name": "Ethiopia",
    "a2": "ET",
    "a3": "ETH",
    "number": "231",
    "dialingCode": "251",
    "currencyCode": "ETB",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Fiji",
    "a2": "FJ",
    "a3": "FJI",
    "number": "242",
    "dialingCode": "679",
    "currencyCode": "FJD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Finland",
    "a2": "FI",
    "a3": "FIN",
    "number": "246",
    "dialingCode": "358",
    "currencyCode": "EUR",
    "VAT": 0.24,
    "bookingCode": "4286006"
  },
  {
    "name": "France",
    "a2": "FR",
    "a3": "FRA",
    "number": "250",
    "dialingCode": "33",
    "currencyCode": "EUR",
    "VAT": 0.2,
    "bookingCode": "4286007"
  },
  {
    "name": "Gabon",
    "a2": "GA",
    "a3": "GAB",
    "number": "266",
    "dialingCode": "241",
    "currencyCode": "XAF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Gambia",
    "a2": "GM",
    "a3": "GMB",
    "number": "270",
    "dialingCode": "220",
    "currencyCode": "GMD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Georgia",
    "a2": "GE",
    "a3": "GEO",
    "number": "268",
    "dialingCode": "995",
    "currencyCode": "GEL",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Germany",
    "a2": "DE",
    "a3": "DEU",
    "number": "276",
    "dialingCode": "49",
    "currencyCode": "EUR",
    "VAT": 0.19,
    "bookingCode": "4286004"
  },
  {
    "name": "Ghana",
    "a2": "GH",
    "a3": "GHA",
    "number": "288",
    "dialingCode": "233",
    "currencyCode": "GHS",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Greece",
    "a2": "GR",
    "a3": "GRC",
    "number": "300",
    "dialingCode": "30",
    "currencyCode": "EUR",
    "VAT": 0.23,
    "bookingCode": "4286008"
  },
  {
    "name": "Grenada",
    "a2": "GD",
    "a3": "GRD",
    "number": "308",
    "dialingCode": "1-473",
    "currencyCode": "XCD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Guatemala",
    "a2": "GT",
    "a3": "GTM",
    "number": "320",
    "dialingCode": "502",
    "currencyCode": "GTQ",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Guinea",
    "a2": "GN",
    "a3": "GIN",
    "number": "324",
    "dialingCode": "224",
    "currencyCode": "GNF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Guinea-Bissau",
    "a2": "GW",
    "a3": "GNB",
    "number": "624",
    "dialingCode": "245",
    "currencyCode": "XOF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Guyana",
    "a2": "GY",
    "a3": "GUY",
    "number": "328",
    "dialingCode": "592",
    "currencyCode": "GYD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Haiti",
    "a2": "HT",
    "a3": "HTI",
    "number": "332",
    "dialingCode": "509",
    "currencyCode": "HTG",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Honduras",
    "a2": "HN",
    "a3": "HND",
    "number": "340",
    "dialingCode": "504",
    "currencyCode": "HNL",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Hungary",
    "a2": "HU",
    "a3": "HUN",
    "number": "348",
    "dialingCode": "36",
    "currencyCode": "HUF",
    "VAT": 0.27,
    "bookingCode": "4286025"
  },
  {
    "name": "Iceland",
    "a2": "IS",
    "a3": "ISL",
    "number": "352",
    "dialingCode": "354",
    "currencyCode": "ISK",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "India",
    "a2": "IN",
    "a3": "IND",
    "number": "356",
    "dialingCode": "91",
    "currencyCode": "INR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Indonesia",
    "a2": "ID",
    "a3": "IDN",
    "number": "360",
    "dialingCode": "62",
    "currencyCode": "IDR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Iraq",
    "a2": "IQ",
    "a3": "IRQ",
    "number": "368",
    "dialingCode": "964",
    "currencyCode": "IQD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Ireland",
    "a2": "IE",
    "a3": "IRL",
    "number": "372",
    "dialingCode": "353",
    "currencyCode": "EUR",
    "VAT": 0.23,
    "bookingCode": "4286009"
  },
  {
    "name": "Israel",
    "a2": "IL",
    "a3": "ISR",
    "number": "376",
    "dialingCode": "972",
    "currencyCode": "ILS",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Italy",
    "a2": "IT",
    "a3": "ITA",
    "number": "380",
    "dialingCode": "39",
    "currencyCode": "EUR",
    "VAT": 0.22,
    "bookingCode": "4286010"
  },
  {
    "name": "Jamaica",
    "a2": "JM",
    "a3": "JAM",
    "number": "388",
    "dialingCode": "1-876",
    "currencyCode": "JMD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Japan",
    "a2": "JP",
    "a3": "JPN",
    "number": "392",
    "dialingCode": "81",
    "currencyCode": "JPY",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Jordan",
    "a2": "JO",
    "a3": "JOR",
    "number": "400",
    "dialingCode": "962",
    "currencyCode": "JOD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Kazakhstan",
    "a2": "KZ",
    "a3": "KAZ",
    "number": "398",
    "dialingCode": "7",
    "currencyCode": "KZT",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Kenya",
    "a2": "KE",
    "a3": "KEN",
    "number": "404",
    "dialingCode": "254",
    "currencyCode": "KES",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Kiribati",
    "a2": "KI",
    "a3": "KIR",
    "number": "296",
    "dialingCode": "686",
    "currencyCode": "AUD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Kuwait",
    "a2": "KW",
    "a3": "KWT",
    "number": "414",
    "dialingCode": "965",
    "currencyCode": "KWD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Kyrgyzstan",
    "a2": "KG",
    "a3": "KGZ",
    "number": "417",
    "dialingCode": "996",
    "currencyCode": "KGS",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Latvia",
    "a2": "LV",
    "a3": "LVA",
    "number": "428",
    "dialingCode": "371",
    "currencyCode": "EUR",
    "VAT": 0.21,
    "bookingCode": "4286012"
  },
  {
    "name": "Lebanon",
    "a2": "LB",
    "a3": "LBN",
    "number": "422",
    "dialingCode": "961",
    "currencyCode": "LBP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Lesotho",
    "a2": "LS",
    "a3": "LSO",
    "number": "426",
    "dialingCode": "266",
    "currencyCode": "LSL",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Liberia",
    "a2": "LR",
    "a3": "LBR",
    "number": "430",
    "dialingCode": "231",
    "currencyCode": "LRD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Libya",
    "a2": "LY",
    "a3": "LBY",
    "number": "434",
    "dialingCode": "218",
    "currencyCode": "LYD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Liechtenstein",
    "a2": "LI",
    "a3": "LIE",
    "number": "438",
    "dialingCode": "423",
    "currencyCode": "CHF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Lithuania",
    "a2": "LT",
    "a3": "LTU",
    "number": "440",
    "dialingCode": "370",
    "currencyCode": "EUR",
    "VAT": 0.21,
    "bookingCode": "4286013"
  },
  {
    "name": "Luxembourg",
    "a2": "LU",
    "a3": "LUX",
    "number": "442",
    "dialingCode": "352",
    "currencyCode": "EUR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Madagascar",
    "a2": "MG",
    "a3": "MDG",
    "number": "450",
    "dialingCode": "261",
    "currencyCode": "MGA",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Malawi",
    "a2": "MW",
    "a3": "MWI",
    "number": "454",
    "dialingCode": "265",
    "currencyCode": "MWK",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Malaysia",
    "a2": "MY",
    "a3": "MYS",
    "number": "458",
    "dialingCode": "60",
    "currencyCode": "MYR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Maldives",
    "a2": "MV",
    "a3": "MDV",
    "number": "462",
    "dialingCode": "960",
    "currencyCode": "MVR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Mali",
    "a2": "ML",
    "a3": "MLI",
    "number": "466",
    "dialingCode": "223",
    "currencyCode": "XOF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Malta",
    "a2": "MT",
    "a3": "MLT",
    "number": "470",
    "dialingCode": "356",
    "currencyCode": "EUR",
    "VAT": 0.18,
    "bookingCode": "4286015"
  },
  {
    "name": "Marshall Islands",
    "a2": "MH",
    "a3": "MHL",
    "number": "584",
    "dialingCode": "692",
    "currencyCode": "USD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Mauritania",
    "a2": "MR",
    "a3": "MRT",
    "number": "478",
    "dialingCode": "222",
    "currencyCode": "MRO",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Mauritius",
    "a2": "MU",
    "a3": "MUS",
    "number": "480",
    "dialingCode": "230",
    "currencyCode": "MUR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Mexico",
    "a2": "MX",
    "a3": "MEX",
    "number": "484",
    "dialingCode": "52",
    "currencyCode": "MXN",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Monaco",
    "a2": "MC",
    "a3": "MCO",
    "number": "492",
    "dialingCode": "377",
    "currencyCode": "EUR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Mongolia",
    "a2": "MN",
    "a3": "MNG",
    "number": "496",
    "dialingCode": "976",
    "currencyCode": "MNT",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Montenegro",
    "a2": "ME",
    "a3": "MNE",
    "number": "499",
    "dialingCode": "382",
    "currencyCode": "EUR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Morocco",
    "a2": "MA",
    "a3": "MAR",
    "number": "504",
    "dialingCode": "212",
    "currencyCode": "MAD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Mozambique",
    "a2": "MZ",
    "a3": "MOZ",
    "number": "508",
    "dialingCode": "258",
    "currencyCode": "MZN",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Myanmar",
    "a2": "MM",
    "a3": "MMR",
    "number": "104",
    "dialingCode": "95",
    "currencyCode": "MMK",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Namibia",
    "a2": "NA",
    "a3": "NAM",
    "number": "516",
    "dialingCode": "264",
    "currencyCode": "NAD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Nauru",
    "a2": "NR",
    "a3": "NRU",
    "number": "520",
    "dialingCode": "674",
    "currencyCode": "AUD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Nepal",
    "a2": "NP",
    "a3": "NPL",
    "number": "524",
    "dialingCode": "977",
    "currencyCode": "NPR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Netherlands",
    "a2": "NL",
    "a3": "NLD",
    "number": "528",
    "dialingCode": "31",
    "currencyCode": "EUR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "New Zealand",
    "a2": "NZ",
    "a3": "NZL",
    "number": "554",
    "dialingCode": "64",
    "currencyCode": "NZD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Nicaragua",
    "a2": "NI",
    "a3": "NIC",
    "number": "558",
    "dialingCode": "505",
    "currencyCode": "NIO",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Niger",
    "a2": "NE",
    "a3": "NER",
    "number": "562",
    "dialingCode": "227",
    "currencyCode": "XOF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Nigeria",
    "a2": "NG",
    "a3": "NGA",
    "number": "566",
    "dialingCode": "234",
    "currencyCode": "NGN",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Norway",
    "a2": "NO",
    "a3": "NOR",
    "number": "578",
    "dialingCode": "47",
    "currencyCode": "NOK",
    "VAT": 0.25,
    "bookingCode": "4295011"
  },
  {
    "name": "Oman",
    "a2": "OM",
    "a3": "OMN",
    "number": "512",
    "dialingCode": "968",
    "currencyCode": "OMR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Pakistan",
    "a2": "PK",
    "a3": "PAK",
    "number": "586",
    "dialingCode": "92",
    "currencyCode": "PKR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Palau",
    "a2": "PW",
    "a3": "PLW",
    "number": "585",
    "dialingCode": "680",
    "currencyCode": "USD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Panama",
    "a2": "PA",
    "a3": "PAN",
    "number": "591",
    "dialingCode": "507",
    "currencyCode": "USD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Papua New Guinea",
    "a2": "PG",
    "a3": "PNG",
    "number": "598",
    "dialingCode": "675",
    "currencyCode": "PGK",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Paraguay",
    "a2": "PY",
    "a3": "PRY",
    "number": "600",
    "dialingCode": "595",
    "currencyCode": "PYG",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Peru",
    "a2": "PE",
    "a3": "PER",
    "number": "604",
    "dialingCode": "51",
    "currencyCode": "PEN",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Philippines",
    "a2": "PH",
    "a3": "PHL",
    "number": "608",
    "dialingCode": "63",
    "currencyCode": "PHP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Poland",
    "a2": "PL",
    "a3": "POL",
    "number": "616",
    "dialingCode": "48",
    "currencyCode": "PLN",
    "VAT": 0.23,
    "bookingCode": "4286017"
  },
  {
    "name": "Portugal",
    "a2": "PT",
    "a3": "PRT",
    "number": "620",
    "dialingCode": "351",
    "currencyCode": "EUR",
    "VAT": 0.23,
    "bookingCode": "4286018"
  },
  {
    "name": "Qatar",
    "a2": "QA",
    "a3": "QAT",
    "number": "634",
    "dialingCode": "974",
    "currencyCode": "QAR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Romania",
    "a2": "RO",
    "a3": "ROU",
    "number": "642",
    "dialingCode": "40",
    "currencyCode": "RON",
    "VAT": 0.24,
    "bookingCode": "4286019"
  },
  {
    "name": "Rwanda",
    "a2": "RW",
    "a3": "RWA",
    "number": "646",
    "dialingCode": "250",
    "currencyCode": "RWF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Saint Kitts and Nevis",
    "a2": "KN",
    "a3": "KNA",
    "number": "659",
    "dialingCode": "1-869",
    "currencyCode": "XCD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Saint Lucia",
    "a2": "LC",
    "a3": "LCA",
    "number": "662",
    "dialingCode": "1-758",
    "currencyCode": "XCD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Saint Vincent and the Grenadines",
    "a2": "VC",
    "a3": "VCT",
    "number": "670",
    "dialingCode": "1-784",
    "currencyCode": "XCD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Samoa",
    "a2": "WS",
    "a3": "WSM",
    "number": "882",
    "dialingCode": "685",
    "currencyCode": "WST",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "San Marino",
    "a2": "SM",
    "a3": "SMR",
    "number": "674",
    "dialingCode": "378",
    "currencyCode": "EUR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Sao Tome and Principe",
    "a2": "ST",
    "a3": "STP",
    "number": "678",
    "dialingCode": "239",
    "currencyCode": "STD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Saudi Arabia",
    "a2": "SA",
    "a3": "SAU",
    "number": "682",
    "dialingCode": "966",
    "currencyCode": "SAR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Senegal",
    "a2": "SN",
    "a3": "SEN",
    "number": "686",
    "dialingCode": "221",
    "currencyCode": "XOF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Serbia",
    "a2": "RS",
    "a3": "SRB",
    "number": "688",
    "dialingCode": "381 p",
    "currencyCode": "RSD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Seychelles",
    "a2": "SC",
    "a3": "SYC",
    "number": "690",
    "dialingCode": "248",
    "currencyCode": "SCR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Sierra Leone",
    "a2": "SL",
    "a3": "SLE",
    "number": "694",
    "dialingCode": "232",
    "currencyCode": "SLL",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Singapore",
    "a2": "SG",
    "a3": "SGP",
    "number": "702",
    "dialingCode": "65",
    "currencyCode": "SGD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Slovakia",
    "a2": "SK",
    "a3": "SVK",
    "number": "703",
    "dialingCode": "421",
    "currencyCode": "EUR",
    "VAT": 0.2,
    "bookingCode": "4286021"
  },
  {
    "name": "Slovenia",
    "a2": "SI",
    "a3": "SVN",
    "number": "705",
    "dialingCode": "386",
    "currencyCode": "EUR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Solomon Islands",
    "a2": "SB",
    "a3": "SLB",
    "number": "90",
    "dialingCode": "677",
    "currencyCode": "SBD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Somalia",
    "a2": "SO",
    "a3": "SOM",
    "number": "706",
    "dialingCode": "252",
    "currencyCode": "SOS",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "South Africa",
    "a2": "ZA",
    "a3": "ZAF",
    "number": "710",
    "dialingCode": "27",
    "currencyCode": "ZAR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "South Sudan",
    "a2": "SS",
    "a3": "SSD",
    "number": "728",
    "dialingCode": "211",
    "currencyCode": "SSP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Spain",
    "a2": "ES",
    "a3": "ESP",
    "number": "724",
    "dialingCode": "34",
    "currencyCode": "EUR",
    "VAT": 0.21,
    "bookingCode": "4286023"
  },
  {
    "name": "Sri Lanka",
    "a2": "LK",
    "a3": "LKA",
    "number": "144",
    "dialingCode": "94",
    "currencyCode": "LKR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Sudan",
    "a2": "SD",
    "a3": "SDN",
    "number": "729",
    "dialingCode": "249",
    "currencyCode": "SDG",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Suriname",
    "a2": "SR",
    "a3": "SUR",
    "number": "740",
    "dialingCode": "597",
    "currencyCode": "SRD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Swaziland",
    "a2": "SZ",
    "a3": "SWZ",
    "number": "748",
    "dialingCode": "268",
    "currencyCode": "SZL",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Sweden",
    "a2": "SE",
    "a3": "SWE",
    "number": "752",
    "dialingCode": "46",
    "currencyCode": "SEK",
    "VAT": 0.25,
    "bookingCode": "4286020"
  },
  {
    "name": "Switzerland",
    "a2": "CH",
    "a3": "CHE",
    "number": "756",
    "dialingCode": "41",
    "currencyCode": "CHF",
    "VAT": 0.08,
    "bookingCode": "4295001"
  },
  {
    "name": "Tajikistan",
    "a2": "TJ",
    "a3": "TJK",
    "number": "762",
    "dialingCode": "992",
    "currencyCode": "TJS",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Thailand",
    "a2": "TH",
    "a3": "THA",
    "number": "764",
    "dialingCode": "66",
    "currencyCode": "THB",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Timor-Leste",
    "a2": "TL",
    "a3": "TLS",
    "number": "626",
    "dialingCode": "670",
    "currencyCode": "USD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Togo",
    "a2": "TG",
    "a3": "TGO",
    "number": "768",
    "dialingCode": "228",
    "currencyCode": "XOF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Tonga",
    "a2": "TO",
    "a3": "TON",
    "number": "776",
    "dialingCode": "676",
    "currencyCode": "TOP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Trinidad and Tobago",
    "a2": "TT",
    "a3": "TTO",
    "number": "780",
    "dialingCode": "1-868",
    "currencyCode": "TTD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Tunisia",
    "a2": "TN",
    "a3": "TUN",
    "number": "788",
    "dialingCode": "216",
    "currencyCode": "TND",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Turkey",
    "a2": "TR",
    "a3": "TUR",
    "number": "792",
    "dialingCode": "90",
    "currencyCode": "TRY",
    "VAT": 0.18,
    "bookingCode": "4295002"
  },
  {
    "name": "Turkmenistan",
    "a2": "TM",
    "a3": "TKM",
    "number": "795",
    "dialingCode": "993",
    "currencyCode": "TMT",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Tuvalu",
    "a2": "TV",
    "a3": "TUV",
    "number": "798",
    "dialingCode": "688",
    "currencyCode": "AUD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Uganda",
    "a2": "UG",
    "a3": "UGA",
    "number": "800",
    "dialingCode": "256",
    "currencyCode": "UGX",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Ukraine",
    "a2": "UA",
    "a3": "UKR",
    "number": "804",
    "dialingCode": "380",
    "currencyCode": "UAH",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "United Arab Emirates",
    "a2": "AE",
    "a3": "ARE",
    "number": "784",
    "dialingCode": "971",
    "currencyCode": "AED",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "United Kingdom",
    "a2": "GB",
    "a3": "GBR",
    "number": "826",
    "dialingCode": "44",
    "currencyCode": "GBP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Uruguay",
    "a2": "UY",
    "a3": "URY",
    "number": "858",
    "dialingCode": "598",
    "currencyCode": "UYU",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Uzbekistan",
    "a2": "UZ",
    "a3": "UZB",
    "number": "860",
    "dialingCode": "998",
    "currencyCode": "UZS",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Vanuatu",
    "a2": "VU",
    "a3": "VUT",
    "number": "548",
    "dialingCode": "678",
    "currencyCode": "VUV",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Venezuela",
    "a2": "VE",
    "a3": "VEN",
    "number": "862",
    "dialingCode": "58",
    "currencyCode": "VEF",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Congo",
    "a2": "CG",
    "a3": "COG",
    "number": "178",
    "dialingCode": "242",
    "currencyCode": "",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Democratic Republic of the Congo",
    "a2": "CD",
    "a3": "COD",
    "number": "180",
    "dialingCode": "243",
    "currencyCode": "",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Palestine, State of",
    "a2": "PS",
    "a3": "PSE",
    "number": "275",
    "dialingCode": "970",
    "currencyCode": "",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Gibraltar",
    "a2": "GI",
    "a3": "GIB",
    "number": "292",
    "dialingCode": "350",
    "currencyCode": "GBP",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Hong Kong",
    "a2": "HK",
    "a3": "HKG",
    "number": "344",
    "dialingCode": "852",
    "currencyCode": "HKD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Iran, Islamic Republic of",
    "a2": "IR",
    "a3": "IRN",
    "number": "364",
    "dialingCode": "98",
    "currencyCode": "IRR",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Korea, Democratic People's Republic of",
    "a2": "KP",
    "a3": "PRK",
    "number": "408",
    "dialingCode": "850",
    "currencyCode": "",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Korea, Republic of",
    "a2": "KR",
    "a3": "KOR",
    "number": "410",
    "dialingCode": "82",
    "currencyCode": "KRW",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Puerto Rico",
    "a2": "PR",
    "a3": "PRI",
    "number": "630",
    "dialingCode": "1",
    "currencyCode": "USD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Viet Nam",
    "a2": "VN",
    "a3": "VNM",
    "number": "704",
    "dialingCode": "84",
    "currencyCode": "VND",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Syrian Arab Republic",
    "a2": "SY",
    "a3": "SYR",
    "number": "760",
    "dialingCode": "963",
    "currencyCode": "",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "United States",
    "a2": "US",
    "a3": "USA",
    "number": "840",
    "dialingCode": "1",
    "currencyCode": "USD",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Yemen",
    "a2": "YE",
    "a3": "YEM",
    "number": "887",
    "dialingCode": "967",
    "currencyCode": "YER",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Zambia",
    "a2": "ZM",
    "a3": "ZMB",
    "number": "894",
    "dialingCode": "260",
    "currencyCode": "ZMW",
    "VAT": 0.0,
    "bookingCode": ""
  },
  {
    "name": "Zimbabwe",
    "a2": "ZW",
    "a3": "ZWE",
    "number": "716",
    "dialingCode": "263",
    "currencyCode": "USD",
    "VAT": 0.0,
    "bookingCode": ""
  }
]
