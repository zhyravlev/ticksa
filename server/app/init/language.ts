/// <reference path="../typings/tsd.d.ts"/>

let languages = [
  {
    "name": "Arabic",
    "key": null
  },
  {
    "name": "Bengali",
    "key": null
  },
  {
    "name": "Bulgarian",
    "key": null
  },
  {
    "name": "Chinese",
    "key": null
  },
  {
    "name": "Croatian",
    "key": null
  },
  {
    "name": "Czech",
    "key": null
  },
  {
    "name": "Danish",
    "key": null
  },
  {
    "name": "Dutch",
    "key": null
  },
  {
    "name": "English",
    "key": null
  },
  {
    "name": "Estonian",
    "key": null
  },
  {
    "name": "Filipino",
    "key": null
  },
  {
    "name": "Finnish",
    "key": null
  },
  {
    "name": "French",
    "key": null
  },
  {
    "name": "German",
    "key": null
  },
  {
    "name": "Greek",
    "key": null
  },
  {
    "name": "Hebrew",
    "key": null
  },
  {
    "name": "Hindi",
    "key": null
  },
  {
    "name": "Hungarian",
    "key": null
  },
  {
    "name": "Icelandic",
    "key": null
  },
  {
    "name": "Indonesian",
    "key": null
  },
  {
    "name": "Irish",
    "key": null
  },
  {
    "name": "Italian",
    "key": null
  },
  {
    "name": "Japanese",
    "key": null
  },
  {
    "name": "Korean",
    "key": null
  },
  {
    "name": "Latvian",
    "key": null
  },
  {
    "name": "Lithuanian",
    "key": null
  },
  {
    "name": "Norwegian",
    "key": null
  },
  {
    "name": "Persian",
    "key": null
  },
  {
    "name": "Polish",
    "key": null
  },
  {
    "name": "Portuguese",
    "key": null
  },
  {
    "name": "Punjabi",
    "key": null
  },
  {
    "name": "Romanian",
    "key": null
  },
  {
    "name": "Russian",
    "key": null
  },
  {
    "name": "Serbian",
    "key": null
  },
  {
    "name": "Slovak",
    "key": null
  },
  {
    "name": "Slovenian",
    "key": null
  },
  {
    "name": "Spanish",
    "key": null
  },
  {
    "name": "Swedish",
    "key": null
  },
  {
    "name": "Thai",
    "key": null
  },
  {
    "name": "Turkish",
    "key": null
  },
  {
    "name": "Ukrainian",
    "key": null
  },
  {
    "name": "Vietnamese",
    "key": null
  },
  {
    "name": "Yiddish",
    "key": null
  },
];

export = languages;