/// <reference path="../typings/tsd.d.ts"/>

let categories = [
  {
    "name": "Film & Photos",
    "key": null,
  },
  {
    "name": "Autos & Vehicles",
    "key": null,
  },
  {
    "name": "Music",
    "key": null,
  },
  {
    "name": "Pets & Animals",
    "key": null,
  },
  {
    "name": "Sports",
    "key": null,
  },
  {
    "name": "Travel & Events",
    "key": null,
  },
  {
    "name": "Gaming",
    "key": null,
  },
  {
    "name": "People & Blogs",
    "key": null,
  },
  {
    "name": "Comedy",
    "key": null,
  },
  {
    "name": "Entertainment",
    "key": null,
  },
  {
    "name": "News & Politics",
    "key": null,
  },
  {
    "name": "Howto & Style",
    "key": null,
  },
  {
    "name": "Education",
    "key": null,
  },
  {
    "name": "Science & Technology",
    "key": null,
  },
  {
    "name": "Nonprofits & Activism",
    "key": null,
  },
];

export = categories;