/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import Sequelize = require('sequelize');
import q = require('q');
import _ = require('lodash');

import globalConfigs = require('./global-config');
import accounts = require('./account');
import tags = require('./tag');
import currencies = require('./currency');
import countries = require('./country');
import categories = require('./category');
import languages = require('./language');

import Models = require('../models/models')
import Account = Models.Account ;
import Country = Models.Country;
import Currency = Models.Currency;
import Tag = Models.Tag;
import GlobalConfig = Models.GlobalConfig;
import Content = Models.Content;
import Category = Models.Category;
import Language = Models.Language;

import conn = require('../connection');
import config = require('../config');

export function initData(): q.Promise<any> {

  var def = q.defer<any>();

  var defConfig = q.defer();

  q.all(
    _.map(globalConfigs, config => {
      return GlobalConfig.Model.findOrCreate({
        where: {
          field: config.field
        },
        defaults: config
      });
    })
  ).then(result => {
    defConfig.resolve('done');
  }, err => {
    defConfig.reject(err);
  });

  var defAccount = q.defer();
  q.all(
    _.map(accounts, account => {
        return Account.Model.findOrCreate({
          where: {
            id: account.id
          }, defaults: account
        });
      }
    )
  ).then(result => {
    defAccount.resolve('');
  }, err => {
    defAccount.reject(err);
  });

  var defTag = q.defer();
  q.all(
    _.map(tags, tag => {
      return Tag.Model.findOrCreate({
        where: {
          name: tag.name
        }, defaults: tag
      });
    })
  ).then(result => {
    defTag.resolve('');
  }, err => {
    defTag.reject(err);
  });

  var defCurrency = q.defer();

  q.all(
    _.map(currencies, currency => {
        return Currency.Model.findOrCreate({
          where: {
            iso4217Code: currency.iso4217Code
          }, defaults: currency
        });
      }
    )
  ).then(result => {
    defCurrency.resolve('');
  }, err => {
    defCurrency.reject(err);
  });
  var defCountry = q.defer();


  defCurrency.promise.then(function (result) {
    q.all(
      _.map(countries, function (country: any) {
        var def = q.defer();
        Currency.Model.findOne({
          where: {
            iso4217Code: country.currencyCode
          }
        }).then(currency => {
          if (currency != null && currency.iso4217Code && currency.iso4217Code.length > 0) {
            country.currencyId = currency.id;
          }
          Country.Model.findOrCreate({
            where: {
              a3: country.a3
            }, defaults: country
          }).then(result => {
            def.resolve(result);
          }, err => {
            def.reject(err);
          })
        }, err => {
          def.reject(err);
        });
        return def.promise;
      })
    ).then(result => {
      defCountry.resolve('');
    }, err => {
      defCountry.reject(err);
    });
  });


  q.all([
    defConfig.promise,
    defAccount.promise,
    defTag.promise,
    defCurrency.promise,
    defCountry.promise,
    initLanguages(),
    initCategories()
  ]).then(result => {
    def.resolve(result);
  }, err => {
    def.reject(err);
  });

  return def.promise;
}

function initLanguages(): Promise<any> {
  var saveLanguages: Promise<Language.Instance>[] = _.map(languages, language => {
    return Language.Model.findOrCreate({
      where: {
        name: language.name,
      }, defaults: language
    });
  });
  return Promise.all(saveLanguages);
}

function initCategories(): Promise<any> {
  var saveCategories: Promise<Category.Instance>[] = _.map(categories, category => {
    return Category.Model.findOrCreate({
      where: {
        name: category.name,
      }, defaults: category
    });
  });
  return Promise.all(saveCategories);
}

export function initContentRandomPosition(): Promise<any> {
  let replacements = {
    randCoefficient: Content.RANDOM_TO_SHUFFLE_CONTENTS_IN_DISCOVER
  };

  return conn.query('UPDATE "Content" SET "random_position"=floor(random() * :randCoefficient);', {replacements: replacements});
}

export function initGlobalConfig() {
  logger.info('INIT GlobalConfig');
  return GlobalConfig.Model.findAll().then(function (globalConfigs: GlobalConfig.Instance[]) {
    globalConfigs.forEach(function (globalConfig) {
      switch (globalConfig.type) {
        case GlobalConfig.Type.INTEGER:
          config.global[globalConfig.field] = globalConfig.dataInteger;
          break;
        case GlobalConfig.Type.DECIMAL:
          config.global[globalConfig.field] = globalConfig.dataDecimal;
          break;
        case GlobalConfig.Type.STRING:
          config.global[globalConfig.field] = globalConfig.dataString;
          break;
      }
    });
  })
}

