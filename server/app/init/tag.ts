/// <reference path="../typings/tsd.d.ts"/>

var tags = [
  {
    name: 'ART'
  },
  {
    name: 'SPORT'
  },
  {
    name: 'HUMOUR'
  },
  {
    name: 'POLITICS'
  }
];

export = tags;