/// <reference path="../typings/tsd.d.ts"/>

var currencies = [
  {
    "iso4217Code": "AFN",
    "name": "Afghan afghani"
  },
  {
    "iso4217Code": "EUR",
    "name": "European euro"
  },
  {
    "iso4217Code": "ALL",
    "name": "Albanian lek"
  },
  {
    "iso4217Code": "DZD",
    "name": "Algerian dinar"
  },
  {
    "iso4217Code": "USD",
    "name": "United States dollar"
  },
  {
    "iso4217Code": "AOA",
    "name": "Angolan kwanza"
  },
  {
    "iso4217Code": "XCD",
    "name": "East Caribbean dollar"
  },
  {
    "iso4217Code": "ARS",
    "name": "Argentine peso"
  },
  {
    "iso4217Code": "AMD",
    "name": "Armenian dram"
  },
  {
    "iso4217Code": "AWG",
    "name": "Aruban florin"
  },
  {
    "iso4217Code": "AUD",
    "name": "Australian dollar"
  },
  {
    "iso4217Code": "AZN",
    "name": "Azerbaijani manat"
  },
  {
    "iso4217Code": "BSD",
    "name": "Bahamian dollar"
  },
  {
    "iso4217Code": "BHD",
    "name": "Bahraini dinar"
  },
  {
    "iso4217Code": "BDT",
    "name": "Bangladeshi taka"
  },
  {
    "iso4217Code": "BBD",
    "name": "Barbadian dollar"
  },
  {
    "iso4217Code": "BYR",
    "name": "Belarusian ruble"
  },
  {
    "iso4217Code": "BZD",
    "name": "Belize dollar"
  },
  {
    "iso4217Code": "XOF",
    "name": "West African CFA franc"
  },
  {
    "iso4217Code": "BMD",
    "name": "Bermudian dollar"
  },
  {
    "iso4217Code": "BTN",
    "name": "Bhutanese ngultrum"
  },
  {
    "iso4217Code": "BOB",
    "name": "Bolivian boliviano"
  },
  {
    "iso4217Code": "BAM",
    "name": "Bosnia and Herzegovina convertible mark"
  },
  {
    "iso4217Code": "BWP",
    "name": "Botswana pula"
  },
  {
    "iso4217Code": "BRL",
    "name": "Brazilian real"
  },
  {
    "iso4217Code": "BND",
    "name": "Brunei dollar"
  },
  {
    "iso4217Code": "BGN",
    "name": "Bulgarian lev"
  },
  {
    "iso4217Code": "BIF",
    "name": "Burundi franc"
  },
  {
    "iso4217Code": "CVE",
    "name": "Cape Verdean escudo"
  },
  {
    "iso4217Code": "KHR",
    "name": "Cambodian riel"
  },
  {
    "iso4217Code": "XAF",
    "name": "Central African CFA franc"
  },
  {
    "iso4217Code": "CAD",
    "name": "Canadian dollar"
  },
  {
    "iso4217Code": "KYD",
    "name": "Cayman Islands dollar"
  },
  {
    "iso4217Code": "CLP",
    "name": "Chilean peso"
  },
  {
    "iso4217Code": "CNY",
    "name": "Chinese Yuan Renminbi"
  },
  {
    "iso4217Code": "COP",
    "name": "Colombian peso"
  },
  {
    "iso4217Code": "KMF",
    "name": "Comorian franc"
  },
  {
    "iso4217Code": "CDF",
    "name": "Congolese franc"
  },
  {
    "iso4217Code": "",
    "name": "Cook Islands dollar"
  },
  {
    "iso4217Code": "CRC",
    "name": "Costa Rican colon"
  },
  {
    "iso4217Code": "HRK",
    "name": "Croatian kuna"
  },
  {
    "iso4217Code": "CUP",
    "name": "Cuban peso"
  },
  {
    "iso4217Code": "ANG",
    "name": "Netherlands Antillean guilder"
  },
  {
    "iso4217Code": "CZK",
    "name": "Czech koruna"
  },
  {
    "iso4217Code": "DKK",
    "name": "Danish krone"
  },
  {
    "iso4217Code": "DJF",
    "name": "Djiboutian franc"
  },
  {
    "iso4217Code": "DOP",
    "name": "Dominican peso"
  },
  {
    "iso4217Code": "EGP",
    "name": "Egyptian pound"
  },
  {
    "iso4217Code": "ERN",
    "name": "Eritrean nakfa"
  },
  {
    "iso4217Code": "ETB",
    "name": "Ethiopian birr"
  },
  {
    "iso4217Code": "FKP",
    "name": "Falkland Islands pound"
  },
  {
    "iso4217Code": "",
    "name": "Faroese krona"
  },
  {
    "iso4217Code": "FJD",
    "name": "Fijian dollar"
  },
  {
    "iso4217Code": "XPF",
    "name": "CFP franc"
  },
  {
    "iso4217Code": "GMD",
    "name": "Gambian dalasi"
  },
  {
    "iso4217Code": "GEL",
    "name": "Georgian lari"
  },
  {
    "iso4217Code": "GHS",
    "name": "Ghanaian cedi"
  },
  {
    "iso4217Code": "GIP",
    "name": "Gibraltar pound"
  },
  {
    "iso4217Code": "GTQ",
    "name": "Guatemalan quetzal"
  },
  {
    "iso4217Code": "GGP",
    "name": "Guernsey Pound"
  },
  {
    "iso4217Code": "GNF",
    "name": "Guinean franc"
  },
  {
    "iso4217Code": "GYD",
    "name": "Guyanese dollar"
  },
  {
    "iso4217Code": "HTG",
    "name": "Haitian gourde"
  },
  {
    "iso4217Code": "HNL",
    "name": "Honduran lempira"
  },
  {
    "iso4217Code": "HKD",
    "name": "Hong Kong dollar"
  },
  {
    "iso4217Code": "HUF",
    "name": "Hungarian forint"
  },
  {
    "iso4217Code": "ISK",
    "name": "Icelandic krona"
  },
  {
    "iso4217Code": "INR",
    "name": "Indian rupee"
  },
  {
    "iso4217Code": "IDR",
    "name": "Indonesian rupiah"
  },
  {
    "iso4217Code": "XDR",
    "name": "SDR (Special Drawing Right)"
  },
  {
    "iso4217Code": "IRR",
    "name": "Iranian rial"
  },
  {
    "iso4217Code": "IQD",
    "name": "Iraqi dinar"
  },
  {
    "iso4217Code": "IMP",
    "name": "Manx pound"
  },
  {
    "iso4217Code": "ILS",
    "name": "Israeli new sheqel"
  },
  {
    "iso4217Code": "JMD",
    "name": "Jamaican dollar"
  },
  {
    "iso4217Code": "JPY",
    "name": "Japanese yen"
  },
  {
    "iso4217Code": "JEP",
    "name": "Jersey pound"
  },
  {
    "iso4217Code": "JOD",
    "name": "Jordanian dinar"
  },
  {
    "iso4217Code": "KZT",
    "name": "Kazakhstani tenge"
  },
  {
    "iso4217Code": "KES",
    "name": "Kenyan shilling"
  },
  {
    "iso4217Code": "KWD",
    "name": "Kuwaiti dinar"
  },
  {
    "iso4217Code": "KGS",
    "name": "Kyrgyzstani som"
  },
  {
    "iso4217Code": "LAK",
    "name": "Lao kip"
  },
  {
    "iso4217Code": "LBP",
    "name": "Lebanese pound"
  },
  {
    "iso4217Code": "LSL",
    "name": "Lesotho loti"
  },
  {
    "iso4217Code": "LRD",
    "name": "Liberian dollar"
  },
  {
    "iso4217Code": "LYD",
    "name": "Libyan dinar"
  },
  {
    "iso4217Code": "CHF",
    "name": "Swiss franc"
  },
  {
    "iso4217Code": "MOP",
    "name": "Macanese pataca"
  },
  {
    "iso4217Code": "MKD",
    "name": "Macedonian denar"
  },
  {
    "iso4217Code": "MGA",
    "name": "Malagasy ariary"
  },
  {
    "iso4217Code": "MWK",
    "name": "Malawian kwacha"
  },
  {
    "iso4217Code": "MYR",
    "name": "Malaysian ringgit"
  },
  {
    "iso4217Code": "MVR",
    "name": "Maldivian rufiyaa"
  },
  {
    "iso4217Code": "MRO",
    "name": "Mauritanian ouguiya"
  },
  {
    "iso4217Code": "MUR",
    "name": "Mauritian rupee"
  },
  {
    "iso4217Code": "MXN",
    "name": "Mexican peso"
  },
  {
    "iso4217Code": "MDL",
    "name": "Moldovan leu"
  },
  {
    "iso4217Code": "MNT",
    "name": "Mongolian tugrik"
  },
  {
    "iso4217Code": "MAD",
    "name": "Moroccan dirham"
  },
{
  "iso4217Code": "MZN",
  "name": "Mozambican metical"
},
{
  "iso4217Code": "MMK",
  "name": "Myanmar kyat"
},
{
  "iso4217Code": "NAD",
  "name": "Namibian dollar"
},
{
  "iso4217Code": "NPR",
  "name": "Nepalese rupee"
},
{
  "iso4217Code": "NZD",
  "name": "New Zealand dollar"
},
{
  "iso4217Code": "NIO",
  "name": "Nicaraguan cordoba"
},
{
  "iso4217Code": "NGN",
  "name": "Nigerian naira"
},
{
  "iso4217Code": "KPW",
  "name": "North Korean won"
},
{
  "iso4217Code": "NOK",
  "name": "Norwegian krone"
},
{
  "iso4217Code": "OMR",
  "name": "Omani rial"
},
{
  "iso4217Code": "PKR",
  "name": "Pakistani rupee"
},
{
  "iso4217Code": "PGK",
  "name": "Papua New Guinean kina"
},
{
  "iso4217Code": "PYG",
  "name": "Paraguayan guarani"
},
{
  "iso4217Code": "PEN",
  "name": "Peruvian nuevo sol"
},
{
  "iso4217Code": "PHP",
  "name": "Philippine peso"
},
{
  "iso4217Code": "PLN",
  "name": "Polish zloty"
},
{
  "iso4217Code": "QAR",
  "name": "Qatari riyal"
},
{
  "iso4217Code": "RON",
  "name": "Romanian leu"
},
{
  "iso4217Code": "RUB",
  "name": "Russian ruble"
},
{
  "iso4217Code": "RWF",
  "name": "Rwandan franc"
},
{
  "iso4217Code": "SHP",
  "name": "Saint Helena pound"
},
{
  "iso4217Code": "WST",
  "name": "Samoan tala"
},
{
  "iso4217Code": "STD",
  "name": "Sao Tome and Principe dobra"
},
{
  "iso4217Code": "SAR",
  "name": "Saudi riyal"
},
{
  "iso4217Code": "RSD",
  "name": "Serbian dinar"
},
{
  "iso4217Code": "SCR",
  "name": "Seychellois rupee"
},
{
  "iso4217Code": "SLL",
  "name": "Sierra Leonean leone"
},
{
  "iso4217Code": "SGD",
  "name": "Singapore dollar"
},
{
  "iso4217Code": "SBD",
  "name": "Solomon Islands dollar"
},
{
  "iso4217Code": "SOS",
  "name": "Somali shilling"
},
{
  "iso4217Code": "ZAR",
  "name": "South African rand"
},
{
  "iso4217Code": "KRW",
  "name": "South Korean won"
},
{
  "iso4217Code": "SSP",
  "name": "South Sudanese pound"
},
{
  "iso4217Code": "LKR",
  "name": "Sri Lankan rupee"
},
{
  "iso4217Code": "SDG",
  "name": "Sudanese pound"
},
{
  "iso4217Code": "SRD",
  "name": "Surinamese dollar"
},
{
  "iso4217Code": "SZL",
  "name": "Swazi lilangeni"
},
{
  "iso4217Code": "SEK",
  "name": "Swedish krona"
},
{
  "iso4217Code": "SYP",
  "name": "Syrian pound"
},
{
  "iso4217Code": "TWD",
  "name": "New Taiwan dollar"
},
{
  "iso4217Code": "TJS",
  "name": "Tajikistani somoni"
},
{
  "iso4217Code": "TZS",
  "name": "Tanzanian shilling"
},
{
  "iso4217Code": "THB",
  "name": "Thai baht"
},
{
  "iso4217Code": "TOP",
  "name": "Tongan paʻanga"
},
{
  "iso4217Code": "TTD",
  "name": "Trinidad and Tobago dollar"
},
{
  "iso4217Code": "TND",
  "name": "Tunisian dinar"
},
{
  "iso4217Code": "TRY",
  "name": "Turkish lira"
},
{
  "iso4217Code": "TMT",
  "name": "Turkmenistani manat"
},
{
  "iso4217Code": "UGX",
  "name": "Ugandan shilling"
},
{
  "iso4217Code": "UAH",
  "name": "Ukrainian hryvnia"
},
{
  "iso4217Code": "AED",
  "name": "UAE dirham"
},
{
  "iso4217Code": "GBP",
  "name": "Pound sterling"
},
{
  "iso4217Code": "UYU",
  "name": "Uruguayan peso"
},
{
  "iso4217Code": "UZS",
  "name": "Uzbekistani som"
},
{
  "iso4217Code": "VUV",
  "name": "Vanuatu vatu"
},
{
  "iso4217Code": "VEF",
  "name": "Venezuelan bolivar"
},
{
  "iso4217Code": "VND",
  "name": "Vietnamese dong"
},
{
  "iso4217Code": "YER",
  "name": "Yemeni rial"
},
{
  "iso4217Code": "ZMW",
  "name": "Zambian kwacha"
}
  ];

export = currencies;