/// <reference path="../typings/tsd.d.ts"/>

import Models = require('../models/models');
import GlobalConfig = Models.GlobalConfig;

var configs = [
  {
    field: 'maxChannels',
    type: GlobalConfig.Type.INTEGER,
    dataInteger: 50
  },
  {
    field: 'extendContentPrice',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 5.0
  },
  {
    field: 'contentTickPrice',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 1.0
  },
  {
    field: 'adminCostPercent',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 0.2
  },
  {
    field: 'campaignTicksPercent',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 0.2
  },
  {
    field: 'campaignFreeTicksPercent',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 0.05
  },
  {
    field: 'VATRate',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 0.2
  },
  {
    field: 'euroToTicksRate',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 100
  },
  {
    field: 'feedPostPrice',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 1.0
  },
  {
    field: 'contentExpiryExtensionTimeInSeconds',
    type: GlobalConfig.Type.INTEGER,
    dataInteger: 2592000
  },
  {
    field: 'registrationFreeTicks',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 15.0
  },
  {
    field: 'videoPer200MBTickPrice',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 1.0
  },
  {
    field: 'adminCostFreeTicksPercent',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 0.8
  },
  {
    field: 'contentPostPrice',
    type: GlobalConfig.Type.DECIMAL,
    dataDecimal: 1
  },
  {
    field: 'contentExpiryTimeInSeconds',
    type: GlobalConfig.Type.INTEGER,
    dataInteger: 31536000
  },
  {
    field: 'voucherInvalidChecksToBan',
    type: GlobalConfig.Type.INTEGER,
    dataInteger: 3
  },
  {
    field: 'voucherMaxCount',
    type: GlobalConfig.Type.INTEGER,
    dataInteger: 10
  },
  {
    field: 'exchangeRateSmartLimit',
    type: GlobalConfig.Type.DECIMAL,
    dataInteger: 2.0
  },
];

export = configs;