/// <reference path="./typings/tsd.d.ts"/>

import Ticksa = require('ticksa');
import Configuration = require('./configuration/index');

const config: Ticksa.Configuration.FullConfiguration = {
  global: {},
  env: function (): Ticksa.Configuration.EnvConfig {
    return config[process.env.mode];
  },
  isLocal: function (): boolean {
    return process.env.mode == 'local';
  },
  isTest: function (): boolean {
    return process.env.mode == 'test';
  },
  isQA: function (): boolean {
    return process.env.mode == 'qa';
  },
  isStaging: function (): boolean {
    return process.env.mode == 'staging';
  },
  isProduction: function (): boolean {
    return process.env.mode == 'production';
  },
  getMode: function() {
    return process.env.mode;
  },
  getTemplateFilePath: function(path: string): string {
    return __dirname + '/views/' + path;
  },
  DATE_FORMAT: 'YYYY-MM-DD',
  DATE_TIME_FORMAT: 'YYYY-MM-DD HH:mm:ss',
  test: Configuration.TEST,
  local: Configuration.LOCAL,
  qa: Configuration.QA,
  staging: Configuration.STAGING,
  production: Configuration.PRODUCTION
};

export = config;
