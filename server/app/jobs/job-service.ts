/// <reference path="../typings/tsd.d.ts"/>
const logger = require('../logger/logger')(__filename);

import cron = require('cron');
import moment = require('moment');
import Moment = moment.Moment;
import Duration = moment.Duration;

import init = require('../init/init');

export function start() {
  startContentUpdateRandomPositionJob();
}

function startContentUpdateRandomPositionJob() {
  new cron.CronJob('00 00 00 * * *', function () {
    const start: Moment = moment();

    init.initContentRandomPosition()
      .then(function () {
        const stop: Moment = moment();
        const duration: Duration = moment.duration(stop.diff(start));
        logger.info('ContentUpdateRandomPositionJob [success]'
          + '. start: ' + start.format()
          + '. stop: ' + stop.format()
          + '. duration: ' + duration.asSeconds()
        );
      })
      .catch(function (err) {
        const stop: Moment = moment();
        const duration: Duration = moment.duration(stop.diff(start));
        logger.warn('ContentUpdateRandomPositionJob [failure]'
          + '. start: ' + start.format()
          + '. stop: ' + stop.format()
          + '. duration: ' + duration.asSeconds()
          + '. err: ' + JSON.stringify(err)
        );
      });
  }).start();
}