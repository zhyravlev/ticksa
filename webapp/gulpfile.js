/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are splitted in several files in the gulp directory
 *  because putting all here was really too long
 */

'use strict';

var path = require('path');
var gulp = require('gulp');
var wrench = require('wrench');
var del = require('del');
var htmlReplace = require('gulp-html-replace');
var conf = require('./gulp/conf');


/**
 *  This will load all js or coffee files in the gulp directory
 *  in order to load all gulp tasks
 */
wrench.readdirSyncRecursive('./gulp').filter(function (file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function (file) {
  require('./gulp/' + file);
});

gulp.task('clean-deployment', function (done) {
  del(['../server/app/public/**'], {force: true}).then(function () {
    done();
  });
});

gulp.task('deploy', ['clean-deployment'], function(){
  return gulp.src([
    path.join(conf.paths.dist, '/**/*'),
    path.join('!' + conf.paths.dist, '/**/*.map')
  ]).pipe(gulp.dest('../server/app/public'));
});

gulp.task('replace', function () {
  gulp.src('./app/index.html')
    .pipe(htmlReplace({
      'base': '<test></test>'
    }))
    .pipe(gulp.dest('./app/index-test.html'))
});