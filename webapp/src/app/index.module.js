(function () {
  'use strict';

  angular
    .module('ticksaWebapp',
      [
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngAnimate',
        'ngSanitize',
        'ui.router',
        'ngMaterial',
        'pascalprecht.translate',
        'jkuri.gallery.custom',
        'chart.js',
        'infinite-scroll',
        'wu.masonry.custom',
        'ngLodash',
        'angular-jwt',
        'textAngular',
        'com.2fdevs.videogular',
        'com.2fdevs.videogular.plugins.controls',
        'com.2fdevs.videogular.plugins.overlayplay',
        'com.2fdevs.videogular.plugins.buffering',
        'pasvaz.bindonce',
        'angulartics',
        'angulartics.google.tagmanager',
        'ng-sortable',
        'ngclipboard',
        'checklist-model',
        'duScroll',
        'yaru22.angular-timeago',
        'ng.jGrowl'
      ]);

})();
