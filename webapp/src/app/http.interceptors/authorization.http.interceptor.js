(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('AuthorizationHttpInterceptor', AuthorizationHttpInterceptor);

  /** @ngInject */
  function AuthorizationHttpInterceptor($q, $log, AuthorizationTokenService, CircularDependenciesResolverService) {

    $log = $log.getInstance('AuthorizationHttpInterceptor');

    // this code is chosen to let server notify that client request was unauthorized
    var HTTP_CODE__UNAUTHORIZED = 401;

    return {
      request: function (config) {
        var token = AuthorizationTokenService.getToken();
        if (token != null) {
          config.headers['token'] = token;
        }
        return config;
      },

      requestError: function (config) {
        return config;
      },

      response: function (res) {
        return res;
      },

      responseError: function (rejection) {
        var def = $q.defer();
        if (rejection.status == HTTP_CODE__UNAUTHORIZED) {
          $log.warn('Unauthorized request detected. Redirected to login page');

          var AuthorizationService = CircularDependenciesResolverService.getAuthorizationService();
          AuthorizationService.resetUserAndToken();

          var NavigationService = CircularDependenciesResolverService.getNavigationService();
          NavigationService.goToDiscoveryPage(true);
        }
        def.reject(rejection);
        return def.promise;
      }
    }
  }
})();
