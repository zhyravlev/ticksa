(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('RedirectToPaymentPageHttpInterceptor', RedirectToPaymentPageHttpInterceptor);

  /** @ngInject */
  function RedirectToPaymentPageHttpInterceptor($q, CircularDependenciesResolverService) {

    // this code is chosen to let server notify client that payment was aborted because not enough ticks
    var HTTP_CODE__PAYMENT_REQUIRED = 402;

    return {
      request: function (config) {
        return config;
      },

      requestError: function (config) {
        return config;
      },

      response: function (res) {
        return res;
      },

      responseError: function (rejection) {
        var def = $q.defer();
        var NavigationService = CircularDependenciesResolverService.getNavigationService();
        if (rejection.status == HTTP_CODE__PAYMENT_REQUIRED) {
          NavigationService.saveStateAndGoToTopUp();
        }
        def.reject(rejection);
        return def.promise;
      }
    }
  }
})();
