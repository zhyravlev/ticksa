(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        url: '',
        abstract: true,
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'ctrl',
        resolve: {
          initializationPromise: function (AuthorizationService) {
            return AuthorizationService.initUserOrGoDiscovery();
          }
        }
      })
      .state('main.history', {
        url: '/history',
        templateUrl: 'app/components/history/history.html',
        controller: 'HistoryController',
        controllerAs: 'ctrl'
      })
      .state('main.dashboard', {
        url: '/dashboard',
        templateUrl: 'app/components/dashboard/dashboard.html',
        controller: 'DashboardController',
        controllerAs: 'ctrl'
      })
      .state('retrievePassword', {
        url: '/retrieve-password',
        templateUrl: 'app/components/password/retrievePassword.html',
        controller: 'RetrievePasswordController',
        controllerAs: 'ctrl'
      });

    /** If route not exists redirect to `main.contentList` state */
    $urlRouterProvider.otherwise('/contents');
  }

})();
