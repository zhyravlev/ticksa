(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkDateFormat', TkDateFormat);

  /** @ngInject */
  function TkDateFormat(Formats, tkMoment) {
    return {
      restrict: 'A',
      require: "ngModel",
      link: function link(scope, element, attrs, ngModelCtrl) {
        var format;
        if (!attrs.tkDateFormat || attrs.tkDateFormat === 'date') {
          format = Formats.DATE_FORMAT;
        } else if(attrs.tkDateFormat === 'datetime') {
          format = Formats.DATE_TIME_FORMAT;
        }

        ngModelCtrl.$formatters.push(function(modelValue) {
          if(!modelValue){
            return null;
          }
          var m = tkMoment(modelValue);
          return m.isValid()
            ? m.toDate()
            : tkMoment(modelValue, [Formats.DATE, Formats.DATE_TIME_FORMAT], true).toDate();
        });
        ngModelCtrl.$parsers.push(function(date) {
          return date ? tkMoment(date).format(format) : null;
        });
      }
    };
  }
})();

