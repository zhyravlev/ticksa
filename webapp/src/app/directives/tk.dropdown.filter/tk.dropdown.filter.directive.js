(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkDropDownFilter', tkDropDownFilter);

  /**
   * Ticksa drop down filter directive.
   *
   * Params:
   * @label {object} Pass object with two fields `selected` and `unSelected`. If field `unSelected` not been passed, used only `selected`.
   * @items {object} Data object.
   * @selected {object} Selected items.
   * @disabled {boolean} Default: false.
   * @search {boolean} Default: false. Enable search bar.
   * @searchId {string} MANDATORY if search == true. Id to link label and input in html code.
   * @order-by {boolean} Default: false. Enable to sort by name.
   * @unique-key {string} The key by which will be checked by the uniqueness of the field. Is REQUIRED! Example: Model is [{name:'',id:''}, {...}], html layout is <tk-drop-down-filter ... unique-key="id"><tk-drop-down-filter>
   */

  /** @ngInject */
  function tkDropDownFilter($filter, lodash) {
    return {
      replace: true,
      restrict: 'E',
      templateUrl: 'app/directives/tk.dropdown.filter/tk.dropdown.filter.html',
      scope: {
        label: '=',
        items: '=',
        selected: '=',
        disabled: '=',
        search: '=',
        searchId: '@',
        orderBy: '=',
        uniqueKey: '@'
      },
      link: function (scope) {
        if (!angular.isString(scope.uniqueKey)) {
          throw new Error('Attribute "unique-key" is required!');
        }
        if (scope.search && lodash.isEmpty(scope.searchId)) {
          scope.searchId = '' + Math.random();
          throw new Error('Attribute "searchId" is required!');
        }

        if (scope.orderBy) {
          scope.items = $filter('orderBy')(scope.items, 'name');
        }

        scope.q = '';

        scope.toggle = function (item) {
          if (scope.selected == null) {
            return;
          }

          var idx = lodash.findIndex(scope.selected, scope.uniqueKey, item[scope.uniqueKey]);
          if (idx > -1) {
            scope.selected.splice(idx, 1);
            return;
          }

          scope.selected.push(item);
        };

        scope.exists = function (item) {
          if (scope.selected == null) {
            return false;
          }

          var idx = lodash.findIndex(scope.selected, scope.uniqueKey, item[scope.uniqueKey]);
          return idx > -1;
        };

        scope.isSelectAllIndeterminate = function () {
          if (scope.selected == null) {
            return false;
          }

          return !lodash.isEmpty(scope.selected)
            && scope.selected.length !== scope.items.length;
        };

        scope.isSelectAllChecked = function () {
          if (scope.selected == null) {
            return false;
          }

          return scope.selected.length === scope.items.length;
        };

        scope.toggleAll = function () {
          if (scope.selected == null) {
            return;
          }

          if (scope.selected.length === scope.items.length) {
            scope.selected = [];
            return;
          }

          if (scope.selected.length === 0 || scope.items.length > 0) {
            scope.selected = scope.items.slice(0);
          }
        };
      }
    }
  }
})();
