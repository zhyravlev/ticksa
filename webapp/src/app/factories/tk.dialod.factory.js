(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('TkDialogFactory', TkDialogFactory);

  /** @ngInject */
  function TkDialogFactory($rootScope, $timeout, $mdDialog, $document, tkModernizr, tkDialog, EVENTS) {

    /******************************************************************************************************************
     *
     * How it use.
     *
     * Javascript:
     *
     *    var tkDialog = new TkDialogFactory();
     *    tkDialog.controller( ... controller name here ... );
     *    tkDialog.templateUrl( ... url to your dialog template ... );
     *    tkDialog.show();                                                // For showing dialog. Return promises.
     *
     * HTML:
     *
     *    <md-dialog class="as-tk-dialog col-**-**">                      // Use .col-{size prefix}-{percent multiple 4} classes for setting window size е.с. .col-xs-96 .col-sm-48 .col-md-40 .col-lg-32
     *      ... Your template ...                                         // .col-xs- 0px and more
     *    </md-dialog>                                                    // .col-sm- 768px and more
     *                                                                    // .col-md- 992px and more
     *                                                                    // .col-lg- 1200px and more
     * Enjoy ;-)
     *
     ******************************************************************************************************************/

    var TkDialogFactory = function () {
      this.options = {
        controllerAs: 'ctrl',
        escapeToClose: false,
        focusOnOpen: false,
        transformTemplate: function (template, options) {
          return '<div class="tk-dialog-container" tabindex="-1">' +
            '<div class="tk-dialog-container__table">' +
            '<div class="tk-dialog-container__table-cell">' + validatedTemplate(template) + '</div></div></div>';

          function validatedTemplate(template) {
            if (options.autoWrap && !/<\/md-dialog>/g.test(template)) {
              return '<md-dialog>' + (template || '') + '</md-dialog>';
            } else {
              return template || '';
            }
          }
        },
        onComplete: function ($scope, $element) {

          // Animate
          var dieTimeout;
          var dieBroadcast = $rootScope.$on(EVENTS.ANIMATE_DIALOG, function () {
            var animateClassName = tkDialog.animateClassName;
            var dialogElement = angular.element($element).find('md-dialog');
            dialogElement.addClass('animated ' + animateClassName);
            dieTimeout = $timeout(function () {
              dialogElement.removeClass('animated ' + animateClassName);
            }, 1000);
          });

          // Click outside to close handler
          var eventType = tkModernizr.touch ? 'touchstart' : 'click';
          $document.on(eventType, clickOutsideToCloseHandler);
          function clickOutsideToCloseHandler(event) {
            var target = event.target;
            while (target.parentNode != null) {
              if (target.tagName.toLowerCase().indexOf('md-dialog') != -1) {
                return;
              }
              if (target.className.toLowerCase().indexOf('tk-dialog-container') != -1) {
                $mdDialog.cancel();
                $document.off(eventType, clickOutsideToCloseHandler);
                return;
              }
              target = target.parentNode;
            }
          }

          $scope.$on('$destroy', function () {
            $document.off(eventType, clickOutsideToCloseHandler);
            dieBroadcast();
            if (angular.isFunction(dieTimeout)) {
              dieTimeout();
            }
          });
        }
      }
    };

    TkDialogFactory.prototype.templateUrl = function (templateUrl) {
      this.options.templateUrl = templateUrl;
    };

    TkDialogFactory.prototype.controller = function (controller) {
      this.options.controller = controller;
    };

    TkDialogFactory.prototype.controllerAs = function (controllerAs) {
      this.options.controllerAs = controllerAs;
    };

    TkDialogFactory.prototype.locals = function (locals) {
      this.options.locals = locals;
    };

    TkDialogFactory.prototype.show = function () {
      return $mdDialog.show(this.options);
    };

    return TkDialogFactory;
  }
})();
