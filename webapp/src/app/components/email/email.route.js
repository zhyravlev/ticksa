(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('email', {
        abstract: true,
        url: '/emails',
        templateUrl: 'app/components/email/email.html'
      })
      .state('email.reply', {
        url: '/reply?token',
        templateUrl: 'app/components/email/email.reply.html',
        controller: 'EmailReplyController',
        controllerAs: 'ctrl'
      })
      .state('email.unsubscribe', {
        url: '/unsubscribe?token',
        templateUrl: 'app/components/email/email.unsubscribe.html',
        controller: 'EmailUnsubscribeController',
        controllerAs: 'ctrl'
      })
      .state('email.badToken', {
        url: '/bad-token',
        templateUrl: 'app/components/email/email.badToken.html',
        controller: 'EmailBadTokenController',
        controllerAs: 'ctrl'
      });

  }
})();
