(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('EmailUnsubscribeController', EmailUnsubscribeController);

  /** @ngInject */
  function EmailUnsubscribeController($state, $stateParams, EmailService, NavigationService) {
    var ctrl = this;

    ctrl.attemptRequests = true;
    ctrl.subscription = false;

    ctrl.close = function () {
      NavigationService.goToDiscoveryPage();
    };

    ctrl.onChange = function (subscription) {
      if (!ctrl.attemptRequests) {
        return;
      }

      ctrl.attemptRequests = false;
      if (subscription) {
        EmailService.subscribe($stateParams.token).then(enableRequests, function () {
          enableRequests();
          ctrl.subscription = !ctrl.subscription;
        });
      } else {
        EmailService.unsubscribe($stateParams.token).then(enableRequests, function () {
          enableRequests();
          ctrl.subscription = !ctrl.subscription;
        });
      }

      function enableRequests() {
        ctrl.attemptRequests = true;
      }
    };

    activate();

    function activate() {
      EmailService.checkSubscription($stateParams.token)
        .then(function (res) {
          ctrl.subscription = res.data.subscription;
        })
        .catch(function () {
          $state.go('email.badToken');
        });
    }
  }
})();
