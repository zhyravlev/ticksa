(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('EmailReplyController', EmailReplyController);

  /** @ngInject */
  function EmailReplyController($state, $stateParams, $translate, EmailService, NavigationService, jGrowlNotify) {
    var ctrl = this;
    ctrl.replyText = '';

    activate();

    function activate() {
      return EmailService.verifyToken($stateParams.token)
        .then(function (res) {
          ctrl.tokenData = res.data;
        })
        .catch(function () {
          $state.go('email.badToken');
        });
    }

    ctrl.cancel = function () {
      NavigationService.goToDiscoveryPage();
    };

    ctrl.reply = function (form) {
      if (form.$valid) {
        EmailService.reply($stateParams.token, ctrl.replyText)
          .then(function (res) {
            new jGrowlNotify().success($translate.instant('EMAILS_REPLY_SENDING', {id: res.data.commentId}));
            NavigationService.goToDiscoveryPage();
          })
          .catch(function () {
            new jGrowlNotify().error($translate.instant('ERROR_SERVER'));
          });
      }
    };
  }
})();
