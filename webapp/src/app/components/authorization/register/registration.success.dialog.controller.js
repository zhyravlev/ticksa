(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ModalRegistrationSuccessController', ModalRegistrationSuccessController);

  /** @ngInject */
  function ModalRegistrationSuccessController($mdDialog) {
    var ctrl = this;

    ctrl.confirm = function () {
      $mdDialog.hide();
    };

  }
})();
