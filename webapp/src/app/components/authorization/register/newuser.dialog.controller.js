(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('NewUserDialogController', NewUserDialogController);

  /** @ngInject */
  function NewUserDialogController($window, RECAPTCHA, $scope, $timeout, $mdDialog, AdminService) {

    var ctrl = this;


    ctrl.publicKey = RECAPTCHA.publicKey;
    ctrl.cancel = function () {
      $mdDialog.cancel();
    };


    var recaptchaWidget;
    $window.recaptchaApiLoaded = function () {
      recaptchaWidget = grecaptcha.render('test1', {
        'sitekey': RECAPTCHA.publicKey,
        'theme': 'light',
        'callback': ctrl.onResponse
      })
    };

    ctrl.onResponse = function (response) {
      ctrl.recaptcha = response;
    };

    ctrl.submit = function (form) {
      $timeout(function () {
        ctrl.attemptedSave = true;
        if (form.$valid) {
          $mdDialog.hide({name: ctrl.name, recaptcha: ctrl.recaptcha});
        }
      })

    };

  }
})();
