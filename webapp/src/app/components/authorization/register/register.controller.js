(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('RegisterController', RegisterController);

  /** @ngInject */
  function RegisterController($log, $mdDialog, $stateParams, $document,
                              SelectionsService, AuthorizationHttpService, NavigationService, CountryService) {

    $log = $log.getInstance('RegisterController');

    var ctrl = this;

    ctrl.register = register;
    ctrl.languages = SelectionsService.getLanguages();
    CountryService.getCountries()
      .then(function(countries) {
        ctrl.countries = countries;
      });

    function register(registerForm, user) {
      ctrl.attemptedSave = true;
      if (registerForm.$valid) {
        user.referrerId = $stateParams.ref;
        AuthorizationHttpService.register(user)
          .success(function () {
            ctrl.registrationSuccess = true;

            $mdDialog.show({
              controller: 'ModalRegistrationSuccessController',
              controllerAs: 'ctrl',
              templateUrl: 'app/components/authorization/register/registration.success.dialog.html',
              parent: angular.element($document[0].body),
              clickOutsideToClose: true,
              escapeToClose: true
            }).finally(function () {
              NavigationService.goToLoginPage();
            });

          })
          .error(function (error) {
            $log.log(error);
          })
      }
    }
  }
})();
