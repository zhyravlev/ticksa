(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('register', {
        url: '/register?ref',
        templateUrl: 'app/components/authorization/register/register.html',
        controller: 'RegisterController',
        controllerAs: 'ctrl'
      })
      .state('base', {
        abstract: true,
        templateUrl: 'app/components/authorization/login/login.base.html',
        resolve: {
          initializationPromise: function (AuthorizationService) {
            return AuthorizationService.initUser();
          }
        }
      })
      .state('base.forgotPassword', {
        url: '/change-password/:code',
        templateUrl: 'app/components/authorization/login/new.password.html',
        controller: 'NewPasswordController',
        controllerAs: 'ctrl'
      })
      .state('base.login', {
        url: '/login',
        templateUrl: 'app/components/authorization/login/login.form.html',
        controller: 'LoginFormController',
        controllerAs: 'ctrl'
      })
      // here user is redirected by server when it handles GET request for 'Share as link' URLs
      .state('base.teaser', {
        url: '/teaser/:channelId/:contentId?referrerId',
        templateUrl: 'app/components/authorization/login/login.teaser.html',
        controller: 'LoginTeaserController',
        controllerAs: 'ctrl'
      })

  }

})();
