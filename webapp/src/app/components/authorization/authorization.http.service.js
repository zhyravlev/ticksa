(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('AuthorizationHttpService', AuthorizationHttpService);

  /** @ngInject */
  function AuthorizationHttpService($http, $location, API) {
    var AuthorizationHttpService = this;

    AuthorizationHttpService.startForgotPasswordProcess = function (email) {
      return $http.post(API.URL + '/registrations/forgot-password', {email: email});
    };

    AuthorizationHttpService.finishForgetPasswordProcess = function (password, url) {
      return $http.post(API.URL + '/registrations/new-password', {url: url, password: password});
    };

    AuthorizationHttpService.register = function (user) {
      return $http.post(API.URL + '/registrations/register', user)
    };

    AuthorizationHttpService.checkAvailableEmail = function (email) {
      return $http.get(API.URL + '/registrations/check-email?email=' + email);
    };

    AuthorizationHttpService.refreshToken = function () {
      return $http.post(API.URL + '/login/refresh-token', null)
    };

    AuthorizationHttpService.login = function (username, password) {
      return $http.post(API.URL + '/login', {username: username, password: password})
    };

    AuthorizationHttpService.loginGoogle = function (idToken, referrerId) {
      return $http.post(API.URL + '/login/google', {idToken: idToken}, {params: {referral: referrerId}})
    };

    // unused since APP-818
    AuthorizationHttpService.loginFacebook = function (signedRequest, accessToken, referrerId) {
      return $http.post(API.URL + '/login/facebook', {
        signedRequest: signedRequest,
        accessToken: accessToken
      }, {params: {referral: referrerId}});
    };

    AuthorizationHttpService.registerLanding = function () {
      return $http.post(API.URL + '/landing', {url: $location.url()});
    };
  }

})();
