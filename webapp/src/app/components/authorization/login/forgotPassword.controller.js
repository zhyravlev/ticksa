(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ForgotPasswordController', ForgotPasswordController);

  /** @ngInject */
  function ForgotPasswordController($rootScope, $mdDialog, EVENTS) {

    var ctrl = this;

    ctrl.submitForgotPassword = submitForgotPassword;
    ctrl.close = close;

    function close() {
      $mdDialog.cancel();
    }

    function submitForgotPassword(form) {
      ctrl.attemptedSave = true;

      if (form.$valid) {
        return $mdDialog.hide({email: ctrl.email});
      }

      $rootScope.$broadcast(EVENTS.ANIMATE_DIALOG);
    }
  }
})();
