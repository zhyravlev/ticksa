(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('LoginTeaserController', LoginTeaserController);

  /** @ngInject */
  function LoginTeaserController($log, $rootScope, $scope, $stateParams, $state, $translate, $timeout,
                                 ContentService, UserService, EVENTS) {

    $log = $log.getInstance('LoginTeaserController');

    var ctrl = this;

    var redirectTimeout = null;
    ctrl.content = null;
    ctrl.error = false;

    if (!UserService.isEmpty()) {
      ContentService.getContentTeaser($stateParams.channelId, $stateParams.contentId)
        .success(successHandler)
        .error(errorHandler);
    }

    else {
      ContentService.getContentTeaserNotLoggedIn($stateParams.channelId, $stateParams.contentId)
        .success(successHandler)
        .error(errorHandler);
    }

    function successHandler(data) {
      ctrl.content = data;
      if (data.channel.background !== null) {
        $rootScope.$broadcast(EVENTS.CHANGE_CHANNEL_BACKGROUND, {
          key: data.channel.background.key
        })
      }
    }

    function errorHandler(err, status) {
      var stateName = UserService.isEmpty() ? 'discovery' : 'main.contentList';
      var type = err.body != null ? err.body.type : '';
      switch (status) {
        case 403:
          ctrl.error = $translate.instant('ERROR_' + type);
          redirectTimeout = $timeout(function () {
            $state.go(stateName);
          }, 5000);
          break;

        default:
          $state.go(stateName);
      }

      $rootScope.$broadcast(EVENTS.CHANGE_CHANNEL_BACKGROUND); // reset background
      $log.error(err);
    }

    $scope.$on('$destroy', function () {
      $timeout.cancel(redirectTimeout);
    });
  }
})();
