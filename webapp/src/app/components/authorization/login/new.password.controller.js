(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('NewPasswordController', NewPasswordController);

  /** @ngInject */
  function NewPasswordController($stateParams, $log, $translate, AuthorizationHttpService, NavigationService, jGrowlNotify) {

    $log = $log.getInstance('NewPasswordController');

    var ctrl = this;

    ctrl.cancel = function () {
      NavigationService.goToLoginPage(true);
    };

    ctrl.newPasswordSubmit = function (form) {
      ctrl.attemptedSave = true;
      if (form.$valid) {
        AuthorizationHttpService.finishForgetPasswordProcess(ctrl.password, $stateParams.code)
          .success(function () {
            new jGrowlNotify().success($translate.instant('NEW_PASSWORD_SUCCESS')).then(function () {
              NavigationService.goToLoginPage(true);
            });
          })
          .error(function (err) {
            $log.error(err);
            new jGrowlNotify().error($translate.instant('ERROR_UNKNOWN'));
          })
      }
    }

  }
})();
