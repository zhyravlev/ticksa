(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('LoginFormController', LoginFormController);

  /** @ngInject */
  function LoginFormController($mdDialog, $log, $document, $translate,
                               AuthorizationService, AuthorizationHttpService, UserService, NavigationService, TkDialogFactory, jGrowlNotify) {

    $log = $log.getInstance('LoginFormController');

    var ctrl = this;

    ctrl.errors = {
      invalidCredentials: false
    };

    ctrl.login = login;
    ctrl.forgotPassword = forgotPassword;
    ctrl.loginWithGoogle = loginWithGoogle;
    ctrl.loginWithFacebook = loginWithFacebook;
    ctrl.showTermsAndConditions = showTermsAndConditions;

    if (!UserService.isEmpty()) {
      NavigationService.goToPreviousStateOrProvided('main.contentList', null)
    }

    function loginWithGoogle() {
      return AuthorizationService.loginGoogle();
    }

    function loginWithFacebook() {
      return AuthorizationService.loginFacebook();
    }

    function login(form, user) {
      ctrl.attemptedSave = true;
      if (form.$valid) {
        AuthorizationService.login(user.username, user.password)
          .then(function (data) {
            return AuthorizationService.finishLogin(data.token);
          })
          .catch(handleLoginError);
      }
    }

    function register() {
      if (ctrl.user.password && ctrl.user.password.length < 8) {
        ctrl.errors = {passwordTooShort: true};
        return;
      }

      $mdDialog.show({
        parent: $document[0].body,
        templateUrl: 'app/components/authorization/register/newuser.dialog.html',
        controller: 'NewUserDialogController',
        controllerAs: 'ctrl',
        clickOutsideToClose: true,
        escapeToClose: true
      }).then(function (result) {
        var newUser = {
          username: ctrl.user.username,
          password: ctrl.user.password,
          name: result.name,
          recaptcha: result.recaptcha,
          referrerId: NavigationService.getNextStateReferrerId(),
          redirectUrl: NavigationService.getNextStateRedirectUrl()
        };
        AuthorizationHttpService.register(newUser)
          .success(function () {
            $mdDialog.show({
              controller: 'ModalRegistrationSuccessController',
              controllerAs: 'ctrl',
              templateUrl: 'app/components/authorization/register/registration.success.dialog.html',
              parent: angular.element($document[0].body),
              clickOutsideToClose: true,
              escapeToClose: true
            });
          })
          .error(function (err) {
            $log.error(err);
          });
      })
    }

    // see server application errors.ts module
    var HTTP_CODE__UNKNOWN_USER = 451;
    var HTTP_CODE__SUSPENDED_USER = 452;
    var HTTP_CODE__INVALID_CREDENTIALS = 453;

    function handleLoginError(error) {
      if (error != null) {
        if (error.status == HTTP_CODE__INVALID_CREDENTIALS) {
          ctrl.errors = {
            invalidCredentials: true
          };
          return;
        }

        if (error.status == HTTP_CODE__UNKNOWN_USER) {
          register();
          return;
        }

        if (error.status == HTTP_CODE__SUSPENDED_USER) {
          ctrl.errors = {
            suspendedUser: true
          };
          return;
        }
      }

      new jGrowlNotify().error($translate.instant('ERROR_UNKNOWN'));

      $log.toServer.error('Unknown error while login: ', error);
    }

    function forgotPassword() {
      var tkDialog = new TkDialogFactory();
      tkDialog.controller('ForgotPasswordController');
      tkDialog.templateUrl('app/components/authorization/login/forgotPassword.html');
      tkDialog.show().then(function (result) {
        AuthorizationHttpService.startForgotPasswordProcess(result.email)
          .success(function () {
            new jGrowlNotify().success($translate.instant('FORGOT_PASSWORD_SUCCESS'));
          })
          .error(function (err) {
            $log.error(err);
            new jGrowlNotify().error($translate.instant('ERROR_UNKNOWN'));
          });
      });
    }

    function showTermsAndConditions() {
      $mdDialog.show({
        clickOutsideToClose: true,
        escapeToClose: true,
        templateUrl: 'app/components/account/conditions/conditions.dialog.html',
        controller: 'ConditionsDialogController',
        controllerAs: 'ctrl'
      });
    }
  }
})();
