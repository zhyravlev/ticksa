(function () {
  'use strict';

  /*eslint angular/definedundefined: 0*/

  angular
    .module('ticksaWebapp')
    .service('AuthorizationService', AuthorizationService);

  /** @ngInject */
  function AuthorizationService($q, $log, $window, $interval, $translate, $state,
                                LanguageUIService, NavigationService, AuthorizationTokenService, AccountService,
                                AuthorizationHttpService, UserService, jGrowlNotify,
                                API, ENV, FACEBOOK_APP_ID, CHANNEL_UNKNOWN) {

    $log = $log.getInstance('AuthorizationService');

    var AuthorizationService = this;


    /** ------------- Facebook API ------------- */

    var facebookApi = null;

    AuthorizationService.startWatchingOnFB = function () {
      var FBTimer = $interval(function () {
        if (typeof FB !== 'undefined') {
          FB.init({
            appId: FACEBOOK_APP_ID,
            version: 'v2.5'
          });

          facebookApi = FB;
          $interval.cancel(FBTimer);
        }
      }, 200);
    };

    AuthorizationService.loginFacebook = function () {
      var redirect_uri = generateFacebookRedirectUri();

      $window.location.href = 'https://www.facebook.com/dialog/oauth' +
        '?client_id=' + FACEBOOK_APP_ID +
        '&scope=email,public_profile' +
        '&response_type=code,granted_scopes' +
        '&redirect_uri=' + encodeURI(redirect_uri);
    };

    AuthorizationService.connectFacebook = function () {
      if (!facebookApi) {
        return $q.reject({status: 'api-not-initialized', message: 'FB not initialized', type: 'facebook'});
      }

      var def = $q.defer();
      facebookApi.login(function (response) {
        if (!response || !response.authResponse) {
          def.reject({message: 'No response from API', type: 'facebook'});
        } else {
          def.resolve(AccountService.connectFacebook(response.authResponse.signedRequest));
        }
      }, {scope: 'email'});
      return def.promise;
    };

    /** ------------- End Facebook API ------------- */

    /** ------------- Google API ------------- */

    var googleApi = null;

    AuthorizationService.startWatchingOnGApi = function () {
      var GApiTimer = $interval(function () {
        if (typeof gapi !== 'undefined') {
          gapi.load('auth2', function () {

            googleApi = gapi.auth2.init({
              client_id: '620387887013-bp6r9rk7gn7h32quv5v7anlropddpq98.apps.googleusercontent.com',
              cookie_policy: 'single_host_origin',
              scope: 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
            });
          });
          $interval.cancel(GApiTimer);
        }
      }, 200);
    };

    AuthorizationService.startGoogle = function () {
      if (!googleApi) {
        return $q.reject({status: 'api-not-initialized', message: 'gapi not initialized', type: 'google'});
      }

      var def = $q.defer();
      googleApi.signIn().then(function (data) {
        var idToken = data.getAuthResponse().id_token;
        var referrerId = NavigationService.getNextStateReferrerId();
        AuthorizationHttpService.loginGoogle(idToken, referrerId)
          .then(function (res) {
            def.resolve(res.data);
          })
          .catch(function (error) {
            var message = error.data ? error.data.message : 'no message';
            def.reject({status: error.status, message: message, type: 'google'});
          });
      });
      return def.promise;
    };

    AuthorizationService.connectGoogle = function () {
      if (!googleApi) {
        return $q.reject({status: 'api-not-initialized', message: 'gapi not initialized', type: 'google'});
      }

      var def = $q.defer();
      googleApi.signIn().then(function (data) {
        if (!data) {
          def.reject('Error!');
        } else {
          def.resolve(AccountService.connectGoogle(data.getAuthResponse().id_token));
        }
      });
      return def.promise;
    };

    AuthorizationService.loginGoogle = function () {
      AuthorizationService.startGoogle()
        .then(function (data) {
          return AuthorizationService.finishLogin(data.token);
        })
        .catch(handleGoogleError);
    };

    function handleGoogleError(error) {
      if (error.status == 'api-not-initialized') {
        new jGrowlNotify().error($translate.instant('COMMONS_SERVICE_UNAVAILABLE'));
      }

      $log.toServer.error('Unknown error while login: ', error);
    }

    /** ------------- End Google API ------------- */

    AuthorizationService.login = function (username, password) {
      var def = $q.defer();
      AuthorizationHttpService.login(username, password)
        .success(function (data) {
          def.resolve(data);
        })
        .error(function (error, status) {
          def.reject({status: status, message: error.message});
        });
      return def.promise;
    };

    AuthorizationService.setUser = function (userData) {
      UserService.setUser(userData);
    };

    AuthorizationService.resetUserAndToken = function () {
      UserService.removeUser();
      AuthorizationTokenService.resetToken();
    };

    AuthorizationService.initUserOrGoDiscovery = function () {
      return checkAuthCookieAndInitUser()
        .catch(function () {
          NavigationService.goToDiscoveryPage();
          return $q.resolve();
        });
    };

    AuthorizationService.initUser = function () {
      return checkAuthCookieAndInitUser()
        .catch(function () {
          return $q.resolve();
        });
    };

    AuthorizationService.finishLogin = function (token) {
      AuthorizationTokenService.setToken(token);

      return LanguageUIService.getLanguage()
        .then(function (language) {
          return AccountService.getAccountInfo(language.key)
        })
        .then(function (response) {
          var userData = response.data;
          AuthorizationService.setUser(userData);

          var state = NavigationService.getNextState();
          if (state != null && state.name === 'main.contentAdd') {
            var type = state.params.type;
            var channelId = (state.params.channelId == CHANNEL_UNKNOWN)
              ? userData.defaultChannelId
              : state.params.channelId;
            $state.go('main.contentAdd', {channelId: channelId, type: type});
            return $q.resolve();
          }

          NavigationService.goToNextStateOrProvided('main.contentList');
          return $q.resolve();
        })
        .catch(function (err) {
          $log.error('[login] Err: ', err);
          return $q.reject(err);
        });
    };

    function checkAuthCookieAndInitUser() {
      if (!UserService.isEmpty()) {
        return $q.resolve();
      }

      var token = AuthorizationTokenService.loadAndSetTokenFromCookies();
      if (!token) {
        AuthorizationHttpService.registerLanding();
        return $q.reject();
      }

      return AuthorizationHttpService.refreshToken()
        .then(function (tokenResponse) {
          AuthorizationTokenService.setToken(tokenResponse.data.token);
          AuthorizationHttpService.registerLanding();
          return LanguageUIService.getLanguage();
        })
        .then(function (language) {
          return AccountService.getAccountInfo(language.key)
        })
        .then(function (userResponse) {
          AuthorizationService.setUser(userResponse.data);
          return $q.resolve();
        })
        .catch(function (e) {
          $log.error('[checkAuthCookieAndInitUser] error: ', e);
          AuthorizationService.resetUserAndToken();
          return $q.reject();
        });
    }

    function generateFacebookRedirectUri() {
      var state = NavigationService.getNextState();
      var redirectUri = ENV.URL + '/social/login/facebook';
      var defaultUri = redirectUri + '/content-list';

      if (state === null) {
        return defaultUri;
      }

      if (!state.hasOwnProperty('name') || !state.hasOwnProperty('params')) {
        return defaultUri;
      }

      var facebookLoginRedirectUriToTeaser = function (referrerId, contentId) {
        var template = '/ref-:referrerId-content-:contentId';
        return redirectUri + template.replace(':referrerId', referrerId).replace(':contentId', contentId);
      };

      var facebookLoginRedirectUriToContentAdd = function (contentType) {
        var template = '/content-add-type-:contentType';
        return redirectUri + template.replace(':contentType', contentType);
      };

      switch (state.name) {

        case 'main.contentAdd':
          var contentType = state.params.type == null ? 'no' : state.params.type;
          return facebookLoginRedirectUriToContentAdd(contentType);

        case 'base.teaser':
          var refId = state.params.referrerId == null ? 'no' : state.params.referrerId;
          var conId = state.params.contentId == null ? 'no' : state.params.contentId;
          return facebookLoginRedirectUriToTeaser(refId, conId);

        default:
          return defaultUri;
      }
    }
  }
})();
