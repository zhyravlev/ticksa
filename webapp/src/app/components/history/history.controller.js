(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('HistoryController', HistoryController);

  /** @ngInject */
  function HistoryController($log, $state,
                             lodash, AccountService, LinkContentService, HeaderTitleService) {

    $log = $log.getInstance('HistoryController');

    var ctrl = this;

    HeaderTitleService.setTranslatedTitle('COMMONS_HISTORY');

    ctrl.goToContent = function (content) {
      LinkContentService.openBlankWindowIfNeeded(content);
      $state.go('main.contentView', {channelId: content.channel.id, contentId: content.id});
    };

    AccountService.getContentHistory()
      .success(function (history, status) {
        ctrl.historyPages = {};
        lodash.forEach(history, function (item) {
          var day = item.createdAt.substring(0, item.createdAt.indexOf('T'));
          if (ctrl.historyPages[day]) {
            ctrl.historyPages[day].push(item);
          } else {
            ctrl.historyPages[day] = [item];
          }
        })
      })
      .error(function (err) {
        $log.error(err);
      });
  }
})();
