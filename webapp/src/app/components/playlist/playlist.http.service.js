(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('PlaylistHttpService', PlaylistHttpService);

  /** @ngInject */
  function PlaylistHttpService(API, $http) {
    return {
      postUserPlaylist: postUserPlaylist,
      getUserPlaylists: getUserPlaylists,
      updateUserPlaylist: updateUserPlaylist,
      deleteUserPlaylist: deleteUserPlaylist,
      getUserPlaylist: getUserPlaylist,
      getPlaylist: getPlaylist,
      postDecodePlaylistId: postDecodePlaylistId
    };

    function postUserPlaylist(playlist) {
      return $http.post(API.URL + '/user/playlists', playlist);
    }

    function getUserPlaylists() {
      return $http.get(API.URL + '/user/playlists');
    }

    function updateUserPlaylist(playlistId, playlist) {
      return $http.put(API.URL + '/user/playlists/' + playlistId, playlist);
    }

    function deleteUserPlaylist(playlistId) {
      return $http.delete(API.URL + '/user/playlists/' + playlistId);
    }

    function getUserPlaylist(playlistId) {
      return $http.get(API.URL + '/user/playlists/' + playlistId);
    }

    function getPlaylist(playlistId) {
      return $http.get(API.URL + '/playlists/' + playlistId);
    }

    function postDecodePlaylistId(encodedPlaylistId) {
      return $http.post(API.URL + '/playlists/decode', {encodedPlaylistId: encodedPlaylistId});
    }
  }

})();