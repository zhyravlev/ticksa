(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkPlaylistSettings', tkPlaylistSettings);

  /** @ngInject */
  function tkPlaylistSettings($log, lodash,
                              CommonsUIService, LibraryService, DialogService, ContentService, CONTENT_TYPES) {

    $log = $log.getInstance('tkPlaylistSettings');

    return {
      restrict: 'E',
      scope: {
        playlist: '=',
        playlistForm: '='
      },
      templateUrl: 'app/components/playlist/playlist.settings.directive/playlist.settings.directive.html',
      controllerAs: 'ctrl',
      controller: function ($scope) {

        var ctrl = this;
        ctrl.CONTENT_TYPES = CONTENT_TYPES;
        ctrl.images = [];
        ctrl.DialogService = DialogService;
        ctrl.formSubmit = false;
        ctrl.playlistForm = $scope.playlistForm;
        ctrl.playlist = $scope.playlist;
        ctrl.selectCoverImage = selectCoverImage;
        ctrl.selectBackgroundImage = selectBackgroundImage;

        ctrl.searchText = '';
        ctrl.order = [];
        ctrl.sortableOptions = {
          handle: '.sort-handle',
          animation: 150,
          ghostClass: 'ghost',
          onSort: function (sort) {
            if (!sort.models) {
              return;
            }
            ctrl.order = lodash.pluck(sort.models, 'id');
          }
        };
        ctrl.removeContent = removeContent;
        ctrl.querySearch = querySearch;
        ctrl.selectedItemChange = selectedItemChange;

        LibraryService.getImages()
          .success(function (data) {
            ctrl.images = data
          })
          .error(function (err) {
            $log.error(err)
          });

        function selectCoverImage(event) {
          CommonsUIService.selectSingleImage(event, ctrl.images, $scope.cover)
            .then(function (data) {
              ctrl.playlist.coverImage = {
                id: data.id,
                key: data.key
              };
            });
        }

        function selectBackgroundImage(event) {
          CommonsUIService.selectSingleImage(event, ctrl.images, $scope.background)
            .then(function (data) {

              ctrl.playlist.backgroundImage = {
                id: data.id,
                key: data.key
              };
            })
        }

        function removeContent(content) {
          var index = ctrl.playlist.contents.indexOf(content);
          ctrl.playlist.contents.splice(index, 1);
        }

        function querySearch(query) {
          return ContentService.getContentSearch(query).then(function (res) {

            // Remove duplicates from response
            lodash.remove(res.data, function (n) {
              return lodash.find(ctrl.playlist.contents, function (o) {
                return o.id == n.id;
              });
            });

            return res.data;
          });
        }

        function selectedItemChange(item) {
          if (lodash.isEmpty(item)) {
            return;
          }
          ctrl.searchText = '';
          ctrl.playlist.contents.unshift(item);
        }
      }
    }
  }
})();
