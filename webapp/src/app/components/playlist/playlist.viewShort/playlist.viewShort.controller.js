(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('PlaylistViewShortController', PlaylistViewShortController);

  /** @ngInject */
  function PlaylistViewShortController($log, $state, $stateParams, PlaylistHttpService) {
    $log = $log.getInstance('PlaylistViewShortController');

    PlaylistHttpService.postDecodePlaylistId($stateParams.encodedPlaylistId)
      .then(function (res) {
        $state.go('main.playlist.view', {playlistId: res.data.playlistId});
      })
      .catch(function (err) {
        $log.error(err);
        $state.go('main.contentList');
      });
  }
})();
