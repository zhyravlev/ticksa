(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.playlist', {
        template: '<ui-view></ui-view>'
      })

      // Short playlist list link. Available only for registered users
      .state('main.playlist.viewShort', {
        url: '/p/{encodedPlaylistId}',
        templateUrl: 'app/components/playlist/playlist.viewShort/playlist.viewShort.html',
        controller: 'PlaylistViewShortController',
        controllerAs: 'ctrl'
      })

      // Playlist page
      .state('main.playlist.view', {
        url: '/playlists/{playlistId:int}',
        templateUrl: 'app/components/playlist/playlist.view/playlist.view.html',
        controller: 'PlaylistViewController',
        controllerAs: 'ctrl'
      })

      // Playlist creation page
      .state('main.playlist.add', {
        url: '/playlists/add',
        templateUrl: 'app/components/playlist/playlist.add/playlist.add.html',
        controller: 'PlaylistAddController',
        controllerAs: 'ctrl'
      })

      // Playlist edit page
      .state('main.playlist.edit', {
        url: '/playlists/{playlistId:int}/edit',
        templateUrl: 'app/components/playlist/playlist.edit/playlist.edit.html',
        controller: 'PlaylistEditController',
        controllerAs: 'ctrl'
      })
  }
})();
