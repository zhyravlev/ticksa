(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('PlaylistAddController', PlaylistAddController);

  /** @ngInject */
  function PlaylistAddController($log, $translate, $state, lodash, $rootScope,
                                 HeaderTitleService, NavigationService, PlaylistHttpService, jGrowlNotify) {

    $log = $log.getInstance('PlaylistAddController');
    HeaderTitleService.setTranslatedTitle('COMMONS_PLAYLIST_ADD');

    var ctrl = this;

    ctrl.playlist = {
      name: null,
      coverImage: null,
      backgroundImage: null,
      contents: [],
      published: true
    };

    ctrl.addPlaylist = addPlaylist;
    ctrl.cancel = cancel;

    function addPlaylist(form) {
      ctrl.formSubmit = true;
      if (form.$valid) {
        var data = {
          name: ctrl.playlist.name,
          coverImageId: (ctrl.playlist.coverImage != null) ? ctrl.playlist.coverImage.id : null,
          backgroundImageId: (ctrl.playlist.backgroundImage != null) ? ctrl.playlist.backgroundImage.id : null,
          contentsIds: lodash.map(ctrl.playlist.contents, function (content) {
            return content.id
          }),
          published: ctrl.playlist.published
        };

        PlaylistHttpService.postUserPlaylist(data)
          .then(function (res) {
            $rootScope.$broadcast('UserPlaylistsChange', {});
            if (data.published) {
              $state.go('main.playlist.view', {playlistId: res.data.id})
            } else {
              NavigationService.goToPreviousStateOrProvided('main.contentList', null);
            }
          })
          .catch(function (err) {
            $log.error(err);
            new jGrowlNotify().error($translate.instant('ERROR_UNKNOWN'));
          })
      }
    }

    function cancel() {
      NavigationService.goToPreviousStateOrProvided('main.contentList', null);
    }
  }
})();
