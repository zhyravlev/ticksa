(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('PlaylistViewController', PlaylistViewController);

  /** @ngInject */
  function PlaylistViewController($log, $state, $stateParams, $rootScope,
                                  PlaylistHttpService, HeaderTitleService, EVENTS) {

    $log = $log.getInstance('PlaylistViewController');

    var ctrl = this;
    ctrl.loadingInProgress = false;

    PlaylistHttpService.getPlaylist($stateParams.playlistId)
      .then(function (res) {
        ctrl.loadingInProgress = false;
        ctrl.playlist = {
          id: res.data.id,
          name: res.data.name,
          coverImage: res.data.coverImage,
          backgroundImage: res.data.backgroundImage,
          published: res.data.published,
          contents: res.data.contents
        };

        HeaderTitleService.setCustomTitle(ctrl.playlist.name);
        HeaderTitleService.setPlaylistId(ctrl.playlist.id);

        if (ctrl.playlist.backgroundImage !== null) {
          $rootScope.$broadcast(EVENTS.CHANGE_CHANNEL_BACKGROUND, {
            key: ctrl.playlist.backgroundImage.key
          })
        }
      })
      .catch(function (err) {
        if (err.status == 404) {
          $state.go('main.contentList');
        }
        $log.log(err);
      });
  }
})();
