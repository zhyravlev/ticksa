(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('PlaylistEditController', PlaylistEditController);

  /** @ngInject */
  function PlaylistEditController($log, $translate, $stateParams, $state, lodash, $rootScope,
                                  HeaderTitleService, NavigationService, PlaylistHttpService, ModalService, jGrowlNotify) {

    $log = $log.getInstance('PlaylistEditController');
    HeaderTitleService.setTranslatedTitle('COMMONS_PLAYLIST_EDIT');

    var ctrl = this;
    var playlistId = $stateParams.playlistId;

    ctrl.playlist = null;
    ctrl.updatePlaylist = updatePlaylist;
    ctrl.deactivatePlaylist = deactivatePlaylist;
    ctrl.cancel = cancel;

    PlaylistHttpService.getUserPlaylist(playlistId)
      .then(function (res) {
        ctrl.playlist = {
          name: res.data.name,
          coverImage: res.data.coverImage,
          backgroundImage: res.data.backgroundImage,
          published: res.data.published,
          contents: res.data.contents
        };
      })
      .catch(function (err) {
        $log.error(err);
        new jGrowlNotify().error($translate.instant('ERROR_UNKNOWN'));
        NavigationService.goToPreviousStateOrProvided('main.contentList', null)
      });

    function updatePlaylist(form) {
      ctrl.formSubmit = true;
      if (form.$valid) {

        var data = {
          name: ctrl.playlist.name,
          coverImageId: (ctrl.playlist.coverImage != null) ? ctrl.playlist.coverImage.id : null,
          backgroundImageId: (ctrl.playlist.backgroundImage != null) ? ctrl.playlist.backgroundImage.id : null,
          contentsIds: lodash.map(ctrl.playlist.contents, function (content) {
            return content.id
          }),
          published: ctrl.playlist.published
        };

        PlaylistHttpService.updateUserPlaylist(playlistId, data)
          .success(function () {
            if (data.published) {
              $state.go('main.playlist.view', {playlistId: playlistId})
            } else {
              NavigationService.goToPreviousStateOrProvided('main.contentList', null);
            }
          })
          .error(function (err) {
            $log.error(err);
            new jGrowlNotify().error($translate.instant('ERROR_UNKNOWN'))
          })
      }
    }

    function cancel() {
      NavigationService.goToPreviousStateOrProvided('main.contentList', null)
    }

    function deactivatePlaylist(event) {
      ModalService.confirmationBuilder()
        .confirmationTargetEvent(event)
        .confirmationClickOutsideToClose()
        .confirmationTexts({
          title: $translate.instant('PLAYLIST_DEACTIVATE'),
          body: $translate.instant('PLAYLIST_DEACTIVATE_BODY'),
          cancel: $translate.instant('COMMONS_CANCEL'),
          confirm: $translate.instant('CONTENT_MENU__DELETE')
        })
        .confirmationShowModal()
        .then(function () {
          PlaylistHttpService.deleteUserPlaylist(playlistId)
            .then(function () {
              $rootScope.$broadcast('UserPlaylistsChange', {});
              NavigationService.goToPreviousStateOrProvided('main.contentList', null)
            })
            .catch(function(err) {
              $log.error(err);
              new jGrowlNotify().error($translate.instant('ERROR_UNKNOWN'));
            });
        })
    }
  }
})();