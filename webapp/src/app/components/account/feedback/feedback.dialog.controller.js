(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('FeedbackDialogController', FeedbackController);

  /** @ngInject */
  function FeedbackController($rootScope, $log, $mdDialog, $translate, SelectionsService, AdminService, jGrowlNotify, EVENTS) {

    $log = $log.getInstance('FeedbackController');

    var ctrl = this;

    ctrl.feedbackTypes = SelectionsService.getFeedbackTypes();
    ctrl.submitFeedback = submitFeedback;
    ctrl.close = close;

    function close() {
      $mdDialog.cancel();
    }

    function submitFeedback(form) {
      ctrl.attemptedSave = true;
      
      if (form.$valid) {
        return AdminService.postFeedback(ctrl.type, ctrl.text)
          .success(function () {
            $mdDialog.hide();
            new jGrowlNotify().success($translate.instant('FEEDBACK_POSTED_TOAST'));
          })
          .error(function (err) {
            $log.error(err);
            $mdDialog.hide();
            new jGrowlNotify().error($translate.instant('ERROR_UNKNOWN'));
          });
      }

      $rootScope.$broadcast(EVENTS.ANIMATE_DIALOG);
    }
  }
})();
