(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('AccountService', AccountService);

  /** @ngInject */
  function AccountService(API, $http) {

    var AccountService = this;

    AccountService.savePhoneAndSendVerificationCode = function (phone) {
      return $http.post(API.URL + '/account/save-phone-and-send-verification-code', {phone: phone});
    };

    AccountService.verifyPhone = function (code) {
      return $http.post(API.URL + '/account/verify-phone?code=' + code, null);
    };

    AccountService.connectGoogle = function (idToken) {
      return $http.post(API.URL + '/account/connect-social/google', {idToken: idToken});
    };

    AccountService.disconnectGoogle = function () {
      return $http.post(API.URL + '/account/connect-social/google-disconnect', null);
    };

    AccountService.disconnectFacebook = function () {
      return $http.post(API.URL + '/account/connect-social/facebook-disconnect', null);
    };

    AccountService.getCurrencies = function () {
      return $http.get(API.URL + '/account/currencies');
    };

    AccountService.connectFacebook = function (signedRequest) {
      return $http.post(API.URL + '/account/connect-social/facebook', {signedRequest: signedRequest});
    };

    AccountService.changePassword = function (oldPassword, newPassword) {
      return $http.post(API.URL + '/account/change-password', {oldPassword: oldPassword, newPassword: newPassword});
    };

    AccountService.terminateAccount = function () {
      return $http.delete(API.URL + '/account/terminate');
    };

    AccountService.updateProfile = function (profile) {
      return $http.put(API.URL + '/account/profile/update', profile);
    };

    AccountService.updatePreferences = function (preferences) {
      return $http.put(API.URL + '/account/preferences/update', {preferences: preferences});
    };

    AccountService.getAccountInfo = function (language) {
      return $http.get(API.URL + '/account/info', {params: {language: language}})
    };

    AccountService.getContentHistory = function () {
      return $http.get(API.URL + '/history')
    };
  }
})();
