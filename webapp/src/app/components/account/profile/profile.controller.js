(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ProfileController', ProfileController);

  /** @ngInject */
  function ProfileController(translationsLanguage /*it is resolved in state*/,
                             TkDialogFactory, $scope, $log, $rootScope, $translate, $timeout,
                             thumbnailsS3CdnFilter, SOCIAL_TYPES, UI_LANGUAGES,
                             CountryService, LibraryService, AuthorizationService,
                             AccountService, NavigationService, ModalService, HeaderTitleService, UserService,
                             LanguageUIService, jGrowlNotify) {

    $log = $log.getInstance('ProfileController');

    var ctrl = this;

    /****************************************************
     +                    Initialize                    +
     ****************************************************/

    HeaderTitleService.setTranslatedTitle('HEADER_TITLE_PROFILE');

    ctrl.verificationCodeExists = UserService.verificationCodeExists();
    ctrl.isPhoneVerified = UserService.isPhoneVerified();
    ctrl.email = UserService.getEmail();

    var name = UserService.getName();
    var phone = UserService.getPhone();

    ctrl.profile = {
      name: name ? name : '',
      phone: phone ? phone : '',
      profileImage: UserService.getProfileImage(),
      currencyId: UserService.getCurrencyId(),
      autoAuthorize: UserService.getAutoAuthorize(),
      defaultChannelId: UserService.getDefaultChannelId(),
      autoAuthorizeMaxTicks: UserService.getAutoAuthorizeMaxTicks(),
      paymentMethod: UserService.getPaymentMethod(),
      profileImageUrl: UserService.getProfileImageUrl()
    };

    var unregisterCountryWatcher = angular.noop;

    CountryService.getCountries()
      .then(function (data) {
        ctrl.countries = data;
        ctrl.selectedCountry = UserService.get('country');
      })
      .catch(function (err) {
        $log.error(err);
      });

    AccountService.getCurrencies()
      .success(function (data) {
        ctrl.currencies = data;
        unregisterCountryWatcher = $scope.$watch('ctrl.selectedCountry', function (newVal) {
          if (newVal && !ctrl.profile.currencyId) {
            ctrl.profile.currencyId = newVal.currencyId;
          }
        });
      })
      .error(function (err) {
        $log.error(err);
      });

    LibraryService.getImages()
      .success(function (data) {
        ctrl.images = data;
      })
      .error(function (err) {
        $log.error(err);
      });

    /********************************************************
     +                    Destroy events                    +
     ********************************************************/

    $scope.$on('$destroy', function () {
      unregisterCountryWatcher();
    });

    /********************************************************
     +                    Change country                    +
     ********************************************************/

    ctrl.searchTextCountry = '';
    ctrl.querySearchCountry = function () {
      if (ctrl.searchTextCountry.length === 0) {
        return ctrl.countries;
      }

      return ctrl.countries.filter(function (country) {
        return country.name.toLowerCase().indexOf(ctrl.searchTextCountry.toLowerCase()) != -1
      })
    };

    /*********************************************************
     +                    Change language                    +
     *********************************************************/

    var userLanguageKey = UserService.getLanguageKey();
    var uiLanguageKey = (userLanguageKey != null) ? userLanguageKey : translationsLanguage;

    ctrl.uiLanguages = UI_LANGUAGES;
    ctrl.uiLanguage = LanguageUIService.constructUILanguageObject(uiLanguageKey);

    ctrl.changeUILanguage = function () {
      LanguageUIService.setLanguage(ctrl.uiLanguage.id);
    };

    /************************************************************
     +                    Verification phone                    +
     ************************************************************/

    ctrl.selectVerificationCode = false;
    ctrl.verificationWait = false;

    ctrl.savePhoneAndSendVerificationCode = function () {
      if (angular.isString(ctrl.profile.phone)) {
        ctrl.verificationWait = true;
        AccountService.savePhoneAndSendVerificationCode(ctrl.profile.phone)
          .success(function () {
            ctrl.verificationCodeExists = true;
            ctrl.verificationWait = false;
            $timeout(function () {
              ctrl.selectVerificationCode = true;
            }, 100);
          })
          .error(function (err) {
            new jGrowlNotify().error($translate.instant('ERROR_PHONE_ALREADY_VERIFIED'));
            // Todo: Throw correct error!
            $log.error(err);
            ctrl.verificationWait = false;
          });
      }
    };
    ctrl.verifyPhone = function () {
      if (angular.isString(ctrl.verificationCode)) {
        AccountService.verifyPhone(ctrl.verificationCode)
          .success(function (data) {
            ctrl.isPhoneVerified = true;
            ctrl.verificationCodeExists = false;
            if (data.firstTime) {
              UserService.pushBalance(10);
              new jGrowlNotify().success($translate.instant('PROFILE_PHONE_VERIFIED_TOAST'));
            }
          })
          .error(function (err) {
            // Todo: Throw correct error!
            $log.error(err);
          });
      }
    };

    /**************************************************************
     +                    Social API functions                    +
     **************************************************************/

    ctrl.googleConnected = false;
    ctrl.facebookConnected = false;

    UserService.get('socialAccounts').forEach(function (social) {
      switch (social.type) {
        case SOCIAL_TYPES.GOOGLE:
          ctrl.googleConnected = true;
          break;
        case SOCIAL_TYPES.FACEBOOK:
          ctrl.facebookConnected = true;
          break;
      }
    });

    ctrl.connectGoogle = function () {
      AuthorizationService.connectGoogle()
        .then(function () {
          ctrl.googleConnected = true;
        })
        .catch(socialConnectErrorHandler);
    };

    ctrl.connectFacebook = function () {
      AuthorizationService.connectFacebook()
        .then(function () {
          ctrl.facebookConnected = true;
        })
        .catch(socialConnectErrorHandler);
    };

    ctrl.disconnectFacebook = function () {
      AccountService.disconnectFacebook()
        .then(function () {
          ctrl.facebookConnected = false;
        })
        .catch(socialDisconnectErrorHandler)
    };

    ctrl.disconnectGoogle = function disconnectGoogle() {
      AccountService.disconnectGoogle()
        .then(function () {
          ctrl.googleConnected = false;
        })
        .catch(socialDisconnectErrorHandler)
    };

    function socialConnectErrorHandler(err) {
      switch (err.status) {
        case 400:
          new jGrowlNotify().error($translate.instant('PROFILE_SOCIAL_ACCOUNT_ALREADY_TIED'));
          break;
        default:
          new jGrowlNotify().error($translate.instant('ERROR_UNKNOWN'));
      }

      $log.error(err);
    }

    function socialDisconnectErrorHandler(err) {
      new jGrowlNotify().error($translate.instant('ERROR_UNKNOWN'));
      $log.error(err);
    }

    /********************************************************
     +                    Remove account                    +
     ********************************************************/

    ctrl.terminateAccount = function (event) {
      ModalService.confirmationBuilder()
        .confirmationTargetEvent(event)
        .confirmationTexts({
          title: $translate.instant('DELETE_ACCOUNT_TITLE'),
          body: $translate.instant('DELETE_ACCOUNT_BODY'),
          cancel: $translate.instant('COMMONS_CANCEL'),
          confirm: $translate.instant('DELETE_ACCOUNT_CONFIRM')
        })
        .confirmationShowModal()
        .then(function () {
          AccountService.terminateAccount()
            .success(function () {
              AuthorizationService.resetUserAndToken();
              NavigationService.goToDiscoveryPage();
            })
            .error(function (error) {
              $log.error(error);
            });
        })
    };

    /**************************************************************
     +                    Select profile image                    +
     *************************************************************/

    ctrl.selectProfileImage = function () {
      LibraryService.getImages()
        .success(function (data) {
          var tkDialog = new TkDialogFactory();
          tkDialog.controller('ImagePickerModalController');
          tkDialog.templateUrl('app/components/library/image/image.picker.modal.html');
          tkDialog.locals({images: data, multiple: false});
          tkDialog.show().then(function (data) {
            if (angular.isObject(data.selectedImage)) {
              ctrl.profile.profileImageUrl = thumbnailsS3CdnFilter(data.selectedImage.key, 120, 120);
              ctrl.profile.profileImage = {
                id: data.selectedImage.id,
                key: data.selectedImage.key
              };
            } else {
              ctrl.profile.profileImageUrl = UserService.get('socialImageUrl');
              ctrl.profile.profileImage = null;
            }
          });
        });
    };

    /*****************************************************
     +                    Save changes                   +
     *****************************************************/

    ctrl.updateProfile = function (profileForm) {
      ctrl.formSubmit = true;
      if (profileForm.$valid) {
        ctrl.profile.language = ctrl.uiLanguage.id;
        ctrl.profile.countryId = ctrl.selectedCountry ? ctrl.selectedCountry.id : null;
        AccountService.updateProfile(ctrl.profile)
          .then(function () {
            $rootScope.$broadcast('AccountInfoChange');
            NavigationService.goToPreviousStateOrProvided('main.contentList');
          })
          .catch(function (error) {
            $log.error(error);
          })
      }
    };
  }
})();
