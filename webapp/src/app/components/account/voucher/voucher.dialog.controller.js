(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('VoucherDialogController', VoucherDialogController);

  /** @ngInject */
  function VoucherDialogController($log, $mdDialog, $rootScope, VoucherService) {

    $log = $log.getInstance('VoucherDialogController');

    var ctrl = this;

    ctrl.tickserRights = {
      processing: true,
      canCheck: false,
      banned: false,
      alreadyMaxVouchers: false
    };

    ctrl.voucher = {
      code: '',
      ticks: 0,
      status: {
        validCode: false,
        invalidCode: false,
        successfullyApplied: false,
        alreadyApplied: false
      }
    };

    ctrl.checkVoucher = checkVoucher;
    ctrl.applyVoucher = applyVoucher;
    ctrl.shouldPreventRepeats = shouldPreventRepeats;
    ctrl.resetVoucherStatus = resetVoucherStatus;
    ctrl.close = close;

    function close() {
      $mdDialog.cancel();
    }

    VoucherService.getTickserRights()
      .success(function (data, status) {
        ctrl.tickserRights.processing = false;
        ctrl.tickserRights.canCheck = data.canCheck;
        ctrl.tickserRights.banned = data.banned;
        ctrl.tickserRights.alreadyMaxVouchers = data.alreadyMaxVouchers;
      })
      .error(function (err) {
        $log.log(err);
      });

    function checkVoucher(form) {
      ctrl.attemptedSave = true;
      ctrl.working = true;
      if (form.$valid) {
        VoucherService.checkVoucher(ctrl.voucher.code)
          .success(function (data, status) {
            ctrl.working = false;
            updateRightsAndStatus(data);
          })
          .error(function (err) {
            ctrl.working = false;
            $log.log(err);
          });
      } else {
        ctrl.working = false;
      }
    }

    function applyVoucher(form) {
      ctrl.attemptedSave = true;
      ctrl.working = true;
      if (form.$valid && ctrl.voucher.status.validCode) {
        VoucherService.applyVoucher(ctrl.voucher.code)
          .success(function (data, status) {
            ctrl.working = false;
            updateRightsAndStatus(data);
            $rootScope.$broadcast('TicksTransaction', {});
          })
          .error(function (err) {
            ctrl.working = false;
            $log.log(err);
          });
      } else {
        ctrl.working = false;
      }
    }

    function shouldPreventRepeats() {
      return ctrl.voucher.status.validCode == true
        || ctrl.voucher.status.alreadyApplied == true
        || ctrl.voucher.status.successfullyApplied == true;
    }

    function resetVoucherStatus() {
      return resetStatus();
    }

    function resetRights() {
      ctrl.tickserRights.processing = false;
      ctrl.tickserRights.canCheck = false;
      ctrl.tickserRights.banned = false;
      ctrl.tickserRights.alreadyMaxVouchers = false;
    }

    function resetStatus() {
      ctrl.voucher.status.validCode = false;
      ctrl.voucher.status.invalidCode = false;
      ctrl.voucher.status.successfullyApplied = false;
      ctrl.voucher.status.alreadyApplied = false;
    }

    function updateRightsAndStatus(data) {
      resetRights();
      resetStatus();

      if (data.rights.banned) {
        ctrl.tickserRights.banned = true;

        // if banned because this last attempt to check code
        if (data.status.invalidCode) {
          ctrl.voucher.status.invalidCode = true;
        }
      }

      else if (data.rights.alreadyMaxVouchers) {
        ctrl.tickserRights.alreadyMaxVouchers = true;

        // if had max vouchers because of this last attempt to apply voucher
        if (data.status.successfullyApplied) {
          ctrl.voucher.status.validCode = false;  // to prevent user click 'apply' second time for same code
          ctrl.voucher.status.successfullyApplied = true;
          ctrl.voucher.ticks = data.ticks;
        }
      }

      else {
        ctrl.tickserRights.canCheck = true;

        if (data.status.alreadyApplied) {
          ctrl.voucher.status.alreadyApplied = true;
        }

        else if (data.status.invalidCode) {
          ctrl.voucher.status.invalidCode = true;
        }

        else if (data.status.validCode) {
          ctrl.voucher.status.validCode = true;
          ctrl.voucher.ticks = data.ticks;
        }

        else if (data.status.successfullyApplied) {
          ctrl.voucher.status.validCode = false;  // to prevent user click 'apply' second time for same code
          ctrl.voucher.status.successfullyApplied = true;
          ctrl.voucher.ticks = data.ticks;
        }
      }
    }
  }
})();
