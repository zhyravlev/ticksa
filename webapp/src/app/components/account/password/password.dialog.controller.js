(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('PasswordDialogController', PasswordDialogController);

  /** @ngInject */
  function PasswordDialogController($rootScope, $mdDialog, $translate, AccountService, jGrowlNotify, UserService, EVENTS) {

    var ctrl = this;

    ctrl.oldPassword = '';
    ctrl.isOldPasswordExists = UserService.isOldPasswordExists();

    ctrl.changePassword = changePassword;
    ctrl.close = close;

    function close() {
      $mdDialog.cancel();
    }

    function changePassword(form) {
      if (ctrl.isOldPasswordExists) {
        form.oldPassword.$setValidity("passMatch", true);
      }

      ctrl.attemptedSave = true;

      if (form.$valid) {
        return AccountService.changePassword(ctrl.oldPassword, ctrl.newPassword1)
          .success(function () {
            UserService.setOldPasswordExists();
            $mdDialog.hide();
            new jGrowlNotify().success($translate.instant('NEW_PASSWORD_SUCCESS'));
          })
          .error(function (data, status) {
            if (status == 403) {
              form.oldPassword.$setValidity("passMatch", false);
              animateDialog();
            }
          });
      }

      animateDialog();
    }

    function animateDialog() {
      $rootScope.$broadcast(EVENTS.ANIMATE_DIALOG);
    }
  }
})();
