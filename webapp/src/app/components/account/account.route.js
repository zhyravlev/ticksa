(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.account', {
        url: '/account',
        template: '<ui-view flex></ui-view>'
      })
      .state('main.account.profile', {
        url: '/profile',
        templateUrl: 'app/components/account/profile/profile.html',
        controller: 'ProfileController',
        controllerAs: 'ctrl',
        resolve: {
          // here we wait until translations became available
          translationsLanguage: function (LanguageUIService) {
            return LanguageUIService.getLanguage();
          }
        }
      });
  }
})();
