(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ConditionsDialogController', ConditionsDialogController);

  /** @ngInject */
  function ConditionsDialogController($mdDialog, tkAssetsCdnFilter) {

    var ctrl = this;

    ctrl.tosURL = tkAssetsCdnFilter('assets/html/tos.html');

    ctrl.close = function () {
      $mdDialog.hide()
    }
  }
})();