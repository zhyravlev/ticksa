(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('FinancialPaymentStartController', FinancialPaymentStartController);

  /** @ngInject */
  function FinancialPaymentStartController($log, $window, $translate, $stateParams, $state, lodash, $rootScope, $sce, $timeout,
                                           CountryService, SelectionsService, CashIOService, LemonwayHttpService,
                                           HeaderTitleService, UserService) {

    $log = $log.getInstance('FinancialPaymentStartController');

    var ctrl = this;

    HeaderTitleService.setTranslatedTitle('COMMONS_TOP_UP_ACCOUNT');

    init();

    function init() {
      ctrl.showRedirectMessage = $stateParams.showRedirectMessage;

      ctrl.currentUser = UserService.getUser();
      ctrl.lemonwayForm = null;
      ctrl.amounts = [3, 5, 10, 15, 20, 25];
      ctrl.cardTypes = ['VISA', 'MASTERCARD', 'CB'];
      ctrl.creditCardLoading = true;
      ctrl.lemonwayWallet = null;
      ctrl.errors = {
        country: false,
        walletDetailsError: false
      };
      ctrl.infinCountries = SelectionsService.getInfinCountries();
      ctrl.paymentMethods = SelectionsService.getPaymentMethods();
      ctrl.moneyAmount = 1;
      ctrl.data = {
        infinAmount: 1,
        paypalAmount: 3,
        paymentMethod: ctrl.currentUser.financialAccount.paymentMethod || null
      };
      ctrl.cardData = {
        moneyAmount: 3,
        cardType: null,
        card: null,
        firstName: null,
        lastName: null
      };

      if (ctrl.currentUser.isPhoneVerified) {
        CountryService.getCountries()
          .then(function (countries) {
            var phoneCountry = null;
            var phoneStripLeadingZeroes = parseInt(ctrl.currentUser.phone).toString();


            lodash.forEach(countries, function (country) {
              var matchingCountries = lodash.filter(ctrl.infinCountries, function (infinCountry) {
                return infinCountry.codeA2 == country.codeA2
              });
              if (matchingCountries.length > 0) {
                if (phoneStripLeadingZeroes.indexOf(country.dialingCode) == 0) {
                  phoneCountry = matchingCountries[0];
                  return false;
                }
              }
            });
            if (phoneCountry == null) {
              ctrl.infinCountryNotSupportedError = true;
            } else {
              ctrl.data.infinCountry = phoneCountry;
              ctrl.data.infinAmount = phoneCountry.tariffs[0];
              ctrl.infinValid = true;
            }
          })
          .catch(function (err) {
            $log.log(err);
          })
      } else {
        ctrl.infinNoVerifiedPhoneError = true;
      }

      LemonwayHttpService.getWalletDetails().then(function (res) {
        ctrl.lemonwayWallet = res.data;
        if (!ctrl.lemonwayWallet.cards) {
          ctrl.lemonwayWallet.cards = [];
        }
        ctrl.lemonwayWallet.cards.reverse();
        ctrl.lemonwayWallet.cards.push({id: null, num: $translate.instant('FINANCIAL_START_CREDIT_CARD_NEW_CARD')});

        ctrl.creditCardLoading = false;
      }).catch(function (err) {
        ctrl.creditCardLoading = false;
        ctrl.errors.walletDetailsError = err.status == 400; // because: wallet.status == 'ERROR'
      })
    }

    ctrl.loadThroughInfin = loadThroughInfin;
    ctrl.loadThroughPayPal = loadThroughPayPal;
    ctrl.loadThroughCreditCard = loadThroughCreditCard;
    ctrl.goToEditProfile = goToEditProfile;

    function goToEditProfile() {
      return $state.go('main.account.profile');
    }

    function loadThroughInfin(form) {
      ctrl.attemptedSave = true;
      ctrl.working = true;
      if (form.$valid) {
        CashIOService.startInfinCashIn(ctrl.data.infinAmount, ctrl.data.infinCountry.codeA2)
          .success(function (data, status) {
            $window.location = data.url;
          })
          .error(function (err, status) {
            ctrl.working = false;
            $log.log(err);
          });
      } else {
        ctrl.working = false;
      }
    }

    function loadThroughPayPal(form) {
      ctrl.attemptedSave = true;
      if (form.$valid) {
        ctrl.working = true;
        CashIOService.startPayPalCashIn(ctrl.data.paypalAmount)
          .success(function (data, status) {
            $window.location = data.url;
          })
          .error(function (err, status) {
            ctrl.working = false;
            $log.log(err);
            if (status == 400) {
              ctrl.errors.country = true;
            }
          });
      } else {
        ctrl.working = false;
      }
    }

    function loadThroughCreditCard(form) {
      ctrl.attemptedSave = true;
      ctrl.creditCardLoading = true;

      if (!form.$valid) {
        ctrl.creditCardWorking = false;
      }

      ctrl.creditCardWorking = true;
      CashIOService.startLemonwayCashIn(ctrl.cardData).then(function (res) {
        var result = res.data;
        if (!result) {
          return;
        }

        if (result.withCard) {
          $state.go('main.financial.paymentCallback', {transactionId: result.transactionId});
          $rootScope.$broadcast('TicksTransaction', {});
          ctrl.creditCardLoading = false;
          return;
        }

        if (!result.cardForm) {
          $state.go('main.financial.paymentCallback', {error: true});
          ctrl.creditCardLoading = false;
          return;
        }

        // pass LW form into special directive, that tries to click on proper button or shows form to user
        ctrl.lemonwayForm = result.cardForm;

      }).catch(function (err) {
        ctrl.creditCardLoading = false;
        ctrl.creditCardWorking = false;
        $log.error('[startLemonwayCashIn] Error occured:', err);
      });
    }
  }
})();
