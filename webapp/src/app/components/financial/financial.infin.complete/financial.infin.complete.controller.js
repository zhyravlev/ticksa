(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('FinancialInfinCompleteController', FinancialInfinCompleteController);

  /** @ngInject */
  function FinancialInfinCompleteController($stateParams, NavigationService) {
    var ctrl = this;

    ctrl.data = $stateParams;
    ctrl.showContinueTopUpBtn = false;
    ctrl.continueTopUp = continueTopUp;

    if (NavigationService.getBeforeTopUpState()) {
      ctrl.showContinueTopUpBtn = true;
    }

    function continueTopUp() {
      NavigationService.goToBeforeTopUpState();
    }
  }

})();
