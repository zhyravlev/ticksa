(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('FinancialPaymentCallbackController', FinancialPaymentCallbackController);

  /** @ngInject */
  function FinancialPaymentCallbackController($stateParams, $log,
                                              NavigationService, CashIOService, HeaderTitleService) {
    $log = $log.getInstance('FinancialPaymentCallbackController');

    var ctrl = this;

    ctrl.cashIO = null;
    ctrl.error = false;
    ctrl.cancel = false;
    ctrl.showContinueTopUpBtn = false;
    ctrl.continueTopUp = continueTopUp;

    HeaderTitleService.setTranslatedTitle('COMMONS_TOP_UP_ACCOUNT');

    if ($stateParams.error) {
      ctrl.error = true;
    } else if ($stateParams.cancel) {
      ctrl.cancel = true;
    } else if(!$stateParams.transactionId) {
      ctrl.error = true;
    } else {
      CashIOService.findById($stateParams.transactionId).then(function (res) {
        ctrl.cashIO = res.data;
        if (NavigationService.getBeforeTopUpState()) {
          ctrl.showContinueTopUpBtn = true;
        }
      }).catch(function (err) {
        ctrl.error = true;
        $log.error(err);
      });
    }

    function continueTopUp() {
      NavigationService.goToBeforeTopUpState();
    }
  }
})();
