(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('main.financial', {
        url: '/financial',
        template: '<ui-view flex></ui-view>'
      })
      .state('main.financial.start', {
        url: '/start?showRedirectMessage',
        templateUrl: 'app/components/financial/payment.start/financial.payment.start.html',
        controller: 'FinancialPaymentStartController',
        controllerAs: 'ctrl'
      })
      .state('main.financial.infinComplete', {
        url: '/infin-complete',
        templateUrl: 'app/components/financial/financial.infin.complete/financial.infin.complete.html',
        controller: 'FinancialInfinCompleteController',
        controllerAs: 'ctrl'
      })
      .state('main.financial.paymentCallback', {
        url: '/payment-callback?transactionId&error&cancel',
        templateUrl: 'app/components/financial/payment.callback/financial.payment.callback.html',
        controller: 'FinancialPaymentCallbackController',
        controllerAs: 'ctrl'
      })
    ;

    $urlRouterProvider.when('/infin/complete', '/financial/infin-complete');
  }
})();
