(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('FinancialTransferTicksDialogController', FinancialTransferTicksDialogController);

  /** @ngInject */
  function FinancialTransferTicksDialogController($log, CashIOService, $mdDialog, $rootScope, UserService) {

    $log = $log.getInstance('FinancialTransferTicksDialogController');

    var ctrl = this;

    ctrl.UserService = UserService;

    ctrl.transferTicks = function (ticks, form) {
      if (form.$valid) {
        CashIOService.transferTicks(ticks).then(function () {
          $rootScope.$broadcast('TicksTransaction');
          $mdDialog.hide();
        }).catch(function (err) {
          $log.error(err);
          $mdDialog.hide();
        });
      }
    }

  }
})();
