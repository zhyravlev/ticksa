(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkFinancialLemonwayForm', tkFinancialLemonwayForm);

  /** @ngInject */
  function tkFinancialLemonwayForm($log) {

    $log = $log.getInstance('tkFinancialLemonwayForm');

    return {
      restrict: 'E',
      scope: {
        lemonwayForm: '=',
        cardType: '='
      },
      templateUrl: 'app/components/financial/financial.lemonway.form.directive/financial.lemonway.form.directive.html',
      controllerAs: 'ctrl',
      controller: function ($scope, $element) {
        var ctrl = this;

        if (!$scope.lemonwayForm) {
          return;
        }

        try {

          // It is necessary to put form into DOM because if one doesn't do it then Firefox will not fire 'click'-event.
          // During tests it was found out that in Firefox we can't use Angular bindings (e.g. ng-bind-html + $sce), form
          // should be placed into DOM explicitly, e.g. via jquery append() function. Only in this case 'auto-clicking'
          // works in Firefox.
          var container = $element.find('.lemonway-form-container');
          container.append($scope.lemonwayForm);

          var cardInput = container
            .find('input[type="image"][name="' + $scope.cardType + '"]');

          // if input-element for auto-click found then it is unnecessary to show form to user
          var isInputFound = cardInput.length == 1;
          ctrl.hideForm = isInputFound;

          if (isInputFound) {
            cardInput.trigger('click');
            return;
          }

          $log.toServer.warn('Cant find correct input for auto-click: %j. Form: %s',
            cardInput, $scope.lemonwayForm.replace(/\s+/g, ' ')
          );

        } catch (err) {
          $log.toServer.error(
            'Error occurred: %j. Form: %s', err, $scope.lemonwayForm.replace(/\s+/g, ' ')
          );
        }
      }
    }
  }
})();

