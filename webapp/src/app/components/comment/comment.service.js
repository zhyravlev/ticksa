(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('CommentService', CommentService);

  /** @ngInject */
  function CommentService(API, $http) {

    return {
      getCommentsForContent: getCommentsForContent,
      postComment: postComment,
      likeComment: likeComment,
      dislikeComment: dislikeComment,
      getCommentsForLoggedInUser: getCommentsForLoggedInUser,
      getUserComment: getUserComment,
      updateComment: updateComment

    };

    function updateComment(commentId, comment) {
      return $http.put(API.URL + '/user/feeds/' + commentId, comment);
    }

    function getUserComment(commentId) {
      return $http.get(API.URL + '/user/feeds/' + commentId);
    }

    function likeComment(commentId) {
      return $http.post(API.URL + '/feeds/' + commentId + '/like', null);
    }

    function dislikeComment(commentId) {
      return $http.post(API.URL + '/feeds/' + commentId + '/dislike', null);
    }

    function postComment(contentId, comment) {
      return $http.post(API.URL + '/contents/' + contentId + '/feeds', comment);
    }

    function getCommentsForContent(contentId, page, pageSize) {
      return $http.get(API.URL + '/contents/' + contentId + '/feeds', {params: {page: page, pageSize: pageSize}});
    }

    function getCommentsForLoggedInUser() {
      return $http.get(API.URL + '/user/feeds');
    }

  }

})();
