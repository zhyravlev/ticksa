(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkCommentViewCard', tkCommentViewCard);

  /** @ngInject */
  function tkCommentViewCard($log, $stateParams, $rootScope, $state,
                             TICKSER_PREFERENCES, PRICES,
                             s3cdnFilter, thumbnailsS3CdnFilter,
                             CommentService, PaymentConfirmationService, UserService) {

    $log = $log.getInstance('tkCommentViewCard');

    return {
      controllerAs: 'ctrl',
      templateUrl: 'app/components/comment/comment.view.directive/comment.view.directive.html',
      scope: {
        comment: '='
      },
      link: function (scope, element, attrs) {
        //element.append($compile('<div ng-if="ctrl.comment.childComments">' +
        //  '<tk-comment-view-card comment="childComment", flex="100", ng-repeat="childComment in ctrl.comment.childComments"></tk-comment-view-card>' +
        //  '</div>')(scope))
      },
      controller: function ($scope) {

        var ctrl = this;
        ctrl.comment = $scope.comment;
        ctrl.UserService = UserService;

        ctrl.likeComment = likeComment;
        ctrl.dislikeComment = dislikeComment;
        ctrl.postComment = postComment;
        ctrl.editComment = editComment;

        var imageWatch = $scope.$watch('comment', function (comment) {
          if (comment) {
            if (comment.image) {
              ctrl.images = [{
                thumb: thumbnailsS3CdnFilter(comment.image.key, 200, 200),
                img: s3cdnFilter(comment.image.key),
                description: comment.image.name
              }]
            }
            imageWatch();
          }
        });


        function editComment(comment) {
          $state.go('main.commentEdit', {commentId: comment.id});
        }

        function likeComment(comment) {
          PaymentConfirmationService.confirmTicksAction(PRICES.LIKE, TICKSER_PREFERENCES.SKIP_LIKE_NOTIFICATION).then(function () {
            CommentService.likeComment(comment.id)
              .success(function () {
                $rootScope.$broadcast('TicksTransaction', {});
                comment.likes += 1;
              })
              .error(function (err) {
                $log.error(err);
              })
          });
        }

        function postComment(comment) {
          $state.go('main.commentAdd', {
            channelId: $stateParams.channelId,
            contentId: $stateParams.contentId,
            parentId: comment.id
          });
        }

        function dislikeComment(comment) {
          PaymentConfirmationService.confirmTicksAction(PRICES.DISLIKE, TICKSER_PREFERENCES.SKIP_DISLIKE_NOTIFICATION).then(function () {
            CommentService.dislikeComment(comment.id)
              .success(function () {
                $rootScope.$broadcast('TicksTransaction', {});
                comment.dislikes += 1;
              })
              .error(function (err) {
                $log.error(err);
              })
          });
        }

      }
    }
  }

})();
