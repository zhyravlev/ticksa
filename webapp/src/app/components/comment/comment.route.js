(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider

      .state('main.commentList', {
        url: '/comments',
        templateUrl: 'app/components/comment/comment.list/comment.list.html',
        controller: 'CommentListController',
        controllerAs: 'ctrl'
      })
      .state('main.commentEdit', {
        url: '/comments/:commentId',
        templateUrl: 'app/components/comment/comment.edit/comment.edit.html',
        controller: 'CommentEditController',
        controllerAs: 'ctrl'
      })
      .state('main.commentAdd', {
        url: '/channels/:channelId/contents/:contentId/parent/add-comment?parentId',
        templateUrl: 'app/components/comment/comment.add/comment.add.html',
        controller: 'CommentAddController',
        controllerAs: 'ctrl'
      })
  }
})();
