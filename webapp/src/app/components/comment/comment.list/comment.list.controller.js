(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('CommentListController', CommentListController);

  /** @ngInject */
  function CommentListController($log, $state, lodash, CONTENT_TYPES, CommentService) {
    $log = $log.getInstance('CommentListController');

    var ctrl = this;

    ctrl.CONTENT_TYPES = CONTENT_TYPES;
    CommentService.getCommentsForLoggedInUser()
      .success(function (data, status) {
        ctrl.commentList = data;
      })
      .error(function (err) {
        $log.log(err);
      });

    ctrl.editComment = function (comment) {
      $state.go('main.commentEdit', {commentId: comment.id});
    }

  }
})();

