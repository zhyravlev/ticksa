(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('CommentEditController', CommentEditController);

  /** @ngInject */
  function CommentEditController($log, NavigationService, TkDialogFactory, $stateParams, $state, $document, LibraryService, CommentService) {

    $log = $log.getInstance('CommentEditController');

    var ctrl = this;
    ctrl.selectImage = selectImage;
    ctrl.selectVideo = selectVideo;
    ctrl.editComment = editComment;
    ctrl.cancel = cancel;

    init();

    function cancel() {
      NavigationService.goToPreviousStateOrProvided('main.contentList', null);
    }

    function editComment(form) {
      ctrl.attemptedSave = true;
      ctrl.disablePost = true;
      if (form.$valid) {
        ctrl.selectedImage ? ctrl.comment.imageId = ctrl.selectedImage.id : null;
        ctrl.selectedVideo ? ctrl.comment.videoId = ctrl.selectedVideo.id : null;
        CommentService.updateComment(ctrl.comment.id, ctrl.comment)
          .success(function (data, status) {
            NavigationService.goToPreviousStateOrProvided('main.contentList', null);
          })
          .error(function (err) {
            ctrl.disablePost = false;
            $log.log(err);
          })
      } else {
        ctrl.disablePost = false;
      }
    }

    function init() {

      CommentService.getUserComment($stateParams.commentId)
        .success(function (data, status) {
          ctrl.comment = data;
          ctrl.comment.image ? ctrl.selectedImage = ctrl.comment.image : null;
          ctrl.comment.video ? ctrl.selectedVideo = ctrl.comment.video : null;
        })
        .error(function (err, status) {
          if (status == 404) {
            $state.go('main.commentList');
          }
          $log.log(err);
        });
      LibraryService.getImages()
        .success(function (data, status) {
          ctrl.images = data;
        })
        .error(function (err) {
          $log.log(err);
        });
      LibraryService.getVideos()
        .success(function (data, status) {
          ctrl.videos = data;
        })
        .error(function (err) {
          $log.log(err);
        })
    }

    function selectImage() {
      var tkDialog = new TkDialogFactory();
      tkDialog.controller('ImagePickerModalController');
      tkDialog.templateUrl('app/components/library/image/image.picker.modal.html');
      tkDialog.locals({images: ctrl.images, multiple: false});
      tkDialog.show().then(function (data) {
        ctrl.images = data.images;
        ctrl.selectedImage = data.selectedImage;
      });
    }

    function selectVideo() {
      var tkDialog = new TkDialogFactory();
      tkDialog.controller('VideoPickerModalController');
      tkDialog.templateUrl('app/components/library/video/video.picker.modal.html');
      tkDialog.locals({videos: ctrl.videos});
      tkDialog.show().then(function (data) {
        ctrl.selectedVideo = data.selectedVideo;
      });
    }


  }

})
();
