(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('CommentAddController', CommentAddController);

  /** @ngInject */
  function CommentAddController($log, $state, $rootScope, $stateParams,
                                TICKSER_PREFERENCES, PRICES,
                                LibraryService, CommentService, PaymentConfirmationService, TkDialogFactory) {

    $log = $log.getInstance('CommentAddController');

    var ctrl = this;

    ctrl.postComment = postComment;
    ctrl.cancel = cancel;
    ctrl.selectImage = selectImage;
    ctrl.selectVideo = selectVideo;
    ctrl.comment = {
      parentId: $stateParams.parentId
    };

    LibraryService.getImages()
      .success(function (data, status) {
        ctrl.images = data;
      })
      .error(function (err) {
        $log.log(err);
      });

    LibraryService.getVideos()
      .success(function (data, status) {
        ctrl.videos = data;
      })
      .error(function (err) {
        $log.log(err);
      });


    function selectImage() {
      var tkDialog = new TkDialogFactory();
      tkDialog.controller('ImagePickerModalController');
      tkDialog.templateUrl('app/components/library/image/image.picker.modal.html');
      tkDialog.locals({images: ctrl.images, multiple: false});
      tkDialog.show().then(function (data) {
        ctrl.images = data.images;
        ctrl.selectedImage = data.selectedImage;
      });
    }

    function selectVideo() {
      var tkDialog = new TkDialogFactory();
      tkDialog.controller('VideoPickerModalController');
      tkDialog.templateUrl('app/components/library/video/video.picker.modal.html');
      tkDialog.locals({videos: ctrl.videos});
      tkDialog.show().then(function (data) {
        ctrl.selectedVideo = data.selectedVideo;
      });
    }

    function postComment(form) {
      ctrl.attemptedSave = true;
      if (form.$valid && (ctrl.selectedVideo || ctrl.selectedImage || ctrl.comment.text)) {
        PaymentConfirmationService.confirmTicksAction(PRICES.ADD_FEED, TICKSER_PREFERENCES.SKIP_ADD_FEED_NOTIFICATION).then(function () {
          ctrl.comment.imageId = ctrl.selectedImage ? ctrl.selectedImage.id : null;
          ctrl.comment.videoId = ctrl.selectedVideo ? ctrl.selectedVideo.id : null;
          CommentService.postComment($stateParams.contentId, ctrl.comment)
            .success(function (data, status) {
              $state.go('main.contentView', {channelId: $stateParams.channelId, contentId: $stateParams.contentId});
              $rootScope.$broadcast('TicksTransaction', {});
            })
            .error(function (err) {
              $log.log(err);
            })
        });
      }
    }

    function cancel() {
      $state.go('main.contentView', {channelId: $stateParams.channelId, contentId: $stateParams.contentId});
    }

  }
})();
