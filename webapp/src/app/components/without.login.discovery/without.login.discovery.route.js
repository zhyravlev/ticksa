(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('discovery', {
        url: '/?q',
        templateUrl: 'app/components/without.login.discovery/without.login.discovery.html',
        controller: 'WithoutLoginDiscoveryController',
        controllerAs: 'ctrl',
        resolve: {
          initializationPromise: function (AuthorizationService) {
            return AuthorizationService.initUser();
          },

          // here we wait until translations became available
          translationsLanguage: function (LanguageUIService) {
            return LanguageUIService.getLanguage();
          }
        }
      })
  }
})();
