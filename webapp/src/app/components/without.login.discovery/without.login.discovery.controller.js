(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('WithoutLoginDiscoveryController', WithoutLoginDiscoveryController);

  /** @ngInject */
  function WithoutLoginDiscoveryController(translationsLanguage /*it is resolved in state*/,
                                           $q, $window, $state, $stateParams, $log, lodash, $translate,
                                           Pageable, CategoryFilterService, LanguageFilterService, LanguageUIService,
                                           ContentService, AuthorizationService, UserService, NavigationService,
                                           UI_LANGUAGES, LOGOTYPE_LINK) {

    $log = $log.getInstance('WithoutLoginDiscoveryController');

    var ctrl = this;

    var EMPTY_SEARCH = '';
    ctrl.LOGOTYPE_LINK = LOGOTYPE_LINK;

    if (!UserService.isEmpty()) {
      NavigationService.goToPreviousStateOrProvided('main.contentList', null)
    }

    var currentPage = 0;
    var pageSize = $window.innerHeight < 900 ? 6 : 12;

    ctrl.waitLoad = true;
    ctrl.searchString = normalizeSearchParam($stateParams.q);
    ctrl.searchIsUsed = ctrl.searchString !== EMPTY_SEARCH;
    ctrl.showFilters = false;
    ctrl.showSearchForm = false;
    ctrl.searchFocus = false;
    ctrl.disabledInfiniteScroll = false;
    ctrl.loadComplete = false;
    ctrl.loadingInProgress = false;
    ctrl.notFound = false;
    ctrl.languages = [];
    ctrl.selectedLanguages = [];
    ctrl.categories = [];
    ctrl.selectedCategories = [];
    ctrl.rawContents = [{form: true}];

    $translate('COMMONS_LANGUAGE').then(function (translation) {
      ctrl.filterLanguageLabel = translation;
    });

    $translate('COMMONS_CATEGORY').then(function (translation) {
      ctrl.filterCategoryLabel = translation;
    });

    ctrl.uiLanguages = UI_LANGUAGES;
    ctrl.uiLanguage = LanguageUIService.constructUILanguageObject(translationsLanguage.key);

    ctrl.disabledFilter = disabledFilter;
    ctrl.toggleFilter = toggleFilter;
    ctrl.searchSetFocus = searchSetFocus;
    ctrl.loginGoogle = loginGoogle;
    ctrl.loginFacebook = loginFacebook;
    ctrl.goToLogin = goToLogin;
    ctrl.search = search;
    ctrl.load = load;
    ctrl.loadMore = loadMore;
    ctrl.applyFilter = applyFilter;
    ctrl.searchCancel = searchCancel;
    ctrl.saveUILanguage = saveUILanguage;

    $q.all([CategoryFilterService.getCategories(), LanguageFilterService.getLanguages()]).then(function (result) {
      ctrl.categories = result[0];
      ctrl.languages = result[1];
    });

    function saveUILanguage() {

      // Take a look at LanguageUIService.constructUILanguageObject.
      // Because such method is used current method should not be fired if in md-select is selected same language as
      // applied to $translate. Such behavior was seen before LanguageUIService.constructUILanguageObject used.
      if (translationsLanguage.key === ctrl.uiLanguage.id) {
        $log.warn('md-select ng-change logic repeatedly broken');
        return;
      }

      LanguageUIService.setLanguage(ctrl.uiLanguage.id)
        .then(function () {
          $window.location.reload()
        });
    }

    function reset() {
      currentPage = 0;
      ctrl.rawContents = [{form: true}];
      ctrl.loadComplete = false;
      ctrl.notFound = false;
      ctrl.showFilters = false;
    }

    function searchCancel() {
      ctrl.searchString = EMPTY_SEARCH;
      ctrl.searchIsUsed = false;

      changeSearchParamInCurrentState(EMPTY_SEARCH);

      if (ctrl.showSearchForm) {
        ctrl.showSearchForm = false;
        ctrl.searchFocus = false;
      }

      reset();
      load();
    }

    function applyFilter() {
      reset();
      load();
    }

    function loadMore() {
      ctrl.waitLoad = false;
      load();
    }

    function load() {
      if (currentPage > 0 && ctrl.waitLoad) {
        return;
      }

      if (ctrl.loadComplete) {
        return;
      }

      ctrl.loadingInProgress = true;
      ctrl.disabledInfiniteScroll = true;

      getContent().then(function (res) {
        var contents = res.data;

        if (contents.length == 0 && currentPage == 0) {
          ctrl.notFound = true;
        }

        if (contents.length == 0 || contents.length < pageSize) {
          ctrl.loadComplete = true;
        }

        if (contents.length == pageSize) {
          currentPage++;
        }

        lodash.forEach(contents, function (content) {
          ctrl.rawContents.push(content);
        });

        ctrl.loadingInProgress = false;
        ctrl.disabledInfiniteScroll = false;
      });
    }

    function getContent() {
      var pageable = new Pageable()
        .addArrayFilter('languages', lodash.pluck(ctrl.selectedLanguages, 'id'))
        .addArrayFilter('categories', lodash.pluck(ctrl.selectedCategories, 'id'))
        .searchString(ctrl.searchString)
        .page(currentPage)
        .pageSize(pageSize)
        .build();

      return ContentService.getDiscoveryContents(pageable);
    }

    function toggleFilter() {
      ctrl.showFilters = !ctrl.showFilters;
    }

    function disabledFilter() {
      return ctrl.showFilters = false;
    }

    function searchSetFocus() {
      ctrl.showSearchForm = true;
      ctrl.searchFocus = true;
    }

    function loginGoogle() {
      return AuthorizationService.loginGoogle();
    }

    function loginFacebook() {
      return AuthorizationService.loginFacebook();
    }

    function goToLogin() {
      return NavigationService.goToLoginPage();
    }

    function search(event) {

      if ((event.type === 'keyup' && event.keyCode === 13) || (event.type === 'click')) {

        if (ctrl.searchString === EMPTY_SEARCH) {
          searchCancel();
          return true;
        }

        reset();
        load();

        ctrl.searchIsUsed = true;
        changeSearchParamInCurrentState(ctrl.searchString);

        if (ctrl.showSearchForm) {
          ctrl.showSearchForm = false;
          ctrl.searchFocus = false;
        }
        return true;
      }

      if (event.type === 'keyup' && event.keyCode == 27) {
        searchCancel();
        return true;
      }

      return true;
    }

    // http://stackoverflow.com/questions/20884551
    function changeSearchParamInCurrentState(newSearchParam) {
      $state.transitionTo($state.current, {q: newSearchParam}, {notify: false});
    }

    // we have to check equality to 'true' because when in url there is '/?q' (without any search string) then
    // in $stateParams parameters it appears as 'true'
    function normalizeSearchParam(searchParamFromQuery) {
      if (!angular.isString(searchParamFromQuery)) {
        return EMPTY_SEARCH;
      }
      if (searchParamFromQuery === 'true') {
        return EMPTY_SEARCH;
      }
      return searchParamFromQuery;
    }
  }
})();
