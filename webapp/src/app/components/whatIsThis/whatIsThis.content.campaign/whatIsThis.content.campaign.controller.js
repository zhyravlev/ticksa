(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('WhatIsThisContentCampaignController', WhatIsThisContentCampaignController);

  /** @ngInject */
  function WhatIsThisContentCampaignController($mdDialog) {
    var ctrl = this;
    ctrl.close = function () {
      $mdDialog.hide()
    }
  }
})();