(function() {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('DashboardController', DashboardController);

  /** @ngInject */
  function DashboardController(lodash, DashboardService, HeaderTitleService) {
    var ctrl = this;

    HeaderTitleService.setTranslatedTitle('COMMONS_DASHBOARD');

    DashboardService.getIncomePerMonthForCurrentYear()
      .success(function (data, status) {
        var totalTicks = 0;
        lodash.forEach(data, function (dataForMonth) {
          ctrl.data[0][parseInt(dataForMonth.month) - 1] = dataForMonth.ticks;
          totalTicks += dataForMonth.ticks;
        });
        ctrl.totalTicks = totalTicks;
      });

    ctrl.labels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    ctrl.series = ['Earning by month in ticks'];
    ctrl.data = [
      [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];

    ctrl.subs_series = ['Subscribers'];
    ctrl.subs_data = [
      [4500, 4877, 4995, 4880, 5121, 5400, 5224, 3400, 3222, 4100, 4444, 5000]
    ];

  }
})();
