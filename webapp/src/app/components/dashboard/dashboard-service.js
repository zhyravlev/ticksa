(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('DashboardService', DashboardService);

  /** @ngInject */
  function DashboardService(API, $http) {

    return {
      getIncomePerMonthForCurrentYear: getIncomePerMonthForCurrentYear
    };


    function getIncomePerMonthForCurrentYear() {
      return $http.get(API.URL + '/dashboard/income-per-month');
    }

  }
})();
