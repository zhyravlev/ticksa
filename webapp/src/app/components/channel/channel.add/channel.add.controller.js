(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelAddController', ChannelAddController);

  /** @ngInject */
  function ChannelAddController($log, $state, $translate,
                                NavigationService, ChannelService, jGrowlNotify, HeaderTitleService,
                                CHANNEL_TYPES) {

    $log = $log.getInstance('ChannelAddController');
    HeaderTitleService.setTranslatedTitle('COMMONS_CHANNEL_ADD');

    var ctrl = this;

    ctrl.channel = {
      coverImage: null,
      backgroundImage: null,
      type: CHANNEL_TYPES.PUBLIC,
      shareIncome: false,
      revenues: []
    };

    ctrl.addChannel = addChannel;
    ctrl.cancel = cancel;

    function addChannel(form) {

      ctrl.formSubmit = true;

      if (form.$valid) {
        ChannelService.postChannel(ctrl.channel)
          .success(function () {
            $state.go('main.channels', {}, {reload: true})
          })
          .error(function (err, code) {
            $log.error(err);

            if (code == 400) {
              new jGrowlNotify().error($translate.instant('ERROR_MAX_NUM_CHANNELS'))
            }
          })
      }
    }

    function cancel() {
      NavigationService.goToPreviousStateOrProvided('main.channels', null);
    }
  }
})();
