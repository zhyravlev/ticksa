(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkChannelSettings', tkChannelSettings);

  /** @ngInject */
  function tkChannelSettings($log, $translate, $mdDialog, $stateParams, lodash,
                             CommonsUIService, ChannelService, LibraryService, DialogService, jGrowlNotify,
                             SelectionsService, ModalService, TickserHttpService, UserService) {

    $log = $log.getInstance('tkChannelSettings');

    return {
      restrict: 'E',
      scope: {
        channel: '=',
        form: '=',
        isRevenuesError: '='
      },
      templateUrl: 'app/components/channel/directive/channel.settings/channel.settings.directive.html',
      controllerAs: 'ctrl',
      controller: function ($scope) {

        var ctrl = this;

        ctrl.images = [];
        ctrl.DialogService = DialogService;
        ctrl.channelTypes = SelectionsService.getChannelTypes();
        ctrl.formSubmit = false;
        ctrl.channelForm = $scope.form;
        ctrl.revenues = {
          error: false,
          errorList: {},
          addUser: addUser,
          removeUser: removeUser
        };

        ctrl.votingPreview = votingPreview;
        ctrl.rankedPreview = rankedPreview;
        ctrl.rankedReset = rankedReset;
        ctrl.selectCoverImage = selectCoverImage;
        ctrl.selectBackgroundImage = selectBackgroundImage;
        ctrl.coverImage = {};
        ctrl.backgroundImage = {};

        ctrl.revenuesErrors = {};

        /** Load all images of the media library */
        LibraryService.getImages()
          .success(function (data) {
            ctrl.images = data
          })
          .error(function (err) {
            $log.error(err)
          });

        /** We are watching the field `enter user email` */
        $scope.$watch('ctrl.userEmail', function (data) {
          if (data) {
            // Error reset
            ctrl.userNotFound = false;
            ctrl.isExists = false;
            ctrl.isSelf = false
          }
        });

        var unwatchChannel = $scope.$watch('channel', function (data) {
          if (!lodash.isEmpty(data)) {

            ctrl.channel = data;

            ctrl.coverImage = ctrl.channel.coverImage ? {
              key: ctrl.channel.coverImage.key
            } : null;
            ctrl.backgroundImage = (ctrl.channel.backgroundImage !== null) ? {
              key: ctrl.channel.backgroundImage.key
            } : null;

            unwatchChannel();
          }
        });

        $scope.$watch('ctrl.channel.revenues', function (data) {
          if (data) {

            // Reset.
            var users = 0;
            var percent = 0;

            ctrl.revenuesErrors = {};

            lodash.forEach(data, function (value, key) {

              var date = data[key].expiryDate;
              var percent_str = String(data[key].percent).replace(',', '.');

              /**
               * If you choose more than the current date.
               * @param {isExpired} - boolean. True if the date has expired.
               */
              if (date !== null) {
                data[key].isExpired = !moment().isBefore(date);
              } else {
                data[key].isExpired = false;
              }

              // Input validation percent.
              if (!/^(\d+)(\.\d)?$/.test(percent_str)) {
                ctrl.revenuesErrors.invalid = true
              }

              if (data[key].percent < 0.1) {
                ctrl.revenuesErrors.small = true
              }
              // End validation.

              // If the date has expired, do not show this user and percent in the summary.
              if (!data[key].isExpired) {
                users++;
                percent += Math.abs(data[key].percent)
              }
            });

            if (percent > 100) {
              ctrl.revenuesErrors.total = true
            }

            ctrl.revenuesInfo = $translate.instant('CHANNEL_REVENUE_TABLE_INFO', {
              users: users,
              percent: isNaN(percent) ? 0 : percent.toFixed(1)
            });

            $scope.isRevenuesError = !lodash.isEmpty(ctrl.revenuesErrors);
          }
        }, true);

        function addUser() {

          ctrl.isSelf = false;
          ctrl.isExists = false;
          ctrl.userNotFound = false;

          var isExists = function () {
            return lodash.find(ctrl.channel.revenues, function (data) {
              return ctrl.userEmail == data.tickser.email
            })
          };

          if (ctrl.userEmail == UserService.getEmail()) {

            ctrl.isSelf = true;
            ctrl.selectUserEmail = true

          } else if (typeof isExists() == 'object') {

            ctrl.isExists = true;
            ctrl.selectUserEmail = true

          } else {
            TickserHttpService.getUserByEmail(ctrl.userEmail)
              .success(function (data) {

                ctrl.channel.revenues.unshift({
                  percent: 10,
                  expiryDate: null,
                  isExpired: false,
                  tickser: {
                    id: data.id,
                    email: data.email
                  }
                });

                ctrl.userEmail = ''

              })
              .error(function (err, code) {
                if (code == 404) {
                  ctrl.userNotFound = true;
                  ctrl.selectUserEmail = true
                } else {
                  $log.error(err)
                }
              })
          }
        }

        function removeUser(data) {
          ModalService.confirmationBuilder()
            .confirmationClickOutsideToClose()
            .confirmationTexts({
              title: $translate.instant('COMMONS_DELETE_USER'),
              body: $translate.instant('DELETE_REVENUE_BODY'),
              cancel: $translate.instant('COMMONS_CANCEL'),
              confirm: $translate.instant('COMMONS_DELETE_USER')
            })
            .confirmationShowModal()
            .then(function () {
              ctrl.channel.revenues.splice(ctrl.channel.revenues.indexOf(data), 1)
            })
        }

        function selectCoverImage(event) {
          CommonsUIService.selectSingleImage(event, ctrl.images, $scope.cover)
            .then(function (data) {
              ctrl.coverImage = data;
              ctrl.channel.coverImageId = data ? data.id : null
            })
        }

        function selectBackgroundImage(event) {
          CommonsUIService.selectSingleImage(event, ctrl.images, $scope.background)
            .then(function (data) {
              ctrl.backgroundImage = data;
              ctrl.channel.backgroundImageId = data ? data.id : null;
            })
        }

        /** RANKED */

        function rankedPreview() {
          $mdDialog.show({
            templateUrl: 'app/components/channel/ranked.preview.dialog/ranked.preview.dialog.html',
            controller: 'RankedPreviewDialogController',
            controllerAs: 'ctrl',
            clickOutsideToClose: true,
            locals: {
              channelId: $stateParams.channelId
            }
          });
        }

        function rankedReset() {
          ModalService.confirmationBuilder()
            .confirmationClickOutsideToClose()
            .confirmationTexts({
              title: $translate.instant('CHANNEL_TOPRANKED__RESET_DIALOG__TITLE'),
              body: $translate.instant('CHANNEL_TOPRANKED__RESET_DIALOG__BODY'),
              cancel: $translate.instant('COMMONS_CANCEL'),
              confirm: $translate.instant('COMMONS_OK')
            })
            .confirmationShowModal()
            .then(function () {
              ChannelService.resetRankedList($stateParams.channelId)
                .success(function () {
                  new jGrowlNotify().success($translate.instant('CHANNEL_TOPRANKED__RESET_SUCCESS'));
                })
                .error(function (err) {
                  $log.error(err)
                })
            });
        }

        /** VOTING */

        function votingPreview() {
          $mdDialog.show({
            templateUrl: 'app/components/channel/voteboard.preview.dialog/voteBoard.preview.dialog.html',
            controller: 'VoteBoardPreviewDialogController',
            controllerAs: 'ctrl',
            clickOutsideToClose: true,
            locals: {
              channelId: $stateParams.channelId
            }
          });
        }
      }
    }
  }
})();
