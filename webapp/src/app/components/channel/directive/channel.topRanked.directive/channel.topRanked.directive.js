(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkTopRanked', tkTopRanked);

  /** @ngInject */
  function tkTopRanked($rootScope, $log, $translate, $state, tkMoment, ChannelService, ModalService) {

    $log = $log.getInstance('tkTopRanked');

    /**
     * How can use this directive.
     *
     * <div data-tk-top-ranked="{ view:'card', channelId:1, max:5 }"></div>
     *
     * @param view - String. `card` or `full`.
     * @param max - Number. The maximum number of rows.
     * @param channelId - Number. Current channel id.
     * @param channelData - Object. Channel.
     * @param preview - Boolean. True if it should be is run as a preview.
     */

    return {
      restrict: 'A',
      templateUrl: 'app/components/channel/directive/channel.topRanked.directive/channel.topRanked.directive.html',
      scope: {
        tkTopRanked: '='
      },
      controllerAs: 'ctrl',
      controller: function ($scope) {

        var ctrl = this;

        var max = $scope.tkTopRanked.hasOwnProperty('max') ? $scope.tkTopRanked.max : null;
        var isChannelData = $scope.tkTopRanked.hasOwnProperty('channelData');
        var isCardView = $scope.tkTopRanked.view == 'card';
        var isFullView = $scope.tkTopRanked.view == 'full';
        var isPreview = $scope.tkTopRanked.hasOwnProperty('preview');
        var channelId = $scope.tkTopRanked.channelId;
        var currentPage = 0;
        var pageSize = 10;

        ctrl.data = [];
        ctrl.nothing = false;
        ctrl.canScroll = isFullView;

        ctrl.pwyw = pwyw;
        ctrl.topUp = topUp;
        ctrl.goToFullList = goToFullList;
        ctrl.loadMore = loadMore;

        // If channel data has been already received
        if (isChannelData) {
          setHeader($scope.tkTopRanked.channelData.showRankedListDate);
        } else {
          // request to server
          ChannelService.getChannel(channelId)
            .success(function (channel) {
              setHeader(channel.showRankedListDate);
            })
            .error(function (err) {
              $log.error(err);
            });
        }

        // because infinite scroll is enable only in full view there will be no query to server - we have to do it
        // explicitly
        if (isCardView) {
          refresh();
        }

        /**
         * Load leaderboard data.
         *
         * @param page {Number} Current page.
         * @param pageSize {Number} Size of page.
         * @param clear {Boolean} If true, will clear leaderboard list.
         * @returns Promise
         */
        function loadFromServer(page, pageSize, clear) {
          ChannelService.getRankedList(channelId, page, pageSize)
            .success(function (contents) {

              if (clear) {
                ctrl.data = [];
              }

              var length = contents.length;

              if (page == 0) {
                ctrl.nothing = length == 0;
              }

              ctrl.canScroll = clear
                ? isFullView
                : isFullView && length == pageSize;

              if (ctrl.canScroll) {
                currentPage++;
              }

              if (length > 0) {
                var contentsToPush = !!max
                  ? contents.slice(0, max)
                  : contents;

                contentsToPush.forEach(function (item) {
                  ctrl.data.push(item);
                });
              }

              if (!!max && isCardView) {
                if (page == 0 && length > 0 && length < max) {
                  ctrl.data.push({
                    extension: true
                  });
                }
              }

              $rootScope.$broadcast('tkMasonryResize', {});
            })
            .error(function (err) {
              $log.error(err);
            });
        }

        function refresh() {
          currentPage = 0;
          loadFromServer(currentPage, pageSize, true);
        }

        function pwyw() {
          if (isPreview) {
            return true;
          }

          ModalService.showPwywChannelModal().then(function (enteredTicks) {
            ChannelService.pwyw(channelId, enteredTicks)
              .success(function () {
                $rootScope.$broadcast('TicksTransaction', {});
                refresh();
              })
              .error(function (err) {
                $log.error(err)
              });
          });
        }

        function topUp(tickserId) {
          if (isPreview) {
            return true;
          }

          ModalService.showTopUpChannelModal().then(function (enteredTicks) {
            ChannelService.topUp(channelId, enteredTicks, tickserId)
              .success(function () {
                $rootScope.$broadcast('TicksTransaction', {});
                refresh();
              })
              .error(function (err) {
                $log.error(err)
              });
          });
        }

        function goToFullList() {
          if (isPreview) {
            return true;
          }

          return $state.go('main.channels.topRanked', {channelId: channelId});
        }

        function setHeader(date) {
          ctrl.header = $translate.instant('CHANNEL_TOPRANKED__LIST_HEADER', {
            date: tkMoment(date).format('DD/MM')
          });
        }

        function loadMore() {
          if (ctrl.canScroll) {
            loadFromServer(currentPage, pageSize);
          }
        }
      }
    }
  }
})();
