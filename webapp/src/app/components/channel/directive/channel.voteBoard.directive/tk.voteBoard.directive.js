(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkVoteBoard', tkVoteBoard);

  /** @ngInject */
  function tkVoteBoard($rootScope, $log, $state, lodash,
                       ModalService, ChannelService, VoteBoardService, ContentService) {

    $log = $log.getInstance('tkVoteBoardDirective');

    /**
     * How to use this directive.
     *
     * <div data-tk-vote-board="{ view:'card', channelId:1, max:5 }"></div>
     *
     * @param view - String. `card` or `full`.
     * @param max - Number. The maximum number of rows.
     * @param channelId - Number. Current channel id.
     * @param channelData - Object. Channel.
     * @param preview - Boolean. True if it should be shown as a preview.
     */

    return {
      restrict: 'A',
      templateUrl: 'app/components/channel/directive/channel.voteBoard.directive/tk.voteBoard.directive.html',
      scope: {
        tkVoteBoard: '='
      },
      controllerAs: 'ctrl',
      controller: function ($scope) {

        var ctrl = this;

        var max = $scope.tkVoteBoard.hasOwnProperty('max') ? $scope.tkVoteBoard.max : null;
        var isCardView = $scope.tkVoteBoard.view == 'card';
        var isFullView = $scope.tkVoteBoard.view == 'full';
        var isPreview = $scope.tkVoteBoard.hasOwnProperty('preview');
        var channelId = $scope.tkVoteBoard.channelId;
        var currentPage = 0;
        var pageSize = 10;

        ctrl.data = [];
        ctrl.nothing = false;
        ctrl.canScroll = isFullView;
        ctrl.isCardView = isCardView;

        ctrl.goToFullList = goToFullList;
        ctrl.loadMore = loadMore;
        ctrl.goToContent = goToContent;
        ctrl.voteContent = voteContent;

        var unregisterVoteBoardObserver = VoteBoardService.registerObserver(function (data) {
          var content = undefined;
          if ('id' in data && 'likes' in data) {
            content = lodash.find(ctrl.data, function (o) {
              return o.id == data.id;
            });
            if (angular.isObject(content)) {
              content.likes = data.likes;
            }
          }
        });

        $scope.$on('$destroy', unregisterVoteBoardObserver);

        // because infinite scroll is enable only in full view there will be no query to server - we have to do it
        // explicitly
        if (isCardView) {
          refresh();
        }

        /**
         * Load vote board data.
         *
         * @param page {Number} Current page.
         * @param pageSize {Number} Size of page.
         * @param clear {Boolean} If true, will clear vote board list.
         */
        function loadFromServer(page, pageSize, clear) {
          ChannelService.getVotingList(channelId, page, pageSize)
            .success(function (contents) {
              if (clear) {
                ctrl.data = [];
              }

              var length = contents.length;

              if (page == 0) {
                ctrl.nothing = length == 0;
              }

              ctrl.canScroll = clear
                ? isFullView
                : isFullView && length == pageSize;

              if (ctrl.canScroll) {
                currentPage++;
              }

              if (length > 0) {
                var contentsToPush = !!max
                  ? contents.slice(0, max)
                  : contents;

                contentsToPush.forEach(function (item) {
                  ctrl.data.push(item);
                });
              }

              if (!!max && isCardView) {
                if (page == 0 && length > 0 && length < max) {
                  ctrl.data.push({
                    extension: true
                  });
                }
              }

              $rootScope.$broadcast('tkMasonryResize', {});
            })
            .error(function (err) {
              $log.log(err);
              ctrl.loadingInProgress = false;
            });
        }

        function refresh() {
          currentPage = 0;
          loadFromServer(currentPage, pageSize, true);
        }

        function goToFullList() {
          if (isPreview) {
            return true;
          }

          return $state.go('main.channels.voteBoard', {channelId: channelId});
        }

        function goToContent(content) {
          if (isPreview) {
            return true;
          }

          return $state.go('main.contentView', {
            channelId: content.channelId,
            contentId: content.id
          });
        }

        function loadMore() {
          if (ctrl.canScroll) {
            loadFromServer(currentPage, pageSize);
          }
        }

        function voteContent(content) {
          if (isPreview) {
            return true;
          }

          ModalService.showPwywContentModal(content).then(function (enteredTicks) {
            ContentService.likeContent(content.id, enteredTicks)
              .success(function () {
                var likes = content.likes + enteredTicks;
                VoteBoardService.broadcast(content.id, likes);
                $rootScope.$broadcast('TicksTransaction', {});
              })
              .error(function (err) {
                $log.error(err);
              })
          });
        }
      }
    }
  }
})();
