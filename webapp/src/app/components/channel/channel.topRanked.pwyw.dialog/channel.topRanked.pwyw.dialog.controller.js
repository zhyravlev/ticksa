(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelTopRankedPwywDialogController', ChannelTopRankedPwywDialogController);

  /** @ngInject */
  function ChannelTopRankedPwywDialogController($mdDialog) {
    var ctrl = this;
    ctrl.enteredTicks = null;
    ctrl.pwywContent = pwywContent;
    ctrl.cancel = cancel;

    function cancel() {
      $mdDialog.cancel();
    }

    function pwywContent(form) {
      if (form.$valid) {
        $mdDialog.hide(ctrl.enteredTicks);
      }
    }
  }

})();
