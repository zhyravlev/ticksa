(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelListController', ChannelListController);

  /** @ngInject */
  function ChannelListController($log, $state, ChannelService, HeaderTitleService) {

    $log = $log.getInstance('ChannelListController');

    var ctrl = this;

    HeaderTitleService.setTranslatedTitle('COMMONS_YOUR_CHANNELS');

    ctrl.channelDetails = channelDetails;
    ctrl.openChannel = openChannel;
    ctrl.addChannel = addChannel;
    //ctrl.channelMembers = channelMembers;

    ChannelService.getUserChannels()
      .success(function (data, status) {
        ctrl.channels = data;
      })
      .error(function (err) {
        $log.log(err);
      });

    function openChannel(channel) {
      $state.go('main.channelContentEdit', {channelId: channel.id});
    }

    function channelDetails(channel) {
      $state.go('main.channels.edit', {channelId: channel.id});
    }


    function addChannel() {
      $state.go('main.channels.add');
    }

    //function channelMembers(channel, $event) {
    //  $mdDialog.show({
    //    parent: document.body,
    //    targetEvent: $event,
    //    templateUrl: 'app/components/channel/channel.members/channel.members.dialog.html',
    //    controller: 'ChannelMembersDialogController',
    //    controllerAs: 'ctrl',
    //    locals: {
    //      channel: channel
    //    },
    //    clickOutsideToClose: true,
    //    escapeToClose: true
    //  });
    //}

  }

})();
