(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('ChannelService', ChannelService);

  /** @ngInject */
  function ChannelService(API, $http) {
    var ChannelService = this;

    ChannelService.getSubscribedChannels = function () {
      return $http.get(API.URL + '/user/subscriptions');
    };

    ChannelService.getUserChannel = function (channelId) {
      return $http.get(API.URL + '/user/channels/' + channelId);
    };

    ChannelService.getUserChannels = function () {
      return $http.get(API.URL + '/user/channels');
    };

    ChannelService.postChannel = function (channel) {
      return $http.post(API.URL + '/user/channels', channel);
    };

    ChannelService.updateChannel = function (channelId, channel) {
      return $http.put(API.URL + '/user/channels/' + channelId, channel);
    };

    ChannelService.getChannel = function (channelId) {
      return $http.get(API.URL + '/channels/' + channelId);
    };

    ChannelService.subscribeToChannel = function (channelId) {
      return $http.post(API.URL + '/channels/' + channelId + '/subscribe');
    };

    ChannelService.unsubscribeToChannel = function (channelId) {
      return $http.post(API.URL + '/channels/' + channelId + '/unsubscribe');
    };

    ChannelService.deactivateChannel = function (channelId) {
      return $http.delete(API.URL + '/user/channels/' + channelId);
    };

    ChannelService.inviteToChannel = function (channelId, invite) {
      return $http.post(API.URL + '/user/channels/' + channelId + '/invite', invite);
    };

    ChannelService.sortContent = function (channelId, data) {
      return $http.post(API.URL + '/channels/' + channelId + '/sort-content', data);
    };

    ChannelService.resetRankedList = function (channelId) {
      return $http.post(API.URL + '/channels/' + channelId + '/reset-ranked-list');
    };

    ChannelService.getRankedList = function (channelId, page, pageSize) {
      return $http.get(API.URL + '/channels/' + channelId + '/ranked-list', {params: {page: page, pageSize: pageSize}});
    };

    ChannelService.getVotingList = function (channelId, page, pageSize) {
      return $http.get(API.URL + '/channels/' + channelId + '/voting-list', {params: {page: page, pageSize: pageSize}});
    };

    ChannelService.pwyw = function (channelId, amount) {
      return $http.post(API.URL + '/channels/' + channelId + '/donate', {amount: amount});
    };

    ChannelService.topUp = function (channelId, amount, tickserId) {
      return $http.post(API.URL + '/channels/' + channelId + '/donate', {amount: amount, behalfTickserId: tickserId});
    };
  }

})();
