(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelVotingListController', ChannelVotingListController);

  /** @ngInject */
  function ChannelVotingListController($stateParams, HeaderTitleService) {
    var ctrl = this;

    HeaderTitleService.setTranslatedTitle('HEADER_TITLE_CHANNEL_VOTING_LIST');

    ctrl.channelId = $stateParams.channelId;
  }
})();
