(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('RankedPreviewDialogController', RankedPreviewDialogController);

  /** @ngInject */
  function RankedPreviewDialogController($mdDialog, channelId) {
    var ctrl = this;
    ctrl.channelId = channelId;

    ctrl.close = function (event) {
      event.preventDefault();
      event.stopPropagation();
      $mdDialog.hide()
    }
  }
})();