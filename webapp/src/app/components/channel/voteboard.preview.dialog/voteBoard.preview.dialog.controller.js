(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('VoteBoardPreviewDialogController', VoteBoardPreviewDialogController);

  /** @ngInject */
  function VoteBoardPreviewDialogController($mdDialog, channelId) {
    var ctrl = this;
    ctrl.channelId = channelId;

    ctrl.close = function (event) {
      event.preventDefault();
      event.stopPropagation();
      $mdDialog.hide()
    }
  }
})();