(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelTopRankedController', ChannelTopRankedController);

  /** @ngInject */
  function ChannelTopRankedController($stateParams, HeaderTitleService) {
    var ctrl = this;

    HeaderTitleService.setTranslatedTitle('HEADER_TITLE_CHANNEL_TOPRANKED');

    ctrl.channelId = $stateParams.channelId;
  }
})();