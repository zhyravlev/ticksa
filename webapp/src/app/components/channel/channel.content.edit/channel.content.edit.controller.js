(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelContentEditController', ChannelContentEditController);

  /** @ngInject */
  function ChannelContentEditController($log, $stateParams, $state, $translate, $mdDialog, $document, $sanitize, lodash,
                                        CONTENT_TYPES, jGrowlNotify, ChannelService, ContentService, ModalService, HeaderTitleService) {

    $log = $log.getInstance('ChannelContentEditController');

    var ctrl = this;
    ctrl.contents = [];
    ctrl.CONTENT_TYPES = CONTENT_TYPES;

    ctrl.getHtmlCode = getHtmlCode;
    ctrl.editContent = editContent;
    ctrl.deleteContent = deleteContent;
    ctrl.viewContent = viewContent;
    ctrl.changePublish = changePublish;
    ctrl.openChannelSettings = openChannelSettings;
    ctrl.addContent = addContent;
    ctrl.sanitizeText = sanitizeText;

    // Sortable options
    ctrl.sortableOptions = {
      handle: '.sort-handle',
      animation: 150,
      ghostClass: 'ghost',
      onSort: function (sort) {
        if (!sort.models) {
          return;
        }
        var ids = lodash.pluck(sort.models, 'id');

        var data = {
          'ids[]': ids
        };

        ChannelService.sortContent(ctrl.channel.id, data)
          .error(function (err) {
            $log.error(err)
          })
      }
    };

    // Get info about this channel
    ChannelService.getUserChannel($stateParams.channelId)
      .success(function (data) {
        ctrl.channel = data;
        HeaderTitleService.setCustomTitle(ctrl.channel.name);
      })
      .error(function (err, status) {
        if (status == 404) {
          $state.go('main.channels');
        }
        $log.error(err);
      });

    // Load contents
    ContentService.getContentByChannelForUser($stateParams.channelId, 0, 999)
      .success(function (contents) {
        contents.forEach(function (content) {
          ctrl.contents.push(content);
        })
      })
      .error(function (err) {
        $log.error(err)
      });

    function getHtmlCode($event, content) {
      $mdDialog.show({
        closeTo: angular.element($document[0].body),
        parent: $document[0].body,
        targetEvent: $event,
        templateUrl: 'app/components/content/content.getHtmlCode.dialog/content.getHtmlCode.dialog.html',
        controller: 'ContentGetHtmlCodeDialogController',
        controllerAs: 'ctrl',
        clickOutsideToClose: true,
        escapeToClose: true,
        locals: {
          content: content,
          url: null
        }
      });
    }

    function editContent(content) {
      $state.go('main.contentEdit', {
        channelId: content.channel.id,
        contentId: content.id
      })
    }

    function deleteContent(ev, content) {
      ModalService.confirmationBuilder()
        .confirmationTargetEvent(ev)
        .confirmationClickOutsideToClose()
        .confirmationTexts({
          title: $translate.instant('COMMONS_DELETE_CONTENT'),
          body: $translate.instant('DELETE_CONTENT_BODY'),
          cancel: $translate.instant('COMMONS_CANCEL'),
          confirm: $translate.instant('COMMONS_DELETE_CONTENT')
        })
        .confirmationShowModal()
        .then(function () {
          ContentService.deactivateContent(content.channel.id, content.id)
            .success(function () {
              ctrl.contents.splice(ctrl.contents.indexOf(content), 1)
            })
            .error(function (err) {
              $log.error(err)
            })
        });
    }

    function viewContent(content) {
      $state.go('main.contentView', {channelId: content.channel.id, contentId: content.id});
    }

    function changePublish (content) {
      if(content.published){
        return ContentService.publishContent(content.channel.id, content.id)
          .success(function () {
            new jGrowlNotify().info($translate.instant('CONTENT_NOTIFY_PUBLISHED', {content: content.title}))
          })
          .error(function (err) {
            $log.error(err)
          })
      }

      return ContentService.unpublishContent(content.channel.id, content.id)
        .success(function () {
          new jGrowlNotify().warn($translate.instant('CONTENT_NOTIFY_UNPUBLISHED', {content: content.title}));
        })
        .error(function (err) {
          $log.error(err)
        })
    }

    function openChannelSettings() {
      $state.go('main.channels.edit', {
        channelId: $stateParams.channelId
      });
    }

    function addContent(type) {
      $state.go('main.contentAdd', {
        type: type,
        channelId: $stateParams.channelId
      });
    }

    function sanitizeText(str) {
      return $sanitize(str.replace(/\n/g, '<br/>'));
    }
  }
})();
