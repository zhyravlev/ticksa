(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelTopRankedTopUpDialogController', ChannelTopRankedTopUpDialogController);

  /** @ngInject */
  function ChannelTopRankedTopUpDialogController($mdDialog) {
    var ctrl = this;
    ctrl.enteredTicks = null;
    ctrl.topUpContent = topUpContent;
    ctrl.cancel = cancel;

    function cancel() {
      $mdDialog.cancel();
    }

    function topUpContent(form) {
      if (form.$valid) {
        $mdDialog.hide(ctrl.enteredTicks);
      }
    }
  }

})();
