(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelVoteBoardController', ChannelVoteBoardController);

  /** @ngInject */
  function ChannelVoteBoardController($stateParams, HeaderTitleService) {
    var ctrl = this;

    HeaderTitleService.setTranslatedTitle('HEADER_TITLE_CHANNEL_VOTE_BOARD');

    ctrl.channelId = $stateParams.channelId;
  }
})();