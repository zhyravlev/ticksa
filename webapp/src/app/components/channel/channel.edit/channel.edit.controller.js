(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelEditController', ChannelEditController);

  /** @ngInject */
  function ChannelEditController($log, $rootScope, $state, $stateParams, $translate,
                                 NavigationService, ChannelService, ModalService, UserService, HeaderTitleService,
                                 TICKSER_CHANNEL_OWNER_TYPE) {

    $log = $log.getInstance('ChannelEditController');
    HeaderTitleService.setTranslatedTitle('COMMONS_CHANNEL_SETTINGS');

    var ctrl = this;

    ctrl.channel = {};
    ctrl.initialShowRankedList = false;
    ctrl.TICKSER_CHANNEL_OWNER_TYPE = TICKSER_CHANNEL_OWNER_TYPE;
    ctrl.UserService = UserService;

    ctrl.updateChannel = updateChannel;
    ctrl.deactivateChannel = deactivateChannel;
    ctrl.cancel = cancel;

    ChannelService.getUserChannel($stateParams.channelId).then(function (result) {
      ctrl.channel = result.data;
      ctrl.initialShowRankedList = result.data.showRankedList;
    });

    function updateChannel(form) {
      var update = function () {
        ChannelService.updateChannel(ctrl.channel.id, ctrl.channel)
          .success(function () {
            $state.go('main.channels', {}, {reload: true})
          })
          .error(function (err) {
            $log.error(err)
          });
      };

      ctrl.formSubmit = true;

      if (form.$valid) {
        var askConfirmation = ctrl.initialShowRankedList && !ctrl.channel.showRankedList;
        if (!askConfirmation) {
          update();
          return;
        }

        ModalService.confirmationBuilder()
          .confirmationClickOutsideToClose()
          .confirmationTexts({
            title: $translate.instant('CHANNEL_TOPRANKED__RESET_DIALOG__TITLE'),
            body: $translate.instant('CHANNEL_TOPRANKED__RESET_DIALOG__BODY'),
            cancel: $translate.instant('COMMONS_CANCEL'),
            confirm: $translate.instant('COMMONS_OK')
          })
          .confirmationShowModal()
          .then(function () {
            update();
          });
      }
    }

    function deactivateChannel(event) {
      ModalService.confirmationBuilder()
        .confirmationTargetEvent(event)
        .confirmationClickOutsideToClose()
        .confirmationTexts({
          title: $translate.instant('COMMONS_DELETE_CHANNEL'),
          body: $translate.instant('DELETE_CHANNEL_BODY'),
          cancel: $translate.instant('COMMONS_CANCEL'),
          confirm: $translate.instant('COMMONS_DELETE_CHANNEL')
        })
        .confirmationShowModal()
        .then(function () {
          ChannelService.deactivateChannel(ctrl.channel.id)
            .success(function () {
              $rootScope.$broadcast('UserChannelsChange', {});
              $rootScope.$broadcast('AccountInfoChange');
              $state.go('main.channels', {}, {reload: true})
            })
            .error(function (err) {
              $log.error(err)
            })
        })
    }

    function cancel() {
      NavigationService.goToPreviousStateOrProvided('main.channels', null)
    }
  }
})();