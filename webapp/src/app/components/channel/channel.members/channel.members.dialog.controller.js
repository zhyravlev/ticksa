(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelMembersDialogController', ChannelMembersDialogController);

  /** @ngInject */
  function ChannelMembersDialogController($log, $mdDialog, SelectionsService, ChannelService, channel) {

    $log = $log.getInstance('ChannelMembersDialogController');

    var ctrl = this;

    ctrl.invite = {};
    ctrl.ownerTypes = SelectionsService.getOwnerTypes();

    ctrl.inviteToChannel = inviteToChannel;

    function inviteToChannel(form, invite) {
      if (form.$valid) {
        ctrl.loadingInProgress = true;
        ChannelService.inviteToChannel(channel.id, invite)
          .success(function () {
            $mdDialog.hide();
          })
          .error(function (err) {
            ctrl.loadingInProgress = false;
            $log.error(err);
          })
      }
    }
  }
})();
