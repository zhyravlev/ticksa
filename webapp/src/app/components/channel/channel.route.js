(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.channels', {
        url: '/channels',
        templateUrl: 'app/components/channel/channel.list/channel.list.html',
        controller: 'ChannelListController',
        controllerAs: 'ctrl'
      })
      .state('main.channels.topRanked', {
        url: '/ranked/:channelId',
        templateUrl: 'app/components/channel/channel.topRanked/channel.topRanked.html',
        controller: 'ChannelTopRankedController',
        controllerAs: 'ctrl'
      })
      .state('main.channels.voteBoard', {
        url: '/voteBoard/:channelId',
        templateUrl: 'app/components/channel/channel.voteBoard/channel.voteBoard.html',
        controller: 'ChannelVoteBoardController',
        controllerAs: 'ctrl'
      })
      .state('main.channels.edit', {
        url: '/:channelId',
        templateUrl: 'app/components/channel/channel.edit/channel.edit.html',
        controller: 'ChannelEditController',
        controllerAs: 'ctrl'
      })
      .state('main.channels.add', {
        url: '/add/channel',
        templateUrl: 'app/components/channel/channel.add/channel.add.html',
        controller: 'ChannelAddController',
        controllerAs: 'ctrl'
      })
      .state('main.channelContentList', {
        url: '/channels/:channelId/contents',
        templateUrl: 'app/components/channel/channel.content.list/channel.content.list.html',
        controller: 'ChannelContentListController',
        controllerAs: 'ctrl'
      })
      .state('main.channelContentEdit', {
        url: '/channels/:channelId/edit-content',
        templateUrl: 'app/components/channel/channel.content.edit/channel.content.edit.html',
        controller: 'ChannelContentEditController',
        controllerAs: 'ctrl'
      })
  }
})();
