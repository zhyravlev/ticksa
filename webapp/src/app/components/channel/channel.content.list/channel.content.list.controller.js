(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ChannelContentListController', ChannelContentListController);

  /** @ngInject */
  function ChannelContentListController($log, $state, $stateParams, $rootScope, $scope,
                                        ChannelService, ContentService, HeaderTitleService, EVENTS) {

    $log = $log.getInstance('ChannelContentListController');

    var ctrl = this;

    ctrl.rawContents = [];
    ctrl.channelId = $stateParams.channelId;
    ctrl.loadPage = loadPage;
    ctrl.loadMore = loadMore;
    ctrl.loadingInProgress = false;

    var currentPage = 0;
    var pageSize = 12;
    ctrl.loadPage(currentPage, pageSize);

    ChannelService.getChannel(ctrl.channelId)
      .success(function (channel) {
        ctrl.channel = channel;
        HeaderTitleService.setCustomTitle(ctrl.channel.name);
        HeaderTitleService.setChannelSubscribed(ctrl.channel.id, ctrl.channel.name, ctrl.channel.subscribed);

        if (channel.backgroundImage !== null) {
          $rootScope.$broadcast(EVENTS.CHANGE_CHANNEL_BACKGROUND, {
            key: channel.backgroundImage.key
          })
        }

        if (ctrl.channel.allowVoting) {
          ctrl.rawContents.unshift({
            voteBoardCard: true
          });
        }

        if (ctrl.channel.showRankedList) {
          ctrl.rawContents.unshift({
            topRankedCard: true
          });
        }

      })
      .error(function (err, status) {
        if (status == 404) {
          $state.go('main.channels');
        }
        $log.log(err);
      });

    function loadMore() {
      if (ctrl.loadComplete) return;
      loadPage(currentPage, pageSize);
    }

    function loadPage(page, pageSize) {
      ctrl.loadingInProgress = true;

      ContentService.getContentByChannel(ctrl.channelId, page, pageSize)
        .success(function (contents, status) {
          if (contents.length == 0 || contents.length < pageSize) {
            ctrl.loadComplete = true;
          }

          if (contents.length == pageSize) {
            currentPage++;
          }
          contents.forEach(function (content) {
            ctrl.rawContents.push(content);
          });
          ctrl.loadingInProgress = false;

          if (page == 0) {
            if (contents.length > 0) {
              HeaderTitleService.setChanelFirstContentId(contents[0].id);
            } else {
              HeaderTitleService.resetChanelFirstContentId();
            }
          }
        })
        .error(function (err) {
          $log.log(err);
          ctrl.loadingInProgress = false;
        });
    }
  }

})();
