(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('DialogShareContentExtendController', DialogShareContentExtendController);

  /** @ngInject */
  function DialogShareContentExtendController($window, $log, $mdDialog, $timeout, content,
                                              EncodingService, ContentService, ENV) {

    $log = $log.getInstance('DialogShareContentExtendController');

    var ctrl = this;

    var fadeoutTimeoutForLink = null;
    var fadeoutTimeoutForEmail = null;

    ctrl.copied = false;
    ctrl.wasSent = false;
    ctrl.selectURL = false;

    EncodingService.encodeContentAndUserId(content.contentId, content.userId)
      .success(function (data) {
        ctrl.url = ENV.URL + '/s/' + data.contentId + '-' + data.userId;
        ctrl.selectURL = true;
      })
      .error(function (err) {
        $log.error(err)
      });

    ctrl.close = function () {
      $mdDialog.hide()
    };

    ctrl.onCopied = function (e) {
      if (e.action == 'copy') {
        if (!!fadeoutTimeoutForLink) {
          $timeout.cancel(fadeoutTimeoutForLink);
        }

        ctrl.copied = true;
        fadeoutTimeoutForLink = $timeout(function () {
          ctrl.copied = false;
        }, 2500);
      }
    };

    ctrl.shareTwitter = function () {
      $window.open('https://twitter.com/intent/tweet?text=' + ctrl.url, '_blank');
      return true;
    };

    ctrl.shareFacebook = function () {
      FB.ui({
        display: 'iframe',
        method: 'share',
        href: ctrl.url
      }, function (response) {
      });
    };

    ctrl.sendContentCardToMe = function() {
      ContentService.sendContentCardToMe(content.contentId).then(function() {
        if (!!fadeoutTimeoutForEmail) {
          $timeout.cancel(fadeoutTimeoutForEmail);
        }
        ctrl.wasSent = true;
        fadeoutTimeoutForEmail = $timeout(function () {
          ctrl.wasSent = false;
        }, 2500);
      })
    };
  }
})();
