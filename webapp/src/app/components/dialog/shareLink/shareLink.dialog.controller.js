(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ShareLinkDialogController', ShareLinkDialogController);

  /** @ngInject */
  function ShareLinkDialogController($scope, $timeout, $mdDialog, $document,
                                     url, texts) {

    var ctrl = this;
    var fadeoutTimeout = null;

    ctrl.url = url;
    ctrl.copied = false;
    ctrl.texts = texts;

    var select = function () {
      $timeout(function () {
        $document[0].getElementsByName('shareUrl')[0].select();
      }, 0);
    };

    ctrl.select = select;
    ctrl.close = function () {
      $mdDialog.hide();
    };

    select();

    ctrl.onCopied = function (e) {
      if (e.action == 'copy') {
        if (!!fadeoutTimeout) {
          $timeout.cancel(fadeoutTimeout);
        }

        $scope.$apply(function(){
          ctrl.copied = true;
        });

        fadeoutTimeout = $timeout(function () {
          ctrl.copied = false;
        }, 2500);
      }
    };
  }
})();