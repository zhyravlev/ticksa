(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('DialogHelpController', DialogHelpController);

  /** @ngInject */
  function DialogHelpController($mdDialog, texts) {

    var ctrl = this;

    ctrl.texts = texts;

    ctrl.close = function () {
      $mdDialog.hide()
    }
  }
})();