(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ContentListController', ContentListController);

  /** @ngInject */
  function ContentListController($scope, $log, $q, $window, lodash, $translate,
                                 ContentService, SearchService, CategoryFilterService, LanguageFilterService,
                                 UserService, Pageable, HeaderFilterService) {

    $log = $log.getInstance('ContentListController');

    var ctrl = this;

    var currentPage = 0;
    var pageSize = $window.innerHeight < 900 ? 6 : 12;
    var searchString = '';
    var pageable = null;
    var pageInitialized = false;

    ctrl.rawContents = [{form: true}];
    ctrl.loadingInProgress = false;
    ctrl.disabledInfiniteScroll = false;
    ctrl.loadComplete = false;
    ctrl.showFilters = false;
    ctrl.notFound = false;
    ctrl.categories = [];
    ctrl.languages = [];
    ctrl.selectedCategories = [];
    ctrl.selectedLanguages = [];

    ctrl.filterLanguageLabel = $translate.instant('COMMONS_LANGUAGE');
    ctrl.filterCategoryLabel = $translate.instant('COMMONS_CATEGORY');

    ctrl.refreshPage = refreshPage;
    ctrl.loadMore = loadMore;
    ctrl.disabledFilter = disabledFilter;

    var unRegisterSearchObserver = SearchService.registerObserver(function (data) {
      searchString = data.searchString;

      if (data.enableSearchBar) {  // such check helps prevent unnecessary contents query when ui-router state changes
        refreshPage();
      }
    });

    var unRegisterFilterObserver = HeaderFilterService.registerObserver(function (data) {
      ctrl.showFilters = data.in;
    });

    $scope.$on('$destroy', function () {
      unRegisterSearchObserver();
      unRegisterFilterObserver();
    });

    function refreshPage() {
      ctrl.rawContents = [{form: true}];
      ctrl.loadComplete = false;
      ctrl.notFound = false;
      currentPage = 0;
      HeaderFilterService.disabledFilter();
      loadPage();
    }

    function loadMore() {
      if (ctrl.loadComplete) {
        return;
      }
      loadPage();
    }

    function loadPage() {

      ctrl.loadingInProgress = true;
      ctrl.disabledInfiniteScroll = true;

      var initFilters = pageInitialized ? $q.resolve() : initPageFilters();
      pageInitialized = true;

      initFilters.then(function () {
        return getContent();
      }).then(function (res) {
        var contents = res.data;

        if (contents.length == 0 || contents.length < pageSize) {
          ctrl.loadComplete = true;
        }

        if (contents.length == 0 && currentPage == 0) {
          ctrl.notFound = true;
        }

        if (contents.length == pageSize) {
          currentPage++;
        }

        lodash.forEach(contents, function (content) {
          ctrl.rawContents.push(content);
        });

        ctrl.loadingInProgress = false;
        ctrl.disabledInfiniteScroll = false;

      }).catch(function (err) {

        $log.error(err);
        ctrl.loadingInProgress = false;

      }).finally(function () {

        if (!lodash.isEmpty(ctrl.selectedCategories) || !lodash.isEmpty(ctrl.selectedLanguages)) {
          HeaderFilterService.setUse(true);
        } else {
          HeaderFilterService.setUse(false);
        }

        UserService.setCategoryFilter(lodash.pluck(ctrl.selectedCategories, 'id'));
        UserService.setLanguageFilter(lodash.pluck(ctrl.selectedLanguages, 'id'));
      });
    }

    function getContent() {
      pageable = new Pageable()
        .addArrayFilter('languages', lodash.pluck(ctrl.selectedLanguages, 'id'))
        .addArrayFilter('categories', lodash.pluck(ctrl.selectedCategories, 'id'))
        .searchString(searchString)
        .page(currentPage)
        .pageSize(pageSize)
        .build();

      return ContentService.getContents(pageable);
    }

    function initPageFilters() {
      return $q.all([CategoryFilterService.getCategories(), LanguageFilterService.getLanguages()]).then(function (result) {
        ctrl.categories = result[0];
        ctrl.selectedCategories = lodash.filter(ctrl.categories, function (c) {
          return lodash.includes(UserService.getCategoryFilter(), c.id);
        });

        ctrl.languages = result[1];
        ctrl.selectedLanguages = lodash.filter(ctrl.languages, function (l) {
          return lodash.includes(UserService.getLanguageFilter(), l.id);
        });
      });
    }

    function disabledFilter() {
      return HeaderFilterService.disabledFilter();
    }
  }

})();
