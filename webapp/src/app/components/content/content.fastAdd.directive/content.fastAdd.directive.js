(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkContentFastAdd', tkContentFastAdd);

  /** @ngInject */
  function tkContentFastAdd() {
    return {
      controllerAs: 'ctrl',
      templateUrl: 'app/components/content/content.fastAdd.directive/content.fastAdd.directive.html',
      controller: function ($state, $rootScope, $stateParams, UserService, NavigationService, CONTENT_TYPES, CHANNEL_UNKNOWN) {

        var ctrl = this;

        ctrl.CONTENT_TYPES = CONTENT_TYPES;
        ctrl.addContent = addContent;

        function addContent(type) {
          var channelId = UserService.getDefaultChannelId() ? UserService.getDefaultChannelId() : CHANNEL_UNKNOWN;

          if (UserService.isEmpty()) {
            NavigationService.setNextState('main.contentAdd', {
              type: type,
              channelId: channelId
            });
            NavigationService.goToLoginPage();
          } else {
            $state.go('main.contentAdd', {
              type: type,
              channelId: channelId
            });
          }
        }

      }
    }

  }
})();
