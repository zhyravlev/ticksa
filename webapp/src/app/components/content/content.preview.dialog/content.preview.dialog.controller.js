(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ContentPreviewDialogController', ContentPreviewDialogController);

  /** @ngInject */
  function ContentPreviewDialogController($mdDialog, content) {
    var ctrl = this;
    ctrl.content = content;

    ctrl.close = function () {
      $mdDialog.hide()
    }
  }
})();