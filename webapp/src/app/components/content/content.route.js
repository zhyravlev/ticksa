(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider

      .state('main.contentList', {
        url: '/contents',
        templateUrl: 'app/components/content/content.list/content.list.html',
        controller: 'ContentListController',
        controllerAs: 'ctrl'
      })
      .state('main.contentView', {
        url: '/channels/:channelId/contents/:contentId?:list',
        templateUrl: 'app/components/content/content.view/content.view.html',
        controller: 'ContentViewController',
        controllerAs: 'ctrl'
      })
      .state('main.contentAdd', {
        url: '/channels/:channelId/add-content/:type',
        templateUrl: 'app/components/content/content.add/content.add.html',
        controller: 'ContentAddController',
        controllerAs: 'ctrl'
      })
      .state('main.contentEdit', {
        url: '/channels/:channelId/contents/:contentId/edit',
        templateUrl: 'app/components/content/content.edit/content.edit.html',
        controller: 'ContentEditController',
        controllerAs: 'ctrl'
      })
  }
})();
