(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkContentCondition', tkContentCondition);

  /** @ngInject */
  function tkContentCondition($timeout, ContentService, CONTENT_CONDITION_TYPE) {

    return {
      restrict: 'E',
      templateUrl: 'app/components/content/directive/tk.content.condition.directive.html',
      require: ['ngModel', '^form'],
      scope: {
        conditions: '=',
        onRemoveListener: '='
      },
      controller: function ($scope) {

      },
      link: function (scope, iElement, iAttrs, ctrls) {
        var ngModelCtrl = ctrls[0];
        var formCtrl = ctrls[1];

        scope.types = CONTENT_CONDITION_TYPE;
        scope.contentName = iAttrs['name'] + '-content';
        scope.typeName = iAttrs['name'] + '-type';
        scope.form = formCtrl;


        ngModelCtrl.$render = function () {
          scope.condition = ngModelCtrl.$modelValue;
        };

        scope.simulateQuery = true;
        scope.isDisabled = false;

        scope.searchText = "";
        // list of `state` value/display objects
        scope.querySearch = querySearch;
        scope.selectedItemChange = selectedItemChange;
        scope.fireRemoveListener = fireRemoveListener;

        $timeout(clearInputBoxOnBlur, 0);

        function clearInputBoxOnBlur() {
          iElement.find("#input-search").bind("blur", function () {
            if (scope.condition.content) return;
            angular.element(this).val('');
            formCtrl[scope.contentName].$setValidity('required', false);
          });
        }

        // ******************************
        // Internal methods
        // ******************************
        /**
         * Search for states... use $timeout to simulate
         * remote dataservice call.
         */
        function querySearch(query) {
          return ContentService.getContentSearch(query).then(function (res) {
            return res.data;
          });
        }

        function selectedItemChange(item) {
          if (!item) {
            return;
          }

          scope.condition.content = item;
          ngModelCtrl.$setViewValue(scope.condition);
          formCtrl[scope.contentName].$setValidity('required', true);
        }

        function fireRemoveListener() {
          scope.onRemoveListener(scope.condition);
        }
      }
    }
  }
})();
