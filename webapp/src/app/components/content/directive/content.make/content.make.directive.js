(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkContentMake', tkContentMake);

  /** @ngInject */
  function tkContentMake($log, $mdDialog, $stateParams, $timeout, $translate, $state, $rootScope, $document, lodash,
                         LibraryService, ContentService, NavigationService, LanguageFilterService, CategoryFilterService,
                         ChannelService, HeaderTitleService, PaymentConfirmationService, UserService, CountryService, TkDialogFactory,
                         CONTENT_TYPES, PRICES, TICKSER_PREFERENCES, CONTENT_CONDITION_TYPE) {

    $log = $log.getInstance('tkContentMake');

    return {
      restrict: 'E',
      templateUrl: 'app/components/content/directive/content.make/content.make.directive.html',
      link: function (scope) {

        scope.selectedTeaserImage = null;
        scope.selectedTeaserVideo = null;
        scope.selectedImages = [];
        scope.selectedVideo = null;
        scope.formSubmit = false;
        scope.CONTENT_TYPES = CONTENT_TYPES;
        scope.errorList = {};

        scope.selectedLanguageId = null;
        scope.selectedCategoryId = null;
        scope.languages = [];
        scope.categories = [];

        scope.countries = [];

        scope.content = {
          campaign: false,
          tickPrice: 10,
          teaserText: '',
          tags: [],
          type: parseInt($stateParams.type),
          geographicCountries: [],
          conditions: []
        };
        scope.geographicLimitEnable = false;
        scope.changeGeographicLimitEnable = changeGeographicLimitEnable;

        scope.filterCountrySelectedLabel = $translate.instant('FILTER__COUNTRY_SELECTED');
        scope.filterCountryUnselectedLabel = $translate.instant('FILTER__COUNTRY_UNSELECTED');

        scope.isAdd = false;
        scope.isEdit = false;

        scope.selectedChannel = {
          id: $stateParams.channelId
        };

        scope.preview = preview;
        scope.selectTeaserImage = selectTeaserImage;
        scope.selectTeaserVideo = selectTeaserVideo;
        scope.removeSelectedImage = removeSelectedImage;
        scope.selectImage = selectImage;
        scope.selectVideo = selectVideo;
        scope.campaignDialog = campaignDialog;
        scope.cancel = cancel;
        scope.addContent = addContent;
        scope.editContent = editContent;
        scope.languages = [];
        scope.categories = [];

        ////////////////
        // CONDITIONS //
        ////////////////

        scope.addCondition = addCondition;
        scope.onRemoveCondition = onRemoveCondition;
        scope.clearAllConditions = clearAllConditions;

        scope.isConditions = false;
        scope.$watch('isConditions', function (a) {
          if (a === true && scope.content.conditions.length === 0) {
            return addCondition();
          }
        });

        function clearAllConditions() {
          scope.content.conditions = [];
        }

        function addCondition() {
          scope.content.conditions.push({
            type: CONTENT_CONDITION_TYPE.PURCHASE,
            content: null
          });
        }

        function onRemoveCondition(condition) {
          lodash.remove(scope.content.conditions, condition);
        }

        // --------------------------------------------------------------

        CountryService.getCountries()
          .then(function (countries) {
            var countryKeys = lodash.pluck(countries, 'codeA2');
            countryKeys.forEach(function (countryKey) {
              var translateId = 'COUNTRY_' + countryKey.toUpperCase();
              var countryName = $translate.instant(translateId);
              if (translateId !== countryName) {
                scope.countries.push({
                  key: countryKey,
                  name: countryName
                });
              } else {
                $log.warn('Translate id: "' + translateId + '" not found!');
              }
            });
          });

        if ($stateParams.contentId) {
          scope.isEdit = true;
          ContentService.getContentById($stateParams.channelId, $stateParams.contentId).then(function (res) {
            scope.content = res.data;
            scope.isConditions = !lodash.isEmpty(scope.content.conditions);
            scope.geographicLimitEnable = !lodash.isEmpty(scope.content.geographicCountries);

            // TODO [dborisov] what does this piece of code do?
            if (angular.isDefined(scope.content.tickPrice)) {
              scope.content.tickPrice = Number(scope.content.tickPrice);
            }

            if (scope.content.type == CONTENT_TYPES.IMAGE) {
              scope.selectedImages = lodash.map(scope.content.contentImages, function (contentImage) {
                return {
                  name: contentImage.name,
                  key: contentImage.key,
                  id: contentImage.imageId
                }
              })
            }

            if (scope.content.type == CONTENT_TYPES.VIDEO) {
              scope.selectedVideo = scope.content.contentVideo ? scope.content.contentVideo : null
            }

            scope.selectedTeaserVideo = scope.content.teaserVideo ? scope.content.teaserVideo : null;
            scope.selectedTeaserImage = scope.content.teaserImage ? {
              name: scope.content.teaserImage.name,
              key: scope.content.teaserImage.key,
              id: scope.content.teaserImage.imageId
            } : null;

            LanguageFilterService.getLanguages().then(function (languages) {
              scope.languages = languages;
            });

            CategoryFilterService.getCategories().then(function (categories) {
              scope.categories = categories;
            });

            scope.selectedLanguageId = scope.content.language.id;
            scope.selectedCategoryId = scope.content.category.id;

          }).catch(function (err) {
            $log.error(err);
            if (err.status == 404) {
              $state.go('main.contentList')
            }
          });
        } else {
          contentPreBuild(scope.content.type);
          scope.isAdd = true;
        }

        function preview() {
          $mdDialog.show({
            templateUrl: 'app/components/content/content.preview.dialog/content.preview.dialog.html',
            controller: 'ContentPreviewDialogController',
            controllerAs: 'ctrl',
            clickOutsideToClose: true,
            locals: {
              content: scope.content
            }
          });
        }

        function selectTeaserImage() {

          var images = lodash.map(scope.images, function (image) {
            image.selected = scope.selectedTeaserImage ? scope.selectedTeaserImage.id == image.id : false;
            return image;
          });

          var tkDialog = new TkDialogFactory();
          tkDialog.controller('ImagePickerModalController');
          tkDialog.templateUrl('app/components/library/image/image.picker.modal.html');
          tkDialog.locals({images: images, multiple: false});
          tkDialog.show().then(function (data) {
            if (angular.isDefined(data.selectedImage)) {

              scope.content.teaserImage = {
                key: data.selectedImage.key,
                name: data.selectedImage.name
              };
              scope.selectedTeaserImage = data.selectedImage;

              scope.selectedTeaserVideo = null;
              scope.content.teaserVideo = null;
              scope.content.teaserVideoId = null
            }
          })
        }

        function selectTeaserVideo() {
          var tkDialog = new TkDialogFactory();
          tkDialog.controller('VideoPickerModalController');
          tkDialog.templateUrl('app/components/library/video/video.picker.modal.html');
          tkDialog.locals({videos: scope.videos});
          tkDialog.show().then(function (data) {
            if (angular.isDefined(data.selectedVideo)) {

              scope.content.teaserVideo = data.selectedVideo;
              scope.selectedTeaserVideo = data.selectedVideo;

              scope.content.teaserImage = null;
              scope.content.teaserImageId = null;
              scope.selectedTeaserImage = null
            }
          })
        }

        function removeSelectedImage(image) {
          scope.selectedImages = scope.selectedImages.filter(function (selectedImage) {
            return selectedImage.id != image.id;
          })
        }

        function selectImage() {

          var images = lodash.map(scope.images, function (image) {
            image.selected = false;
            lodash.forEach(scope.selectedImages, function (selectedImage) {
              if (image.id == selectedImage.id) {
                image.selected = true;
              }
            });
            return image;
          });

          var tkDialog = new TkDialogFactory();
          tkDialog.controller('ImagePickerModalController');
          tkDialog.templateUrl('app/components/library/image/image.picker.modal.html');
          tkDialog.locals({images: images, multiple: true});
          tkDialog.show().then(function (data) {
            if (angular.isDefined(data.images)) {
              scope.images = data.images;
              scope.selectedImages = lodash.filter(data.images, function (image) {
                return image.selected
              })
            }
          })
        }

        function selectVideo() {
          var tkDialog = new TkDialogFactory();
          tkDialog.controller('VideoPickerModalController');
          tkDialog.templateUrl('app/components/library/video/video.picker.modal.html');
          tkDialog.locals({videos: scope.videos});
          tkDialog.show().then(function (data) {
            if (angular.isDefined(data.selectedVideo)) {
              scope.selectedVideo = data.selectedVideo;
              checkContentPrice(scope.content);
              scope.errorList.VIDEO = false;

              $timeout(function () {
                if (data.change) {
                  LibraryService.getVideos()
                    .success(function (data) {
                      scope.videos = data
                    })
                    .error(function (err) {
                      $log.error(err)
                    })
                }
              })
            }
          })
        }

        function checkContentPrice(content) {
          if (scope.selectedVideo !== null) {
            var size = lodash.sum(scope.selectedVideo.videoFiles, function (file) {
                return file.size;
              }) / scope.selectedVideo.videoFiles.length / (1024 * 1024);

            if (size < 200) {
              if (content.tickPrice < 1) {
                scope.content.tickPrice = 1;
                showMinPriceDialog(scope.content.tickPrice);
                return false;
              }
            }
            else if (Math.floor(size / 200.0) * 10 > content.tickPrice) {
              scope.content.tickPrice = Math.floor(size / 200.0) * 10;
              showMinPriceDialog(scope.content.tickPrice);
              return false;
            }
          }

          return true;
        }

        function showMinPriceDialog(price) {
          $mdDialog.show(
            $mdDialog.alert({
              title: $translate.instant('CONTENT_ADD_EDIT__MIN_PRICE__TITLE'),
              textContent: $translate.instant('CONTENT_ADD_EDIT__MIN_PRICE__BODY', {amount: price}),
              ok: $translate.instant('COMMONS_OK')
            })
          );
        }

        function campaignDialog() {
          $mdDialog.show({
            templateUrl: 'app/components/whatIsThis/whatIsThis.content.campaign/whatIsThis.content.campaign.html',
            controller: 'WhatIsThisContentCampaignController',
            controllerAs: 'ctrl',
            clickOutsideToClose: true
          });
        }

        function contentPreBuild(type) {

          scope.content.contentText = undefined;
          scope.content.contentImage = undefined;
          scope.content.contentLink = undefined;
          scope.content.contentVideo = undefined;

          if (type == CONTENT_TYPES.TEXT) {
            scope.content.contentText = {}
          } else if (type == CONTENT_TYPES.LINK) {
            scope.content.contentLink = {
              text: ''
            }
          }

          LanguageFilterService.getLanguages().then(function (languages) {
            scope.languages = languages;
            scope.selectedLanguageId = lodash.findWhere(scope.languages, {name: 'English'}).id;
          });

          CategoryFilterService.getCategories().then(function (categories) {
            scope.categories = categories;
            scope.selectedCategoryId = lodash.findWhere(scope.categories, {name: 'Music'}).id;
          });
        }

        function cancel() {
          NavigationService.goToPreviousStateOrProvided('main.channelContentList', null);
        }

        function addContent(form) {
          var error = false;
          scope.errorList = {};
          scope.formSubmit = true;

          if (!isValidGeographicFencing()) {
            return;
          }

          // Any content type
          if (
            (scope.content.teaserImageId = (scope.selectedTeaserImage !== null) ? scope.selectedTeaserImage.id : null) === null
            &&
            (scope.content.teaserVideoId = (scope.selectedTeaserVideo !== null) ? scope.selectedTeaserVideo.id : null) === null
          ) {
            error = true;
            scope.errorList.IMAGE_OR_VIDEO_PREVIEW = true;
          }

          if (scope.content.type === CONTENT_TYPES.IMAGE && scope.selectedImages.length === 0) {
            error = true;
            scope.errorList.IMAGE = true;
          } else if (scope.content.type === CONTENT_TYPES.IMAGE && scope.selectedImages.length > 0) {
            scope.content.contentImages = lodash.map(scope.selectedImages, function (image, index) {
              return {
                imageId: image.id,
                position: index
              }
            })
          }

          if (scope.content.type === CONTENT_TYPES.VIDEO && scope.selectedVideo === null) {
            error = true;
            scope.errorList.VIDEO = true;
          } else if (scope.content.type === CONTENT_TYPES.VIDEO && scope.selectedVideo !== null) {
            scope.content.contentVideo = {
              videoLength: scope.selectedVideo.durationInMillis,
              videoId: scope.selectedVideo.id
            }
          }

          if (form.$valid && !error) {

            if (!checkContentPrice(scope.content)) {
              return false;
            }

            PaymentConfirmationService.confirmTicksAction(PRICES.ADD_CONTENT, TICKSER_PREFERENCES.SKIP_ADD_CONTENT_NOTIFICATION).then(function () {

              delete scope.content.language;
              delete scope.content.category;
              scope.content.languageId = scope.selectedLanguageId;
              scope.content.categoryId = scope.selectedCategoryId;
              scope.content.geographicCountries = !scope.geographicLimitEnable ? null :
                lodash.map(scope.content.geographicCountries, function (c) {
                  return {key: c.key}
                });

              if (!scope.isConditions) {
                clearAllConditions();
              } else {
                scope.content.conditions = lodash.filter(scope.content.conditions, function (c) {
                  return c.content != null;
                });
              }

              ContentService.postContent(scope.selectedChannel.id, scope.content)
                .success(function (res) {
                  $mdDialog.show({
                    closeTo: angular.element($document[0].body),
                    templateUrl: 'app/components/dialog/shareContent.extend/dialog.shareContent.extend.html',
                    controller: 'DialogShareContentExtendController',
                    controllerAs: 'ctrl',
                    clickOutsideToClose: true,
                    escapeToClose: true,
                    locals: {
                      content: {
                        contentId: res.contentId,
                        userId: res.tickserId
                      }
                    }
                  }).finally(function () {
                    $rootScope.$broadcast('TicksTransaction', {});
                    NavigationService.goToPreviousStateOrProvided('main.channelContentEdit', {channelId: scope.selectedChannel.id});
                  })
                })
                .error(function (err) {
                  $log.error(err);
                })
            })
          }
        }

        function editContent(form) {
          var error = false;
          scope.errorList = {};
          scope.formSubmit = true;

          if (!isValidGeographicFencing()) {
            return;
          }

          if (scope.content.type === CONTENT_TYPES.IMAGE && scope.selectedImages.length === 0) {
            error = true;
            scope.errorList.IMAGE = true;
          } else if (scope.content.type === CONTENT_TYPES.IMAGE && scope.selectedImages.length > 0) {
            scope.content.contentImages = lodash.map(scope.selectedImages, function (image, index) {
              return {
                imageId: image.id,
                position: index
              }
            })
          }

          if (scope.content.type === CONTENT_TYPES.VIDEO && scope.selectedVideo === null) {
            error = true;
            scope.errorList.VIDEO = true;
          } else if (scope.content.type === CONTENT_TYPES.VIDEO && scope.selectedVideo !== null) {
            scope.content.contentVideo = {
              videoLength: scope.selectedVideo.durationInMillis,
              videoId: scope.selectedVideo.id
            }
          }

          if (
            (scope.content.teaserImageId = (scope.selectedTeaserImage !== null) ? scope.selectedTeaserImage.id : null) === null
            &&
            (scope.content.teaserVideoId = (scope.selectedTeaserVideo !== null) ? scope.selectedTeaserVideo.id : null) === null
          ) {
            error = true;
            scope.errorList.IMAGE_OR_VIDEO_PREVIEW = true;
          }

          if (form.$valid && !error) {

            if (!checkContentPrice(scope.content)) {
              return false;
            }

            scope.content.channelId = scope.selectedChannel.id;

            delete scope.content.language;
            delete scope.content.category;
            scope.content.languageId = scope.selectedLanguageId;
            scope.content.categoryId = scope.selectedCategoryId;
            scope.content.geographicCountries = !scope.geographicLimitEnable ? null :
              lodash.map(scope.content.geographicCountries, function (c) {
                return {key: c.key}
              });

            if (!scope.isConditions) {
              clearAllConditions();
            } else {
              scope.content.conditions = lodash.filter(scope.content.conditions, function (c) {
                return c.content != null;
              });
            }

            ContentService.updateContent(scope.content.channelId, $stateParams.contentId, scope.content)
              .success(function () {
                $mdDialog.show({
                  closeTo: angular.element($document[0].body),
                  templateUrl: 'app/components/dialog/shareContent.extend/dialog.shareContent.extend.html',
                  controller: 'DialogShareContentExtendController',
                  controllerAs: 'ctrl',
                  clickOutsideToClose: true,
                  escapeToClose: true,
                  locals: {
                    content: {
                      contentId: $stateParams.contentId,
                      userId: UserService.getId()
                    }
                  }
                }).finally(function () {
                  $state.go('main.channelContentEdit', {channelId: scope.content.channelId})
                });
              })
              .error(function (err) {
                $log.error(err);
              })
          }
        }

        function isValidGeographicFencing() {
          if (!scope.geographicLimitEnable) {
            return true;
          }

          if (scope.content.geographicCountries.length > 0) {
            return true;
          }

          $mdDialog.show({
            templateUrl: 'app/components/dialog/help/dialog.help.html',
            controller: 'DialogHelpController',
            controllerAs: 'ctrl',
            clickOutsideToClose: true,
            locals: {
              texts: {
                title: $translate.instant('COMMONS_ERROR_DIALOG_TITLE'),
                body: $translate.instant('ERROR_GEOGRAPHIC_FENCING_MUST_BE_SELECTED_ONE_COUNTRY')
              }
            }
          });

          return false;
        }

        function changeGeographicLimitEnable() {
          scope.content.geographicCountries = scope.geographicLimitEnable ? [] : null;
        }

        scope.$watch('selectedChannel', function (value) {
          ChannelService.getChannel(value.id)
            .success(function (channel) {
              HeaderTitleService.setCustomTitle(channel.name);
              scope.content.previewChannelName = channel.name;
            })
            .error(function (err) {
              $log.error(err)
            })
        }, true);

        LibraryService.getVideos()
          .success(function (data) {
            scope.videos = data
          })
          .error(function (err) {
            $log.error(err)
          });

        LibraryService.getImages()
          .success(function (data) {
            scope.images = data
          })
          .error(function (err) {
            $log.error(err)
          });

        ChannelService.getUserChannels()
          .success(function (data) {
            scope.channels = data;
          })
          .error(function (err) {
            $log.error(err);
          });


      }
    }
  }
})();
