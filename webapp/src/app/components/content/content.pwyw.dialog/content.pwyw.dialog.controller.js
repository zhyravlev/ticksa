(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ContentPwywDialogController', ContentPwywDialogController);

  /** @ngInject */
  function ContentPwywDialogController($mdDialog, $timeout, $document) {
    var ctrl = this;
    ctrl.enteredTicks = null;
    ctrl.pwywContent = pwywContent;
    ctrl.cancel = cancel;

    $timeout(function () {
      $document[0].getElementsByName('enteredTicks')[0].focus();
    }, 0);

    function cancel() {
      $mdDialog.cancel();
    }

    function pwywContent(form) {
      if(form.$valid) {
        $mdDialog.hide(ctrl.enteredTicks);
      }
    }
  }

})();
