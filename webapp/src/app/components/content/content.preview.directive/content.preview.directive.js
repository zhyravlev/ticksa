(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkContentPreview', tkContentPreview);

  /** @ngInject */
  function tkContentPreview() {

    return {
      controllerAs: 'ctrl',
      templateUrl: 'app/components/content/content.preview.directive/content.preview.directive.html',
      scope: {
        content: '=',
        cardAsPreview: '@',
        cardInDiscovery: '@'
      },
      controller: function ($scope, $mdDialog, $log, $rootScope, $state, $stateParams, $translate, lodash, $timeout, $document, $sanitize,
                            API, CONTENT_TYPES, USER_STATUS, EVENTS,
                            PaymentConfirmationService, ChannelService, ContentService, LinkContentService, UserService,
                            NavigationService, ModalService, VoteBoardService) {

        $log = $log.getInstance('tkContentPreview');

        var ctrl = this;

        ctrl.CONTENT_TYPES = CONTENT_TYPES;
        ctrl.UserService = UserService;
        ctrl.content = $scope.content;
        ctrl.cardAsPreview = $scope.cardAsPreview != null;
        ctrl.cardInDiscovery = $scope.cardInDiscovery != null;
        ctrl.showVideo = false;
        ctrl.isAuth = !UserService.isEmpty();
        ctrl.contentIsFree = $scope.content.tickPrice == 0;

        ctrl.publishContent = publishContent;
        ctrl.unPublishContent = unPublishContent;
        ctrl.extendExpiryDate = extendExpiryDate;
        ctrl.flagAsInappropriate = flagAsInappropriate;
        ctrl.payForContent = payForContent;
        ctrl.editContent = editContent;
        ctrl.subscribeToChannel = subscribeToChannel;
        ctrl.unsubscribeToChannel = unsubscribeToChannel;
        ctrl.viewContent = viewContent;
        ctrl.voteContent = voteContent;
        ctrl.sanitizeText = sanitizeText;

        var unregisterSubscriptionsListener = angular.noop;
        var unregisterVoteBoardObserver = VoteBoardService.registerObserver(function (data) {
          if ('id' in data && 'likes' in data) {
            if (data.id == $scope.content.id) {
              $scope.content.likes = data.likes;
            }
          }
        });

        if (!ctrl.cardAsPreview) {
          checkSubscriptions();
          unregisterSubscriptionsListener = $rootScope.$on(EVENTS.SUBSCRIPTIONS_LOADED, checkSubscriptions);
        }

        $scope.$on('$destroy', function () {
          unregisterSubscriptionsListener();
          unregisterVoteBoardObserver();
        });

        function checkSubscriptions() {
          if (ctrl.content.channel) {
            var subscribed = lodash.findWhere($rootScope.subscriptions, {id: ctrl.content.channel.id});
            ctrl.content.channel.subscribed = !!subscribed;
          }
        }

        function subscribeToChannel(channel) {
          ChannelService.subscribeToChannel(channel.id)
            .success(function () {
              channel.subscriberCount += 1;
              channel.subscribed = true;
              $rootScope.$broadcast('SubscriptionChange', {type: 1, channel: channel});
            })
            .error(function (err) {
              $log.log(err);
            })
        }

        function unsubscribeToChannel(channel) {
          ChannelService.unsubscribeToChannel(channel.id)
            .success(function () {
              channel.subscriberCount -= 1;
              channel.subscribed = false;
              $rootScope.$broadcast('SubscriptionChange', {type: 0, channel: channel});
            })
            .error(function (err) {
              $log.log(err);
            });
        }

        function flagAsInappropriate(content, $event) {
          $mdDialog.show({
            closeTo: angular.element($document[0].body),
            parent: $document[0].body,
            targetEvent: $event,
            templateUrl: 'app/components/common/feedback/flag.as.inappropriate/flag.as.inappropriate.dialog.html',
            controller: 'FlagAsInappropriateController',
            controllerAs: 'ctrl',
            clickOutsideToClose: true,
            escapeToClose: true,
            locals: {
              id: content.id,
              entity: 'content'
            }
          });
        }

        function viewContent(content) {
          LinkContentService.openBlankWindowIfNeeded(content);

          if ($stateParams.playlistId > 0) {
            return $state.go('main.contentView', {
              channelId: content.channel.id,
              contentId: content.id,
              list: $stateParams.playlistId
            });
          }

          return $state.go('main.contentView', {
            channelId: content.channel.id,
            contentId: content.id
          });
        }

        function payForContent(content) {
          if (!UserService.isEmpty()) {
            LinkContentService.openBlankWindowIfNeeded(content);
            ContentService.payForContent(content.id)
              .success(function () {
                $rootScope.$broadcast('TicksTransaction', {});
                $state.go('main.contentView', {channelId: content.channel.id, contentId: content.id});
              })
              .error(function (err) {
                LinkContentService.closeBlankWindowIfNeeded(content);
                $log.error(err);
              });
          } else {
            if (ctrl.cardInDiscovery) {
              NavigationService.setNextState('base.teaser', {
                channelId: content.channel.id,
                contentId: content.id
              });
            }

            NavigationService.goToLoginPage();
          }
        }

        function editContent(content) {
          $state.go('main.contentEdit', {channelId: content.channel.id, contentId: content.id});
        }

        function extendExpiryDate(content) {
          ContentService.extendContent(content.id)
            .success(function (data) {
              ctrl.content.expiresAt = data.expiresAt;
              $rootScope.$broadcast('TicksTransaction', {});
            })
            .error(function (err) {
              $log.error(err);
            });
        }

        function publishContent(content) {
          ContentService.publishContent(content.channel.id, content.id)
            .success(function () {
              ctrl.content.published = true;
            })
            .error(function (err) {
              $log.error(err);
            });
        }

        function unPublishContent(content) {
          ContentService.unpublishContent(content.channel.id, content.id)
            .success(function () {
              ctrl.content.published = false;
            })
            .error(function (err) {
              $log.error(err);
            });
        }

        function voteContent(content) {
          if (UserService.isEmpty()) return;
          ModalService.showPwywContentModal(content).then(function (enteredTicks) {
            ContentService.likeContent(content.id, enteredTicks)
              .success(function () {
                var likes = content.likes + enteredTicks;
                VoteBoardService.broadcast(content.id, likes);
                $rootScope.$broadcast('TicksTransaction', {});
              })
              .error(function (err) {
                $log.error(err);
              })
          });
        }

        function sanitizeText(str) {
          return $sanitize(str.replace(/\n/g, '<br/>'));
        }
      }
    }
  }
})();
