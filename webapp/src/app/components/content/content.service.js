(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('ContentService', ContentService);

  /** @ngInject */
  function ContentService(API, $http) {

    return {
      postContent: postContent,
      getContents: getContents,
      getContentByChannel: getContentByChannel,
      getContentByChannelForUser: getContentByChannelForUser,
      payForContent: payForContent,
      getContentById: getContentById,
      publishContent: publishContent,
      unpublishContent: unpublishContent,
      likeContent: likeContent,
      dislikeContent: dislikeContent,
      refundContent: refundContent,
      extendContent: extendContent,
      deactivateContent: deactivateContent,
      updateContent: updateContent,
      getContentTeaser: getContentTeaser,
      getContentTeaserNotLoggedIn: getContentTeaserNotLoggedIn,
      sendContentCardToMe: sendContentCardToMe,
      getContentHtmlCard: getContentHtmlCard,
      getDiscoveryContents: getDiscoveryContents,
      getContentSearch: getContentSearch,
    };

    function getContentByChannel(channelId, page, pageSize) {
      return $http.get(API.URL + '/channels/' + channelId + '/contents', {params: {page: page, pageSize: pageSize}});
    }

    function getContentByChannelForUser(channelId, page, pageSize) {
      return $http.get(API.URL + '/user/channels/' + channelId + '/contents', {
        params: {
          page: page,
          pageSize: pageSize
        }
      });
    }

    function getContents(filter) {
      return $http.get(API.URL + '/contents', {params: filter});
    }

    function getDiscoveryContents(filter) {
      return $http.get(API.URL + '/discovery', {params: filter});
    }

    function getContentTeaser(channelId, contentId) {
      return $http.get(API.URL + '/channels/' + channelId + '/contents/' + contentId + '/teaser');
    }

    function getContentTeaserNotLoggedIn(channelId, contentId) {
      return $http.get(API.URL + '/teaser/channels/' + channelId + '/contents/' + contentId);
    }

    function postContent(channelId, content) {
      return $http.post(API.URL + '/user/channels/' + channelId + '/contents', content);
    }

    function updateContent(channelId, contentId, content) {
      return $http.put(API.URL + '/user/channels/' + channelId + '/contents/' + contentId, content);
    }

    function payForContent(contentId) {
      return $http.post(API.URL + '/contents/' + contentId + '/pay', null);
    }

    function getContentById(channelId, contentId) {
      return $http.get(API.URL + '/channels/' + channelId + '/contents/' + contentId);
    }

    function publishContent(channelId, contentId) {
      return $http.post(API.URL + '/user/channels/' + channelId + '/contents/' + contentId + '/publish', null);
    }

    function unpublishContent(channelId, contentId) {
      return $http.post(API.URL + '/user/channels/' + channelId + '/contents/' + contentId + '/unpublish', null);
    }

    function likeContent(contentId, numberOfLikes) {
      return $http.post(API.URL + '/contents/' + contentId + '/like', {likes: numberOfLikes});
    }

    function dislikeContent(contentId, numberOfDislikes) {
      return $http.post(API.URL + '/contents/' + contentId + '/dislike', {dislikes: numberOfDislikes});
    }

    function refundContent(contentId) {
      return $http.post(API.URL + '/contents/'  + contentId + '/refund');
    }

    function extendContent(contentId) {
      return $http.post(API.URL + '/contents/' + contentId + '/extend', null);
    }

    function deactivateContent(channelId, contentId) {
      return $http.delete(API.URL + '/channels/' + channelId + '/contents/' + contentId);
    }

    function sendContentCardToMe(contentId) {
      return $http.post(API.URL + '/contents/' + contentId + '/send-content-card-to-me', null);
    }

    function getContentHtmlCard(contentId) {
      return $http.get(API.URL + '/contents/' + contentId + '/html-card', null);
    }

    function getContentSearch(query) {
      return $http.get(API.URL + '/contents/search', {params: {
        q: query
      }});
    }

  }

})();
