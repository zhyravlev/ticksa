(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ContentGetHtmlCodeDialogController', ContentGetHtmlCodeDialogController);

  /** @ngInject */
  function ContentGetHtmlCodeDialogController($scope, $document, $timeout, $mdDialog, content, ContentService) {
    var ctrl = this;
    var fadeoutTimeout = null;

    ctrl.copied = false;

    ContentService.getContentHtmlCard(content.id).then(function(res) {
      ctrl.html = res.data.renderedCard;
      select();
    });

    var select = function () {
      $timeout(function () {
        $document[0].getElementsByName('html')[0].select();
      }, 0);
    };

    ctrl.select = select;
    ctrl.close = function () {
      $mdDialog.hide();
    };

    ctrl.onCopied = function (e) {
      if (e.action == 'copy') {
        if (!!fadeoutTimeout) {
          $timeout.cancel(fadeoutTimeout);
        }

        $scope.$apply(function () {
          ctrl.copied = true;
        });

        fadeoutTimeout = $timeout(function () {
          ctrl.copied = false;
        }, 2500);
      }
    };
  }
})();
