(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ShowChannelListController', ShowChannelListController);

  /** @ngInject */
  function ShowChannelListController($log, $rootScope, $mdDialog, $state, $stateParams, $timeout, $document,
                                     CONTENT_TYPES, ContentService, LinkContentService, PlaylistHttpService, playlistId) {

    $log = $log.getInstance('ShowChannelListController');

    var ctrl = this;

    ctrl.contents = [];
    ctrl.CONTENT_TYPES = CONTENT_TYPES;
    ctrl.stateParams = $stateParams;
    ctrl.close = close;
    ctrl.viewContent = viewContent;
    ctrl.payForContent = payForContent;

    function close() {
      $mdDialog.hide();
    }

    if (playlistId) {
      PlaylistHttpService.getPlaylist(playlistId)
        .success(function (playlist) {
          drawContentList(playlist.contents);
        })
        .error(function (err) {
          $log.error(err);
        });
    } else {
      // Load contents
      ContentService.getContentByChannel($stateParams.channelId, 0, 999)
        .success(function (contents) {
          drawContentList(contents);
        })
        .error(function (err) {
          $log.error(err)
        });
    }

    function drawContentList(contents) {
      contents.forEach(function (content) {
        ctrl.contents.push(content)
      });

      $timeout(function () {
        var scrollContainer = angular.element($document[0].getElementsByClassName('channel-list')[0]),
          em = angular.element($document[0].getElementsByClassName('current')[0]);

        scrollContainer.scrollToElement(em, 0, 500);
      }, 300); // Wait ng-repeat render.
    }

    function viewContent(content) {
      var clickOnItself = $stateParams.channelId == content.channel.id && $stateParams.contentId == content.id;
      if (!clickOnItself) {
        LinkContentService.openBlankWindowIfNeeded(content);
        $state.go('main.contentView', {channelId: content.channel.id, contentId: content.id, list: playlistId});
      }
      close();
    }

    function payForContent(content) {
      LinkContentService.openBlankWindowIfNeeded(content);
      ContentService.payForContent(content.id)
        .success(function () {
          $rootScope.$broadcast('TicksTransaction', {});
          $state.go('main.contentView', {channelId: content.channel.id, contentId: content.id, list: playlistId});
        })
        .error(function (err) {
          LinkContentService.closeBlankWindowIfNeeded(content);
          $log.error(err)
        });
      close();
    }
  }
})();