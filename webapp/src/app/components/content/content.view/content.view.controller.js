(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ContentViewController', ContentViewController);

  /** @ngInject */
  function ContentViewController($scope, $log, $stateParams, $state, $translate, $timeout,
                                 ChannelService, CommentService, ContentService, HeaderTitleService) {

    $log = $log.getInstance('ContentViewController');

    var ctrl = this;
    var currentPage = 0;
    var pageSize = 12;
    var redirectTimeout = null;
    var errorRedirectToState = 'main.contentList';

    ctrl.error = false;
    ctrl.commentList = [];
    ctrl.remaining = [];
    ctrl.rawComments = [];
    ctrl.columnsNumber = 0;
    ctrl.loadingInProgress = false;
    ctrl.loadMore = loadMore;

    function loadMore() {
      if (ctrl.loadComplete) return;
      loadPage(currentPage, pageSize);
    }

    function loadPage(page, pageSize) {
      ctrl.loadingInProgress = true;
      CommentService.getCommentsForContent($stateParams.contentId, page, pageSize)
        .success(function (comments) {
          if (comments.length == 0 || comments.length < pageSize) {
            ctrl.loadComplete = true;
          }

          if (comments.length == pageSize) {
            currentPage++;
          }

          comments.forEach(function (comment) {
            ctrl.rawComments.push(comment);
          });

          ctrl.loadingInProgress = false;
        })
        .error(function (err) {
          ctrl.loadingInProgress = false;
          $log.error(err);
        });
    }

    loadPage(currentPage, pageSize);

    ContentService.getContentById($stateParams.channelId, $stateParams.contentId)
      .success(function (data) {
        ctrl.content = data;
      })
      .error(errorHandler);

    ChannelService.getChannel($stateParams.channelId)
      .success(function (data) {
        ctrl.channel = data;
        HeaderTitleService.setChannelSubscribed(ctrl.channel.id, ctrl.channel.name, ctrl.channel.subscribed);
      })
      .error(errorHandler);

    function errorHandler(err, status) {
      $log.error(err);
      switch (status) {
        case 403:
          if (!err.hasOwnProperty('type')) {
            return $state.go(errorRedirectToState);
          }
          if(err.type === 'NOT_BOUGHT'){
            return $state.go(errorRedirectToState);
          }
          ctrl.error = $translate.instant('ERROR_' + err.type);
          redirectTimeout = $timeout(function () {
            $state.go(errorRedirectToState);
          }, 5000);
          break;
        default:
          $state.go(errorRedirectToState);
      }
    }

    $scope.$on('$destroy', function () {
      if (redirectTimeout != null) {
        $timeout.cancel(redirectTimeout);
      }
    });
  }
})();
