(function () {
  'use strict';

  // time in seconds while user can refund
  var SECONDS_TO_LET_REFUND = 30;

  // because of network there can be time delay. this delta is for compensation
  var SECONDS_OF_TIME_DELAY = 2;

  angular
    .module('ticksaWebapp')
    .directive('tkContentView', tkContentView);

  /** @ngInject */
  function tkContentView($translate, $log, $mdDialog, $timeout, $stateParams, $rootScope, $state, $window, $document, $sanitize,
                         lodash, ENV, USER_STATUS, TICKSER_PREFERENCES, PRICES,
                         s3cdnFilter, thumbnailsS3CdnFilter, jGrowlNotify,
                         PlaylistHttpService, UserService, FrontendService, ContentService, PaymentConfirmationService, EncodingService, ModalService, LinkContentService) {

    $log = $log.getInstance('tkContentView');

    return {
      controllerAs: 'ctrl',
      templateUrl: 'app/components/content/content.view.directive/content.view.directive.html',
      scope: {
        content: '=',
        channel: '='
      },
      controller: function ($scope, CONTENT_TYPES) {

        var ctrl = this;

        ctrl.UserService = UserService;
        ctrl.touch = FrontendService.device.isTouch;

        ctrl.channelContents = [];
        ctrl.prevContent = false;
        ctrl.nextContent = false;
        ctrl.canRefundContent = false;
        ctrl.CONTENT_TYPES = CONTENT_TYPES;
        ctrl.USER_STATUS = USER_STATUS;

        ctrl.getHtmlCode = getHtmlCode;
        ctrl.shareTwitter = shareTwitter;
        ctrl.shareFacebook = shareFacebook;
        ctrl.shareUrl = shareUrl;
        ctrl.extendExpiryDate = extendExpiryDate;
        ctrl.likeContent = likeContent;
        ctrl.postComment = postComment;
        ctrl.dislikeContent = dislikeContent;
        ctrl.pwywContent = pwywContent;
        ctrl.refundContent = refundContent;
        ctrl.publishContent = publishContent;
        ctrl.unPublishContent = unPublishContent;
        ctrl.flagAsInappropriate = flagAsInappropriate;
        ctrl.editContent = editContent;
        ctrl.showChannelList = showChannelList;
        ctrl.sanitizeText = sanitizeText;

        function showChannelList($event) {
          $mdDialog.show({
            closeTo: angular.element($document[0].body),
            parent: $document[0].body,
            targetEvent: $event,
            templateUrl: 'app/components/content/content.showChannelList.dialog/content.showChannelList.dialog.html',
            controller: 'ShowChannelListController',
            controllerAs: 'ctrl',
            clickOutsideToClose: true,
            escapeToClose: true,
            locals: {
              playlistId: normalizeListId($stateParams.list)
            }
          });
        }

        EncodingService.encodeContentAndUserId($stateParams.contentId, UserService.getId())
          .success(function (data) {
            ctrl.url = ENV.URL + '/s/' + data.contentId + '-' + data.userId;
          })
          .error(function (err) {
            $log.error(err);
          });

        var imageWatch = $scope.$watch('content', function (content) {
          if (content) {
            if (content.contentImages) {
              ctrl.images = lodash.map(content.contentImages, function (contentImage) {
                return {
                  thumb: thumbnailsS3CdnFilter(contentImage.key, 200, 200),
                  img: s3cdnFilter(contentImage.key),
                  description: contentImage.name
                }
              });
            }
            imageWatch();
          }
        });

        var contentListener = $scope.$watch('content', function (content) {
          if (content != null) {
            LinkContentService.showExternalLink(content);
            contentListener();
          }
        });

        $scope.$watch('content', function (content) {
          if (content) {
            var secondsToRefund = SECONDS_TO_LET_REFUND - SECONDS_OF_TIME_DELAY - content.secondsFromBuy;
            ctrl.canRefundContent = secondsToRefund > 0;

            if (secondsToRefund > 0) {
              var refundResetTimeout = $timeout(function () {
                ctrl.canRefundContent = false;
              }, secondsToRefund * 1000);

              $scope.$on('$destroy', function () {
                $timeout.cancel(refundResetTimeout);
              });
            }
          }
        });

        function shareUrl($event) {
          $mdDialog.show({
            closeTo: angular.element($document[0].body),
            parent: $document[0].body,
            targetEvent: $event,
            templateUrl: 'app/components/dialog/shareLink/shareLink.dialog.html',
            controller: 'ShareLinkDialogController',
            controllerAs: 'ctrl',
            clickOutsideToClose: true,
            escapeToClose: true,
            locals: {
              texts: {
                header: $translate.instant('COMMONS_LINK_TO_CONTENT'),
                label: $translate.instant('COMMONS_LINK_TO_CONTENT'),
                copied: $translate.instant('COMMONS_URL_WAS_COPIED_TO_CLIP_BOARD')
              },
              url: ctrl.url
            }
          });
        }

        function getHtmlCode($event, content) {

          // Add channel data to content
          content.channel = $scope.channel;

          $mdDialog.show({
            closeTo: angular.element($document[0].body),
            parent: $document[0].body,
            targetEvent: $event,
            templateUrl: 'app/components/content/content.getHtmlCode.dialog/content.getHtmlCode.dialog.html',
            controller: 'ContentGetHtmlCodeDialogController',
            controllerAs: 'ctrl',
            clickOutsideToClose: true,
            escapeToClose: true,
            locals: {
              content: content,
              url: ctrl.url
            }
          });
        }

        function shareTwitter() {
          $window.open('https://twitter.com/intent/tweet?text=' + ctrl.url, '_blank');
          return true;
        }

        function shareFacebook() {
          FB.ui({
            display: 'iframe',
            method: 'share',
            href: ctrl.url
          }, function (response) {
          });
        }

        function publishContent(content) {
          ContentService.publishContent(content.channelId, content.id)
            .success(function (data, status) {
              content.published = true;
            })
            .error(function (err) {
              $log.error(err);
            })
        }

        function editContent(content) {
          $state.go('main.contentEdit', {channelId: content.channelId, contentId: content.id});
        }

        function unPublishContent(content) {
          ContentService.unpublishContent(content.channelId, content.id)
            .success(function (data, status) {
              content.published = false;
            })
            .error(function (err) {
              $log.error(err);
            })
        }

        function flagAsInappropriate(content, $event) {
          $mdDialog.show({
            closeTo: angular.element($document[0].body),
            parent: $document[0].body,
            targetEvent: $event,
            templateUrl: 'app/components/common/feedback/flag.as.inappropriate/flag.as.inappropriate.dialog.html',
            controller: 'FlagAsInappropriateController',
            controllerAs: 'ctrl',
            clickOutsideToClose: true,
            escapeToClose: true,
            locals: {
              id: content.id,
              entity: 'content'
            }
          })
        }

        function postComment() {
          $state.go('main.commentAdd', {
            channelId: $stateParams.channelId,
            contentId: $stateParams.contentId
          });
        }

        function extendExpiryDate(content) {
          ContentService.extendContent(content.id)
            .success(function (data, status) {
              content.expiresAt = data.expiresAt;
              $rootScope.$broadcast('TicksTransaction', {});
            })
            .error(function (err) {
              $log.error(err);
            })
        }

        function likeContent(content) {
          PaymentConfirmationService.confirmTicksAction(PRICES.LIKE, TICKSER_PREFERENCES.SKIP_LIKE_NOTIFICATION).then(function () {
            ContentService.likeContent(content.id)
              .success(function (data, status) {
                content.ticks = data.ticks;
                $rootScope.$broadcast('TicksTransaction', {});
                $scope.content.likes += 1;
                $log.log("DEBUG DATA. content: ", content.id, "likes", $scope.content.likes);
              })
              .error(function (err) {
                $log.error(err);
              })
          });
        }

        function dislikeContent(content) {
          PaymentConfirmationService.confirmTicksAction(PRICES.DISLIKE, TICKSER_PREFERENCES.SKIP_DISLIKE_NOTIFICATION).then(function () {
            ContentService.dislikeContent(content.id)
              .success(function (data, status) {
                content.ticks = data.ticks;
                $rootScope.$broadcast('TicksTransaction', {});
                $scope.content.dislikes += 1;
              })
              .error(function (err) {
                $log.error(err);
              })
          });
        }

        function pwywContent(content) {
          ModalService.showPwywContentModal(content).then(function (enteredTicks) {
            ContentService.likeContent(content.id, enteredTicks)
              .success(function (data, status) {
                content.ticks = data.ticks;
                $rootScope.$broadcast('TicksTransaction', {});
                $scope.content.likes += enteredTicks;
              })
              .error(function (err) {
                $log.error(err);
              })
          });
        }

        function refundContent(content) {
          ContentService.refundContent(content.id).then(function (res) {
            $rootScope.$broadcast('TicksTransaction', {});
            $state.go('main.contentList');
          }).catch(function (res) {
            if (res.data.error.status == 400) {
              new jGrowlNotify().error($translate.instant('CONTENT_VIEW__TOAST__REFUND_NOT_POSSIBLE'));
            }
          });
        }

        function payForContent(content) {
          LinkContentService.openBlankWindowIfNeeded(content);
          ContentService.payForContent(content.id)
            .success(function () {
              $rootScope.$broadcast('TicksTransaction', {});
              $state.go('main.contentView', {channelId: content.channel.id, contentId: content.id})
            })
            .error(function (err) {
              LinkContentService.closeBlankWindowIfNeeded(content);
              $log.error(err)
            });
        }

        function sanitizeText(str) {
          return $sanitize(str.replace(/\n/g, '<br/>'));
        }

        ctrl.goTo = function (content) {
          if (content.bought || content.manager) {
            LinkContentService.openBlankWindowIfNeeded(content);
            $state.go('main.contentView', {channelId: content.channel.id, contentId: content.id});
          } else {
            payForContent(content)
          }
        };

        if (normalizeListId($stateParams.list)) {
          PlaylistHttpService.getPlaylist($stateParams.list)
            .success(function (playlist) {
              drawNavigation(playlist.contents)
            })
            .error(function (err) {
              $log.error(err);
            });
        } else {
          // Load contents
          ContentService.getContentByChannel($stateParams.channelId, 0, 999)
            .success(function (contents) {
              drawNavigation(contents)
            })
            .error(function (err) {
              $log.error(err)
            });
        }

        function drawNavigation(contents) {
          var index;

          contents.forEach(function (content) {
            ctrl.channelContents.push(content);
            if ($stateParams.contentId == content.id) {
              index = ctrl.channelContents.indexOf(content)
            }
          });

          if (!!ctrl.channelContents[index - 1]) {
            ctrl.prevContent = ctrl.channelContents[index - 1]
          }

          if (!!ctrl.channelContents[index + 1]) {
            ctrl.nextContent = ctrl.channelContents[index + 1]
          }
        }

        function normalizeListId(listIdFromQuery) {
          if (!angular.isString(listIdFromQuery)) {
            return false;
          }
          if (listIdFromQuery === 'true') {
            return false;
          }

          var num = Number(listIdFromQuery);
          return !isNaN(num) ? num : false;
        }
      }
    }
  }

})();
