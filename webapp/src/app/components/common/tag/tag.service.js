(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('TagService', TagService);

  /** @ngInject */
  function TagService(API, $http) {

    return {
      Public: {
        getTags: getTags
      }
    };

    // server-side code was deleted in SPRINT 20
    function getTags() {
      return $http.get(API.URL + '/tags');
    }

  }
})();
