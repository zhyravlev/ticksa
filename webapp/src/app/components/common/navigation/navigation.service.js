(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('NavigationService', NavigationService);

  /** @ngInject */
  function NavigationService($state, $cookies, $mdDialog) {

    var NavigationService = this;

    var previousState = null;
    var nextState = null;
    var resetStates = function () {
      previousState = null;
      nextState = null;
    };

    NavigationService.getNextStateReferrerId = function () {
      if (nextState != null) {
        return nextState.params.referrerId;
      } else {
        return null;
      }
    };

    NavigationService.getNextStateRedirectUrl = function () {
      if (nextState != null) {
        return $state.href(nextState.name, nextState.params, {absolute: true});
      } else {
        return null;
      }
    };

    NavigationService.getPreviousState = function () {
      return previousState;
    };

    NavigationService.getNextState = function () {
      return nextState;
    };

    NavigationService.setPreviousState = function (name, params) {
      previousState = {name: name, params: params}
    };

    NavigationService.setNextState = function (name, params) {
      nextState = {name: name, params: params};
    };

    NavigationService.saveStateAndGoToTopUp = function () {
      $mdDialog.hide();
      $cookies.put('BTU_STATE', {name: $state.current.name, params: $state.params});
      $state.go('main.financial.start', {showRedirectMessage: true});
    };

    NavigationService.goToBeforeTopUpState = function () {
      var beforeTopUpState = $cookies.get('BTU_STATE');
      $cookies.remove('BTU_STATE');

      if (beforeTopUpState) {
        $state.go(beforeTopUpState.name, beforeTopUpState.params);
      }
    };

    NavigationService.getBeforeTopUpState = function () {
      return $cookies.get('BTU_STATE');
    };

    NavigationService.goToPreviousStateOrProvided = function (toState, toParams) {
      if (previousState != null) {
        $state.go(previousState.name, previousState.params);
      }
      else {
        $state.go(toState, toParams);
      }
    };

    NavigationService.goToNextStateOrProvided = function (toState, toParams) {
      if (nextState != null) {
        $state.go(nextState.name, nextState.params);
      }
      else {
        $state.go(toState, toParams);
      }
    };

    NavigationService.goToLoginPage = function (logout) {
      if(logout){
        resetStates();
      }

      $state.go('base.login');
    };

    NavigationService.goToDiscoveryPage = function(logout){
      if(logout){
        resetStates();
      }

      $state.go('discovery');
    }
  }
})();
