(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('NotificationsHttpService', NotificationsHttpService);

  /** @ngInject */
  function NotificationsHttpService($http, API) {

    var NotificationsHttpService = this;

    NotificationsHttpService.getNotificationsForUser = function () {
      return $http.get(API.URL + '/notifications');
    };

    NotificationsHttpService.getCountNotificationsForUser = function () {
      return $http.get(API.URL + '/notifications/count');
    };

    NotificationsHttpService.getNotification = function (notificationId) {
      return $http.get(API.URL + '/notifications/' + notificationId);
    };

    NotificationsHttpService.viewNotifications = function () {
      return $http.post(API.URL + '/notifications/view');
    }
  }

})();
