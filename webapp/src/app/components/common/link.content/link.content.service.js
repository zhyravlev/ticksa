(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('LinkContentService', LinkContentService);

  /** @ngInject */
  function LinkContentService($window, $log, CONTENT_TYPES) {
    $log = $log.getInstance('LinkContentService');

    var LinkContentService = this;

    var createdBlankWindow = null;

    // it is used to define that user opens direct link to LINK content.
    // in this case we should not open content's link in the same tab/window because it looks bad.
    //
    // e.g. if user is trying to use 'go back' browser button from already opened content's link page then new angular
    // app is started. so, there was no attempt to open blank tab/window and createdBlankWindow == null.
    // by analyzing lastTriedContentId this service prevents situation when user will be redirected back to the page
    // he wants to 'go back' from
    var thereWasAnAttemptToOpenBlankWindow = false;

    // IMPORTANT!
    // Method should be called in code handler of user's action (e.g. onClick), but not in any programmatic callback
    // (e.g. in Promise then/catch callback)
    // Only in this case we can be sure that browser's pop-up blocker wont block new window.
    LinkContentService.openBlankWindowIfNeeded = function (content) {
      if (content.type !== CONTENT_TYPES.LINK) {
        return;
      }

      if (!!createdBlankWindow) {
        $log.error('openBlankWindowIfNeeded: Error! Expected that [createdBlankWindow] should be null before assignment')
      }

      thereWasAnAttemptToOpenBlankWindow = true;

      // if creation is blocked by pop-up blocker then $window.open() will return null
      createdBlankWindow = $window.open('about:blank', '_blank');
    };

    // according to Michah's comment
    // https://ticksa.atlassian.net/browse/APP-559?focusedCommentId=14030&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-14030
    //   1) external link is opened in saved blank tab/window.
    //   2) if blank tab/window doesnt present then external link will be opened in current tab.
    LinkContentService.showExternalLink = function (content) {
      if (content.type !== CONTENT_TYPES.LINK) {
        return;
      }

      if (!thereWasAnAttemptToOpenBlankWindow) {
        return;
      }

      // such check will help to define if created window present
      // (e.g. it can be closed by user or blocked by pop-up blocker)
      var isCreatedWindowStillAppears = !!createdBlankWindow && !!createdBlankWindow.location;

      if (isCreatedWindowStillAppears) {
        createdBlankWindow.location.href = content.contentLink.url;
      } else {
        $window.location = content.contentLink.url;
      }

      createdBlankWindow = null;
      thereWasAnAttemptToOpenBlankWindow = false;
    };

    LinkContentService.closeBlankWindowIfNeeded = function (content) {
      if (content.type !== CONTENT_TYPES.LINK) {
        return;
      }

      if (!createdBlankWindow) {
        $log.error('closeBlankWindowIfNeeded: Error! No [createdBlankWindow]');
        return;
      }

      createdBlankWindow.close();
      createdBlankWindow = null;
      thereWasAnAttemptToOpenBlankWindow = false;
    };
  }
})();
