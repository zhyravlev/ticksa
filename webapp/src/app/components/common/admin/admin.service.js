(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('AdminService', AdminService);

  /** @ngInject */
  function AdminService($http, API) {

    var Service = {};

    Service.flagContentAsInappropriate = flagContentAsInappropriate;
    Service.postFeedback = postFeedback;

    function postFeedback(type, text) {
      return $http.post(API.URL + '/admin/feedback', {type: type, text: text});
    }

    function flagContentAsInappropriate(contentId, text) {
      return $http.post(API.URL + '/admin/contents/' + contentId + '/flag-as-inappropriate', {text: text});
    }

    return Service;

  }

})();
