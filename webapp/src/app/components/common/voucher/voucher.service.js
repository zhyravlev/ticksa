(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('VoucherService', VoucherService);

  /** @ngInject */
  function VoucherService($http, API) {

    var Service = {};

    Service.checkVoucher = checkVoucher;
    Service.applyVoucher = applyVoucher;
    Service.getTickserRights = getTickserRights;

    function checkVoucher(voucherCode) {
      return $http.post(API.URL + '/voucher/check', {code: voucherCode});
    }

    function applyVoucher(voucherCode) {
      return $http.post(API.URL + '/voucher/apply', {code: voucherCode});
    }

    function getTickserRights() {
      return $http.get(API.URL + '/voucher/rights');
    }

    return Service;

  }

})();
