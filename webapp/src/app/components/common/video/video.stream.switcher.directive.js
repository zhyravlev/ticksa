(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkStreamSwitcher', streamSwitcher);

  function streamSwitcher($timeout) {
    var qualities = [
      {bitrate: 2400, name: 'HD'},
      {bitrate: 720, name: 'SD'}
    ];

    return {
      restrict: "E",
      require: "^videogular",
      template: "<button class='iconButton' ng-click='switchStream()'>{{currentQuality.name}}</button>",
      controller: function ($scope) {

        $scope.currentQuality = qualities[0];

        $scope.switchStream = function () {

          var currentTime = $scope.API.currentTime;
          $scope.API.pause();

          if ($scope.currentQuality.name == 'HD') {
            $scope.currentQuality = qualities[1];
          } else {
            $scope.currentQuality = qualities[0];
          }
          $scope.config.sources = $scope.sources.filter(function (source) {
            return source.bitrate == $scope.currentQuality.bitrate;
          });

          $timeout(function () {
            $scope.API.seekTime(currentTime / 1000);
            $timeout($scope.API.play.bind($scope.API), 100);
          }, 100);
        };
      }
    }
  }
})();
