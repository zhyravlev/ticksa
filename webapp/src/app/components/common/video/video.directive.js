(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkVideo', video);

  /** @ngInject */
  function video() {
    return {
      templateUrl: 'app/components/common/video/video.html',
      scope: {
        autoPlay: '@',
        video: '=',
        thumbnailWidth: '@',
        thumbnailHeight: '@'
      },
      controller: function ($log, $scope, $timeout, s3cdnFilter) {

        var ctrl = this;
        $scope.sources = [];

        $scope.video.videoFiles.forEach(function (file) {
          $scope.sources.push({src: s3cdnFilter(file.key), type: 'video/mp4', bitrate: file.bitrate});
        });

        $scope.config = {
          sources: [$scope.sources[0]],
          plugins: {
            controls: {
              autoHide: true,
              autoHideTime: 1000
            }
          }
        };

        $scope.onPlayerReady = function (API) {
          ctrl.API = API;
          if ($scope.autoPlay) {
            $timeout(API.play.bind(API));
          }
        };

        /** Poster */
        $scope.showPoster = true;

        $scope.updateState = function($state){
          $scope.showPoster = $state == 'stop';
      }
      }
    }
  }
})();
