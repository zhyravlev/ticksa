(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ModalConfirmCheckContentBuyController', ModalConfirmCheckContentBuyController);

  // IMPORTANT!
  // Current controller implements specific logic to work with buys of content, especially LINK content.
  //
  // Details:
  //  - by design webapp opens LINK of content in parallel tab immediately after user is redirected to content
  //    view page.
  //  - to prevent browser pop-up blocking some stuff is doing by LinkContentService. it opens new tab in user action
  //    handler, save opened tab link, then updates tab's location to value of the content's LINK
  //  - usually the result of user's confirmation is return in $mdDialog.show() returned promise but NOT IN THIS CASE
  //  - such behavior is implemented because after LinkContentService.openBlankWindowIfNeeded() is being called then
  //    focus is moved to new created window. BUT for some strange reason result promise of $mdDialog.show() is not
  //    returned. Look like it hangs. But if user switches browser's tab to webapp - it becomes alive and continue its
  //    work. And only after all of this new view is opened (content view page) and new created tab's location will
  //    be updated to LINK of the content.
  //  - in the case to find a workaround $mdDialog.show() result is not used. inside this controller 'defResult'
  //    parameter is passed. it is used to notify about user's confirmation.

  /** @ngInject */
  function ModalConfirmCheckContentBuyController($mdDialog, LinkContentService, texts, content, defResult) {
    var ctrl = this;
    ctrl.texts = texts;
    ctrl.checkboxValue = false;

    ctrl.cancel = function () {
      defResult.reject();
      $mdDialog.cancel();
    };

    ctrl.confirm = function () {
      LinkContentService.openBlankWindowIfNeeded(content);
      defResult.resolve(ctrl.checkboxValue);
      $mdDialog.hide();
    };

  }
})();
