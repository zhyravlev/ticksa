(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ModalConfirmCheckTicksActionController', ModalConfirmCheckTicksActionController);

  /** @ngInject */
  function ModalConfirmCheckTicksActionController($mdDialog, texts) {
    var ctrl = this;
    ctrl.texts = texts;
    ctrl.checkboxValue = false;
    ctrl.cancel = function () {
      $mdDialog.cancel();
    };
    ctrl.confirm = function () {
      $mdDialog.hide(ctrl.checkboxValue);
    };

  }
})();
