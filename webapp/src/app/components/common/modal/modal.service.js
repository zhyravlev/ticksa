(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('ModalService', ModalService);

  /** @ngInject */
  function ModalService($translate, $mdDialog, $q, $document) {
    var ModalService = this;

    ModalService.showConfirmTicksActionModal = function (price) {
      return showPaymentConfirmationDialog(
        'ModalConfirmCheckTicksActionController',
        {
          texts: {
            title: $translate.instant('CONFIRM_CHECK_TITLE'),
            body: $translate.instant('CONFIRM_CHECK_BODY', {amount: price}),
            checkbox: $translate.instant('CONFIRM_CHECK_CHECKBOX'),
            cancel: $translate.instant('COMMONS_CANCEL'),
            confirm: $translate.instant('CONFIRM_CHECK_CONFIRM')
          }
        }
      );
    };

    // INFO!
    // it has specific logic of returned value - it uses special deferred object to provide result.
    // for more information read comments on ModalConfirmCheckContentBuyController
    ModalService.showConfirmContentBuyModal = function (content) {
      var defResult = $q.defer();
      showPaymentConfirmationDialog(
        'ModalConfirmCheckContentBuyController',
        {
          content: content,
          defResult: defResult,
          texts: {
            title: $translate.instant('CONFIRM_CHECK_TITLE'),
            body: $translate.instant('CONFIRM_CHECK_BODY', {amount: content.tickPrice}),
            checkbox: $translate.instant('CONFIRM_CHECK_CHECKBOX'),
            cancel: $translate.instant('COMMONS_CANCEL'),
            confirm: $translate.instant('CONFIRM_CHECK_CONFIRM')
          }
        }
      );
      return defResult.promise;
    };

    ModalService.showPwywContentModal = function (content) {
      return $mdDialog.show({
        templateUrl: 'app/components/content/content.pwyw.dialog/content.pwyw.dialog.html',
        controller: 'ContentPwywDialogController',
        controllerAs: 'ctrl',
        closeTo: angular.element($document[0].body),
        parent: angular.element($document[0].body),
        clickOutsideToClose: true,
        escapeToClose: true,
        locals: {
          content: content
        }
      });
    };

    ModalService.showPwywChannelModal = function () {
      return $mdDialog.show({
        templateUrl: 'app/components/channel/channel.topRanked.pwyw.dialog/channel.topRanked.pwyw.dialog.html',
        controller: 'ChannelTopRankedPwywDialogController',
        controllerAs: 'ctrl',
        closeTo: angular.element($document[0].body),
        parent: angular.element($document[0].body),
        clickOutsideToClose: true,
        escapeToClose: true
      });
    };

    ModalService.showTopUpChannelModal = function () {
      return $mdDialog.show({
        templateUrl: 'app/components/channel/channel.topRanked.topUp.dialog/channel.topRanked.topUp.dialog.html',
        controller: 'ChannelTopRankedTopUpDialogController',
        controllerAs: 'ctrl',
        closeTo: angular.element($document[0].body),
        parent: angular.element($document[0].body),
        clickOutsideToClose: true,
        escapeToClose: true
      });
    };

    ModalService.confirmationBuilder = function () {
      return new ConfirmationBuilder();
    };

    var ConfirmationBuilder = function () {
      var dialog = {
        templateUrl: 'app/components/common/modal/modal.confirm/modal.confirm.html',
        controller: 'ModalConfirmController',
        controllerAs: 'ctrl',
        parent: angular.element($document[0].body)
      };

      return {
        confirmationTargetEvent: function (event) {
          dialog.targetEvent = event;
          return this;
        },

        confirmationClickOutsideToClose: function () {
          dialog.clickOutsideToClose = true;
          return this;
        },

        confirmationTexts: function (texts) {
          dialog.locals = {
            texts: texts
          };
          return this;
        },

        confirmationShowModal: function () {
          return $mdDialog.show(dialog);
        }
      }
    };

    var showPaymentConfirmationDialog = function (controllerName, locals) {
      return $mdDialog.show({
        templateUrl: 'app/components/common/modal/modal.confirm.check/modal.confirm.check.html',
        controller: controllerName,
        controllerAs: 'ctrl',
        parent: angular.element($document[0].body),
        clickOutsideToClose: true,
        escapeToClose: true,
        locals: locals
      })
    };
  }
})();
