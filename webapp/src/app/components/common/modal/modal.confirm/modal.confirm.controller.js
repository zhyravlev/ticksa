(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ModalConfirmController', ModalConfirmController);

  /** @ngInject */
  function ModalConfirmController($mdDialog, texts) {
    var ctrl = this;
    ctrl.texts = texts;
    ctrl.cancel = function () {
      $mdDialog.cancel();
    };
    ctrl.confirm = function () {
      $mdDialog.hide();
    };
  }
})();
