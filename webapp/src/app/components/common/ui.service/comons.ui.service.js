(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('CommonsUIService', CommonsUIService);

  /** @ngInject */
  function CommonsUIService($log, $rootScope, $q, lodash, TkDialogFactory, ChannelService) {

    $log = $log.getInstance('CommonsUIService');

    var CommonsUIService = this;

    /**
     * @todo Return multiple images
     *
     * @param {Object} event Event object
     * @param {Array} availableImages List of available images to select
     * @param {Object} alreadySelectedImage Already selected image. Will be marked as selected in GUI
     * @param {boolean} isMultiple Flag to define if user can select several images or only one image
     * @returns {Object} Promise with new selected image(s)
     */
    var selectImage = function (event, availableImages, alreadySelectedImage, isMultiple) {
      var _images = lodash.map(availableImages, function (image) {
        image.selected = alreadySelectedImage ? alreadySelectedImage.id == image.id : false;
        return image;
      });

      var tkDialog = new TkDialogFactory();
      tkDialog.controller('ImagePickerModalController');
      tkDialog.templateUrl('app/components/library/image/image.picker.modal.html');
      tkDialog.locals({images: _images, multiple: isMultiple});

      return tkDialog.show().then(function (data) {
        return data.selectedImage
      });
    };

    CommonsUIService.selectSingleImage = function (event, availableImages, alreadySelectedImage) {
      return selectImage(event, availableImages, alreadySelectedImage, false);
    };

    CommonsUIService.selectMultipleImages = function (event, availableImages, alreadySelectedImage) {
      return selectImage(event, availableImages, alreadySelectedImage, true);
    };

    CommonsUIService.subscribeToChannel = function (channel) {
      return ChannelService.subscribeToChannel(channel.id)
        .success(function () {

          // channel.subscriberCount - don't used

          $rootScope.$broadcast('SubscriptionChange', {
            type: 1,
            channel: channel
          });

          return $q.resolve();
        })
        .error(function (err) {
          $log.log(err);
          return $q.reject();
        })
    };

    CommonsUIService.unsubscribeToChannel = function (channel) {
      return ChannelService.unsubscribeToChannel(channel.id)
        .success(function () {

          // channel.subscriberCount - don't used

          $rootScope.$broadcast('SubscriptionChange', {
            type: 0,
            channel: channel
          });

          return $q.resolve();
        })
        .error(function (err) {
          $log.log(err);
          return $q.reject();
        })
    }
  }

})();
