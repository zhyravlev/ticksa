(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkCompareString', CompareStringValidator);

  /** @ngInject */
  function CompareStringValidator() {
    return {
      scope: {
        tkCompareString: '='
      },
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$validators.equals = function (modelValue, viewValue) {

          var valid = false;
          if (ctrl.$isEmpty(scope.tkCompareString)) {
            valid = true;
          }
          else {
            valid = viewValue == scope.tkCompareString;
          }

          return valid;

        };
      }
    }
  }
})();
