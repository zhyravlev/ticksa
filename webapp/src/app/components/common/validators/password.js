(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkPassword', tkPassword);

  /** @ngInject */
  function tkPassword() {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$validators.password = function (modelValue, viewValue) {

          var valid = false;
          if (viewValue != null && viewValue.length >= 8 && viewValue.indexOf('')) {
            valid = true;
          }

          return valid;

        };
      }
    }
  }
})();
