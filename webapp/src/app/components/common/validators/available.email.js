(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkEmailAvailable', CompareStringValidator);

  /** @ngInject */
  function CompareStringValidator($q, AuthorizationHttpService) {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {

        ctrl.$asyncValidators.availableEmail = function (modelValue, viewValue) {

          if (ctrl.$isEmpty(modelValue)) {
            return $q.resolve();
          }

          var def = $q.defer();
          AuthorizationHttpService.checkAvailableEmail(modelValue)
            .success(function (data, status) {
              def.resolve(data);
            })
            .error(function (data, status) {
              def.reject(data);
            });
          return def.promise;
        };
      }
    }
  }
})();
