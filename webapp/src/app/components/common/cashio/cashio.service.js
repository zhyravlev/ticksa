(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('CashIOService', CashIOService);

  /** @ngInject */
  function CashIOService($http, API) {

    var Service = {};

    Service.startInfinCashIn = startInfinCashIn;
    Service.startPayPalCashIn = startPayPalCashIn;
    Service.startLemonwayCashIn = startLemonwayCashIn;
    Service.transferTicks = transferTicks;
    Service.findById = findById;

    function findById(id) {
      return $http.get(API.URL + '/cashio/' + id);
    }

    function startInfinCashIn(moneyAmount, countryCode) {
      return $http.post(API.URL + '/cashio/cashin/infin', {moneyAmount: moneyAmount, countryCode: countryCode});
    }

    function startPayPalCashIn(moneyAmount) {
      return $http.post(API.URL + '/cashio/cashin/paypal', {moneyAmount: moneyAmount});
    }

    function startLemonwayCashIn(cardData) {
      return $http.post(API.URL + '/cashio/cashin/lemonway', cardData);
    }

    function transferTicks(ticks) {
      return $http.post(API.URL + '/cashio/transfer/earned-purchased', {tickAmount: ticks});
    }

    return Service;

  }

})();
