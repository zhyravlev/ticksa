(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('EmailService', EmailService);

  /** @ngInject */
  function EmailService($http, API) {

    var Service = {
      verifyToken: verifyToken,
      checkSubscription: checkSubscription,
      reply: reply,
      unsubscribe: unsubscribe,
      subscribe: subscribe,
    };

    function checkSubscription(token) {
      return $http.get(API.URL + '/emails/check-subscription', {params: {token: token}});
    }

    function verifyToken(token) {
      return $http.get(API.URL + '/emails/verify-token', {params: {token: token}});
    }

    function reply(token, text) {
      return $http.post(API.URL + '/emails/reply', {token: token, text: text});
    }

    function subscribe(token) {
      return $http.post(API.URL + '/emails/subscribe', {token: token});
    }

    function unsubscribe(token) {
      return $http.post(API.URL + '/emails/unsubscribe', {token: token});
    }

    return Service;

  }

})();
