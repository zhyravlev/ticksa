(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('UserService', UserService);

  /** @ngInject */
  function UserService(lodash, LanguageUIService) {

    var data = {};
    var UserService = this;


    /** Profile */

    UserService.get = function (name) {
      return data[name] || false;
    };

    UserService.isOldPasswordExists = function () {
      return this.get('oldPasswordExists');
    };

    UserService.setOldPasswordExists = function () {
      return data.oldPasswordExists = true;
    };

    UserService.getLanguageKey = function () {
      return this.get('language');
    };

    UserService.getCountry = function () {
      return this.get('country');
    };

    UserService.getSystemChannelId = function () {
      return this.get('systemChannelId');
    };

    UserService.getStatus = function () {
      return this.get('status');
    };

    UserService.getId = function () {
      return this.get('id');
    };

    UserService.getEmail = function () {
      return this.get('email');
    };

    UserService.getName = function () {
      return this.get('name');
    };

    UserService.getPhone = function () {
      return this.get('phone');
    };

    UserService.isPhoneVerified = function () {
      return this.get('isPhoneVerified');
    };

    UserService.verificationCodeExists = function () {
      return this.get('verificationCodeExists');
    };

    UserService.getProfileImageUrl = function () {
      return this.get('profileImageUrl');
    };

    UserService.getProfileImage = function () {
      return this.get('profileImage') || null;
    };

    UserService.getDefaultChannelId = function () {
      return this.get('defaultChannelId');
    };

    UserService.getNotificationCount = function () {
      return data.notificationCount;
    };

    UserService.getUser = function () {
      return data;
    };

    UserService.setUser = function (value) {
      LanguageUIService.setLanguage(value.language);
      return data = value;
    };

    UserService.removeUser = function () {
      return data = {};
    };

    UserService.mergeData = function (addData) {
      return lodash.merge(data, addData);
    };

    UserService.isEmpty = function () {
      return lodash.isEmpty(data);
    };


    /** Financial */

    UserService.getCurrencyId = function () {
      return data.financialAccount.getCurrencyId;
    };

    UserService.getAutoAuthorize = function () {
      return data.financialAccount.autoAuthorize;
    };

    UserService.getAutoAuthorizeMaxTicks = function () {
      return data.financialAccount.autoAuthorizeMaxTicks;
    };

    UserService.getPaymentMethod = function () {
      return data.financialAccount.paymentMethod;
    };

    UserService.getBalanceCurrent = function () {
      return data.financialAccount.currentBalance.currentBalance;
    };

    UserService.getBalanceEarned = function () {
      return data.financialAccount.currentBalance.currentBalanceEarnedTicks;
    };

    UserService.getBalanceFree = function () {
      return data.financialAccount.currentBalance.currentBalanceFreeTicks;
    };

    UserService.getBalanceTotal = function () {
      return UserService.getBalanceCurrent() + UserService.getBalanceEarned() + UserService.getBalanceFree();
    };

    UserService.pushBalance = function (amount) {
      return data.financialAccount.currentBalance.currentBalanceFreeTicks += amount;
    };

    UserService.isPaymentAutoAuthorized = function (price) {
      return data.financialAccount.autoAuthorize
        && data.financialAccount.autoAuthorizeMaxTicks >= price;
    };


    /** Filters */

    UserService.getCategoryFilter = function () {
      return data.categoryFilter;
    };

    UserService.setCategoryFilter = function (ids) {
      return data.categoryFilter = ids;
    };

    UserService.getLanguageFilter = function () {
      return data.languageFilter;
    };

    UserService.setLanguageFilter = function (ids) {
      return data.languageFilter = ids;
    };


    /** Preference */

    UserService.getPreference = function (type) {
      return data.preferences[type] || false;
    };

    UserService.getPreferences = function () {
      return data.preferences;
    };

    UserService.setPreference = function (name, value) {
      return data.preferences[name] = value;
    };
  }
})();