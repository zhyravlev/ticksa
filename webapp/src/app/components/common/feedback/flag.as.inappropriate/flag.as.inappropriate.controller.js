(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('FlagAsInappropriateController', FlagAsInappropriateController);

  /** @ngInject */
  function FlagAsInappropriateController($translate, $mdDialog, AdminService, jGrowlNotify, id, entity) {

    var ctrl = this;

    ctrl.flagAsInappropriate = function (form) {

      ctrl.attemptedSave = true;

      if (form.$valid) {
        AdminService.flagContentAsInappropriate(id, ctrl.text)
          .success(function (data, status) {
            new jGrowlNotify().success($translate.instant('TOAST_CONTENT_REPORT'));
            $mdDialog.hide();
          })
          .error(function (data, status) {
            $mdDialog.cancel();
          });
      }
    }
  }
})();
