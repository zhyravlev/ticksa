/* global angular, Evaporate, evaporateOptions */

(function (Evaporate, evaporateOptions) {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('eva', function () {
      return {
        _: new Evaporate(evaporateOptions),
        urlBase: 'http://' + evaporateOptions.bucket + '.s3-us-west-2.amazonaws.com/'
      };
    })

    .directive('evaporate', function (eva, $q, $rootScope, lodash, $timeout, $log, $translate, $window, AuthorizationTokenService) {

      function link(scope, element) {

        scope.addExternalFile = addExternalFile;

        function indexOf(arr, obj) {
          var imax = arr.length;
          for (var i = 0; i < imax; i++) if (angular.equals(arr[i], obj)) return i;
          return -1;
        }

        // allocate eva's data
        if (!scope.data) scope.data = {};

        // apply defaults for input parameters
        var data = scope.data,
          dir = data.dir ? (data.dir + '/') : '',
          timestampSeparator = data.timestampSeparator || '$',
          headersCommon = data.headersCommon || {},
          headersSigned = data.headersSigned || {},
          onFileAdd = (angular.isFunction(data.onFileAdd) ? data.onFileAdd : null),
          onFileProgress = (angular.isFunction(data.onFileProgress) ? data.onFileProgress : foo),
          onFileComplete = (angular.isFunction(data.onFileComplete) ? data.onFileComplete : foo),
          onFileCancelled = (angular.isFunction(data.onFileCancelled) ? data.onFileCancelled : foo),
          onFileError = (angular.isFunction(data.onFileError) ? data.onFileError : foo);

        // expose some info for parent scope
        data.ready = false;
        data.files = [];

        scope.startUpload = startUpload;

        var filelist = [];

        function cancelUploadFile() {

          $timeout(function () {
            if (filelist.length > 0) {
              angular.forEach(filelist, function (index) {
                eva._.cancel(index);
              });
            }
          }, 0);
        }

        function startUpload() {

          // don't close window in time uploading files
          if (!angular.isFunction($window.onbeforeunload)) {
            $window.onbeforeunload = function () {
              return $translate.instant('ON_BEFORE_UNLOAD_FILE');
            };
          }

          // process added files
          angular.forEach(data.files, function (file) {

            // process file attrs
            file.started = Date.now();
            file.path_ = file.path || (dir + file.started + timestampSeparator + file.name);
            file.url = file.url || (eva.urlBase + file.path_);

            var evaId = null;

            if (!file.error) {
              // queue file for upload
              var evaObject = {

                // filename, relative to bucket
                name: file.path_,

                // content
                file: file,

                // headers
                contentType: file.type || 'binary/octet-stream',
                signHeaders: {token: AuthorizationTokenService.getToken()},
                notSignedHeadersAtInitiate: lodash.assign(headersCommon, file.headersCommon),
                xAmzHeadersAtInitiate: lodash.assign(headersSigned, file.headersSigned),

                // event callbacks
                complete: function () {
                  // check file as completed
                  file.completed = true;

                  // execute user's callback
                  onFileComplete(file);

                  // If all the files already loaded, we can close the window.
                  $rootScope.uploadFileInProgress--;

                  if ($rootScope.uploadFileInProgress == 0) {
                    $window.onbeforeunload = null;
                  }

                  // update ui
                  scope.$apply();
                },
                progress: function (progress) {

                  // update progress
                  file.progress = Math.round(progress * 10000) / 100;
                  file.timeLeft = Math.round(
                    (100 - file.progress) / file.progress *
                    (Date.now() - file.started) / 1000
                  );

                  // execute user's callback
                  onFileProgress(file);

                  // update ui
                  scope.$apply();
                },
                error: function (message) {
                  // remove file from the queue
                  var index = indexOf(data.files, file);
                  if (index !== -1) data.files.splice(index, 1);

                  // execute user's callback
                  onFileError(file, message, this.uploadId);

                  // update ui
                  scope.$apply();
                },
                cancelled: function () {

                  // remove file from the queue
                  var index = indexOf(data.files, file);
                  if (index !== -1) data.files.splice(index, 1);

                  // update ui
                  scope.$apply();
                }
              };

              evaId = eva._.add(evaObject);
              filelist.push(evaId);
            }
          });

          $rootScope.uploadFileInProgress += data.files.length;


          $timeout(function () {
            // update ui
            scope.$apply();
          });

          data.ready = true;

        }

        // ready..
        if (eva._.supported) {
          element.bind('change', function (event) {

            angular.forEach(event.target.files, function (file) {
              file.cancel = function () {
                data.files = data.files.filter(function (existingFile) {
                  return existingFile.id != file.id;
                });

                onFileCancelled(file);
              };
              onFileAdd(file);
              data.files.push(file);
            });

            event.target.value = "";

            scope.$apply();

          });
        }

        function addExternalFile(file) {
          file.cancel = function () {
            data.files = data.files.filter(function (existingFile) {
              return existingFile.id != file.id;
            });
            eva._.cancel(file.id);
            onFileCancelled(file);
          };
          onFileAdd(file);
          data.files.push(file);

        }
      }

      return {
        restrict: 'A',
        link: link,
        scope: {
          data: '=evaModel',
          startUpload: '=',
          addExternalFile: '=addExternalFile',
          cancelUploadFile: '=cancelUploadFile'
        }
      };
    });

})(Evaporate, config.evaporate);
