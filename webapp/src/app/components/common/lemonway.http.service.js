(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('LemonwayHttpService', LemonwayHttpService);

  /** @ngInject */
  function LemonwayHttpService($http, API) {
    var LemonwayHttpService = this;

    LemonwayHttpService.getWalletDetails = function () {
      return $http.get(API.URL + '/lemonway/wallet-details');
    };
  }

})();
