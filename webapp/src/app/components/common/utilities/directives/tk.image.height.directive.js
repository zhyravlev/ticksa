(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkImageHeight', tkImageHeight);

  /** @ngInject */
  function tkImageHeight() {

    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        elem.bind('load', function () {
          angular.element(elem).parent().css({
            height: elem[0].offsetHeight
          });
        })
      }

    }

  }
})();
