(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkBrowserNotSupported', tkBrowserNotSupported);

  /** @ngInject */
  function tkBrowserNotSupported($log, $window, $cookies, tkModernizr) {
    $log = $log.getInstance('tkBrowserNotSupported');

    var DONT_SHOW_BROWSER_NOTIFICATION_COOKIE = 'dont-show-browser-warn';

    return {
      restrict: 'E',
      template: '' +
      '<div class="browser-not-supported" data-ng-class="{\'in\': ctrl.showNotification}">' +
      '<h2>We cannot guarantee that Ticksa works properly on your browser`s version. Please <a href="http://outdatedbrowser.com/en" target="_blank">upgrade</a> to the latest version.</h2>' +
      '<h3>Click <a class="continue" href="javascript:void(0)" data-ng-click="ctrl.clickToContinue($event)">HERE</a>  to continue to Ticksa.</h3>' +
      '</div>',
      controllerAs: 'ctrl',
      controller: function () {
        var ctrl = this;

        ctrl.showNotification = !$cookies.get(DONT_SHOW_BROWSER_NOTIFICATION_COOKIE)
          && !tkModernizr.flexbox;

        if (ctrl.showNotification) {
          $log.toServer.warn('Unsupported browser detected. UserAgent: %s', $window.navigator.userAgent);
        }

        ctrl.clickToContinue = function (event) {
          event.preventDefault();
          ctrl.showNotification = false;
          setCookie();
        };

        // ToDo [d.borisov]
        // for unknown reason configuring $cookiesProvider doesnt work.
        // that's why direct $cookie 'expires' used
        var setCookie = function () {
          var d = new Date();
          d.setTime(d.getTime() + (360 * 24 * 60 * 60 * 1000)); // 360 days
          $cookies.put(DONT_SHOW_BROWSER_NOTIFICATION_COOKIE, 'true', {expires: d});
        };
      }
    }
  }
})();
