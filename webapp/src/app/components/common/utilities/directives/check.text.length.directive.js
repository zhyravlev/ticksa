(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkCheckTextLength', tkCheckTextLength);

  /** @ngInject */
  function tkCheckTextLength($q) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (s, e, a, model) {

        model.$asyncValidators.tkLongText = function (modelValue) {

          if (model.$isEmpty(modelValue)) {
            return $q.resolve();
          }

          var def = $q.defer();

          if (modelValue.length > 250) {
            def.reject();
          } else {
            def.resolve();
          }

          return def.promise;
        };
      }
    }
  }
})();