(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkBackground', tkBackground);

  /** @ngInject */
  function tkBackground() {

    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        var url = attrs.tkBackground;
        elem.css({
          'background-image': 'url(' + url + ')',
          'background-size': 'cover'
        });
      }

    }

  }
})();
