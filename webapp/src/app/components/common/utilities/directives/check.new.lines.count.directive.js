(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkCheckNewLinesCount', tkCheckNewLinesCount);

  /** @ngInject */
  function tkCheckNewLinesCount($q) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (s, e, a, model) {

        model.$asyncValidators.tkNewLines = function (modelValue) {

          if (model.$isEmpty(modelValue)) {
            return $q.resolve();
          }

          var def = $q.defer();

          var re = /(^)/gm, new_line_count = 0, m;

          while ((m = re.exec(modelValue)) !== null) {
            if (m.index === re.lastIndex) {
              re.lastIndex++;
              new_line_count += 1;
            }
          }

          if (new_line_count > 8) {
            def.reject();
          } else {
            def.resolve();
          }

          return def.promise;
        };
      }
    }
  }
})();