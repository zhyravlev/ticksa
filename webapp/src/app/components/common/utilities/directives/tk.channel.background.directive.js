(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkChannelBackground', tkChannelBackground);

  /** @ngInject */
  function tkChannelBackground($rootScope, thumbnailsS3CdnFilter, EVENTS) {

    return {
      restrict: 'A',
      link: function (scope, elem) {

        var eventChangeChannelBackground = $rootScope.$on(EVENTS.CHANGE_CHANNEL_BACKGROUND, function (event, options) {

          $rootScope.channelBackgroundImage = true;

          if(options != null){
            elem.css({
              'background-image': 'url(' + thumbnailsS3CdnFilter(options.key, 1600, 1200) + ')'
            })
          }
        });

        var eventStateChangeSuccess = $rootScope.$on('$stateChangeSuccess', function (event, toState) {
          if (toState.name !== 'base.teaser' || toState.name !== 'main.channelContentList') {

            $rootScope.channelBackgroundImage = false;

            elem.removeAttr('style')
          }
        });

        scope.$on('$destroy', function () {
          eventChangeChannelBackground();
          eventStateChangeSuccess()
        });
      }
    }
  }
})();
