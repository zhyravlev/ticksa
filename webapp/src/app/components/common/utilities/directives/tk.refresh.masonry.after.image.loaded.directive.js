(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkRefreshMasonryAfterImageLoaded', tkRefreshMasonryAfterImageLoadedDirective);

  /** @ngInject */
  function tkRefreshMasonryAfterImageLoadedDirective($rootScope) {

    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        element.bind('load', function () {
          $rootScope.$broadcast('tkMasonryResize', {});
          element.unbind('load');
        });

      }
    };

  }
})();
