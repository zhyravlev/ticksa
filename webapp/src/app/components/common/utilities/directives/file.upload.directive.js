(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkFileUpload', fileUpload);

  /** @ngInject */
  function fileUpload() {

    return {
      templateUrl: 'app/components/common/utilities/directives/file.upload.directive.html',
      scope: {
        startUpload: '=?',
        cancelUpload: '=?',
        onUploadFinish: '=?',
        onUploadCancel: '=?',
        clearFileList: '=?',
        fileList: '=?',
        file: '=?',
        multiple: '@',
        fileInputId: '@',
        addFile: '=?',
        triggerFileSelect: '=?',
        accept: '@'
      },
      link: function (scope) {
      },
      controllerAs: 'ctrl',
      controller: function ($scope, $timeout, $rootScope, $log,
                            UuidService, AuthorizationTokenService, FileTypeService,
                            CONTENT_TYPES, EVENTS) {

        var ctrl = this;

        $scope.addFile = function (file) {
          ctrl.addExternalFile(file);
        };

        $scope.clearFileList = function () {
          ctrl.evaData.files = [];
        };

        $scope.startUpload = function () {
          if (ctrl.evaData.files.length == 0 && angular.isFunction($scope.onUploadFinish)) {
            $scope.onUploadFinish();
          } else {
            ctrl.startEvaUpload();
          }

          $rootScope.$broadcast(EVENTS.AFTER_START_UPLOAD_FILE);
        };

        $scope.cancelUpload = function (uploadId) {
          ctrl.cancelEvaUpload(uploadId);
        };

        $scope.triggerFileSelect = function () {
          angular.element('#' + $scope.fileInputId).trigger('click');
        };

        var fileCount = 0;

        ctrl.evaData = {

          files: [],
          headersCommon: {},
          headersSigned: {
            'x-amz-acl': 'public-read',
            'x-amz-storage-class': 'STANDARD'
          },

          onFileProgress: function (file) {
            $rootScope.$broadcast(EVENTS.PROGRESS_BAR_PROCESS, file);
          },

          onFileComplete: function (file) {

            fileCount--;

            if (fileCount == 0 && angular.isFunction($scope.onUploadFinish)) {
              $scope.onUploadFinish();
            }

            $rootScope.$broadcast(EVENTS.ON_COMPLETE_UPLOAD_FILE, {
              name: file.name,
              file: getFileData(file),
              type: FileTypeService.extractFileType(file)
            });
          },

          onFileError: function (file, message, uploadId) {
            fileCount--;
          },

          onFileCancelled: function (file, uploadId) {

            if ($scope.multiple) {
              fileCount--;
              $scope.fileList = $scope.fileList.filter(function (existingFile) {
                return existingFile.key != file.key;
              });
            } else {
              fileCount = 0;
              $scope.file = null;
            }
          },

          onFileAdd: function (file) {

            var uuid = UuidService.uuid.v4();
            file.signHeaders = {
              token: AuthorizationTokenService.getToken()
            };
            file.headersCommon = {
              'Content-Disposition': 'attachment; filename="' + encodeURI(file.name) + '"'
            };
            file.id = uuid;
            file.extension = file.name.substr((Math.max(0, file.name.lastIndexOf(".")) || Infinity) + 1);
            file.path = uuid + '.' + file.extension;
            file.key = file.path;

            if ($scope.multiple) {
              fileCount++;
              $scope.fileList.push(getFileData(file));
            } else {
              fileCount = 1;
              ctrl.evaData.files = [];
              $scope.file = getFileData(file);
            }
          }
        };

        /**
         * @file object
         * @return object */
        function getFileData(file) {
          return {
            name: file.name,
            extension: file.extension,
            key: file.path,
            size: file.size ? file.size : 0,
            isUploaded: true,
            raw: file
          };
        }
      }
    }
  }
})();
