(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkInputFocus', tkInputFocus);

  /** @ngInject */
  function tkInputFocus($timeout) {

    return {
      restrict: 'A',
      scope: {
        tkInputFocus: '='
      },
      require: 'ngModel',
      link: function($scope, $elem, $attr, model) {
        $scope.$watch('tkInputFocus', function(data) {
          if(data == true) {
            $timeout(function() {
              if(model.$viewValue){
                $elem[0].select();
              } else {
                $elem[0].focus();
              }
            }, 100);
          }
        });

        $elem.bind('keyup', function(event) {
          if (event.which === 13) {
            $elem.blur();
          }
        });
      }
    };
  }
})();
