(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkAnimateChange', animateChange);

  /** @ngInject */
  function animateChange($animate) {

    return {
      restrict: 'A',
      scope: {
        animation: '@',
        variable: '='
      },
      link: function (scope, elem, attrs) {

        scope.$watch('variable', function (newVal, oldVal) {
          $animate.removeClass(elem, scope.animation).then(function () {
            $animate.addClass(elem, scope.animation);
          })
        })
      }

    }

  }
})();
