(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkIsImageLoaded', tkIsImageLoaded);

  /** @ngInject */
  function tkIsImageLoaded($timeout) {

    return {
      restrict: 'A',
      scope: {
        loaded: '='
      },
      link: function (scope, element, attrs) {
        scope.loaded = false;
        element.bind('load', function () {
          $timeout(function () {
            scope.loaded = true;
          })
        });
      }
    };

  }
})();
