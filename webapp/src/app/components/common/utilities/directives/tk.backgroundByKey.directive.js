(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkBackgroundByKey', tkBackgroundByKey);

  /** @ngInject */
  function tkBackgroundByKey(thumbnailsS3CdnFilter, s3cdnFilter) {

    return {
      restrict: 'A',
      scope: {
        tkBackgroundByKey: "="
      },
      link: function ($scope, $elem) {
        return $scope.$watch('tkBackgroundByKey', function (data) {
          if (typeof data == 'object') {

            var getPosterUrl = function(thumbnailImageKey, thumbnailWidth, thumbnailHeight) {
              if (thumbnailWidth == null && thumbnailHeight == null) {
                return s3cdnFilter(thumbnailImageKey);
              }
              var width = thumbnailWidth || 300;
              var height = thumbnailHeight || '-';
              return thumbnailsS3CdnFilter(thumbnailImageKey, width, height);
            };

            $elem.css({
              'background-image': 'url(' + getPosterUrl(data.key, data.width, data.height) + ')'
            });

            if (data.hasOwnProperty('responsive')) {
              $elem.addClass('background-responsive');
            }
          }
        });
      }
    }
  }
})();
