(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkAvatar', tkAvatar);

  /** @ngInject */
  function tkAvatar($timeout, $translate) {

    /**
     * Usage:
     *
     * <tk-avatar data-avatar-url="ctrl.avatar" data-avatar-class="medium"></tk-avatar>
     *
     * @param {avatar-url} - Angular model. Url to avatar image.
     * @param {avatar-class} - String. Avatar class. Available values: small (20px), medium (32px), large (75px)
     * @param {avatar-tooltip} - String. Translate constant.
     */

    return {
      restrict: 'E',
      transclude: true,
      scope: {
        avatarUrl: '=',
        avatarClass: '@',
        avatarTooltip: '@'
      },
      templateUrl: 'app/components/common/utilities/directives/tk.avatar.html',
      link: function ($scope, $em, $attr) {

        $timeout(function () {

          $scope.avatarTooltip = $scope.avatarTooltip ? $translate.instant($scope.avatarTooltip) : false;

          if ($attr.ngClick) {
            $em.addClass('cursor');
          }

          var _class;

          switch ($scope.avatarClass) {
            case 'small':
              _class = 'avatar-icon-small';
              break;
            case 'medium':
              _class = 'avatar-icon-medium';
              break;
            case 'large':
              _class = 'avatar-icon-large';
              break;
            case 'header':
              _class = 'avatar-in-header';
              break;
            default:
              _class = false;
          }

          if (_class) {
            $em.find('.avatar-icon').addClass(_class)
          }
        });

        $scope.$watch('avatarUrl', function (avatarUrl) {
          if (avatarUrl) {
            $em.find('.image').css({
              'background-image': 'url(' + avatarUrl + ')'
            });
          }
        });
      }
    }
  }
})();
