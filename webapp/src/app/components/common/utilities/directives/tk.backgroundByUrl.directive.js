(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkBackgroundByUrl', tkBackgroundByUrl);

  /** @ngInject */
  function tkBackgroundByUrl() {

    return {
      restrict: 'A',
      link: function ($scope, $elem, $attr) {
        var url = $attr.tkBackgroundByUrl;
        if (url) {
          $elem.css({
            'background-image': 'url(' + url + ')'
          });
        }
      }
    }
  }
})();
