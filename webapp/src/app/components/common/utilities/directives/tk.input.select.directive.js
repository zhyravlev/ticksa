(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkInputSelect', tkInputSelect);

  /** @ngInject */
  function tkInputSelect() {

    return {
      restrict: 'A',
      scope: {
        tkInputSelect: '='
      },

      link: function ($scope, $elem) {
        return $scope.$watch('tkInputSelect', function(data){
          if(data == true){
            $elem.select()
          }
        })
      }
    }
  }
})();
