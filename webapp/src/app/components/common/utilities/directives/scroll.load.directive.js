(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkTest', tkTest);

  /** @ngInject */
  function tkTest() {

    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        angular.element(element).triggerHandler('click');
      }

    }

  }
})();
