(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkPopupProgressBar', tkPopupProgressBar);

  /** @ngInject */
  function tkPopupProgressBar() {
    return {
      restrict: 'E',
      templateUrl: 'app/components/common/utilities/directives/tk.progressbar.directive.html',
      scope: {
        space: "=?"
      },
      controllerAs: 'ctrl',
      controller: function ($rootScope, $scope, $timeout, $log, $window, EVENTS) {

        var original = function () {
          return {
            showProgress: false,
            showInfo: true,
            showSuccess: false,
            fileName: '',
            InProgress: 0,
            percent: 0
          }
        };

        $scope.space = original();

        var location_hash = $window.location.hash;
        var always = true; // @boolean that would show the process of uploading is always

        $scope.$on(EVENTS.PROGRESS_BAR_PROCESS, function (event, options) {

          if (location_hash != $window.location.hash || always) {
            $scope.space.showProgress = true;
          }

          $scope.space.fileName = options.name;
          $scope.space.percent = options.progress;
          $scope.space.InProgress = $rootScope.uploadFileInProgress;

          if (options.progress == 100 && $rootScope.uploadFileInProgress == 1) {

            $scope.space.showInfo = false;
            $scope.space.showSuccess = true;

            $timeout(function () {
              $scope.space.showProgress = false;
            }, 3000);

            $timeout(function () {
              $scope.space = original();
            }, 3500);
          }
        });
      }
    }
  }
})();