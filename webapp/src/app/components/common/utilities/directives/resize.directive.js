(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkResize', tkResize);

  /** @ngInject */
  function tkResize($window, lodash) {

    return {
      link: function (scope) {
        angular.element($window).on('resize', lodash.debounce(function () {
          scope.$broadcast('resize::resize');
        }, 100))
      }
    }
  }
})();
