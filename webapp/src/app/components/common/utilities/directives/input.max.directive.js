(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('ngMax', ngMax);

  /** @ngInject */
  function ngMax() {

    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, elem, attr, ctrl) {
        scope.$watch(attr.ngMax, function () {
          ctrl.$setViewValue(ctrl.$viewValue);
        });
        var maxValidator = function (value) {
          var max = scope.$eval(attr.ngMax) || Infinity;
          if (value != null && value > max) {
            ctrl.$setValidity('max', false);
            return undefined;
          } else {
            ctrl.$setValidity('max', true);
            return value;
          }
        };

        ctrl.$parsers.push(maxValidator);
        ctrl.$formatters.push(maxValidator);
      }
    };
  }
})();
