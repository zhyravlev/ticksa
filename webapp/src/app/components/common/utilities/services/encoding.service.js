(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('EncodingService', EncodingService);

  /** @ngInject */
  function EncodingService($http, API) {
    return {
      encodeNumber: encodeNumber,
      encodeContentAndUserId: encodeContentAndUserId,
      encodePlaylistId: encodePlaylistId
    };

    function encodeNumber(number) {
      return $http.get(API.URL + '/utilities/encode-number?number=' + number);
    }

    function encodeContentAndUserId(contentId, userId) {
      return $http.get(API.URL + '/utilities/encode-content-user-id', {params: {contentId: contentId, userId: userId}});
    }

    function encodePlaylistId(playlistId) {
      return $http.get(API.URL + '/utilities/encode-playlist-id', {params: {playlistId: playlistId}});
    }
  }

})();
