(function () {
  'use strict';

  /*eslint angular/definedundefined: 0*/

  angular
    .module('ticksaWebapp')
    .service('FrontendService', FrontendService);

  /** @ngInject */
  function FrontendService(tkModernizr, $document, $window) {
    var FrontendService = this;

    FrontendService.device = {
      isTouch: !!tkModernizr.touch
    };

    // http://stackoverflow.com/questions/9847580
    FrontendService.browser = {
      // Opera 8.0+
      isOpera: (!!$window.opr && !!opr.addons) || !!$window.opera || navigator.userAgent.indexOf(' OPR/') >= 0,

      // Firefox 1.0+
      isFirefox: typeof InstallTrigger !== 'undefined',

      // At least Safari 3+: "[object HTMLElementConstructor]"
      isSafari: Object.prototype.toString.call($window.HTMLElement).indexOf('Constructor') > 0,

      // Internet Explorer 6-11
      isIE: /*@cc_on!@*/false || !!$document[0].documentMode,

      // Edge 20+
      isEdge: function(){
        try {
          return !isIE && !!$window.StyleMedia
        } catch (e) {}
      },

      // Chrome 1+
      isChrome: !!$window.chrome && !!$window.chrome.webstore,

      // Blink engine detection
      isBlink: function(){
        try {
          return (isChrome || isOpera) && !!$window.CSS
        } catch (e) {}
      }
    }
  }
})();
