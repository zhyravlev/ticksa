(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('LanguageLoaderService', LanguageLoaderService);

  /** @ngInject */
  function LanguageLoaderService($http, $q, CDN) {
    return function (options) {
      var def = $q.defer();
      $http.get(CDN.assets.cloudFront + CDN.assets.key + '/assets/languages/' + options.key + '.json')
        .success(function (data) {
          def.resolve(data);
        })
        .error(function (err) {
          def.reject(err);
        });
      return def.promise;
    }

  }
})();
