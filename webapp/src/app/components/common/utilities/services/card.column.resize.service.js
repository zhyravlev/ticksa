(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('CardColumnResizeService', CardColumnResizeService);

  /** @ngInject */
  function CardColumnResizeService($window) {

    var Service = {};

    Service.sortContentsInColumns = sortContentsInColumns;

    function sortContentsInColumns(columnsNumber, pageSize, rawContents) {
      var contentList = [];
      var currentPage = {};
      for (var i = 0; i < rawContents.length; i++) {

        if (i % pageSize == 0) {
          if (Object.keys(currentPage).length != 0) {
            contentList.push(currentPage);
          }
          currentPage = {};
          for (var columnNumber = 0; columnNumber < columnsNumber; columnNumber++) {
            currentPage[columnNumber] = [];
          }
        }
        currentPage[i % columnNumber].push(rawContents[i]);
      }
      contentList.push(currentPage);

      return contentList;
    }

    return Service;

  }
})();
