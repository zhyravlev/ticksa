(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('PaymentConfirmationService', PaymentConfirmationService);

  /** @ngInject */
  function PaymentConfirmationService($q, UserService, AccountService, ModalService, LinkContentService, TICKSER_PREFERENCES) {
    var PaymentConfirmationService = this;

    PaymentConfirmationService.confirmTicksAction = function (price, preference) {
      if (isAutoConfirmed(price, preference)) {
        return $q.resolve();
      }

      var def = $q.defer();
      ModalService.showConfirmTicksActionModal(price)
        .then(function (isNoNeedToShowSuchConfirmations) {
          updatePreferenceIfNeeded(isNoNeedToShowSuchConfirmations, preference);
          def.resolve();
        })
        .catch(function () {
          def.reject();
        });
      return def.promise;
    };

    // according to APP-722 not used - no confirmations when buy content
    PaymentConfirmationService.confirmContentBuy = function (content) {
      if (isAutoConfirmed(content.tickPrice, TICKSER_PREFERENCES.SKIP_BUY_CONTENT_NOTIFICATION)) {
        LinkContentService.openBlankWindowIfNeeded(content);
        return $q.resolve();
      }

      var def = $q.defer();
      ModalService.showConfirmContentBuyModal(content)
        .then(function (isNoNeedToShowSuchConfirmations) {
          updatePreferenceIfNeeded(isNoNeedToShowSuchConfirmations, TICKSER_PREFERENCES.SKIP_BUY_CONTENT_NOTIFICATION);
          def.resolve();
        })
        .catch(function () {
          def.reject();
        });
      return def.promise;
    };

    function isAutoConfirmed(price, preference) {
      return UserService.getPreference(preference) || UserService.isPaymentAutoAuthorized(price);
    }

    function updatePreferenceIfNeeded(isNoNeedToShowSuchConfirmations, preference) {
      if (isNoNeedToShowSuchConfirmations) {
        UserService.setPreference(preference, true);
        AccountService.updatePreferences(UserService.getPreferences());
      }
    }
  }

})();
