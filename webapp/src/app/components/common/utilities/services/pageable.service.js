(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('Pageable', Pageable);

  /** @ngInject */
  function Pageable(lodash) {

    var Pageable = function () {
      this.filter = {};
    };

    Pageable.prototype.addFilter = function (name, value) {
      var _this = this;
      if(lodash.isArray(value)) {
        lodash.forEach(value, function(val, index) {
          _this.filter['filter[' + name + ']['+ index +']'] = val;
        })
      } else {
        this.filter['filter[' + name + ']'] = value;
      }
      return this;
    };

    Pageable.prototype.addArrayFilter = function (name, value) {
      this.filter['filter[' + name + ']'] = value;
      return this;
    };

    Pageable.prototype.addOrderFilter = function (property, type) {
      this.filter['filter[order][' + property + ']'] = type || 'desc';
      return this;
    };

    Pageable.prototype.page = function (page) {
      if(page != null) {
        this.filter.page = page;
      }
      return this;
    };

    Pageable.prototype.pageSize = function (pageSize) {
      if(pageSize != null) {
        this.filter.pageSize = pageSize;
      }
      return this;
    };

    Pageable.prototype.searchString = function (searchString) {
      if(searchString) {
        this.filter.searchString = searchString;
      }
      return this;
    };

    Pageable.prototype.build = function () {
      return this.filter;
    };

    return Pageable;

  }
})();
