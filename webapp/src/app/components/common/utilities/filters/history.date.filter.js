(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('dayGroup', dayGroup);


  /** @ngInject */
  function dayGroup($translate, tkMoment) {
    var minuteInterval = 1000 * 60;
    var hourInterval = minuteInterval * 60;
    var dayInterval = hourInterval * 24;
    var weekInterval = dayInterval * 7;
    var monthInterval = dayInterval * 30;
    var yearInterval = monthInterval * 365;

    return function (input) {
      var today = new Date();
      var dayGroup = new Date(input);
      var timeDiff = Math.abs(today.getTime() - dayGroup.getTime());
      var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24));
      if (diffDays == 0) {
        return $translate.instant('HISTORY_TODAY');
      } else if (diffDays == 1) {
        return $translate.instant('HISTORY_YESTERDAY');
      } else if (diffDays < 7) {
        return tkMoment(dayGroup).format('dddd');
      } else {
        return diffDays + ' ' + $translate.instant('HISTORY_DAYS');
      }
    }
  }

})();
