(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('tkRound', tkRound);

  /** @ngInject */
  function tkRound() {
    return function (input) {
      return Math.round(input);
    }
  }
})();
