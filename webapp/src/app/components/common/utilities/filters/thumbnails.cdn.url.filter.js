(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('thumbnailsS3Cdn', thumbnailsS3Cdn);

  /** @ngInject */
  function thumbnailsS3Cdn(CDN) {
    return function (input, width, height) {
      return CDN.s3.thumbnailCloudFront + 'resize/' + width + 'x' + height + '/' + input;
    }
  }

})();
