(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('tkConvertTicks', tkConvertTicks);

  /** @ngInject */
  function tkConvertTicks() {
    return function (input) {

      var selectLegend = function (ticks) {
        if (ticks == 1) {
          return 'cent';
        }
        else if (ticks <= 50) {
          return 'cents';
        }
        else {
          return '€';
        }
      };

      var selectAmount = function (ticks) {
        var num = Number(ticks);
        if (isNaN(num)) {
          return '0';
        }

        var cents = Math.round(num);
        if (cents <= 50) {
          return cents.toFixed();
        }

        var eur = cents * 0.01;

        if (cents % 100 == 0) {
          return eur.toFixed();
        }
        else if (cents % 10 == 0) {
          return eur.toFixed(1);
        }
        else {
          return eur.toFixed(2);
        }
      };

      return selectAmount(input) + ' ' + selectLegend(input);
    }
  }
})();
