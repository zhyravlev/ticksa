(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('textLength', textLength);

  /** @ngInject */
  function textLength() {
    return function (input) {
      if (!input) return "";
      return Math.ceil(input / 1000);
    }
  }

})();
