(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('nl2br', nl2br);

  /** @ngInject */
  function nl2br($sce) {
    return function (str) {
      return $sce.trustAsHtml( str.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1 <br> $2') );
    }
  }
})();
