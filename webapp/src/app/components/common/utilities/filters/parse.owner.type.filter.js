(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('parseOwnerType', parseOwnerType);

  /** @ngInject */
  function parseOwnerType() {

    var ownerTypes = {
      0: 'Creator',
      1: 'Manager',
      2: 'Participant'
    };

    return function (input) {
      return ownerTypes[input];
    }
  }

})();
