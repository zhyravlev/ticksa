(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('checkChannelOwnerType', checkChannelOwnerType);

  /** @ngInject */
  function checkChannelOwnerType() {

    return function (input, types) {

      for (var i = 0; i < types.length; i++) {
        if (input == types[i]) {
          return true;
        }
      }
      return false;
    }
  }

})();
