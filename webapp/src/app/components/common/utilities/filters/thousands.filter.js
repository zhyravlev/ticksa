(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('thousands', thousandsFilter);

  /** @ngInject */
  function thousandsFilter() {
    return function (input) {

      var num = parseInt(input);
      if (num >= 1000) {
        return (num / 1000).toFixed(1) + 'k';
      } else {
        return input;
      }
    }
  }

})();
