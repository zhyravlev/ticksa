(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('checkExpiryDate', checkExpiryDate);

  /** @ngInject */
  function checkExpiryDate() {
    return function (input) {
      return ((new Date(input) - new Date()) / (1000 * 60 * 60 * 24)).toFixed();
    }
  }

})();
