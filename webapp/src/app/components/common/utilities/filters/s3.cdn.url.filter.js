(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('s3cdn', s3cdn);

  /** @ngInject */
  function s3cdn(CDN) {
    return function (input) {
      return CDN.s3.cloudFront + input;
    }
  }

})();
