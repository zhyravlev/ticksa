(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('videoLength', videoLength);

  /** @ngInject */
  function videoLength() {
    return function (input) {
      if (!input) return "";
      return Math.ceil(input / 1000 / 60);
    }
  }

})();
