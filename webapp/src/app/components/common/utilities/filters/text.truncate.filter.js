(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('truncate', cdnFilter);

  /** @ngInject */
  function cdnFilter() {
    return function (input, length) {

      if (input.length > length) {
        input = input.slice(0, length) + '...';
      }
      return input;
    }
  }

})();
