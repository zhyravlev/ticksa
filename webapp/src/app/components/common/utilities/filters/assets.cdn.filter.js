(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .filter('tkAssetsCdn', tkAssetsCdn);

  /** @ngInject */
  function tkAssetsCdn(CDN) {
    return function (input) {
      return CDN.assets.cloudFront + CDN.assets.key + '/' + input;
    }
  }

})();
