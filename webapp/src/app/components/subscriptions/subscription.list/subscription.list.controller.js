(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('SubscriptionListController', SubscriptionListController);

  /** @ngInject */
  function SubscriptionListController($log, $rootScope, $state, ChannelService, HeaderTitleService) {

    $log = $log.getInstance('SubscriptionListController');

    var ctrl = this;

    HeaderTitleService.setTranslatedTitle('COMMONS_FAVORITES');

    ctrl.channelDetails = channelDetails;
    ctrl.unsubscribeToChannel = unsubscribeToChannel;

    ChannelService.getSubscribedChannels()
      .success(function (data, status) {
        ctrl.channels = data;
      })
      .error(function (err) {
        $log.log(err);
      });

    function channelDetails(channel) {
      $state.go('main.channelContentList', {channelId: channel.id});
    }

    function unsubscribeToChannel(channelToRemove) {
      ChannelService.unsubscribeToChannel(channelToRemove.id)
        .success(function (data, status) {
          $rootScope.$broadcast('SubscriptionChange', {type: 0, channel: channelToRemove});
          ctrl.channels = ctrl.channels.filter(function (channel) {
            return channelToRemove.id != channel.id;
          })
        })
        .error(function (err) {
          $log.log(err);
        })
    }
  }

})();
