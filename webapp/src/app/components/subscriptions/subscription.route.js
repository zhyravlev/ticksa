(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.subscriptions', {
        url: '/subscriptions',
        templateUrl: 'app/components/subscriptions/subscription.list/subscription.list.html',
        controller: 'SubscriptionListController',
        controllerAs: 'ctrl'
      });
  }

})();
