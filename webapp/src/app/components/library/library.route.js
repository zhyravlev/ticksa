(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.library', {
        url: '/library',
        template: '<ui-view flex></ui-view>'
      })
      .state('main.library.media', {
        url: '/media',
        templateUrl: 'app/components/library/media/media.html',
        controller: 'MediaController',
        controllerAs: 'ctrl'
      })
  }

})();
