(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('MediaController', MediaController);

  /** @ngInject */
  function MediaController($translate, $timeout, $scope, $rootScope, $interval, $log,
                           lodash, LibraryService, Pageable, ModalService, UserService, HeaderTitleService, FileTypeService, jGrowlNotify,
                           EVENTS, MEDIA_TYPE, MEDIA_FILE_USED, SORTING_ORDER_FOR_FILTER) {

    $log = $log.getInstance('MediaController');

    var ctrl = this;

    HeaderTitleService.setTranslatedTitle('COMMONS_YOUR_MEDIA');

    ctrl.VIDEO_FILE_ACCEPT_EXTENSIONS = FileTypeService.AVAILABLE_VIDEO_FILE_EXTENSIONS_LIST;
    ctrl.IMAGE_FILE_ACCEPT_EXTENSIONS = FileTypeService.AVAILABLE_IMAGE_FILE_EXTENSIONS_LIST;

    ctrl.usedFilter = {
      values: [
        {
          key: $translate.instant('FILTER__USED__ALL'),
          value: MEDIA_FILE_USED.ALL
        },
        {
          key: $translate.instant('FILTER__USED__USED'),
          value: MEDIA_FILE_USED.USED
        },
        {
          key: $translate.instant('FILTER__USED__UNUSED'),
          value: MEDIA_FILE_USED.UNUSED
        }
      ],
      selected: MEDIA_FILE_USED.ALL
    };

    ctrl.mediaTypeFilter = {
      values: [
        {
          key: $translate.instant('FILTER__TYPE_IMAGE'),
          value: MEDIA_TYPE.IMAGE
        },
        {
          key: $translate.instant('FILTER__TYPE_VIDEO'),
          value: MEDIA_TYPE.VIDEO
        }
      ],
      selected: [MEDIA_TYPE.IMAGE, MEDIA_TYPE.VIDEO]
    };

    ctrl.createdAtFilter = {
      values: [
        {
          key: $translate.instant('FILTER__SORTED__CREATED_DESC'),
          value: SORTING_ORDER_FOR_FILTER.DESC
        },
        {
          key: $translate.instant('FILTER__SORTED__CREATED_ASC'),
          value: SORTING_ORDER_FOR_FILTER.ASC
        }
      ],
      selected: SORTING_ORDER_FOR_FILTER.DESC
    };

    ctrl.media = [];
    ctrl.image = {file: {}};
    ctrl.video = {file: {}};

    ctrl.imageFileList = [];

    ctrl.deleteImage = deleteImage;
    ctrl.deleteVideo = deleteVideo;
    ctrl.deleteMedia = deleteMedia;

    ctrl.checkAndStartUploadImage = checkAndStartUploadImage;
    ctrl.checkAndStartUploadVideo = checkAndStartUploadVideo;
    ctrl.uploadStarted = false;

    refresh();

    ctrl.rotate = {
      timeout: undefined,
      data: [],
      handler: function (data) {

        if (typeof ctrl.rotate.timeout == 'object') {
          $timeout.cancel(ctrl.rotate.timeout);
        }

        var index,

        // We are looking for an image of the array
          getIndex = function () {
            return lodash.findIndex(ctrl.rotate.data, function (o) {
              return o.id == data.id
            });
          };

        // If the image was found
        if ((index = getIndex()) !== -1) {

          // When you turned 360 degrees
          // Was removed from the array
          // Since 360 degrees, visually equal 0
          if (data.deg == 360) {
            ctrl.rotate.data.splice(index, 1)
          } else {
            // Or update the data for this image
            ctrl.rotate.data[index].deg = data.deg
          }

        } else {
          // Adding to the array if there is no image
          ctrl.rotate.data.push(data)
        }

        ctrl.rotate.timeout = $timeout(function () {
          sendRotateData();
        }, 3000)
      }
    };

    function sendRotateData() {
      if (ctrl.rotate.data.length > 0) {

        var sendToServer = {
          data: ctrl.rotate.data
        };
        ctrl.rotate.data = [];

        LibraryService.imageRotate(sendToServer)
          .error(function (err) {
            $log.error(err)
          })
      }
    }

    var unregisterUploadStartListener = $scope.$on(EVENTS.AFTER_START_UPLOAD_FILE, function () {
      ctrl.video = {file: {}};
      ctrl.image = {file: {}};
      ctrl.imageFileList = [];
      ctrl.filePrice = null;
      ctrl.clearFileListImage();
      ctrl.clearFileListVideo();
    });

    var unregisterUploadCompleteListener = $rootScope.$on(EVENTS.ON_COMPLETE_UPLOAD_FILE, function (event, eventData) {

      if (eventData.type == FileTypeService.FILE_TYPE.VIDEO) {

        LibraryService.postVideo(eventData).success(function (createdVideo) {
          if (ctrl.mediaTypeFilter.selected.indexOf(MEDIA_TYPE.VIDEO) >= 0
            && ctrl.usedFilter.selected != MEDIA_FILE_USED.USED) {

            ctrl.media.unshift({
              prepend: true,
              image: null,
              type: 1,
              createdAt: createdVideo.createdAt,
              video: createdVideo
            })
          }
          $rootScope.$broadcast('TicksTransaction', {});

        }).error(function (err) {
          $log.error(err);
        })

      } else if (eventData.type == FileTypeService.FILE_TYPE.IMAGE) {

        LibraryService.postImage(eventData).success(function (createdImage) {
          if (ctrl.mediaTypeFilter.selected.indexOf(MEDIA_TYPE.IMAGE) >= 0
            && ctrl.usedFilter.selected != MEDIA_FILE_USED.USED) {

            ctrl.media.unshift({
              type: 0,
              createdAt: createdImage.createdAt,
              image: createdImage
            })
          }
          $rootScope.$broadcast('TicksTransaction', {});

        }).error(function (err) {
          $log.error(err);
        })

      } else {
        new jGrowlNotify().error($translate.instant('ERROR_CANNOT_PROCESS_FILE', {file: eventData.file.name}));
      }
    });

    var refreshVideoInterval = $interval(function () {
      lodash.forEach(ctrl.media, function (item) {
        if (item.type == 1) {
          if (item.video.transcodingStatus == 0) {
            LibraryService.getVideo(item.video.id)
              .success(function (video) {
                $timeout(function () {
                  item.video = video;
                  $timeout(ctrl.reload, 1000);
                })
              })
              .error(function (err) {
                $log.error(err);
              })
          }
        }
      })
    }, 30 * 1000);

    $scope.$watch('ctrl.video.file', function (newVal) {
      if (newVal && newVal.name) {
        ctrl.filePrice = null;
        LibraryService.calculateVideoPrice(ctrl.video.file.size)
          .success(function (data, status) {
            ctrl.filePrice = data.price;
          })
          .error(function (err) {
            $log.error(err);
          })
      }
    });

    $scope.$watchCollection('ctrl.imageFileList', function (newVal) {
      var size = 0;
      lodash.forEach(ctrl.imageFileList, function (imageFile) {
        size += imageFile.size;
      });
      if (newVal.length > 0) {
        ctrl.filePrice = null;
        LibraryService.calculateImagePrice(size)
          .success(function (data, status) {
            ctrl.filePrice = data.price;
          })
          .error(function (err) {
            $log.error(err);
          });
      }
    });

    $scope.$on('$destroy', function () {
      sendRotateData();

      unregisterUploadStartListener();
      unregisterUploadCompleteListener();
      $interval.cancel(refreshVideoInterval);
    });

    function checkAndStartUploadImage() {
      if ((UserService.getBalanceCurrent() + UserService.getBalanceFree()) >= ctrl.filePrice) {
        ctrl.startUploadImage();
      } else {
        showNotEnoughTicksToast();
      }
    }

    function checkAndStartUploadVideo() {
      if (( (UserService.getBalanceCurrent() + UserService.getBalanceFree())) >= ctrl.filePrice) {
        ctrl.startUploadVideo();
      } else {
        showNotEnoughTicksToast();
      }
    }

    function showNotEnoughTicksToast() {
      new jGrowlNotify().error($translate.instant('LIBRARY_ERROR_NOT_ENOUGH_TICKS'));
    }

    function deleteMedia(event, media) {

      event.stopPropagation();

      if (media.type == 0) {
        deleteImage(media.image)
      }
      if (media.type == 1) {
        deleteVideo(media.video);
      }
    }

    function deleteVideo(video) {
      ModalService.confirmationBuilder()
        .confirmationClickOutsideToClose()
        .confirmationTexts({
          title: $translate.instant('COMMONS_DELETE_VIDEO'),
          body: $translate.instant('DELETE_VIDEO_BODY'),
          cancel: $translate.instant('COMMONS_CANCEL'),
          confirm: $translate.instant('COMMONS_DELETE_VIDEO')
        })
        .confirmationShowModal()
        .then(function (answer) {
          LibraryService.deleteVideo(video.id)
            .success(function (data, status) {
              lodash.forEach(ctrl.media, function (media, index) {
                if (media.type == 1 && media.video.id == video.id) {
                  ctrl.media.splice(index, 1);
                  return false;
                }
              })
            })
            .error(function (err) {
              new jGrowlNotify().error($translate.instant('LIBRARY_DELETE_ERROR'));
            })
        })
    }

    function deleteImage(image) {
      ModalService.confirmationBuilder()
        .confirmationClickOutsideToClose()
        .confirmationTexts({
          title: $translate.instant('COMMONS_DELETE_IMAGE'),
          body: $translate.instant('DELETE_IMAGE_BODY'),
          cancel: $translate.instant('COMMONS_CANCEL'),
          confirm: $translate.instant('COMMONS_DELETE_IMAGE')
        })
        .confirmationShowModal()
        .then(function (answer) {
          LibraryService.deleteImage(image.id)
            .success(function (data, status) {
              lodash.forEach(ctrl.media, function (media, index) {
                if (media.type == 0 && media.image.id == image.id) {
                  ctrl.media.splice(index, 1);
                  return false;
                }
              })
            })
            .error(function (err) {
              new jGrowlNotify().error($translate.instant('LIBRARY_DELETE_ERROR'));
            })
        })
    }

    function refresh() {

      ctrl.refresh = refresh;

      var pageable = new Pageable()
        .addOrderFilter('createdAt', ctrl.createdAtFilter.selected)
        .addFilter('mediaTypes', ctrl.mediaTypeFilter.selected)
        .addFilter('used', ctrl.usedFilter.selected)
        .build();

      LibraryService.getMedia(pageable)
        .success(function (data, status) {
          if (data && data[0]) {
            data[0].prepend = true;
          }
          ctrl.media = data;
        })
        .error(function (err) {
          $log.error(err);
        })
    }
  }
})();
