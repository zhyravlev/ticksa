(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkMediaCard', tkMediaCard);

  /** @ngInject */
  function tkMediaCard(MEDIA_TYPE, VIDEO_TRANSCODING_STATUS) {

    return {
      controllerAs: 'ctrl',
      templateUrl: 'app/components/library/media.card.directive/media.card.directive.html',
      scope: {
        media: '=',
        deleteMedia: '=',
        rotateHandler: '='
      },
      link: function ($scope, elem) {
        $scope.MEDIA_TYPE = MEDIA_TYPE;
        $scope.VIDEO_TRANSCODING_STATUS = VIDEO_TRANSCODING_STATUS;

        /** Rotate image */
        $scope.rotate = function (media) {
          var em = elem.find('.rotate');
          var deg = em.data('rotate-deg');

          em.removeClass(function () {
            return 'deg' + deg; // .deg90
          });

          deg += 90;

          if (deg == 360) {
            em.data('rotate-deg', 0);
          } else {
            em.data('rotate-deg', deg);
          }

          em.addClass('deg' + deg); // .deg180

          $scope.rotateHandler({
            id: media.image.id,
            deg: (deg == 90) ? 270 :
              (deg == 270) ? 90 : deg
          });
        }
      }
    }
  }
})();
