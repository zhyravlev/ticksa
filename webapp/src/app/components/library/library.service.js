(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('LibraryService', LibraryService);

  /** @ngInject */
  function LibraryService(API, $http) {
    var LibraryService = this;

    LibraryService.getMedia = function (filters) {
      return $http.get(API.URL + '/library/media', {params: filters});
    };

    LibraryService.calculateVideoPrice = function (size) {
      return $http.get(API.URL + '/library/calculate-video-price', {params: {size: size}});
    };

    LibraryService.calculateImagePrice = function (size) {
      return $http.get(API.URL + '/library/calculate-image-price', {params: {size: size}});
    };

    LibraryService.getVideo = function (videoId) {
      return $http.get(API.URL + '/library/video/' + videoId);
    };

    LibraryService.getImages = function () {
      return $http.get(API.URL + '/library/images');
    };

    LibraryService.postImages = function (imageList) {
      return $http.post(API.URL + '/library/images-multiple', imageList);
    };

    LibraryService.postImage = function (image) {
      return $http.post(API.URL + '/library/images', image);
    };

    LibraryService.deleteImage = function (imageId) {
      return $http.delete(API.URL + '/library/images/' + imageId);
    };

    LibraryService.getVideos = function () {
      return $http.get(API.URL + '/library/videos');
    };

    LibraryService.postVideo = function(video) {
      return $http.post(API.URL + '/library/videos', video);
    };

    LibraryService.deleteVideo = function(videoId) {
      return $http.delete(API.URL + '/library/videos/' + videoId);
    };

    LibraryService.imageRotate = function(data) {
      return $http.post(API.URL + '/library/images/rotate', data);
    }
  }
})();
