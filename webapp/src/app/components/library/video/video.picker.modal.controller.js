(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('VideoPickerModalController', VideoPickerModalController);

  /** @ngInject */
  function VideoPickerModalController(videos,
                                      $scope, $log, $rootScope, $translate, $mdDialog, $interval, lodash,
                                      LibraryService, FileTypeService, UserService, jGrowlNotify,
                                      SORTING_ORDER_FOR_ORDER_BY_REVERSE, VIDEO_TRANSCODING_STATUS) {

    $log = $log.getInstance('VideoPickerModalController');

    var ctrl = this;

    ctrl.videos = videos;
    ctrl.video = {file: {}};
    ctrl.videoForm = null;
    ctrl.finishUploadVideo = finishUploadVideo;

    ctrl.submit = submit;
    ctrl.close = close;
    ctrl.videoClick = videoClick;
    ctrl.checkAndStartUploadVideo = checkAndStartUploadVideo;
    ctrl.cancel = $mdDialog.cancel;
    ctrl.uploadStarted = false;

    ctrl.VIDEO_FILE_ACCEPT_EXTENSIONS = FileTypeService.AVAILABLE_VIDEO_FILE_EXTENSIONS_LIST;
    ctrl.VIDEO_TRANSCODING_STATUS = VIDEO_TRANSCODING_STATUS;

    // Filter to sort by date
    ctrl.filterCreatedAt = {
      values: [
        {
          key: $translate.instant('FILTER__SORTED__CREATED_DESC'),
          value: SORTING_ORDER_FOR_ORDER_BY_REVERSE.DESC
        },
        {
          key: $translate.instant('FILTER__SORTED__CREATED_ASC'),
          value: SORTING_ORDER_FOR_ORDER_BY_REVERSE.ASC
        }
      ],
      // By default newest first
      selected: SORTING_ORDER_FOR_ORDER_BY_REVERSE.DESC
    };

    var resetInterval = $interval(function () {
      lodash.forEach(ctrl.videos, function (video, index) {
        if (video.transcodingStatus == 0) {
          LibraryService.getVideo(video.id)
            .success(function (data) {
              ctrl.videos[index].transcodingStatus = data.transcodingStatus;
              ctrl.videos[index].durationInMillis = data.durationInMillis;
              ctrl.videos[index].videoFiles = data.videoFiles;
              ctrl.videos[index].updatedAt = data.updatedAt;
              ctrl.videos[index].thumbnailImageKey = data.thumbnailImageKey;
              $rootScope.$broadcast('tkMasonryResize', {})
            })
            .error(function (err) {
              $log.log(err);
            })
        }
      })
    }, 30 * 1000);

    $scope.$on('$destroy', function () {
      $interval.cancel(resetInterval);
    });

    function checkAndStartUploadVideo() {
      if ((UserService.getBalanceCurrent() + UserService.getBalanceFree()) >= ctrl.filePrice) {
        ctrl.disablePostVideo = true;
        ctrl.uploadStarted = true;
        ctrl.startUploadVideo();
      } else {
        showNotEnoughTicksToast();
      }
    }

    function showNotEnoughTicksToast() {
      new jGrowlNotify().error($translate.instant('LIBRARY_ERROR_NOT_ENOUGH_TICKS'));
    }

    function videoClick(video) {
      video.selected = true;
      if (ctrl.selectedVideo != null && ctrl.selectedVideo.id != video.id) {
        ctrl.selectedVideo.selected = false;
      }
      ctrl.selectedVideo = video;
    }

    function submit() {
      $mdDialog.hide({selectedVideo: ctrl.selectedVideo, change: ctrl.change});
    }

    function close() {
      return $mdDialog.cancel();
    }

    $scope.$watch('ctrl.video.file', function (newVal) {
      if (newVal && newVal.name) {
        ctrl.filePrice = null;
        LibraryService.calculateVideoPrice(ctrl.video.file.size)
          .success(function (data, status) {
            ctrl.filePrice = data.price
          })
          .error(function (err) {
            $log.log(err);
          })
      }
    });

    function finishUploadVideo() {
      ctrl.video.raw = null;
      ctrl.video.name = ctrl.video.file.name;
      LibraryService.postVideo(ctrl.video)
        .success(function (data, status) {
          ctrl.disablePostVideo = false;
          ctrl.clearFileListVideo();
          ctrl.video = {file: {}};
          ctrl.videos.unshift(data);
          ctrl.change = true;
          ctrl.uploadStarted = false;
          $rootScope.$broadcast('TicksTransaction', {})
        })
        .error(function (err) {
          $log.log(err);
        })
    }

  }
})();
