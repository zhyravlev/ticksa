(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('ImagePickerModalController', ImagePickerModalController);

  /** @ngInject */
  function ImagePickerModalController(images, multiple,
                                      $scope, $rootScope, $mdDialog, $translate, $log, lodash,
                                      LibraryService, FileTypeService, UserService, jGrowlNotify,
                                      SORTING_ORDER_FOR_ORDER_BY_REVERSE) {

    $log = $log.getInstance('ImagePickerModalController');

    var ctrl = this;

    ctrl.images = images;
    ctrl.multiple = multiple;

    ctrl.image = {file: {}};
    ctrl.imageFileList = [];

    ctrl.selectedImages = [];

    ctrl.finishUploadImage = finishUploadImage;
    ctrl.submit = submit;
    ctrl.close = close;
    ctrl.imageClick = imageClick;
    ctrl.finishUploadImages = finishUploadImages;
    ctrl.checkAndStartUploadImage = checkAndStartUploadImage;

    ctrl.IMAGE_FILE_ACCEPT_EXTENSIONS = FileTypeService.AVAILABLE_IMAGE_FILE_EXTENSIONS_LIST;

    // Filter to sort by date
    ctrl.filterCreatedAt = {
      values: [
        {
          key: $translate.instant('FILTER__SORTED__CREATED_DESC'),
          value: SORTING_ORDER_FOR_ORDER_BY_REVERSE.DESC
        },
        {
          key: $translate.instant('FILTER__SORTED__CREATED_ASC'),
          value: SORTING_ORDER_FOR_ORDER_BY_REVERSE.ASC
        }
      ],
      // By default newest first
      selected: SORTING_ORDER_FOR_ORDER_BY_REVERSE.DESC
    };

    function checkAndStartUploadImage() {
      if (( UserService.getBalanceCurrent() + UserService.getBalanceFree()) >= ctrl.filePrice) {
        ctrl.disablePostImage = true;
        ctrl.startUploadImage();
      } else {
        showNotEnoughTicksToast();
      }
    }

    function showNotEnoughTicksToast() {
      new jGrowlNotify().error($translate.instant('LIBRARY_ERROR_NOT_ENOUGH_TICKS'));
    }

    function imageClick(image) {
      if (image.selected) {
        imageDeselect(image)
      } else {
        imageSelect(image);
      }
    }

    function imageSelect(image) {
      image.selected = true;
      if (!multiple) {
        if (ctrl.selectedImage != null && ctrl.selectedImage.id != image.id) {
          ctrl.selectedImage.selected = false;
        }
        ctrl.selectedImage = image;
      }
    }

    function imageDeselect(deselectedImage) {
      deselectedImage.selected = false;
      if (!multiple) {
        ctrl.selectedImage = null;
      }
    }

    function submit() {
      if (!multiple) {
        $mdDialog.hide({images: ctrl.images, selectedImage: ctrl.selectedImage});
      } else {
        $mdDialog.hide({images: ctrl.images});
      }
    }

    function close() {
      return $mdDialog.cancel();
    }

    function finishUploadImage() {
      ctrl.image.raw = null;
      ctrl.image.name = ctrl.image.file.name;
      LibraryService.postImage(ctrl.image)
        .success(function (data, status) {
          ctrl.image = null;
          ctrl.clearFileListImage();
          ctrl.disablePostImage = false;
          $rootScope.$broadcast('TicksTransaction', {});
          imageSelect(data);
          ctrl.images.unshift(data);
        })
        .error(function (err) {
          $log.log(err);
        })
    }

    function finishUploadImages() {
      LibraryService.postImages(lodash.map(ctrl.imageFileList, function (imageFile) {
        return {name: imageFile.name, file: imageFile}
      }))
        .success(function (data, status) {
          ctrl.imageFileList = [];
          ctrl.clearFileListImage();
          ctrl.disablePostImage = false;
          $rootScope.$broadcast('TicksTransaction', {});
          lodash.forEach(data, function (image) {
            imageSelect(image);
            ctrl.images.unshift(image);
          });
        })
        .error(function (err) {
          $log.log(err);
        });
    }

    if (multiple) {
      $scope.$watchCollection('ctrl.imageFileList', function (newVal) {
        var size = 0;
        lodash.forEach(ctrl.imageFileList, function (imageFile) {
          size += imageFile.size;
        });
        if (newVal.length > 0) {
          ctrl.filePrice = null;
          LibraryService.calculateImagePrice(size)
            .success(function (data, status) {
              ctrl.filePrice = data.price;
            })
            .error(function (err) {
              $log.log(err);
            })
        }
      });
    } else {
      $scope.$watch('ctrl.image.file', function (newVal) {
        if (newVal && newVal.name) {
          ctrl.filePrice = null;
          LibraryService.calculateImagePrice(ctrl.image.file.size)
            .success(function (data, status) {
              ctrl.filePrice = data.price;
            })
            .error(function (err) {
              $log.log(err);
            })
        }
      });
    }


  }
})();
