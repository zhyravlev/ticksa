(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .run(runBlock);

  angular
    .module('infinite-scroll')
    .value('THROTTLE_MILLISECONDS', 200);

  /** @ngInject */
  function runBlock($rootScope, $state, $stateParams, $log,
                    NavigationService, AuthorizationService, HeaderTitleService, FrontendService, SearchService, HeaderFilterService,
                    CASH_IO_STATUS) {

    $log = $log.getInstance('index.run.js');

    watchForExternalLibrariesLoaded();
    initializeGlobalVariables();
    configureStateChangingHandling();

    function watchForExternalLibrariesLoaded() {
      AuthorizationService.startWatchingOnFB();
      AuthorizationService.startWatchingOnGApi();
    }

    function initializeGlobalVariables() {
      $rootScope.uploadFileInProgress = 0;
      $rootScope.sidebar = false;

      $rootScope.$state = $state;

      $rootScope.$stateParams = $stateParams;

      $rootScope.subscriptions = [];

      $rootScope.CASH_IO_STATUS = CASH_IO_STATUS;

      $rootScope.isGecko = FrontendService.browser.isFirefox || FrontendService.browser.isSafari;

      $rootScope.channelBackgroundImage = false;
    }

    function configureStateChangingHandling() {

      var eventStateChangeStart = $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
        if (fromState.name.length > 0) {
          NavigationService.setPreviousState(fromState.name, fromParams);
        }
        if (toState.name.length > 0 && toState.name !== 'base.login' && toState.name !== 'discovery') {
          NavigationService.setNextState(toState.name, toParams);
        }
      });

      var eventStateChangeSuccess = $rootScope.$on('$stateChangeSuccess', function (event, toState) {
        SearchService.enableSearchBarComponent(toState.name === 'main.contentList');
        HeaderFilterService.enableFilter(toState.name === 'main.contentList');
        HeaderTitleService.enableClickableChannelNameAsTitle(toState.name === 'main.contentView');
        if (toState.name != 'main.channelContentList') {
          HeaderTitleService.resetChanelFirstContentId();
        }
      });

      var eventStateChangeError = $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
        $log.error(event, toState, toParams, fromState, fromParams, error);
      });

      $rootScope.$on('$destroy', function () {
        eventStateChangeStart();
        eventStateChangeSuccess();
        eventStateChangeError();
      });
    }
  }
})();
