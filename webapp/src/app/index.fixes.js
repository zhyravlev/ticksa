(function () {
  'use strict';

  /**
   * Fix bug: The Safari browser throws a RangeError instead of returning null when it tries to stringify a Date object
   * with an invalid date value.
   * See 'Known issues' at https://docs.angularjs.org/api/ng/function/angular.toJson
   *
   * @type {any}
   * @private
   */
  var _DateToJSON = Date.prototype.toJSON;
  Date.prototype.toJSON = function() {
    try {
      return _DateToJSON.call(this);
    } catch(e) {
      if (e instanceof RangeError) {
        return null;
      }
      throw e;
    }
  };

})();
