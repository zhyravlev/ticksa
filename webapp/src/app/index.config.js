(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(config);

  /** @ngInject */
  function config($provide, $httpProvider, $sceDelegateProvider, $translateProvider, $mdThemingProvider,
                  $analyticsProvider, $mdDateLocaleProvider, $locationProvider, tkMobileDeviceProvider, jGrowlProvider,
                  CDN, Formats, tkMoment) {

    jGrowlProvider.defaults({
      position: 'bottom-left',
      closer: false
    });

    configMaterialDesign();
    configHttpInterceptors();
    configContextualEscaping();
    configTranslations();
    configEditor();
    configGoogleAnalytics();
    configApplicationUrlLocation();
    configMobileDevice();

    function configMaterialDesign() {
      $mdDateLocaleProvider.formatDate = function (date) {
        return date ? tkMoment(date).format(Formats.DATE_FORMAT) : null;
      };

      //angular material theme selection and configuration
      var customTheme = $mdThemingProvider.extendPalette('blue', {
        '500': '0fb7ff'
      });
      $mdThemingProvider.definePalette('customTheme', customTheme);

      $mdThemingProvider.theme('default')
        .primaryPalette('customTheme')
        .accentPalette('red');
    }

    function configHttpInterceptors() {
      $httpProvider.interceptors.push('RedirectToPaymentPageHttpInterceptor');
      $httpProvider.interceptors.push('AuthorizationHttpInterceptor');
    }

    function configContextualEscaping() {
      $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',

        // Allow loading from our assets domain.  Notice the difference between * and **.
        'https://s3-us-west-2.amazonaws.com**',
        'http://s3-us-west-2.amazonaws.com**',

        'https://da7pflu8zosoc.cloudfront.net**',
        'http://da7pflu8zosoc.cloudfront.net**',

        'https://dxdzm86win8ok.cloudfront.net**',
        'http://dxdzm86win8ok.cloudfront.net**',

        'http://d2a26exmn60ooi.cloudfront.net**',
        'https://d2a26exmn60ooi.cloudfront.net**',

        'http://d3u3f37j7l8o2d.cloudfront.net**',
        'https://d3u3f37j7l8o2d.cloudfront.net**',

        'http://d24igwv6mu24pz.cloudfront.net**',
        'https://d24igwv6mu24pz.cloudfront.net**',

        'http://dcdwas7wam6fa.cloudfront.net**',
        'https://dcdwas7wam6fa.cloudfront.net**',

        'http://d3k7vzalrkl263.cloudfront.net**',
        'https://d3k7vzalrkl263.cloudfront.net**',

        'http://d2qepbnwtabmdf.cloudfront.net**',
        'https://d2qepbnwtabmdf.cloudfront.net**'
      ]);
    }

    function configTranslations() {
      var DEFAULT_LANGUAGE = 'en';

      var setPreferredLanguage = function (lang, languages) {
        if (languages != null && angular.isArray(languages)) {
          for (var i = 0; i < languages.length - 1; i++) {
            if (lang.indexOf(languages[i]) === 0) {
              $translateProvider.preferredLanguage(languages[i]);
              return;
            }
          }
        }
        $translateProvider.preferredLanguage(DEFAULT_LANGUAGE);
      };

      //$translateProvider.useLoader('LanguageLoaderService', {}).fallbackLanguage('en');
      $translateProvider.useStaticFilesLoader({
        prefix: CDN.assets.cloudFront + CDN.assets.key + '/assets/languages/',
        suffix: '.json'
      }).fallbackLanguage(DEFAULT_LANGUAGE);

      // http://stackoverflow.com/questions/1043339
      var language = navigator.languages ? navigator.languages[0] : (navigator.language || navigator.userLanguage);
      if (language) {
        setPreferredLanguage(language, ['cz', 'nl', 'en', 'fr', 'de', 'hu', 'it', 'pl', 'ro', 'ru', 'sk', 'es']);
      }

      $translateProvider.useSanitizeValueStrategy('sanitize');
      $translateProvider.useCookieStorage();
    }

    function configEditor() {
      $provide.decorator('taOptions', ['$delegate', function (taOptions) {
        // $delegate is the taOptions we are decorating
        // here we override the default toolbars and classes specified in taOptions.
        taOptions.toolbar = [
          ['bold', 'italics', 'underline', 'h1', 'h2', 'h3', 'justifyLeft', 'justifyCenter', 'justifyRight'],
          //['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote'],
          //['bold', 'italics', 'underline', 'ul', 'ol', 'redo', 'undo', 'clear'],
          //['justifyLeft','justifyCenter','justifyRight'],
          //['html', 'insertImage', 'insertLink']
        ];
        taOptions.classes = {
          focussed: '',
          toolbar: '',
          toolbarGroup: '',
          toolbarButton: 'md-button',
          toolbarButtonActive: 'md-primary md-raised',
          disabled: 'disabled md-raised',
          textEditor: 'text',
          htmlEditor: 'html-editor'
        };
        return taOptions; // whatever you return will be the taOptions
      }]);
    }

    function configGoogleAnalytics() {
      /* Records pages that don't use $state or $route */
      $analyticsProvider.firstPageview(true);
      /* Records full path */
      $analyticsProvider.withAutoBase(true);
    }

    /*
     * According to APP-757 we use Angular's html5mode + hashbang (#!) to let FB crawler works.
     */
    function configApplicationUrlLocation() {
      $locationProvider
        .html5Mode(true)
        .hashPrefix('!')
      ;
    }

    function configMobileDevice(){
      tkMobileDeviceProvider.saveAsClass(true);
      tkMobileDeviceProvider.detectMobileDevice();
    }
  }
})();
