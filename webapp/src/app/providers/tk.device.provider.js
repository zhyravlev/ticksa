(function () {
  'use strict';

  /*eslint angular/document-service: 0*/

  angular
    .module('ticksaWebapp')
    .provider('tkMobileDevice', tkMobileDeviceProvider);

  /** @ngInject */
  function tkMobileDeviceProvider() {

    var save = false;

    /**
     * Detected Apply mobile device. iPad, iPhone or iPod.
     */
    var isApplyMobileDevice = false;
    var detectApplyMobileDevice = function () {
      var devices = ['iPad', 'iPhone', 'iPod'];
      if (!!navigator.platform) {
        while (devices.length) {
          if (navigator.platform === devices.pop()) {
            if (save) {
              angular.element(window.document)[0].querySelector('html').className += ' ios';
            }
            return true;
          }
        }
      }

      if (save) {
        angular.element(window.document)[0].querySelector('html').className += ' no-ios';
      }
      return false;
    };

    this.saveAsClass = function (value) {
      save = value;
    };

    this.detectMobileDevice = function () {
      isApplyMobileDevice = detectApplyMobileDevice();
    };

    this.$get = function () {
      return {
        isApplyMobileDevice: isApplyMobileDevice
      }
    }
  }
})();