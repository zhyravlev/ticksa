(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .provider('tkDialog', tkDialogProvider);

  /** @ngInject */
  function tkDialogProvider() {

    var animateClassName = 'shake';

    this.setAnimateClassName = function (value) {
      animateClassName = value;
    };

    this.$get = function () {
      return {
        animateClassName: animateClassName
      }
    }
  }
})();