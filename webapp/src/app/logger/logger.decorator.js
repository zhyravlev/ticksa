(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .config(LoggerDecorator);

  /**
   * It can be that to let angular-mocks works we should somehow refactor current decorator:
   * http://solutionoptimist.com/2013/10/07/enhance-angularjs-logging-using-decorators/
   * (take a look at phrase: 'only needed to support angular-mocks expectations')
   */

  /** @ngInject */
  function LoggerDecorator($provide, $logProvider, LoggerBuilderServiceProvider) {

    // enable 'debug' level by default. however it depends on configuration.
    $logProvider.debugEnabled(true);

    $provide.decorator('$log', function ($delegate) {
      var angularLog = (function (aLog) {
        return {
          log: aLog.log,
          info: aLog.info,
          warn: aLog.warn,
          debug: aLog.debug,
          error: aLog.error
        };
      })($delegate);

      var LoggerBuilder = LoggerBuilderServiceProvider.$get();

      // 'toServer' and 'noServer' defined to let programmer explicitly choose is it necessary to post log to server or not
      var enhancedLogger = new LoggerBuilder(angularLog).withTimeStamp().withPrefix('default').build();
      enhancedLogger.toServer = new LoggerBuilder(angularLog).withTimeStamp().withPrefix('default').postToServer().build();
      enhancedLogger.noServer = new LoggerBuilder(angularLog).withTimeStamp().withPrefix('default').dontPostToServer().build();
      enhancedLogger.getInstance = function (prefix) {
        var instanceLogger = new LoggerBuilder(angularLog).withTimeStamp().withPrefix(prefix).build();
        instanceLogger.toServer = new LoggerBuilder(angularLog).withTimeStamp().withPrefix(prefix).postToServer().build();
        instanceLogger.noServer = new LoggerBuilder(angularLog).withTimeStamp().withPrefix(prefix).dontPostToServer().build();
        return instanceLogger;
      };

      return enhancedLogger;
    });
  }
})();
