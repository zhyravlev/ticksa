(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('LoggerBuilderService', LoggerBuilderService);

  /** @ngInject */
  function LoggerBuilderService(lodash, Formatter, tkMoment, Formats,
                                CircularDependenciesResolverService, LOG_CONFIGURATION) {

    var LoggerBuilder = function (angularLog) {
      this._serverAcceptedLogs = _acceptedLevelsFromConfiguration(LOG_CONFIGURATION, 'toServer');
      this._consoleAcceptedLogs = _acceptedLevelsFromConfiguration(LOG_CONFIGURATION, 'toConsole');
      this._angularLog = angularLog;
      this._withTimeStamp = false;
      this._prefix = null;
    };

    LoggerBuilder.prototype.withTimeStamp = function () {
      this._withTimeStamp = true;
      return this;
    };

    LoggerBuilder.prototype.withPrefix = function (prefix) {
      this._prefix = prefix;
      return this;
    };

    LoggerBuilder.prototype.dontPostToServer = function () {
      this._serverAcceptedLogs = _fixedAcceptedLevels([]);
      return this;
    };

    LoggerBuilder.prototype.postToServer = function () {
      this._serverAcceptedLogs = _fixedAcceptedLevels(['debug', 'info', 'warn', 'error']);
      return this;
    };

    LoggerBuilder.prototype.build = function () {
      return {
        log: this._logFunction('info', this._angularLog.info),
        info: this._logFunction('info', this._angularLog.info),
        debug: this._logFunction('debug', this._angularLog.debug),
        warn: this._logFunction('warn', this._angularLog.warn),
        error: this._logFunction('error', this._angularLog.error)
      };
    };

    LoggerBuilder.prototype._logFunction = function (type, angularFunc) {

      // Full configuration is unlinked from Builder object.
      // It is very important!
      // It means that Builder object can be used multiple times without any worries about side effects.
      var _builder = this;
      var prefix = _builder._prefix;
      var withTimeStamp = _builder._withTimeStamp;
      var serverAcceptedLogs = _builder._serverAcceptedLogs;
      var consoleAcceptedLogs = _builder._consoleAcceptedLogs;

      return function () {
        var acceptedToConsole = consoleAcceptedLogs();
        var acceptedToServer = serverAcceptedLogs();
        if (lodash.isEmpty(acceptedToConsole) && lodash.isEmpty(acceptedToServer)) {
          return;
        }

        var args = Array.prototype.slice.call(arguments);
        var message = _parseLogArguments.apply(null, args);

        if (prefix != null) {
          message = Formatter.format('(%s) %s', prefix, message);
        }
        if (withTimeStamp) {
          message = Formatter.format('(%s) %s', tkMoment().format(Formats.DATE_TIME_FORMAT_WITH_MILLIS), message);
        }

        if (acceptedToConsole.indexOf(type) > -1) {
          angularFunc.call(null, message);
        }

        if (acceptedToServer.indexOf(type) > -1) {
          var LoggerCollectorService = CircularDependenciesResolverService.getLoggerCollectorService();
          LoggerCollectorService.collectLogEntry(type, message);
        }
      }
    };

    return LoggerBuilder;


    /**
     * This method
     *  - parses arguments passed to logging function.
     *  - crops null values from the end of passed arguments list.
     *  - formats message to let it be logged.
     *
     * @return string
     *   - Formatted message string ready to been logged
     */
    function _parseLogArguments() {
      var args = Array.prototype.slice.call(arguments);

      while (args[args.length - 1] === null) {
        args.pop();
      }

      return Formatter.format.apply(null, args);
    }

    function _fixedAcceptedLevels(acceptedLevels) {
      return function () {
        return acceptedLevels;
      };
    }

    function _acceptedLevelsFromConfiguration(logConfiguration, destinationName) {
      return function () {
        return logConfiguration[destinationName];
      }
    }
  }
})();
