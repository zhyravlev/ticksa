(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('LoggerCollectorService', LoggerCollectorService);

  /** @ngInject */
  function LoggerCollectorService($rootScope, $interval, LoggerHttpService, AuthorizationTokenService) {

    var _logs = [];
    var sendToServer = $interval(flush, 5000);

    $rootScope.$on('$destroy', function () {
      flush();
      $interval.cancel(sendToServer);
    });

    return {
      collectLogEntry: collectLogEntry
    };

    function collectLogEntry(type, message) {
      _logs.push({
        type: type,
        message: message,

        // encrypted tickserId stored because logs can be produced while user switches through accounts
        encId: AuthorizationTokenService.getEncryptedTickserId()
      });

      if (_logs.length > 20) {
        flush();
      }
    }

    function flush() {
      if (_logs.length > 0) {
        LoggerHttpService.sendLogsToServer(_logs);
        _logs = [];
      }
    }
  }
})();
