/* global malarkey:false, toastr:false, moment:false */
(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .constant('LOGOTYPE_LINK', 'http://join.ticksa.com/')
    .constant('tkMoment', moment)
    .constant('tkModernizr', Modernizr)
    .constant('ENV', config.env)
    .constant('API', config.api)
    .constant('CDN', config.cdn)
    .constant('LOG_CONFIGURATION', config.log)
    .constant('FACEBOOK_APP_ID', config.facebookAppId)
    .constant('CHANNEL_UNKNOWN', -1)
    .constant('RECAPTCHA', {
      publicKey: '6Ld3LA8TAAAAADv7zX1FOX96cOZNjeeZj7KI8_Wx'
    })
    .constant('Formats', {
      DATE_FORMAT: 'YYYY-MM-DD',
      DATE_TIME_FORMAT: 'YYYY-MM-DD HH:mm:ss',
      DATE_TIME_FORMAT_WITH_MILLIS: 'YYYY-MM-DD HH:mm:ss.SSS'
    })
    .constant('CONTENT_TYPES', {
      TEXT: 0,
      IMAGE: 1,
      LINK: 2,
      VIDEO: 3
    })
    .constant('SOCIAL_TYPES', {
      GOOGLE: 0,
      FACEBOOK: 1,
      TWITTER: 2
    })
    .constant('NOTIFICATION_TYPES', {
      MESSAGE: 0,
      CHANNEL_EDIT_INVITE: 1,
      CHANNEL_SUBSCRIBE: 2,
      GRAND_FREE_TICKS: 3,
      GRANT_COMMISSION_TICKS: 4,
      CHANNEL_SHARED_REVENUE_CHANGED: 5
    })
    .constant('USER_STATUS', {
      CONFIRMED: 1,
      ACTIVE: 3
    })
    .constant('CHANNEL_TYPES', {
      PUBLIC: 0,
      PRIVATE: 1
    })
    .constant('CASH_IO_STATUS', {
      PENDING: 0,
      STARTED: 1,
      FAILED: 2,
      COMPLETED: 3,
      ON_HOLD: 4,
      FINAL: 5,
      TRANSFERRED: 6
    })
    .constant('TICKSER_PREFERENCES', {
      FIRST_TIME_MESSAGE_SHOWED: "firstTimeMessageShowed",
      SKIP_ADD_CONTENT_NOTIFICATION: 'skipAddContentNotification',
      SKIP_ADD_FEED_NOTIFICATION: 'skipAddFeedNotification',
      SKIP_LIKE_NOTIFICATION: 'skipLikeNotification',
      SKIP_DISLIKE_NOTIFICATION: 'skipDislikeNotification',
      SKIP_BUY_CONTENT_NOTIFICATION: 'skipBuyContentNotification',
      TOS_AGREED: 'TOSAgreed',
      TOS_AGREED_DATE: 'TOSAgreedDate'
    })
    .constant('EVENTS', {
      AFTER_START_UPLOAD_FILE: 'AFTER_START_UPLOAD_FILE',
      ON_COMPLETE_UPLOAD_FILE: 'ON_COMPLETE_UPLOAD_FILE',
      PROGRESS_BAR_PROCESS: 'PROGRESS_BAR_PROCESS',
      DISCOVER_CONTENT_REMOVE: 'DISCOVER_CONTENT_REMOVE',
      SUBSCRIPTIONS_LOADED: 'SUBSCRIPTIONS_LOADED',
      CHANGE_CHANNEL_BACKGROUND: 'CHANGE_CHANNEL_BACKGROUND',
      ANIMATE_DIALOG: 'ANIMATE_DIALOG'
})
    .constant('TICKSER_CHANNEL_OWNER_TYPE', {
      CREATOR: 0,
      MANAGER: 1,
      PARTICIPANT: 2
    })
    .constant('PRICES', {
      ADD_FEED: 1,
      LIKE: 1,
      DISLIKE: 1,
      ADD_CONTENT: 1
    })
    .constant('MEDIA_FILE_USED', {
      ALL: null,
      UNUSED: 0,
      USED: 1
    })
    .constant('MEDIA_TYPE', {
      IMAGE: 0,
      VIDEO: 1
    })
    .constant('VIDEO_TRANSCODING_STATUS', {
      START: 0,
      COMPLETE: 1,
      ERROR: 2
    })
    .constant('SORTING_ORDER_FOR_FILTER', {
      ASC: 'asc',
      DESC: 'desc'
    })
    .constant('SORTING_ORDER_FOR_ORDER_BY_REVERSE', {
      ASC: false,
      DESC: true
    })
    .constant('CONTENT_CONDITION_TYPE', {
      PURCHASE: 0
    })
    .constant('UI_LANGUAGES', [
      {
        id: 'en',
        name: 'English'
      },
      {
        id: 'cs',
        name: 'Český'
      },
      {
        id: 'de',
        name: 'Deutsch'
      },
      {
        id: 'es',
        name: 'Español'
      },
      {
        id: 'fr',
        name: 'Français'
      },
      {
        id: 'hu',
        name: 'Magyar'
      },
      {
        id: 'it',
        name: 'Italiano'
      },
      {
        id: 'nl',
        name: 'Nederlands (Olandese)'
      },
      {
        id: 'pl',
        name: 'Polski'
      },
      {
        id: 'ro',
        name: 'Românesc'
      },
      {
        id: 'ru',
        name: 'Русский'
      },
      {
        id: 'sk',
        name: 'Slovenský'
      }
    ])
})();

