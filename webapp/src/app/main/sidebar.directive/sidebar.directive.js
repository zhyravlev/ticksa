(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkSidebar', tkSidebar);

  /** @ngInject */
  function tkSidebar() {

    return {
      templateUrl: 'app/main/sidebar.directive/sidebar.directive.html',
      controllerAs: 'sidebar',
      bindToController: true,
      controller: function ($log, $state, $document, $rootScope, $mdDialog, $timeout, $window, $scope, jGrowlNotify,
                            $translate, ChannelService, PlaylistHttpService, EVENTS, LOGOTYPE_LINK) {

        $log = $log.getInstance('tkSidebar');

        var ctrl = this;
        init();

        ctrl.LOGOTYPE_LINK = LOGOTYPE_LINK;

        ctrl.openDiscover = openDiscover;
        ctrl.openSubscriptions = openSubscriptions;
        ctrl.openChannels = openChannels;
        ctrl.openPlaylist = openPlaylist;
        ctrl.goToPlaylist = goToPlaylist;
        ctrl.openHistory = openHistory;
        ctrl.openMedia = openMedia;
        ctrl.openSubscription = openSubscription;
        ctrl.hideExtra = hideExtra;
        ctrl.openChannel = openChannel;
        ctrl.openChannelSettings = openChannelSettings;
        ctrl.openPlaylistSettings = openPlaylistSettings;
        ctrl.manageChannelMembers = manageChannelMembers;
        ctrl.openNewChannel = openNewChannel;
        ctrl.openNewPlaylist = openNewPlaylist;
        ctrl.toggleSidebar = toggleSidebar;

        ctrl.playlists = [];

        function init() {
          ctrl.$state = $state;
          refreshSubscriptions();
          refreshUserChannels();
          refreshUserPlaylists();
        }

        function toggleSidebar() {
          $rootScope.sidebar = !$rootScope.sidebar;
        }

        $scope.$on('UserChannelsChange', function (event, args) {
          refreshUserChannels();
        });

        $scope.$on('UserPlaylistsChange', function (event, args) {
          refreshUserPlaylists();
        });

        $scope.$on('$stateChangeSuccess', function (e, toState, toParams, fromState, fromParams) {
          ctrl.showSubscriptionsNarrow = false;
          ctrl.showUserChannelsNarrow = false;
          ctrl.showPlaylistNarrow = false;
          ctrl.showSubscriptionsWide = false;
          ctrl.showUserChannelsWide = false;
          ctrl.showPlaylistWide = false;
        });


        $scope.$on('SubscriptionChange', subscriptionChange);

        function subscriptionChange(event, args) {
          refreshSubscriptions();
          if (args.type == 0) {
            new jGrowlNotify().success($translate.instant('CHANNEL_UNSUBSCRIBED_EVENT') + " " + args.channel.name + '"');
          } else {
            new jGrowlNotify().success($translate.instant('CHANNEL_SUBSCRIBED_EVENT') + " " + args.channel.name + '"');
          }
        }

        function openNewChannel() {
          hideExtra();
          $state.go('main.channels.add');
        }

        function openNewPlaylist() {
          hideExtra();
          $state.go('main.playlist.add');
        }

        function openChannels() {
          if ($window.innerWidth < 600) {
            ctrl.showSubscriptionsNarrow = false;
            ctrl.showPlaylistNarrow = false;
            ctrl.showUserChannelsNarrow = !ctrl.showUserChannelsNarrow;
          } else {
            ctrl.showSubscriptionsWide = false;
            ctrl.showPlaylistWide = false;
            ctrl.showUserChannelsWide = !ctrl.showUserChannelsWide;
            if (ctrl.showUserChannelsWide) {
              $document.on('click', clickHandler);
            }
          }
        }

        function openPlaylist() {
          if ($window.innerWidth < 600) {
            ctrl.showSubscriptionsNarrow = false;
            ctrl.showUserChannelsNarrow = false;
            ctrl.showPlaylistNarrow = !ctrl.showPlaylistNarrow;
          } else {
            ctrl.showSubscriptionsWide = false;
            ctrl.showUserChannelsWide = false;
            ctrl.showPlaylistWide = !ctrl.showPlaylistWide;
            if (ctrl.showPlaylistWide) {
              $document.on('click', clickHandler);
            }
          }
        }

        function goToPlaylist(id) {
          hideExtra();
          $state.go('main.playlist.view', {playlistId: id});
        }

        function hideExtra() {
          ctrl.showSubscriptionsWide = false;
          ctrl.showUserChannelsWide = false;
          ctrl.showPlaylistWide = false;
          ctrl.showSubscriptionsNarrow = false;
          ctrl.showUserChannelsNarrow = false;
          ctrl.showPlaylistNarrow = false;
          $document.off('click', clickHandler);
        }

        function clickHandler(e) {
          var target = e.target;
          while (target.parentNode != null) {
            if (target.tagName.toLowerCase().indexOf('sidebar') != -1) {
              return;
            }
            if (target.tagName.toLowerCase().indexOf('body') != -1) {
              $timeout(hideExtra);
              return;
            }
            target = target.parentNode;
          }
        }

        function openHistory() {
          hideExtra();
          $state.go('main.history');
        }

        function openMedia() {
          hideExtra();
          $state.go('main.library.media');
        }

        function manageChannelMembers(channel, $event) {
          $mdDialog.show({
            parent: $document[0].body,
            targetEvent: $event,
            templateUrl: 'app/components/channel/channel.members/channel.members.dialog.html',
            controller: 'ChannelMembersDialogController',
            controllerAs: 'ctrl',
            locals: {
              channel: channel
            },
            clickOutsideToClose: true,
            escapeToClose: true
          });
        }

        function openChannelSettings(channelId) {
          hideExtra();
          $state.go('main.channels.edit', {channelId: channelId});
        }

        function openPlaylistSettings(playlistId) {
          hideExtra();
          $state.go('main.playlist.edit', {playlistId: playlistId});
        }

        function openSubscriptions() {
          if ($window.innerWidth < 600) {
            ctrl.showUserChannelsNarrow = false;
            ctrl.showPlaylistNarrow = false;
            ctrl.showSubscriptionsNarrow = !ctrl.showSubscriptionsNarrow;
          } else {
            ctrl.showUserChannelsWide = false;
            ctrl.showPlaylistWide = false;
            ctrl.showSubscriptionsWide = !ctrl.showSubscriptionsWide;
            if (ctrl.showSubscriptionsWide) {
              $document.on('click', clickHandler);
            }
          }
        }

        function openSubscription(subscriptionId) {
          hideExtra();
          ctrl.showSubscriptionsWide = false;
          $state.go('main.channelContentList', {channelId: subscriptionId});
        }

        function openChannel(channelId) {
          hideExtra();
          ctrl.showUserChannelsWide = false;
          $state.go('main.channelContentEdit', {channelId: channelId});
        }

        function openDiscover() {
          hideExtra();
          $state.go('main.contentList');
        }

        function refreshUserChannels() {
          ChannelService.getUserChannels()
            .success(function (data, status) {
              ctrl.channels = data;
            })
            .error(function (err) {
              $log.log(err);
            });
        }

        function refreshUserPlaylists() {
          PlaylistHttpService.getUserPlaylists()
            .then(function (res) {
              ctrl.playlists = res.data;
            })
            .catch(function (err) {
              $log.error(err);
            });
        }

        function refreshSubscriptions() {
          ChannelService.getSubscribedChannels()
            .success(function (data, status) {
              ctrl.subscriptions = data;
              $rootScope.subscriptions = data;
              $rootScope.$broadcast(EVENTS.SUBSCRIPTIONS_LOADED);
            })
            .error(function (err) {
              $log.log(err);
            });
        }
      }
    }
  }

})();
