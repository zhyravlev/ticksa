(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController(AccountService, UserService, TICKSER_PREFERENCES) {

    if (!UserService.getPreference(TICKSER_PREFERENCES.TOS_AGREED)) {
      UserService.setPreference(TICKSER_PREFERENCES.TOS_AGREED, true);
      UserService.setPreference(TICKSER_PREFERENCES.TOS_AGREED_DATE, new Date());
      AccountService.updatePreferences(UserService.getPreferences());
    }
  }
})();
