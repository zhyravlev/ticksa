(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkHeader', tkHeader);

  /** @ngInject */
  function tkHeader() {

    return {
      replace: true,
      restrict: 'E',
      templateUrl: 'app/main/header.directive/header.directive.html',
      controllerAs: 'ctrl',
      controller: function ($rootScope, $mdDialog, $scope, $translate, $mdMenu, $mdSidenav, $state, $interval,
                            lodash, $log, $document,
                            AccountService, AuthorizationService, HeaderTitleService, SearchService, DialogService,
                            NotificationsHttpService, NotificationsService, CommonsUIService, EncodingService, jGrowlNotify,
                            NavigationService, UserService, HeaderFilterService, ENV, LOGOTYPE_LINK, FrontendService, TkDialogFactory) {

        $log = $log.getInstance('tkHeader');

        var ctrl = this;
        var isTouch = FrontendService.device.isTouch;

        ctrl.LOGOTYPE_LINK = LOGOTYPE_LINK;

        ctrl.searchString = '';
        ctrl.searchIsShow = false;
        ctrl.filterIsShow = false;
        ctrl.filterIn = false;
        ctrl.filterUsed = false;
        ctrl.notificationsCount = 0;
        ctrl.showChannelSubscribeTools = false;
        ctrl.showChannelShareTools = false;
        ctrl.showPlaylistShareTools = false;
        ctrl.showSearchForm = false;
        ctrl.searchFocus = false;
        ctrl.searchIsUsed = false;
        ctrl.header = {
          title: '',
          channel: {
            id: null,
            name: null,
            canClick: false,
            isSubscribed: false,
            firstContentId: null
          },
          playlist: {
            id: null
          }
        };
        ctrl.balance = {
          current: UserService.getBalanceCurrent(),
          earnedTicks: UserService.getBalanceEarned(),
          freeTicks: UserService.getBalanceFree(),
          total: UserService.getBalanceTotal()
        };
        ctrl.profileImageUrl = UserService.getProfileImageUrl();

        ctrl.openProfile = openProfile;
        ctrl.postFeedback = postFeedback;
        ctrl.logout = logout;
        ctrl.changePassword = changePassword;
        ctrl.openBuyTicks = openBuyTicks;
        ctrl.redeemVoucher = redeemVoucher;
        ctrl.conditions = conditions;
        ctrl.toggleSidebar = toggleSidebar;
        ctrl.toggleFilter = toggleFilter;
        ctrl.openNotificationBar = openNotificationBar;
        ctrl.subscribeToChannel = subscribeToChannel;
        ctrl.unsubscribeToChannel = unsubscribeToChannel;
        ctrl.shareChannel = shareChannel;
        ctrl.sharePlaylist = sharePlaylist;
        ctrl.searchSetFocus = searchSetFocus;
        ctrl.search = search;
        ctrl.searchCancel = searchCancel;

        init();
        function init() {
          updateBalance();

          var makeChannelTools = function (state) {
            var _state = state || $state.current.name;
            ctrl.showChannelSubscribeTools = _state === 'main.channelContentList' || _state === 'main.contentView';
            ctrl.showChannelShareTools = _state === 'main.channelContentList';
            ctrl.showPlaylistShareTools = _state === 'main.playlist.view';
          };

          makeChannelTools();

          var unregisterSidebarWatch = $rootScope.$watch('sidebar', function (value) {
            if (isTouch && value) {
              $document[0].ontouchmove = function (event) {
                event.preventDefault()
              }
            } else {
              $document[0].ontouchmove = null;
            }
          });

          var unregisterStateChangeObserver = $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            makeChannelTools(toState.name);
            if ($rootScope.sidebar) {
              $rootScope.sidebar = false;
              $document.off('click', clickHandler)
            }
          });

          var unregisterNotificationsServiceDataObserver = NotificationsService.registerDataObserver(function (data) {
            ctrl.notificationsCount = data.notificationsCount;
          });

          NotificationsService.refreshNotificationsCount();
          NotificationsService.startNotificationsCountRefreshing();

          var unregisterHeaderTitleObserver = HeaderTitleService.registerObserver(function (data) {
            ctrl.header.title = data.title;
            ctrl.header.titleTranslationKey = data.titleTranslationKey;
            ctrl.header.channel.id = data.channel.id;
            ctrl.header.channel.name = data.channel.name;
            ctrl.header.channel.canClick = data.channel.canClick;
            ctrl.header.channel.isSubscribed = data.channel.isSubscribed || false;
            ctrl.header.channel.firstContentId = data.channel.firstContentId || null;
            ctrl.header.playlist.id = data.playlist.id;
          });

          var unregisterAccountInfoChangeObserver = $scope.$on('AccountInfoChange', function () {
            refreshAccountInfo();
          });
          var unregisterTicksTransactionObserver = $scope.$on('TicksTransaction', function () {
            refreshAccountInfo().then(function () {
              updateBalance();
            });
          });
          var unregisterSearchObserver = SearchService.registerObserver(function (data) {
            ctrl.searchIsShow = data.enableSearchBar;
          });

          var unregisterFilterObserver = HeaderFilterService.registerObserver(function (data) {
            ctrl.filterIsShow = data.enable;
            ctrl.filterIn = data.in;
            ctrl.filterUsed = data.used;
          });

          function updateBalance() {
            ctrl.balance = {
              current: UserService.getBalanceCurrent(),
              earnedTicks: UserService.getBalanceEarned(),
              freeTicks: UserService.getBalanceFree(),
              total: UserService.getBalanceTotal()
            };
          }

          $scope.$on('$destroy', function () {
            unregisterSidebarWatch();
            NotificationsService.stopNotificationsCountRefreshing();
            unregisterNotificationsServiceDataObserver();
            unregisterHeaderTitleObserver();
            unregisterAccountInfoChangeObserver();
            unregisterTicksTransactionObserver();
            unregisterStateChangeObserver();
            unregisterSearchObserver();
            unregisterFilterObserver();
          });
        }

        function search(event) {
          if ((event.type === 'keyup' && event.keyCode === 13) || (event.type === 'click')) {

            if (ctrl.searchString === '') {
              searchCancel();
              return true;
            }

            SearchService.setSearchString(ctrl.searchString);
            ctrl.searchIsUsed = true;

            if (ctrl.showSearchForm) {
              ctrl.showSearchForm = false;
              ctrl.searchFocus = false;
            }
            return true;
          }

          if (event.type === 'keyup' && event.keyCode == 27) {
            searchCancel();
            return true;
          }

          return true;
        }

        function searchCancel() {
          ctrl.searchString = '';
          ctrl.searchIsUsed = false;
          SearchService.setSearchString(ctrl.searchString);

          if (ctrl.showSearchForm) {
            ctrl.showSearchForm = false;
            ctrl.searchFocus = false;
          }
        }

        function searchSetFocus() {
          ctrl.showSearchForm = true;
          ctrl.searchFocus = true;
        }

        function toggleFilter() {
          HeaderFilterService.toggleFilter();
        }

        function subscribeToChannel(channel) {
          CommonsUIService.subscribeToChannel(channel)
            .then(function () {
              ctrl.header.channel.isSubscribed = true;
            });
        }

        function unsubscribeToChannel(channel) {
          CommonsUIService.unsubscribeToChannel(channel)
            .then(function () {
              ctrl.header.channel.isSubscribed = false;
            });
        }

        function openNotificationBar() {
          NotificationsService.openNotificationsBar();
        }

        function conditions() {
          $mdDialog.show({
            clickOutsideToClose: true,
            escapeToClose: true,
            templateUrl: 'app/components/account/conditions/conditions.dialog.html',
            controller: 'ConditionsDialogController',
            controllerAs: 'ctrl'
          });
        }

        function toggleSidebar() {
          $rootScope.sidebar = !$rootScope.sidebar;
          $document.on('click', clickHandler);
        }

        function clickHandler(event) {
          var target = event.target;
          while (target.parentNode != null) {
            if (target.tagName.toLowerCase().indexOf('sidebar') != -1) {
              return;
            }
            if (target.tagName.toLowerCase().indexOf('header') != -1) {
              return;
            }
            if (target.tagName.toLowerCase().indexOf('body') != -1) {

              $scope.$apply(function () {
                $rootScope.sidebar = false;
                $document.off('click', clickHandler);
              });

              return;
            }
            target = target.parentNode;
          }
        }

        function openProfile() {
          $state.go('main.account.profile');
        }

        function postFeedback() {
          var tkDialog = new TkDialogFactory();
          tkDialog.controller('FeedbackDialogController');
          tkDialog.templateUrl('app/components/account/feedback/feedback.dialog.html');
          tkDialog.show().then(function () {
            new jGrowlNotify().success($translate.instant('FEEDBACK_POSTED_TOAST'));
          })
        }

        function openBuyTicks() {
          $state.go('main.financial.start');
        }

        function redeemVoucher() {
          $mdDialog.show({
            clickOutsideToClose: true,
            escapeToClose: true,
            templateUrl: 'app/components/account/voucher/voucher.dialog.html',
            controller: 'VoucherDialogController',
            controllerAs: 'ctrl'
          });
        }

        function changePassword() {
          var tkDialog = new TkDialogFactory();
          tkDialog.controller('PasswordDialogController');
          tkDialog.templateUrl('app/components/account/password/password.dialog.html');
          tkDialog.show();
        }

        function logout() {
          AuthorizationService.resetUserAndToken();
          NavigationService.goToDiscoveryPage(true);
        }

        function refreshAccountInfo() {
          return AccountService.getAccountInfo()
            .success(function (data) {
              if (!UserService.isEmpty() && (data.notificationCount !== UserService.getNotificationCount())) {
                NotificationsService.refreshNotificationsCount();
              }
              UserService.mergeData(data);
              ctrl.profileImageUrl = UserService.getProfileImageUrl();
            })
            .error(function (err) {
              $log.error(err);
            })
        }

        function shareChannel(event) {
          if (ctrl.header.channel.firstContentId == null) {
            DialogService.showHelp(event, 'CHANNEL_SHARE_URL__NO_CONTENTS__HELP');
            return;
          }

          EncodingService.encodeContentAndUserId(ctrl.header.channel.firstContentId, UserService.getId())
            .success(function (data) {
              $mdDialog.show({
                clickOutsideToClose: true,
                escapeToClose: true,
                templateUrl: 'app/components/dialog/shareLink/shareLink.dialog.html',
                controller: 'ShareLinkDialogController',
                controllerAs: 'ctrl',
                locals: {
                  texts: {
                    header: $translate.instant('COMMONS_LINK_TO_CHANNEL'),
                    label: $translate.instant('COMMONS_LINK_TO_CHANNEL'),
                    copied: $translate.instant('COMMONS_URL_WAS_COPIED_TO_CLIP_BOARD')
                  },
                  url: ENV.URL + '/s/' + data.contentId + '-' + data.userId
                }
              })
            })
            .error(function (err) {
              $log.error(err)
            })
        }

        function sharePlaylist(event) {
          if (ctrl.header.playlist.id == null) {
            return;
          }

          EncodingService.encodePlaylistId(ctrl.header.playlist.id)
            .success(function (data) {
              $mdDialog.show({
                clickOutsideToClose: true,
                escapeToClose: true,
                templateUrl: 'app/components/dialog/shareLink/shareLink.dialog.html',
                controller: 'ShareLinkDialogController',
                controllerAs: 'ctrl',
                locals: {
                  texts: {
                    header: $translate.instant('PLAYLIST_SHARE_URL__TITLE'),
                    label: $translate.instant('PLAYLIST_SHARE_URL__FIELD__LABEL'),
                    copied: $translate.instant('PLAYLIST_SHARE_URL__LABEL__COPIED')
                  },
                  url: ENV.URL + '/p/' + data.playlistId
                }
              })
            })
            .error(function (err) {
              $log.error(err)
            });
        }
      }
    }
  }

})();
