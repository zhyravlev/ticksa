(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .directive('tkNotification', tkNotification);

  /** @ngInject */
  function tkNotification($timeout, $mdSidenav, $translate, lodash,
                          NotificationsHttpService, NotificationsService, NOTIFICATION_TYPES) {
    var SIDEBAR_ID = 'notification-sidebar';

    return {
      templateUrl: 'app/main/tk.notification.directive/tk.notification.directive.html',
      link: function (scope) {

        scope.loadingInProgress = true;

        var unRegisterNotificationsServiceOpeningObserver = NotificationsService.registerOpeningObserver(function () {
          showLoading(true);
          $mdSidenav(SIDEBAR_ID).toggle()
            .then(function () {
              return NotificationsHttpService.getNotificationsForUser();
            })
            .then(function (res) {
              clearAllNotifications();
              pushAllNotification(res);

              if (scope.notifications.length != 0) {
                return NotificationsHttpService.viewNotifications();
              }
            })
            .then(function (res) {
              if (res && res.data) {
                NotificationsService.setNotificationsCount(res.data.count);
              }
              showLoading(false);
            })
            .catch(function () {
              showLoading(false);
            })
        });

        scope.$on('$destroy', function () {
          unRegisterNotificationsServiceOpeningObserver();
        });

        function showLoading(value) {
          $timeout(function () {
            scope.$apply(function () {
              scope.loadingInProgress = value;
            });
          });
        }

        function clearAllNotifications() {
          scope.notifications = [];
          scope.invites = [];
          scope.messages = [];
          showLoading(true);
        }

        function pushAllNotification(res) {

          var freeTicksAmount = 0;
          var freeTicksCount = 0;

          lodash.forEach(res.data, function (item) {
            switch (item.type){
              case NOTIFICATION_TYPES.GRAND_FREE_TICKS:
                freeTicksAmount += item.metadata.amount;
                freeTicksCount++;
                break;

              case NOTIFICATION_TYPES.MESSAGE:
                scope.messages.push(item);
                break;

              case NOTIFICATION_TYPES.CHANNEL_EDIT_INVITE:
                scope.invites.push(item);
                break;

              case NOTIFICATION_TYPES.GRANT_COMMISSION_TICKS:
                item.content = $translate.instant('NOTIFICATIONS_GRANT_COMMISSION_TICKS_CONTENT', {
                  amount: item.metadata.amount
                });
                scope.notifications.push(item);
                break;

              case NOTIFICATION_TYPES.CHANNEL_SHARED_REVENUE_CHANGED:
                item.content = $translate.instant('NOTIFICATIONS_CHANNEL_SHARED_REVENUE_CHANGED',
                  {
                    percent: item.metadata.percent,
                    untilDate: item.metadata.expiryDate ? $translate.instant('COMMONS_UNTIL') + ' ' + item.metadata.expiryDate : '',
                    channelName: item.metadata.channelName
                  }
                );
                scope.notifications.push(item);
                break;

              default:
                scope.notifications.push(item);
            }
          });

          if(freeTicksAmount > 0){
            scope.notifications.unshift({
              content: $translate.instant('NOTIFICATIONS_GRAND_FREE_TICKS_CONTENT', {
                amount: freeTicksAmount
              }),
              freeTicksCount: freeTicksCount
            });
          }
        }
      }
    }
  }

})();
