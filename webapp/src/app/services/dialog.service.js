(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('DialogService', DialogService);

  /** @ngInject */
  function DialogService($mdDialog, $translate) {
    var DialogService = this;

    DialogService.showHelp = function (event, body) {
      showWindow(event, 'COMMONS_HELP', body)
    };

    /**
     * Shows simple window with title and body.
     *
     * @param {Object} event Event object
     * @param {String} title Window title
     * @param {String} body Window body
     */
    var showWindow = function (event, title, body) {
      event.stopPropagation();

      $mdDialog.show({
        templateUrl: 'app/components/dialog/help/dialog.help.html',
        controller: 'DialogHelpController',
        controllerAs: 'ctrl',
        clickOutsideToClose: true,
        locals: {
          texts: {
            title: $translate.instant(title),
            body: $translate.instant(body)
          }
        }
      })
    }
  }

})();
