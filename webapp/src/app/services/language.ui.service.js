(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('LanguageUIService', LanguageUIService);

  /** @ngInject */
  function LanguageUIService($q, $interval, $translate, $rootScope, lodash,
                             timeAgoSettings, UI_LANGUAGES) {

    var _keyGetPromise = $q.defer();

    var key = $translate.use();
    if (key != null) {
      _keyGetPromise.resolve({key: key});

    } else {
      // according to https://github.com/angular-translate/angular-translate/issues/485
      // $translate.use() can return undefined - so we have to query it periodically
      var getLanguageKeyInterval = $interval(function () {
        var translationKey = $translate.use();
        if (translationKey != null) {
          _keyGetPromise.resolve({key: translationKey});
          $interval.cancel(getLanguageKeyInterval);
        }
      }, 100);

      $rootScope.$on('$destroy', function () {
        $interval.cancel(getLanguageKeyInterval);
      });
    }

    return {
      setLanguage: setLanguage,
      getLanguage: getLanguage,
      constructUILanguageObject: constructUILanguageObject
    };

    function angularTimeago(key) {
      var getCode = function (key) {
        switch (key) {
          case 'en':
            return 'en_US';
            break;
          case 'de':
            return 'de_DE';
            break;
          case 'fr':
            return 'fr_FR';
            break;
          case 'hu':
            return 'hu_HU';
            break;
          case 'it':
            return 'it_IT';
            break;
          case 'nl':
            return 'nl_NL';
            break;
          case 'pl':
            return 'pl_PL';
            break;
          case 'sk':
          case 'ru':
          case 'ro':
          case 'es':
          case 'cs':
            return 'COSTUME';
            break;
          default:
            return false;
        }
      };

      var code;
      if ((code = getCode(key)) && code) {

        /**
         * For only: sk, ru, ro, es, cs.
         * Because these languages are not defined in the timeAgoSettings.
         */
        if (code === 'COSTUME') {
          timeAgoSettings.strings['COSTUME'] = {
            prefixAgo             : $translate.instant('TIME_AGO__PREFIX_AGO'),
            prefixFromNow         : $translate.instant('TIME_AGO__PREFIX_FROM_NOW'),
            suffixAgo             : $translate.instant('TIME_AGO__SUFFIX_AGO'),
            suffixFromNow         : $translate.instant('TIME_AGO__SUFFIX_FROM_NOW'),
            seconds               : $translate.instant('TIME_AGO__SECONDS'),
            minute                : $translate.instant('TIME_AGO__MINUTE'),
            minutes               : $translate.instant('TIME_AGO__MINUTES'),
            hour                  : $translate.instant('TIME_AGO__HOUR'),
            hours                 : $translate.instant('TIME_AGO__HOURS'),
            day                   : $translate.instant('TIME_AGO__DAY'),
            days                  : $translate.instant('TIME_AGO__DAYS'),
            month                 : $translate.instant('TIME_AGO__MONTH'),
            months                : $translate.instant('TIME_AGO__MONTHS'),
            year                  : $translate.instant('TIME_AGO__YEAR'),
            years                 : $translate.instant('TIME_AGO__YEARS'),
            numbers               : []
          };
        }

        return timeAgoSettings.overrideLang = code;
      }
    }

    function setLanguage(key) {
      if (key != null) {
        return $translate.use(key)
          .then(function () {
            _keyGetPromise = $q.defer();
            _keyGetPromise.resolve({key: key});
            angularTimeago(key);
            return $q.resolve();
          });
      }
      return $q.resolve();
    }

    function getLanguage() {
      return _keyGetPromise.promise;
    }

    // method returns object constructed to be used in md-select. with list of values from constant UI_LANGUAGES
    function constructUILanguageObject(langKey) {
      return {
        id: langKey,

        // this field fixes md-select strange behavior
        // even if ng-model-options="{trackBy: '$value.id'}" set initially it looks like that md-select makes
        // deep equal comparison. because if one doesn't set 'name' field to current object then function applied to
        // ng-change will be fired. so we have to set 'name' to the correct value.
        name: lodash.result(lodash.findWhere(UI_LANGUAGES, {id: langKey}), 'name')
      }
    }
  }
})
();
