(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('SearchService', SearchService);

  /**
   * Helper-service to manage interactions between searchbar component that is in header and discover page
   * controller that queries server for contents and filters then by searchString.
   */

  /** @ngInject */
  function SearchService($log, lodash) {
    $log = $log.getInstance('SearchService');

    var SearchService = this;

    var observers = [];
    var data = {
      searchString: '',
      enableSearchBar: false
    };

    var unregisterObserver = function (observer) {
      return function () {
        var index = observers.indexOf(observer);
        if (index > -1) {
          observers.splice(index, 1);
          return;
        }

        $log.warn('doesnt have registered observer: ' + angular.toJson(observer));
      }
    };

    var notifyObservers = function () {
      lodash.forEach(observers, function (observer) {
        observer(lodash.clone(data));
      });
    };

    SearchService.registerObserver = function (observerFunction) {
      if (observers.indexOf(observerFunction) > -1) {
        $log.warn('already has registered observer: ' + angular.toJson(observerFunction));
        return unregisterObserver(observerFunction);
      }

      observers.push(observerFunction);
      observerFunction(lodash.clone(data));

      return unregisterObserver(observerFunction);
    };

    SearchService.setSearchString = function (searchString) {
      var previousValue = data.searchString;
      data.searchString = searchString;

      if (previousValue !== data.searchString) {
        notifyObservers();
      }
    };

    SearchService.enableSearchBarComponent = function (enable) {
      var previousValue = data.enableSearchBar;
      data.enableSearchBar = enable;

      if (!data.enableSearchBar) {
        data.searchString = '';
      }

      if (previousValue != data.enableSearchBar) {
        notifyObservers();
      }
    }
  }
})();
