(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('FileTypeService', FileTypeService);

  /** @ngInject */
  function FileTypeService($log) {

    $log = $log.getInstance('FileTypeService');

    var FileTypeService = this;

    var AVAILABLE_VIDEO_FILE_EXTENSIONS = ['flv', 'mkv', 'webm', 'avi', 'mov', 'qt', 'wmv', 'mp4', 'x-m4v', 'mpg', 'mp2', 'mpeg', 'mpe', 'mpv', 'm2v', '3gp'];
    var AVAILABLE_IMAGE_FILE_EXTENSIONS = ['jpeg', 'jpg', 'jpe', 'png', 'gif', 'bmp', 'tif', 'tiff'];

    FileTypeService.FILE_TYPE = {
      VIDEO: 'video',
      IMAGE: 'image',
      UNKNOWN: 'unknown'
    };
    FileTypeService.AVAILABLE_IMAGE_FILE_EXTENSIONS_LIST = '.' + AVAILABLE_IMAGE_FILE_EXTENSIONS.join(',.');
    FileTypeService.AVAILABLE_VIDEO_FILE_EXTENSIONS_LIST = '.' + AVAILABLE_VIDEO_FILE_EXTENSIONS.join(',.');

    FileTypeService.extractFileType = function (file) {
      if (/^(video)/.exec(file.type) !== null || FileTypeService.AVAILABLE_VIDEO_FILE_EXTENSIONS_LIST.indexOf(file.extension) >= 0) {
        return FileTypeService.FILE_TYPE.VIDEO;
      }

      if (/^(image)/.exec(file.type) !== null || FileTypeService.AVAILABLE_IMAGE_FILE_EXTENSIONS_LIST.indexOf(file.extension) >= 0) {
        return FileTypeService.FILE_TYPE.IMAGE;
      }

      $log.toServer.warn('Undefined file type: %j', file);
      return FileTypeService.FILE_TYPE.UNKNOWN;
    }
  }

})();
