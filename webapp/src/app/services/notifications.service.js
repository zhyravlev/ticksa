(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('NotificationsService', NotificationsService);

  /** @ngInject */
  function NotificationsService($log, $interval, lodash, NotificationsHttpService) {
    $log = $log.getInstance('NotificationsService');

    var NotificationsService = this;

    var openingObservers = [];
    var dataObservers = [];
    var data = {
      notificationsCount: 0
    };
    var notificationCountRefreshingInterval = null;

    var unregisterOpeningObserver = function (observer) {
      return function () {
        var index = openingObservers.indexOf(observer);
        if (index > -1) {
          openingObservers.splice(index, 1);
          return;
        }

        $log.warn('doesnt have registered opening observer: ' + angular.toJson(observer));
      }
    };

    var unregisterDataObserver = function (observer) {
      return function () {
        var index = dataObservers.indexOf(observer);
        if (index > -1) {
          dataObservers.splice(index, 1);
          return;
        }

        $log.warn('doesnt have registered data observer: ' + angular.toJson(observer));
      }
    };

    var notifyOpeningObservers = function () {
      lodash.forEach(openingObservers, function (observer) {
        observer();
      });
    };

    var notifyDataObservers = function () {
      lodash.forEach(dataObservers, function (observer) {
        observer(lodash.clone(data));
      });
    };

    NotificationsService.registerOpeningObserver = function (observerFunction) {
      if (openingObservers.indexOf(observerFunction) > -1) {
        $log.warn('already has registered opening observer: ' + angular.toJson(observerFunction));
        return unregisterOpeningObserver(observerFunction);
      }

      openingObservers.push(observerFunction);
      return unregisterOpeningObserver(observerFunction);
    };

    NotificationsService.registerDataObserver = function (observerFunction) {
      if (dataObservers.indexOf(observerFunction) > -1) {
        $log.warn('already has registered data observer: ' + angular.toJson(observerFunction));
        return unregisterOpeningObserver(observerFunction);
      }

      dataObservers.push(observerFunction);
      observerFunction(lodash.clone(data));

      return unregisterDataObserver(observerFunction);
    };

    NotificationsService.openNotificationsBar = function () {
      notifyOpeningObservers();
    };

    NotificationsService.setNotificationsCount = function (count) {
      if (data.notificationsCount == count) {
        return;
      }

      data.notificationsCount = count;
      notifyDataObservers();
    };

    NotificationsService.refreshNotificationsCount = function () {
      NotificationsHttpService.getCountNotificationsForUser()
        .then(function (res) {
          NotificationsService.setNotificationsCount(res.data.count || 0);
        });
    };

    NotificationsService.startNotificationsCountRefreshing = function () {
      if (notificationCountRefreshingInterval != null) {
        $log.warn('already has started refreshing notifications count interval: ', notificationCountRefreshingInterval);
        return;
      }
      notificationCountRefreshingInterval = $interval(NotificationsService.refreshNotificationsCount, 60000); //1min
    };

    NotificationsService.stopNotificationsCountRefreshing = function () {
      if (notificationCountRefreshingInterval == null) {
        $log.warn('still has no started refreshing notifications count interval');
        return;
      }
      $interval.cancel(notificationCountRefreshingInterval);
      notificationCountRefreshingInterval = null;
    };
  }
})();
