(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('LoggerHttpService', LoggerHttpService);

  /** @ngInject */
  function LoggerHttpService($http, $q, API) {

    return {
      sendLogsToServer: sendLogsToServer
    };

    function sendLogsToServer(logs) {
      if (logs == null || !angular.isArray(logs) || logs.length == 0) {
        return $q.resolve();
      }

      return $http.post(API.URL + '/logger/log', {logs: logs});
    }
  }
})();
