(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('CircularDependenciesResolverService', CircularDependenciesResolverService);

  /** @ngInject */
  function CircularDependenciesResolverService($injector) {
    return {
      getNavigationService: function () {
        return $injector.get('NavigationService');
      },

      getAuthorizationService: function () {
        return $injector.get('AuthorizationService');
      },

      getLoggerCollectorService: function () {
        return $injector.get('LoggerCollectorService');
      }
    }
  }
})();
