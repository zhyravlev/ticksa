(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('VoteBoardService', VoteBoardService);

  /** @ngInject */
  function VoteBoardService($log, lodash) {
    $log = $log.getInstance('VoteBoardService');

    var VoteBoardService = this;

    var observers = [];
    var data = {};

    VoteBoardService.registerObserver = function (observerFunction) {
      if (observers.indexOf(observerFunction) > -1) {
        $log.warn('already has registered observer: ' + angular.toJson(observerFunction));
        return unregisterObserver(observerFunction);
      }

      observers.push(observerFunction);
      observerFunction(lodash.clone(data));

      return unregisterObserver(observerFunction);
    };

    VoteBoardService.broadcast = function (id, likes) {
      data.id = id;
      data.likes = likes;
      notifyObservers();
    };

    var unregisterObserver = function (observer) {
      return function () {
        var index = observers.indexOf(observer);
        if (index > -1) {
          observers.splice(index, 1);
          return;
        }

        $log.warn('doesnt have registered observer: ' + angular.toJson(observer));
      }
    };

    var notifyObservers = function () {
      lodash.forEach(observers, function (observer) {
        observer(lodash.clone(data));
      });
    };
  }
})();
