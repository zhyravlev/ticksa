(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('HeaderFilterService', HeaderFilterService);

  /** @ngInject */
  function HeaderFilterService($log, lodash) {
    $log = $log.getInstance('HeaderFilterService');

    var HeaderFilterService = this;

    var observers = [];
    var data = {
      in: false,
      enable: false,
      used: false
    };

    HeaderFilterService.registerObserver = function (observerFunction) {
      if (observers.indexOf(observerFunction) > -1) {
        $log.warn('already has registered observer: ' + angular.toJson(observerFunction));
        return unregisterObserver(observerFunction);
      }

      observers.push(observerFunction);
      observerFunction(lodash.clone(data));

      return unregisterObserver(observerFunction);
    };

    var unregisterObserver = function (observer) {
      return function () {
        var index = observers.indexOf(observer);
        if (index > -1) {
          observers.splice(index, 1);
          return;
        }

        $log.warn('doesnt have registered observer: ' + angular.toJson(observer));
      }
    };

    var notifyObservers = function () {
      lodash.forEach(observers, function (observer) {
        observer(lodash.clone(data));
      });
    };

    HeaderFilterService.toggleFilter = function () {
      data.in = !data.in;
      notifyObservers();
    };

    HeaderFilterService.disabledFilter = function () {
      data.in = false;
      notifyObservers();
    };

    HeaderFilterService.enableFilter = function (enable) {
      var previousValue = data.enable;
      data.enable = enable;

      if (previousValue != data.enable) {
        notifyObservers();
      }
    };

    HeaderFilterService.setUse = function (value) {
      data.used = value;
      notifyObservers();
    };
  }
})();
