(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('AuthorizationTokenService', AuthorizationTokenService);

  /** @ngInject */
  function AuthorizationTokenService($interval, $cookies, jwtHelper) {
    var AuthorizationTokenService = this;

    var AUTHORIZATION_TOKEN_COOKIES_NAME = 'TicksaToken';
    var rereadCookiesInterval = null;
    var authorizationToken = null;
    var decodedAuthorizationToken = null;

    AuthorizationTokenService.loadAndSetTokenFromCookies = function () {
      var fromCookies = $cookies.get(AUTHORIZATION_TOKEN_COOKIES_NAME);
      _setAuthorizationToken(fromCookies);
      if (fromCookies != null) {
        _initRereadCookiesInterval();
      }
      return fromCookies;
    };

    AuthorizationTokenService.setToken = function (token) {
      _setAuthorizationToken(token);
      $cookies.put(AUTHORIZATION_TOKEN_COOKIES_NAME, token, {
        expires: new Date(decodedAuthorizationToken.exp * 1000)
      });
      _initRereadCookiesInterval();
    };

    AuthorizationTokenService.resetToken = function () {
      $cookies.remove(AUTHORIZATION_TOKEN_COOKIES_NAME);
      _setAuthorizationToken(null);
      _finishRereadCookiesInterval();
    };

    AuthorizationTokenService.getToken = function () {
      return authorizationToken;
    };

    AuthorizationTokenService.getEncryptedTickserId = function () {
      return decodedAuthorizationToken == null ? null : decodedAuthorizationToken.encId;
    };

    function _initRereadCookiesInterval() {
      _finishRereadCookiesInterval();
      rereadCookiesInterval = $interval(function () {
        var fromCookies = $cookies.get(AUTHORIZATION_TOKEN_COOKIES_NAME);
        _setAuthorizationToken(fromCookies);
        if (fromCookies == null) {
          _finishRereadCookiesInterval();
        }
      }, 1000);
    }

    function _finishRereadCookiesInterval() {
      if (rereadCookiesInterval != null) {
        $interval.cancel(rereadCookiesInterval);
      }
    }

    function _setAuthorizationToken(encodedToken) {
      if (encodedToken == null) {
        authorizationToken = null;
        decodedAuthorizationToken = null;
      } else {
        authorizationToken = encodedToken;
        decodedAuthorizationToken = jwtHelper.decodeToken(authorizationToken);
      }
    }
  }

})();
