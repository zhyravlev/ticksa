(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('CountryService', CountryService);

  /** @ngInject */
  function CountryService(API, $http, $q) {
    var countries = [];

    return {
      getCountries: getCountries
    };

    function getCountries() {
      var def = $q.defer();

      if (countries.length == 0) {
        $http.get(API.URL + '/countries', null).then(function (res) {
          countries = res.data;
          def.resolve(countries);
        });
      } else {
        def.resolve(countries);
      }

      return def.promise;
    }
  }
})();
