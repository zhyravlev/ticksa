(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('CategoryFilterService', CategoryFilterService);

  /** @ngInject */
  function CategoryFilterService(API, $http, $q) {

    var categories = [];

    function getCategories() {
      var def = $q.defer();

      if(categories.length == 0)  {
        $http.get(API.URL + '/categories', null).then(function(res) {
          categories = res.data;
          def.resolve(categories);
        });
      } else {
        def.resolve(categories);
      }

      return def.promise;
    }

    return {
      getCategories: getCategories
    }
  }

})();
