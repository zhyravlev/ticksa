(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('LanguageFilterService', LanguageFilterService);

  /** @ngInject */
  function LanguageFilterService(API, $http, $q) {

    var languages = [];

    function getLanguages() {
      var def = $q.defer();

      if(languages.length == 0)  {
        $http.get(API.URL + '/languages', null).then(function(res) {
          languages = res.data;
          def.resolve(languages);
        });
      } else {
        def.resolve(languages);
      }

      return def.promise;
    }

    return {
      getLanguages: getLanguages
    }
  }

})();
