(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .factory('Formatter', Formatter)
  ;

  /** @ngInject */
  function Formatter() {
    var formatRegExp = /%[sdj%]/g;

    return {
      format: format
    };

    // copied from NodeJS 0.12.10 core: util.format.
    function format(f) {
      if (!angular.isString(f)) {
        var objects = [];
        for (var k = 0; k < arguments.length; k++) {
          objects.push(safeStringify(arguments[k]));
        }
        return objects.join(' ');
      }

      var i = 1;
      var args = arguments;
      var len = args.length;
      var str = String(f).replace(formatRegExp, function (x) {
        if (x === '%%') {
          return '%';
        }

        if (i >= len) {
          return x;
        }

        switch (x) {
          case '%s':
            return String(args[i++]);

          case '%d':
            return Number(args[i++]);

          case '%j':
            return safeStringify(args[i++]);

          default:
            return x;
        }
      });

      for (var x = args[i]; i < len; x = args[++i]) {
        if (x === null || !angular.isObject(x)) {
          str += ' ' + x;
        } else {
          str += ' ' + safeStringify(x);
        }
      }

      return str;
    }

    function safeStringify(value) {
      try {
        var type = Object.prototype.toString.call(value);
        switch (type) {
          case '[object Error]':
            return formatError(value);

          case '[object HTMLElement]':
            return formatHTMLElement(value);

          default:
            return angular.toJson(value);
        }
      } catch (_) {
        return '[Circular]';
      }
    }

    // copied from angular.js v1.4.9 core.
    function formatError(arg) {
      if (arg.stack) {
        arg = (arg.message && arg.stack.indexOf(arg.message) === -1)
          ? 'Error: ' + arg.message + '\n' + arg.stack
          : arg.stack;
      } else if (arg.sourceURL) {
        arg = arg.message + '\n' + arg.sourceURL + ':' + arg.line;
      }
      return arg;
    }

    // copied from here:
    // http://stackoverflow.com/questions/3682278/
    //
    // because angular's jqLite doesn't have 'outerHtml' method:
    // https://docs.angularjs.org/api/ng/function/angular.element
    function formatHTMLElement(arg) {
      return angular.element('<div>').append(arg).html();
    }
  }
})();
