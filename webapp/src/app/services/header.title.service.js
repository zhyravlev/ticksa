(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('HeaderTitleService', HeaderTitleService);

  /** @ngInject */
  function HeaderTitleService($log, lodash) {
    $log = $log.getInstance('HeaderTitleService');

    var HeaderTitleService = this;

    var observers = [];
    var data = {
      title: '',
      titleTranslationKey: '',
      channel: {
        id: null,
        name: null,
        canClick: false,
        isSubscribed: false,
        firstContentId: null
      },
      playlist: {
        id: null
      }
    };

    HeaderTitleService.registerObserver = function (observerFunction) {
      if (observers.indexOf(observerFunction) > -1) {
        $log.warn('already has registered observer: ' + angular.toJson(observerFunction));
        return unregisterObserver(observerFunction);
      }

      observers.push(observerFunction);
      observerFunction(lodash.clone(data));

      return unregisterObserver(observerFunction);
    };

    HeaderTitleService.setCustomTitle = function (title) {
      data.title = title;
      data.titleTranslationKey = '';
      notifyObservers();
    };

    HeaderTitleService.setTranslatedTitle = function (translationKey) {
      data.title = '';
      data.titleTranslationKey = translationKey;
      notifyObservers();
    };

    HeaderTitleService.setChannelSubscribed = function (channelId, channelName, channelSubscribed) {
      data.channel.id = channelId;
      data.channel.name = channelName;
      data.channel.isSubscribed = channelSubscribed;
      notifyObservers();
    };

    HeaderTitleService.setChanelFirstContentId = function (firstContentId) {
      data.channel.firstContentId = firstContentId;
      notifyObservers();
    };

    HeaderTitleService.resetChanelFirstContentId = function () {
      data.channel.firstContentId = null;
      notifyObservers();
    };

    HeaderTitleService.enableClickableChannelNameAsTitle = function (canClick) {
      data.channel.canClick = canClick;
      notifyObservers();
    };

    HeaderTitleService.setPlaylistId = function (playlistId) {
      data.playlist.id = playlistId;
      notifyObservers();
    };

    var unregisterObserver = function (observer) {
      return function () {
        var index = observers.indexOf(observer);
        if (index > -1) {
          observers.splice(index, 1);
          return;
        }

        $log.warn('doesnt have registered observer: ' + angular.toJson(observer));
      }
    };

    var notifyObservers = function () {
      lodash.forEach(observers, function (observer) {
        observer(lodash.clone(data));
      });
    };
  }
})();
