(function () {
  'use strict';

  angular
    .module('ticksaWebapp')
    .service('TickserHttpService', TickserHttpService);

  /** @ngInject */
  function TickserHttpService($http, API) {
    var TickserHttpService = this;

    TickserHttpService.getUserByEmail = function (email) {
      return $http.get(API.URL + '/ticksers/q', {params: {email: email}});
    }
  }

})();
