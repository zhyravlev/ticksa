/**
 * Description.
 *
 * This module executed before main Angular application. Historically in webapp for Angular routing '#' symbol was used.
 * But according to APP-757 it was changed to hashbang (#!). This module is a workaround for legacy shared links. If
 * someone tries to access Ticksa via oldstyle '#'-based links then href should be changed to hashbang-based one.
 */
(function () {
  'use strict';

  // value of hash. includes '#' symbol as the first symbol
  var hash = window.location.hash;

  if (hash) {
    var hashBang = '#!';
    if (hash.substr(0, 2) === hashBang) {
      // do not do anything - already hashbang is used
      return;
    }

    // if hashbang is not used - then redirect to href with hashbang
    window.location.href = hashBang + hash.substring(1);
  }

})();