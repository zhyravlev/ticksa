module.exports = {
  '/fixture/playlist/new': {
    POST: {
      data: {
        "playlistId": 1
      },
      code: 200
    }
  },
  '/fixture/playlist/getAll': {
    GET: {
      data: [
        {
          "id": 1,
          "name": "My first playlist",
          "coverImage": {
            "id": 460,
            "key": "7d0b65ed-205c-41ac-8846-b4d30d5a60a9.jpg"
          },
          "backgroundImage": {
            "id": 1558,
            "key": "25d093fb-eca2-45b2-9625-c1a34c4e10b6.jpg"
          },
          "published": true
        },
        {
          "id": 2,
          "name": "My second playlist",
          "coverImage": {
            "id": 492,
            "key": "ccf3514c-3525-4c8e-b0e5-724a05ad193a.jpg"
          },
          "backgroundImage": null,
          "published": false
        }
      ],
      code: 200
    }
  },
  '/fixture/playlist/getSettings': {
    GET: {
      query: {
        'playlistId=1': {
          data: {
            "id": 1,
            "name": "My first playlist",
            "coverImage": {
              "id": 460,
              "key": "7d0b65ed-205c-41ac-8846-b4d30d5a60a9.jpg"
            },
            "backgroundImage": {
              "id": 1558,
              "key": "25d093fb-eca2-45b2-9625-c1a34c4e10b6.jpg"
            },
            "published": true
          }
        }
      },
      code: 200
    }
  },
  '/fixture/playlist/updateSettings/1': {
    PUT: {
      data: {
        "success": true
      },
      code: 200
    }
  },
  '/fixture/playlist/update/1': {
    PUT: {
      data: {
        "success": true
      },
      code: 200
    }
  },
  '/fixture/playlist/get': {
    GET: {
      query: {
        'playlistId=1': {
          data: {
            "id": 1,
            "name": "My first playlist",
            "content": [
              {
                "id": 1181,
                "title": "Bob Proctor at \"The Week for Life\"",
                "type": 3,
                "likes": 0,
                "dislikes": 0,
                "tickPrice": 3,
                "textLength": null,
                "imageCount": null,
                "videoLength": 90666,
                "bought": false,
                "manager": true,
                "published": true,
                "createdAt": "2016-07-01T11:52:39.875Z",
                "updatedAt": "2016-07-01T11:52:39.875Z",
                "haveCondition": false,
                "haveGeographicLimited": false,
                "channel": {
                  "id": 42,
                  "name": "Rene Klampfer's Video Channel",
                  "verified": false,
                  "coverImage": {"id": 229, "key": "7b7a2754-442d-47b0-aaee-8b3c2999f938.jpg"},
                  "background": null
                },
                "tags": [],
                "teaserText": "",
                "teaserImage": {
                  "name": "Bob_Proctor__Week_for_Life.jpg",
                  "key": "525afbbb-69de-4742-9d2a-32beea86e939.jpg"
                },
                "teaserVideo": null
              },
              {
                "id": 1127,
                "title": "Strong Lead Workshop",
                "type": 1,
                "likes": 1,
                "dislikes": 0,
                "tickPrice": 2,
                "textLength": null,
                "imageCount": 1,
                "videoLength": null,
                "bought": false,
                "manager": true,
                "published": true,
                "createdAt": "2016-05-10T14:26:33.567Z",
                "updatedAt": "2016-05-13T21:18:10.297Z",
                "haveCondition": false,
                "haveGeographicLimited": false,
                "channel": {
                  "id": 42,
                  "name": "Rene Klampfer's Video Channel",
                  "verified": false,
                  "coverImage": {"id": 229, "key": "7b7a2754-442d-47b0-aaee-8b3c2999f938.jpg"},
                  "background": null
                },
                "tags": [],
                "teaserText": "Teil 2",
                "teaserImage": {"name": "StrongLead1.png", "key": "5ea4c357-49ff-4487-816c-0fe64cd17167.png"},
                "teaserVideo": null
              },
              {
                "id": 1121,
                "title": "Strong Lead Workshop",
                "type": 1,
                "likes": 0,
                "dislikes": 0,
                "tickPrice": 2,
                "textLength": null,
                "imageCount": 2,
                "videoLength": null,
                "bought": false,
                "manager": true,
                "published": true,
                "createdAt": "2016-05-04T19:42:59.883Z",
                "updatedAt": "2016-05-04T19:42:59.883Z",
                "haveCondition": false,
                "haveGeographicLimited": false,
                "channel": {
                  "id": 42,
                  "name": "Rene Klampfer's Video Channel",
                  "verified": false,
                  "coverImage": {"id": 229, "key": "7b7a2754-442d-47b0-aaee-8b3c2999f938.jpg"},
                  "background": null
                },
                "tags": [],
                "teaserText": "Erlebe deine Ressourcen!",
                "teaserImage": {"name": "DSC_0005.JPG", "key": "05c24a09-96ec-4bb2-acb5-7dc41f2e6b7c.JPG"},
                "teaserVideo": null
              },
              {
                "id": 1056,
                "title": "Bungee Jump",
                "type": 3,
                "likes": 1,
                "dislikes": 0,
                "tickPrice": 5,
                "textLength": null,
                "imageCount": null,
                "videoLength": 159568,
                "bought": false,
                "manager": true,
                "published": true,
                "createdAt": "2016-04-13T13:40:38.611Z",
                "updatedAt": "2016-04-15T14:46:36.525Z",
                "haveCondition": false,
                "haveGeographicLimited": false,
                "channel": {
                  "id": 42,
                  "name": "Rene Klampfer's Video Channel",
                  "verified": false,
                  "coverImage": {"id": 229, "key": "7b7a2754-442d-47b0-aaee-8b3c2999f938.jpg"},
                  "background": null
                },
                "tags": [],
                "teaserText": "",
                "teaserImage": {"name": "IMG_4141.PNG", "key": "4c54df4a-574a-42b6-b820-2792cc2f0e96.PNG"},
                "teaserVideo": null
              }
            ]
          }
        }
      },
      code: 200
    }
  },
  '/fixture/playlist/1/contents': {
    GET: {
      query: {
        'page=0&pageSize=12': {
          data: [
            {
              "id": 1181,
              "title": "Bob Proctor at \"The Week for Life\"",
              "type": 3,
              "likes": 0,
              "dislikes": 0,
              "tickPrice": 3,
              "textLength": null,
              "imageCount": null,
              "videoLength": 90666,
              "bought": false,
              "manager": true,
              "published": true,
              "createdAt": "2016-07-01T11:52:39.875Z",
              "updatedAt": "2016-07-01T11:52:39.875Z",
              "haveCondition": false,
              "haveGeographicLimited": false,
              "channel": {
                "id": 42,
                "name": "Rene Klampfer's Video Channel",
                "verified": false,
                "coverImage": {"id": 229, "key": "7b7a2754-442d-47b0-aaee-8b3c2999f938.jpg"},
                "background": null
              },
              "tags": [],
              "teaserText": "",
              "teaserImage": {
                "name": "Bob_Proctor__Week_for_Life.jpg",
                "key": "525afbbb-69de-4742-9d2a-32beea86e939.jpg"
              },
              "teaserVideo": null
            },
            {
              "id": 1127,
              "title": "Strong Lead Workshop",
              "type": 1,
              "likes": 1,
              "dislikes": 0,
              "tickPrice": 2,
              "textLength": null,
              "imageCount": 1,
              "videoLength": null,
              "bought": false,
              "manager": true,
              "published": true,
              "createdAt": "2016-05-10T14:26:33.567Z",
              "updatedAt": "2016-05-13T21:18:10.297Z",
              "haveCondition": false,
              "haveGeographicLimited": false,
              "channel": {
                "id": 42,
                "name": "Rene Klampfer's Video Channel",
                "verified": false,
                "coverImage": {"id": 229, "key": "7b7a2754-442d-47b0-aaee-8b3c2999f938.jpg"},
                "background": null
              },
              "tags": [],
              "teaserText": "Teil 2",
              "teaserImage": {"name": "StrongLead1.png", "key": "5ea4c357-49ff-4487-816c-0fe64cd17167.png"},
              "teaserVideo": null
            },
            {
              "id": 1121,
              "title": "Strong Lead Workshop",
              "type": 1,
              "likes": 0,
              "dislikes": 0,
              "tickPrice": 2,
              "textLength": null,
              "imageCount": 2,
              "videoLength": null,
              "bought": false,
              "manager": true,
              "published": true,
              "createdAt": "2016-05-04T19:42:59.883Z",
              "updatedAt": "2016-05-04T19:42:59.883Z",
              "haveCondition": false,
              "haveGeographicLimited": false,
              "channel": {
                "id": 42,
                "name": "Rene Klampfer's Video Channel",
                "verified": false,
                "coverImage": {"id": 229, "key": "7b7a2754-442d-47b0-aaee-8b3c2999f938.jpg"},
                "background": null
              },
              "tags": [],
              "teaserText": "Erlebe deine Ressourcen!",
              "teaserImage": {"name": "DSC_0005.JPG", "key": "05c24a09-96ec-4bb2-acb5-7dc41f2e6b7c.JPG"},
              "teaserVideo": null
            },
            {
              "id": 1056,
              "title": "Bungee Jump",
              "type": 3,
              "likes": 1,
              "dislikes": 0,
              "tickPrice": 5,
              "textLength": null,
              "imageCount": null,
              "videoLength": 159568,
              "bought": false,
              "manager": true,
              "published": true,
              "createdAt": "2016-04-13T13:40:38.611Z",
              "updatedAt": "2016-04-15T14:46:36.525Z",
              "haveCondition": false,
              "haveGeographicLimited": false,
              "channel": {
                "id": 42,
                "name": "Rene Klampfer's Video Channel",
                "verified": false,
                "coverImage": {"id": 229, "key": "7b7a2754-442d-47b0-aaee-8b3c2999f938.jpg"},
                "background": null
              },
              "tags": [],
              "teaserText": "",
              "teaserImage": {"name": "IMG_4141.PNG", "key": "4c54df4a-574a-42b6-b820-2792cc2f0e96.PNG"},
              "teaserVideo": null
            }
          ]
        }
      },
      code: 200
    }
  }
};