/**
 *  This file contains the variables used in other gulp files
 *  which defines tasks
 *  By design, we only put there very generic config values
 *  which are used in several places to keep good readability
 *  of the tasks
 */

var gutil = require('gulp-util');

/**
 *  The main paths of your project handle these with care
 */
exports.paths = {
  src: 'src',
  dist: 'dist',
  tmp: '.tmp',
  e2e: 'e2e',
  fixtures: 'fixtures'
};

exports.fontello = {
  config: 'assets/fonts/source/config.json',
  source: 'assets/fonts/source/svg/*.svg',
  dist: 'assets/fonts/fontello'
};

/**
 *  Wiredep is the lib which inject bower dependencies in your project
 *  Mainly used to inject script tags in the index.html but also used
 *  to inject css preprocessor deps and js files in karma
 */
exports.wiredep = {
  directory: 'bower_components'
};

/**
 *  Common implementation for an error handler of a Gulp plugin
 */
exports.errorHandler = function(title) {
  'use strict';

  return function(err) {
    gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
    this.emit('end');
  };
};

var date = new Date();
exports.time = date.getTime();

exports.autoprefixerOptions = {
  browsers: ['Explorer >= 10',
    'Edge >= 12',
    'Firefox >= 2',
    'Chrome >= 4',
    'Safari >= 4',
    'Opera >= 12.1',
    'iOS >= 7.1',
    'OperaMini >= 8',
    'Android >= 2.3',
    'BlackBerry >= 7',
    'OperaMobile >= 33',
    'ChromeAndroid >= 47',
    'FirefoxAndroid >= 44',
    'ExplorerMobile >= 10']
};
