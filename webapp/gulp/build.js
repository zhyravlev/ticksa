'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del', 'run-sequence']
});

gulp.task('partials', ['markups'], function () {
  return gulp.src([
    path.join(conf.paths.src, '/app/**/*.html'),
    path.join(conf.paths.tmp, '/serve/app/**/*.html')
  ])
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'ticksaWebapp',
      root: 'app'
    }))
    .pipe(gulp.dest(conf.paths.tmp + '/partials/'));
});

gulp.task('html', ['inject', 'partials'], function () {

  var partialsInjectFile = gulp.src(path.join(conf.paths.tmp, '/partials/templateCacheHtml.js'), { read: false });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: path.join(conf.paths.tmp, '/partials'),
    addRootSlash: false
  };

  var htmlFilter = $.filter('*.html', { restore: true });
  var jsFilter = $.filter('**/*.js', { restore: true });
  var cssFilter = $.filter('**/*.css', { restore: true });
  var assets;

  return gulp.src(path.join(conf.paths.tmp, '/serve/*.html'))
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = $.useref.assets())
    .pipe(jsFilter)
    .pipe($.ngAnnotate())
    .pipe($.uglify({ preserveComments: $.uglifySaveLicense })).on('error', conf.errorHandler('Uglify'))
    .pipe(jsFilter.restore)
    .pipe(cssFilter)
    .pipe($.csso())
    .pipe(cssFilter.restore)
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true,
      conditionals: true
    }))
    .pipe(htmlFilter.restore)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/' + conf.time + '/')))
    .pipe($.size({ title: path.join(conf.paths.dist, '/' + conf.time + '/'), showFiles: true }));
  });

// Only applies for fonts from bower dependencies
// Custom fonts are handled by the "other" task
gulp.task('fonts', function () {
  return gulp.src($.mainBowerFiles())
    .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe($.flatten())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/' + conf.time + '/fonts/')))

    // because of videogular-themes-default/videogular.css file.
    // it has hard-coded links to fonts in path.
    // that is why fonts files are duplicated to /styles/fonts folder
    .pipe(gulp.dest(path.join(conf.paths.dist, '/' + conf.time + '/styles/fonts/')));
});

gulp.task('other', function () {
  var fileFilter = $.filter(function (file) {
    return file.stat.isFile();
  });

  return gulp.src([
    path.join(conf.paths.src, '/**/*'),
    path.join('!' + conf.paths.src, '/**/*.{html,css,js,scss,jade}'),
    path.join('!' + conf.paths.src, conf.fontello.config),
    path.join('!' + conf.paths.src, conf.fontello.source)
  ])
    .pipe(fileFilter)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/' + conf.time + '/')));
});

gulp.task('lemonway-css', function () {
  return gulp.src(path.join(conf.paths.src, '/assets/lemonway/style.css'))
    .pipe(gulp.dest(path.join(conf.paths.dist, '/' + conf.time + '/assets/lemonway/')));
});

gulp.task('tos', function () {
  return gulp.src(path.join(conf.paths.src, '/assets/html/tos.html'))
    .pipe(gulp.dest(path.join(conf.paths.dist, '/' + conf.time + '/assets/html/')));
});

gulp.task('clean', function (done) {
  $.del([path.join(conf.paths.dist, '/'), path.join(conf.paths.tmp, '/')]).then(function () {
    done();
  });
});

gulp.task('clean-extra', function () {
  $.del([conf.paths.dist + '/' + conf.time + '/index.html']).then(function () {
    done();
  });
});

gulp.task('config', function () {
  return gulp.src(path.join(conf.paths.src, '/config*.js'))
    .pipe($.insert.append('config.cdn.assets.key=' + conf.time + ';'))
    .pipe(gulp.dest(path.join(conf.paths.dist, '/' + conf.time + '/config')));
});

gulp.task('routing-location', function () {
  return gulp.src(path.join(conf.paths.src, '/routing-location/routing-location.js'))
    .pipe(gulp.dest(path.join(conf.paths.dist, '/' + conf.time + '/routing-location')));
});

gulp.task('build', function (callback) {
  return $.runSequence('clean', 'html', 'fontello', 'fonts', 'other', 'config', 'routing-location', 'clean-extra', 'lemonway-css', 'tos', callback)
});
