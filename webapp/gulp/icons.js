'use strict';

/**
 * If something goes wrong it can be done manually.
 * Please follow instruction: http://wiki.ticksa.com/index.php/Implementation_details#Icons_inside_font
 */

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'del']
});

gulp.task('fontello', ['fontello:deploy'], function () {
  $.del(path.join(conf.paths.src, conf.fontello.dist)).then(function () {
    done();
  });
});

gulp.task('fontello:deploy', ['fontello:get'], function () {
  return gulp.src(path.join(conf.paths.src, conf.fontello.dist, 'font/*.*'))
    .pipe(gulp.dest(path.join(conf.paths.src, '/assets/fonts')));
});

gulp.task('fontello:get', function () {
  return gulp.src(path.join(conf.paths.src, conf.fontello.config))
    .pipe($.fontello())
    .pipe(gulp.dest(path.join(conf.paths.src, conf.fontello.dist)))
});

gulp.task('fontello:clean', function (done) {
  $.del(path.join(conf.paths.src, 'assets/fonts/*.{eot,ttf,woff,woff2,svg}')).then(function () {
    done();
  });
});